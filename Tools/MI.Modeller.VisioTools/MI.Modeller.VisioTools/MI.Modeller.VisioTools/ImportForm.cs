﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using CsvHelper;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.VisioTools
{
    /// <summary>
    /// A form to allow for importing saved shape CSV shapesheet data
    /// </summary>
    public partial class ImportForm : BaseImportExportForm
    {
        /// <summary>
        ///     Creates a new instance of the Import form. This will display a prompt for users to select which sections they would
        ///     like to export
        /// </summary>
        /// <param name="selection">The shapes currently selected in Visio</param>
        public ImportForm(Selection selection) : base(selection, "Import")
        {
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
        }

        /// <summary>
        /// Displays the open file dialog
        /// </summary>
        /// <param name="shapeName">The shape name being operated on</param>
        protected override void ShowDialog(string shapeName)
        {
            openFileDialog.ShowDialog();
            _shapeDictionary[shapeName].Item3.Text = openFileDialog.FileName;
            _shapeDictionary[shapeName].Item2.Checked = true;
        }

        protected override void actionButton_Click(object sender, EventArgs e)
        {
            foreach (var entry in _shapeDictionary)
                if (entry.Value.Item2.Checked)
                    if (string.IsNullOrEmpty(entry.Value.Item3.Text))
                        MessageBox.Show("Unable to import " + entry.Key + " due to invalid filename");
                    else
                        Import(entry.Value.Item1, entry.Value.Item3.Text);
            Close();
        }

        private void Import(Shape s, string source)
        {
            StreamReader textReader = new StreamReader(source);
            CsvReader reader = new CsvReader(textReader);
            var selectedSections = _sections.SelectedSections();
            foreach (var cell in reader.GetRecords<ShapeCell>())
            {
                if (selectedSections.Contains(cell.Section))
                {
                    // Add rows that don't exist
                    if (0 == s.RowExists[cell.Section, cell.Row, 0] && 0 == s.CellExists[cell.RowName,0])
                    {
                        if (CanAddNamedRow(cell.Section))
                        {
                            s.AddNamedRow(cell.Section, cell.RowName, cell.RowTag);
                        }
                        else
                        {
                            s.AddRow(cell.Section, cell.Row, cell.RowTag);
                        }
                    }
                   
                    // Don't try and populate cells that don't exist
                    if (-1 == s.CellExists[cell.Name, 0])
                    {
                        s.Cells[cell.Name].Formula = "=" + cell.Formula;
                    }
                }
            }
            textReader.Close();
        }
    }
}