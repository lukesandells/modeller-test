﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.VisioTools
{
    public partial class ReplaceIDForm : Form
    {

        private readonly Dictionary<int,Shape> _shapeIndexes = new Dictionary<int, Shape>();
        
        public ReplaceIDForm(Selection selection)
        {
            InitializeComponent();
            foreach (Shape s in selection)
            {
                AddShapeToList(s);
            }
        }

        private void AddShapeToList(Shape s)
        {
            var index = checkedListBox.Items.Add(s.Name, true);
            _shapeIndexes.Add(index,s);
            foreach (Shape child in s.Shapes)
            {
                AddShapeToList(child);
            }

        }

        private void allButton_Click(object sender, EventArgs e)
        {
            foreach (int i in _shapeIndexes.Keys)
            {
                checkedListBox.SetItemCheckState(i,CheckState.Checked);
            }
        }

        private void noneButton_Click(object sender, EventArgs e)
        {
            foreach (int i in _shapeIndexes.Keys)
            {
                checkedListBox.SetItemCheckState(i, CheckState.Unchecked);
            }
        }

        private void replaceButton_Click(object sender, EventArgs e)
        {
            foreach (var checkIndex in _shapeIndexes.Keys)
            {
                if (CheckState.Checked.Equals(checkedListBox.GetItemCheckState(checkIndex)))
                {
                    ReplaceIdInShapeFormula(this.originalId.Text, newId.Text, _shapeIndexes[checkIndex]);
                }
            }
            this.Close();
        }

        private void ReplaceIdInShapeFormula(string oldId, string newId, Shape s)
        {
            foreach (VisSectionIndices sectionEnum in Enum.GetValues(typeof(VisSectionIndices)))
            {
                short section = (short)sectionEnum;
                for (short row = 0; row < s.RowCount[(short) section]; row++)
                    if (1 == s.SectionExists[(short) section, 0])
                    {
                        for (short column = 0; column < s.RowsCellCount[(short)section, row]; column++)
                        {
                            s.CellsSRC[(short) section, row, column].Formula =
                                s.CellsSRC[(short) section, row, column].Formula.Replace("Sheet." + oldId, "Sheet." + newId);
                        }
                    }
            }
            foreach (Shape child in s.Shapes)
            {
                ReplaceIdInShapeFormula(oldId, newId, child);
            }
        }
    }
}
