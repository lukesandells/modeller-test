﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.VisioTools
{
    /// <summary>
    ///     Dialog for selecting which Visio sections you would like to import or export
    /// </summary>
    public partial class SectionSelector : Form
    {
        /// <summary>
        ///     Creates a new section selector
        /// </summary>
        public SectionSelector()
        {
            InitializeComponent();
            foreach (var section in Enum.GetValues(typeof(VisSectionIndices)))
            {
                listView.Items.Add(section.ToString());
            }
            allButton_Click(null, null);
        }

        /// <summary>
        ///     Selects all items in the listview
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments</param>
        private void allButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView.Items)
                item.Checked = true;
        }

        /// <summary>
        ///     Selects none of the items in the listview
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments</param>
        private void noneButton_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in listView.Items)
                item.Checked = false;
        }

        /// <summary>
        ///     Closes the form with the current selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OkButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        /// <summary>
        ///     Returns a list of the selected sections
        /// </summary>
        /// <returns>A list of shorts representing the selected sections</returns>
        public List<short> SelectedSections()
        {
            var selectedSections = new List<short>();
            foreach (ListViewItem sectionItem in listView.CheckedItems)
            {
                VisSectionIndices sectionEnum;
                Enum.TryParse(sectionItem.Text, false, out sectionEnum);
                selectedSections.Add((short) sectionEnum);
            }
            return selectedSections;
        }
    }
}