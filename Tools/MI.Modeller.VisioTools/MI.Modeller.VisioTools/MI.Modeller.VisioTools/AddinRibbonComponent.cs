﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Office.Interop.Visio;
using Microsoft.Office.Tools.Ribbon;

namespace MI.Modeller.VisioTools
{
    public partial class AddinRibbonComponent
    {

        private void importButton_Click(object sender, RibbonControlEventArgs e)
        {
            var selection = Globals.ThisAddIn.Application.ActiveWindow.Selection;
            if (selection.Count > 0)
            {
                new ImportForm(selection).ShowDialog();
            }
            else
            {
                MessageBox.Show("Nothing selected to import!");
            }
        }

        private void exportButton_Click(object sender, RibbonControlEventArgs e)
        {
            var selection = Globals.ThisAddIn.Application.ActiveWindow.Selection;
            if (selection.Count > 0)
            {
                new Exportform(selection).ShowDialog();
            }
            else
            {
                MessageBox.Show("Nothing selected to export!");
            }
            
        }

        private void replaceIdButton_Click(object sender, RibbonControlEventArgs e)
        {
            var selection = Globals.ThisAddIn.Application.ActiveWindow.Selection;
            if (selection.Count > 0)
            {
                new ReplaceIDForm(selection).ShowDialog();
            }
            else
            {
                MessageBox.Show("Nothing selected to export!");
            }
        }
    }
}
