﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MI.Modeller.VisioTools
{
    /// <summary>
    /// Holder DTO for writing to the CSV file
    /// </summary>
    public class ShapeCell
    {
        /// <summary>
        /// The section that the cell is in
        /// </summary>
        public short Section { get; set; }
        /// <summary>
        /// The row that the cell is in
        /// </summary>
        public short Row { get; set; }
        /// <summary>
        /// The name of the row being added
        /// </summary>
        public string RowName { get; set; }
        /// <summary>
        /// The row type
        /// </summary>
        public short RowTag { get; set; }
        /// <summary>
        /// Name of the cell
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Formula of the cell
        /// </summary>
        public string Formula { get; set; }
    }
}
