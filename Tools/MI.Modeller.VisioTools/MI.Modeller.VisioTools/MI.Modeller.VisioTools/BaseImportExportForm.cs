﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.VisioTools
{
    /// <summary>
    ///     The form to handle exporting a shape's shapesheet to a CSV
    /// </summary>
    public abstract partial class BaseImportExportForm : Form
    {
        /// <summary>
        ///     The selections selected for export
        /// </summary>
        protected readonly SectionSelector _sections = new SectionSelector();

        /// <summary>
        ///     A dictionary to lookup the items we've added to our flow view
        /// </summary>
        protected readonly Dictionary<string, Tuple<Shape, CheckBox, TextBox>> _shapeDictionary =
            new Dictionary<string, Tuple<Shape, CheckBox, TextBox>>();

        /// <summary>
        ///     Creates a new instance of the BaseImportExportForm. This will display a prompt for users to select which sections
        ///     they would
        ///     like to export
        /// </summary>
        /// <param name="selection">The shapes currently selected in Visio</param>
        public BaseImportExportForm(Selection selection, string actionString)
        {
            InitializeComponent();
            actionButton.Text = actionString;
            foreach (Shape s in selection) AddShape(s);
            _sections.ShowDialog();
        }

        /// <summary>
        ///     Adds a single shape to the form. Also adds to the dictionary
        /// </summary>
        /// <param name="s">The shape to present the option to export</param>
        private void AddShape(Shape s)
        {
            // Each shape needs a checkbox for selecting, a text box for the file and a button to present the prompt
            var check = new CheckBox
            {
                Text = s.Name,
                Width = 250,
                Name = s.Name + "CheckBox"
            };
            check.CheckedChanged += CheckboxChecked;
            flowLayoutPanel.Controls.Add(check);
            var text = new TextBox
            {
                Width = 500,
                Name = s.Name + "TextBox"
            };
            flowLayoutPanel.Controls.Add(text);
            var button = new Button
            {
                Width = 25,
                Text = "...",
                Name = s.Name + "Button"
            };
            button.Click += ButtonClick;
            flowLayoutPanel.Controls.Add(button);
            flowLayoutPanel.SetFlowBreak(button, true);
            _shapeDictionary.Add(s.Name, new Tuple<Shape, CheckBox, TextBox>(s, check, text));
        }

        /// <summary>
        ///     Called when the button is clicked
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments</param>
        private void ButtonClick(object sender, EventArgs e)
        {
            var textBox = (Button) sender;
            ShowDialog(textBox.Name.Replace("Button", ""));
        }

        /// <summary>
        ///     Called when an item is checked
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments</param>
        private void CheckboxChecked(object sender, EventArgs e)
        {
            var checkBox = (CheckBox) sender;
            var textBox = _shapeDictionary[checkBox.Name.Replace("CheckBox", "")].Item3;
            if (checkBox.Checked && string.IsNullOrEmpty(textBox.Text))
                ShowDialog(checkBox.Name.Replace("CheckBox", ""));
        }

        /// <summary>
        ///     Displays the file dialog
        /// </summary>
        /// <param name="shapeName">The name of the shape to use as the default filename</param>
        protected abstract void ShowDialog(string shapeName);

        /// <summary>
        ///     Called when the action button is clicked
        /// </summary>
        /// <param name="sender">The event sender</param>
        /// <param name="e">The event arguments</param>
        protected abstract void actionButton_Click(object sender, EventArgs e);

        protected void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        protected static Boolean CanAddNamedRow(short section)
        {
            //You can add named rows to the 
            //Actions (visSectionAction), 
            //Controls (visSectionControls), 
            //Shape Data (visSectionProp), 
            //User -Defined Cells (visSectionUser), 
            //Hyperlinks (visSectionHyperlink), and 
            //Connection Points (visSectionConnectionPts) ShapeSheet sections
            return (section == (short)VisSectionIndices.visSectionAction
                    || section == (short)VisSectionIndices.visSectionControls
                    || section == (short)VisSectionIndices.visSectionProp
                    || section == (short)VisSectionIndices.visSectionUser
                    || section == (short)VisSectionIndices.visSectionHyperlink
                    || section == (short)VisSectionIndices.visSectionConnectionPts);
        }
    }
}