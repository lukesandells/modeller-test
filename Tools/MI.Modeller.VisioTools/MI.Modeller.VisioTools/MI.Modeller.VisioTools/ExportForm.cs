﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using CsvHelper;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.VisioTools
{
    /// <summary>
    /// A form to allow for importing saved shape CSV shapesheet data
    /// </summary>
    public partial class Exportform : BaseImportExportForm
    {
        public Exportform(Selection selection) : base(selection, "Export")
        {
            // 
            // saveFileDialog
            // 
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.saveFileDialog.DefaultExt = "csv";
            this.saveFileDialog.RestoreDirectory = true;
        }

        /// <summary>
        /// Called when the action button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected override void actionButton_Click(object sender, EventArgs e)
        {
            foreach (var entry in _shapeDictionary)
                if (entry.Value.Item2.Checked)
                    if (string.IsNullOrEmpty(entry.Value.Item3.Text))
                        MessageBox.Show("Unable to export " + entry.Key + " due to invalid filename");
                    else
                        Export(entry.Value.Item1, entry.Value.Item3.Text);
            Close();
        }

        /// <summary>
        ///     Exports a specific shape
        /// </summary>
        /// <param name="s">The shape to export</param>
        /// <param name="destination">Where the CSV file should be written</param>
        private void Export(Shape s, string destination)
        {
            var textWriter = new StreamWriter(destination);
            var writer = new CsvWriter(textWriter);
            writer.WriteHeader(typeof(ShapeCell));

          // Go over each row in each section and export all the cells
            foreach (var section in _sections.SelectedSections())
            {
                for (short row = 0; row < s.RowCount[section]; row++)
                    for (short column = 0; column < s.RowsCellCount[section, row]; column++)
                    {
                        var record = new ShapeCell
                        {
                            Section = section,
                            Row = row,
                            RowName = CanAddNamedRow(section) ? s.CellsSRC[section, row, column].RowName : "",
                            RowTag = s.RowType[section, row],
                            Name = s.CellsSRC[section, row, column].LocalName,
                            Formula = s.CellsSRC[section, row, column].Formula
                        };
                        writer.WriteRecord(record);
                    }
            }
            

            textWriter.Close();
        }

        /// <summary>
        /// Displays the save file dialog
        /// </summary>
        /// <param name="shapeName">The shape name being operated on</param>
        protected override void ShowDialog(string shapeName)
        {
            saveFileDialog.FileName = shapeName + ".csv";
            saveFileDialog.ShowDialog();
            _shapeDictionary[shapeName].Item3.Text = saveFileDialog.FileName;
            _shapeDictionary[shapeName].Item2.Checked = true;
        }
    }
}