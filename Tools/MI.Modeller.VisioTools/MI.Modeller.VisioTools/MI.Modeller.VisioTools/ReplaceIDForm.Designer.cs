﻿namespace MI.Modeller.VisioTools
{
    partial class ReplaceIDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.originalLabel = new System.Windows.Forms.Label();
            this.newLabel = new System.Windows.Forms.Label();
            this.checkedListBox = new System.Windows.Forms.CheckedListBox();
            this.replaceButton = new System.Windows.Forms.Button();
            this.noneButton = new System.Windows.Forms.Button();
            this.allButton = new System.Windows.Forms.Button();
            this.originalId = new System.Windows.Forms.NumericUpDown();
            this.newId = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.originalId)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newId)).BeginInit();
            this.SuspendLayout();
            // 
            // originalLabel
            // 
            this.originalLabel.AutoSize = true;
            this.originalLabel.Location = new System.Drawing.Point(13, 13);
            this.originalLabel.Name = "originalLabel";
            this.originalLabel.Size = new System.Drawing.Size(54, 13);
            this.originalLabel.TabIndex = 1;
            this.originalLabel.Text = "Original Id";
            // 
            // newLabel
            // 
            this.newLabel.AutoSize = true;
            this.newLabel.Location = new System.Drawing.Point(194, 13);
            this.newLabel.Name = "newLabel";
            this.newLabel.Size = new System.Drawing.Size(41, 13);
            this.newLabel.TabIndex = 1;
            this.newLabel.Text = "New Id";
            // 
            // checkedListBox
            // 
            this.checkedListBox.FormattingEnabled = true;
            this.checkedListBox.Location = new System.Drawing.Point(16, 49);
            this.checkedListBox.Name = "checkedListBox";
            this.checkedListBox.Size = new System.Drawing.Size(332, 334);
            this.checkedListBox.TabIndex = 2;
            // 
            // replaceButton
            // 
            this.replaceButton.Location = new System.Drawing.Point(274, 400);
            this.replaceButton.Name = "replaceButton";
            this.replaceButton.Size = new System.Drawing.Size(75, 23);
            this.replaceButton.TabIndex = 3;
            this.replaceButton.Text = "Replace";
            this.replaceButton.UseVisualStyleBackColor = true;
            this.replaceButton.Click += new System.EventHandler(this.replaceButton_Click);
            // 
            // noneButton
            // 
            this.noneButton.Location = new System.Drawing.Point(53, 400);
            this.noneButton.Name = "noneButton";
            this.noneButton.Size = new System.Drawing.Size(44, 23);
            this.noneButton.TabIndex = 4;
            this.noneButton.Text = "None";
            this.noneButton.UseVisualStyleBackColor = true;
            this.noneButton.Click += new System.EventHandler(this.noneButton_Click);
            // 
            // allButton
            // 
            this.allButton.Location = new System.Drawing.Point(17, 400);
            this.allButton.Name = "allButton";
            this.allButton.Size = new System.Drawing.Size(30, 23);
            this.allButton.TabIndex = 5;
            this.allButton.Text = "All";
            this.allButton.UseVisualStyleBackColor = true;
            this.allButton.Click += new System.EventHandler(this.allButton_Click);
            // 
            // originalId
            // 
            this.originalId.Location = new System.Drawing.Point(73, 11);
            this.originalId.Name = "originalId";
            this.originalId.Size = new System.Drawing.Size(101, 20);
            this.originalId.TabIndex = 6;
            // 
            // newId
            // 
            this.newId.Location = new System.Drawing.Point(241, 11);
            this.newId.Name = "newId";
            this.newId.Size = new System.Drawing.Size(101, 20);
            this.newId.TabIndex = 6;
            // 
            // ReplaceIDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(361, 435);
            this.Controls.Add(this.newId);
            this.Controls.Add(this.originalId);
            this.Controls.Add(this.noneButton);
            this.Controls.Add(this.allButton);
            this.Controls.Add(this.replaceButton);
            this.Controls.Add(this.checkedListBox);
            this.Controls.Add(this.newLabel);
            this.Controls.Add(this.originalLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReplaceIDForm";
            this.Text = "Replace ID";
            ((System.ComponentModel.ISupportInitialize)(this.originalId)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label originalLabel;
        private System.Windows.Forms.Label newLabel;
        private System.Windows.Forms.CheckedListBox checkedListBox;
        private System.Windows.Forms.Button replaceButton;
        private System.Windows.Forms.Button noneButton;
        private System.Windows.Forms.Button allButton;
        private System.Windows.Forms.NumericUpDown originalId;
        private System.Windows.Forms.NumericUpDown newId;
    }
}