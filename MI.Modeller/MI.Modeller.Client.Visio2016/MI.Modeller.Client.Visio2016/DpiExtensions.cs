﻿using MI.Framework;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Windows.Forms;
using MI.Framework.ObjectBrowsing;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///     Extension methods for handling DPI related functions
	/// </summary>
	public static class DpiExtensions
    {
        #region Static Fields and Constants

        /// <summary>
        ///     All forms and controls must be designed in 96 DPI
        /// </summary>
        public const int C_DESIGN_DPI = 96;

        /// <summary>
        ///     The icon sets for Equipment Levels
        /// </summary>
        private static readonly IDictionary<EquipmentLevel, IconSet> _equipmentLevelIcons = new Dictionary<EquipmentLevel, IconSet>
        {
            {EquipmentLevel.Enterprise, new IconSet(Properties.Resources.Enterprise_16, Properties.Resources.Enterprise_20, Properties.Resources.Enterprise_24, Properties.Resources.Enterprise_32, Properties.Resources.Enterprise_40)},
            {EquipmentLevel.Site, new IconSet(Properties.Resources.Site_16, Properties.Resources.Site_20, Properties.Resources.Site_24, Properties.Resources.Site_32, Properties.Resources.Site_40)},
            {EquipmentLevel.Area, new IconSet(Properties.Resources.Area_16, Properties.Resources.Area_20, Properties.Resources.Area_24, Properties.Resources.Area_32, Properties.Resources.Area_40)},
            {EquipmentLevel.WorkCenter, new IconSet(Properties.Resources.WorkCenter_16, Properties.Resources.WorkCenter_20, Properties.Resources.WorkCenter_24, Properties.Resources.WorkCenter_32, Properties.Resources.WorkCenter_40)},
            {EquipmentLevel.ProductionLine, new IconSet(Properties.Resources.WorkCenter_16, Properties.Resources.WorkCenter_20, Properties.Resources.WorkCenter_24, Properties.Resources.WorkCenter_32, Properties.Resources.WorkCenter_40)},
            {EquipmentLevel.ProductionUnit, new IconSet(Properties.Resources.WorkCenter_16, Properties.Resources.WorkCenter_20, Properties.Resources.WorkCenter_24, Properties.Resources.WorkCenter_32, Properties.Resources.WorkCenter_40)},
            {EquipmentLevel.ProcessCell, new IconSet(Properties.Resources.WorkCenter_16, Properties.Resources.WorkCenter_20, Properties.Resources.WorkCenter_24, Properties.Resources.WorkCenter_32, Properties.Resources.WorkCenter_40)},
            {EquipmentLevel.StorageZone, new IconSet(Properties.Resources.WorkCenter_16, Properties.Resources.WorkCenter_20, Properties.Resources.WorkCenter_24, Properties.Resources.WorkCenter_32, Properties.Resources.WorkCenter_40)},
            {EquipmentLevel.WorkUnit, new IconSet(Properties.Resources.WorkUnit_16, Properties.Resources.WorkUnit_20, Properties.Resources.WorkUnit_24, Properties.Resources.WorkUnit_32, Properties.Resources.WorkUnit_40)},
            {EquipmentLevel.Unit, new IconSet(Properties.Resources.WorkUnit_16, Properties.Resources.WorkUnit_20, Properties.Resources.WorkUnit_24, Properties.Resources.WorkUnit_32, Properties.Resources.WorkUnit_40)},
            {EquipmentLevel.WorkCell, new IconSet(Properties.Resources.WorkUnit_16, Properties.Resources.WorkUnit_20, Properties.Resources.WorkUnit_24, Properties.Resources.WorkUnit_32, Properties.Resources.WorkUnit_40)},
			{EquipmentLevel.StorageUnit, new IconSet(Properties.Resources.WorkUnit_16, Properties.Resources.WorkUnit_20, Properties.Resources.WorkUnit_24, Properties.Resources.WorkUnit_32, Properties.Resources.WorkUnit_40)},
	        {EquipmentLevel.ControlModule, new IconSet(Properties.Resources.ControlModule_16, Properties.Resources.ControlModule_20, Properties.Resources.ControlModule_24, Properties.Resources.ControlModule_32, Properties.Resources.ControlModule_40)},
	        {EquipmentLevel.EquipmentModule, new IconSet(Properties.Resources.EquipmentModule_16, Properties.Resources.EquipmentModule_20, Properties.Resources.EquipmentModule_24, Properties.Resources.EquipmentModule_32, Properties.Resources.EquipmentModule_40)},
		};

        /// <summary>
        ///     The icon sets for Equipment Levels when used as a class
        /// </summary>
        private static readonly IDictionary<EquipmentLevel, IconSet> _equipmentLevelClassIcons = new Dictionary<EquipmentLevel, IconSet>
        {
            {EquipmentLevel.Enterprise, new IconSet(Properties.Resources.EnterpriseClass_16, Properties.Resources.EnterpriseClass_20, Properties.Resources.EnterpriseClass_24, Properties.Resources.EnterpriseClass_32, Properties.Resources.EnterpriseClass_40)},
            {EquipmentLevel.Site, new IconSet(Properties.Resources.SiteClass_16, Properties.Resources.SiteClass_20, Properties.Resources.SiteClass_24, Properties.Resources.SiteClass_32, Properties.Resources.SiteClass_40)},
            {EquipmentLevel.Area, new IconSet(Properties.Resources.AreaClass_16, Properties.Resources.AreaClass_20, Properties.Resources.AreaClass_24, Properties.Resources.AreaClass_32, Properties.Resources.AreaClass_40)},
            {EquipmentLevel.WorkCenter, new IconSet(Properties.Resources.WorkCenterClass_16, Properties.Resources.WorkCenterClass_20, Properties.Resources.WorkCenterClass_24, Properties.Resources.WorkCenterClass_32, Properties.Resources.WorkCenterClass_40)},
            {EquipmentLevel.ProductionLine, new IconSet(Properties.Resources.WorkCenterClass_16, Properties.Resources.WorkCenterClass_20, Properties.Resources.WorkCenterClass_24, Properties.Resources.WorkCenterClass_32, Properties.Resources.WorkCenterClass_40)},
            {EquipmentLevel.ProductionUnit, new IconSet(Properties.Resources.WorkCenterClass_16, Properties.Resources.WorkCenterClass_20, Properties.Resources.WorkCenterClass_24, Properties.Resources.WorkCenterClass_32, Properties.Resources.WorkCenterClass_40)},
            {EquipmentLevel.ProcessCell, new IconSet(Properties.Resources.WorkCenterClass_16, Properties.Resources.WorkCenterClass_20, Properties.Resources.WorkCenterClass_24, Properties.Resources.WorkCenterClass_32, Properties.Resources.WorkCenterClass_40)},
            {EquipmentLevel.StorageZone, new IconSet(Properties.Resources.WorkCenterClass_16, Properties.Resources.WorkCenterClass_20, Properties.Resources.WorkCenterClass_24, Properties.Resources.WorkCenterClass_32, Properties.Resources.WorkCenterClass_40)},
            {EquipmentLevel.WorkUnit, new IconSet(Properties.Resources.WorkUnitClass_16, Properties.Resources.WorkUnitClass_20, Properties.Resources.WorkUnitClass_24, Properties.Resources.WorkUnitClass_32, Properties.Resources.WorkUnitClass_40)},
            {EquipmentLevel.Unit, new IconSet(Properties.Resources.WorkUnitClass_16, Properties.Resources.WorkUnitClass_20, Properties.Resources.WorkUnitClass_24, Properties.Resources.WorkUnitClass_32, Properties.Resources.WorkUnitClass_40)},
            {EquipmentLevel.WorkCell, new IconSet(Properties.Resources.WorkUnitClass_16, Properties.Resources.WorkUnitClass_20, Properties.Resources.WorkUnitClass_24, Properties.Resources.WorkUnitClass_32, Properties.Resources.WorkUnitClass_40)},
            {EquipmentLevel.StorageUnit, new IconSet(Properties.Resources.WorkUnitClass_16, Properties.Resources.WorkUnitClass_20, Properties.Resources.WorkUnitClass_24, Properties.Resources.WorkUnitClass_32, Properties.Resources.WorkUnitClass_40)},
	        {EquipmentLevel.ControlModule, new IconSet(Properties.Resources.ControlModuleClass_16, Properties.Resources.ControlModuleClass_20, Properties.Resources.ControlModuleClass_24, Properties.Resources.ControlModuleClass_32, Properties.Resources.ControlModuleClass_40)},
	        {EquipmentLevel.EquipmentModule, new IconSet(Properties.Resources.EquipmentModuleClass_16, Properties.Resources.EquipmentModuleClass_20, Properties.Resources.EquipmentModuleClass_24, Properties.Resources.EquipmentModuleClass_32, Properties.Resources.EquipmentModuleClass_40)},
		};

        /// <summary>
        ///     The icon sets for Model Objects
        /// </summary>
        private static readonly IDictionary<Guid, IconSet> _modelObjectIcons = new Dictionary<Guid, IconSet>
        {
            {NodeTypeId.MaterialClass, new IconSet(Properties.Resources.MaterialClass_16, Properties.Resources.MaterialClass_20, Properties.Resources.MaterialClass_24, Properties.Resources.MaterialClass_32, Properties.Resources.MaterialClass_40)},
            {NodeTypeId.MaterialDefinition, new IconSet(Properties.Resources.MaterialDefinition_16, Properties.Resources.MaterialDefinition_20, Properties.Resources.MaterialDefinition_24, Properties.Resources.MaterialDefinition_32, Properties.Resources.MaterialDefinition_40)},
            {NodeTypeId.Model, new IconSet(Properties.Resources.Model_16, Properties.Resources.Model_20, Properties.Resources.Model_24, Properties.Resources.Model_32, Properties.Resources.Model_40)},
            {NodeTypeId.OperationsDefinition, new IconSet(Properties.Resources.OperationsDefinition_16, Properties.Resources.OperationsDefinition_20, Properties.Resources.OperationsDefinition_24, Properties.Resources.OperationsDefinition_32, Properties.Resources.OperationsDefinition_40)},
            {NodeTypeId.OperationsSegment, new IconSet(Properties.Resources.OperationsSegment_16, Properties.Resources.OperationsSegment_20, Properties.Resources.OperationsSegment_24, Properties.Resources.OperationsSegment_32, Properties.Resources.OperationsSegment_40)},
			{NodeTypeId.WorkMaster, new IconSet(Properties.Resources.WorkMaster_16, Properties.Resources.WorkMaster_20, Properties.Resources.WorkMaster_24, Properties.Resources.WorkMaster_32, Properties.Resources.WorkMaster_40)},
			{NodeTypeId.ProcessSegment, new IconSet(Properties.Resources.ProcessSegment_16, Properties.Resources.ProcessSegment_20, Properties.Resources.ProcessSegment_24, Properties.Resources.ProcessSegment_32, Properties.Resources.ProcessSegment_40)},
			{NodeTypeId.PhysicalAssetClass, new IconSet(Properties.Resources.PhysicalAssetClass_16, Properties.Resources.PhysicalAssetClass_20, Properties.Resources.PhysicalAssetClass_24, Properties.Resources.PhysicalAssetClass_32, Properties.Resources.PhysicalAssetClass_40)},
	        {NodeTypeId.PersonnelClass, new IconSet(Properties.Resources.PersonnelClass_16, Properties.Resources.PersonnelClass_20, Properties.Resources.PersonnelClass_24, Properties.Resources.PersonnelClass_32, Properties.Resources.PersonnelClass_40)},

	        {NodeTypeId.OperationsSegmentMaterialSpecification, new IconSet(Properties.Resources.MaterialSpecification_16, Properties.Resources.MaterialSpecification_20, Properties.Resources.MaterialSpecification_24, Properties.Resources.MaterialSpecification_32, Properties.Resources.MaterialSpecification_40)},
	        {NodeTypeId.OperationsSegmentEquipmentSpecification, new IconSet(Properties.Resources.EquipmentSpecification_16, Properties.Resources.EquipmentSpecification_20, Properties.Resources.EquipmentSpecification_24, Properties.Resources.EquipmentSpecification_32, Properties.Resources.EquipmentSpecification_40)},
	        {NodeTypeId.OperationsSegmentPhysicalAssetSpecification, new IconSet(Properties.Resources.PhysicalAssetSpecification_16, Properties.Resources.PhysicalAssetSpecification_20, Properties.Resources.PhysicalAssetSpecification_24, Properties.Resources.PhysicalAssetSpecification_32, Properties.Resources.PhysicalAssetSpecification_40)},
	        {NodeTypeId.OperationsSegmentPersonnelSpecification, new IconSet(Properties.Resources.PersonnelSpecification_16, Properties.Resources.PersonnelSpecification_20, Properties.Resources.PersonnelSpecification_24, Properties.Resources.PersonnelSpecification_32, Properties.Resources.PersonnelSpecification_40)},

	        {NodeTypeId.ProcessSegmentMaterialSpecification, new IconSet(Properties.Resources.MaterialSpecification_16, Properties.Resources.MaterialSpecification_20, Properties.Resources.MaterialSpecification_24, Properties.Resources.MaterialSpecification_32, Properties.Resources.MaterialSpecification_40)},
	        {NodeTypeId.ProcessSegmentEquipmentSpecification, new IconSet(Properties.Resources.EquipmentSpecification_16, Properties.Resources.EquipmentSpecification_20, Properties.Resources.EquipmentSpecification_24, Properties.Resources.EquipmentSpecification_32, Properties.Resources.EquipmentSpecification_40)},
	        {NodeTypeId.ProcessSegmentPhysicalAssetSpecification, new IconSet(Properties.Resources.PhysicalAssetSpecification_16, Properties.Resources.PhysicalAssetSpecification_20, Properties.Resources.PhysicalAssetSpecification_24, Properties.Resources.PhysicalAssetSpecification_32, Properties.Resources.PhysicalAssetSpecification_40)},
	        {NodeTypeId.ProcessSegmentPersonnelSpecification, new IconSet(Properties.Resources.PersonnelSpecification_16, Properties.Resources.PersonnelSpecification_20, Properties.Resources.PersonnelSpecification_24, Properties.Resources.PersonnelSpecification_32, Properties.Resources.PersonnelSpecification_40)},

	        {NodeTypeId.WorkMasterMaterialSpecification, new IconSet(Properties.Resources.MaterialSpecification_16, Properties.Resources.MaterialSpecification_20, Properties.Resources.MaterialSpecification_24, Properties.Resources.MaterialSpecification_32, Properties.Resources.MaterialSpecification_40)},
	        {NodeTypeId.WorkMasterEquipmentSpecification, new IconSet(Properties.Resources.EquipmentSpecification_16, Properties.Resources.EquipmentSpecification_20, Properties.Resources.EquipmentSpecification_24, Properties.Resources.EquipmentSpecification_32, Properties.Resources.EquipmentSpecification_40)},
	        {NodeTypeId.WorkMasterPhysicalAssetSpecification, new IconSet(Properties.Resources.PhysicalAssetSpecification_16, Properties.Resources.PhysicalAssetSpecification_20, Properties.Resources.PhysicalAssetSpecification_24, Properties.Resources.PhysicalAssetSpecification_32, Properties.Resources.PhysicalAssetSpecification_40)},
	        {NodeTypeId.WorkMasterPersonnelSpecification, new IconSet(Properties.Resources.PersonnelSpecification_16, Properties.Resources.PersonnelSpecification_20, Properties.Resources.PersonnelSpecification_24, Properties.Resources.PersonnelSpecification_32, Properties.Resources.PersonnelSpecification_40)},
		};

        #endregion

        #region Other Methods

        /// <summary>
        ///     Gets the relevent icon for a givem model object or equipmentlevel
        /// </summary>
        /// <param name="control">The control</param>
        /// <param name="modelObject">The type of model object the icon is for</param>
        /// <param name="equipmentLevel">The equipment level of the Hierarchy Scope, Equipment or Equipment Class</param>
        /// <returns></returns>
        public static Image GetScaledIcon(this Control control, BrowserNode browserNode)
        {
            using (var g = control.CreateGraphics())
            {
                if (browserNode.TypeId == NodeTypeId.HierarchyScope || browserNode.TypeId == NodeTypeId.Equipment)
                {
	                var equipmentLevel = (browserNode.TargetObject as HierarchyScope)?.EquipmentLevel ?? (browserNode.TargetObject as Equipment)?.EquipmentLevel;
	                if (equipmentLevel == null)
	                {
						return null; 
	                }
	                return _equipmentLevelIcons[(EquipmentLevel)equipmentLevel.Value].GetImage(g);

				}
                if (browserNode.TypeId == NodeTypeId.EquipmentClass)
                {
	                var equipmentLevel = ((EquipmentClass) browserNode.TargetObject).EquipmentLevel;
	                if (equipmentLevel == EquipmentLevel.Other)
	                {
		                return null;
	                }
					return _equipmentLevelClassIcons[(EquipmentLevel)equipmentLevel]
						.GetImage(g);
				}

                return _modelObjectIcons.ContainsKey(browserNode.TypeId)
					? _modelObjectIcons[browserNode.TypeId].GetImage(g)
					:  null;
            }
        }

        /// <summary>
        ///     Gets the scale factor for this control
        /// </summary>
        /// <param name="control">This control</param>
        /// <returns></returns>
        public static float ScaleFactor(this Control control)
        {
            using (var g = control.CreateGraphics())
            {
                return g.DpiX / C_DESIGN_DPI;
            }
        }

        /// <summary>
        ///     Scale a value from it's original value to one relevent for the current form or control's DPI
        /// </summary>
        /// <param name="control">The control that the value is scaled for</param>
        /// <param name="originalValue">The value</param>
        /// <returns></returns>
        public static int ScaleValue(this Control control, int originalValue)
        {
            return (int) Math.Round(originalValue * control.ScaleFactor());
        }

        #endregion
    }

    /// <summary>
    ///     An icon set reprsents the set of icons that are supported in modeller
    /// </summary>
    internal class IconSet
    {
        #region  Fields/Properties

        /// <summary>
        ///     The image to use at 96 DPI
        /// </summary>
        private readonly Image _image96;

        /// <summary>
        ///     The image to use at 120 DPI
        /// </summary>
        private readonly Image _image120;

        /// <summary>
        ///     The image to use at 144 DPI
        /// </summary>
        private readonly Image _image144;

        /// <summary>
        ///     The image to use at 192 DPI
        /// </summary>
        private readonly Image _image192;

        /// <summary>
        ///     The image to use at 240 DPI
        /// </summary>
        private readonly Image _image240;

        #endregion

        #region Constructors

        /// <summary>
        ///     Constructs a new IconSet
        /// </summary>
        /// <param name="image96"></param>
        /// <param name="image120"></param>
        /// <param name="image144"></param>
        /// <param name="image192"></param>
        /// <param name="image240"></param>
        internal IconSet(Image image96, Image image120, Image image144, Image image192, Image image240)
        {
            _image96 = image96;
            _image120 = image120;
            _image144 = image144;
            _image192 = image192;
            _image240 = image240;
        }

        #endregion

        #region Other Methods

        /// <summary>
        ///     The the appropriate image for this graphics set
        /// </summary>
        /// <param name="g"></param>
        /// <returns></returns>
        public Image GetImage(Graphics g)
        {
	        return _image96;
			// TODO DPI scaling messes this up...
			if (g.DpiX < 96)
			{
				return ScaleImage(_image96, g, 96);
			}
			if (Math.Abs(g.DpiX - 96) < 1)
			{
				return _image96;
			}
			if (g.DpiX < 120)
			{
				return ScaleImage(_image120, g, 120);
			}
			if (Math.Abs(120F - g.DpiX) < 1)
			{
				return _image120;
			}
			if (g.DpiX < 144)
			{
				return ScaleImage(_image144, g, 144);
			}
			if (Math.Abs(g.DpiX - 144) < 1)
			{
				return _image144;
			}
			if (g.DpiX < 192)
			{
				return ScaleImage(_image192, g, 192);
			}
			if (Math.Abs(g.DpiX - 192) < 1)
			{
				return _image192;
			}
			// If it's below or above 240 it will be scaled by default
			if (Math.Abs(g.DpiX - 240) < 1)
			{
				return _image240;
			}
			return ScaleImage(_image96, g, 96);
		}

        /// <summary>
        ///     Scales an image to the current DPI
        /// </summary>
        /// <param name="image"></param>
        /// <param name="g"></param>
        /// <param name="originalDpi">The original DPI of the icon</param>
        /// <returns></returns>
        private Image ScaleImage(Image image, Graphics g, int originalDpi)
        {
            // Adapted from https://stackoverflow.com/questions/1922040/resize-an-image-c-sharp
            var width = (int) Math.Round(image.Width * g.DpiX / originalDpi);
            var height = (int) Math.Round(image.Height * g.DpiY / originalDpi);

            var targetSize = new Rectangle(0, 0, width, height);
            var targetImage = new Bitmap(width, height);

            using (var g2 = Graphics.FromImage(targetImage))
            {
                g2.CompositingMode = CompositingMode.SourceCopy;
                g2.CompositingQuality = CompositingQuality.HighQuality;
                g2.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g2.SmoothingMode = SmoothingMode.HighQuality;
                g2.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    // Prevents ghosting
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    g2.DrawImage(image, targetSize, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }
            return targetImage;
        }

        #endregion
    }
}
