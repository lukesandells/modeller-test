﻿using System.Linq;
using Microsoft.Office.Interop.Visio;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///     Undo unit for Visio 2016
	/// </summary>
	public class Visio2016UndoUnit : IVBUndoUnit
	{
		#region Constructors

		public Visio2016UndoUnit(NodeTransaction nodeTransaction, string description)
		{
			_nodeTransaction = nodeTransaction;
			Description = description;
		}

		#endregion

		#region  Interface Implementations

		/// <summary>
		///     Perform the undo or redo action
		/// </summary>
		/// <param name="pMgr"></param>
		public void Do(IVBUndoManager pMgr)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var reverseAction = _nodeTransaction.RollBackAndPersist();
				// And add self back to queue for undo/redo
				pMgr.Add(new Visio2016UndoUnit(reverseAction, Description));
				scope.Done();
			}
		}

		/// <summary>
		///     Called when something else is added to the undo/redo queue
		/// </summary>
		public void OnNextAdd()
		{
			// Nothing at this stage
		}

		#endregion

		#region  Fields

		/// <summary>
		///     The node transaction this undo unit is for
		/// </summary>
		private readonly NodeTransaction _nodeTransaction;

		/// <summary>
		///     A description for this undo/redo action
		/// </summary>
		public string Description { get; }

		/// <summary>
		///     Approximate memory size in bytes of the undo unit
		/// </summary>
		public int UnitSize => 6; // From SDK example code

		/// <summary>
		///     The CLSID and a type identifier for the undo unit.
		/// </summary>
		public string UnitTypeCLSID => "";

		/// <summary>
		///     The type of the undo unit
		/// </summary>
		public int UnitTypeLong => 0; // From SDK example code

		#endregion
	}
}