﻿using MI.Framework.Validation;
using MI.Modeller.Client.ModellerEventArgs;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Domain;
using Microsoft.Office.Interop.Visio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Timers;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Visio.Application;
using Page = Microsoft.Office.Interop.Visio.Page;
using Timer = System.Timers.Timer;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///     Class ModellerVisio2016Document.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.IModellerDocument"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.IModellerDocument"</cref>
	/// </seealso>
	public class ModellerVisio2016Document : IModellerDocument
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The c hierarchy scope cell name
        /// </summary>
        private const string C_HIERARCHY_SCOPE_CELL_NAME = "User.HierarchyScope";

        /// <summary>
        ///     The Hover Delay period
        /// </summary>
        private const int C_HOVER_DELAY = 1000;

        /// <summary>
        ///     The intneral ID of the model
        /// </summary>
        private const string C_MODEL_INTERNAL_ID = "User.ModelInternalId";

        /// <summary>
        ///     The c model type cell name
        /// </summary>
        private const string C_MODEL_TYPE_CELL_NAME = "User.ModelType";

        /// <summary>
        ///     The c modeller version cell name
        /// </summary>
        private const string C_MODELLER_VERSION_CELL_NAME = "User.ModellerVersion";

        /// <summary>
        ///     The modeller visio2016 documents
        /// </summary>
        private static readonly IDictionary<Document, ModellerVisio2016Document> _modellerVisio2016Documents =
            new Dictionary<Document, ModellerVisio2016Document>();

        #endregion

        #region  Fields

        /// <summary>
        ///     The hover timer - counts once the cursor is idle
        /// </summary>
        private readonly Timer _hoverTimer = new Timer(C_HOVER_DELAY);

        /// <summary>
        ///     The document
        /// </summary>
        private readonly Document _document;

        /// <summary>
        ///     The hover shape
        /// </summary>
        private IModellerShape _hoverShape;

        /// <summary>
        ///     The hover timer triggered flag
        /// </summary>
        private bool _hoverTimerTriggered;

        /// <summary>
        ///     The cursor x position
        /// </summary>
        private int _cursorX;

        /// <summary>
        ///     The cursor y position
        /// </summary>
        private int _cursorY;

        /// <summary>
        /// The modeller presenter to interact with
        /// </summary>
        private readonly IModellerPresenter _modellerPresenter;

        /// <summary>
        ///     The underlying document type
        /// </summary>
        public object BaseDocument => _document;

        /// <summary>
        ///     Gets the document identifier.
        /// </summary>
        /// <value>The document identifier.</value>
        public int DocumentId => _document.ID;

        /// <summary>
        ///     The file path
        /// </summary>
        public string FilePath => _document.FullName;

        /// <summary>
        ///     Gets or sets the hierarchy scope identifier.
        /// </summary>
        /// <value>The hierarchy scope identifier.</value>
        public Guid HierarchyScopeInternalId
        {
            get
            {
                var hierarchyScope = _document.DocumentSheet.CellsU[C_HIERARCHY_SCOPE_CELL_NAME].Formula;
                return string.IsNullOrEmpty(hierarchyScope)
                    ? default(Guid)
                    : Guid.Parse(hierarchyScope.Trim('"')
                        .Replace(@"""""",
                            @""""));
            }
            set { _document.DocumentSheet.CellsU[C_HIERARCHY_SCOPE_CELL_NAME].FormulaU = $"\"{value}\""; }
        }

        /// <summary>
        ///     Indicates whether or not this document is a modeller document
        /// </summary>
        public bool IsModellerDocument => CheckIsModellerDocument(_document);

		/// <summary>
		/// The pages within this visio document
		/// </summary>
	    public IEnumerable<IModellerPage> Pages 
	    {
		    get
		    {
			    foreach (var p in _document.Pages)
			    {
				    yield return new ModellerVisio2016Page((Page) p, _modellerPresenter);
			    }
		    }
	    }

	    public IEnumerable<IModellerConnector> Connectors
	    {
		    get
		    {
				foreach (Page p in _document.Pages)
				{
					foreach (Shape s in p.Shapes)
					{
						if (s.LineStyle == ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE)
						{
							yield return new ModellerVisio2016Connector(s, _modellerPresenter);
						}
					}
				}
			}
	    }

	    /// <summary>
	///     Gets or sets the model.
	/// </summary>
	/// <value>The model.</value>
	public Model Model { get; set; }

        public Guid ModelInternalId
        {
            get
            {
                var internalId = _document.DocumentSheet.CellsU[C_MODEL_INTERNAL_ID].Formula;
                return string.IsNullOrEmpty(internalId)
                    ? default(Guid)
                    : Guid.Parse(internalId.Trim('"')
                        .Replace(@"""""",
                            @""""));
            }
            set { _document.DocumentSheet.CellsU[C_MODEL_INTERNAL_ID].FormulaU = $"\"{value}\""; }
        }

        /// <summary>
        ///     Gets the type of the model.
        /// </summary>
        /// <value>The type of the model.</value>
        public ModelType ModelType
        {
            get
            {
                if (_document.DocumentSheet.CellExistsU[C_MODEL_TYPE_CELL_NAME,
                        0] == 0)
                {
                    return ModelType.Unknown;
                }

                var modelType = _document.DocumentSheet.CellsU[C_MODEL_TYPE_CELL_NAME].Formula;

                if (string.IsNullOrEmpty(modelType))
                {
                    return ModelType.Unknown;
                }
                modelType = modelType.Trim('"')
                    .Replace(@"""""",
                        @"""");

                var type = typeof(ModelType);

                foreach (var modelTypeValue in Enum.GetValues(typeof(ModelType))
                    .Cast<ModelType>())
                {
                    var memInfo = type.GetMember(modelTypeValue.ToString());
                    var attributes = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute),
                        false);
                    var description = ((DescriptionAttribute) attributes[0]).Description;
                    if (string.Equals(Regex.Replace(description,
                                @"\s+",
                                "")
                            .ToUpper(),
                        Regex.Replace(modelType,
                                @"\s+",
                                "")
                            .ToUpper()))

                    {
                        return modelTypeValue;
                    }
                }

                return ModelType.Unknown;
            }
        }

        /// <summary>
        ///     Gets the selection.
        /// </summary>
        /// <value>The selection.</value>
        public IEnumerable<IModellerShape> Selection => (from Shape shape in _document.Application.ActiveWindow.Selection.OfType<Shape>()
                select ((VisioShapeFactory)_modellerPresenter.ShapeFactory).ConstructShape(shape, _modellerPresenter));

        /// <summary>
        ///     Gets the shapes.
        /// </summary>
        /// <value>The shapes.</value>
        public IEnumerable<IModellerShape> Shapes
        {
            get
            {
                foreach (Page page in _document.Pages)
                {
                    foreach (Shape shape in page.Shapes)
                    {
	                    if (shape.LineStyle != ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE)
	                    {
		                    yield return ((VisioShapeFactory)_modellerPresenter.ShapeFactory).ConstructShape(shape, _modellerPresenter);
	                    }
                    }
                }
            }
        }

	    /// <summary>
	    ///     Gets the title.
	    /// </summary>
	    /// <value>The title.</value>
	    public string Title
	    {
		    get => string.IsNullOrEmpty(_document.Title)
			    ? _document.Name
			    : _document.Title;
		    set => _document.Title = value;
	    }

	    #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="ModellerVisio2016Document" /> class.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="modellerPresenter">The modeller presenter to interact with</param>
        private ModellerVisio2016Document(Document doc,
            IModellerPresenter modellerPresenter)
        {
            _document = doc;
            _modellerPresenter = modellerPresenter;

			// required for Hover 
	        _document.Application.ActiveWindow.MouseMove += HandleMouseMove;
	        _document.Application.ActiveWindow.Application.VisioIsIdle += HandleVisioIdle;

            _hoverTimer.Enabled = false;
            _hoverTimer.Elapsed += HandleHoverTimerElapsedEvent;
            _hoverTimer.AutoReset = false;

            HoverOverErrorShape += delegate(object sender,
                ModellerShapeEventArgs e)
            {
                MessageBox.Show(e.ModellerShape.IdText);
            };
        }

        #endregion

        #region  Interface Implementations

        /// <summary>
        ///     Occurs when [hover over error shape].
        /// </summary>
        public event EventHandler<ModellerShapeEventArgs> HoverOverErrorShape;

        /// <summary>
        ///     Deselects all.
        /// </summary>
        public void DeselectAll()
        {
	        _document.Application.ActiveWindow.DeselectAll();
        }

        /// <summary>
        ///     Selects all.
        /// </summary>
        public void SelectAll()
        {
	        _document.Application.ActiveWindow.SelectAll();
        }

	    public void AddPage(Domain.Page page)
	    {
		    new ModellerVisio2016Page(_document.Pages.Add(), _modellerPresenter)
		    {
			    Title = page.DisplayId,
				PageInternalId = page.Id
		    };
	    }

	    #endregion

        #region Members

        /// <summary>
        ///     Gets a value indicating whether the given instance is a modeller document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <returns><c>true</c> if [is modeller document] [the specified document]; otherwise, <c>false</c>.</returns>
        /// <value><c>true</c> if this instance is modeller document; otherwise, <c>false</c>.</value>
        public static bool CheckIsModellerDocument(Document document)
            => document.DocumentSheet.CellExistsU[C_MODELLER_VERSION_CELL_NAME,
                   0] != 0;

        // Rationale for this method, and Hiding the constructor
        // Previously there was an instance method on ModellerVisio2016Document called IsModellerDocument()
        // problem is you have to instantiate the object before checking if it is in fact a modeller document
        // Now - the GC doesnt clean the object up if it is NOT a ModellerDocument. Because the object holds a reference to the Visio document
        // So now the incorrect ModellerVisio2016Document starts responding to all sorts of events like Mouseover and idle
        // So the IsModellerDocument is now static. But the issues are not solved
        // Whenever a document is opened or activated, the ModellerAddin attempts to, and does re-create a new instance of ModellerVisio2016Document
        // using the same underlying Document object. We now have dozens of ModellerVisio2016Documents floating around, each responding to and triggering the same
        // mouse , idle etc events
        // We could ensure that the class creating the instance holds a list of existing object. But this doesnt guarantee that there isnt another class
        // out there that also needs an instance - and should receive the same instance
        // SO - we implement a simple Multiton pattern here. Make sure only 1 instance of a given ModellerVisio2016Document can exist for a given document
        /// <summary>
        ///     Gets the modeller visio2016 document.
        /// </summary>
        /// <param name="doc">The document.</param>
        /// <param name="modellerPresenter"></param>
        /// <returns>ModellerVisio2016Document.</returns>
        public static ModellerVisio2016Document GetModellerVisio2016Document(Document doc,
            IModellerPresenter modellerPresenter)
        {
            ArgumentValidation.AssertNotNull(doc,
                nameof(doc));

            // Only create actual Modeller Documents
            if (!CheckIsModellerDocument(doc))
            {
                return null;
            }

            if (_modellerVisio2016Documents.ContainsKey(doc))
            {
                return _modellerVisio2016Documents[doc];
            }

            var modellerDocument = new ModellerVisio2016Document(doc, modellerPresenter);
            _modellerVisio2016Documents.Add(doc,
                modellerDocument);
            return modellerDocument;
        }

        /// <summary>
        ///     Determines whether [is cursor over error shape].
        /// </summary>
        /// <returns>Visio2016ModellerShape.</returns>
        private IModellerShape GetErrorShapeUnderCursor()
        {
            return _document.Application.ActiveWindow.Application.ActivePage?.Shapes.OfType<Shape>()
                .Select(visioShape => ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(visioShape, _modellerPresenter))
                .Where(modellerShape => modellerShape.IsModellerShape())
                .FirstOrDefault(modellerShape => modellerShape.Bounds()
                                                     .Contains(_cursorX,
                                                         _cursorY) && modellerShape.HasErrors);
        }

        /// <summary>
        ///     Handles the hover timer elapsed event.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ElapsedEventArgs" /> instance containing the event data.</param>
        private void HandleHoverTimerElapsedEvent(object sender,
            ElapsedEventArgs e)
        {
            // Order is important here. If you swap these 2 around, you end up with the idle event being triggered again for the time between
            // setting the 2 flags, and a new timer event being fired
            // Hover timer triggered is to prevent multiple invokations - 
            // user hovers over shape till hover is triggered 
            // then moves mouse and hovers again, before responding to previous event
            _hoverTimerTriggered = true;
            _hoverTimer.Enabled = false;

            if (_hoverShape == null)
            {
                return;
            }

	        var args = new ModellerShapeEventArgs
	        {
		        ModellerShape = _hoverShape
	        };


            HoverOverErrorShape?.Invoke(this,
                args);

            _hoverTimer.Enabled = false;
            _hoverTimerTriggered = false;
        }

        /// <summary>
        ///     Handles the mouse move.
        /// </summary>
        /// <param name="button">The button.</param>
        /// <param name="keybuttonstate">The keybuttonstate.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="canceldefault">if set to <c>true</c> [canceldefault].</param>
        private void HandleMouseMove(int button,
            int keybuttonstate,
            double x,
            double y,
            ref bool canceldefault)
        {
            // For Hover functionality
            // as soon as the cursor is moved, we cancel the timer. We are no longer hovering
            _hoverTimer.Enabled = false;

            // here we simply set the curso x and y position. We cannot get these from the application directly, and we need
            // them in the idle event to see where the cursor is
            _cursorX = Convert.ToInt32(x);
            _cursorY = Convert.ToInt32(y);
        }

        /// <summary>
        ///     Handles the visio idle.
        /// </summary>
        /// <param name="app">The application.</param>
        private void HandleVisioIdle(Application app)
        {
            return; // TODO, this is causing issues...
            // Visio is Idle - Mouse is not moving
            // If the cursor is over a modeller shape that has errors,
            // then we start the timer

            // Visio doesnt expose its hover events or mouse over shape events
            // so we need to figure out:
            // 1. Are we over a shape
            // 2. Is the shape a modeller Shape
            // 3. Is the shape in Error status.
            // If all 3 conditions are satisfied, we must enable the hover functionality
            // Also.
            // if the hover Timer is already enabled or hover timer has been triggered, we just exit (Idle keeps getting re-triggered)
            // Remember - when the hover Timer Event is triggered, the Timer will be disabled again, so we need the _HoverTimerTriggered Flag
            if (_hoverTimer.Enabled || _hoverTimerTriggered)
            {
                return;
            }

            // to avoid doing this lookup a lot, we first check the conditions above
            _hoverShape = GetErrorShapeUnderCursor();
            if (_hoverShape == null)
            {
                return;
            }

            _hoverTimer.Enabled = true;
        }

        #endregion
    }
}