﻿using System;
using Microsoft.Office.Interop.Visio;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using NHibernate.Util;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	/// The Visio 2016 implementation of a modeller connector
	/// </summary>
	internal class ModellerVisio2016Connector : IModellerConnector
	{
		#region Static Fields and Constants

		/// <summary>
		/// The cell in which the connection id is stored
		/// </summary>
		private const string C_CONNECTION_ID_CELL_NAME = "User.ConnectionOccurrenceId";

		/// <summary>
		/// The cell in which the model element id is stored
		/// </summary>
		private const string C_MODEL_ELEMENT_ID_CELL_NAME = "User.ModelElementId";

		/// <summary>
		/// Arrow cell names
		/// </summary>
		private const string C_END_ARROW_CELL_NAME = "EndArrow";
		private const string C_BEGIN_ARROW_CELL_NAME = "BeginArrow";

		/// <summary>
		/// Dynamic connectors are shapes in of themsslves. The way to differentiate is shape.LineStyle == C_CONNECTOR_SHAPE_LINESTYLE
		/// </summary>
		public const string C_CONNECTOR_SHAPE_LINESTYLE = "Connector";

		#endregion

		#region  Fields/Properties

		/// <summary>
		/// The conenctor shape
		/// </summary>
		private readonly Shape _connector;

		/// <summary>
		/// The modeller presenter
		/// </summary>
		private readonly IModellerPresenter _presenter;

		/// <summary>
		/// Wheather or not the connector is reversed
		/// </summary>
		private readonly bool _isReversed;

		/// <summary>
		/// The connection occurrence ID
		/// </summary>
		public Guid? ConnectionOccurrenceId
		{
			get => string.IsNullOrEmpty(_connector.GetOrCreateUserCell(C_CONNECTION_ID_CELL_NAME).Formula) ? (Guid?)null : Guid.Parse(_connector.GetOrCreateUserCell(C_CONNECTION_ID_CELL_NAME).FormulaU.Replace("\"", ""));
			set => _connector.GetOrCreateUserCell(C_CONNECTION_ID_CELL_NAME).Formula = "\"" + value + "\"";
		}

		/// <summary>
		/// The connection occurrence ID
		/// </summary>
		public Guid? ModelElementId
		{
			get => string.IsNullOrEmpty(_connector.GetOrCreateUserCell(C_MODEL_ELEMENT_ID_CELL_NAME).Formula) ? (Guid?)null : Guid.Parse(_connector.GetOrCreateUserCell(C_MODEL_ELEMENT_ID_CELL_NAME).FormulaU.Replace("\"", ""));
			set => _connector.GetOrCreateUserCell(C_MODEL_ELEMENT_ID_CELL_NAME).Formula = "\"" + value + "\"";
		}

		/// <summary>
		/// The page the connection is on
		/// </summary>
		public IModellerPage ModellerPage => new ModellerVisio2016Page(_connector.ContainingPage, _presenter);

		/// <summary>
		/// The direction of this connection
		/// </summary>
		public ConnectionDirection ConnectionDirection
		{
			set
			{
				_connector.Cells[C_BEGIN_ARROW_CELL_NAME].Formula = "0";
				_connector.Cells[C_END_ARROW_CELL_NAME].Formula = "0";
				if ((value == ConnectionDirection.Forward) || (value == ConnectionDirection.Reverse && _isReversed))
				{
					_connector.Cells[C_BEGIN_ARROW_CELL_NAME].Formula = "1";
				}
				if ((value == ConnectionDirection.Reverse && !_isReversed) || (value == ConnectionDirection.Forward && _isReversed))
				{
					_connector.Cells[C_END_ARROW_CELL_NAME].Formula = "1";
				}
			}
		}

		/// <summary>
		/// The shape this connection is from
		/// </summary>
		public IModellerShape ShapeFrom { get; }

		/// <summary>
		/// The shape this connection is to
		/// </summary>
		public IModellerShape ShapeTo { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs a new connector
		/// </summary>
		/// <param name="connectShape">The connect shape to consturct from</param>
		/// <param name="presenter">The modeller presenter</param>
		public ModellerVisio2016Connector(Shape connectShape, IModellerPresenter presenter)
		{
			_connector = ArgumentValidation.AssertNotNull(connectShape, nameof(connectShape));
			if (connectShape.LineStyle != C_CONNECTOR_SHAPE_LINESTYLE)
			{
				throw new Exception("Given shape is not a valid connector");
			}
			_presenter = presenter;
			Connect connect = connectShape.Connects.Item16[1];
			// There are two shapes in a connect, one will be the 1D connector and the other will therefor be a shape
			if (connect.FromSheet.OneD == -1)
			{
				ShapeTo = new Visio2016ModellerShape(connect.ToSheet, _presenter);
				ShapeFrom = GetOtherConnectedShape(_connector, connect.ToSheet);
			}
			else
			{
				ShapeFrom = new Visio2016ModellerShape(connect.FromSheet, _presenter);
				_isReversed = true;
				ShapeTo = GetOtherConnectedShape(_connector, connect.FromSheet);
			}
		}

		#endregion

		#region Other Methods
		/// <summary>
		/// For a given connector it will find the shapa at the other end
		/// </summary>
		/// <param name="connector">The connector shape</param>
		/// <param name="otherShape">The shape that we already know is connected</param>
		/// <returns></returns>
		private Visio2016ModellerShape GetOtherConnectedShape(Shape connector, Shape otherShape)
		{
			foreach (Connect c in connector.Connects)
			{
				if (c.FromSheet != connector && c.FromSheet != otherShape)
				{
					return new Visio2016ModellerShape(c.FromSheet, _presenter);
				}
				if (c.ToSheet != connector && c.ToSheet != otherShape)
				{
					return new Visio2016ModellerShape(c.ToSheet, _presenter);
				}
			}
			return null;
		}

		#endregion
	}
}