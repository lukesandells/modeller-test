﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Microsoft.Office.Interop.Visio;
using MI.Framework.Validation;
using MI.Modeller.Client.MasterShapes;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.Visio2016.Forms;
using MI.Modeller.Client.Visio2016.Resources;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Client.Visio2016
{
    /// <summary>
    ///     Class Visio2016ModellerShape.
    /// </summary>
    /// <seealso>
    ///     <cref>"MI.Modeller.Client.IModellerShape"</cref>
    /// </seealso>
    public class Visio2016ModellerShape : IModellerShape
    {
        #region Static Fields and Constants

        private const string C_DESCRIPTION_TEXT_NAME = "DescriptionText";
	    private const string C_EQUIPMENT_LEVEL_CELL_NAME = "User.EquipmentLevel";
	    private const string C_WORK_TYPE_CELL_NAME = "User.WorkType";
		private const string C_ERROR_MESSAGE_CELL_NAME = "User.ErrorMessage";
	    private const string C_LOCK_TEXT_CELL_NAME = "LockTextEdit";
		/// <summary>
		///     The c enterprise hierarchy scope cell name
		/// </summary>
		private const string C_ENTERPRISE_HIERARCHY_SCOPE_CELL_NAME = "User.EnterpriseId";

        private const string C_HAS_ERRORS_CELL_NAME = "User.HasErrors";

        /// <summary>
        ///     The c hierarchy scope cell name
        /// </summary>
        private const string C_HIERARCHY_SCOPE_CELL_NAME = "User.HieararchyScopeId";

        private const string C_ID_TEXT_NAME = "IdText";

        /// <summary>
        ///     The c internal identifier cell name
        /// </summary>
        private const string C_INTERNAL_ID_CELL_NAME = "User.InternalId";

        private const string C_IS_ORPHAN_CELL_NAME = "User.IsOrphan";

	    private const string C_OCCURRENCE_ID_CELLNAME = "User.OccurrenceId";

	    private const string C_MODEL_ELEMENT_ID_CELLNAME = "User.ModelElementId";

		#endregion

		#region  Fields

		/// <summary>
		///     The shape
		/// </summary>
		private readonly Shape _shape;

        private readonly IModellerPresenter _modellerPresenter;

        public string ClassText
        {
            get
            {
                return FindInnerShapeById("ClassText",
                        _shape.Shapes)
                    .Text;
            }
            set
            {
                var classShape = FindInnerShapeById("ClassText",
		                _shape.Shapes);
	            if (!string.Equals(classShape.Text,value))
	            {
		            classShape.Cells[C_LOCK_TEXT_CELL_NAME].Formula = "0";
		            classShape.Text = value;
		            classShape.Cells[C_LOCK_TEXT_CELL_NAME].Formula = "1";
	            }
            }
        }

        public string DescriptionText
        {
            get => FindInnerShapeById(C_DESCRIPTION_TEXT_NAME,
		            _shape.Shapes)
	            .Text;
	        set
            {
	            var descriptionShape = FindInnerShapeById(C_DESCRIPTION_TEXT_NAME, _shape.Shapes);
				if (!string.Equals(value, descriptionShape.Text))
	            {
		            descriptionShape.Text = value;
	            }
            }
        }

        /// <summary>
        ///     The EquipmentId of the Shape's Enterprise Hierarchy Scope
        /// </summary>
        /// <value>The enterprise hierarchy scope identifier.</value>
        public string EnterpriseHierarchyScopeId
        {
            get { return CellValue(C_ENTERPRISE_HIERARCHY_SCOPE_CELL_NAME); }
            set
            {
                SetCellValue(C_ENTERPRISE_HIERARCHY_SCOPE_CELL_NAME,
                    value);
            }
        }

        public string EquipmentLevelText
        {
            get
            {
                return FindInnerShapeById("ObjectTypeText",
                        _shape.Shapes)
                    .Text;
            }
            set
            {
	            var objectTypeShape = FindInnerShapeById("ObjectTypeText",
		            _shape.Shapes);
	            if (!string.Equals(objectTypeShape.Text, value))
	            {
		            objectTypeShape.Cells[C_LOCK_TEXT_CELL_NAME].Formula = "0";
		            objectTypeShape.Text = value;
		            objectTypeShape.Cells[C_LOCK_TEXT_CELL_NAME].Formula = "1";
	            }
            }
        }

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is an orphan.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is orphan - I.e. there is no matching workspace / domain object for the shape ;
        ///     otherwise, <c>false</c>.
        /// </value>
        public bool HasErrors
        {
            get
            {
                return CellValue(C_HAS_ERRORS_CELL_NAME)
                    .Equals(true.ToString()
                        .ToUpper());
            }
            set
            {
                SetCellValue(C_HAS_ERRORS_CELL_NAME,
                    value
                        ? true.ToString()
                            .ToUpper()
                        : string.Empty);
            }
        }

        /// <summary>
        ///     The EquipmentId of the Shape's Hierarchy Scope
        /// </summary>
        /// <value>The hierarchy scope identifier.</value>
        public string HierarchyScopeId
        {
            get { return CellValue(C_HIERARCHY_SCOPE_CELL_NAME); }
            set
            {
                SetCellValue(C_HIERARCHY_SCOPE_CELL_NAME,
                    value);
            }
        }

        public string IdText
        {
            get
            {
                return FindInnerShapeById(C_ID_TEXT_NAME,
                        _shape.Shapes)
                    .Text;
            }
            set
            {
	            var idShape = FindInnerShapeById(C_ID_TEXT_NAME, _shape.Shapes);
	            if (!string.Equals(value, idShape.Text))
	            {
		            idShape.Text = value;
	            }
            }
        }

        public bool IsDeleted => _shape.Stat == (short) VisStatCodes.visStatDeleted;

        /// <summary>
        ///     Gets or sets a value indicating whether this instance is an orphan.
        /// </summary>
        /// <value>
        ///     <c>true</c> if this instance is orphan - I.e. there is no matching workspace / domain object for the shape ;
        ///     otherwise, <c>false</c>.
        /// </value>
        /// TODO - This needs to go away and be absorbed into the Has Errors, along with a mechanism for storing all validation errors against the shape
        public bool IsOrphan
        {
            get
            {
                return CellValue(C_IS_ORPHAN_CELL_NAME)
                    .Equals(true.ToString()
                        .ToUpper());
            }
            set
            {
                SetCellValue(C_IS_ORPHAN_CELL_NAME,
                    value
                        ? true.ToString()
                            .ToUpper()
                        : string.Empty);
            }
        }

        /// <summary>
        ///     Gets the name of the master.
        /// </summary>
        /// <value>The name of the master.</value>
        public string MasterName => _shape.Master?.Name.Split('.')[0];

		/// <summary>
		/// The underlying occurrence ID for this shape
		/// </summary>
	    public Guid? OccurrenceId {
			get => string.IsNullOrEmpty(_shape.GetOrCreateUserCell(C_OCCURRENCE_ID_CELLNAME).Formula)
				? (Guid?) null
				: Guid.Parse(_shape.GetOrCreateUserCell(C_OCCURRENCE_ID_CELLNAME).FormulaU.Replace("\"", ""));
			set => _shape.GetOrCreateUserCell(C_OCCURRENCE_ID_CELLNAME)
				.Formula = "\"" + value + "\"";
		}

	    /// <summary>
	    /// The underlying ID of the model element this shape represents
	    /// </summary>
	    public Guid? ModelElementId
	    {
		    get => string.IsNullOrEmpty(_shape.GetOrCreateUserCell(C_MODEL_ELEMENT_ID_CELLNAME).Formula)
			    ? (Guid?)null
			    : Guid.Parse(_shape.GetOrCreateUserCell(C_MODEL_ELEMENT_ID_CELLNAME).FormulaU.Replace("\"", ""));
		    set => _shape.GetOrCreateUserCell(C_MODEL_ELEMENT_ID_CELLNAME)
			    .Formula = "\"" + value + "\"";
	    }

	    /// <summary>
	    /// Allows for a shape to display an error message
	    /// </summary>
	    public string ErrorMessage
	    {
		    set => _shape.GetOrCreateUserCell(C_ERROR_MESSAGE_CELL_NAME)
			    .Formula = "\"" + value + "\"";
	    }

		#endregion

		#region Constructors

		/// <summary>
		///     Initializes a new instance of the <see cref="Visio2016ModellerShape" /> class.
		///     This should only be called from the VisioShapeFactory to ensure that we end up with a single list of these objects
		/// </summary>
		/// <param name="shape">The shape.</param>
		/// <param name="modellerPresenter">The modeller presenter to interact with</param>
		public Visio2016ModellerShape(Shape shape,
            IModellerPresenter modellerPresenter)
        {
            _shape = shape;
            _modellerPresenter = modellerPresenter;
        }

        #endregion

        #region  Interface Implementations

        /// <summary>
        ///     Asserts the is of master.
        /// </summary>
        /// <param name="master">The master.</param>
        /// <exception cref="ArgumentException">Shape does not match master of type " + master.Name</exception>
        public void AssertIsOfMaster(MasterShape master)
        {
            if (!IsOfMaster(master))
            {
                throw new ArgumentException("Shape does not match master of type " + master.Name);
            }
        }

        /// <summary>
        ///     Delete's the underlying shape
        /// </summary>
        public void Delete()
        {
	        if (!IsDeleted)
	        {
		        try
		        {
			        _shape.Delete();
		        }
		        catch (Exception)
		        {
			        // ignored
		        }
	        }
        }

        /// <summary>
        ///     Firsts the level children.
        /// </summary>
        /// <param name="master">The master.</param>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MI.Modeller.Client.IModellerShape&gt;.</returns>
        public IEnumerable<IModellerShape> FirstLevelChildren(MasterShape master)

        {
            ArgumentValidation.AssertNotNull(master,
                nameof(master));

            //The first level shape will be the shape that matches the target master and is contained within the least number of shapes
            var candidateList =
            (from Shape candidate in _shape.SpatialNeighbors[(short) VisSpatialRelationCodes.visSpatialContain,
                    0,
                    0]
                where IsOfMaster(candidate,
                    master)
                select new Tuple<int, Shape>(_shape.SpatialNeighbors[(short) VisSpatialRelationCodes.visSpatialContain,
                        0,
                        0].Count,
                    candidate)).ToArray();

            if (candidateList.Length > 0)
            {
                var targetCount = candidateList.Min(c => c.Item1);

                return candidateList.Where(c => c.Item1 == targetCount)
                    .Select(c => ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(c.Item2,
                        _modellerPresenter))
                    .ToArray();
            }
            return new HashSet<IModellerShape>();
        }

        /// <summary>
        ///     Sets the cell value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="formula">The formula.</param>
        /// <exception cref="Exception"></exception>
        public void SetCellValue(string key,
            string formula)
        {
            var regex = new Regex("^[0-9]+$",
                RegexOptions.Compiled);

            if (_shape.CellExists[key,
                    0] != 0)
            {
                // if the string is a number we set, else we need to parse this into the formula
                if (formula != null && regex.IsMatch(formula))
                {
                    _shape.CellsU[key].Formula = formula;
                }
                else
                {
                    string formulaValue = $"=\"{formula?.Replace("\"", "\"\"")}\"";
                    _shape.CellsU[key].Formula = formulaValue;
                }
            }
            else
            {
                throw new Exception(string.Format(Messages.ShapeCellDoesNotExist,
                    key));
            }
        }

        /// <summary>
        ///     Sets the protected shape data value.
        ///     This is for setting Shape Data that is of type Fixed List, with a single fixed Value
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="formula">The formula.</param>
        public void SetProtectedShapeDataValue(string key,
            string formula)
        {
            if (_shape.CellExists[key,
                    0] != 0 && _shape.CellsSRCExists[(short) VisSectionIndices.visSectionProp,
                    _shape.Cells[key].Row,
                    (short) VisCellIndices.visCustPropsValue,
                    0] != 0)
            {
                var formatCell = _shape.CellsSRC[(short) VisSectionIndices.visSectionProp,
                    _shape.Cells[key].Row,
                    (short) VisCellIndices.visCustPropsFormat];
                string formulaValue = $"=\"{formula.Replace("\"", "\"\"")}\"";
                formatCell.FormulaU = formulaValue;
            }
        }

	    public string EquipmentLevel
	    {
		    set
		    {
			    if (value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.ProcessCell.ToString()
					|| value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.ProductionUnit.ToString()
					|| value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.ProductionLine.ToString() 
					|| value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.StorageZone.ToString())
			    {
				    value = OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.WorkCenter.ToString();
			    }
			    if (value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.Unit.ToString()
					|| value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.WorkCell.ToString()
					|| value == OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.StorageUnit.ToString())
			    {
				    value = OperationsManagement.Domain.MasterData.Design.Resources.EquipmentLevel.WorkUnit.ToString();
			    }
				_shape.CellsU[C_EQUIPMENT_LEVEL_CELL_NAME].Formula = $"=\"{value}\"";
			}
	    }

	    public string ProcessType
	    {
		    set => _shape.CellsU[C_WORK_TYPE_CELL_NAME].Formula = $"=\"{value}\"";
	    }

		public IModellerPage Page => new ModellerVisio2016Page(_shape.ContainingPage, _modellerPresenter);

		public Shape UnderlyingShape => _shape;

	    /// <summary>
        ///     Ases the data object.
        /// </summary>
        /// <returns>System.Windows.Forms.DataObject.</returns>
        public IModellerDataObject AsDataObject()
        {
            return new ModellerDataObject(_shape);
        }

        /// <summary>
        ///     Firsts the level parent.
        /// </summary>
        /// <param name="master">The master.</param>
        /// <returns>MI.Modeller.Client.IModellerShape.</returns>
        /// <exception cref="Exception">Shape contains no clear first level shape</exception>
        public IModellerShape FirstLevelParent(MasterShape master)

        {
            ArgumentValidation.AssertNotNull(master,
                nameof(master));

			var target = _shape.ContainingShapes()
				.Where(s => IsOfMaster(s, master))
				// The top level will be the one contained within the most other shapes
				.OrderByDescending(s => s.ContainingShapes().Count(c => IsOfMaster(c, master)))
				.FirstOrDefault();

            return target == null
                ? null
                : ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(target,
                    _modellerPresenter);
        }


	    /// <summary>
	    ///     Firsts the level parent.
	    /// </summary>
	    /// <param name="masterName">The master's name.</param>
	    /// <returns>MI.Modeller.Client.IModellerShape.</returns>
	    /// <exception cref="Exception">Shape contains no clear first level shape</exception>
	    public IModellerShape FirstLevelParent(string masterName)

	    {
		    ArgumentValidation.AssertNotNullOrEmpty(masterName,
			    nameof(masterName));

		    var target = _shape.ContainingShapes()
			    .Where(s => IsOfMaster(s, masterName))
			    // The top level will be the one contained within the most other shapes
			    .OrderByDescending(s => s.ContainingShapes().Count(c => IsOfMaster(c, masterName)))
			    .FirstOrDefault();

		    return target == null
			    ? null
			    : ((VisioShapeFactory)_modellerPresenter.ShapeFactory).ConstructShape(target,
				    _modellerPresenter);
	    }

		/// <summary>
		///     Gets the connected connectors.
		/// </summary>
		/// <returns>System.Collections.Generic.IEnumerable&lt;MI.Modeller.Client.IModellerShape&gt;.</returns>
		public IEnumerable<IModellerShape> GetConnectedConnectors()
        {
            //// The nature of connectors is quite convoluted
            //// http://stackoverflow.com/questions/6456831/shape-connectors-in-visio
            //// So we really need a connector to be connected on both ends else we just cannot tell if the shape it 
            //// is connected to is at the tip or tail of the connector

            foreach (Connect connect in _shape.Connects)
            {
                if (connect?.ToSheet != null && !ReferenceEquals(connect.ToSheet,
                        _shape) && string.Equals(ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE,
                        connect.ToSheet.LineStyle))
                {
                    yield return ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(connect.ToSheet,
                        _modellerPresenter);
                }
            }

            foreach (Connect connect in _shape.FromConnects)
            {
                if (connect?.FromSheet != null && !ReferenceEquals(connect.FromSheet,
                        _shape) && string.Equals(ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE,
                        connect.FromSheet.LineStyle))
                {
                    yield return ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(connect.FromSheet,
                        _modellerPresenter);
                }
            }
        }

        /// <summary>
        ///     Gets the connected shapes.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MI.Modeller.Client.IModellerShape&gt;.</returns>
        public IEnumerable<IModellerShape> GetConnectedShapes()
        {
            // The nature of connectors is quite convoluted
            // http://stackoverflow.com/questions/6456831/_shape-connectors-in-visio
            // So we really need a connector to be connected on both ends else we just cannot tell if the _shape it 
            // is connected to is at the tip or tail of the connector
            if (_shape.LineStyle == ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE)
            {
                foreach (Connect connect in _shape.Connects)
                {
                    if (connect?.ToSheet != null && !ReferenceEquals(connect.ToSheet,
                            _shape))
                    {
                        yield return
                            ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(
                                connect.ToSheet.RootShape,
                                _modellerPresenter);
                    }
                }

                foreach (Connect connect in _shape.FromConnects)
                {
                    if (connect?.FromSheet != null && !ReferenceEquals(connect.FromSheet,
                            _shape))
                    {
                        yield return
                            ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(
                                connect.FromSheet.RootShape,
                                _modellerPresenter);
                    }
                }
            }
            // this is not a connector - we do a different lookup to find the connected _shapes
            else
            {
                foreach (
                    int connectedShape in _shape.ConnectedShapes(VisConnectedShapesFlags.visConnectedShapesAllNodes,
                        string.Empty))
                {
                    yield return
                        ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(
                            Globals.ModellerAddin.Application.ActivePage.Shapes.ItemFromID[connectedShape],
                            _modellerPresenter);
                }
            }
        }

        /// <summary>
        ///     Determines whether [is of master] [the specified master].
        /// </summary>
        /// <param name="master">The master.</param>
        /// <returns>System.Boolean.</returns>
        public bool IsOfMaster(MasterShape master)
        {
            return IsOfMaster(_shape,
                master);
        }

        /// <summary>
        ///     Tops the level parent.
        /// </summary>
        /// <param name="master">The master.</param>
        /// <returns>MI.Modeller.Client.IModellerShape.</returns>
        /// <exception cref="Exception">Shape contains no clear Top level _shape</exception>
        public IModellerShape TopLevelParent(MasterShape master)
        {
            ArgumentValidation.AssertNotNull(_shape,
                nameof(_shape));
            ArgumentValidation.AssertNotNull(master,
                nameof(master));

            //The Top level _shape will be the _shape that matches the target master and is contained within the LEAST number of _shapes
            var candidateList =
            (from Shape candidate in _shape.SpatialNeighbors[(short) VisSpatialRelationCodes.visSpatialContainedIn,
                    0,
                    0]
                where IsOfMaster(candidate,
                    master)
                select
                new Tuple<int, Shape>(candidate.SpatialNeighbors[(short) VisSpatialRelationCodes.visSpatialContainedIn,
                        0,
                        0].Count,
                    candidate)).ToArray();

            if (candidateList.Length <= 0)
            {
                return null;
            }
            var targetCount = candidateList.Min(c => c.Item1);

            if (candidateList.Count(c => c.Item1 == targetCount) > 1)
            {
                throw new Exception("Shape contains no clear Top level _shape");
            }

            return candidateList.Any(c => c.Item1 == targetCount)
                ? ((VisioShapeFactory) _modellerPresenter.ShapeFactory).ConstructShape(
                    candidateList.SingleOrDefault(c => c.Item1 == targetCount)
                        ?.Item2,
                    _modellerPresenter)
                : null;
        }

		/// <summary>
		/// Selects the shape
		/// </summary>
	    public void Select()
	    {
		    _shape.Application.ActiveWindow.Selection.Select(_shape, (short)VisSelectArgs.visSelect);
	    }

	    /// <summary>
	    ///     Create a connection to another shape
	    /// </summary>
	    /// <param name="shapeTo"></param>
	    /// <param name="connectionDirection"></param>
	    public void ConnectTo(IModellerShape shapeTo, ConnectionDirection connectionDirection)
	    {
			// Keep direction as none. If you set a direction it auto layouts the shapes which causes them to move around
		    _shape.AutoConnect(((Visio2016ModellerShape) shapeTo)._shape, VisAutoConnectDir.visAutoConnectDirNone);
	    }

        /// <summary>
        ///     Cells the value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>System.String.</returns>
        public string CellValue(string key)
        {
            if (_shape.CellExists[key,
                    0] == 0)
            {
                return string.Empty;
            }

            var cell = _shape.Cells[key];

            return string.IsNullOrEmpty(cell.Formula)
                ? string.Empty
                : cell.FormulaU.Trim('"')
                    .Replace(@"""""",
                        @"""");
        }

        /// <summary>
        ///     Determines whether [is modeller shape].
        /// </summary>
        /// <returns><c>true</c> if [is modeller shape]; otherwise, <c>false</c>.</returns>
        public bool IsModellerShape()
        {
            // ALL shapes must reference an object, and must have this property
			// In VBA -1 = true, 0 = false
            return _shape.Stat == (short) VisStatCodes.visStatNormal 
				&& _shape.CellExists[C_OCCURRENCE_ID_CELLNAME,
                       0] == -1;
        }

        /// <summary>
        ///     Returns the Bounds of the shape as a Rect Object
        /// </summary>
        /// <returns>Rect.</returns>
        public Rectangle Bounds()
        {
            double left;
            double width;
            double top;
            double hieght;

            _shape.BoundingBox((short) VisBoundingBoxArgs.visBBoxExtents,
                out left,
                out hieght,
                out width,
                out top);

            // x, y, width, hieght
            return new Rectangle(Convert.ToInt32(left),
                Convert.ToInt32(top),
                Convert.ToInt32(width),
                Convert.ToInt32(hieght));
        }

        #endregion

        #region Members

        private Shape FindInnerShapeById(string id,
            Shapes outterShapes)
        {
            foreach (Shape shape in outterShapes)
            {
                if (id.Equals(shape.Name))
                {
                    return shape;
                }
                var innerShape = FindInnerShapeById(id,
                    shape.Shapes);
                if (innerShape != null)
                {
                    return innerShape;
                }
            }
            return null;
        }


		/// <summary>
		///     Determines whether [is of master] [the specified s].
		/// </summary>
		/// <param name="s">The s.</param>
		/// <param name="master">The master.</param>
		/// <returns><c>true</c> if [is of master] [the specified s]; otherwise, <c>false</c>.</returns>
		private static bool IsOfMaster(Shape s,
			MasterShape master)
		{
			return IsOfMaster(s, master.Name);
		}

		/// <summary>
		///     Determines whether [is of master] [the specified s].
		/// </summary>
		/// <param name="s">The s.</param>
		/// <param name="masterName">The master.</param>
		/// <returns><c>true</c> if [is of master] [the specified s]; otherwise, <c>false</c>.</returns>
		private static bool IsOfMaster(Shape s,
		    string masterName)
	    {
		    return masterName != null && (s.Master?.Name.StartsWith(masterName) ?? false);
	    }

		#endregion
	}
}