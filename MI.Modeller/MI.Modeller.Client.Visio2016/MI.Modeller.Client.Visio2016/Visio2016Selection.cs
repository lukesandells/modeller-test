﻿using MI.Framework;
using System.Collections;
using System.Collections.Generic;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	/// The visio 2016 implementation of a modeller selection
	/// </summary>
	internal class Visio2016Selection : IModellerSelection
	{
		/// <summary>
		/// The shapes this selection is for
		/// </summary>
		private readonly IEnumerable<IModellerShape> _shapes;

		/// <summary>
		/// Initialises a new Visio2016Selection
		/// </summary>
		/// <param name="shapes">The shapes in this selection</param>
		public Visio2016Selection(IEnumerable<IModellerShape> shapes)
		{
			_shapes = shapes;
		}

		/// <summary>
		/// Reselect all shapes in this selection
		/// </summary>
		public void ReselectAll()
		{
			Globals.ModellerAddin.Application.ActiveWindow.DeselectAll();
			_shapes.ForEach(s => Globals.ModellerAddin.Application.ActiveWindow.Select(((Visio2016ModellerShape)s).UnderlyingShape, (short)VisSelectArgs.visSelect));
		}

		/// <summary>
		/// Select all shapes in this selection
		/// </summary>
		public void SelectAll()
		{
			_shapes.ForEach(s => Globals.ModellerAddin.Application.ActiveWindow.Select(((Visio2016ModellerShape)s).UnderlyingShape, (short)VisSelectArgs.visSelect));
		}

		/// <summary>
		/// Deseselect all shapes in this selection
		/// </summary>
		public void DeselectAll()
		{
			_shapes.ForEach(s => Globals.ModellerAddin.Application.ActiveWindow.Select(((Visio2016ModellerShape)s).UnderlyingShape, (short)VisSelectArgs.visDeselect));
		}

		/// <summary>
		/// Gets the enumarator for the shapes in this selection
		/// </summary>
		public IEnumerator<IModellerShape> GetEnumerator()
		{
			return _shapes.GetEnumerator();
		}

		/// <summary>
		/// Gets the enumarator for the shapes in this selection
		/// </summary>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return _shapes.GetEnumerator();
		}

		/// <summary>
		/// Returns the selection as a data object
		/// </summary>
		/// <returns></returns>
		public IModellerDataObject AsDataObject()
		{
			// We need to reselect all shapes to ensure that the active selection holds the intended shapes
			ReselectAll();
			return new ModellerDataObject(Globals.ModellerAddin.Application.ActiveWindow.Selection);
		}

		/// <summary>
		/// Arrange the shapes
		/// </summary>
		public void AutoArrange()
		{
			// We need to reselect all shapes to ensure that the active selection holds the intended shapes
			ReselectAll();
			Globals.ModellerAddin.Application.ActiveWindow.Selection.Layout();
		}
	}
}