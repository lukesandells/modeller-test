﻿namespace MI.Modeller.Client.Visio2016
{
    /// <summary>
    /// The Modeller Office Ribbon
    /// </summary>
    partial class ModellerRibbonComponent : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public ModellerRibbonComponent()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.ModellerTab = this.Factory.CreateRibbonTab();
			this.Group1 = this.Factory.CreateRibbonGroup();
			this.PanelToggleButton = this.Factory.CreateRibbonButton();
			this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
			this.ModellerTab.SuspendLayout();
			this.Group1.SuspendLayout();
			this.SuspendLayout();
			// 
			// ModellerTab
			// 
			this.ModellerTab.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
			this.ModellerTab.ControlId.OfficeId = "TabHome";
			this.ModellerTab.Groups.Add(this.Group1);
			this.ModellerTab.Label = "TabHome";
			this.ModellerTab.Name = "ModellerTab";
			// 
			// Group1
			// 
			this.Group1.Items.Add(this.PanelToggleButton);
			this.Group1.Label = "MI Modeller";
			this.Group1.Name = "Group1";
			// 
			// PanelToggleButton
			// 
			this.PanelToggleButton.ControlSize = Microsoft.Office.Core.RibbonControlSize.RibbonControlSizeLarge;
			this.PanelToggleButton.Image = global::MI.Modeller.Client.Visio2016.Properties.Resources.TogglePanel;
			this.PanelToggleButton.Label = "Object Browser";
			this.PanelToggleButton.Name = "PanelToggleButton";
			this.PanelToggleButton.ShowImage = true;
			this.PanelToggleButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.PanelToggleButton_Click);
			// 
			// ModellerRibbonComponent
			// 
			this.Name = "ModellerRibbonComponent";
			this.RibbonType = "Microsoft.Visio.Drawing";
			this.Tabs.Add(this.ModellerTab);
			this.ModellerTab.ResumeLayout(false);
			this.ModellerTab.PerformLayout();
			this.Group1.ResumeLayout(false);
			this.Group1.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab ModellerTab;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup Group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton PanelToggleButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }

    partial class ThisRibbonCollection
    {
        internal ModellerRibbonComponent Ribbon
        {
            get { return this.GetRibbon<ModellerRibbonComponent>(); }
        }
    }
}
