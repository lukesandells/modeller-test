using System;
using System.Windows.Forms;

namespace MI.Modeller.Client.Forms
{
    /// <summary>
    ///     A simple form for displaying a simple input
    /// </summary>
    public partial class SimpleInputForm : Telerik.WinControls.UI.RadForm
    {
        #region  Fields

        public string Result => TextBox.Text;

        /// <summary>
        ///     Sealed text to avoid virtual call in constructor
        /// </summary>
        public sealed override string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        #endregion

        #region Constructors

        //public new ModellerDialogResult DialogResult;
        public SimpleInputForm(string title,
            string label)
        {
            InitializeComponent();
            Text = title;
            Label.Text = label;
        }

        #endregion

        #region Members

        private void CancelButtonClick(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TextBox.Text))
            {
                MessageBox.Show("Please enter a value for " + Label.Text);
            }
            else
            {
                DialogResult = DialogResult.OK;
                Close();
            }
        }

        #endregion
    }
}