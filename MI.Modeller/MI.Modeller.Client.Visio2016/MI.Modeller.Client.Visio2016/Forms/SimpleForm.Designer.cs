using MI.Modeller.Client.Visio2016.Resources;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.Forms
{
    partial class SimpleForm: RadForm
	{
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKButton = new Telerik.WinControls.UI.RadButton();
            this.CancelButton = new Telerik.WinControls.UI.RadButton();
            this.ButtonPanel = new System.Windows.Forms.FlowLayoutPanel();
            this.ContentPanel = new System.Windows.Forms.FlowLayoutPanel();
	        this.office2013LightTheme1 = new Telerik.WinControls.Themes.Office2013LightTheme();
			base.ThemeName = "Office2013Light";
			this.ButtonPanel.SuspendLayout();
            this.SuspendLayout();
			// 
			// OKButton
			// 
			this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.OKButton.Location = new System.Drawing.Point(113, 247);
			this.OKButton.Name = "OkButton";
			this.OKButton.Size = new System.Drawing.Size(75, 23);
			this.OKButton.TabIndex = 1;
			this.OKButton.Text = UIMessages.OKText;
			this.OKButton.Click += this.OKButton_Click;
	        // 
	        // CancelButton
	        // 
	        this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
	        this.CancelButton.Location = new System.Drawing.Point(194, 247);
	        this.CancelButton.Name = "CancelButton";
	        this.CancelButton.Size = new System.Drawing.Size(75, 23);
	        this.CancelButton.TabIndex = 1;
	        this.CancelButton.Text = UIMessages.Cancel_text;
			// 
			// ButtonPanel
			// 
			this.ButtonPanel.AutoSize = true;
            this.ButtonPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ButtonPanel.CausesValidation = false;
            this.ButtonPanel.Controls.Add(this.CancelButton);
            this.ButtonPanel.Controls.Add(this.OKButton);
            this.ButtonPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.ButtonPanel.Location = new System.Drawing.Point(0, 553);
            this.ButtonPanel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ButtonPanel.Name = "ButtonPanel";
            this.ButtonPanel.Size = new System.Drawing.Size(1157, 69);
            this.ButtonPanel.TabIndex = 6;
            // 
            // ContentPanel
            // 
            this.ContentPanel.AutoSize = true;
			this.ContentPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ContentPanel.Location = new System.Drawing.Point(0, 0);
            this.ContentPanel.Margin = new System.Windows.Forms.Padding(8, 7, 8, 7);
            this.ContentPanel.Name = "ContentPanel";
            this.ContentPanel.Size = new System.Drawing.Size(1157, 553);
            this.ContentPanel.TabIndex = 7;
            // 
            // SimpleForm
            // 
            this.AcceptButton = this.OKButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1157, 622);
            this.Controls.Add(this.ContentPanel);
            this.Controls.Add(this.ButtonPanel);
            this.Name = "SimpleForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Title";
            this.ButtonPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        #endregion
        private Telerik.WinControls.UI.RadButton OKButton;
        private new Telerik.WinControls.UI.RadButton CancelButton;
        private System.Windows.Forms.FlowLayoutPanel ButtonPanel;
        private System.Windows.Forms.FlowLayoutPanel ContentPanel;
		private Telerik.WinControls.Themes.Office2013LightTheme office2013LightTheme1;
	}
}