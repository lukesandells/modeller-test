﻿// ***********************************************************************
//  Solution    : MI.Modeller
//  FileName    : NewHierachyForm.cs 
//  Created     : 2016/09/29
//  Modified    : 2016/09/29
// ***********************************************************************

using System;
using System.Windows.Forms;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Client.Forms
{
    public partial class NewHierachyForm : Form,
                                           INewEnterpriseView
    {
        #region  Fields
        /// <summary>
        /// The presenter controlling this view
        /// </summary> 
        private readonly NewHierarchyPresenter _presenter;

        /// <summary>
        /// The Equipment ID being presented in this view
        /// </summary>
        public string EquipmentId
        {
            get { return EquipmentIdTextBox.Text; }
        }

        public string FileName
        {
            get { return FilenameTextBox.Text; }

            set { FilenameTextBox.Text = value; }
        }

        #endregion

        #region Constructors

        public NewHierachyForm(Workspace workspace)
        {
            _presenter = new NewHierarchyPresenter(this,
                workspace);
        }

        public NewHierachyForm()
        {
            InitializeComponent();
        }

        #endregion

        #region  Interface Implementations

        public event EventHandler<EventArgs> CreateEnterpriseTask;

        #endregion

        #region Members

        private void CancelButton_Click(object sender,
            EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void OkButton_Click(object sender,
            EventArgs e)
        {
            // Basic validation
            if (string.IsNullOrEmpty(FileName))
            {
                MessageBox.Show("Please select a file location for this hierarchy");
            }
            else if (string.IsNullOrEmpty(EquipmentId))
            {
                MessageBox.Show("Please enter an Equipment ID for this hierarchy");
            }
            else
            {
                //if (CreateHierarchy != null)
                //{
                //    CreateHierarchy(this,
                //        EventArgs.Empty);
                //    Close();
                //}
            }
        }

        private void SetFileButton_Click(object sender,
            EventArgs e)
        {
            if (DialogResult.OK == saveFileDialog.ShowDialog())
            {
                FilenameTextBox.Text = saveFileDialog.FileName;
            }
        }

        #endregion
    }
}