﻿using System.Windows.Forms;
using MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser;
using Telerik.WinControls.Themes;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	partial class PanelForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.office2013LightTheme = new Telerik.WinControls.Themes.Office2013LightTheme();
			this.splitContainer = new Telerik.WinControls.UI.RadSplitContainer();
			this.splitPanelUpper = new Telerik.WinControls.UI.SplitPanel();
			this.splitPanelLower = new Telerik.WinControls.UI.SplitPanel();
			this._modelObjectPropertyUserControl = new MI.Modeller.Client.Visio2016.UserControls.ModelObjectPropertyUserControl(_modellerPresenter);
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
			this.splitContainer.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitPanelUpper)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.splitPanelLower)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this._modelObjectPropertyUserControl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// splitContainer
			// 
			this.splitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer.Controls.Add(this.splitPanelUpper);
			this.splitContainer.Controls.Add(this.splitPanelLower);
			this.splitContainer.Location = new System.Drawing.Point(0, 0);
			this.splitContainer.Name = "splitContainer";
			this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// 
			// 
			this.splitContainer.RootElement.MinSize = new System.Drawing.Size(25, 25);
			this.splitContainer.Size = new System.Drawing.Size(638, 341);
			this.splitContainer.TabIndex = 0;
			this.splitContainer.TabStop = false;
			this.splitContainer.Text = "splitContainer";
			// 
			// splitPanelUpper
			// 
			this.splitPanelUpper.Location = new System.Drawing.Point(0, 0);
			this.splitPanelUpper.Name = "splitPanelUpper";
			// 
			// 
			// 
			this.splitPanelUpper.RootElement.MinSize = new System.Drawing.Size(25, 25);
			this.splitPanelUpper.Size = new System.Drawing.Size(638, 198);
			this.splitPanelUpper.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, 0.0887574F);
			this.splitPanelUpper.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 30);
			this.splitPanelUpper.TabIndex = 0;
			this.splitPanelUpper.TabStop = false;
			this.splitPanelUpper.Text = "splitPanelUpper";
			// 
			// splitPanelLower
			// 
			this.splitPanelLower.Controls.Add(this._modelObjectPropertyUserControl);
			this.splitPanelLower.Location = new System.Drawing.Point(0, 202);
			this.splitPanelLower.Name = "splitPanelLower";
			// 
			// 
			// 
			this.splitPanelLower.RootElement.MinSize = new System.Drawing.Size(25, 25);
			this.splitPanelLower.Size = new System.Drawing.Size(638, 139);
			this.splitPanelLower.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.0887574F);
			this.splitPanelLower.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -30);
			this.splitPanelLower.TabIndex = 1;
			this.splitPanelLower.TabStop = false;
			this.splitPanelLower.Text = "splitPanelLower";
			// 
			// _modelObjectPropertyUserControl
			// 
			this._modelObjectPropertyUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this._modelObjectPropertyUserControl.Location = new System.Drawing.Point(0, 0);
			this._modelObjectPropertyUserControl.Name = "_modelObjectPropertyUserControl";
			this._modelObjectPropertyUserControl.Size = new System.Drawing.Size(355, 127);
			this._modelObjectPropertyUserControl.TabIndex = 0;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = global::MI.Modeller.Client.Visio2016.Properties.Resources.MI_Modeller;
			this.pictureBox1.Location = new System.Drawing.Point(3, 344);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(260, 60);
			this.pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// PanelControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.splitContainer);
			this.FormBorderStyle =  FormBorderStyle.None;
			this.Name = "PanelControl";
			this.RootElement.ApplyShapeToControl = true;
			this.Text = "Object Browser";
			this.ThemeName = "Office2013Light";
			this.Size = new System.Drawing.Size(638, 407);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
			this.splitContainer.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitPanelUpper)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.splitPanelLower)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this._modelObjectPropertyUserControl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private Telerik.WinControls.UI.RadSplitContainer splitContainer;
		private Telerik.WinControls.UI.SplitPanel splitPanelUpper;
		private Telerik.WinControls.UI.SplitPanel splitPanelLower;
		private ModellerTreeView _treeView;
		private ModelObjectPropertyUserControl _modelObjectPropertyUserControl;
		private Office2013LightTheme office2013LightTheme;
		private FolderBrowserDialog FolderBrowserDialog;
		private PictureBox pictureBox1;
	}
}