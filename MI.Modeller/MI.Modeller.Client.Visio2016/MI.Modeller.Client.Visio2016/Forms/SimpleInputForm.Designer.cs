namespace MI.Modeller.Client.Forms
{
    partial class SimpleInputForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radLayoutControl1 = new Telerik.WinControls.UI.RadLayoutControl();
            this.TextBox = new Telerik.WinControls.UI.RadTextBoxControl();
            this.Label = new Telerik.WinControls.UI.RadLabel();
            this.CancelButton = new Telerik.WinControls.UI.RadButton();
            this.OkButton = new Telerik.WinControls.UI.RadButton();
            this.layoutControlItem2 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem1 = new Telerik.WinControls.UI.LayoutControlItem();
            this.layoutControlItem3 = new Telerik.WinControls.UI.LayoutControlItem();
            ((System.ComponentModel.ISupportInitialize)(this.radLayoutControl1)).BeginInit();
            this.radLayoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OkButton)).BeginInit();
            this.SuspendLayout();
            // 
            // radLayoutControl1
            // 
            this.radLayoutControl1.Controls.Add(this.TextBox);
            this.radLayoutControl1.Controls.Add(this.Label);
            this.radLayoutControl1.DrawBorder = false;
            this.radLayoutControl1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.layoutControlItem1,
            this.layoutControlItem3});
            this.radLayoutControl1.Location = new System.Drawing.Point(12, 12);
            this.radLayoutControl1.Name = "radLayoutControl1";
            this.radLayoutControl1.Size = new System.Drawing.Size(351, 28);
            this.radLayoutControl1.TabIndex = 3;
            this.radLayoutControl1.Text = "radLayoutControl1";
            // 
            // TextBox
            // 
            this.TextBox.Location = new System.Drawing.Point(71, 3);
            this.TextBox.Name = "TextBox";
            this.TextBox.Size = new System.Drawing.Size(277, 22);
            this.TextBox.TabIndex = 3;
            // 
            // Label
            // 
            this.Label.Location = new System.Drawing.Point(3, 5);
            this.Label.Name = "Label";
            this.Label.Size = new System.Drawing.Size(65, 18);
            this.Label.TabIndex = 4;
            this.Label.Text = "Placeholder";
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(296, 46);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(67, 24);
            this.CancelButton.TabIndex = 4;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.Click += this.CancelButtonClick;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(223, 46);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(67, 24);
            this.OkButton.TabIndex = 5;
            this.OkButton.Text = "Ok";
            this.OkButton.Click += this.OkButton_Click;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.AssociatedControl = null;
            this.layoutControlItem2.BorderBoxStyle = Telerik.WinControls.BorderBoxStyle.FourBorders;
            this.layoutControlItem2.BorderWidth = 0F;
            this.layoutControlItem2.Bounds = new System.Drawing.Rectangle(0, 0, 97, 28);
            this.layoutControlItem2.ControlVerticalAlignment = Telerik.WinControls.UI.RadVerticalAlignment.Center;
            this.layoutControlItem2.DisabledTextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.AssociatedControl = this.TextBox;
            this.layoutControlItem1.Bounds = new System.Drawing.Rectangle(68, 0, 283, 28);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Text = "";
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.AssociatedControl = this.Label;
            this.layoutControlItem3.Bounds = new System.Drawing.Rectangle(0, 0, 68, 28);
            this.layoutControlItem3.ControlVerticalAlignment = Telerik.WinControls.UI.RadVerticalAlignment.Center;
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Text = "Placeholder";
            // 
            // SimpleInputForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 79);
            this.Controls.Add(this.radLayoutControl1);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.CancelButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SimpleInputForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SimpleInputForm";
            ((System.ComponentModel.ISupportInitialize)(this.radLayoutControl1)).EndInit();
            this.radLayoutControl1.ResumeLayout(false);
            this.radLayoutControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OkButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadLayoutControl radLayoutControl1;
        private Telerik.WinControls.UI.RadTextBoxControl TextBox;
        private Telerik.WinControls.UI.RadLabel Label;
        private new Telerik.WinControls.UI.RadButton CancelButton;
        private Telerik.WinControls.UI.RadButton OkButton;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem1;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem3;
        private Telerik.WinControls.UI.LayoutControlItem layoutControlItem2;
    }
}