using System;
using System.Collections.Generic;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.Visio2016.Resources;
using MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser;
using System.Drawing;
using System.Windows.Forms;
using MI.Framework.ObjectBrowsing;
using Telerik.WinControls.Enumerations;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.Forms
{
	partial class TreeViewForm: RadForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		/// <param name="rootNodeIds">The root nodes for this form</param>
		/// <param name="modelElementTreeFilter">The tree filter.</param>
		/// <param name="modellerPresenter">Themodeller Presenter to interact with</param>
		private void InitializeComponent(
			IEnumerable<Guid> rootNodeIds,
			ModelElementTreeFilter modelElementTreeFilter,
			IModellerPresenter modellerPresenter)
		{
			this.TreeView = new ModellerTreeView(rootNodeIds, modellerPresenter, modelElementTreeFilter);
			this.OkButton = new Telerik.WinControls.UI.RadButton();
			this.CancelButton = new Telerik.WinControls.UI.RadButton();
			this.office2013LightTheme1 = new Telerik.WinControls.Themes.Office2013LightTheme();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.modelNameTextBox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.TreeView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.OkButton)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.CancelButton)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			this.SuspendLayout();
			// 
			// TreeView
			// 
			this.TreeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.TreeView.Location = new System.Drawing.Point(13, 111);
			this.TreeView.Name = "TreeView";
			this.TreeView.Size = new System.Drawing.Size(356, 217);
			this.TreeView.SpacingBetweenNodes = -1;
			this.TreeView.TabIndex = 0;
			// 
			// OkButton
			// 
			this.OkButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.OkButton.Location = new System.Drawing.Point(213, 355);
			this.OkButton.Name = "OkButton";
			this.OkButton.Size = new System.Drawing.Size(75, 23);
			this.OkButton.TabIndex = 1;
			this.OkButton.Text = global::MI.Modeller.Client.Visio2016.Resources.UIMessages.OKText;
			this.OkButton.Click += this.OkButton_Click;
			// 
			// CancelButton
			// 
			this.CancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.CancelButton.Location = new System.Drawing.Point(294, 355);
			this.CancelButton.Name = "CancelButton";
			this.CancelButton.Size = new System.Drawing.Size(75, 23);
			this.CancelButton.TabIndex = 1;
			this.CancelButton.Text = global::MI.Modeller.Client.Visio2016.Resources.UIMessages.Cancel_text;
			this.CancelButton.Click += this.CancelButton_Click;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pictureBox1.Image = global::MI.Modeller.Client.Visio2016.Properties.Resources.MI_Modeller;
			this.pictureBox1.Location = new System.Drawing.Point(13, 12);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(260, 60);
			this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.pictureBox1.TabIndex = 2;
			this.pictureBox1.TabStop = false;
			// 
			// modelNameTextBox
			// 
			this.modelNameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.modelNameTextBox.Location = new System.Drawing.Point(111, 334);
			this.modelNameTextBox.Name = "modelNameTextBox";
			this.modelNameTextBox.Size = new System.Drawing.Size(258, 20);
			this.modelNameTextBox.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 337);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(92, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "New Model Name";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 79);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(143, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Select where in the Hierarchy you would like to place your Model";
			// 
			// TreeViewForm
			// 
			this.AcceptButton = OkButton;
			base.CancelButton = this.CancelButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(381, 395);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.modelNameTextBox);
			this.Controls.Add(this.pictureBox1);
			this.Controls.Add(this.CancelButton);
			this.Controls.Add(this.OkButton);
			this.Controls.Add(this.TreeView);
			this.Name = "TreeViewForm";
			this.IconScaling = ImageScaling.SizeToFit;
			this.ShowIcon = false;
			// 
			// 
			// 
			this.RootElement.ApplyShapeToControl = true;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "Create New Model";
			this.ThemeName = "Office2013Light";
			((System.ComponentModel.ISupportInitialize)(this.TreeView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.OkButton)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.CancelButton)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private ModellerTreeView TreeView;
        private Telerik.WinControls.UI.RadButton OkButton;
        private new Telerik.WinControls.UI.RadButton CancelButton;
        private Telerik.WinControls.Themes.Office2013LightTheme office2013LightTheme1;
		private System.Windows.Forms.PictureBox pictureBox1;
		private System.Windows.Forms.TextBox modelNameTextBox;
		private System.Windows.Forms.Label label1;
		private Label label2;
	}
}