﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser;
using MI.Modeller.Domain;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	/// <summary>
	///     Class PanelForm.
	/// </summary>
	/// <seealso>
	///     <cref>"System.Windows.Forms.Form"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.ViewInterfaces.IPanelView"</cref>
	/// </seealso>
	public partial class PanelForm : RadForm,
										IPanelView
	{
		#region  Fields

		/// <summary>
		///     The presenter
		/// </summary>
		private readonly IPanelPresenter _presenter;
		/// <summary>
		/// The modeller presenter to interact with
		/// </summary>

		private IModellerPresenter _modellerPresenter;

		/// <summary>
		///     The tree view being presented
		/// </summary>
		/// <value>The TreeView.</value>
		public IModellerTreeView TreeView => _treeView;

		#endregion

		#region Constructors


		/// <summary>
			///     Initializes a new instance of the <see cref="PanelForm" /> class.
			/// </summary>
			/// <param name="modellerPresenter">The modeller presenter to interact with</param>
			public PanelForm(IModellerPresenter modellerPresenter)
		{
			_modellerPresenter = ArgumentValidation.AssertNotNull(modellerPresenter,
				nameof(modellerPresenter));

			FolderBrowserDialog = new FolderBrowserDialog();


			_presenter = new PanelPresenter(this);
			InitializeComponent();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				_treeView = new ModellerTreeView(
					new[] { scope.Query<RootNode>().Single().Id },
					_modellerPresenter,
					// We can do anything with this treeview
					null);
				SetupTreeView();
				HidePropertyPane();
				scope.Done();
			}

			AlignPictureBox();
		}

		/// <summary>
		/// Aligns the picture box to the right
		/// </summary>
		private void AlignPictureBox()
		{
			pictureBox1.Width = pictureBox1.Image.Width;
			pictureBox1.Left = this.Right - pictureBox1.Width;
		}

		#endregion

		#region  Interface Implementations

		public void SelectObjects(IEnumerable<BrowserNode> browserNodes)
		{
			_treeView.SelectNodes(browserNodes);
		}

		public void HidePropertyPane()
		{
			_modelObjectPropertyUserControl.Visible = false;
		}

		public void SetupPropertyPane(ModelElement modelElement)
		{
			_modelObjectPropertyUserControl.InitialisePropertyStore(modelElement);
			_modelObjectPropertyUserControl.Visible = true;
		}

		#endregion

		#region Members

		/// <summary>
		///     Does the after select.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The argument instance containing the event data.</param>
		private void DoAfterSelect(object sender,
			NodeSelectionChangedEventArguments e)
		{
			_presenter.ShowNodeProperties(e.NewSelectedNodeIds);
		}

		/// <summary>
		///     Setups the TreeView.
		/// </summary>
		private void SetupTreeView()
		{
			splitPanelUpper.Controls.Add(_treeView);
			_treeView.Dock = DockStyle.Fill;
			_treeView.Location = new Point(0, 0);
			_treeView.Size = new Size(1042, 324);
			_treeView.SpacingBetweenNodes = -1;
			_treeView.TabIndex = 0;

			_treeView.SelectedNodeChanged += DoAfterSelect;
		}

		#endregion
	}
}
