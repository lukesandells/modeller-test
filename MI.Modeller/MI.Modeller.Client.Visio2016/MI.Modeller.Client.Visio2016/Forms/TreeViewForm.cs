using MI.Modeller.Client.Forms;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.Visio2016.Resources;
using MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.Client.Visio2016.Forms
{
	/// <summary>
	///     The form for displaying a tree view
	/// </summary>
	public partial class TreeViewForm : ITreeViewForm
    {
        #region  Fields

        /// <summary>
        ///     The selected object
        /// </summary>
        public Guid SelectedNodeId { get; private set; }

		/// <summary>
		/// The name for the new model
		/// </summary>
	    public string NewModelName => modelNameTextBox.Text;

        #endregion

        #region Constructors

	    /// <summary>
	    ///     Creates a new isntance of TreeViewForm
	    /// </summary>
	    /// <param name="rootNodeIds">The root nodes for this form</param>
	    /// <param name="modelElementTreeFilter">The filter to apply to the tree</param>
	    /// <param name="modellerPresenter"></param>
	    public TreeViewForm(
			IEnumerable<Guid> rootNodeIds,
			ModelElementTreeFilter modelElementTreeFilter,
            IModellerPresenter modellerPresenter)
        {
            InitializeComponent(
	            rootNodeIds,
				modelElementTreeFilter,
                modellerPresenter);
	        this.pictureBox1.Visible = false;
	        TreeView.AllowDrag = false;
			this.Shown += (s, a) =>
			{
				RefreshPictureLocation();
				this.pictureBox1.Visible = true;
			};
			this.SizeChanged += (sender, args) => this.RefreshPictureLocation();
		}

        #endregion

        #region  Interface Implementations

        public new ModellerDialogResult ShowDialog()
        {
            var result = base.ShowDialog();
            if (result == DialogResult.OK)
            {
                return ModellerDialogResult.OK;
            }
            return ModellerDialogResult.Cancel;
        }

        #endregion

        #region Members

        private void CancelButton_Click(object sender,
            EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void OkButton_Click(object sender,
            EventArgs e)
        {
	        using (var scope = new PersistenceScope(TransactionOption.Required))
	        {
		        var treeNode = TreeView.SelectedNode as BrowserNodeModellerTreeNode;
		        if (treeNode == null)
		        {
					MessageBox.Show(UIMessages.Select_valid_node);
					scope.Done();
			        return;
		        }
				var node = scope.FindById<DefinitionNode>(treeNode.TargetId);
		        if (!TreeView.Filter?.SelectableNodes().Any(n => n == node as ModelElement) ?? false)
		        {
			        MessageBox.Show(UIMessages.Select_valid_node);
		        }
				// Model name must be entered
				else if (string.IsNullOrEmpty(modelNameTextBox.Text) 
					// And unique within this hierarchy scope only
					|| node.NonEmptyFolders.Any(f => f.Nodes.Any(n => n.Type == NodeType.For(typeof(Model)) && n.DisplayId == modelNameTextBox.Text)))
		        {
			        MessageBox.Show(UIMessages.TreeViewForm_OkButton_Click_Please_enter_a_valid_model_name);
		        }
				else
		        {
			        DialogResult = DialogResult.OK;
			        SelectedNodeId = ((BrowserNodeModellerTreeNode) TreeView.SelectedNode).TargetId;
			        Close();
		        }
				scope.Done();
	        }
        }

		#endregion

		private void RefreshPictureLocation()
		{
			pictureBox1.SizeMode =  PictureBoxSizeMode.StretchImage;
			pictureBox1.Height = this.ScaleValue(pictureBox1.Image.Height);
			pictureBox1.Width = this.ScaleValue(pictureBox1.Image.Width);
			pictureBox1.Location = new Point((pictureBox1.Parent.ClientSize.Width / 2) - (pictureBox1.Width / 2),
				pictureBox1.Location.Y);
			pictureBox1.Refresh();
		}
	}
}