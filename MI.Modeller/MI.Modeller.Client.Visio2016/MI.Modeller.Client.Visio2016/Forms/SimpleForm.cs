using System;
using System.ComponentModel;
using System.Windows.Forms;
using MI.Framework.Validation;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.UserControls;

namespace MI.Modeller.Client.Visio2016.Forms
{
    /// <summary>
    ///     Class SimpleForm.
    /// </summary>
    /// <seealso>
    ///     <cref>"System.Windows.Forms.Form"</cref>
    /// </seealso>
    /// <seealso>
    ///     <cref>"MI.Modeller.Client.Forms.ISimpleFormEvents"</cref>
    /// </seealso>
    public partial class SimpleForm : ISimpleForm

    {
        #region  Fields

        /// <summary>
        ///     The equipment class user control
        /// </summary>
        private UserControl _userControl;

        public string Title
        {
            get { return Text; }
            set { Text = value; }
        }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SimpleForm" /> class.
        /// </summary>
        public SimpleForm()
        {
            InitializeComponent();
        }

        #endregion

        #region  Interface Implementations

        public void AddButtonPanelButton(Telerik.WinControls.UI.RadButton button)
        {
            button = ArgumentValidation.AssertNotNull(button,
                nameof(button));
            ButtonPanel.ResumeLayout();
            ButtonPanel.Controls.Add(button);
            ButtonPanel.ResumeLayout(false);
        }


        /// <summary>
        ///     Occurs when [ok click].
        /// </summary>
        public event EventHandler<CancelEventArgs> OkClicked;

        #endregion

        #region Members

        /// <summary>
        ///     Sets the user control.
        /// </summary>
        /// <param name="userControl">The user control.</param>
        /// <exception cref="Exception">User Control is already Set</exception>
        public void SetUserControl(IUserControl userControl)
        {
            if (_userControl != null)
            {
                throw new Exception("User Control is already Set");
            }

            if (!(userControl is UserControl))
            {
                throw new Exception("User control does not implement Windows UserControl");
            }

            _userControl = (UserControl)ArgumentValidation.AssertNotNull(userControl,
                nameof(userControl));

            _userControl.Name = nameof(_userControl);
            _userControl.Dock = DockStyle.Fill;
	        _userControl.AutoSize = true;
            ContentPanel.Controls.Add(_userControl);
        }


        /// <summary>
        ///     Handles the Click event of the OKButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
        private void OKButton_Click(object sender, EventArgs e)
        {
			try
			{
				var cancelEventArgs = new CancelEventArgs();
				OkClicked?.Invoke(this,
					cancelEventArgs);
				if (!cancelEventArgs.Cancel)
				{
					DialogResult = DialogResult.OK;
					Close();
				}
			}
			catch (Exception err)
			{
				MessageBox.Show(err.Message);
			}
		}


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        /// <summary>
        /// Sets the forms user control
        /// </summary>
        /// <param name="control"></param>
        public void SetUserControl(IModellerTreeView control)
        {
			SetUserControl((IUserControl) control);
        }

        /// <summary>
        /// Shows the form as a dialog
        /// </summary>
        /// <returns></returns>
        public new ModellerDialogResult ShowDialog()
        {
			var result = base.ShowDialog();
	        this.Width = 1000;
			this.Height = 1000;
            if (result == DialogResult.OK)
            {
                return ModellerDialogResult.OK;
            }
            return ModellerDialogResult.Cancel;
        }

        #endregion
    }
}