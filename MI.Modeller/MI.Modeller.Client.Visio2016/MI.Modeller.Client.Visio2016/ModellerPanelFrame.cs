﻿using Microsoft.Office.Interop.Visio;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using MI.Framework.Validation;
using MI.Modeller.Client.ViewInterfaces;

// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///     Integrates a winform in Visio.
	///     Creates an anchor window for the given diagram window, and installs the specified panelView as a child in that panel.
	/// </summary>
	public sealed class ModellerPanelFrame : IVisEventProc
    {
        /// <summary>
        /// The event is triggered when user closes the panel using "x" button
        /// </summary>
        /// <param name="sender">The sender.</param>
        public delegate void PanelFrameClosedEventHandler(object sender, EventArgs e);

        #region Static Fields and Constants

        private const string C_ADDON_WINDOW_MERGE_ID = "c22d056b-3638-48c9-8781-b97f807b8c43";

        #endregion

        #region Constructors

        /// <summary>
        ///     Constructs a new panel frame.
        /// </summary>
        /// <param name="panelView">The panelView to install</param>
        public ModellerPanelFrame(IPanelView panelView)
        {
            PanelView = ArgumentValidation.AssertNotNull(panelView, nameof(panelView));
        }

	    #endregion

        #region  Interface Implementations

        object IVisEventProc.VisEventProc(short nEventCode,
            object pSourceObj,
            int nEventId,
            int nEventSeqNum,
            object pSubjectObj,
            object vMoreInfo)
        {
            object returnValue = false;

            try
            {
                var subjectWindow = pSubjectObj as Window;
                switch (nEventCode)
                {
                    case (short) VisEventCodes.visEvtDel + (short) VisEventCodes.visEvtWindow:
                    {
                        OnBeforeWindowClosed(subjectWindow);
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }

            return returnValue;
        }

        #endregion

        #region Members

        public event PanelFrameClosedEventHandler PanelFrameClosed;

        private void OnBeforeWindowClosed(Window visioWindow)
        {
            PanelFrameClosed?.Invoke(_visioWindow.ParentWindow, null);

            DestroyWindow();
        }

		#endregion

		#region WIN API Declares

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
		[DllImport("user32.dll", EntryPoint = "SetWindowLong", CharSet = CharSet.Auto,
			CallingConvention = CallingConvention.Winapi)]
		private static extern int SetWindowLong(IntPtr hWnd,
			int index,
			uint newLong);

	    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
	    [DllImport("user32.dll", EntryPoint = "GetWindowLong", CharSet = CharSet.Auto,
		    CallingConvention = CallingConvention.Winapi)]
	    private static extern IntPtr GetWindowLong(IntPtr hWnd,
		    int index);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
		[DllImport("user32.dll", EntryPoint = "SetParent", CharSet = CharSet.Auto,
			CallingConvention = CallingConvention.Winapi)]
		private static extern int SetParent(IntPtr hWndChild,
			IntPtr hWndNewParent);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        [DllImport("user32.dll", EntryPoint = "GetWindowRect", CharSet = CharSet.Auto,
             CallingConvention = CallingConvention.Winapi)]
        private static extern int GetWindowRect(IntPtr hWnd,
            ref Rect lpRect);

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1060:MovePInvokesToNativeMethodsClass")]
        [DllImport("user32.dll", EntryPoint = "SetWindowPos", CharSet = CharSet.Auto,
             CallingConvention = CallingConvention.Winapi)]
        private static extern int SetWindowPos(IntPtr hWnd,
            IntPtr hWndInsertAfter,
            int x,
            int y,
            int cx,
            int cy,
            int wFlags);

        private const int C_GWL_STYLE = -16;
	    private const int C_GWL_WNDPROC = -4;
	    private const int C_WS_OVERLAPPED = 0x00000000;
	    private const int C_WS_CHILD = 0x40000000;
		private const int C_SWP_NOCOPYBITS = 0x100;
        private const int C_SWP_NOMOVE = 0x2;
        private const int C_SWP_NOZORDER = 0x4;


        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        private struct Rect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        #endregion

        #region fields

        private Window _visioWindow;
        public IPanelView PanelView { get; private set; }
	    private IntPtr _originalHandler;

	    #endregion

        #region methods

        /// <summary>
        ///     Destroys the panel frame along with the panelView.
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "MI.Modeller.Client.Visio2016.ModellerPanelFrame.SetParent(System.IntPtr,System.IntPtr)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "MI.Modeller.Client.Visio2016.ModellerPanelFrame.SetWindowLong(System.IntPtr,System.Int32,System.Int32)")]
        public void DestroyWindow()
        {
            try
            {
                if ((_visioWindow != null) && (PanelView != null))
                {
                    var childWindowHandle = ((Form) PanelView).Handle;

					((Form)PanelView).Hide();

                    SetWindowLong(childWindowHandle,
                        C_GWL_STYLE,
                        C_WS_OVERLAPPED);
                    SetParent(childWindowHandle,
                        (IntPtr) 0);

                    _visioWindow.Close();
                    _visioWindow = null;
                }
              
                    PanelView = null;
            }
            // ReSharper disable once EmptyGeneralCatchClause : ignore all errors on exit
            catch {}
        }

        /// <summary>
        ///     Install the panel into given window (actually creates the panelView and shows it)
        /// </summary>
        /// <param name="visioParentWindow">The parent Visio window where the panel should be installed to.</param>
        /// <returns></returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "MI.Modeller.Client.Visio2016.ModellerPanelFrame.SetParent(System.IntPtr,System.IntPtr)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "MI.Modeller.Client.Visio2016.ModellerPanelFrame.SetWindowLong(System.IntPtr,System.Int32,System.Int32)")]
        public Window CreateWindow(Window visioParentWindow)
        {
            Window retVal = null;

            try
            {
                if (visioParentWindow == null)
                {
                    return null;
                }

                if (PanelView != null)
                {
					// Store the original event handler
	                _originalHandler = GetWindowLong(((Form)PanelView).Handle, C_GWL_WNDPROC);
					// Create a new visio window
					_visioWindow = visioParentWindow.Windows.Add(((Form)PanelView).Text,
									  (int)VisWindowStates.visWSDockedRight | (int)VisWindowStates.visWSAnchorMerged,
									  VisWinTypes.visAnchorBarAddon,
									  0,
									  0,
									  ((Form)PanelView).Width,
									  ((Form)PanelView).Height,
									  C_ADDON_WINDOW_MERGE_ID,
							C_ADDON_WINDOW_MERGE_ID + "Class",
									  0);
					// Wire up some events, specifically we need to pass through key presses and we need to close the panel when the winodw is closed
	                _visioWindow.OnKeystrokeMessageForAddon += HandleKeyForAddon;
					_visioWindow.BeforeWindowClosed += OnBeforeWindowClosed;
					
					// Make if visisble
					_visioWindow.Visible = true;
					// Set the new window to the parent of our panelView
					var parentWindowHandle = (IntPtr)_visioWindow.WindowHandle32;
					SetParent(((Form)PanelView).Handle,
						parentWindowHandle);
	                SetWindowLong(((Form)PanelView).Handle, C_GWL_STYLE, C_WS_CHILD);

					// Make it all visible
					
					JiggleWindow(parentWindowHandle);
                    _visioWindow.Visible = true;
					// Padding needed to prevent clipping
					((Form)PanelView).Padding = new Padding(0, 0, (int) (((Form)PanelView).ScaleFactor() * 15), (int)(((Form)PanelView).ScaleFactor() * 8));
					((Form)PanelView).Dock = DockStyle.Fill;
					((Form)PanelView).Show();
					retVal = _visioWindow;
                }
            }
            catch (Exception ex)
            {
                Debug.Write(ex.Message);
            }

            return retVal;
        }

	    [DllImport("user32.dll")]
	    private static extern IntPtr CallWindowProc(IntPtr lpPrevWndFunc, IntPtr hWnd, uint msg, IntPtr wParam, IntPtr lParam);

		/// <summary>
		/// Handles a key press destined for the addon
		/// </summary>
		/// <returns></returns>
		private bool HandleKeyForAddon(MSGWrap msg)
	    {
			// Visio in it's infinite wisdom ignores some key presses that we want to listen to. 
			// Specifically we need F2 and Escape for the controls to handle correctly
			if ((Keys)msg.wParam == Keys.F2 || (Keys)msg.wParam == Keys.Escape )
		    {
			    CallWindowProc(_originalHandler, (IntPtr) msg.hwnd, (uint) msg.message, (IntPtr) msg.wParam, (IntPtr) msg.lParam);
		    }
			return false;
	    }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "MI.Modeller.Client.Visio2016.ModellerPanelFrame.SetWindowPos(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)")]
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1806:DoNotIgnoreMethodResults", MessageId = "MI.Modeller.Client.Visio2016.ModellerPanelFrame.GetWindowRect(System.IntPtr,MI.Modeller.Client.Visio2016.ModellerPanelFrame+RECT@)")]
        private static void JiggleWindow(IntPtr handle)
        {
            var lpRect = new Rect();
            // ReSharper disable once UnusedVariable
            var result = GetWindowRect(handle,
                ref lpRect);

            var l = lpRect.left;
            var T = lpRect.top;
            var w = lpRect.right - lpRect.left + 10;
            var h = lpRect.bottom - lpRect.top;

            const int flags = C_SWP_NOCOPYBITS | C_SWP_NOMOVE | C_SWP_NOZORDER;
            SetWindowPos(handle,
                new IntPtr(0),
                l,
                T,
                w,
                h + 1,
                flags);
            SetWindowPos(handle,
                new IntPtr(0),
                l,
                T,
                w,
                h,
                flags);
        }

        #endregion
    }
}