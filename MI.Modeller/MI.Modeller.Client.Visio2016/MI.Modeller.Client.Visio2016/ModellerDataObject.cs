using System.Windows.Forms;
using Microsoft.Office.Interop.Visio;

namespace MI.Modeller.Client.Visio2016
{
    /// <summary>
    /// Wrapper for the DataObject class
    /// </summary>
    internal class ModellerDataObject : DataObject, IModellerDataObject
    {
		#region Constructors

		/// <summary>
		/// Construct a new ModellerDataObject
		/// </summary>
		/// <param name="data">The data to wrap in the dataobject</param>
		public ModellerDataObject(object data) : base(data) { }

        #endregion
    }
}