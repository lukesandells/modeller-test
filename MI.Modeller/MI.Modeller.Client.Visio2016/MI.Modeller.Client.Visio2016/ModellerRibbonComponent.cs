﻿using System.Runtime.InteropServices;
using Microsoft.Office.Tools.Ribbon;

namespace MI.Modeller.Client.Visio2016
{
    /// <summary>
    ///     Class ModellerRibbonComponent.
    /// </summary>
    /// <seealso>
    ///     <cref>"Microsoft.Office.Tools.Ribbon.RibbonBase"</cref>
    /// </seealso>
    [ComVisible(false)]
    public partial class ModellerRibbonComponent
    {
        #region Members

		/// <summary>
		/// Weather or not this ribbon is enabled
		/// </summary>
	    public bool Enabled
	    {
		    set => PanelToggleButton.Enabled = value;
	    }

        /// <summary>
        ///     Handles the Click event of the PanelToggleButton control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="RibbonControlEventArgs" /> instance containing the event data.</param>
        private void PanelToggleButton_Click(object sender,
            RibbonControlEventArgs e)
        {
            Globals.ModellerAddin.HandleTogglePanel();
        }

        #endregion
    }
}