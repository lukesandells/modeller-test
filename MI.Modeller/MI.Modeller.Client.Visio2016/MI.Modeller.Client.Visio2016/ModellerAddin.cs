﻿using log4net;
using log4net.Config;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;
using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.ModellerEventArgs;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.Properties;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Client.Visio2016.Forms;
using MI.Modeller.Client.Visio2016.UserControls;
using MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;
using MI.Modeller.Persistence;
using Microsoft.Office.Interop.Visio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Application = Microsoft.Office.Interop.Visio.Application;
using Page = Microsoft.Office.Interop.Visio.Page;
using Path = System.IO.Path;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///     Class ModellerAddin.
	///     Main Class for Visio AddIn
	/// </summary>
	/// <seealso>
	///     <cref>"Microsoft.Office.Tools.AddInBase"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.IModeller"</cref>
	/// </seealso>
	[ComVisible(false)]
    public partial class ModellerAddin : IModeller
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The c argument
        /// </summary>
        private const string C_ARGUMENT = "Argument";

        #endregion

        #region  Fields

        /// <summary>
        ///     The modeller presenter
        /// </summary>
        // ReSharper disable once NotAccessedField.Local
        private ModellerPresenter _modellerPresenter;

        /// <summary>
        ///     Private instance of the panel manager
        /// </summary>
        private ModellerPanelManager _modellerPanelManager;

        /// <summary>
        ///     The shape factory
        /// </summary>
        private VisioShapeFactory _shapeFactory;

        /// <summary>
        ///     Gets or sets the panel form.
        /// </summary>
        /// <value>The panel form.</value>
        public IPanelView PanelForm { get; set; }

	    public IModellerDocument ActiveDocument
	    {
		    get
		    {
			    if (_activeDocument.Item1 != Application.ActiveDocument)
			    {
				    _activeDocument = (Application.ActiveDocument, ModellerVisio2016Document.GetModellerVisio2016Document(Application.ActiveDocument, _modellerPresenter));

			    }
				
			    return _activeDocument.Item2;
			}
	    }
	    private (Document, IModellerDocument) _activeDocument;

	    /// <summary>
        ///     Factory for creating shapes on the page
        /// </summary>
        public IShapeFactory ShapeFactory => _shapeFactory;

	    /// <summary>
	    /// The currently active page
	    /// </summary>
	    public IModellerPage ActivePage => new ModellerVisio2016Page(Application.ActivePage, _modellerPresenter);

	    #endregion

        #region  Interface Implementations

        /// <summary>
        ///     Triggered when a new document is created
        /// </summary>
        public event EventHandler<ModellerDocumentEventArgs> DocumentCreated;

        /// <summary>
        ///     Occurs when [document opened].
        /// </summary>
        public event EventHandler<ModellerDocumentEventArgs> DocumentOpened;

        /// <summary>
        ///     Occurs when [document saved].
        /// </summary>
        public event EventHandler<ModellerDocumentEventArgs> DocumentSaved;

        /// <summary>
        ///     Occurs when [document activated].
        /// </summary>
        public event EventHandler<ModellerDocumentEventArgs> DocumentActivated;

        /// <summary>
        ///     Occurs when [document closed].
        /// </summary>
        public event EventHandler<ModellerDocumentEventArgs> DocumentClosed;

		/// <summary>
		///		Occurs when [page created].
		/// </summary>
		public event EventHandler<ModellerPageEventArgs> PageCreated;

		/// <summary>
		///		Occurs when [page changed].
		/// </summary>
		public event EventHandler<ModellerPageEventArgs> PageChanged;

	    /// <summary>
	    ///		Occurs when [page deleted].
	    /// </summary>
	    public event EventHandler<ModellerPageEventArgs> PageDeleted;

		/// <summary>
		///     Occurs when [shape added].
		/// </summary>
		public event EventHandler<ModellerShapeEventArgs> ShapeAdded;

        /// <summary>
        ///     Occurs when [connection removed].
        /// </summary>
        public event EventHandler<ModellerShapeConnectionEventArgs> ConnectionRemoved;

        /// <summary>
        ///     Occurs when [shape event triggered].
        /// </summary>
        public event EventHandler<ModellerShapeEventArgs> ShapeEventTriggered;

        /// <summary>
        ///     Occurs when [shut down].
        /// </summary>
        public event EventHandler ShutDown;

	    /// <summary>
	    /// log for modeller addin events
	    /// </summary>
	    private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// Create a connection between two occurrences
		/// </summary>
		/// <param name="from">The occurrence to connect from</param>
		/// <param name="to">The occurrence to connect to</param>
		/// <param name="direction">The direction of the connection</param>
		public void Connect(Occurrence from, Occurrence to, ConnectionDirection direction)
	    {
			var shapeFrom = ActiveDocument?.Shapes.FirstOrDefault(s => s.OccurrenceId == from.Id);
		    var shapeTo = ActiveDocument?.Shapes.FirstOrDefault(s => s.OccurrenceId == to.Id);
		    if (shapeTo != null && !shapeTo.GetConnectedShapes().Select(s => s.OccurrenceId).Contains(from.Id))
		    {
			    shapeFrom?.ConnectTo(shapeTo, direction);
		    }
	    }

		/// <summary>
		/// Select the given shapes and return it as a selection
		/// </summary>
		/// <param name="shapes">The shapes to select</param>
		/// <returns></returns>
		public IModellerSelection SelectShapes(IEnumerable<IModellerShape> shapes)
		{
			var selection = new Visio2016Selection(shapes);
			selection.SelectAll();
			return selection;
		}

	    /// <summary>
	    /// Deselect the given shapes and return it as a selection
	    /// </summary>
	    /// <param name="shapes">The shapes to select</param>
	    /// <returns></returns>
	    public IModellerSelection DeselectShapes(IEnumerable<IModellerShape> shapes)
	    {
		    var selection = new Visio2016Selection(shapes);
		    selection.DeselectAll();
		    return selection;
	    }

		/// <summary>
		/// Forces the given document to close
		/// </summary>
		/// <param name="document"></param>
		public void ForceClose(IModellerDocument document)
	    {
			((Document)document.BaseDocument).Close();
		}

		/// <summary>
		/// Opens a given model
		/// </summary>
		/// <param name="model"></param>
	    public void OpenModel(Model model)
	    {
			var path = FilePathFactory.GetModelFilePath(model);
		    Application.Documents.Open(path);
	    }

	    /// <summary>
	    /// Displays a message for the user
	    /// </summary>
	    /// <param name="message">The message to display</param>
		public void DisplayMessage(string message)
	    {
		    MessageBox.Show(message);
	    }

	    public void SelectObjects(IEnumerable<ModelElement> modelElements)
	    {
		    _modellerPanelManager.FindWindowPanelFrame(Application.ActiveWindow)?.PanelView.SelectObjects(modelElements);
	    }

	    /// <summary>
		///     Occurs when [shape deleted].
		/// </summary>
		public event EventHandler<ModellerShapeEventArgs> ShapeDeleted;

        /// <summary>
        ///     Occurs when [connection added].
        /// </summary>
        public event EventHandler<ModellerShapeConnectionEventArgs> ConnectionAdded;

        /// <summary>
        ///     Occurs when [selection changed].
        /// </summary>
        public event EventHandler<ModellerShapeSelectionEventArgs> SelectionChanged;

	    /// <summary>
	    ///     Creates a new ModellerTreeView
	    /// </summary>
	    /// <param name="modelElementTreeFilter">The filter to apply to the tree</param>
	    /// <param name="parent">The form this control is placed in</param>
	    /// <param name="allowMultipleSelections">Whether or not multiple nodes may be selected</param>
	    /// <param name="rootNodeIds">The root nodes to display</param>
	    /// <returns></returns>
	    public IModellerTreeView CreateObjectBrowserView(ModelElementTreeFilter modelElementTreeFilter, ISimpleForm parent, bool allowMultipleSelections, IEnumerable<Guid> rootNodeIds)
        {
            var treeView = new ModellerTreeView(
				rootNodeIds,
				_modellerPresenter,
				modelElementTreeFilter,
				allowMultipleSelections);
            parent?.SetUserControl(treeView);
            return treeView;
        }

        /// <summary>
        ///     Creates a new simpleform
        /// </summary>
        /// <param name="title">The form title</param>
        /// <returns></returns>
        public ISimpleForm CreateSimpleForm(string title)
        {
            var form = new SimpleForm
            {
                Title = title
            };
            return form;
        }

	    /// <summary>
	    ///     Creates a new TreeViewForm
	    /// </summary>
	    /// <param name="rootNodeIds"></param>
	    /// <param name="filter"></param>
	    /// <returns></returns>
	    public ITreeViewForm CreateModellerTreeViewForm(IEnumerable<Guid> rootNodeIds, ModelElementTreeFilter filter)
        {
            return new TreeViewForm(
				rootNodeIds, 
				filter,
                _modellerPresenter);
        }

	    /// <summary>
	    ///     Creates and queues an undo unit for a given transaction
	    /// </summary>
	    /// <param name="nodeTransaction">The node transaction to create an undo unit for</param>
	    /// <param name="description">A description of the event that this undo unit is for</param>
		public void CreateAndQueueUndoUnit(NodeTransaction nodeTransaction, string description)
	    {
		    Application.AddUndoUnit(new Visio2016UndoUnit(nodeTransaction, description));
	    }

	    public void SaveDocument(IModellerDocument document)
        {
	        using (var scope = new PersistenceScope(TransactionOption.Required))
	        {
		        var modelNode = scope.FindById<DefinitionNode>(document.ModelInternalId);
		        var path = FilePathFactory.GetModelFilePath(((Model) modelNode.TargetObject));
		        // ReSharper disable once AssignNullToNotNullAttribute
		        System.IO.Directory.CreateDirectory(Path.GetDirectoryName(path));
		        ((Document) document.BaseDocument).SaveAs(path);
				scope.Done();
	        }
        }

	    public IEnumerable<IModellerDocument> OpenDocuments => (from Document d in Application.Documents
		    select ModellerVisio2016Document.GetModellerVisio2016Document(d, _modellerPresenter)).Where(d => d != null);

		#endregion

		#region Members

	    /// <summary>
		///     Toggles the panel on or off
		/// </summary>
		public void HandleTogglePanel()
        {
			// this is a Visio specific action, no need to push this event to the client layer. Toggling happens purely on the frontend
	        _modellerPanelManager.TogglePanel(Application.ActiveWindow, new PanelForm(_modellerPresenter));
		}

        /// <summary>
        ///     Handles the connections added.
        /// </summary>
        /// <param name="connects">The connects.</param>
        private void HandleConnectionsAdded(Connects connects)
        {
		    foreach (Connect connect in connects)
		    {
			    var modellerConnection = new ModellerVisio2016Connector(
					connect.FromSheet.LineStyle == ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE
						? connect.FromSheet
						: connect.ToSheet,
					_modellerPresenter);
			    
			    ConnectionAdded?.Invoke(this, new ModellerShapeConnectionEventArgs(modellerConnection));
		    }
        }

        /// <summary>
        ///     Handles the connections deleted.
        /// </summary>
        /// <param name="connects">The connects.</param>
        private void HandleConnectionsDeleted(Connects connects)
        {
			foreach (Connect connect in connects) {
				ConnectionRemoved?.Invoke(this,
				new ModellerShapeConnectionEventArgs(new ModellerVisio2016Connector(connect.FromSheet.LineStyle == ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE
						? connect.FromSheet
						: connect.ToSheet,
					_modellerPresenter)));
			}
        }

        /// <summary>
        ///     Handles the document closed.
        /// </summary>
        /// <param name="doc">The document.</param>
        private void HandleDocumentClosed(Document doc)
        {
            var modellerDocument = ModellerVisio2016Document.GetModellerVisio2016Document(doc,
                _modellerPresenter);
            if (modellerDocument != null)
            {
                DocumentClosed?.Invoke(this,
                    new ModellerDocumentEventArgs(modellerDocument));
            }
        }

        /// <summary>
        ///     Handles the document created.
        /// </summary>
        /// <param name="doc">The document.</param>
        private void HandleDocumentCreated(Document doc)
        {
            var modellerDocument = ModellerVisio2016Document.GetModellerVisio2016Document(doc,
                _modellerPresenter);
            if (modellerDocument != null)
            {
                DocumentCreated?.Invoke(this,
                    new ModellerDocumentEventArgs(modellerDocument));
	            _modellerPanelManager.TogglePanel(Application.ActiveWindow, new PanelForm(_modellerPresenter));

			}
	        Globals.Ribbons.GetRibbon<ModellerRibbonComponent>().Enabled = Application.ActiveWindow != null;
		}

        /// <summary>
        ///     Handles the document opened.
        /// </summary>
        /// <param name="doc">The document.</param>
        private void HandleDocumentOpened(Document doc)
        {
            var modellerDocument = ModellerVisio2016Document.GetModellerVisio2016Document(doc,
                _modellerPresenter);
            if (modellerDocument != null)
            {
                DocumentOpened?.Invoke(this,
                    new ModellerDocumentEventArgs(modellerDocument));
	            _modellerPanelManager.TogglePanel(Application.ActiveWindow, new PanelForm(_modellerPresenter));
			}
        }

        /// <summary>
        ///     Handes the document saved event.
        /// </summary>
        /// <param name="doc"></param>
        private void HandleDocumentSaved(Document doc)
        {
            var modellerDocument = ModellerVisio2016Document.GetModellerVisio2016Document(doc,
                _modellerPresenter);
            if (modellerDocument != null)
            {
                DocumentSaved?.Invoke(this,
                    new ModellerDocumentEventArgs(modellerDocument));
            }
	        using (var scope = new PersistenceScope(TransactionOption.Required))
	        {
		        scope.Flush();
				scope.Done();
	        }
        }

        /// <summary>
        ///     Applications the marker event.
        ///     Called from Visio by creating an Action with formaula as follows:
        ///     =RUNADDONWARGS("QueueMarkerEvent","/app=ModellerAddin /args=EditQuantities")
        ///     Where the "args" parameter is the arugument to be checked below (I.e. the command)
        /// </summary>
        /// <param name="visapp">The visapp.</param>
        /// <param name="sequenceNum">The sequence number.</param>
        /// <param name="contextString">The context string.</param>
        private void HandleMarkerEvent(Application visapp,
            int sequenceNum,
            string contextString)
        {
            if (contextString == null)
            {
                return;
            }
            const string pattern = @"^.*(args=)(?<Argument>([^\/ ]*)+).*$";

            var arg = new Regex(pattern,
                    RegexOptions.IgnoreCase).Match(contextString)
                .Groups[C_ARGUMENT].Value;

            var selection = visapp.ActiveWindow.Selection;

            // invoke the event for the presenter
	        ShapeEventTriggered?.Invoke(this,
		        new ModellerShapeEventArgs
		        {
			        Argument = arg,
			        ModellerShape = _shapeFactory.ConstructShape(selection[1], _modellerPresenter)
		        });
        }

        /// <summary>
        ///     Handles the selection changed.
        /// </summary>
        /// <param name="window">The window.</param>
        private void HandleSelectionChanged(Window window)
        {
	        if (_modellerPresenter.SupressSelectionChange)
	        {
		        return;
	        }
            var selectedModellerShapes = window.Selection.OfType<Shape>()
                .Select(selectedShape => _shapeFactory.ConstructShape(selectedShape,
                    _modellerPresenter))
                .ToArray();
            SelectionChanged?.Invoke(this,
                new ModellerShapeSelectionEventArgs(null,
                    selectedModellerShapes));
        }

        /// <summary>
        ///     Applications the shape added.
        /// </summary>
        /// <param name="shape">The shape.</param>
        private void HandleShapeAdded(Shape shape)
        {
            // invoke the event for the presenter
	        ShapeAdded?.Invoke(this,
		        new ModellerShapeEventArgs
		        {
			        ModellerShape = _shapeFactory.ConstructShape(shape, _modellerPresenter),
					ModellerPage =  new ModellerVisio2016Page(shape.ContainingPage, _modellerPresenter)
		        });
        }

        /// <summary>
        ///     Handles the shape deleted.
        /// </summary>
        /// <param name="shape">The shape.</param>
        private void HandleShapeDeleted(Shape shape)
        {
            // invoke the event for the presenter
	        ShapeDeleted?.Invoke(this, new ModellerShapeEventArgs
	        {
		        Argument = null,
		        ModellerShape = _shapeFactory.ConstructShape(shape, _modellerPresenter),
		        ModellerPage = new ModellerVisio2016Page(shape.ContainingPage, _modellerPresenter),
		        ModellerDocument = ModellerVisio2016Document.GetModellerVisio2016Document(shape.Document, _modellerPresenter)
			});
        }

        private void HandleWindowActivated(Window window)
        {
            var modellerDocument = ModellerVisio2016Document.GetModellerVisio2016Document(window.Document,
                _modellerPresenter);
            if (modellerDocument != null)
            {
                DocumentActivated?.Invoke(this,
                    new ModellerDocumentEventArgs(modellerDocument));
            }
        }

        /// <summary>
        ///     Required method for Designer support - do not modify
        ///     the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            Startup += ThisAddIn_Startup;
            Shutdown += ThisAddIn_Shutdown;
        }

        /// <summary>
        ///     On shutdown disposes of the panel manager
        /// </summary>
        /// <param name="sender">The sender of the call</param>
        /// <param name="e">The event arguments</param>
        private void ThisAddIn_Shutdown(object sender,
            EventArgs e)
        {
			// unwire events
	        Application.MarkerEvent -= HandleMarkerEvent;
	        Application.ShapeAdded -= HandleShapeAdded;
	        Application.BeforeShapeDelete -= HandleShapeDeleted;
	        Application.DocumentCreated -= HandleDocumentCreated;
	        Application.PageAdded -= HandlePageAdded;
	        Application.PageChanged -= HandlePageChanged;
	        Application.BeforePageDelete -= HandlePageDeleted;
	        Application.SelectionChanged -= HandleSelectionChanged;
	        Application.DocumentOpened -= HandleDocumentOpened;
	        Application.BeforeDocumentClose -= HandleDocumentClosed;
	        Application.WindowActivated -= HandleWindowActivated;
	        Application.BeforeDocumentSave -= HandleDocumentSaved;
	        Application.BeforeDocumentSaveAs -= HandleDocumentSaved;
	        Application.WindowChanged -= HandleWindowChanged;

			Application.ConnectionsAdded -= HandleConnectionsAdded;
	        Application.ConnectionsDeleted -= HandleConnectionsDeleted;
	        Application.TextChanged -= HandleTextChanged;

			_modellerPanelManager.Dispose();
            Startup -= ThisAddIn_Startup;
            Shutdown -= ThisAddIn_Shutdown;
            ShutDown?.Invoke(this,
                null);
        }

        /// <summary>
        ///     On startup instanciates the panel manager
        /// </summary>
        /// <param name="sender">The sender of the call</param>
        /// <param name="e">The event arguments</param>
        private void ThisAddIn_Startup(object sender,
            EventArgs e)
        {
	        XmlConfigurator.Configure(); // init log4net
			_log.Debug("Starting MI Modeller...");
	        _log.Debug("Setting unit library");
			// Configure storage
			UnitLibrary.Current = new UneceRecommendation20Library();
	        _log.Debug("Adding mappings");
			NodeType.AddMappingAssembly(typeof(ApplicationRootNodeMapping).Assembly);
	        _log.Debug("Initialising database");
			var databasePath = Environment.ExpandEnvironmentVariables($"{Settings.Default.WorkspaceDirectory}\\modeller.db");
	        // ReSharper disable once AssignNullToNotNullAttribute
			System.IO.Directory.CreateDirectory(Path.GetDirectoryName(databasePath));
			_log.Debug("Connecting to database");
			Database.Connect(databasePath, false, false);
			// Setup the root node
	        _log.Debug("Initialising root node");
			using (var scope = new PersistenceScope(TransactionOption.Required))
	        {
		        var rootNode = scope.Query<RootNode>()
			        .SingleOrDefault();
		        if (rootNode == null)
		        {
			        rootNode = new RootNode(NodeTypeId.Root);
			        rootNode.Save();
					scope.Flush();
		        }
		        rootNode.RecycleBin.EmptyAndPersist();
		        scope.Done();
	        }

			// set event handlers for the visio events
	        _log.Debug("Setting event handlers");
			Application.MarkerEvent += HandleMarkerEvent;
            Application.ShapeAdded += HandleShapeAdded;
            Application.BeforeShapeDelete += HandleShapeDeleted;
            Application.DocumentCreated += HandleDocumentCreated;
	        Application.PageAdded += HandlePageAdded;
			Application.PageChanged += HandlePageChanged;
	        Application.BeforePageDelete += HandlePageDeleted;
			Application.SelectionChanged += HandleSelectionChanged;
            Application.DocumentOpened += HandleDocumentOpened;
            Application.BeforeDocumentClose += HandleDocumentClosed;
            Application.WindowActivated += HandleWindowActivated;
            Application.BeforeDocumentSave += HandleDocumentSaved;
            Application.BeforeDocumentSaveAs += HandleDocumentSaved;
	        Application.WindowChanged += HandleWindowChanged;

            Application.ConnectionsAdded += HandleConnectionsAdded;
            Application.ConnectionsDeleted += HandleConnectionsDeleted;

            Application.TextChanged += HandleTextChanged;

			// create the presenter object
			_log.Debug("Creating presenters");
			_shapeFactory = new VisioShapeFactory();
            _modellerPresenter = new ModellerPresenter(this,
                _shapeFactory);
			// create the panel manager that will be responsible for toggling the panel open and closed on the RHS
			_log.Debug("Initialising panel manager");
			_modellerPanelManager = new ModellerPanelManager();
	        _log.Debug("MI Modeller running");
		}

		/// <summary>
		///  Handles the window changing to change the state of the ribbon
		/// </summary>
		/// <param name="window"></param>
	    private void HandleWindowChanged(Window window)
	    {
		    Globals.Ribbons.GetRibbon<ModellerRibbonComponent>().Enabled = Application.ActiveWindow != null;
	    }

	    /// <summary>
	    /// Handles the page created events
	    /// </summary>
	    /// <param name="page">The page that has been created</param>
	    private void HandlePageAdded(Page page)
	    {
		    PageCreated?.Invoke(this, new ModellerPageEventArgs(new ModellerVisio2016Page(page, _modellerPresenter)));
	    }

		/// <summary>
		/// Handles the page changed events
		/// </summary>
		/// <param name="page">The page that has been changed</param>
		private void HandlePageChanged(Page page)
		{
			PageChanged?.Invoke(this, new ModellerPageEventArgs(new ModellerVisio2016Page(page, _modellerPresenter)));
		}

	    /// <summary>
	    /// Handles the page deleted events
	    /// </summary>
	    /// <param name="page">The page that has been changed</param>
	    private void HandlePageDeleted(Page page)
	    {
		    PageDeleted?.Invoke(this, new ModellerPageEventArgs(new ModellerVisio2016Page(page, _modellerPresenter)));
	    }

		/// <summary>
		/// Handle the text changed on a shape
		/// </summary>
		/// <param name="shape"></param>
		private void HandleTextChanged(Shape shape)
        {
	        if (shape.Stat == (short) VisStatCodes.visStatDeleted)
	        {
		        return;
	        }
	        shape = shape.GetTopLevelShape();
			if (!_shapeFactory.ShapeUpdateInProgress)
	        {
		        var modellerShape = _shapeFactory.ConstructShape(shape, _modellerPresenter);
		        _modellerPresenter.UpdateModelObjectDescriptionFromShape(modellerShape);
		        _modellerPresenter.UpdateModelObjectExternalIdFromShape(modellerShape);
	        }
        }

        #endregion
    }
}