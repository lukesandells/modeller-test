﻿using Microsoft.Office.Interop.Visio;
using System.Collections.Generic;
using System.Windows;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///  Extensions to operate on Visio shapes
	/// </summary>
	public static class ShapeExtenions
	{
		/// <summary>
		///  The prefix used by visio for user calls
		/// </summary>
		private const string C_USER_CELL_PREFIX = "User.";

		/// <summary>
		/// Gets a given cell, creating it if it doesn't already exist
		/// </summary>
		/// <param name="shape">The shape to get the cell from</param>
		/// <param name="cellName">The identifier of the cell</param>
		/// <returns></returns>
		public static Cell GetOrCreateUserCell(this IVShape shape, string cellName)
		{
			// In VBA true = -1, false  = 0. Google it.
			if (shape.CellExistsU[cellName, 0] == 0)
			{
				shape.AddNamedRow((short)VisSectionIndices.visSectionUser, cellName.Replace(C_USER_CELL_PREFIX, ""), (short)tagVisRowTags.visTagDefault);
				shape.CellsU[cellName]
					.Formula = ""; // default formula is "0", which is no good 
			}
			return shape.CellsU[cellName];
		}

		/// <summary>
		/// Gets the top level shape of a shape i.e. the one that we're targeting within Modeller.
		/// This is needed because often events are triggered on the contained shape e.g. when text
		/// is changes it occurs on the text box within the shape rather than the shape itself.
		/// </summary>
		/// <param name="shape">The shape to find the top level shape for</param>
		/// <returns></returns>
		public static Shape GetTopLevelShape(this Shape shape)
		{
			while (true)
			{
				if (shape.ContainingShape == null || shape.CellExistsU["User.OccurrenceId", 0] == -1)
				{
					return shape;
				}
				shape = shape.ContainingShape;
			}
		}

		/// <summary>
		/// Wheather or not this shape contains another shape
		/// </summary>
		/// <param name="thisShape">The containing shape</param>
		/// <param name="containedShape">The contained shape</param>
		/// <returns></returns>
		public static bool Contains(this Shape thisShape,
			Shape containedShape)
		{
			return thisShape.BoundingRectangle().Contains(containedShape.BoundingRectangle());
		}

		/// <summary>
		/// Wheather or tnot this shapes is contained by another shape
		/// </summary>
		/// <param name="thisShape">The contained shape</param>
		/// <param name="containingShape">The containing shape</param>
		/// <returns></returns>
		public static bool IsContainedWithin(this Shape thisShape, Shape containingShape)
		{
			return containingShape.Contains(thisShape);
		}

		/// <summary>
		/// An enumerable of all shapes contained within this shape
		/// </summary>
		/// <param name="shape">The shape to get the contained shapes for</param>
		/// <returns></returns>
		public static IEnumerable<Shape> ContainedShapes(this Shape shape)
		{
			foreach (Shape candidate in shape.ContainingPage.Shapes)
			{
				if (candidate != shape && shape.Contains(candidate))
				{
					yield return candidate;
				}
			}
		}

		/// <summary>
		/// An enumerable of all shapes this shape is contained within
		/// </summary>
		/// <param name="shape">The shape to examine</param>
		/// <returns></returns>
		public static IEnumerable<Shape> ContainingShapes(this Shape shape)
		{
			foreach (Shape candidate in shape.ContainingPage.Shapes)
			{
				if (candidate != shape && candidate.Contains(shape))
				{
					yield return candidate;
				}
			}
		}

		/// <summary>
		/// Gets a Rectange of the bounds of this shape
		/// </summary>
		/// <param name="shape"></param>
		/// <returns></returns>
		public static Rect BoundingRectangle(this Shape shape)
		{
			shape.BoundingBox((short) VisBoundingBoxArgs.visBBoxUprightWH, out var left, out var bottom, out var right, out var top);
			return new Rect(left, bottom, right -left, top -bottom);
		}
	}
}
