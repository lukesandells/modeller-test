using MI.Modeller.Client.Presenters.Interfaces;
using Microsoft.Office.Interop.Visio;
using System;
using System.Collections.Generic;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	/// Represents a page within a document
	/// </summary>
	public class ModellerVisio2016Page: IModellerPage
	{
		/// <summary>
		/// The cell to store the page id in
		/// </summary>
		private const string C_SHAPE_ID_CELL = "User.ModellerPageId";

		/// <summary>
		/// The underlying Visio page
		/// </summary>
		private readonly Page _page;

		/// <summary>
		/// The modeller presenter
		/// </summary>
		private readonly IModellerPresenter _modellerPresenter;

		/// <summary>
		/// Constructs a new ModellerVisio2016Page
		/// </summary>
		/// <param name="page">The Visio Page to construct this from</param>
		/// <param name="modellerPresenter">The modeller presenter</param>
		public ModellerVisio2016Page(Page page, IModellerPresenter modellerPresenter)
		{
			_page = page;
			_modellerPresenter = modellerPresenter;
		}

		/// <summary>
		/// The internal ID of the page
		/// </summary>
		public Guid PageInternalId
		{
			get
			{
				if (string.IsNullOrEmpty(_page.PageSheet.GetOrCreateUserCell(C_SHAPE_ID_CELL).FormulaU))
				{
					PageInternalId = Guid.NewGuid();
				}
				return Guid.Parse(_page.PageSheet.CellsU[C_SHAPE_ID_CELL]
					.Formula.Replace("\"", ""));
			}
			set
			{
				_page.PageSheet.GetOrCreateUserCell(C_SHAPE_ID_CELL)
					.FormulaU = $"\"{value}\"";
			}
		}

		/// <summary>
		/// The page's title
		/// </summary>
		public string Title 
		{
			get => _page.Name;
			set => _page.Name = value;
		}

		/// <summary>
		/// Deletes the underlying page
		/// </summary>
		public void Delete()
		{
			_page.Delete(0);
		}

		/// <summary>
		/// The shapes on this page
		/// </summary>
		public IEnumerable<IModellerShape> Shapes
		{
			get
			{
				foreach (Shape s in _page.Shapes)
				{
					if (s.LineStyle != ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE)
					{
						yield return new Visio2016ModellerShape(s, _modellerPresenter);
					}
				}
			}
		}

		public IEnumerable<IModellerConnector> Connectors
		{
			get
			{
				foreach (Shape s in _page.Shapes)
				{
					if (s.LineStyle == ModellerVisio2016Connector.C_CONNECTOR_SHAPE_LINESTYLE)
					{
						yield return new ModellerVisio2016Connector(s, _modellerPresenter);
					}
				}
			}
		}

		/// <summary>
		/// The document this page is within
		/// </summary>
		public IModellerDocument Document => ModellerVisio2016Document.GetModellerVisio2016Document(_page.Document, _modellerPresenter);
	}
}