using MI.Modeller.Client.ViewInterfaces;
using Microsoft.Office.Interop.Visio;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using MI.Framework.Validation;

namespace MI.Modeller.Client.Visio2016
{
	/// <summary>
	///     Manages the list of all installed panels
	/// </summary>
	/// <seealso>
	///     <cref>"System.IDisposable"</cref>
	/// </seealso>
	public sealed class ModellerPanelManager : IDisposable
    {
        #region  Fields

        /// <summary>
        ///     The current panel frames created for this instance
        /// </summary>
        private readonly Dictionary<int, ModellerPanelFrame> _panelFrames = new Dictionary<int, ModellerPanelFrame>();

	    #endregion

        #region Constructors

        #endregion

        #region  Interface Implementations

        /// <summary>
        ///     Called when disposed
        /// </summary>
        public void Dispose()
        {
        }


        #endregion

        #region Members

        /// <summary>
        ///     Returns true if panel is opened in the given Visio diagram window.
        /// </summary>
        /// <param name="window">Visio diagram window</param>
        /// <returns><c>true</c> if [is panel opened] [the specified window]; otherwise, <c>false</c>.</returns>
        public bool IsPanelOpened(Window window)
        {
            return FindWindowPanelFrame(window) != null;
        }

	    /// <summary>
	    ///     Shows or hides panel for the given Visio window.
	    /// </summary>
	    /// <param name="window">BrowserNode Visio diagram window where to show/hide the panel</param>
	    /// <param name="panelview"></param>
	    public void TogglePanel(Window window, IPanelView panelview)
	    {
		    ArgumentValidation.AssertNotNull(panelview, nameof(panelview));
            if (window == null)
            {
                return;
            }

            var panelFrame = FindWindowPanelFrame(window);
            if (panelFrame == null)
            {
                panelFrame = new ModellerPanelFrame(panelview);
                panelview.TreeView.RefreshNodes();
                panelFrame.CreateWindow(window);

                panelFrame.PanelFrameClosed += OnPanelFrameClosed;
                _panelFrames.Add(window.ID,
                    panelFrame);
            }
            else
            {
                panelFrame.DestroyWindow();
                _panelFrames.Remove(window.ID);
            }
        }

        /// <summary>
        ///     Finds the ModellerPanelFrame associated with the current window
        /// </summary>
        /// <param name="window">The window the ModelPanelFrame is searched for</param>
        /// <returns>The current instance of the ModellerPanelFrame if one exists, returns null otherwise</returns>
        public ModellerPanelFrame FindWindowPanelFrame(Window window)
        {
            if (window == null)
            {
                return null;
            }

            return _panelFrames.ContainsKey(window.ID)
                ? _panelFrames[window.ID]
                : null;
        }

        /// <summary>
        /// Called when the frame is closed
        /// </summary>
        /// <param name="e">The e.</param>
        /// <param name="args">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void OnPanelFrameClosed(object e, EventArgs args)
        {
            var window = e as Window;
            if (window != null)
            {
                _panelFrames.Remove(window.ID);
            }
        }

        #endregion
    }
}