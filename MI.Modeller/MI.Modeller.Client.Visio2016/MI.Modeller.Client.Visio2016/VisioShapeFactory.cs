﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Microsoft.Office.Interop.Visio;
using MI.Modeller.Client.MasterShapes;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Client.Visio2016.Resources;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;

namespace MI.Modeller.Client.Visio2016
{
    /// <summary>
    ///     Visio 2016 implementation of the shape factory
    /// </summary>
    /// <seealso>
    ///     <cref>"MI.Modeller.Client.ShapeMappers.ShapeFactory"</cref>
    /// </seealso>
    internal class VisioShapeFactory : ShapeFactory
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The stencils dictionary
        /// </summary>
        private static readonly IDictionary<ModelType, string> _stencilsDictionary = new Dictionary
            <ModelType, string>
            {
                {ModelType.WorkDefinitionModel, @"Work Definition.vssx"},
                {ModelType.OperationsDefinitionModel, @"Operations Definition.vssx"},
                {ModelType.SalesAndOperationsPlanningModel, @"Operations Definition.vssx"},
                {ModelType.Unknown, @"Modeller.vssx"}
            };

		private static readonly IDictionary<Guid, MasterShape> _mastersDictionary = new Dictionary<Guid, MasterShape>
		{
			{ NodeTypeId.HierarchyScope, MasterShape.HierarchyScopeMasterShape },
			{ NodeTypeId.Equipment, MasterShape.EquipmentMasterShape },
			{ NodeTypeId.EquipmentClass, MasterShape.EquipmentClassMasterShape },
			{ NodeTypeId.PhysicalAssetClass, MasterShape.PhysicalAsssetClassMasterShape },
			{ NodeTypeId.PersonnelClass, MasterShape.PersonnelClassMasterShape },
			{ NodeTypeId.MaterialDefinition, MasterShape.MaterialDefinitionMasterShape },
			{ NodeTypeId.MaterialClass, MasterShape.MaterialClassMasterShape },

			{ NodeTypeId.OperationsDefinition, MasterShape.OperationsDefinitionMasterShape },
			{ NodeTypeId.OperationsSegment, MasterShape.OperationsSegmentMasterShape },
			{ NodeTypeId.ProcessSegment, MasterShape.ProcessSegmentMasterShape },
			{ NodeTypeId.WorkMaster, MasterShape.WorkMasterMasterShape },
		};

        #endregion

        #region Members

        private IDictionary<Guid, IModellerShape> existingShapes = new Dictionary<Guid, IModellerShape>();

	    public IModellerShape ConstructShape(Shape shape,
            IModellerPresenter modellerPresenter)
        {
            //var shapeId = Guid.Parse(shape.UniqueID[(short) VisUniqueIDArgs.visGetOrMakeGUID]);
            //if (existingShapes.Any(s => !s.Value.IsDeleted && s.Key == shapeId))
            //{
            //    return
            //        existingShapes.Single(
            //            s => !s.Value.IsDeleted && s.Key == shapeId).Value;
            //}
            var modellerShape = new Visio2016ModellerShape(shape, modellerPresenter);
            //existingShapes.Add(shapeId, modellerShape);
            return modellerShape;
        }

        /// <summary>
        ///     Creates the Shape
        /// </summary>
        /// <param name="modelElement">The object to create a shape for</param>
        /// <param name="modellerPresenter"></param>
        /// <returns>MI.Modeller.Client.IModellerShape.</returns>
        public override IModellerShape CreateNewShapeOffPage(ModelElement modelElement,
            IModellerPresenter modellerPresenter)
        {
			var masterToDrag = GetMaster(modelElement,
                modellerPresenter);
	        if (masterToDrag == null)
	        {
				return null;
	        }

            var position = GetOffPagePosition();
            var shape = Globals.ModellerAddin.Application.ActivePage.Drop(masterToDrag,
                position.X,
                position.Y);
            //PopulateShape(shape,
            //    modelElement);
			var shapeId = Guid.Parse(shape.UniqueID[(short)VisUniqueIDArgs.visGetOrMakeGUID]);
            if (existingShapes.Any(s => !s.Value.IsDeleted && s.Key == shapeId))
            {
                return
                    existingShapes.Single(
                        s => !s.Value.IsDeleted && s.Key == shapeId).Value;
            }
            var newShape = new Visio2016ModellerShape(shape,
                modellerPresenter);
            existingShapes.Add(shapeId,newShape);
            return newShape;
        }

        /// <summary>
        ///     Gets the master for a specific object
        /// </summary>
        /// <param name="modelElement">The modeller object.</param>
        /// <param name="modellerPresenter"></param>
        /// <returns>Master.</returns>
        /// <exception cref="Exception"></exception>
        public Master GetMaster(ModelElement modelElement,
            IModellerPresenter modellerPresenter)
        {
            var app = Globals.ModellerAddin.Application;

            var stencilName = _stencilsDictionary.FirstOrDefault(
                                      e =>
                                          e.Key ==
                                          ModellerVisio2016Document.GetModellerVisio2016Document(app.ActiveDocument,
                                                  modellerPresenter)
                                              ?.ModelType)
                                  .Value ?? _stencilsDictionary[ModelType.Unknown];

            

            var master = GetShapeMaster(modelElement,
                GetStencil(stencilName));
	        return master ?? GetShapeMaster(modelElement, GetStencil(_stencilsDictionary[ModelType.Unknown]));
        }

		/// <summary>
		/// Gets or opens a stencil
		/// </summary>
		/// <param name="stencilName">The name of the stencil</param>
		/// <returns></returns>
	    private static Document GetStencil(string stencilName)
	    {
		    var app = Globals.ModellerAddin.Application;
			Document stencil = null;
		    try
		    {
			    stencil = app.Documents[stencilName];
		    }
		    catch (Exception)
		    {
			    //Document likely not open
		    }
		    if (stencil == null)
		    {
			    var path = $"{stencilName}";

			    try
			    {
				    stencil = app.Documents.OpenEx(stencilName, (short)VisOpenSaveArgs.visOpenDocked | (short)VisOpenSaveArgs.visOpenRO);
			    }
			    catch (Exception ex)
			    {
				    throw new Exception(Messages.UnableToFindStencilPath + " - " + path, ex);
			    }
		    }
		    if (stencil == null)
		    {
			    throw new Exception(Messages.UnableToFindStencilPath);
		    }
		    return stencil;
	    }

        /// <summary>
        ///     Gets the off page position.
        /// </summary>
        /// <returns>Point.</returns>
        private static Point GetOffPagePosition()
        {
            double viewLeft;
            double viewTop;
            double viewWidth;
            double viewHeight;
            Globals.ModellerAddin.Application.ActiveWindow.GetViewRect(out viewLeft,
                out viewTop,
                out viewWidth,
                out viewHeight);

            double boundingBoxLeft;
            double boundingBoxBottom;
            double boundingBoxRight;
            double boundingBoxTop;
            Globals.ModellerAddin.Application.ActivePage.BoundingBox(
                (short) VisBoundingBoxArgs.visBBoxIncludeDataGraphics,
                out boundingBoxLeft,
                out boundingBoxBottom,
                out boundingBoxRight,
                out boundingBoxTop);

            return new Point((int) (viewLeft > boundingBoxLeft
                    ? viewLeft - 1000
                    : boundingBoxLeft - 1000),
                (int) (viewTop > boundingBoxTop
                    ? viewTop + 1000
                    : boundingBoxTop + 1000));
        }

		/// <summary>
		///     Gets the shape master.
		/// </summary>
		/// <param name="modelElement">The modeller element.</param>
		/// <param name="stencil">The stencil.</param>
		/// <returns>Master.</returns>
		/// <exception cref="NotImplementedException"></exception>
		private static Master GetShapeMaster(ModelElement modelElement,
            IVDocument stencil)
        {
	        if (_mastersDictionary.ContainsKey(modelElement.TypeId))
	        {
		        try
		        {
			        return stencil.Masters[_mastersDictionary[modelElement.TypeId].Name];
		        }
		        catch (Exception)
		        {
			        // Ignored. No method to check if it exists first.
			        return null;
		        }
	        }
	        throw new NotImplementedException();
			
        }

        #endregion
    }
}