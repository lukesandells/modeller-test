﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	/// <summary>
	/// Internal class for handle to resources
	/// </summary>
	internal class MaterialSpecificationUserControl { }

	partial class MaterialSpecificationUserControl<TOfProcess>  where TOfProcess : class, IProcessDefinition<TOfProcess>
	{
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
	        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
	        Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewBrowseColumn gridViewBrowseColumn4 = new Telerik.WinControls.UI.GridViewBrowseColumn();
			Telerik.WinControls.UI.GridViewComboBoxColumn gridViewComboBoxColumn4 = new Telerik.WinControls.UI.GridViewComboBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.Data.SortDescriptor sortDescriptor4 = new Telerik.WinControls.Data.SortDescriptor();
			Telerik.WinControls.UI.TableViewDefinition tableViewDefinition4 = new Telerik.WinControls.UI.TableViewDefinition();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MaterialSpecificationUserControl<TOfProcess>));
			this.office2013LightTheme1 = new Telerik.WinControls.Themes.Office2013LightTheme();
			this.radGridView1 = new Telerik.WinControls.UI.RadGridView();
			this.radMenu1 = new Telerik.WinControls.UI.RadMenu();
			this.AddAboveMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.AddBelowMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.AddChildMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.DeleteMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.MoveMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.MaterialBillMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.radMenuItem3 = new Telerik.WinControls.UI.RadMenuItem();
			this.radMenuItem4 = new Telerik.WinControls.UI.RadMenuItem();
			((System.ComponentModel.ISupportInitialize)(this.radGridView1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.radMenu1)).BeginInit();
			this.SuspendLayout();
			// 
			// radGridView1
			// 
			this.radGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.radGridView1.BackColor = System.Drawing.SystemColors.Control;
			this.radGridView1.Cursor = System.Windows.Forms.Cursors.Default;
			this.radGridView1.Font = new System.Drawing.Font("Segoe UI", 9F);
			this.radGridView1.ForeColor = System.Drawing.SystemColors.ControlText;
			this.radGridView1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.radGridView1.Location = new System.Drawing.Point(0, 23);
			this.radGridView1.Margin = new System.Windows.Forms.Padding(1);
			// 
			// 
			// 
	        gridViewTextBoxColumn1.EnableExpressionEditor = false;
	        gridViewTextBoxColumn1.HeaderText = "ParentId";
	        gridViewTextBoxColumn1.Name = "ParentId";
	        gridViewTextBoxColumn1.Width = 20;
	        gridViewTextBoxColumn1.FieldName = "Key.ParentNode.Id";
	        gridViewTextBoxColumn1.IsVisible = false;
			gridViewTextBoxColumn2.EnableExpressionEditor = false;
	        gridViewTextBoxColumn2.HeaderText = "InternalId";
	        gridViewTextBoxColumn2.Name = "InternalId";
	        gridViewTextBoxColumn2.Width = 20;
	        gridViewTextBoxColumn2.FieldName = "Key.Id";
	        gridViewTextBoxColumn2.IsVisible = false;
			gridViewTextBoxColumn13.EnableExpressionEditor = false;
	        gridViewTextBoxColumn13.HeaderText = "ID";
	        gridViewTextBoxColumn13.Name = "IdColumn";
	        gridViewTextBoxColumn13.Width = 20;
	        gridViewTextBoxColumn13.FieldName = "Key.DisplayId";
			gridViewBrowseColumn4.EnableExpressionEditor = false;
			gridViewBrowseColumn4.HeaderText = "Hierarchy Scope";
			gridViewBrowseColumn4.Name = "HierarchyScopeColumn";
			gridViewBrowseColumn4.Width = 97;
	        gridViewBrowseColumn4.FieldName = "Value.HierarchyScope";
			gridViewComboBoxColumn4.EnableExpressionEditor = false;
			gridViewComboBoxColumn4.HeaderText = "Material Use";
			gridViewComboBoxColumn4.Name = "MaterialUseColumn";
			gridViewComboBoxColumn4.Width = 77;
	        gridViewTextBoxColumn15.FieldName = "Value.MaterialUse";
			gridViewTextBoxColumn14.EnableExpressionEditor = false;
			gridViewTextBoxColumn14.HeaderText = "Material Class";
			gridViewTextBoxColumn14.Name = "MaterialClassColumn";
			gridViewTextBoxColumn14.Width = 84;
			gridViewTextBoxColumn15.EnableExpressionEditor = false;
			gridViewTextBoxColumn15.HeaderText = "Material Definition";
			gridViewTextBoxColumn15.Name = "MaterialDefinitionColumn";
			gridViewTextBoxColumn15.Width = 110;
			gridViewTextBoxColumn16.EnableExpressionEditor = false;
			gridViewTextBoxColumn16.HeaderText = "Quantity";
			gridViewTextBoxColumn16.Name = "QuantityColumn";
			gridViewTextBoxColumn16.Width = 55;
			this.radGridView1.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
			gridViewTextBoxColumn1,
			gridViewTextBoxColumn2,
			gridViewTextBoxColumn13,
            gridViewBrowseColumn4,
            gridViewComboBoxColumn4,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16});
			this.radGridView1.MasterTemplate.EnableGrouping = false;
			sortDescriptor4.PropertyName = "ID";
			this.radGridView1.MasterTemplate.SortDescriptors.AddRange(new Telerik.WinControls.Data.SortDescriptor[] {
            sortDescriptor4});
			this.radGridView1.MasterTemplate.ViewDefinition = tableViewDefinition4;
			this.radGridView1.Name = "radGridView1";
			this.radGridView1.RightToLeft = System.Windows.Forms.RightToLeft.No;
			// 
			// 
			// 
			this.radGridView1.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 240, 150);
			this.radGridView1.Size = new System.Drawing.Size(1298, 595);
			this.radGridView1.TabIndex = 4;
			this.radGridView1.Text = "radGridView1";
			this.radGridView1.ThemeName = "Office2013Light";
			// 
			// radMenu1
			// 
			this.radMenu1.BackColor = System.Drawing.SystemColors.ControlLight;
			this.radMenu1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.AddAboveMenuItem,
            this.AddBelowMenuItem,
            this.AddChildMenuItem,
            this.DeleteMenuItem,
            this.MoveMenuItem,
            this.MaterialBillMenuItem});
			this.radMenu1.Location = new System.Drawing.Point(0, 0);
			this.radMenu1.Name = "radMenu1";
			// 
			// 
			// 
			this.radMenu1.RootElement.ControlBounds = new System.Drawing.Rectangle(0, 0, 100, 24);
			this.radMenu1.Size = new System.Drawing.Size(562, 19);
			this.radMenu1.TabIndex = 5;
			this.radMenu1.ThemeName = "Office2013Light";
			// 
			// AddAboveMenuItem
			// 
			this.AddAboveMenuItem.Image = Properties.Resources.AddAboveMenuItem_Image;
			this.AddAboveMenuItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.AddAboveMenuItem.Name = "AddAboveMenuItem";
			this.AddAboveMenuItem.Text = "";
			this.AddAboveMenuItem.ToolTipText = "Add Above";
			// 
			// AddBelowMenuItem
			// 
	        this.AddBelowMenuItem.Image = Properties.Resources.AddBelowMenuItem_Image;
			this.AddBelowMenuItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.AddBelowMenuItem.Name = "AddBelowMenuItem";
			this.AddBelowMenuItem.Text = "";
			this.AddBelowMenuItem.ToolTipText = "Add Below";
			// 
			// AddChildMenuItem
			// 
			this.AddChildMenuItem.Image = this.AddBelowMenuItem.Image = Properties.Resources.AddChildMenuItem_Image;
			this.AddChildMenuItem.Name = "AddChildMenuItem";
			this.AddChildMenuItem.Text = "";
			this.AddChildMenuItem.ToolTipText = "Add Child";
			// 
			// DeleteMenuItem
			// 
			this.DeleteMenuItem.Image = this.AddBelowMenuItem.Image = Properties.Resources.DeleteMenuItem_Image;
			this.DeleteMenuItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.DeleteMenuItem.Name = "DeleteMenuItem";
			this.DeleteMenuItem.Text = "";
			// 
			// MoveMenuItem
			// 
			this.MoveMenuItem.Image = this.AddBelowMenuItem.Image = Properties.Resources.MoveMenuItem_Image;
			this.MoveMenuItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.MoveMenuItem.Name = "MoveMenuItem";
			this.MoveMenuItem.Text = "";
			// 
			// MaterialBillMenuItem
			// 
			this.MaterialBillMenuItem.Image = this.AddBelowMenuItem.Image = Properties.Resources.MaterialBillMenuItem_Image;
			this.MaterialBillMenuItem.ImageAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			this.MaterialBillMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radMenuItem3,
            this.radMenuItem4});
			this.MaterialBillMenuItem.Name = "MaterialBillMenuItem";
			this.MaterialBillMenuItem.Text = "";
			// 
			// radMenuItem3
			// 
			this.radMenuItem3.Name = "radMenuItem3";
			this.radMenuItem3.Text = "radMenuItem3";
			// 
			// radMenuItem4
			// 
			this.radMenuItem4.Name = "radMenuItem4";
			this.radMenuItem4.Text = "radMenuItem4";
			// 
			// MaterialSpecificationUserControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.radMenu1);
			this.Controls.Add(this.radGridView1);
			this.Margin = new System.Windows.Forms.Padding(1);
			this.Name = "MaterialSpecificationUserControl";
			this.Size = new System.Drawing.Size(562, 186);
			((System.ComponentModel.ISupportInitialize)(this.radGridView1.MasterTemplate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.radGridView1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.radMenu1)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private Telerik.WinControls.Themes.Office2013LightTheme office2013LightTheme1;
        private Telerik.WinControls.UI.RadGridView radGridView1;
        private Telerik.WinControls.UI.RadMenu radMenu1;
        private Telerik.WinControls.UI.RadMenuItem AddAboveMenuItem;
        private Telerik.WinControls.UI.RadMenuItem AddBelowMenuItem;
        private Telerik.WinControls.UI.RadMenuItem AddChildMenuItem;
        private Telerik.WinControls.UI.RadMenuItem DeleteMenuItem;
        private Telerik.WinControls.UI.RadMenuItem MoveMenuItem;
        private Telerik.WinControls.UI.RadMenuItem MaterialBillMenuItem;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem3;
        private Telerik.WinControls.UI.RadMenuItem radMenuItem4;
    }
}
