﻿using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using System.Collections.Generic;
using MI.Modeller.Client.Presenters.Interfaces;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	/// <summary>
	///     Class QuantitySpecificResourceTransactionPropertiesUserControl.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.UserControls.AbstractPropertiesUserControl"</cref>
	/// </seealso>
	public class QuantitySpecificResourceTransactionPropertiesUserControl<TOfObject> : AbstractPropertiesUserControl<QuantitySpecificResourceTransactionProperty<TOfObject>, TOfObject>
		where TOfObject : class, IResourceTransaction, IWithProperties<TOfObject, QuantitySpecificResourceTransactionProperty<TOfObject>>, IWithExternalId
	{
		#region Constructors

		//public QuantitySpecificResourceTransactionPropertiesUserControl(object p, List<DisplayProperty> properties, Dictionary<string, ISet<DisplayProperty>> referenceProperties, string v1, string referenceId, FormAction formAction, bool v2)
		//{
		//}
		public QuantitySpecificResourceTransactionPropertiesUserControl() { }

		public QuantitySpecificResourceTransactionPropertiesUserControl(object p,
			List<QuantitySpecificResourceTransactionProperty<TOfObject>> equipmentProperties,
			IDictionary<string, ISet<QuantitySpecificResourceTransactionProperty<TOfObject>>> classProperties,
			string v1,
			string v,
			FormAction formAction)
		{ }

		#endregion

		protected override IPropertyTabPresenter<QuantitySpecificResourceTransactionProperty<TOfObject>, TOfObject> Presenter { get; }
	}
}