﻿using System.Collections.Generic;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	/// <summary>
	///     Custom editor for ModelObjectAttributes which are a drop down type
	/// </summary>
	public sealed class ModellerPropertyGridDropDownEditor : PropertyGridDropDownListEditor
    {
        #region  Fields

	    /// <summary>
	    ///     Wheather or not the drop down options are fixed
	    /// </summary>
	    private readonly bool _dropDownOptionsFixed = false;

        #endregion

        #region Constructors

        /// <summary>
        ///     Create a new ModellerPropertyGridDropDownEditor
        /// </summary>
        /// <param name="attribute">The attribute this editor is for</param>
        public ModellerPropertyGridDropDownEditor(ModellerPropertyStoreItem attribute)
        {
            var editorElement = EditorElement as BaseDropDownListEditorElement;
            if (editorElement != null)
            {
                editorElement.DataSource = new HashSet<string>(attribute.DropDownOptions);
            }
        }

        #endregion

        #region Members

        /// <summary>
        ///     Called when edit is begin. Used to set the correct drop down list behaviour
        /// </summary>
        public override void BeginEdit()
        {
            var editorElement = EditorElement as BaseDropDownListEditorElement;
            if (editorElement != null)
            {
                editorElement.DropDownStyle = _dropDownOptionsFixed
                    ? RadDropDownStyle.DropDownList
                    : RadDropDownStyle.DropDown;
            }
            base.BeginEdit();
        }

        /// <summary>
        ///     Called at the end of the edit. If the value is user entered that is it added to the source list
        /// </summary>
        /// <returns></returns>
        public override bool EndEdit()
        {
            var editorElement = EditorElement as BaseDropDownListEditorElement;
            if (editorElement != null)
            {
                var sourceList = editorElement.DataSource as List<string>;
                if (!sourceList?.Contains((string) editorElement.Value) ?? false)
                {
                    sourceList?.Add((string) editorElement.Value);
                }
            }
            return base.EndEdit();
        }

        #endregion
    }
}