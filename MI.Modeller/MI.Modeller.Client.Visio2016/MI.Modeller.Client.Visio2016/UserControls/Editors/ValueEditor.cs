﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using System;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	/// <summary>
	/// Editor for editing <see cref="OperationsManagement.Domain.Core.DataTypes.Value"/>s
	/// </summary>
	public class ValueEditor: BaseGridEditor
	{
		/// <summary>
		/// Validates this control and sets the value
		/// </summary>
		/// <returns></returns>
		public bool ValidateAndSetValue()
		{
			var result = ((ValueEditorElement)EditorElement).ValidateAndSetValue(out var message);
			if (!string.IsNullOrEmpty(message))
			{
				MessageBox.Show(message);
			}
			else
			{
				OnValueChanged();
			}
			return result;
		}

		/// <summary>
		/// The <see cref="OperationsManagement.Domain.Core.DataTypes.Value"/> for this editor
		/// </summary>
		public override object Value
		{
			get => ((ValueEditorElement) EditorElement).Value;
			set => ((ValueEditorElement)EditorElement).Value = value as Value;
		}

		/// <summary>
		/// Creates the edtitor element
		/// </summary>
		/// <returns></returns>
		protected override RadElement CreateEditorElement()
		{
			return new ValueEditorElement();
		}

		/// <summary>
		/// The type of data being provided by this editor
		/// </summary>
		public override Type DataType => typeof(Value);
	}
}
