﻿using MI.Framework.Validation;
using System.ComponentModel;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	/// <summary>
	/// The default grid view combo box column doesn't allow new values. This editor will 
	/// automatically add the element to the backing list if it's not in the list
	/// </summary>
	public class ExtendedDropDownListEditor: RadDropDownListEditor
	{
		/// <summary>
		/// The binding list that backs the column
		/// </summary>
		private readonly BindingList<string> _bindingList;

		/// <summary>
		/// Constructs a new ExtendedDropDownListEditor
		/// </summary>
		/// <param name="bindingList">The binding list that backs the column</param>
		public ExtendedDropDownListEditor(BindingList<string> bindingList)
		{
			_bindingList = ArgumentValidation.AssertNotNull(bindingList, nameof(bindingList));
		}

		/// <summary>
		/// Ends the edit and in the process adds a new value to the backing list if required
		/// </summary>
		/// <returns></returns>
		public override bool EndEdit()
		{
			var text = ((RadDropDownListEditorElement) EditorElement).Text;
			if (!string.IsNullOrEmpty(text) && !_bindingList.Contains(text))
			{
				_bindingList.Add(text);
				
			}
			GridComboBoxCellElement cellElement = (GridComboBoxCellElement) OwnerElement;
			cellElement.Value = text;
			return base.EndEdit();
		}

		/// <summary>
		/// Begin edit and in the process add the value to the list if required
		/// </summary>
		public override void BeginEdit()
		{
			GridComboBoxCellElement cellElement = (GridComboBoxCellElement) OwnerElement;
			if (!string.IsNullOrEmpty(cellElement.Value as string) && !_bindingList.Contains((string) cellElement.Value))
			{
				_bindingList.Add((string) cellElement.Value);

			}
			base.BeginEdit();
		}

	}
}
