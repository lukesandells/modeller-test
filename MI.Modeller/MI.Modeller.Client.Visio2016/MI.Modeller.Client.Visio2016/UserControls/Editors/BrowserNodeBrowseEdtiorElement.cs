﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.Visio2016.Forms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	internal class BrowserNodeBrowseEdtiorElement : RadEditorElement
	{
		#region  Fields

		/// <summary>
		///     The layout stack which holds the other elements
		/// </summary>
		private StackLayoutElement _layoutStack;

		/// <summary>
		///     The browse button which will trigger a popup for selecting a browser node
		/// </summary>
		private BrowseEditorButton _browseButton;

		/// <summary>
		///     The autocomplete element which will allow the user to type a node name to select
		/// </summary>
		private RadDropDownListElement _autoCompleteElement;

		/// <summary>
		///     The tree filter to apply to the object browser
		/// </summary>
		private readonly ModelElementTreeFilter _modelElementTreeFilter;

		/// <summary>
		///     Backing field for the model object that has been selected
		/// </summary>
		private BrowserNode _browserNode;

		/// <summary>
		///		The modeller presenter to interact with
		/// </summary>
		private readonly IModellerPresenter _modellerPresenter;

		/// <summary>
		/// The root nodes for the object browser
		/// </summary>
		private readonly IEnumerable<Guid> _rootNodeIds;

		/// <summary>
		/// Indicates that the value is currently changing, this will supress the trigger of the auto complete text changing
		/// </summary>
		private bool _valueChanging = false;

		/// <summary>
		///     The model object that has been selected
		/// </summary>
		public BrowserNode Value
		{
			get => _browserNode;
			set
			{
				_valueChanging = true;
				_browserNode = value;
				_autoCompleteElement.Text = value?.DisplayId;
				_valueChanging = false;
			}
		}

		#endregion

		#region Constructors

		/// <summary>
		///     Constructs a new BrowserNodeBrowseEdtiorElement
		/// </summary>
		/// <param name="modellerPresenter">The modeller presenter to use</param>
		/// <param name="rootNodeIds">The root nodes to apply to the object browser</param>
		/// <param name="modelElementTreeFilter">The tree filter to apply to the object browser</param>
		public BrowserNodeBrowseEdtiorElement(IModellerPresenter modellerPresenter, IEnumerable<Guid> rootNodeIds, ModelElementTreeFilter modelElementTreeFilter = null)
		{
			_modellerPresenter = ArgumentValidation.AssertNotNull(modellerPresenter, nameof(modellerPresenter));
			_rootNodeIds = ArgumentValidation.AssertNotNull(rootNodeIds, nameof(rootNodeIds));
			_modelElementTreeFilter = modelElementTreeFilter ?? new ModelElementTreeFilter(null);
		}

		#endregion

		#region Other Methods

		/// <summary>
		/// Sets up the layout stack with the appropriate elements
		/// </summary>
		protected override void CreateChildElements()
		{
			// The stack to add elements to
			_layoutStack = new StackLayoutElement {StretchHorizontally = true, StretchVertically = true, FitInAvailableSize = true};
			Children.Add(_layoutStack);

			// Create the autocomplete element
			_autoCompleteElement = new RadDropDownListElement {Enabled = true, AutoCompleteMode = AutoCompleteMode.SuggestAppend, CaseSensitive = false};
			_autoCompleteElement.TextBox.TextChanged += HandleAutoCompleteTextChanged;
			_autoCompleteElement.ValueChanged += HandleAutoCompleteValueChanged;
			_layoutStack.Children.Add(_autoCompleteElement);

			// Create the browser element
			_browseButton = new BrowseEditorButton();
			_browseButton.Click += HandleBrowserButtonClick;
			_layoutStack.Children.Add(_browseButton);
		}

		/// <summary>
		/// When the selected value in the autocomplete changes we need to set this editor's value
		/// </summary>
		private void HandleAutoCompleteValueChanged(object sender, EventArgs e)
		{
			if (!_valueChanging)
			{
				Value = (BrowserNode) _autoCompleteElement.SelectedValue;
			}
		}

		/// <summary>
		/// When the user starts typing we need to update the backing store
		/// </summary>
		private void HandleAutoCompleteTextChanged(object sender, EventArgs e)
		{
			_autoCompleteElement.ListElement.BeginUpdate();
			_autoCompleteElement.Items.Clear();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// and work back to the nodes they reffer to
				var candidateNodes = 
					_modelElementTreeFilter.SelectableNodes()
					.Where(n => n.DisplayId.StartsWith(_autoCompleteElement.TextBox.Text))
					.OrderBy(n => n.DisplayId).Take(10).ToArray();

				// If we get results populate our list
				if (candidateNodes.Any())
				{
					foreach (var node in candidateNodes)
					{
						var newItem = new RadListDataItem(node.DisplayId)
						{
							Tag = node.DisplayId,
							Value = node
						};
						_autoCompleteElement.Items.Add(newItem);
					}
				}
				scope.Done();
			}
			_autoCompleteElement.ListElement.EndUpdate();
		}

		#endregion

		#region Members

		/// <summary>
		///     Shows the dialog to browse for an object
		/// </summary>
		/// <param name="sender">The sending object</param>
		/// <param name="e">Event arguments</param>
		protected void HandleBrowserButtonClick(object sender, EventArgs e)
		{
			var form = new TreeViewForm(
			_rootNodeIds,
			_modelElementTreeFilter,
			_modellerPresenter);
			if (form.ShowDialog() == ModellerDialogResult.OK)
			{
				using (var scope = new PersistenceScope())
				{
				Value = scope.FindById<BrowserNode>(form.SelectedNodeId);
					scope.Done();
				}
			}	
		}

		#endregion
	}
}