﻿using MI.Framework;
using MI.Framework.Units;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.Visio2016.Forms;
using MI.OperationsManagement.Domain.Core.DataTypes;
using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	/// <summary>
	/// Value editor element extending the telerik control which handles the editing of a 
	/// </summary>
	public class ValueEditorElement : LightVisualElement
	{
		/// <summary>
		/// The text box element used to manage the value
		/// </summary>
		private RadTextBoxElement _valueTextBoxElement;

		/// <summary>
		/// The drop down element to manage the data type
		/// </summary>
		private RadDropDownListElement _dataTypeDropDownElement;

		/// <summary>
		/// The button to allow for editing further details of the value
		/// </summary>
		private RadButtonElement _detailsButtonElement;

		/// <summary>
		/// The value being edited
		/// </summary>
		public Value Value
		{
			get => _value;
			set
			{
				_value = value;
				// Single values will populate the text box and drop down
				if (value != null && (value is SingleValue || value.Elements.Count() == 1))
				{
					_valueTextBoxElement.Enabled = true;
					_dataTypeDropDownElement.Enabled = true;
					var deserialisedValue = value.Serialise().Elements.First();
					_valueTextBoxElement.Text = deserialisedValue.Value + 
						(deserialisedValue.UnitCode != null
						? UnitLibrary.Current.UnitsByCode[deserialisedValue.UnitCode].Symbol
						:"");
					_dataTypeDropDownElement.SelectedItem = _dataTypeDropDownElement.Items.SingleOrDefault(i => i.Text == deserialisedValue.DataType);
				}
				// Complex values will populate only the text box with the current ToString and disable editing othter than via the details editor
				else if (value?.Elements.Count() > 1)
				{
					_valueTextBoxElement.Enabled = false;
					_dataTypeDropDownElement.Enabled = false;
					_valueTextBoxElement.Text = value.ToString();
				}
				else
				{
					_valueTextBoxElement.Enabled = true;
					_dataTypeDropDownElement.Enabled = true;
					_valueTextBoxElement.Text = null;
					_dataTypeDropDownElement.Value = null;
				}
			}
		}
		private Value _value;

		/// <summary>
		/// Override the tooptip to show the full value text
		/// </summary>
		public override string ToolTipText => Value?.ToString();

		/// <summary>
		/// Initialize and add the elements to the cell
		/// </summary>
		protected override void CreateChildElements()
		{
			base.CreateChildElements();
			// Create the text box editor and hook into the value changed event to update the value
			_valueTextBoxElement = new RadTextBoxElement();
			Children.Add(_valueTextBoxElement);

			// Create the drop down editor and hook into the value changed event to update the value
			_dataTypeDropDownElement = new RadDropDownListElement();
			foreach (var valueDataType in ((ValueDataType[])Enum.GetValues(typeof(ValueDataType))).Where(v => v.IsSupported()))
			{
				_dataTypeDropDownElement.Items.Add(valueDataType.ToString());
			}
			Children.Add(_dataTypeDropDownElement);
			_dataTypeDropDownElement.DropDownStyle = RadDropDownStyle.DropDownList;

			// Create the button and hook into the click event to show the details 
			_detailsButtonElement = new ActionButtonElement()
			{
				Text = "..."
			};
			_detailsButtonElement.Click += HandleDetailsButtonClick;
			Children.Add(_detailsButtonElement);
		}

		/// <summary>
		/// Displays the details form as a dialog and update the result
		/// </summary>
		private void HandleDetailsButtonClick(object sender, EventArgs e)
		{
			var form = new SimpleForm
			{
				Title = "Editing Values"
			};
			var userControl = new ValueDetailUserControl(Value);
			form.OkClicked += (_, args) => args.Cancel = !userControl.IsValid;
			form.SetUserControl(userControl);
			if (form.ShowDialog() == ModellerDialogResult.OK)
			{
				Value = userControl.Value;
			}
			form.Dispose();
		}

		/// <summary>
		/// Updates the underlying value with a single value when any element has it's value changed
		/// </summary>
		public bool ValidateAndSetValue(out string validationMessage)
		{
			validationMessage = null;
			// If this isn't enabled (i.e. because it's a multi value) or the use hasn't filled in all fields then return
			if (!_valueTextBoxElement.Enabled || (string.IsNullOrEmpty(_valueTextBoxElement.Text) && string.IsNullOrEmpty(_dataTypeDropDownElement.Value?.ToString())))
			{
				return true;
			}
			var newValue = new SerialisedValue();

			var valueText = _valueTextBoxElement.Text;
			string unitCode = null;
			string key = _value?.Elements.FirstOrDefault()?.Key;
			if (_dataTypeDropDownElement.SelectedItem?.Text != null)
			{
				var dataType = ExtensibleEnum<ValueDataType>.Parse(_dataTypeDropDownElement.SelectedItem?.Text);
				if (dataType == ValueDataType.Quantity || dataType == ValueDataType.Measure)
				{
					UnitDecimal valueAsValue;
					try
					{
						valueAsValue = UnitDecimal.Parse(valueText);
					}
					catch (Exception ex)
					{
						validationMessage = ex.Message;
						return false;
					}
					valueText = valueAsValue.DecimalValue.ToString(CultureInfo.CurrentCulture);
					unitCode = valueAsValue.Unit.Code;
					key = key ?? valueAsValue.Unit.PhysicalProperty.Name;
				}
			}
			newValue.Elements.Add(new SerialisedValue.Element
			{
				Value = valueText,
				DataType = _dataTypeDropDownElement.SelectedItem?.Text,
				UnitCode = unitCode,
				Key = key
			});

			try
			{
				Value = newValue.Deserialise();
			}
			catch (Exception ex)
			{
				validationMessage = ex.Message;
				return false;
			}
			return true;
		}

		/// <summary>
		/// Override the ArrangeOverride method to arrange the children elements of the cell
		/// </summary>
		/// <param name="finalSize"></param>
		/// <returns></returns>
		protected override SizeF ArrangeOverride(SizeF finalSize)
		{
			// Calculate Widths
			var buttonWidth = _detailsButtonElement.DesiredSize.Width;
			var dropDownWidth = finalSize.Width - 150 - 8 <= 0 ? 75 : 150;
			var textBoxWidth = finalSize.Width - buttonWidth - dropDownWidth - 8;// -8 for margins;

			RectangleF clientRect = GetClientRectangle(finalSize);
			// Button rect aligned to right hand side
			RectangleF buttonRect = new RectangleF(clientRect.Right - buttonWidth, clientRect.Top, buttonWidth, clientRect.Height);
			// Align to right, taking into account the button
			RectangleF dropdownRect = new RectangleF(clientRect.Right - buttonWidth - dropDownWidth - 2, clientRect.Top, dropDownWidth, clientRect.Height);
			// Text rect aligned to left hand side, width needs to reduce by the left of the drop down
			RectangleF textRect = new RectangleF(clientRect.Left, clientRect.Top + (clientRect.Height - _valueTextBoxElement.DesiredSize.Height) / 2, textBoxWidth, _valueTextBoxElement.DesiredSize.Height);
			foreach (var element in Children)
			{
				if (element == _valueTextBoxElement)
				{
					element.Arrange(textRect);
				}
				else if (element == _detailsButtonElement)
				{
					element.Arrange(buttonRect);
				}
				else if (element == _dataTypeDropDownElement)
				{
					element.Arrange(dropdownRect);
				}
			}
			return finalSize;
		}

		/// <summary>
		/// Dispose any managed resources
		/// </summary>
		protected override void DisposeManagedResources()
		{
			_detailsButtonElement.Click -= HandleDetailsButtonClick;
			base.DisposeManagedResources();
		}
	}
}
