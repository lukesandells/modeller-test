﻿using System;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.TreeNodes;
using MI.OperationsManagement.Domain.MasterData.Design;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
    public class ModellerPropertyGridSelectObjectEditor : BaseGridEditor
    {
        #region  Fields

        /// <summary>
        ///     The browser editor element
        /// </summary>
        private readonly BrowserNodeBrowseEdtiorElement _browserEditor;

        /// <summary>
        ///     The datatype of this editor
        /// </summary>
        public override Type DataType => typeof(BrowserNode);

        /// <summary>
        ///     The value of this editor
        /// </summary>
        public override object Value
        {
            get => _browserEditor.Value;
	        set => _browserEditor.Value = value as BrowserNode;
        }

        #endregion

        #region Constructors

	    /// <summary>
	    ///     Constructs a new ModellerPropertyGridSelectObjectEditor
	    /// </summary>
	    /// <param name="modellerPresenter">The modeller presenter to use</param>
	    /// <param name="rootNodeIds">The root nodes for the tree</param>
	    /// <param name="modelElementTreeFilter">The treefilter to apply to the tree</param>
	    public ModellerPropertyGridSelectObjectEditor(IModellerPresenter modellerPresenter, IEnumerable<Guid> rootNodeIds, ModelElementTreeFilter modelElementTreeFilter)
        {
            _browserEditor = new BrowserNodeBrowseEdtiorElement(modellerPresenter, rootNodeIds, modelElementTreeFilter);
        }

        #endregion

        #region Members

        /// <summary>
        ///     Creates the editor element required for this editor
        /// </summary>
        /// <returns></returns>
        protected override RadElement CreateEditorElement()
        {
            return _browserEditor;
        }

        #endregion
    }
}