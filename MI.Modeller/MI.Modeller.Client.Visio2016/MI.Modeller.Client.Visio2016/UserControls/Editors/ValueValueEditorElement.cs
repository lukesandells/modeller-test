﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	/// <summary>
	/// The editor element for editing the value of values
	/// </summary>
	public class ValueValueEditorElement : LightVisualElement
	{
		/// <summary>
		/// The text being displayed in this editor
		/// </summary>
		public override string Text
		{
			get => _valueTextBoxElement.Text;
			set => _valueTextBoxElement.Text = value;
		}
		/// <summary>
		/// The text box element used to manage the value
		/// </summary>
		private RadTextBoxElement _valueTextBoxElement;

		/// <summary>
		/// The button to allow for editing further details of the value
		/// </summary>
		private RadButtonElement _detailsButtonElement;

		/// <summary>
		/// The extra attributes to be covered for this value. Tuple of the (Name, Value, underlying type)
		/// </summary>
		private IEnumerable<(string, object, Type)> _extendedAttributes;

		/// <summary>
		/// Creates a new ValueValueEditorElement
		/// </summary>
		/// <param name="extendedAttributes">The extra attributes to be covered for this value. Tuple of the (Name, Value, underlying type)</param>
		public ValueValueEditorElement(IEnumerable<(string, object, Type)> extendedAttributes)
		{
			_extendedAttributes = extendedAttributes;
		}

		/// <summary>
		/// Initialize and add the elements to the cell
		/// </summary>
		protected override void CreateChildElements()
		{
			base.CreateChildElements();
			// Create the text box editor and hook into the value changed event to update the value
			_valueTextBoxElement = new RadTextBoxElement();
			Children.Add(_valueTextBoxElement);

			// Only create the button if there are etended elements
			if (_extendedAttributes.Any())
			{
				// Create the button and hook into the click event to show the details 
				_detailsButtonElement = new ActionButtonElement {Text = "..."};
				_detailsButtonElement.Click += HandleDetailsButtonClick;
				Children.Add(_detailsButtonElement);
			}
		}

		/// <summary>
		/// Displays the details form as a dialog and update the result
		/// </summary>
		private void HandleDetailsButtonClick(object sender, EventArgs e)
		{
			var propertyStore = new RadPropertyStore();
			foreach ((var attributeName, var attributesValue, var attributeType) in _extendedAttributes)
			{
				var item = new PropertyStoreItem(attributeType, attributeName, attributesValue);
				if (attributeType.IsEnum)
				{
					//item.
				}
				propertyStore.Add(item);
			}
			var control = new RadPropertyGrid();
			control.SelectedObject = propertyStore;
		}

		/// <summary>
		/// Override the ArrangeOverride method to arrange the children elements of the cell
		/// </summary>
		/// <param name="finalSize"></param>
		/// <returns></returns>
		protected override SizeF ArrangeOverride(SizeF finalSize)
		{
			// Calculate Widths
			var buttonWidth = _detailsButtonElement.DesiredSize.Width;
			var textBoxWidth = finalSize.Width - buttonWidth - 8;// -8 for margins;

			RectangleF clientRect = GetClientRectangle(finalSize);
			// Button rect aligned to right hand side
			RectangleF buttonRect = new RectangleF(clientRect.Right - buttonWidth, clientRect.Top, buttonWidth, clientRect.Height);
			// Text rect aligned to left hand side, width needs to reduce by the left of the button
			RectangleF textRect = new RectangleF(clientRect.Left, clientRect.Top + (clientRect.Height - _valueTextBoxElement.DesiredSize.Height) / 2, textBoxWidth, _valueTextBoxElement.DesiredSize.Height);
			foreach (RadElement element in this.Children)
			{
				if (element == _valueTextBoxElement)
				{
					element.Arrange(textRect);
				}
				else if (element == _detailsButtonElement)
				{
					element.Arrange(buttonRect);
				}
			}
			return finalSize;
		}

		/// <summary>
		/// Dispose any managed resources
		/// </summary>
		protected override void DisposeManagedResources()
		{
			_detailsButtonElement.Click -= HandleDetailsButtonClick;
			base.DisposeManagedResources();
		}
	}
}
