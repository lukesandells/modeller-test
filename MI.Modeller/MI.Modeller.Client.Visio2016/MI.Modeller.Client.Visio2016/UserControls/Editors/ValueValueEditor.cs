﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.Editors
{
	/// <summary>
	/// Editor for editing the value of a value
	/// </summary>
	public class ValueValueEditor : BaseGridEditor
	{
		/// <summary>
		/// The other attributes of this value
		/// </summary>
		public IDictionary<string,object> ValueAttributes = new Dictionary<string, object>();

		/// <summary>
		/// Creates the editor element for this editor
		/// </summary>
		/// <returns></returns>
		protected override RadElement CreateEditorElement()
		{
			return new ValueValueEditorElement(null);
		}
	}
}
