﻿namespace MI.Modeller.Client.Visio2016.UserControls
{
    partial class ModelObjectPropertyUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.TabControl = new Telerik.WinControls.UI.RadPageView();
			this.AttributesTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.PropertyGrid = new Telerik.WinControls.UI.RadPropertyGrid();
			this.BasicPropertiesTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.DistributedPropertiesTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.BillOfMaterialsTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.EquipmentSpecificationsTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.MaterialSpecificationsTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.PhysicalAssetSpecificationsTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.PersonnelSpecificationsTab = new Telerik.WinControls.UI.RadPageViewPage();
			this.office2013LightTheme1 = new Telerik.WinControls.Themes.Office2013LightTheme();
			((System.ComponentModel.ISupportInitialize)(this.TabControl)).BeginInit();
			this.TabControl.SuspendLayout();
			this.AttributesTab.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.PropertyGrid)).BeginInit();
			this.SuspendLayout();
			// 
			// TabControl
			// 
			this.TabControl.Controls.Add(this.AttributesTab);
			this.TabControl.Controls.Add(this.BasicPropertiesTab);
			this.TabControl.Controls.Add(this.DistributedPropertiesTab);
			this.TabControl.Controls.Add(this.BillOfMaterialsTab);
			this.TabControl.Controls.Add(this.EquipmentSpecificationsTab);
			this.TabControl.Controls.Add(this.MaterialSpecificationsTab);
			this.TabControl.Controls.Add(this.PhysicalAssetSpecificationsTab);
			this.TabControl.Controls.Add(this.PersonnelSpecificationsTab);
			this.TabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.TabControl.Location = new System.Drawing.Point(0, 0);
			this.TabControl.Name = "TabControl";
			this.TabControl.SelectedPage = this.AttributesTab;
			this.TabControl.Size = new System.Drawing.Size(360, 376);
			this.TabControl.TabIndex = 0;
			this.TabControl.ThemeName = "Office2013Light";
			// 
			// AttributesTab
			// 
			this.AttributesTab.Controls.Add(this.PropertyGrid);
			this.AttributesTab.ItemSize = new System.Drawing.SizeF(68F, 27F);
			this.AttributesTab.Location = new System.Drawing.Point(5, 31);
			this.AttributesTab.Name = "AttributesTab";
			this.AttributesTab.Padding = new System.Windows.Forms.Padding(3);
			this.AttributesTab.Size = new System.Drawing.Size(350, 340);
			this.AttributesTab.Text = "Attributes";
			// 
			// PropertyGrid
			// 
			this.PropertyGrid.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.PropertyGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.PropertyGrid.HelpBarHeight = 33.54839F;
			this.PropertyGrid.Location = new System.Drawing.Point(3, 3);
			this.PropertyGrid.Name = "PropertyGrid";
			// 
			// 
			// 
			this.PropertyGrid.RootElement.ControlBounds = new System.Drawing.Rectangle(3, 3, 200, 300);
			this.PropertyGrid.Size = new System.Drawing.Size(344, 334);
			this.PropertyGrid.TabIndex = 0;
			this.PropertyGrid.Text = "radPropertyGrid1";
			this.PropertyGrid.ThemeName = "Office2013Light";
			// 
			// BasicPropertiesTab
			// 
			this.BasicPropertiesTab.ItemSize = new System.Drawing.SizeF(70F, 27F);
			this.BasicPropertiesTab.Location = new System.Drawing.Point(1, 12);
			this.BasicPropertiesTab.Name = "BasicPropertiesTab";
			this.BasicPropertiesTab.Padding = new System.Windows.Forms.Padding(3);
			this.BasicPropertiesTab.Size = new System.Drawing.Size(358, 364);
			this.BasicPropertiesTab.Text = "Properties";
			// 
			// DistributedPropertiesTab
			// 
			this.DistributedPropertiesTab.ItemSize = new System.Drawing.SizeF(70F, 27F);
			this.DistributedPropertiesTab.Location = new System.Drawing.Point(10, 37);
			this.DistributedPropertiesTab.Name = "DistributedPropertiesTab";
			this.DistributedPropertiesTab.Padding = new System.Windows.Forms.Padding(3);
			this.DistributedPropertiesTab.Size = new System.Drawing.Size(339, 328);
			this.DistributedPropertiesTab.Text = "Properties";
			// 
			// BillOfMaterialsTab
			// 
			this.BillOfMaterialsTab.ItemSize = new System.Drawing.SizeF(98F, 27F);
			this.BillOfMaterialsTab.Location = new System.Drawing.Point(5, 31);
			this.BillOfMaterialsTab.Name = "BillOfMaterialsTab";
			this.BillOfMaterialsTab.Padding = new System.Windows.Forms.Padding(3);
			this.BillOfMaterialsTab.Size = new System.Drawing.Size(350, 340);
			this.BillOfMaterialsTab.Text = "Bill of Materials";
			// 
			// EquipmentSpecificationsTab
			// 
			this.EquipmentSpecificationsTab.ItemSize = new System.Drawing.SizeF(151F, 27F);
			this.EquipmentSpecificationsTab.Location = new System.Drawing.Point(4, 22);
			this.EquipmentSpecificationsTab.Name = "EquipmentSpecificationsTab";
			this.EquipmentSpecificationsTab.Padding = new System.Windows.Forms.Padding(3);
			this.EquipmentSpecificationsTab.Size = new System.Drawing.Size(352, 350);
			this.EquipmentSpecificationsTab.Text = "Equipment Specifications";
			// 
			// MaterialSpecificationsTab
			// 
			this.MaterialSpecificationsTab.ItemSize = new System.Drawing.SizeF(137F, 27F);
			this.MaterialSpecificationsTab.Location = new System.Drawing.Point(2, 13);
			this.MaterialSpecificationsTab.Name = "MaterialSpecificationsTab";
			this.MaterialSpecificationsTab.Padding = new System.Windows.Forms.Padding(3);
			this.MaterialSpecificationsTab.Size = new System.Drawing.Size(131, 143);
			this.MaterialSpecificationsTab.Text = "Material Specifications";
			// 
			// PhysicalAssetSpecificationsTab
			// 
			this.PhysicalAssetSpecificationsTab.ItemSize = new System.Drawing.SizeF(168F, 27F);
			this.PhysicalAssetSpecificationsTab.Location = new System.Drawing.Point(4, 22);
			this.PhysicalAssetSpecificationsTab.Name = "PhysicalAssetSpecificationsTab";
			this.PhysicalAssetSpecificationsTab.Padding = new System.Windows.Forms.Padding(3);
			this.PhysicalAssetSpecificationsTab.Size = new System.Drawing.Size(352, 350);
			this.PhysicalAssetSpecificationsTab.Text = "Physical Asset Specifications";
			// 
			// PersonnelSpecificationsTab
			// 
			this.PersonnelSpecificationsTab.ItemSize = new System.Drawing.SizeF(146F, 27F);
			this.PersonnelSpecificationsTab.Location = new System.Drawing.Point(4, 22);
			this.PersonnelSpecificationsTab.Name = "PersonnelSpecificationsTab";
			this.PersonnelSpecificationsTab.Padding = new System.Windows.Forms.Padding(3);
			this.PersonnelSpecificationsTab.Size = new System.Drawing.Size(352, 350);
			this.PersonnelSpecificationsTab.Text = "Personnel Specifications";
			// 
			// ModelObjectPropertyUserControl
			// 
			this.Controls.Add(this.TabControl);
			this.Size = new System.Drawing.Size(360, 376);
			((System.ComponentModel.ISupportInitialize)(this.TabControl)).EndInit();
			this.TabControl.ResumeLayout(false);
			this.AttributesTab.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.PropertyGrid)).EndInit();
			this.ResumeLayout(false);
        }

        #endregion

        private Telerik.WinControls.UI.RadPageView TabControl;
        private Telerik.WinControls.UI.RadPageViewPage AttributesTab;
        private Telerik.WinControls.UI.RadPageViewPage BasicPropertiesTab;
        private Telerik.WinControls.UI.RadPageViewPage DistributedPropertiesTab;
        private Telerik.WinControls.UI.RadPageViewPage BillOfMaterialsTab;
        private Telerik.WinControls.UI.RadPageViewPage EquipmentSpecificationsTab;
        private Telerik.WinControls.UI.RadPageViewPage MaterialSpecificationsTab;
        private Telerik.WinControls.UI.RadPageViewPage PhysicalAssetSpecificationsTab;
        private Telerik.WinControls.UI.RadPageViewPage PersonnelSpecificationsTab;
        private dynamic BasicPropertiesUserControl;
        private Telerik.WinControls.UI.RadPropertyGrid PropertyGrid;
        private Telerik.WinControls.Themes.Office2013LightTheme office2013LightTheme1;
        private dynamic materialSpecificationUserControl1;
    }
}
