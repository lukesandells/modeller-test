using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{

	public abstract partial class AbstractPropertiesUserControl<TProperty, TOfObject> : UserControl
		where TOfObject : class, IWithProperties<TOfObject, TProperty>, IWithExternalId
		where TProperty : Property<TProperty, TOfObject>
	{
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			parentIdColumn = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			internalIdColumn = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			idColumn = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			removeButtonColumn = new Telerik.WinControls.UI.GridViewCommandColumn();
			addChildButtonColumn = new Telerik.WinControls.UI.GridViewCommandColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			valueColumn = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
			this.GridView = new Telerik.WinControls.UI.RadGridView();
			this.ErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.WarningProvider = new System.Windows.Forms.ErrorProvider(this.components);
			this.addNewMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.addChildMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.deleteMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.applyToSpecialisationsHeaderMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.applyToSpecialisationsMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.stopApplyingToSpecialisaionsHeaderMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.keepPropertiesMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.removePropertiesMenuItem = new Telerik.WinControls.UI.RadMenuItem();
			this.PropertyControlMenu = new Telerik.WinControls.UI.RadMenu();
			this.office2013LightTheme1 = new Telerik.WinControls.Themes.Office2013LightTheme();
			((System.ComponentModel.ISupportInitialize)(this.GridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.GridView.MasterTemplate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.WarningProvider)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.PropertyControlMenu)).BeginInit();
			this.SuspendLayout();
			// 
			// GridView
			// 
			this.GridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.GridView.AutoSizeRows = true;
			this.GridView.BackColor = System.Drawing.SystemColors.ControlLightLight;
			this.GridView.Cursor = System.Windows.Forms.Cursors.Default;
			this.GridView.Font = new System.Drawing.Font("Segoe UI", 8.25F);
			this.GridView.ForeColor = System.Drawing.SystemColors.ControlText;
			this.GridView.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.GridView.Location = new System.Drawing.Point(3, 26);
			// 
			// 
			// 
			this.GridView.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
			this.GridView.MasterTemplate.AutoGenerateColumns = false;
			this.GridView.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
			parentIdColumn.EnableExpressionEditor = false;
			parentIdColumn.FieldName = "ParentProperty.InternalId";
			parentIdColumn.HeaderText = "Parent ID";
			parentIdColumn.IsVisible = false;
	        parentIdColumn.Name = "ParentIdColumn";
			parentIdColumn.Width = 41;
			internalIdColumn.EnableExpressionEditor = false;
			internalIdColumn.FieldName = "InternalId";
			internalIdColumn.HeaderText = "ID";
			internalIdColumn.IsVisible = false;
	        internalIdColumn.Name = "InternalIdColumn";
			idColumn.EnableExpressionEditor = false;
			idColumn.FieldName = "ExternalId";
			idColumn.HeaderText = "ID";
	        idColumn.Name = "IdColumn";
			idColumn.Width = 305;
			removeButtonColumn.EnableExpressionEditor = false;
			removeButtonColumn.Expression = "=\"-\"";
			removeButtonColumn.MaxWidth = 18;
			removeButtonColumn.MinWidth = 18;
	        removeButtonColumn.Name = "RemoveButtonColumn";
			removeButtonColumn.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			removeButtonColumn.Width = 18;
			addChildButtonColumn.EnableExpressionEditor = false;
			addChildButtonColumn.Expression = "=\"+\"";
			addChildButtonColumn.MaxWidth = 18;
			addChildButtonColumn.MinWidth = 18;
	        addChildButtonColumn.Name = "AddChildButtonColumn";
			addChildButtonColumn.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
			addChildButtonColumn.Width = 18;
			gridViewTextBoxColumn4.EnableExpressionEditor = false;
			gridViewTextBoxColumn4.FieldName = "Description";
			gridViewTextBoxColumn4.HeaderText = "Description";
			gridViewTextBoxColumn4.Name = "DescriptionColumn";
			gridViewTextBoxColumn4.Width = 127;
			valueColumn.EnableExpressionEditor = false;
			valueColumn.FieldName = "Value";
			valueColumn.HeaderText = "Value";
	        valueColumn.Name = "ValueColumn";
			valueColumn.Width = 310;
			this.GridView.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            parentIdColumn,
            internalIdColumn,
            idColumn,
            removeButtonColumn,
            addChildButtonColumn,
            gridViewTextBoxColumn4,
            valueColumn});
			this.GridView.MasterTemplate.EnableGrouping = false;
			this.GridView.MasterTemplate.ViewDefinition = tableViewDefinition1;
			this.GridView.Name = "GridView";
			this.GridView.RightToLeft = System.Windows.Forms.RightToLeft.No;
			// 
			// 
			// 
			this.GridView.RootElement.ControlBounds = new System.Drawing.Rectangle(3, 26, 240, 150);
			this.GridView.Size = new System.Drawing.Size(947, 415);
			this.GridView.TabIndex = 0;
			this.GridView.Text = "GridView";
			this.GridView.ThemeName = "Office2013Light";
			// 
			// ErrorProvider
			// 
			this.ErrorProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.ErrorProvider.ContainerControl = this;
			// 
			// WarningProvider
			// 
			this.WarningProvider.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.NeverBlink;
			this.WarningProvider.ContainerControl = this;
			// 
			// addNewMenuItem
			// 
			this.addNewMenuItem.Name = "addNewMenuItem";
			this.addNewMenuItem.Text = "Add New";
			this.addNewMenuItem.Click += new System.EventHandler(this.addNewMenuItem_Click);
			// 
			// addChildMenuItem
			// 
			this.addChildMenuItem.Name = "addChildMenuItem";
			this.addChildMenuItem.Text = "Add Child";
			this.addChildMenuItem.Click += new System.EventHandler(this.addChildMenuItem_Click);
			// 
			// deleteMenuItem
			// 
			this.deleteMenuItem.Name = "deleteMenuItem";
			this.deleteMenuItem.Text = "Delete";
			this.deleteMenuItem.Click += new System.EventHandler(this.removeMenuItem_Click);
			// 
			// applyToSpecialisationsHeaderMenuItem
			// 
			this.applyToSpecialisationsHeaderMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.applyToSpecialisationsMenuItem,
            this.stopApplyingToSpecialisaionsHeaderMenuItem});
			this.applyToSpecialisationsHeaderMenuItem.Name = "applyToSpecialisationsHeaderMenuItem";
			this.applyToSpecialisationsHeaderMenuItem.Text = "Apply to Specialisations";
			// 
			// applyToSpecialisationsMenuItem
			// 
			this.applyToSpecialisationsMenuItem.Name = "applyToSpecialisationsMenuItem";
			this.applyToSpecialisationsMenuItem.Text = "Apply to Specialisations";
			// 
			// stopApplyingToSpecialisaionsHeaderMenuItem
			// 
			this.stopApplyingToSpecialisaionsHeaderMenuItem.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.keepPropertiesMenuItem,
            this.removePropertiesMenuItem});
			this.stopApplyingToSpecialisaionsHeaderMenuItem.Name = "stopApplyingToSpecialisaionsHeaderMenuItem";
			this.stopApplyingToSpecialisaionsHeaderMenuItem.Text = "Stop Applying to Specialisations";
			// 
			// keepPropertiesMenuItem
			// 
			this.keepPropertiesMenuItem.Name = "keepPropertiesMenuItem";
			this.keepPropertiesMenuItem.Text = "Keep properties in specialisations";
			// 
			// removePropertiesMenuItem
			// 
			this.removePropertiesMenuItem.Name = "removePropertiesMenuItem";
			this.removePropertiesMenuItem.Text = "Remove properties from specialisations";
			// 
			// PropertyControlMenu
			// 
			this.PropertyControlMenu.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.addNewMenuItem,
            this.addChildMenuItem,
            this.deleteMenuItem,
            this.applyToSpecialisationsHeaderMenuItem});
			this.PropertyControlMenu.Location = new System.Drawing.Point(0, 0);
			this.PropertyControlMenu.Name = "PropertyControlMenu";
			this.PropertyControlMenu.Size = new System.Drawing.Size(962, 19);
			this.PropertyControlMenu.TabIndex = 1;
			this.PropertyControlMenu.Text = "PropertyControlMenu";
			this.PropertyControlMenu.ThemeName = "Office2013Light";
			// 
			// AbstractPropertiesUserControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.PropertyControlMenu);
			this.Controls.Add(this.GridView);
			this.Name = "AbstractPropertiesUserControl";
			this.Size = new System.Drawing.Size(962, 444);
			((System.ComponentModel.ISupportInitialize)(this.GridView.MasterTemplate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.GridView)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ErrorProvider)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.WarningProvider)).EndInit();
	        this.GridView.ResumeLayout();
	        this.GridView.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.PropertyControlMenu)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ErrorProvider ErrorProvider;
        private System.Windows.Forms.ErrorProvider WarningProvider;
		protected RadGridView GridView;
		private RadMenuItem addNewMenuItem;
		private RadMenuItem addChildMenuItem;
		private RadMenuItem deleteMenuItem;
		private RadMenuItem applyToSpecialisationsHeaderMenuItem;
		private RadMenuItem applyToSpecialisationsMenuItem;
		private RadMenuItem stopApplyingToSpecialisaionsHeaderMenuItem;
		private RadMenuItem keepPropertiesMenuItem;
		private RadMenuItem removePropertiesMenuItem;
		private RadMenu PropertyControlMenu;
		private Telerik.WinControls.UI.GridViewTextBoxColumn parentIdColumn;
		private Telerik.WinControls.UI.GridViewTextBoxColumn internalIdColumn;
		private Telerik.WinControls.Themes.Office2013LightTheme office2013LightTheme1;
		private Telerik.WinControls.UI.GridViewCommandColumn addChildButtonColumn;
		private Telerik.WinControls.UI.GridViewTextBoxColumn valueColumn;
		private Telerik.WinControls.UI.GridViewCommandColumn removeButtonColumn;
		private Telerik.WinControls.UI.GridViewTextBoxColumn idColumn;
	}
}
