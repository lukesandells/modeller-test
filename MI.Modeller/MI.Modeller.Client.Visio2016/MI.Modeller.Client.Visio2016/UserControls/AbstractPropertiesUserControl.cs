using MI.Framework;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.Visio2016.UserControls.Editors;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	/// <summary>
	///     Class ClassPropertiesUserControl.
	/// </summary>
	/// <seealso>
	///     <cref>"System.Windows.Forms.UserControl"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"System.Windows.Forms.UserControl"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"System.Windows.Forms.UserControl"</cref>
	/// </seealso>
	public abstract partial class AbstractPropertiesUserControl<TProperty, TOfObject> 
		where TOfObject : class, IWithProperties<TOfObject, TProperty>, IWithExternalId
		where TProperty : Property<TProperty, TOfObject>
	{
        #region Static Fields and Constants

		/// <summary>
		/// The presenter for this user control
		/// </summary>
		protected abstract IPropertyTabPresenter<TProperty, TOfObject> Presenter { get; }

        #endregion

        #region  Fields

		protected TOfObject ModelObject;
		private BindingList<TProperty> _bindingList;

		#endregion

        #region Constructors

        /// <summary>
        ///     Paramaterless contructor
        /// </summary>
        protected AbstractPropertiesUserControl()
        {
            InitializeComponent();
        }

		/// <summary>
		///     Initializes a new instance of the <see cref="AbstractPropertiesUserControl&lt;TProperty, TOfObject&gt;" /> class.
		/// </summary>
		/// <param name="modelObject">The model object.</param>
		protected AbstractPropertiesUserControl(TOfObject modelObject)
        {
	        ModelObject = ArgumentValidation.AssertNotNull(modelObject,
                nameof(modelObject));

            InitializeComponent();
            GridView.TableElement.RowHeight = this.ScaleValue(20);
            GridView.GridViewElement.GroupPanelElement.Size = new Size(100,100);

            WarningProvider.Icon = SystemIcons.Warning;

            // Custom for`matter to disable the value fields if there is more than one
            GridView.CellFormatting += FormatCells;

            GridView.CellValueChanged += HandleCellValueChanged;

	        GridView.MasterTemplate.AutoSizeColumnsMode = GridViewAutoSizeColumnsMode.Fill;
	        GridView.AllowDragToGroup = false;
	        GridView.EditorRequired += HandleEditorRequired;
	        // Add a self reference of display properties to show as a tree
	        GridView.Relations.AddSelfReference(GridView.MasterTemplate,
		        internalIdColumn.Name,
		        parentIdColumn.Name);
	        BindProperties();
			GridView.CommandCellClick += HandleCommandCellClick;
	        GridView.AutoExpandGroups = true;
	        GridView.Refresh();
	        GridView.MasterView.Refresh();
			// Disable new row, add from button
	        GridView.AllowAddNewRow = false;
	        //var editor = (RadDropDownListEditor)gridViewComboBoxColumn5.GetDefaultEditor();
	        //((RadDropDownListEditorElement)editor.EditorElement).ArrowButton.MinSize = new Size(200, 200);
	        GridView.CellValidating += HandleValidating;
        }

		private void HandleValidating(object sender, CellValidatingEventArgs e)
		{
			if (!e.Cancel && e.ActiveEditor is ValueEditor editor)
			{
				e.Cancel = !editor.ValidateAndSetValue();
				if (!e.Cancel)
				{
					((TProperty) e.Row.DataBoundItem).Value = editor.Value as Value;
					Presenter.UpdateNode();
					e.Row.InvalidateRow();
				}
			}
		}

		private void HandleEditorRequired(object sender, EditorRequiredEventArgs e)
		{
			if (GridView.Columns[valueColumn.Name].IsCurrent)
			{
				if (GridView.CurrentRow.ChildRows.Any())
				{
					GridView.CurrentCell.Enabled = false;
				} else { 
					e.EditorType = typeof(ValueEditor);
				}
			}
		}

		private void BindProperties()
		{
			_bindingList = new BindingList<TProperty>();
			ModelObject.AllProperties.ForEach(_bindingList.Add);
			GridView.DataSource = _bindingList;
		}

		#endregion

        #region Members

        /// <summary>
        ///     Formats the cell with the given object. This is used to set the correct behaviour of a cell for single or multiple
        ///     values where relevent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FormatCells(object sender,
            CellFormattingEventArgs e)
        {
     //       if (e.CellElement.ColumnInfo.Name == C_VALUE_COLUMN_NAME)
     //       {
     //           var displayProperty = (TProperty) e.CellElement.RowInfo.DataBoundItem;
     //           // If there's one value the cell can be edited
     //           if (!(displayProperty?.HasSubProperties ?? false) &&  (displayProperty?.Value == null || displayProperty?.Value?.Elements.Count() <= 1))
     //           {
	    //            e.CellElement.Text = e.CellElement.ColumnInfo.Name == C_VALUE_COLUMN_NAME 
					//	? displayProperty?.Value?.SingleOrDefault()?.ToObject().ToString() 
					//	: displayProperty?.Value?.SingleOrDefault()?.DataType.ToString();
	    //            SetupEnabledCell(e.CellElement);
     //           }
     //           // Otherwise it cannot and instead shows the list of values
     //           else
     //           {
	    //            if (e.CellElement.ColumnInfo.Name == C_VALUE_COLUMN_NAME)
	    //            {
		   //             e.CellElement.Text = displayProperty?.Value?.ToString();
	    //            }
					//SetupDisabledCell(e.CellElement);
     //           }
     //       }
        }

        /// <summary>
        ///     Handle the cell value change event to trigger DataChangedEvent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleCellValueChanged(object sender,
            GridViewCellEventArgs e)
        {
			Presenter.UpdateNode();
		    e.Row.InvalidateRow();
        }

        /// <summary>
        ///     Handles command cell click events.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void HandleCommandCellClick(object sender,
            EventArgs eventArgs)
        {
            // One handler for all, need to do appropriate action for column
            var e = (GridViewCellEventArgs) eventArgs;
            if (e.Column == removeButtonColumn)
            {
				removeMenuItem_Click(sender, eventArgs);
			}
            else if (e.Column == addChildButtonColumn)
            {
	            addChildMenuItem_Click(sender, eventArgs);
            }
        }

		#endregion

		/// <summary>
		/// Called to add a new property
		/// </summary>
		private void addNewMenuItem_Click(object sender, EventArgs e)
		{
			_bindingList.Add(Presenter.AddProperty());
		}

		/// <summary>
		/// Called to add a new child property
		/// </summary>
		private void addChildMenuItem_Click(object sender, EventArgs e)
		{
			_bindingList.Add(Presenter.AddProperty(parentProperty: (TProperty) GridView.CurrentRow.DataBoundItem));
			GridView.CurrentRow.IsExpanded = true;
		}

		/// <summary>
		/// Called to delete a property
		/// </summary>
		private void removeMenuItem_Click(object sender, EventArgs e)
		{
			if (GridView.CurrentRow?.DataBoundItem != null)
			{
				Presenter.RemoveProperty((TProperty)GridView.CurrentRow.DataBoundItem);
				_bindingList.Remove((TProperty)GridView.CurrentRow.DataBoundItem);
			}
		}
	}
}