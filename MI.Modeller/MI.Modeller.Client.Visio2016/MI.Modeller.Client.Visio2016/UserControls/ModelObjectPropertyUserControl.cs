﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.PropertyPane;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.Visio2016.UserControls.Editors;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	/// <summary>
	///     Provides the method for users to interact with Modeller Object Properties
	/// </summary>
	public partial class ModelObjectPropertyUserControl : RadControl
    {
        #region  Fields
		/// <summary>
		/// A map between the telerik property store items and the modeller item they're representing
		/// </summary>
        private readonly IDictionary<string, Tuple<PropertyStoreItem, ModellerPropertyStoreItem>> _attributes =
            new Dictionary<string, Tuple<PropertyStoreItem, ModellerPropertyStoreItem>>();

		/// <summary>
		/// The modeller presenter
		/// </summary>
		private readonly IModellerPresenter _modellerPresenter;

		/// <summary>
		/// The presenter for this control
		/// </summary>
		private readonly IPropertyPanePresenter _presenter;

		#endregion

		#region Constructors

		/// <summary>
		/// Creates a new ModelObjectPropertyUserControl
		/// </summary>
		/// <param name="modellerPresenter">The modeller presenter</param>
		public ModelObjectPropertyUserControl(IModellerPresenter modellerPresenter)
        {
            _modellerPresenter = modellerPresenter;
            InitializeComponent();
            PropertyGrid.EditorRequired += HandlePropertyGridEditorRequired;
	        PropertyGrid.EditorInitialized += HandleEditorInit;
			_presenter = new PropertyPanePresenter(_modellerPresenter);
        }


	    /// The DPI scale factor is screwing things up with the editor. We need to set the  size to constrain it.
	    private void HandleEditorInit(object sender, PropertyGridItemEditorInitializedEventArgs e)
	    {
		    var editor = (e.Editor as BaseInputEditor)?.EditorElement;
		    if (editor == null)
		    {
			    return;
		    }
		    editor.MinSize = new Size(
			    (int)(editor.Size.Width / editor.DpiScaleFactor.Width),
			    (int)(editor.Size.Height / editor.DpiScaleFactor.Height));
			if (e.Editor is BaseDropDownListEditor gridEditor)
		    {
				var textBox = ((BaseDropDownListEditorElement)gridEditor.EditorElement).TextBox;
			    textBox.MinSize = new Size(
				    (int)(textBox.Size.Width / textBox.DpiScaleFactor.Width),
				    (int)(textBox.Size.Height / textBox.DpiScaleFactor.Height));
			}
		}

		#endregion

		#region Members

		/// <summary>
		/// Initialises the Property store and sets up each tab
		/// </summary>
		/// <param name="modelElement">The model element this control is for</param>
		public void InitialisePropertyStore(ModelElement modelElement)
        {
            PropertyGrid.PropertySort = PropertySort.Categorized;
            SetupModelObjectAttributes(modelElement);
	        var designObject = modelElement.DesignObject;
	        if (modelElement.TypeId == NodeTypeId.HierarchyScope)
	        {
		        designObject = ((HierarchyScope) designObject).DefiningEquipment;
	        }
            SetupBasicPropertiesTab(modelElement);
            SetupDistributedPropertiesTab(designObject);
            SetupBillOfMaterialTab(designObject);
            SetupEquipmentSpecificationsTab(designObject);
            SetupMaterialSpecifiationsTab(modelElement);
            SetupPhysicalAssetSpecificationsTab(designObject);
            SetupPersonnelSpecificationsTab(designObject);
        }

		/// <summary>
		/// Sets up the basic property tab. If the model element does not implement IWithBasicProperties then this tab will be hidden
		/// </summary>
		/// <param name="modelElement">The model element to set it up for</param>
		private void SetupBasicPropertiesTab(ModelElement modelElement)
        {
	        //if (modelElement.DesignObject.GetType().HasInterface(typeof(IWithBasicProperties<>)))
	        //{
		       // BasicPropertiesTab.Item.Visibility = ElementVisibility.Visible;
		       // var type = typeof(BasicPropertiesUserControl<>).MakeGenericType(modelElement.Type.TargetObjectType);
		       // BasicPropertiesUserControl = Activator.CreateInstance(type, modelElement, modelElement.DesignObject);
		       // BasicPropertiesUserControl.Dock = DockStyle.Fill;
		       // BasicPropertiesUserControl.Location = new Point(8, 7);
		       // BasicPropertiesUserControl.Margin = new Padding(21, 17, 21, 17);
		       // BasicPropertiesUserControl.Name = "BasicPropertiesUserControl";
		       // BasicPropertiesUserControl.Size = new Size(888, 768);
		       // BasicPropertiesUserControl.TabIndex = 0;
		       // BasicPropertiesTab.Controls.Clear();
		       // BasicPropertiesTab.Controls.Add((Control) BasicPropertiesUserControl);
	        //}
	        //else
	        //{
		        BasicPropertiesTab.Item.Visibility = ElementVisibility.Collapsed;
		        BasicPropertiesUserControl = null;
	       // }
            
        }

		/// <summary>
		/// Sets up the Bill of Materials Tab. This will result in the tab being hidden if the design object is not a OperationsDefinition
		/// </summary>
		/// <param name="designObject">The design objct this control is for</param>
        private void SetupBillOfMaterialTab(DesignObject designObject)
        {
	        //if (designObject is OperationsDefinition)
	        //{
		       // BillOfMaterialsTab.Item.Visibility = ElementVisibility.Visible;
	        //}
	        //else
	        //{
		        BillOfMaterialsTab.Item.Visibility = ElementVisibility.Collapsed;
	        //}
        }

		/// <summary>
		/// Sets up the distributed properties tab. If the design object does not implement IWithResourceTransactionProperties it will be hidden
		/// </summary>
		/// <param name="designObject">The design objct this control is for</param>
		private void SetupDistributedPropertiesTab(DesignObject designObject)
        {
	        //if (designObject.GetType()
		       // .HasInterface(typeof(IWithResourceTransactionProperties<,>)))
	        //{
		       // DistributedPropertiesTab.Item.Visibility = ElementVisibility.Visible;
				// TODO setup user control
		        //BasicPropertiesUserControl = Activator.CreateInstance(
			       // typeof(BasicPropertiesUserControl<>).MakeGenericType(designObject.GetType().GetInterfaces(typeof(IWithBasicProperties<>))),
			       // designObject);
	        //}
	        //else
	        //{
		        DistributedPropertiesTab.Item.Visibility = ElementVisibility.Collapsed;
				// TODO clear user control
		        //BasicPropertiesUserControl = null;
	        //}
        }

		/// <summary>
		/// When an editor is required for the property grid the appropriate editor will be selected for Modeller types
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void HandlePropertyGridEditorRequired(object sender,
            PropertyGridEditorRequiredEventArgs e)
        {
            var attribute = _attributes[e.Item.Name].Item2;
            switch (attribute.EditorType)
            {
                case EditorType.DropDown:
                    e.EditorType = typeof(ModellerPropertyGridDropDownEditor);
                    e.Editor = new ModellerPropertyGridDropDownEditor(attribute);
                    break;
                case EditorType.Text:
                    break;
                case EditorType.ModelObject:
                    e.EditorType = typeof(ModellerPropertyGridSelectObjectEditor);
	                var filter = new ModelElementTreeFilter(attribute.Configuration.NodeType, attribute.HierarchyScope);
					// Always going to be browsing by Hierarchy Scope
	                using (var scope = new PersistenceScope())
	                {
		                var rootNodeIds = scope.Query<RootNode>().Single().NonEmptyNonSystemFolders.SelectMany(f => f.Nodes).Where(n => n.Type == NodeType.For(typeof(HierarchyScope))).Select(n => n.Id).ToArray();
		                e.Editor = new ModellerPropertyGridSelectObjectEditor(_modellerPresenter, rootNodeIds, filter);
						scope.Done();
	                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void SetupEquipmentSpecificationsTab(DesignObject designObject)
        {
	        //if (designObject.GetType()
		       // .HasInterface(typeof(IProcessDefinition<>)))
	        //{
		       // EquipmentSpecificationsTab.Item.Visibility = ElementVisibility.Visible;
	        //}
	        //else
	        //{
		        EquipmentSpecificationsTab.Item.Visibility = ElementVisibility.Collapsed;
	    //    }
        }

	    private void SetupMaterialSpecifiationsTab(ModelElement modelElement)
	    {
		 //   if (modelElement.DesignObject.GetType().HasInterface(typeof(IProcessDefinition<>)))
		 //   {
			//    MaterialSpecificationsTab.Item.Visibility = ElementVisibility.Visible;
			//    var type = typeof(MaterialSpecificationUserControl<>).MakeGenericType(modelElement.DesignObject.GetType());
			//    materialSpecificationUserControl1 = Activator.CreateInstance(type, modelElement);
			//    materialSpecificationUserControl1.Dock = DockStyle.Fill;
			//    materialSpecificationUserControl1.Location = new Point(3, 3);
			//    materialSpecificationUserControl1.Margin = new Padding(0);
			//    materialSpecificationUserControl1.Name = "materialSpecificationUserControl1";
			//    materialSpecificationUserControl1.Size = new Size(125, 137);
			//    materialSpecificationUserControl1.TabIndex = 0;
			//    MaterialSpecificationsTab.Controls.Clear();
			//    MaterialSpecificationsTab.Controls.Add((Control)materialSpecificationUserControl1);
			//}
		 //   else
		 //   {
			    MaterialSpecificationsTab.Item.Visibility = ElementVisibility.Collapsed;
		  //  }
	    }
		
		private void SetupModelObjectAttributes(ModelElement modelElement)
        {
            var propertyStore = new RadPropertyStore();
            _attributes.Clear();
            foreach (var attribute in modelElement.Type.Configuration.Properties.Values)
            {
	            if (modelElement.Properties[attribute.MemberId].IsApplicable)
	            {
		            var attributePropertyItem = new ModellerPropertyStoreItem(attribute, modelElement, _presenter);
		            propertyStore.Add(attributePropertyItem);
		            _attributes.Add(attributePropertyItem.PropertyName, new Tuple<PropertyStoreItem, ModellerPropertyStoreItem>(attributePropertyItem, attributePropertyItem));
	            }
            }
            PropertyGrid.SelectedObject = propertyStore;
        }

        private void SetupPersonnelSpecificationsTab(DesignObject designObject)
        {
	        //if (designObject.GetType()
		       // .HasInterface(typeof(IProcessDefinition<>)))
	        //{
		       // PersonnelSpecificationsTab.Item.Visibility = ElementVisibility.Visible;
	        //}
	        //else
	        //{
		        PersonnelSpecificationsTab.Item.Visibility = ElementVisibility.Collapsed;
	        //}
        }

        private void SetupPhysicalAssetSpecificationsTab(DesignObject designObject)
        {
	        //if (designObject.GetType()
		       // .HasInterface(typeof(IProcessDefinition<>)))
	        //{
		       // PhysicalAssetSpecificationsTab.Item.Visibility = ElementVisibility.Visible;
	        //}
	        //else
	        //{
		        PhysicalAssetSpecificationsTab.Item.Visibility = ElementVisibility.Collapsed;
	        //}
        }

		#endregion
	}

	/// <summary>
	/// Listener to refresh the property grid when a node property is changed
	/// </summary>
	class PropertyUserControlListener: NodeActivityListener
	{
		/// <summary>
		/// The property grid to refresh
		/// </summary>
		private readonly PropertyGrid _grid;

		/// <summary>
		/// Constructs a new PropertyUserControlListener
		/// </summary>
		/// <param name="grid">The property grid to refresh</param>
		PropertyUserControlListener(PropertyGrid grid)
		{
			_grid = grid;
			_grid.Disposed += (s,e) => Dispose();
		}
		/// <summary>
		/// Refreshes the grid
		/// </summary>
		public override void NodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
		{
			_grid.Refresh();
		}
	}
}