﻿using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.ViewInterfaces;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
	public abstract class BaseModellerTreeNode: RadTreeNode
	{
		public new ModellerTreeView TreeView => (ModellerTreeView) (base.TreeView ?? Parent?.TreeView);

		protected BaseModellerTreeNode(IObjectBrowserPresenter presenter)
		{
			Presenter = ArgumentValidation.AssertNotNull(presenter, nameof(presenter));
			Initialise();
		}

		protected IObjectBrowserPresenter Presenter { get; set; }

		protected void Initialise()
		{
			ContextMenu = new RadContextMenu();
			ContextMenu.DropDownOpening += HandleDropDownOpeneing;
		}

		private void HandleDropDownOpeneing(object sender, CancelEventArgs e)
		{
			ContextMenu.Items.Clear();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var actions = Presenter.GetPermissibleOperations();
				foreach (var action in actions)
				{
					ContextMenu.Items.Add(ConstructContextMenuItem(action, ContextMenu));
				}
				scope.Done();
			}
			if (!ContextMenu.Items.Any())
			{
				e.Cancel = true;
			}
		}

		private RadMenuItem ConstructContextMenuItem(ContextAction contextAction, RadContextMenu contextMenu)
		{
			var item = new RadMenuItem(contextAction.Title);
			if (contextAction.Action != null)
			{
				item.Click += (sender, e) =>
				{
					contextAction.Action.Invoke();
					// TODO this is a work around. See https://feedback.telerik.com/Project/154/Feedback/Details/228901-fix-radcontextmenu-the-context-menu-is-not-closed-when-the-sub-menu-is-over-th
					contextMenu.DropDown.ClosePopup(new PopupCloseInfo(RadPopupCloseReason.CloseCalled, null));
				};
				
			}
			if (contextAction.ChildActions != null)
			{
				foreach (var childAction in contextAction.ChildActions)
				{
					item.Items.Add(ConstructContextMenuItem(childAction, contextMenu));
				}
			}
			return item;
		}

		/// <summary>
		///     All descendent nodes from this node
		/// TODO is this needed?
		/// </summary>
		public IEnumerable<BaseModellerTreeNode> Descendents => Nodes.Union(Nodes.Cast<BaseModellerTreeNode>()
				.SelectMany(n => n.Descendents))
			.Cast<BaseModellerTreeNode>();

		internal abstract void RefreshNode();

	}
}
