﻿using MI.Framework.ObjectBrowsing;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
	/// <summary>
	///     Menu item to handle operations done against the Object Browser
	/// </summary>
	public sealed class ModellerObjectBrowserMenuItem : RadMenuItem
    {
        #region Static Fields and Constants

        #endregion

        #region  Fields

        /// <summary>
        ///     The operation that will be performed by this menu item
        /// </summary>
        public OperationType OperationType { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Construct a new  ModellerObjectBrowserMenuItem
        /// </summary>
        /// <param name="operationType"></param>
        public ModellerObjectBrowserMenuItem(OperationType operationType)
        {
	        Text = operationType.ToString();
            OperationType = operationType;
        }

        #endregion
    }
}