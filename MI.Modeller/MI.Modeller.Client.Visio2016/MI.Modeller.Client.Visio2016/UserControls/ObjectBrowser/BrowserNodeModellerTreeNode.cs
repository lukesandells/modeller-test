﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Domain;
using System;
using System.Drawing;
using System.Linq;

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
	public class BrowserNodeModellerTreeNode: BaseModellerTreeNode
	{
		public BrowserNodeModellerTreeNode(BrowserNode target, IObjectBrowserPresenter presenter, BaseModellerTreeNode parent): base(presenter)
		{
			target = ArgumentValidation.AssertNotNull(target, nameof(target));
			TargetId = target.Id;
			NodeType = target.Type;
			IsDefinitionNode = target.IsDefinitionNode;
			parent?.Nodes.Add(this);
		}

		public Guid TargetId { get; set; }
		public bool IsDefinitionNode { get; }
		public NodeType NodeType { get; }
		public bool IsNewNew { get; set; }

		internal sealed override void RefreshNode()
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var target = scope.FindById<BrowserNode>(TargetId);
				RefreshNode(target);
			scope.Done();
			}
		}

		internal void RefreshNode(BrowserNode target)
		{
			if (target != null)
			{
				if (target.Id != TargetId)
				{
					throw new ArgumentException("Target node does not match the node this is for");
				}
				Text = target.DisplayId;
				Image = TreeView.GetScaledIcon(target);
				// Make Hierarchy Scopes bold
				if (target.IsDefinitionNode && target.TypeId == NodeTypeId.HierarchyScope)
				{
					Font = new Font(TreeView.Font, FontStyle.Bold);
				}
				// And link nodes italic
				if (target.IsLinkNode)
				{
					Font = new Font(TreeView.Font, FontStyle.Italic);
				}
				// Only need to update the children if the parent is expanded
				// Only Definition Nodes will have children
				if ((Parent?.Expanded ?? true) && (target?.IsDefinitionNode ?? true))
				{
					RefreshFolders(target);
					RefreshPrimaryFolderContents(target);
					if (target.Is<ParentNode>(out var parentNode))
					{
						foreach (var n in Nodes.Where(n => n is BrowserNodeModellerTreeNode))
						{
							var targetNode = parentNode.PrimaryFolder.Nodes.Single(fn => fn.Id == ((BrowserNodeModellerTreeNode) n).TargetId);
							((BrowserNodeModellerTreeNode) n).RefreshNode(targetNode);
						}
						foreach (var n in Nodes.Where(n => n is BrowserFolderModellerTreeNode))
						{
							var targetFolder = parentNode.AllFolders.Single(f => new TargetFolderId(f.Parent.Id, f.MemberId) == ((BrowserFolderModellerTreeNode) n).TargetFolderId);
							((BrowserFolderModellerTreeNode) n).RefreshNode(targetFolder);
						}
					}
				}
			}
		}

		private void RefreshPrimaryFolderContents(BrowserNode target)
		{
			var folders = target.Cast<DefinitionNode>().AllFolders.Where(f => f.IsPrimary).ToArray();
			var nodesToHave = folders.SelectMany(f => f.Nodes).Where(n => TreeView.Filter?.IsVisible(n) ?? true).ToArray();
			var currentNodes = Nodes.Where(n => n is BrowserNodeModellerTreeNode)
				.Cast<BrowserNodeModellerTreeNode>()
				.Select(n => n.TargetId)
				.ToArray();
			// Remove folders that aren't in the list of nodes to have
			foreach (var n in currentNodes.Where(n => !nodesToHave.Select(h => h.Id).Contains(n)))
			{
				var nodeToRemove = Nodes.Single(node => (node as BrowserNodeModellerTreeNode)?.TargetId == n);
				Nodes.Remove(nodeToRemove);
			}
			// Add nodes that aren't in the list of current nodes
			foreach (var n in nodesToHave.Where(n => !currentNodes.Contains(n.Id)))
			{
				// ReSharper disable once ObjectCreationAsStatement
				new BrowserNodeModellerTreeNode(n, Presenter, this);
			}
		}

		private void RefreshFolders(BrowserNode target)
		{
			var definitionNode = target.Cast<DefinitionNode>();
			var foldersToHave = definitionNode.NonEmptyNonSystemFolders.Where(f => !f.IsPrimary && f.Nodes.Any(n => TreeView.Filter?.IsVisible(n) ?? true));
			var currentFolders = Nodes.Where(n => n is BrowserFolderModellerTreeNode)
				.Cast<BrowserFolderModellerTreeNode>()
				.Where(n => n.TargetFolderId.ParentNodeId == target.Id)
				.ToArray();
			// Remove folders that aren't in the list of nodes to have
			foreach (var f in currentFolders.Where(f => !foldersToHave.Select(fh => new TargetFolderId(fh.Parent.Id, fh.MemberId)).Contains(f.TargetFolderId)))
			{
				var nodeToRemove = Nodes.Single(n => (n as BrowserFolderModellerTreeNode)?.TargetFolderId == f.TargetFolderId);
				Nodes.Remove(nodeToRemove);
			}
			// Add nodes that aren't in the list of current nodes
			foreach (var f in foldersToHave.Where(f => !currentFolders.Select(cf => cf.TargetFolderId).Contains(new TargetFolderId(f.Parent.Id, f.MemberId))))
			{
				// ReSharper disable once ObjectCreationAsStatement
				new BrowserFolderModellerTreeNode(f, Presenter, this);
			}
		}
	}
}
