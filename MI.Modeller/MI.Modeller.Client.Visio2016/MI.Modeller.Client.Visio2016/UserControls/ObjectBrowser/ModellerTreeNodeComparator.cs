﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Telerik.WinControls.UI;

// ReSharper disable VirtualMemberCallInConstructor

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
	/// <summary>
	/// Comparator for tree nodes
	/// </summary>
	public class ModellerTreeNodeComparator : IComparer<RadTreeNode>
    {
        #region  Interface Implementations

        /// <summary>
        /// Compares two tree nodes
        /// </summary>
        /// <param name="x">The first node to compare</param>
        /// <param name="y">The second node to compare</param>
        /// <returns></returns>
        [SuppressMessage("ReSharper", "PossibleNullReferenceException")]
        public int Compare(RadTreeNode x,
            RadTreeNode y)
        {
            var left = (BaseModellerTreeNode) x;
            var right = (BaseModellerTreeNode) y;
            // Default is to compare text
            if (left == null && right == null)
            {
                return string.Compare(left.Text,
                    right.Text,
                    StringComparison.Ordinal);
            }
            if (left == null)
            {
                return -1;
            }
            if (right == null)
            {
                return 1;
            }
			// Folders first
            if (left is BrowserFolderModellerTreeNode &&
                !(right is BrowserFolderModellerTreeNode))
            {
                return -1;
            }
            if (right is BrowserFolderModellerTreeNode &&
                !(left is BrowserFolderModellerTreeNode))
			{
                return 1;
            }
            // Other wise by name
            return string.Compare(left.Text,
                right.Text,
                StringComparison.Ordinal);
        }

        #endregion
    }
}