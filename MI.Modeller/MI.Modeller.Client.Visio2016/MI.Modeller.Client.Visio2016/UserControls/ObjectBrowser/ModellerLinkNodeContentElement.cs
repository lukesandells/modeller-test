﻿using System;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
    /// <summary>
    ///     The content for a modeller tree link node
    /// </summary>
    internal class ModellerLinkNodeContentElement : TreeNodeContentElement
    {
        #region  Fields/Properties

        /// <summary>
        ///     The container for the content of this node elemnt
        /// </summary>
        private StackLayoutElement _nodeContentContainer;

        /// <summary>
        ///     The text element for the node text
        /// </summary>
        private LightVisualElement _textElement;

        /// <summary>
        ///     The button element
        /// </summary>
        private RadButtonElement _navigationButton;

        /// <summary>
        ///     Required for Telerik theme to display correctly
        /// </summary>
        protected override Type ThemeEffectiveType => typeof(TreeNodeContentElement);

        #endregion

        #region Other Methods

        /// <summary>
        ///     Sets up the content element and keeps it in sync with the node
        /// </summary>
        public override void Synchronize()
        {
            var treeNodeElement = NodeElement;
            var node = treeNodeElement.Data;

            _textElement.Text = node.Text;
        }

        /// <summary>
        ///     Create the children element required for the content
        /// </summary>
        protected override void CreateChildElements()
        {
            _nodeContentContainer = new StackLayoutElement
            {
                Orientation = Orientation.Horizontal,
                StretchHorizontally = true,
                StretchVertically = false,
                DrawFill = false,
                DrawBackgroundImage = false,
                BackgroundImage = null
            };

            _textElement = new LightVisualElement
            {
                ShouldHandleMouseInput = false,
                NotifyParentOnMouseInput = true,
                StretchVertically = false
            };
            _nodeContentContainer.Children.Add(_textElement);

            _navigationButton = new RadButtonElement
            {
                Margin = new Padding(20, 3, 20, 3)
            };
            _navigationButton.Click += HandleNavigationButtonClick;
            _navigationButton.StretchVertically = true;
            _nodeContentContainer.Children.Add(_navigationButton);

            Children.Add(_nodeContentContainer);
        }

        /// <summary>
        ///     Handles the click of the navigation button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleNavigationButtonClick(object sender, EventArgs e)
        {
            MessageBox.Show("Go to definition");
        }

        #endregion
    }
}