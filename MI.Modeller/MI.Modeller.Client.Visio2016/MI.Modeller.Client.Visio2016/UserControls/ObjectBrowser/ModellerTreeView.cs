﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain;
using NHibernate.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
	public class ModellerTreeView: RadTreeView, IModellerTreeView
	{
		private IObjectBrowserPresenter _presenter;

		/// <summary>
		/// Whether or not nodes may be dragged
		/// </summary>
		public bool AllowDrag { get; set; } = true;

		/// <summary>
		///     Set when the mouse is clicked, used to determine if we need to start a dragdrop operation
		/// </summary>
		private Point _lastMouseDownLocation;

		private readonly IModellerPresenter _modellerPresenter;
		/// <summary>
		/// The state that the drag operated under. Need to store because when the drag is done the left or right button is no longer pressed!
		/// </summary>
		private KeyState _dragKeyState;

		/// <summary>
		///     The ThemeClassName, this is required for the telerik theming to apply correctly
		/// </summary>
		public override string ThemeClassName => typeof(RadTreeView).FullName;

		public ModellerTreeView(IEnumerable<Guid> rootNodeIds, IModellerPresenter modellerPresenter, ModelElementTreeFilter filter = null, bool allowMultipleSelections = true, bool allowEdit = true)
		{
			_modellerPresenter = modellerPresenter;
			_presenter = new ObjectBrowserPresenter(this, modellerPresenter);
			base.SelectedNodesChanged += HandleSelectedNodesChanged;
			base.NodeMouseDoubleClick += HandleMouseDoubleClick;
			base.MultiSelect = allowMultipleSelections;
			MouseMove += HandleMouseMove;
			MouseDown += HandleMouseDown;
			NodeExpandedChanged += HandleNodeExpanded;
			this.Filter = filter;
			base.AllowEdit = allowEdit;
			base.TreeViewElement.Comparer = new ModellerTreeNodeComparator();
			base.TreeViewElement.SortOrder = SortOrder.Ascending;
			base.ValueValidating += HandleValueValidating;
			base.Edited += HandleEdited;
			base.EditorInitialized += HandleEditorInit;

			DragOver += HandleDragOver;
			DragDrop += HandleDragDrop;

			base.AllowDrop = true;
			base.MultiSelect = true;

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var rootNodes = scope.Query<ParentNode>().Where(n => rootNodeIds.Contains(n.Id)).ToArray();
				foreach (var n in rootNodes)
				{
					if (n.Is<RootNode>())
					{
						foreach (var folder in n.NonSystemFolders)
						{
							var node = new BrowserFolderModellerTreeNode(folder, _presenter);
							Nodes.Add(node);
							node.RefreshNode();
						}
					}
					else
					{
						var node = new BrowserNodeModellerTreeNode(n, _presenter, null);
						Nodes.Add(node);
						node.RefreshNode();
					}
				}
				
				scope.Done();
			}
		}

		/// The DPI scale factor is screwing things up with the editor. We need to set the minimum size to constrain it.
		private void HandleEditorInit(object sender, TreeNodeEditorInitializedEventArgs e)
		{
			var editor = (e.Editor as BaseInputEditor)?.EditorElement;
			if (editor == null)
			{
				return;
			}
			editor.MinSize = new Size(
				(int)(editor.Size.Width / editor.DpiScaleFactor.Width),
				(int)(editor.Size.Height / editor.DpiScaleFactor.Height));
		}

		/// <summary>
		/// Handles the edit of a node ending. If the edit for a new node is cancelled we need to delete the node that was created
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleEdited(object sender, TreeNodeEditedEventArgs e)
		{
			var browserNode = (e.Node as BrowserNodeModellerTreeNode);
			if (e.Canceled && (browserNode?.IsNewNew ?? false))
			{
				_presenter.ExecuteOperation(OperationType.Delete);
			}
			if (browserNode != null)
			{
				browserNode.IsNewNew = false;
			}
		}

		private void HandleMouseDoubleClick(object sender, RadTreeViewEventArgs e)
		{
			if (Filter?.TargetNodeType == null && e.Node is BrowserNodeModellerTreeNode node && node.IsDefinitionNode && node.NodeType == NodeType.For(typeof(Model)))
			{
				_modellerPresenter.OpenModel(node.TargetId);
			}
		}

		public new ModelElementTreeFilter Filter { get; }

		/// <summary>
		///     Handles the drop operation when node(s) dropped
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleDragDrop(object sender, DragEventArgs e)
		{
			if (_modellerPresenter.DragInOperation)
			{
				var targetNode = GetNodeAt(PointToClient(new Point(e.X, e.Y)));
				var browserNode = targetNode as BrowserNodeModellerTreeNode;
				var browserFolder = targetNode as BrowserFolderModellerTreeNode;
				if (browserNode == null && browserFolder == null)
				{
					return;
				}
				var permittedOperations = _presenter.PermittedDragOperations(_dragKeyState, browserNode?.TargetId ?? browserFolder.TargetFolderId.ParentNodeId, browserFolder?.TargetFolderId.FolderMemeberId).ToArray();
				if (!permittedOperations.Any())
				{
					return;
				}
				if (permittedOperations.Length == 1 && (!permittedOperations.Single().ChildActions?.Any() ?? true))
				{
					permittedOperations.Single().Action();
				}
				else
				{
					var contextMenu = new RadContextMenu();
					foreach (var operation in permittedOperations)
					{
						var item = ConstructContextMenuItem(operation, contextMenu);
						contextMenu.Items.Add(item);
					}
					contextMenu.Show(Cursor.Position);
				}
			}
		}

		/// <summary>
		/// Construct the context menu item for an operation
		/// </summary>
		/// <returns></returns>
		private RadItem ConstructContextMenuItem(ContextAction operation, RadContextMenu contextMenu)
		{
			var item = new RadMenuItem(operation.Title);
			if (operation.Action != null)
			{
				item.Click += (clickS, clickE) =>
				{
					operation.Action();
					// TODO this is a work around. See https://feedback.telerik.com/Project/154/Feedback/Details/228901-fix-radcontextmenu-the-context-menu-is-not-closed-when-the-sub-menu-is-over-th
					contextMenu.DropDown.ClosePopup(new PopupCloseInfo(RadPopupCloseReason.CloseCalled, null));
				};
			}
			if (operation.ChildActions != null)
			{
				foreach (var child in operation.ChildActions)
				{
					item.Items.Add(ConstructContextMenuItem(child, contextMenu));
				}
			}
			return item;
		}

		/// <summary>
		///     Handled the drag over event and sets the drag effect if valid
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleDragOver(object sender, DragEventArgs e)
		{
			if (_modellerPresenter.DragInOperation)
			{
				var targetNode = GetNodeAt(PointToClient(new Point(e.X, e.Y)));
				var browserNode = targetNode as BrowserNodeModellerTreeNode;
				var browserFolder = targetNode as BrowserFolderModellerTreeNode;
				if (browserNode == null && browserFolder == null)
				{
					e.Effect = DragDropEffects.None;
					return;
				}
				_dragKeyState = (KeyState) e.KeyState;
				var firstAction = _presenter.PermittedDragOperations(_dragKeyState, browserNode?.TargetId ?? browserFolder.TargetFolderId.ParentNodeId, browserFolder?.TargetFolderId.FolderMemeberId).FirstOrDefault();
				if (firstAction != null)
				{
					if (firstAction.Operation == OperationType.CopyTo)
					{
						e.Effect = DragDropEffects.Copy;
					} else if (firstAction.Operation == OperationType.AddNew)
					{
						e.Effect = DragDropEffects.Link;
					}
					else
					{
						e.Effect = DragDropEffects.Move;
					}
					return;
				}
			}
			e.Effect = DragDropEffects.None;
		}

		/// <summary>
		///     Handler for when the value in an editor is validated
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleValueValidating(object sender, TreeNodeValidatingEventArgs e)
		{
			string validationMessage = null;
			if (((BrowserNodeModellerTreeNode) e.Node).IsDefinitionNode)
			{
				validationMessage = _presenter.UpdateDisplayId(((BrowserNodeModellerTreeNode)e.Node).TargetId, e.NewValue?.ToString());
			}
			if (!string.IsNullOrEmpty(validationMessage))
			{
				MessageBox.Show(validationMessage);
			}
			e.Cancel = true;
		}

		/// <summary>
		/// Refreshsed a node when expanded
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleNodeExpanded(object sender, RadTreeViewEventArgs e)
		{
			(e.Node as BaseModellerTreeNode)?.RefreshNode();
		}

		/// <summary>
		///     Handler for mouse down event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleMouseDown(object sender, MouseEventArgs e)
		{
			_lastMouseDownLocation = e.Location;
		}

		/// <summary>
		///     Handler for the mouse move event, if the mouse has moved far enough from it's start position it starts a drag
		///     operation
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleMouseMove(object sender, MouseEventArgs e)
		{
			// Only allow drag where it's allowed and is an actual drag
			if (!AllowDrag || (e.Button != MouseButtons.Left && e.Button != MouseButtons.Right) || _modellerPresenter.DragInOperation)
			{
				return;
			}
			// Can't drag if no nodes are selected or it's not a real drag
			if (!SelectedNodes.Any() || !IsRealDrag(_lastMouseDownLocation, e.Location))
			{
				return;
			}
			_modellerPresenter.DragInOperation = true;
			DragDropEffects result;
			Guid?[] occurrenceIds;
			try
			{
				var shapesToDrag = _presenter.GetDragDropCollection().ToArray();
				occurrenceIds = shapesToDrag.Where(s => s.OccurrenceId != null).Select(s => s.OccurrenceId).ToArray();
				var selection = _modellerPresenter.SelectShapes(shapesToDrag);
				result = DoDragDrop(selection.AsDataObject(), DragDropEffects.All);
				shapesToDrag.ForEach(s => s.Delete());
				_modellerPresenter.DragInOperation = false;
			}
			finally
			{
				_modellerPresenter.DragInOperation = false;
			}
			if (result != DragDropEffects.None)
			{
				_modellerPresenter.CreateConnectionsForOccurrences(occurrenceIds);
				_modellerPresenter.LayoutOccurrences(occurrenceIds);
			}
		}

		/// <summary>
		/// Handles the selected nodes changed event. This event is triggered for every node that has it's "selection" changed.
		/// I.e. if multiple nodes are selected a seperate event is fired for each.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleSelectedNodesChanged(object sender, RadTreeViewEventArgs e)
		{
			var args = new NodeSelectionChangedEventArguments(SelectedFolderIds, SelectedNodeIds);
			SelectedNodeChanged?.Invoke(sender, args);
			if (_modellerPresenter.SupressSelectionChange)
			{
				return;
			}
			var node = e.Node as BrowserNodeModellerTreeNode;
			if (node != null)
			{
				using (var scope = new PersistenceScope(TransactionOption.Required))
				{
					if (e.Node.Selected)
					{
						_modellerPresenter.SelectAllOccurrences(scope.FindById<ModelElement>(node.TargetId));
					}
					else
					{
						_modellerPresenter.DeselectAllOccurrences(scope.FindById<ModelElement>(node.TargetId));
					}
					scope.Done();
				}
				// Selected node changed will select the shapes on the page. Steal the focus back
				Focus();
				// Only definition nodes can be edited
				e.Node.TreeViewElement.AllowEdit = !node.IsDefinitionNode;
			}
			else
			{
				// Only definition nodes can be edited
				e.Node.TreeViewElement.AllowEdit = false;
			}
		}

		/// <summary>
		///     The nodes that have been selected
		/// </summary>
		public IEnumerable<Guid> SelectedNodeIds
		{
			get
			{
				var allSelectedNodes = SelectedNode == null
					? SelectedNodes
					: SelectedNodes.Union(new[] { SelectedNode });
				return allSelectedNodes.Where(n => n is BrowserNodeModellerTreeNode).Cast<BrowserNodeModellerTreeNode>().Select(n=> n.TargetId);
			}
		}


		/// <summary>
		/// The (ParentFolderID, FolderMemberId) of the selected folders
		/// </summary>
		public IEnumerable<TargetFolderId> SelectedFolderIds
		{
			get
			{
				var allSelectedNodes = SelectedNode == null 
					? SelectedNodes 
					: SelectedNodes.Union(new[] {SelectedNode});
				return allSelectedNodes.Where(n => n is BrowserFolderModellerTreeNode).Cast<BrowserFolderModellerTreeNode>().Select(n => n.TargetFolderId);
			}
		}

		public void NavigateToNode(Guid nodeId)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var browserNode = scope.FindById<BrowserNode>(nodeId);
				browserNode.Ascendants.OrderBy(a => a.Depth).Where(n => !n.IsRootNode).ForEach(Expand);
				scope.Done();
			}
			var modellerTreeNode = Nodes.Cast<BaseModellerTreeNode>()
				.SelectMany(n => n.Descendents).Where(n => n is BrowserNodeModellerTreeNode).Cast<BrowserNodeModellerTreeNode>()
				.FirstOrDefault(n => n.IsDefinitionNode &&  n.TargetId == nodeId);
			BringIntoView(modellerTreeNode);
			SelectedNodes.Clear();
			if (modellerTreeNode != null)
			{
				modellerTreeNode.Selected = true;
				modellerTreeNode.Current = true;
			}
		}

		private void Expand(BrowserNode node)
		{
			if (node.ParentFolder != null)
			{
				Expand(node.ParentFolder);
			}
			Nodes.Cast<BaseModellerTreeNode>()
				.SelectMany(n => n.Descendents).Where(n => n is BrowserNodeModellerTreeNode).Cast<BrowserNodeModellerTreeNode>()
				.SingleOrDefault(n => n.IsDefinitionNode && n.TargetId == node.Id)?.Expand();
		}
		private void Expand(BrowserFolder folder)
		{
			Nodes.Cast<BaseModellerTreeNode>().Where(n => n is BrowserFolderModellerTreeNode)
				.Union(Nodes.Cast<BaseModellerTreeNode>().SelectMany(n => n.Descendents).Where(n => n is BrowserFolderModellerTreeNode))
				.Cast<BrowserFolderModellerTreeNode>()
				.SingleOrDefault(n =>  n.TargetFolderId.FolderMemeberId == folder.MemberId && n.TargetFolderId.ParentNodeId == folder.Parent.Id)?.Expand();
		}

		public event EventHandler NodeExpanded;

		public void RefreshNodes(IEnumerable<BrowserNode> updatedNodes = null, IEnumerable<BrowserFolder> updatedFolders = null)
		{
			if ((!updatedNodes?.Any() ?? true) && (!updatedFolders?.Any() ?? true))
			{
				foreach (var node in Nodes)
				{
					((BaseModellerTreeNode)node).RefreshNode();
				}
				return;
			}
			// Update the nodes
			if (updatedNodes?.Any() ?? false) { 
				foreach (var updatedNode in updatedNodes)
				{
					Nodes.Cast<BaseModellerTreeNode>()
						.Union(Nodes.Cast<BaseModellerTreeNode>().SelectMany(n => n.Descendents))
						.Where(n => n is BrowserNodeModellerTreeNode && (updatedNode.Id == ((BrowserNodeModellerTreeNode) n).TargetId))
						.Cast<BrowserNodeModellerTreeNode>()
						.ForEach(n => n.RefreshNode(updatedNode));
				}
			}
			// Update the folders
			if (updatedFolders?.Any() ?? false) { 
				foreach (var updatedFolder in updatedFolders)
				{
					Nodes.Cast<BaseModellerTreeNode>()
						.Union(Nodes.Cast<BaseModellerTreeNode>().SelectMany(n => n.Descendents))
						.Where(n => n is BrowserFolderModellerTreeNode && (new TargetFolderId(updatedFolder.Parent.Id, updatedFolder.MemberId) == ((BrowserFolderModellerTreeNode)n).TargetFolderId))
						.Cast<BrowserFolderModellerTreeNode>()
						.ForEach(n => n.RefreshNode(updatedFolder));
				}
			}
		}

		public new event EventHandler<NodeSelectionChangedEventArguments> SelectedNodeChanged;

		/// <summary>
		/// Selects a tree node for a given browser node
		/// </summary>
		/// <param name="browserNode">The browser node to select</param>
		/// <param name="startEditForNewNode">If true this node will be treated as a new new for editing</param>
		public void SelectNode(BrowserNode browserNode, bool startEditForNewNode = false)
		{
			RefreshNodes();
			SelectedNodes.Clear();
			NavigateToNode(browserNode.Id);
			var node = Nodes.Cast<BaseModellerTreeNode>()
				.SelectMany(n => n.Descendents)
				.Where(n => n is BrowserNodeModellerTreeNode).First(n => ((BrowserNodeModellerTreeNode)n).TargetId == browserNode.Id && ((BrowserNodeModellerTreeNode)n).IsDefinitionNode) as BrowserNodeModellerTreeNode;
			SelectedNode = node;
			if (startEditForNewNode)
			{
				node.IsNewNew = true;
				node.BeginEdit();
			}
		}

		public void SelectNodes(IEnumerable<BrowserNode> browserNodes)
		{
			if (!browserNodes.Any())
			{
				return;
			}
			browserNodes.SelectMany(n => n.Ascendants).Distinct().OrderBy(a => a.Depth).Where(n => !n.IsRootNode).ForEach(Expand);
			NavigateToNode(browserNodes.Last().Id);
			SelectNodes(null, browserNodes.Select(n => n.Id));
		}

		/// <summary>
		///     Selects a set of nodes
		/// </summary>
		/// <param name="folderIds">The folders to sellect</param>
		/// <param name="nodeIds">The nodes to select</param>
		public void SelectNodes(IEnumerable<TargetFolderId> folderIds, IEnumerable<Guid> nodeIds)
		{
			SelectedNodes.Clear();
			var modellerNodes = Nodes.Cast<BaseModellerTreeNode>()
				.SelectMany(n => n.Descendents).Where(n => n is BrowserNodeModellerTreeNode)
				.Where(n => nodeIds != null && nodeIds.Contains(((BrowserNodeModellerTreeNode) n).TargetId))
				.Union(Nodes.Cast<BaseModellerTreeNode>()
					.SelectMany(n => n.Descendents).Where(n => n is BrowserFolderModellerTreeNode)
				.Where(n => folderIds != null && folderIds.Contains(((BrowserFolderModellerTreeNode) n).TargetFolderId)));
			foreach (var node in modellerNodes)
			{
				node.Selected = true;
			}
		}

		/// <summary>
		///     Determines if the mouse has moved far enough away from it's starting point to start a drag operation
		/// </summary>
		/// <param name="current"></param>
		/// <param name="capture"></param>
		/// <returns></returns>
		private bool IsRealDrag(Point current, Point capture)
		{
			var dragSize = SystemInformation.DragSize;
			var dragRect = new Rectangle(capture.X - dragSize.Width / 2, capture.Y - dragSize.Height / 2, dragSize.Width, dragSize.Height);
			return !dragRect.Contains(current) && ClientRectangle.Width-VScrollBar.Size.Width > current.X && ClientRectangle.Height-HScrollBar.Size.Height > current.Y;
		}
	}
}
