﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using System;
using System.Linq;
using MI.Modeller.Client.UserControls;

namespace MI.Modeller.Client.Visio2016.UserControls.ObjectBrowser
{
	public class BrowserFolderModellerTreeNode: BaseModellerTreeNode
	{
		public TargetFolderId TargetFolderId { get; }

		public BrowserFolderModellerTreeNode(BrowserFolder browserFolder, IObjectBrowserPresenter presenter, BaseModellerTreeNode parent = null) : base(presenter)
		{
			browserFolder = ArgumentValidation.AssertNotNull(browserFolder, nameof(browserFolder));
			TargetFolderId = new TargetFolderId(browserFolder.Parent.Id, browserFolder.MemberId);
			parent?.Nodes.Add(this);
			Image = Properties.Resources.Folder;
			if (parent?.Expanded ?? false)
			{
				foreach (var n in browserFolder.Nodes)
				{
					new BrowserNodeModellerTreeNode(n, presenter, this);
				}
			}
		}

		internal sealed override void RefreshNode()
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{			
				var targetFolder = scope.FindById<ParentNode>(TargetFolderId.ParentNodeId)
					.AllFolders
					.Single(f => f.MemberId == TargetFolderId.FolderMemeberId);
				RefreshNode(targetFolder);
				scope.Done();
			}
		}

		internal void RefreshNode(BrowserFolder targetFolder)
		{
			if (targetFolder != null)
			{
				if (new TargetFolderId(targetFolder.Parent.Id, targetFolder.MemberId) != TargetFolderId)
				{
					throw new ArgumentException("Node update is called for a different folder than this node is for");
				}
				Text = targetFolder.DisplayName;
				// Only need to update the children if the parent is expanded
				// Only Definition Nodes will have children
				if (Parent == null || Parent.Expanded)
				{
					RefreshNodes(targetFolder);
					foreach (var n in Nodes.Where(n => n is BrowserNodeModellerTreeNode))
					{
						var targetNode = targetFolder.Nodes.Single(fn => fn.Id == ((BrowserNodeModellerTreeNode) n).TargetId);
						((BrowserNodeModellerTreeNode) n).RefreshNode(targetNode);
					}
				}
			}
			else
			{
				Nodes.Clear();
			}
		}

		private void RefreshNodes(BrowserFolder browserFolder)
		{
			var currentNodes = Nodes.Where(n => n is BrowserNodeModellerTreeNode)
				.Cast<BrowserNodeModellerTreeNode>()
				.Select(n => n.TargetId)
				.ToArray();
			var targetNodes = browserFolder.Nodes.Where(n => TreeView.Filter?.IsVisible(n) ?? true);
			// Remove nodes that aren't in the list of nodes to have
			foreach (var n in currentNodes.Where(n => !targetNodes.Select(fn => fn.Id).Contains(n)))
			{
				var nodeToRemove = Nodes.Single(node => (node as BrowserNodeModellerTreeNode)?.TargetId == n);
				if (Nodes.Contains(nodeToRemove))
				{
					Nodes.Remove(nodeToRemove);
				}
			}
			// Add nodes that aren't in the list of current nodes
			foreach (var n in targetNodes.Where(n => !currentNodes.Contains(n.Id)))
			{
				new BrowserNodeModellerTreeNode(n, Presenter, this);
			}
		}
	}
}
