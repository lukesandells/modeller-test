﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.PropertyPane;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	public sealed class ModellerPropertyStoreItem : PropertyStoreItem
    {
        #region  Fields

        public EditorType EditorType;
        public IEnumerable<string> DropDownOptions;

		public PropertyConfiguration Configuration { get; }
	    public HierarchyScope HierarchyScope { get; }

	    private readonly Guid _modelElementId;
	    private readonly IPropertyPanePresenter _presenter;

	    #endregion

		#region Constructors

		public ModellerPropertyStoreItem(PropertyConfiguration configuration, ModelElement modelElement, IPropertyPanePresenter presenter) :
			base(typeof(string), configuration.DisplayName, modelElement.Properties[configuration.MemberId].Value)
		{
			_presenter = ArgumentValidation.AssertNotNull(presenter, nameof(presenter));
			Configuration = configuration;
			_modelElementId = modelElement.Id;
			var h = modelElement.DesignObject as HierarchyScopedObject;
			HierarchyScope = h?.HierarchyScope;
			Category = configuration.GroupName;
			if (configuration.IsNodeType)
			{
				EditorType = EditorType.ModelObject;
				PropertyType = typeof(BrowserNode);
			}

			if (configuration.NativeType.IsEnum || configuration.NativeType.IsExtensibleEnum())
			{
				EditorType = EditorType.DropDown;
				DropDownOptions = configuration.NativeType.GetEnumDisplayNames().Select(dropdownOption => dropdownOption.Value).ToList();
				Value = modelElement.Properties[configuration.MemberId].Value != null ? ExtensibleEnum.GetEnumValueDisplayName(modelElement.Properties[configuration.MemberId].Value) : null;
			}
			PropertyChanged += HandlePropertyChanged;
			ReadOnly = configuration.IsReadOnly;
		}

		#endregion

		#region Members

		private void HandlePropertyChanged(object sender,
            PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Value")
            {
	            var result = _presenter.UpdateProperty(_modelElementId, Configuration, Value);
	            if (string.IsNullOrEmpty(result))
	            {
		            return;
	            }
	            MessageBox.Show(result);
	            ResetValue();
            }
        }

        #endregion
    }
}