﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Validation;
using MI.Modeller.Client.Visio2016.UserControls.Editors;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
    public partial class MaterialSpecificationUserControl<TOfProcess> : UserControl  where TOfProcess: class, IProcessDefinition<TOfProcess>
    {
	    private ModelElement _processNode;
	    private IDictionary<BrowserNode,MaterialSpecification<TOfProcess>> _specificationNodes;

	    #region Constructors

        public MaterialSpecificationUserControl(ModelElement process)
        {
            InitializeComponent();
            radGridView1.EditorRequired += HandleEditorRequired;
            foreach (var radItem in radMenu1.Items)
            {
                var item = (RadMenuItem) radItem;
                item.ScaleTransform = new SizeF(this.ScaleFactor(), this.ScaleFactor());
            }
	        _processNode = ArgumentValidation.AssertNotNull(process, nameof(process));
	        _specificationNodes = new Dictionary<BrowserNode, MaterialSpecification<TOfProcess>>();
			var topLevelSpecifications = _processNode.FolderWithMemberId(NodeTypeMember.Folder.MaterialSpecifications).Nodes.ToArray();
			topLevelSpecifications.Union(topLevelSpecifications.SelectMany(n => n.Descendants.Where(d => d.TypeId == n.TypeId))).ForEach(n => _specificationNodes.Add(n, (MaterialSpecification<TOfProcess>) n.TargetObject));
	        radGridView1.DataSource = _specificationNodes;
	        radGridView1.AllowAddNewRow = false;
	        radGridView1.BestFitColumns();
			//radGridView1.Relations.AddSelfReference(radGridView1.MasterTemplate,
			//	"InternalId",
			//	"ParentId");
		}

        #endregion

        #region Other Methods

        #region Members

        private void HandleEditorRequired(object sender,
            EditorRequiredEventArgs e)
        {
            if (radGridView1.CurrentColumn.Name == "HierarchyScopeColumn")
            {
                e.EditorType = typeof(ModellerPropertyGridSelectObjectEditor);
            }
        }

        #endregion

        #endregion
    }
}