﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.Visio2016.Forms;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	public class BasicPropertiesUserControl<TOfObject> : AbstractPropertiesUserControl<BasicProperty<TOfObject>, TOfObject>
		where TOfObject: class, IWithBasicProperties<TOfObject>, IWithExternalId
    {
	    private TOfObject _basicPropertyObject;
	    private BrowserNode _node;

	    #region Constructors

        /// <summary>
        ///     Parameterless constructor for designer only
        /// </summary>
        protected BasicPropertiesUserControl() {}

	    public BasicPropertiesUserControl(BrowserNode node, TOfObject modelObject) : base(modelObject)
	    {
		    _basicPropertyObject = modelObject;
		    _node = ArgumentValidation.AssertNotNull(node, nameof(node));
			Presenter = new BasicPropertyTabPresenter<TOfObject>(_node);
	    }

        #endregion

        #region Members

	    protected override IPropertyTabPresenter<BasicProperty<TOfObject>, TOfObject> Presenter { get; }

	    #endregion
    }
}