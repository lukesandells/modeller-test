using MI.Framework;
using MI.Framework.Units;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.Visio2016.UserControls.Editors;
using MI.OperationsManagement.Domain.Core.DataTypes;
using System;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;

namespace MI.Modeller.Client.Visio2016.UserControls
{
	public partial class ValueDetailUserControl : UserControl, IUserControl
	{
		#region Constructors

		/// <summary>
		///     Paramaterless contructor
		/// </summary>
		public ValueDetailUserControl()
		{
			InitializeComponent();
		}

		/// <summary>
		///     Creates new instance of the BasicProeprtiesDetailUserControl
		/// </summary>
		/// <param name="value">The value to edit</param>
		public ValueDetailUserControl(Value value)
		{
			InitializeComponent();
			_valueElements = new BindingList<SerialisedValue.Element>();
			value?.Serialise().Elements.ForEach(_valueElements.Add);
			ValuesGridView.DataSource = _valueElements;
			ValuesGridView.MasterView.Refresh();
			ValuesGridView.Refresh();
			ValuesGridView.EditorRequired += HandleEditorRequired;
			((GridViewComboBoxColumn)ValuesGridView.Columns[keyColumn.Name]).DataSource = new BindingList<string>(UnitLibrary.Current.PhysicalProperties.Select(p => p.Name).ToList());
			((GridViewComboBoxColumn)ValuesGridView.Columns[dataTypeColumn.Name]).DataSource = new BindingList<string>(((ValueDataType[])Enum.GetValues(typeof(ValueDataType))).Where(v => v.IsSupported()).Select(dt => dt.ToString()).ToList());

			// We need to sort out the unit of measure when the key or data type changes
			((GridViewComboBoxColumn)ValuesGridView.Columns[keyColumn.Name]).PropertyChanged += (s, a) => PopulateUnitOfMeasureForCurrentRow();
			((GridViewComboBoxColumn)ValuesGridView.Columns[dataTypeColumn.Name]).PropertyChanged += (s, a) => PopulateUnitOfMeasureForCurrentRow();
			// It's different for each row depending on data
			ValuesGridView.CurrentRowChanged += (s, a) => PopulateUnitOfMeasureForCurrentRow();
			// And the first row must be setup on initialise
			PopulateUnitOfMeasureForCurrentRow();
		}

		private void HandleEditorRequired(object sender, EditorRequiredEventArgs e)
		{
			if (e.EditorType == typeof(RadDropDownListEditor) && ValuesGridView.CurrentColumn == keyColumn)
			{
				e.Editor = new ExtendedDropDownListEditor((BindingList<string>) ((GridViewComboBoxColumn) ValuesGridView.CurrentColumn).DataSource);
			}
		}

		/// <summary>
		/// Sets up the unit of measure information for the current row
		/// </summary>
		private void PopulateUnitOfMeasureForCurrentRow()
		{
			var row = ValuesGridView.CurrentRow;
			// Value may be null which will throw an exception when trying to parse
			if (row.Cells[dataTypeColumn.Name].Value == null)
			{
				ClearAndDisableUnitOfMeasure(row);
				return;
			}
			// Data type is only for amounts and measures
			var dataType = ExtensibleEnum<ValueDataType>.Parse(row.Cells[dataTypeColumn.Name].Value?.ToString());
			if (dataType == ValueDataType.Quantity || dataType == ValueDataType.Measure)
			{
				PopulateUnitOfMeasure(row);
			}
			else
			{
				ClearAndDisableUnitOfMeasure(row);
			}
		}

		/// <summary>
		/// Populates the unit of measure column for the given row with measures from the selected property
		/// </summary>
		/// <param name="row"></param>
		private void PopulateUnitOfMeasure(GridViewRowInfo row)
		{
			row.Cells[unitOfMeasureColumn.Name].ReadOnly = false;
			var physicalProperty = UnitLibrary.Current.PhysicalProperties.SingleOrDefault(p => p.Name == row.Cells[keyColumn.Name].Value?.ToString());
			((GridViewComboBoxColumn)ValuesGridView.Columns[unitOfMeasureColumn.Name]).DataSource = 
				physicalProperty?.Units.ToDictionary(u => u.Code, u => u.Symbol) ?? UnitLibrary.Current.UnitsByCode.Values.ToDictionary(u => u.Code, u => u.Symbol);
			((GridViewComboBoxColumn)ValuesGridView.Columns[unitOfMeasureColumn.Name]).DisplayMember = "Value";
			((GridViewComboBoxColumn)ValuesGridView.Columns[unitOfMeasureColumn.Name]).ValueMember = "Key";
		}

		/// <summary>
		/// Clears the unit of measure and disables the column
		/// </summary>
		/// <param name="row"></param>
		private void ClearAndDisableUnitOfMeasure(GridViewRowInfo row)
		{
			row.Cells[unitOfMeasureColumn.Name].Value = null;
			row.Cells[unitOfMeasureColumn.Name].ReadOnly = true;
		}

		#endregion

		#region Other Methods

		/// <summary>
		/// Wheather or not the value being created is valid. Errors will be displayed and the row with the issue will be put in edit mode
		/// </summary>
		public bool IsValid
		{
			get
			{
				{
					// Add the values one at a time so we can isolate the first one to have an error
					var newValue = new SerialisedValue();
					foreach (var el in _valueElements)
					{
						newValue.Elements.Add(el);
						try
						{
							newValue.Deserialise();
						}
						catch (Exception ex)
						{
							// If ther eis an error show the message and put the problematic row into edit mode
							MessageBox.Show(ex.Message);
							var row = ValuesGridView.Rows.Single(r => r.DataBoundItem == el);
							row.IsCurrent = true;
							row.Cells[0].BeginEdit();
							return false;
						}
					}
					return true;
				}
			}
		}

		#endregion

		#region  Fields

		/// <summary>
		///     The value being updated
		/// </summary>
		public Value Value
		{
			get
			{
				var newValue = new SerialisedValue();
				_valueElements.ForEach(e => newValue.Elements.Add(e));
				return newValue.Deserialise();
			}
		}

		/// <summary>
		/// Binding list for the individual serialised elements being edited - bound to the GridView
		/// </summary>
		private readonly BindingList<SerialisedValue.Element> _valueElements;

		#endregion
	}
}