namespace MI.Modeller.Client.Visio2016.UserControls
{
	partial class ValueDetailUserControl
	{
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			dataTypeColumn = new Telerik.WinControls.UI.GridViewComboBoxColumn();
			keyColumn = new Telerik.WinControls.UI.GridViewComboBoxColumn();
			Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
			unitOfMeasureColumn = new Telerik.WinControls.UI.GridViewComboBoxColumn();
			Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
			this.ValuesGridView = new Telerik.WinControls.UI.RadGridView();
			((System.ComponentModel.ISupportInitialize)(this.ValuesGridView)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.ValuesGridView.MasterTemplate)).BeginInit();
			this.SuspendLayout();
			// 
			// ValuesGridView
			// 
			this.ValuesGridView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ValuesGridView.Location = new System.Drawing.Point(0, 0);
			// 
			// 
			// 
			this.ValuesGridView.MasterTemplate.AddNewRowPosition = Telerik.WinControls.UI.SystemRowPosition.Bottom;
			this.ValuesGridView.MasterTemplate.AutoSizeColumnsMode = Telerik.WinControls.UI.GridViewAutoSizeColumnsMode.Fill;
			dataTypeColumn.FieldName = "DataType";
			dataTypeColumn.HeaderText = "Data Type";
	        dataTypeColumn.Name = "DataTypeColumn";
			dataTypeColumn.Width = 108;
			keyColumn.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
			keyColumn.FieldName = "Key";
			keyColumn.HeaderText = "Key";
	        keyColumn.Name = "KeyColumn";
			keyColumn.Width = 274;
			gridViewTextBoxColumn1.FieldName = "Value";
			gridViewTextBoxColumn1.HeaderText = "Value";
			gridViewTextBoxColumn1.Name = "ValueColumn";
			gridViewTextBoxColumn1.Width = 66;
			unitOfMeasureColumn.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDown;
			unitOfMeasureColumn.FieldName = "UnitCode";
			unitOfMeasureColumn.HeaderText = "Unit of Measure";
	        unitOfMeasureColumn.Name = "UnitOfMeasureColumn";
			unitOfMeasureColumn.Width = 102;
			this.ValuesGridView.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            dataTypeColumn,
            keyColumn,
            gridViewTextBoxColumn1,
            unitOfMeasureColumn});
			this.ValuesGridView.MasterTemplate.ViewDefinition = tableViewDefinition1;
			this.ValuesGridView.Name = "ValuesGridView";
			this.ValuesGridView.Size = new System.Drawing.Size(568, 387);
			this.ValuesGridView.TabIndex = 0;
			this.ValuesGridView.Text = "GridView";
			// 
			// ValueDetailUserControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.ValuesGridView);
			this.Name = "ValueDetailUserControl";
			this.Size = new System.Drawing.Size(568, 387);
			((System.ComponentModel.ISupportInitialize)(this.ValuesGridView.MasterTemplate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.ValuesGridView)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView ValuesGridView;
		private Telerik.WinControls.UI.GridViewComboBoxColumn dataTypeColumn;
		private Telerik.WinControls.UI.GridViewComboBoxColumn keyColumn;
		private Telerik.WinControls.UI.GridViewComboBoxColumn unitOfMeasureColumn;
	}
}
