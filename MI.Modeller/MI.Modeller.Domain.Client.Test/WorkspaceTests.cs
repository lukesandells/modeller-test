﻿//using System.Collections.Generic;
//using System.Linq;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class WorkspaceTests.
//    /// </summary>
//    [TestFixture]
//    public class WorkspaceTests
//    {

//        /// <summary>
//        ///     Tests the add hierarchy scope hierarchy scope added.
//        /// </summary>
//        [Test]
//        public void Test_AddHierarchyScope_HierarchyScopeAdded()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var hierarchyScope = new HierarchyScope("EquipmentId",
//                EquipmentLevel.Enterprise);
//            var hierarchyScope2 = new HierarchyScope("EquipmentId2",
//                EquipmentLevel.Enterprise);

//            // Act
//            workspace.Add(hierarchyScope);
//            workspace.Add(hierarchyScope2);

//            // Assert
//            var returnedScope = (HierarchyScope)workspace.FindByGuid(hierarchyScope.InternalId);
//            Assert.IsNotNull(returnedScope,
//                "Workspace GetHierarchycope resulted in a null object reference");
//            Assert.AreEqual("EquipmentId",
//                returnedScope.EquipmentId,
//                "Workspace GetHierarchycope resulted in an object with the incorrect EquipmentId");
//            Assert.AreEqual(EquipmentLevel.Enterprise,
//                returnedScope.EquipmentLevel.Type,
//                "Workspace GetHierarchycope resulted in an object with the incorrect EquipmentLevel.Type");
//        }

//        /// <summary>
//        ///     Tests the add model existing model ignores model.
//        /// </summary>
//        [Test]
//        public void Test_AddModel_ExistingModel_IgnoresModel()
//        {
//            // Arrange
//            var hierarchyScope = new HierarchyScope("EquipmentId",
//                EquipmentLevel.Enterprise);
//            var workspace = new Workspace();
//            WorkspaceEventArgs eventArgs = null;
//            var model1 = new Model(hierarchyScope,
//                "Name",
//                ModelType.WorkDefinitionModel);
//            workspace.AddModel(model1);

//            workspace.ModelAdded += delegate (object sender,
//                WorkspaceEventArgs e)
//            {
//                eventArgs = e;
//            };

//            // Act
//            workspace.AddModel(model1);

//            // assert
//            CollectionAssert.Contains(workspace.Models,
//                model1,
//                "Model was not added to the workspace Models collection");
//            Assert.AreEqual(1,
//                workspace.Models.Count(),
//                "Workspace does not contain the same numbe rof Models that were added to it");
//            Assert.IsNull(eventArgs,
//                "Adding an existing model incorrectly triggered AddModel event");
//        }

//        /// <summary>
//        ///     Tests the add model triggers model added event.
//        /// </summary>
//        [Test]
//        public void Test_AddModel_TriggersModelAddedEvent()
//        {
//            // Arrange
//            var hierarchyScope = new HierarchyScope("EquipmentId",
//                EquipmentLevel.Enterprise);
//            var workspace = new Workspace();
//            var model = new Model(hierarchyScope,
//                "Name",
//                ModelType.WorkDefinitionModel);
//            WorkspaceEventArgs eventArgs = null;

//            workspace.ModelAdded += delegate (object sender,
//                WorkspaceEventArgs e)
//            {
//                eventArgs = e;
//            };

//            // Act
//            workspace.AddModel(model);

//            // assert
//            Assert.IsNotNull(eventArgs,
//                "Adding a model to a workspace did not trigger the relevant event");
//            Assert.AreEqual(eventArgs.Model,
//                model,
//                "Adding a model to a workspace adde dthe incorrect object");
//        }

//        /// <summary>
//        ///     Tests the add model valid model adds model.
//        /// </summary>
//        [Test]
//        public void Test_AddModel_ValidModel_AddsModel()
//        {
//            // Arrange
//            var hierarchyScope = new HierarchyScope("EquipmentId",
//                EquipmentLevel.Enterprise);
//            var workspace = new Workspace();
//            var model1 = new Model(hierarchyScope,
//                "Name",
//                ModelType.WorkDefinitionModel);
//            var model2 = new Model(hierarchyScope,
//                "Name2",
//                ModelType.WorkDefinitionModel);

//            // Act
//            workspace.AddModel(model1);
//            workspace.AddModel(model2);

//            // assert
//            CollectionAssert.Contains(workspace.Models,
//                model1,
//                "Model was not added to the workspace Models collection");
//            CollectionAssert.Contains(workspace.Models,
//                model2,
//                "Model was not added to the workspace Models collection");
//            Assert.AreEqual(2,
//                workspace.Models.Count(),
//                "Workspace does not contain the same numbe rof Models that were added to it");
//        }

//        /// <summary>
//        ///     Tests the add object triggers event.
//        /// </summary>
//        /// <param name="modelObject">The modeller object.</param>
//        [Test]
//        public void Test_AddObject_TriggersEvent(
//            [ValueSource(typeof(RootModelObjectDataGenerator),
//                 nameof(RootModelObjectDataGenerator.GenerateValidRootModelObjects))] RootModelObject modelObject)
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var eventArgs = new HashSet<WorkspaceEventArgs>();

//            workspace.ObjectAdded += delegate (object sender,
//                WorkspaceEventArgs e)
//            {
//                eventArgs.Add(e);
//            };

//            // Act
//            workspace.Add(modelObject);

//            // Assert
//            Assert.IsNotNull(eventArgs,
//                "Adding An Object did not result in the ObjectAdded event being triggered as expected");
//            Assert.IsNotNull(eventArgs.FirstOrDefault(e => ReferenceEquals(e.ModelObject,
//                    modelObject)),
//                "Event was not triggered for the added Object as expected");
//        }

//        /// <summary>
//        ///     Tests the add object valid object adds object.
//        /// </summary>
//        /// <param name="modelObject">The modeller object.</param>
//        [Test]
//        public void Test_AddObject_ValidObject_AddsObject(
//            [ValueSource(typeof(RootModelObjectDataGenerator),
//                 nameof(RootModelObjectDataGenerator.GenerateValidRootModelObjects))] RootModelObject modelObject)
//        {
//            // Arrange
//            var workspace = new Workspace();

//            // Act
//            workspace.Add(modelObject);

//            // Assert
//            CollectionAssert.Contains(workspace.ModelObjects,
//                modelObject,
//                "Object Added to workspace was not added");
//        }

//        /// <summary>
//        ///     Tests the add object with children adds all objects.
//        /// </summary>
//        [Test]
//        [Ignore("Is this true? if so we need to work out how this afects persistent workspace")]
//        // TODO investigate
//        public void Test_AddObjectWithChildren_AddsAllObjects()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var hierarchyScope = new HierarchyScope("EquipmentId",
//                EquipmentLevel.Enterprise);
//            var workMaster2 = new WorkMaster("wm2",
//                hierarchyScope: hierarchyScope);
//            var workMaster3 = new WorkMaster("wm3",
//                hierarchyScope: hierarchyScope);

//            var workMaster = new WorkMaster("wm1",
//                hierarchyScope: hierarchyScope,
//                workType: new ProcessType(ProcessType.Maintenance));
//            workMaster.AddChild(workMaster2);
//            workMaster.AddChild(workMaster3);

//            // Act
//            workspace.Add(workMaster);

//            // Assert
//            Assert.AreEqual(workMaster,
//                workspace.FindByGuid(workMaster.InternalId),
//                "WorkSpace AddObject did not Add Object as requested");
//            Assert.AreEqual(workMaster2,
//                workspace.FindByGuid(workMaster2.InternalId),
//                "WorkSpace AddObject did not Add Object as requested");
//            Assert.AreEqual(workMaster3,
//                workspace.FindByGuid(workMaster3.InternalId),
//                "WorkSpace AddObject did not Add Object as requested");
//            Assert.AreEqual(hierarchyScope,
//                workspace.FindByGuid(hierarchyScope.InternalId),
//                "WorkSpace AddObject did not Add Object as requested");
//        }

//        /// <summary>
//        ///     Tests the get object returns correct object.
//        /// </summary>
//        /// <param name="modelObject">The modeller object.</param>
//        [Test]
//        public void Test_GetObject_ReturnsCorrectObject(
//            [ValueSource(typeof(RootModelObjectDataGenerator),
//                 nameof(RootModelObjectDataGenerator.GenerateValidRootModelObjects))] RootModelObject modelObject)
//        {
//            // Arrange
//            var workspace = new Workspace();
//            workspace.Add(modelObject);

//            // Act
//            var workspaceObject = workspace.FindByGuid(modelObject.InternalId);

//            // Assert
//            Assert.IsNotNull(workspaceObject,
//                "GetObject did not return the object that was added to the workspace");
//            Assert.AreEqual(modelObject,
//                workspaceObject,
//                "Object returned by GetObject was notthe expected object");
//        }

//        /// <summary>
//        ///     Tests the remove object object does not exist ignores object.
//        /// </summary>
//        [Test]
//        public void Test_RemoveObject_ObjectDoesNotExist_IgnoresObject()
//        {
//            // check event not triggered
//            // Arrange
//            var workspace = new Workspace();
//            var equipmentClass = EquipmentClassDataGenerator.GenerateValidEquipmentClasses()
//                .First();
//            WorkspaceEventArgs eventArgs = null;

//            workspace.ObjectRemoved += delegate (object sender,
//                WorkspaceEventArgs e)
//            {
//                eventArgs = e;
//            };

//            // Act
//            workspace.Remove(equipmentClass);

//            // Assert
//            Assert.IsNull(eventArgs,
//                "Calling RemoveObject with an object that does not exist in the workspace incorrectly triggered a RemoveObject event");
//        }

//        /// <summary>
//        ///     Tests the remove object removes object.
//        /// </summary>
//        /// <param name="modelObject">The modeller object.</param>
//        [Test]
//        public void Test_RemoveObject_RemovesObject(
//            [ValueSource(typeof(RootModelObjectDataGenerator),
//                 nameof(RootModelObjectDataGenerator.GenerateValidRootModelObjects))] RootModelObject modelObject)
//        {
//            // Arrange
//            var workspace = new Workspace();
//            workspace.Add(modelObject);

//            // Act
//            workspace.Remove(modelObject);

//            // Assert
//            Assert.IsNull(workspace.FindByGuid(modelObject.InternalId),
//                "Removing an object from the workspace did not remove the item from the objects");
//        }

//        /// <summary>
//        ///     Tests the remove object triggers event.
//        /// </summary>
//        /// <param name="modelObject">The modeller object.</param>
//        [Test]
//        public void Test_RemoveObject_TriggersEvent(
//            [ValueSource(typeof(RootModelObjectDataGenerator),
//                 nameof(RootModelObjectDataGenerator.GenerateValidRootModelObjects))] RootModelObject modelObject)
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var eventArgs = new WorkspaceEventArgs();
//            workspace.Add(modelObject);

//            workspace.ObjectRemoved += delegate (object sender,
//                WorkspaceEventArgs e)
//            {
//                eventArgs = e;
//            };

//            // Act
//            workspace.Remove(modelObject);

//            // Assert
//            Assert.IsNotNull(eventArgs,
//                "Removing an object from the workspace did not Trigger the ObjectRemovedEvent as expected");
//            Assert.AreEqual(modelObject,
//                eventArgs.ModelObject,
//                "Event Arguments did not contain the removed object as expected");
//        }

//        [Test]
//        public void Test_FindById_Found()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA));
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));


//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Assert/Act
//            Assert.AreEqual(equipment, workspace.FindById(ModelObjectType.Equipment, "Ent-SiteA", "Equipment"), "Equipment not found in its Hierarchy Scope");
//            Assert.AreEqual(equipment, workspace.FindById(ModelObjectType.Equipment, "Ent", "Equipment"), "Equipment not found in parent Hierarchy Scope");
//            Assert.AreEqual(equipment, workspace.FindById(ModelObjectType.Equipment, "Ent-SiteA-AreaA", "Equipment"), "Equipment not found in child Hierarchy Scope");
//        }

//        [Test]
//        public void Test_FindById_HierarchyFound()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA));
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));


//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Assert/Act
//            Assert.AreEqual(siteA, workspace.FindById(ModelObjectType.HierarchyScope, null, "Ent-SiteA"), "HierarchyScope not found");
//        }

//        [Test]
//        public void Test_FindById_Wrong_ObjectType_NotFound()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA));
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));

//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Assert/Act
//            Assert.IsNull(workspace.FindById(ModelObjectType.EquipmentClass, "Ent-SiteA", "Equipment"), "Equipment was found in its Hierarchy Scope");
//            Assert.IsNull(workspace.FindById(ModelObjectType.WorkMaster, "Ent", "Equipment"), "Equipment was found in parent Hierarchy Scope");
//            Assert.IsNull(workspace.FindById(ModelObjectType.MaterialClass, "Ent-SiteA-AreaA", "Equipment"), "Equipment was found in child Hierarchy Scope");
//        }

//    [Test]
//        public void Test_FindById_GenericType_Found()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA));
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));


//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Assert/Act
//            Assert.AreEqual(equipment, workspace.FindById<Equipment>("Ent-SiteA", "Equipment"), "Equipment not found in its Hierarchy Scope");
//            Assert.AreEqual(equipment, workspace.FindById<Equipment>("Ent", "Equipment"), "Equipment not found in parent Hierarchy Scope");
//            Assert.AreEqual(equipment, workspace.FindById<Equipment>("Ent-SiteA-AreaA", "Equipment"), "Equipment not found in child Hierarchy Scope");
//        }

//        [Test]
//        public void Test_FindById_GenericType_HierarchyFound()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA));
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));


//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Assert/Act
//            Assert.AreEqual(siteA, workspace.FindById<HierarchyScope>(null, "Ent-SiteA"), "HierarchyScope not found");
//        }

//        [Test]
//        public void Test_FindById_Wrong_ObjectType_GenericType_NotFound()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA));
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));
            
//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Assert/Act
//            Assert.IsNull(workspace.FindById<EquipmentClass>("Ent-SiteA", "Equipment"), "Equipment was found in its Hierarchy Scope");
//            Assert.IsNull(workspace.FindById<WorkMaster>("Ent", "Equipment"), "Equipment was found in parent Hierarchy Scope");
//        }

//        [Test]
//        public void Test_ContainsWithinScope()
//        {
//            // Arrange
//            var workspace = new Workspace();
//            var ent = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            workspace.Add(ent);
//            var siteA = new HierarchyScope("Ent-SiteA", EquipmentLevel.Site, ent);
//            workspace.Add(siteA);
//            var areaA = new HierarchyScope("Ent-SiteA-AreaA", EquipmentLevel.Area, siteA);
//            workspace.Add(areaA);
//            workspace.Add(new HierarchyScope("Ent-SiteA-AreaB", EquipmentLevel.Area, siteA));
//            var siteB = new HierarchyScope("Ent-SiteB", EquipmentLevel.Site, ent);
//            workspace.Add(siteB);
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaA", EquipmentLevel.Area, siteB));
//            workspace.Add(new HierarchyScope("Ent-SiteB-AreaB", EquipmentLevel.Area, siteB));


//            var equipment = new Equipment(siteA, "Equipment",
//                new EquipmentLevelType(EquipmentLevelTypeValue.WorkCenter));
//            workspace.Add(equipment);

//            // Act/Assert
//            Assert.True(workspace.ContainsWithinScope<Equipment>("Ent", "Equipment"), "Equipment not found in expected HierarchyScope");
//            Assert.True(workspace.ContainsWithinScope<Equipment>("Ent-SiteA", "Equipment"), "Equipment not found in expected HierarchyScope");
//            Assert.True(workspace.ContainsWithinScope<Equipment>("Ent-SiteA-AreaA", "Equipment"), "Equipment not found in expected HierarchyScope");
//            Assert.False(workspace.ContainsWithinScope<Equipment>("Ent-SiteB", "Equipment"), "Equipment found in unexpected HierarchyScope");
//            Assert.False(workspace.ContainsWithinScope<Equipment>("Ent-SiteB-AreaA", "Equipment"), "Equipment found in unexpected HierarchyScope");
//            Assert.False(workspace.ContainsWithinScope<Equipment>("Ent-SiteB-AreaB", "Equipment"), "Equipment found in unexpected HierarchyScope");
//        }
//    }
//}