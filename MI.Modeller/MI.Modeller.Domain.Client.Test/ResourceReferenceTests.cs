﻿//using System.Collections.Generic;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class ResourceReferenceTests.
//    /// </summary>
//    [TestFixture]
//    public class ResourceReferenceTests
//    {
//        /// <summary>
//        ///     Setups this instance.
//        /// </summary>
//        [SetUp]
//        public void Setup()
//        {
//            Helpers.Num = 10;
//        }

//        /// <summary>
//        ///     Tests the constructor with valid resource properties returns valid instance.
//        /// </summary>
//        /// <param name="resourceProperties">The resource properties.</param>
//        public void Test_Constructor_WithValidResourceProperties_ReturnsValidInstance(
//            [ValueSource(typeof(Helpers), nameof(Helpers.BuildBasicProperties))] IList<BasicProperty>
//                resourceProperties)
//        {
//            // Arrange
//            const string id = "ResourceReference.ID";
//            const string resourceId = "ResourceReference ResourceId";

//            // Act
//            var newResourceReference = new ResourceReference(id,
//                resourceId,
//                resourceProperties: resourceProperties);

//            // Assert
//            Assert.IsNotNull(newResourceReference,
//                "Instance Creation of ResourceReference object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newResourceReference.Id,
//                "ResourceReference Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(resourceId,
//                newResourceReference.ResourceId,
//                "ResourceReference Creation resulted in an object with the incorrect ResourceId");

//            Helpers.CompareTwoCollections(resourceProperties,
//                newResourceReference.ResourceProperties,
//                "ResourceProperty");
//        }
//    }
//}