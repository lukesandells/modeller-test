﻿//using System;
//using System.Collections.Generic;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class ResourceRelationshipNetworkTests.
//    /// </summary>
//    [TestFixture]
//    public class ResourceRelationshipNetworkTests
//    {
//        /// <summary>
//        ///     Setups this instance.
//        /// </summary>
//        [SetUp]
//        public void Setup()
//        {
//            Helpers.Num = 10;
//        }

//        /// <summary>
//        ///     Tests the constructor with valid values returns valid instance.
//        /// </summary>
//        /// <param name="resourceNetworkConnections">The resource network connections.</param>
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance(
//            [ValueSource(typeof(Helpers), nameof(Helpers.BuildResourceNetworkConnections))] ISet<ResourceNetworkConnection> resourceNetworkConnections)
//        {
//            // Arrange
//            const string id = "ID";
//            var hierarchyScope = new HierarchyScope("Equip.ID",
//                EquipmentLevel.EquipmentModule);
//            const string description = "ResourceRelationshipNetwork Description";
//            var publishedDate = DateTime.Today;
//            var relationshipType = ResourceRelationshipType.Logical;
//            var relationshipForm = ResourceRelationshipFormType.Permanent;

//            // Act
//            var newResourceRelationshipNetwork = new ResourceRelationshipNetwork(id,
//                description,
//                hierarchyScope,
//                publishedDate: publishedDate,
//                relationshipType: relationshipType,
//                relationshipForm: relationshipForm,
//                resourceNetworkConnections: resourceNetworkConnections);

//            // Assert
//            Assert.IsNotNull(newResourceRelationshipNetwork,
//                "Instance Creation of ResourceRelationshipNetwork object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newResourceRelationshipNetwork.Id,
//                "ResourceRelationshipNetwork Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(hierarchyScope,
//                newResourceRelationshipNetwork.HierarchyScope,
//                "ResourceRelationshipNetwork Creation resulted in an object with the incorrect HierarchyScope");
//            Assert.AreEqual(description,
//                newResourceRelationshipNetwork.Description,
//                "ResourceRelationshipNetwork Creation resulted in an object with the incorrect Description");
//            Assert.AreEqual(relationshipType.ToString(),
//                newResourceRelationshipNetwork.RelationshipType.ToString(),
//                "ResourceRelationshipNetwork Creation resulted in an object with the incorrect CollectionRelationshipType");
//            Assert.AreEqual(relationshipForm.ToString(),
//                newResourceRelationshipNetwork.RelationshipForm.ToString(),
//                "ResourceRelationshipNetwork Creation resulted in an object with the incorrect RelationshipForm");
//            Assert.AreEqual(publishedDate,
//                newResourceRelationshipNetwork.PublishedDate,
//                "ResourceRelationshipNetwork Creation resulted in an object with the incorrect PublishedDate");

//            Helpers.CompareTwoCollections(resourceNetworkConnections,
//                newResourceRelationshipNetwork.ResourceNetworkConnections,
//                "ResourceNetworkConnection");
//        }
//    }
//}