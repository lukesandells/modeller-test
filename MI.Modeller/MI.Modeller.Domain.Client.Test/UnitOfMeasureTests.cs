﻿//using System;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class UnitOfMeasureTests.
//    /// </summary>
//    [TestFixture]
//    public class UnitOfMeasureTests
//    {
//        /// <summary>
//        ///     Tests the constructor with invalid values throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Constructor_WithInvalidValues_ThrowsException()
//        {
//            // Arrange
//            Exception err = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var unitOfMeasure = new UnitOfMeasure(null);
//            }
//            catch (Exception e)
//            {
//                err = e;
//            }

//            //Assert
//            Assert.IsNotNull(err,
//                "Unit Of Measure Creation with null value did not throw an error as expected");
//        }

//        /// <summary>
//        ///     Tests the constructor with valid values returns valid instance.
//        /// </summary>
//        [Test]
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance()
//        {
//            // Arrange
//            var unitOfMeasureSymbol = "TO";

//            // Act
//            var newUnitOfMeasure = new UnitOfMeasure(unitOfMeasureSymbol);

//            //Assert
//            Assert.IsNotNull(newUnitOfMeasure,
//                "Unit Of Measure Creation resulted in a null object reference");
//            Assert.AreEqual(unitOfMeasureSymbol,
//                newUnitOfMeasure.Symbol,
//                "Unit Of Measure does not have the same symbol it was created with");
//        }
//    }
//}