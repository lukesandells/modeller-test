﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class Helpers.
    /// </summary>
    internal class Helpers
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The number
        /// </summary>
        public static int Num = 10;

        #endregion

        #region Members

        /// <summary>
        ///     Builds the basic
        ///     properties.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;BasicProperty&gt;&gt;.</returns>
        public static IEnumerable<IList<BasicProperty>> BuildBasicProperties()
        {
            var basicProperties = new List<BasicProperty>();

            for (var i = 0; i < Num; i++)
            {
                basicProperties.Add(new BasicProperty($"BasicProperty.InternalId.{i}"));
            }
            yield return basicProperties;
        }

        /// <summary>
        ///     Builds the class ids.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildClassIds()
        {
            var classIds = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                classIds.Add($"Class.InternalId.{i}");
            }
            yield return classIds;
        }

        /// <summary>
        ///     Builds the distributed properties.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;DistributedProperty&gt;&gt;.</returns>
        public static IEnumerable<IList<DistributedProperty>> BuildDistributedProperties()
        {
            var distributedProperties = new List<DistributedProperty>();

            for (var i = 0; i < Num; i++)
            {
                distributedProperties.Add(new DistributedProperty($"DistributedProperty.InternalId.{i}"));
            }
            yield return distributedProperties;
        }

        /// <summary>
        ///     Build a list of equipment classes
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static IEnumerable<ISet<EquipmentClass>> BuildEquipmentClasses(string prefix = "Ec")
        {
            var defs = new HashSet<EquipmentClass>();
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);

            for (var i = 0; i < Num; i++)
            {
                defs.Add(new EquipmentClass(hierarchyScope,
                    $"EquipmentClass.{prefix}.ID.{i}"));
            }
            yield return defs;
        }

        /// <summary>
        ///     Builds the equipment ids.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildEquipmentIds()
        {
            var equipmentIds = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                equipmentIds.Add($"Equipment.InternalId.{i}");
            }
            yield return equipmentIds;
        }

        /// <summary>
        ///     Builds the equipments.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>IEnumerable&lt;ISet&lt;Equipment&gt;&gt;.</returns>
        public static IEnumerable<ISet<Equipment>> BuildEquipments(string prefix = "Eq")
        {
            var defs = new HashSet<Equipment>();
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);

            for (var i = 0; i < Num; i++)
            {
                defs.Add(new Equipment(hierarchyScope,
                    $"Equipment.{prefix}.ID.{i}",
                    EquipmentLevel.Area));
            }
            yield return defs;
        }

        /// <summary>
        ///     Builds the equipment specifications.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;EquipmentSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<EquipmentSpecification>> BuildEquipmentSpecifications()
        {
            var equipmentSpecifications = new HashSet<EquipmentSpecification>();
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);

            for (var i = 0; i < Num; i++)
            {
                equipmentSpecifications.Add(new EquipmentSpecification(hierarchyScope,
                    $"Master.ID.{i}")
                {
                    Equipment = new Equipment(hierarchyScope,
                        $"Equipment.ID.{i}",
                        EquipmentLevel.Area)
                });
            }
            yield return equipmentSpecifications;
        }

        /// <summary>
        ///     Builds from node i ds.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildFromNodeIDs()
        {
            ISet<string> fromNodeIDs = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                fromNodeIDs.Add($"From.Node.ID.{i}");
            }
            yield return fromNodeIDs;
        }

        /// <summary>
        ///     Builds the hierarchy scopes.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;HierarchyScope&gt;&gt;.</returns>
        public static IEnumerable<ISet<HierarchyScope>> BuildHierarchyScopes()
        {
            ISet<HierarchyScope> hierarchyScopes = new HashSet<HierarchyScope>();
            // return j elements in each collection
            for (var j = 0; j < Num; j++)
            {
                hierarchyScopes.Add(new HierarchyScope($"New.HierarchyScope.InternalId.{j}",
                    EquipmentLevel.Area));
            }
            yield return hierarchyScopes;
        }

        /// <summary>
        ///     Builds the lot ids.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildLotIds()
        {
            var lotIds = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                lotIds.Add($"Lot.InternalId.{i}");
            }
            yield return lotIds;
        }

        /// <summary>
        ///     Builds the physical asset specification properties.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;PhysicalAssetSpecificationProperty&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildManufacturers()
        {
            var manufacturers = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                manufacturers.Add($"Manufacturer.InternalId.{i}");
            }
            yield return manufacturers;
        }

        public static IEnumerable<ISet<MaterialClass>> BuildMaterialClasses()
        {
            var defs = new HashSet<MaterialClass>();
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            for (var i = 0; i < Num; i++)
            {
                defs.Add(new MaterialClass(hierarchyScope,
                    $"MaterialClass.MatDef.ID.{i}"));
            }
            yield return defs;
        }

        /// <summary>
        ///     Builds the material definition ids.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildMaterialDefinitionIds()
        {
            var equipmentIds = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                equipmentIds.Add($"Equipment.InternalId.{i}");
            }
            yield return equipmentIds;
        }

        /// <summary>
        ///     Builds the material definitions.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;MaterialDefinition&gt;&gt;.</returns>
        public static IEnumerable<ISet<MaterialDefinition>> BuildMaterialDefinitions()
        {
            var defs = new HashSet<MaterialDefinition>();

            for (var i = 0; i < Num; i++)
            {
                var hierarchyScope = new HierarchyScope("Ent",
                    EquipmentLevel.Enterprise);
                defs.Add(new MaterialDefinition(hierarchyScope,
                    $"MaterialDef.MatDef.ID.{i}"));
            }
            yield return defs;
        }

        /// <summary>
        ///     Builds the material specification i ds.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildMaterialSpecificationIDs()
        {
            ISet<string> materialSpecificationIDs = new HashSet<string>();
            for (var i = 0; i < Num; i++)
            {
                materialSpecificationIDs.Add($"materialSpecificationID.{i}");
            }
            yield return materialSpecificationIDs;
        }

        /// <summary>
        ///     Builds the material specifications.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;MaterialSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<MaterialSpecification>> BuildMaterialSpecifications()
        {
            var materialSpecifications = new HashSet<MaterialSpecification>();
            for (var i = 0; i < Num; i++)
            {
                materialSpecifications.Add(new MaterialSpecification($"MaterialSpecification.ID.{i}"));
            }
            yield return materialSpecifications;
        }

        /// <summary>
        ///     Builds the operations material bill items.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsMaterialBillItem&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsMaterialBillItem>> BuildOperationsMaterialBillItems()
        {
            ISet<OperationsMaterialBillItem> operationsMaterialBillItems = new HashSet<OperationsMaterialBillItem>();
            for (var i = 0; i < Num; i++)
            {
                operationsMaterialBillItems.Add(new OperationsMaterialBillItem($"OperationsMaterialBillItem.ID.{i}.New",
                    new HierarchyScope("Ent",
                        EquipmentLevel.Enterprise)));
            }
            yield return operationsMaterialBillItems;
        }

        /// <summary>
        ///     Builds the operations material bills.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsMaterialBill&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsMaterialBill>> BuildOperationsMaterialBills()
        {
            ISet<OperationsMaterialBill> operationsMaterialBills = new HashSet<OperationsMaterialBill>();
            for (var i = 0; i < Num; i++)
            {
                operationsMaterialBills.Add(new OperationsMaterialBill($"OperationsMaterialBill.ID.{i}"));
            }
            yield return operationsMaterialBills;
        }

        /// <summary>
        ///     Builds the operations segments.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsSegment&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsSegment>> BuildOperationsSegments()
        {
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            ISet<OperationsSegment> operationsSegments = new HashSet<OperationsSegment>();
            for (var i = 0; i < Num; i++)
            {
                operationsSegments.Add(new OperationsSegment($"operationsSegment.ID.{i}",
                    hierarchyScope));
            }
            yield return operationsSegments;
        }

        /// <summary>
        ///     Builds the personnel specifications.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;PersonnelSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<PersonnelSpecification>> BuildPersonnelSpecifications()
        {
            var personnelSpecifications = new HashSet<PersonnelSpecification>();
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            for (var i = 0; i < Num; i++)
            {
                personnelSpecifications.Add(new PersonnelSpecification(hierarchyScope,
                    $"Master.ID.{i}")
                {
                    PersonnelClass = new PersonnelClass(hierarchyScope,
                        $"Class.ID.{i}")
                });
            }
            yield return personnelSpecifications;
        }

        /// <summary>
        ///     Builds the physical asset specifications.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;PhysicalAssetSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<PhysicalAssetSpecification>> BuildPhysicalAssetSpecifications()
        {
            var physicalAssetSpecifications = new HashSet<PhysicalAssetSpecification>();
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            for (var i = 0; i < Num; i++)
            {
                physicalAssetSpecifications.Add(new PhysicalAssetSpecification(hierarchyScope,
                    $"Master.ID.{i}")
                {
                    PhysicalAssetClass = new PhysicalAssetClass(hierarchyScope,
                        $"Class.ID.{i}")
                });
            }
            yield return physicalAssetSpecifications;
        }

        /// <summary>
        ///     Builds the quantities.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;Quantity&gt;&gt;.</returns>
        public static IEnumerable<IList<Quantity>> BuildQuantities()
        {
            var quantities = new List<Quantity>();

            for (var i = 0; i < Num; i++)
            {
                quantities.Add(new Quantity($"Quantity.Key.{i}"));
            }
            yield return quantities;
        }

        /// <summary>
        ///     Builds the resource network connections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;ResourceNetworkConnection&gt;&gt;.</returns>
        public static IEnumerable<ISet<ResourceNetworkConnection>> BuildResourceNetworkConnections()
        {
            var resourceNetworkConnections = new HashSet<ResourceNetworkConnection>();

            for (var i = 0; i < Num; i++)
            {
                resourceNetworkConnections.Add(new ResourceNetworkConnection($"ResourceNetworkConnection.ID.{i}"));
            }
            yield return resourceNetworkConnections;
        }

        /// <summary>
        ///     Builds the segment dependencies.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;SegmentDependency&gt;&gt;.</returns>
        public static IEnumerable<ISet<SegmentDependency>> BuildSegmentDependencies()
        {
            var segmentDependencies = new HashSet<SegmentDependency>();
            for (var i = 0; i < Num; i++)
            {
                segmentDependencies.Add(new SegmentDependency($"Segment Dependency.InternalId.{i}",
                    DependencyType.AfterEnd,
                    "ProcessSegmentID"));
            }
            yield return segmentDependencies;
        }

        /// <summary>
        ///     Builds the segment i ds.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildSegmentIDs()
        {
            var segmentIDs = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                segmentIDs.Add($"Process Segment ID.{i}");
            }
            yield return segmentIDs;
        }

        /// <summary>
        ///     Builds the test specification ids.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildTestSpecificationIds()
        {
            var testSpecificationIds = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                testSpecificationIds.Add($"Test.Spec.InternalId.{i}");
            }
            yield return testSpecificationIds;
        }

        /// <summary>
        ///     Builds to node i ds.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildToNodeIDs()
        {
            ISet<string> toNodeIds = new HashSet<string>();

            for (var i = 0; i < Num; i++)
            {
                toNodeIds.Add($"To.Node.ID.{i}");
            }
            yield return toNodeIds;
        }

        /// <summary>
        ///     Builds the values.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;Value&gt;&gt;.</returns>
        public static IEnumerable<IList<Value>> BuildValues()
        {
            var values = new List<Value>();

            for (var i = 0; i < Num; i++)
            {
                values.Add(new Value($"Value.Key.{i}"));
            }
            yield return values;
        }

        /// <summary>
        ///     Builds the workflow specification connections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecificationConnection&gt;&gt;.</returns>
        public static IEnumerable<ISet<WorkflowSpecificationConnection>> BuildWorkflowSpecificationConnections()
        {
            ISet<WorkflowSpecificationConnection> connections = new HashSet<WorkflowSpecificationConnection>();

            for (var i = 0; i < Num; i++)
            {
                connections.Add(new WorkflowSpecificationConnection($"Connection.ID.{i}"));
            }
            yield return connections;
        }

        /// <summary>
        ///     Builds the workflow specification IDs.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<string>> BuildWorkflowSpecificationIds()
        {
            var workflowSpecifications = new HashSet<string>();
            for (var i = 0; i < Num; i++)
            {
                workflowSpecifications.Add($"WorkflowSpecification.ID.{i}");
            }
            yield return workflowSpecifications;
        }

        /// <summary>
        ///     Builds the workflow specification nodes.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecificationNode&gt;&gt;.</returns>
        public static IEnumerable<ISet<WorkflowSpecificationNode>> BuildWorkflowSpecificationNodes()
        {
            ISet<WorkflowSpecificationNode> nodes = new HashSet<WorkflowSpecificationNode>();

            for (var i = 0; i < Num; i++)
            {
                nodes.Add(new WorkflowSpecificationNode($"Node.ID.{i}"));
            }
            yield return nodes;
        }

        /// <summary>
        ///     Builds the workflow specifications.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<WorkflowSpecification>> BuildWorkflowSpecifications()
        {
            var workflowSpecifications = new HashSet<WorkflowSpecification>();
            for (var i = 0; i < Num; i++)
            {
                workflowSpecifications.Add(new WorkflowSpecification($"WorkflowSpecification.ID.{i}"));
            }
            yield return workflowSpecifications;
        }

        /// <summary>
        ///     Compares the two collections.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="origin">The origin.</param>
        /// <param name="copy">The copy.</param>
        /// <param name="propertyName">Name of the property.</param>
        public static void CompareTwoCollections<T>(IEnumerable<T> origin,
            IEnumerable<T> copy,
            string propertyName)
        {
            if (origin == null)
            {
                Assert.AreEqual(0,
                    copy.Count(),
                    $"Property {propertyName} does not have the same number of items it was created with");
            }
            else
            {
                CollectionAssert.AreEquivalent(origin,
                    copy,
                    $"Property {propertyName} does not have the same items it was created / set with");
            }
        }

        #endregion
    }
}