﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
	/// <summary>
	///     Class SegmentDependencyTests.
	/// </summary>
	[TestFixture]
    public class SegmentDependencyTests
    {
        /// <summary>
        ///     Tests the constructor required data only returns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_ReturnsValidInstance()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var process = new ProcessSegment(hierarchyScope, "Process");
            // Act
            var segmentDependency = new SegmentDependency<ProcessSegment>("dependencyId")
            {
                Description = "Description",
                DependencyType = DependencyType.AfterStart,
                Process = process
            };
			segmentDependency.AddTimingFactor(150.AsValue());


            // Assert
            Assert.IsNotNull(segmentDependency, "Segment DependencyType constructor returned an invalid instance");
            Assert.AreEqual("dependencyId", segmentDependency.ExternalId, "New Segment Dependency has invalid external idenfifier");
            Assert.AreEqual("Description", segmentDependency.Description, "New Segment Dependency has invalid Description");
            CollectionAssert.AreEquivalent(new [] { 150.AsValue() }, segmentDependency.TimingFactors, "New Segment Dependency has invalid TimingFactors");
            Assert.AreEqual(DependencyType.AfterStart, (DependencyType) segmentDependency.DependencyType, "New Segment Dependency has invalid DependencyType");
            Assert.AreEqual(process, segmentDependency.Process, "New Segment Dependency has invalid Process");
        }
    }
}