﻿//using MI.Modeller.Domain.Client.DataTypes;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    [TestFixture]
//    public class UncertaintyTests
//    {
//        [Test]
//        public void Test_AllValues_ReturnsInstance()
//        {
//            // Arrange/Act
//            var uncertainty = new Uncertainty(1,
//                2,
//                3,
//                4,
//                5);

//            // Assert
//            Assert.AreEqual(5,
//                uncertainty.Accuracy,
//                "New Uncertainty did not have a null Accuracy as expected");
//            Assert.AreEqual(3,
//                uncertainty.CoverageFactor,
//                "New Uncertainty did not have a null CoverageFactor as expected");
//            Assert.AreEqual(2,
//                uncertainty.ExpandedUncertainty,
//                "New Uncertainty did not have a null ExpandedUncertainty as expected");
//            Assert.AreEqual(1,
//                uncertainty.LevelOfConfidence,
//                "New Uncertainty did not have a null LevelOfConfidence as expected");
//            Assert.AreEqual(4,
//                uncertainty.Precision,
//                "New Uncertainty did not have a null Precision as expected");
//        }

//        [Test]
//        public void Test_NullValues_ReturnsInstance()
//        {
//            // Arrange/Act
//            var uncertainty = new Uncertainty(null,
//                null,
//                null,
//                null,
//                null);

//            // Assert
//            Assert.IsNull(uncertainty.Accuracy,
//                "New Uncertainty did not have a null Accuracy as expected");
//            Assert.IsNull(uncertainty.CoverageFactor,
//                "New Uncertainty did not have a null CoverageFactor as expected");
//            Assert.IsNull(uncertainty.ExpandedUncertainty,
//                "New Uncertainty did not have a null ExpandedUncertainty as expected");
//            Assert.IsNull(uncertainty.LevelOfConfidence,
//                "New Uncertainty did not have a null LevelOfConfidence as expected");
//            Assert.IsNull(uncertainty.Precision,
//                "New Uncertainty did not have a null Precision as expected");
//        }
//    }
//}

