﻿using System;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class WorkMasterTests.
    /// </summary>
    [TestFixture]
    public class WorkMasterTests
    {
        /// <summary>
        ///     Tests the constructor with valid values returns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "WorkMaster.ID";
            var version = "Version";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Equipment.ID.001",
                EquipmentLevel.Enterprise);
            var workType = ProcessType.Inventory;
            var duration = new TimeSpan(100,
                30,
                30);
            var operationsSegment = new OperationsSegment(hierarchyScope,"Operations.Segment.ID");

            // Act
            var newWorkMaster = new WorkMaster(hierarchyScope, id)
            {
                Version = version,
                ProcessType = workType,
                Duration = duration,
                OperationsSegment = operationsSegment,
                Description = description,
            };

            // Assert
            Assert.IsNotNull(newWorkMaster,
                "Instance Creation of WorkMaster object resulted in a null object reference");
            Assert.AreEqual(id,
                newWorkMaster.ExternalId,
                "WorkMaster Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(hierarchyScope.ExternalId,
                newWorkMaster.HierarchyScope.ExternalId,
                "WorkMaster Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(workType.ToString(),
                newWorkMaster.ProcessType.ToString(),
                "WorkMaster Creation resulted in an object with the incorrect ProcessType");
            Assert.AreEqual(duration,
                newWorkMaster.Duration,
                "WorkMaster Creation resulted in an object with the incorrect Duration");
            Assert.IsNotNull(newWorkMaster.PublishedDate,
                "WorkMaster Creation resulted in an object with the incorrect PublishedDate");
            Assert.AreEqual(operationsSegment,
                newWorkMaster.OperationsSegment,
                "WorkMaster Creation resulted in an object with the incorrect OperationsSegmentID");
            Assert.AreEqual(description,
                newWorkMaster.Description,
                "WorkMaster Creation resulted in an object with the incorrect Description");
        }
    }
}