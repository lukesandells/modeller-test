﻿//using System.Collections.Generic;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationNodeTests.
//    /// </summary>
//    [TestFixture]
//    public class WorkflowSpecificationNodeTests
//    {
//        /// <summary>
//        ///     Setups this instance.
//        /// </summary>
//        [SetUp]
//        public void Setup()
//        {
//            Helpers.Num = 10;
//        }

//        /// <summary>
//        ///     Tests the constructor identifier only returns valid instance.
//        /// </summary>
//        [Test]
//        public void Test_Constructor_IDOnly_ReturnsValidInstance()
//        {
//            // Arrange
//            const string id = "WorkflowSpecificationNode.ID.001";

//            // Act
//            var newWorkflowSpecificationNode = new WorkflowSpecificationNode(id);

//            // Assert
//            Assert.IsNotNull(newWorkflowSpecificationNode,
//                "Instance Creation of WorkflowSpecificationNode object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newWorkflowSpecificationNode.Id,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(default(string),
//                newWorkflowSpecificationNode.NodeType,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect NodeType");
//            Assert.AreEqual(default(WorkflowSpecification),
//                newWorkflowSpecificationNode.WorkflowSpecification,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect WorkflowSpecification");
//            Assert.IsNull(newWorkflowSpecificationNode.Description,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect Description");

//            Assert.AreEqual(0,
//                newWorkflowSpecificationNode.Properties.Count,
//                "WorkflowSpecificationNode creation without any Properties resulted in an object with Properties");
//        }

//        /// <summary>
//        ///     Tests the constructor with valid values returns valid instance.
//        /// </summary>
//        /// <param name="properties">The properties.</param>
//        [Test]
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance(
//            [ValueSource(typeof(Helpers), nameof(Helpers.BuildBasicProperties))] IList<BasicProperty> properties)

//        {
//            // Arrange
//            const string id = "WorkflowSpecificationNode.ID.001";
//            const string description = "Description.ID.001";
//            const string nodeType = "Node.Type";
//            var specification = new WorkflowSpecification("Workflow.Specification.ID");

//            // Act
//            var newWorkflowSpecificationNode = new WorkflowSpecificationNode(id,
//                nodeType,
//                specification,
//                description,
//                properties);

//            // Assert
//            Assert.IsNotNull(newWorkflowSpecificationNode,
//                "Instance Creation of WorkflowSpecificationNode object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newWorkflowSpecificationNode.Id,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(nodeType,
//                newWorkflowSpecificationNode.NodeType,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect NodeType");
//            Assert.AreEqual(specification.Id,
//                newWorkflowSpecificationNode.WorkflowSpecification.Id,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect WorkflowSpecification");
//            Assert.AreEqual(description,
//                newWorkflowSpecificationNode.Description,
//                "WorkflowSpecificationNode Creation resulted in an object with the incorrect Description");

//            Helpers.CompareTwoCollections(properties,
//                newWorkflowSpecificationNode.Properties,
//                "Property");
//        }
//    }
//}