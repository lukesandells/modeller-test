﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationTests.
//    /// </summary>
//    [TestFixture]
//    public class WorkflowSpecificationTests
//    {
//        /// <summary>
//        ///     Builds the inputs.
//        /// </summary>
//        /// <returns>
//        ///     IEnumerable&lt;Tuple&lt;ISet&lt;WorkflowSpecificationNode&gt;, ISet&lt;
//        ///     WorkflowSpecificationConnection&gt;&gt;&gt;.
//        /// </returns>
//        public static
//            IEnumerable<Tuple<ISet<WorkflowSpecificationNode>, ISet<WorkflowSpecificationConnection>>>
//            BuildInputs()
//        {
//            for (var nod = 0; nod < 3; nod++)
//            {
//                Helpers.Num = nod;
//                var nodes = Helpers.BuildWorkflowSpecificationNodes()
//                    .First();

//                for (var con = 0; con < 3; con++)
//                {
//                    Helpers.Num = con;
//                    var connections = Helpers.BuildWorkflowSpecificationConnections()
//                        .First();

//                    yield return
//                        new Tuple<ISet<WorkflowSpecificationNode>, ISet<WorkflowSpecificationConnection>>(
//                            nodes,
//                            connections);
//                }
//            }
//        }
        
        
        

//        /// <summary>
//        ///     Tests the constructor identifier only returns valid instance.
//        /// </summary>
//        [Test]
//        public void Test_Constructor_IDOnly_ReturnsValidInstance()
//        {
//            // Arrange
//            const string id = "WorkflowSpecification.ID.001";

//            // Act
//            var newWorkflowSpecification = new WorkflowSpecification(id);

//            // Assert
//            Assert.IsNotNull(newWorkflowSpecification,
//                "Instance Creation of WorkflowSpecification object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newWorkflowSpecification.Id,
//                "WorkflowSpecification Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(default(string),
//                newWorkflowSpecification.Version,
//                "WorkflowSpecification Creation resulted in an object with the incorrect Version");
//            Assert.AreEqual(default(HierarchyScope),
//                newWorkflowSpecification.HierarchyScope,
//                "WorkflowSpecification Creation resulted in an object with the incorrect HierarchyScope");

//            CollectionAssert.IsEmpty(newWorkflowSpecification.Nodes,
//                "WorkflowSpecification creation without any Nodes resulted in an object with Nodes");
//            CollectionAssert.IsEmpty(newWorkflowSpecification.Connections,
//                "WorkflowSpecification creation without any Connections resulted in an object with ToNodeIDs");
//        }

      

//        /// <summary>
//        ///     Tests the constructor with valid values returns valid instance.
//        /// </summary>
//        /// <param name="vals">The vals.</param>
//        [Test]
//        [TestCaseSource(nameof(BuildInputs))]
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance(
//            Tuple<ISet<WorkflowSpecificationNode>, ISet<WorkflowSpecificationConnection>> vals)
//        {
//            // Arrange
//            const string id = "WorkflowSpecification.ID.001";
//            const string version = "Version 1.1.1";
//            const string description = "Description";
//            var hierarchyScope = new HierarchyScope("Equipment.ID",
//                EquipmentLevel.Enterprise);

//            // Act
//            var newWorkflowSpecification = new WorkflowSpecification(id,
//                version,
//                hierarchyScope,
//                description,
//                vals.Item1,
//                vals.Item2);

//            // Assert
//            Assert.IsNotNull(newWorkflowSpecification,
//                "Instance Creation of WorkflowSpecification object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newWorkflowSpecification.Id,
//                "WorkflowSpecification Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(version,
//                newWorkflowSpecification.Version,
//                "WorkflowSpecification Creation resulted in an object with the incorrect Version");
//            Assert.AreEqual(hierarchyScope.EquipmentId,
//                newWorkflowSpecification.HierarchyScope.EquipmentId,
//                "WorkflowSpecification Creation resulted in an object with the incorrect HierarchyScope");

//            Helpers.CompareTwoCollections(vals.Item1,
//                newWorkflowSpecification.Nodes,
//                "Node");
//            Helpers.CompareTwoCollections(vals.Item2,
//                newWorkflowSpecification.Connections,
//                "Connection");
//        }
//    }
//}