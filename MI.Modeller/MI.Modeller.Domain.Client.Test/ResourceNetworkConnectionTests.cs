﻿//using System.Collections.Generic;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class ResourceNetworkConnectionTests.
//    /// </summary>
//    [TestFixture]
//    public class ResourceNetworkConnectionTests
//    {
//        /// <summary>
//        ///     Setups this instance.
//        /// </summary>
//        [SetUp]
//        public void Setup()
//        {
//            Helpers.Num = 10;
//        }

//        /// <summary>
//        ///     Tests the constructor with valid values returns valid instance.
//        /// </summary>
//        /// <param name="connectionProperties">The connection properties.</param>
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance(
//            [ValueSource(typeof(Helpers), nameof(Helpers.BuildBasicProperties))] IList<BasicProperty>
//                connectionProperties)
//        {
//            // Arrange
//            const string id = "ID";
//            const string resourceNetworkConnectionId = "ResourceNetworkConnection.ID";
//            const string description = "ResourceNetworkConnection Description";
//            var fromReference = new ResourceReference("From.Ref.ID");
//            var toReference = new ResourceReference("To.Ref.ID");

//            // Act
//            var newResourceNetworkConnection = new ResourceNetworkConnection(id,
//                description,
//                resourceNetworkConnectionId,
//                fromReference,
//                toReference,
//                connectionProperties);

//            // Assert
//            Assert.IsNotNull(newResourceNetworkConnection,
//                "Instance Creation of ResourceNetworkConnection object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newResourceNetworkConnection.Id,
//                "ResourceNetworkConnection Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(resourceNetworkConnectionId,
//                newResourceNetworkConnection.ResourceNetworkConnectionId,
//                "ResourceNetworkConnection Creation resulted in an object with the incorrect ResourceNetworkConnectionId");
//            Assert.AreEqual(description,
//                newResourceNetworkConnection.Description,
//                "ResourceNetworkConnection Creation resulted in an object with the incorrect Description");
//            Assert.AreEqual(fromReference.Id,
//                newResourceNetworkConnection.FromResourceReference.Id,
//                "ResourceNetworkConnection Creation resulted in an object with the incorrect FromResourceReference");
//            Assert.AreEqual(toReference.Id,
//                newResourceNetworkConnection.ToResourceReference.Id,
//                "ResourceNetworkConnection Creation resulted in an object with the incorrect ToResourceReference");

//            Helpers.CompareTwoCollections(connectionProperties,
//                newResourceNetworkConnection.ConnectionProperties,
//                "ResourceProperty");
//        }
//    }
//}