﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    [TestFixture]
//    public class WorkflowSpecificationConnectionTests
//    {
//        [SetUp]
//        public void Setup()
//        {
//            Helpers.Num = 10;
//        }

//        public static
//            IEnumerable<Tuple<ISet<string>, ISet<string>, IList<BasicProperty>>>
//            BuildInputs()
//        {
//            for (var fnod = 0; fnod < 3; fnod++)
//            {
//                Helpers.Num = fnod;
//                var fromNodeIDs = Helpers.BuildFromNodeIDs()
//                    .First();

//                for (var tnod = 0; tnod < 3; tnod++)
//                {
//                    Helpers.Num = tnod;
//                    var toNodeIDs = Helpers.BuildToNodeIDs()
//                        .First();

//                    for (var props = 0; props < 3; props++)
//                    {
//                        Helpers.Num = props;
//                        var properties = Helpers.BuildBasicProperties()
//                            .First();

//                        yield return
//                            new Tuple
//                                <ISet<string>, ISet<string>, IList<BasicProperty>>(
//                                    fromNodeIDs,
//                                    toNodeIDs,
//                                    properties);
//                    }
//                }
//            }
//        }
        
        

//        [Test]
//        public void Test_Constructor_IDOnly_ReturnsValidInstance()
//        {
//            // Arrange
//            const string id = "WorkflowSpecificationConnection.ID.001";

//            // Act
//            var newWorkflowSpecificationConnection = new WorkflowSpecificationConnection(id);

//            // Assert
//            Assert.IsNotNull(newWorkflowSpecificationConnection,
//                "Instance Creation of WorkflowSpecificationConnection object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newWorkflowSpecificationConnection.Id,
//                "WorkflowSpecificationConnection Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(default(string),
//                newWorkflowSpecificationConnection.ConnectionType,
//                "WorkflowSpecificationConnection Creation resulted in an object with the incorrect ConnectionType");
//            Assert.AreEqual(default(string),
//                newWorkflowSpecificationConnection.Description,
//                "WorkflowSpecificationConnection Creation resulted in an object with the incorrect Description");

//            CollectionAssert.IsEmpty(newWorkflowSpecificationConnection.FromNodeIDs,
//                "WorkflowSpecificationConnection creation without any FromNodeIDs resulted in an object with FromNodeIDs");
//            CollectionAssert.IsEmpty(newWorkflowSpecificationConnection.ToNodeIDs,
//                "WorkflowSpecificationConnection creation without any ToNodeIDs resulted in an object with ToNodeIDs");
//            CollectionAssert.IsEmpty(newWorkflowSpecificationConnection.Properties,
//                "WorkflowSpecificationConnection creation without any Properties resulted in an object with Properties");
//        }

       

//        [Test]
//        [TestCaseSource(nameof(BuildInputs))]
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance(
//            Tuple<ISet<string>, ISet<string>, IList<BasicProperty>> vals)
//        {
//            // Arrange
//            const string id = "WorkflowSpecificationConnection.ID.001";
//            const string description = "Description.ID.001";
//            const string connectionType = "Connection.Type";

//            // Act
//            var newWorkflowSpecificationConnection = new WorkflowSpecificationConnection(id,
//                connectionType,
//                description,
//                vals.Item1,
//                vals.Item2,
//                vals.Item3);

//            // Assert
//            Assert.IsNotNull(newWorkflowSpecificationConnection,
//                "Instance Creation of WorkflowSpecificationConnection object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newWorkflowSpecificationConnection.Id,
//                "WorkflowSpecificationConnection Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(connectionType,
//                newWorkflowSpecificationConnection.ConnectionType,
//                "WorkflowSpecificationConnection Creation resulted in an object with the incorrect ConnectionType");
//            Assert.AreEqual(description,
//                newWorkflowSpecificationConnection.Description,
//                "WorkflowSpecificationConnection Creation resulted in an object with the incorrect Description");

//            Helpers.CompareTwoCollections(vals.Item1,
//                newWorkflowSpecificationConnection.FromNodeIDs,
//                "FromNodeID");
//            Helpers.CompareTwoCollections(vals.Item2,
//                newWorkflowSpecificationConnection.ToNodeIDs,
//                "ToNodeID");
//            Helpers.CompareTwoCollections(vals.Item3,
//                newWorkflowSpecificationConnection.Properties,
//                "Property");
//        }
//    }
//}