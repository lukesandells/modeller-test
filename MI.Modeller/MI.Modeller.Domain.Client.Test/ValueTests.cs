﻿//using System;
//using System.Collections.Generic;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class ValueTests.
//    /// </summary>
//    [TestFixture]
//    public class ValueTests
//    {
//        /// <summary>
//        ///     Nulls the type of the and valid data.
//        /// </summary>
//        /// <returns>IEnumerable&lt;DataType&gt;.</returns>
//        public static IEnumerable<DataType?> NullAndValidDataType()
//        {
//            yield return null;
//            foreach (DataType dataTypeValue in Enum.GetValues(typeof(DataType)))
//            {
//                yield return dataTypeValue;
//            }
//        }

//        /// <summary>
//        ///     Nulls the and valid unit of measure.
//        /// </summary>
//        /// <returns>IEnumerable&lt;UnitOfMeasure&gt;.</returns>
//        public static IEnumerable<UnitOfMeasure> NullAndValidUnitOfMeasure()
//        {
//            yield return default(UnitOfMeasure);
//            yield return new UnitOfMeasure("TO");
//        }

//        // empty constructor - returns empty string as key
//        /// <summary>
//        ///     Tests the constructor empty constructor creates valid i nstance.
//        /// </summary>
//        [Test]
//        public void Test_Constructor_EmptyConstructor_CreatesValidINstance()
//        {
//            // Arrange

//            // Act
//            var newValue = new Value();

//            // Assert
//            Assert.IsNotNull(newValue,
//                "Creation of Value object with valid values resulted ina null reference object");
//            Assert.IsNull(newValue.Key,
//                "Creation of Value object resulted in an object with the incorrect Key.");
//            Assert.IsNull(newValue.ValueString,
//                "Creation of Value object resulted in an object with the incorrect ValueString.");
//            Assert.IsNull(newValue.DataType,
//                "Creation of Value object resulted in an object with the incorrect Data Type.");
//            Assert.IsNull(newValue.UnitOfMeasure,
//                "Creation of Value object resulted in an object with the incorrect Unit Of Measure.");
//            Assert.IsNull(newValue.Uncertainty,
//                "Creation of Value object resulted in an object with the incorrect Uncertainty.");
//        }

//        /// <summary>
//        ///     Tests the constructor with valid data creates valid instance.
//        /// </summary>
//        /// <param name="key">The key.</param>
//        /// <param name="valueString">The value string.</param>
//        /// <param name="dataType">Type of the data.</param>
//        /// <param name="unitOfMeasure">The unit of measure.</param>
//        [Test]
//        public void Test_Constructor_WithValidData_CreatesValidInstance([Values("ValidKeyValue")] string key,
//            [Values(default(string), "ValidValueString")] string valueString,
//            [ValueSource(nameof(NullAndValidDataType))] DataType dataType,
//            [ValueSource(nameof(NullAndValidUnitOfMeasure))] UnitOfMeasure unitOfMeasure)

//        {
//            // Test constructor passing all possible combinations of values to it

//            // Arrange
//            var uncertainty = new Uncertainty(1,2,3,4,5);

//            // Act
//            var newValue = new Value(key,
//                valueString,
//                dataType,
//                unitOfMeasure,
//                uncertainty);

//            // Assert
//            Assert.IsNotNull(newValue,
//                "Creation of Value object with valid values resulted ina null reference object");
//            Assert.AreEqual(key,
//                newValue.Key,
//                "Creation of Value object resulted in an object with the incorrect Key.");
//            Assert.AreEqual(valueString,
//                newValue.ValueString,
//                "Creation of Value object resulted in an object with the incorrect ValueString.");
//            Assert.AreEqual(dataType,
//                newValue.DataType,
//                "Creation of Value object resulted in an object with the incorrect Data Type.");
//            Assert.AreEqual(unitOfMeasure,
//                newValue.UnitOfMeasure,
//                "Creation of Value object resulted in an object with the incorrect Unit Of Measure.");
//            Assert.AreEqual(uncertainty,
//                newValue.Uncertainty,
//                "Creation of Value object resulted in an object with the incorrect Uncertainty.");
//        }
//    }
//}