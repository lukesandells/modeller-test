﻿using System;
using System.Linq;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings
{
	public class ModelNodeMapping : DefinitionNodeMapping<Model, Model>
	{
		private static class PermanentFolderIds
		{
			public static Guid Pages = new Guid("c543d8d5-6be0-4e3f-8f25-7418d7370677");
		}

		public ModelNodeMapping()
		{
			// Type information
			Type(NodeTypeId.Model, ObjectName.Model);
			
			// Properties
			DisplayId(NodeTypeMember.Property.DisplayId, target => target.DisplayId, PropertyName.Name)
				.FindUnavailableWith(path => path.SpecifiedFolder.Nodes.Select(n => n.DisplayId).AsQueryable());
			Description(NodeTypeMember.Property.Description, target => target.Description, PropertyName.Description);
			
			// Folders
			PrimaryFolder<Page>(PermanentFolderIds.Pages);
		}
	}
}
