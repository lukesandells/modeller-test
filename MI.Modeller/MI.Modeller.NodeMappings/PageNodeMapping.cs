﻿using System.Linq;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings
{
	public class PageNodeMapping : DefinitionNodeMapping<Page, Page>
	{
		public PageNodeMapping()
		{
			// Type information
			Type(NodeTypeId.Page, ObjectName.Page);

			// Properties
			DisplayId(NodeTypeMember.Property.DisplayId, target => target.DisplayId, PropertyName.Name)
				.FindUnavailableWith(path => path.SpecifiedFolder.Nodes.Select(n => n.DisplayId).AsQueryable());
			Description(NodeTypeMember.Property.Description, target => target.Description, PropertyName.Description);
		}
	}
}
