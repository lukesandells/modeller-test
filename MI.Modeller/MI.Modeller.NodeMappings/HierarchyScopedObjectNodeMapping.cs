﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.Modeller.Domain;
using MI.Modeller.Domain.Validation;

namespace MI.Modeller.NodeMappings
{
	public abstract class HierarchyScopedObjectNodeMapping<TTarget, TNodeType> : DefinitionNodeMapping<TTarget, TNodeType>
		where TTarget : HierarchyScopedObject where TNodeType : DefinitionNode
	{
		protected DisplayIdMapping<TTarget, TNodeType> DisplayIdMapping => _displayIdMapping;
		private DisplayIdMapping<TTarget, TNodeType> _displayIdMapping;

		protected HierarchyScopedObjectNodeMapping(Func<TNodeType, object> hierarchyScopeFormula = null, Func<FolderPath, IEnumerable<TTarget>, IQueryable<string>> unavailableDisplayIdMapping = null, bool? readOnlyHierarchyScope = null)
		{
			// Properties
	
			_displayIdMapping = DisplayId(NodeTypeMember.Property.DisplayId, o => o.ExternalId, PropertyName.ID)
				.FindUnavailableWith((path, exclusions) => unavailableDisplayIdMapping?.Invoke(path, exclusions) ?? PersistenceContext.Current.Session.Query<TTarget>()
					.WhereVisibleTo(path.ParentOfType<HierarchyScope>())
					// Execute DB query and filter the rest in memory because the number of exclusions may exceed the available number of SQL parameters
					.Select(e => new { e.InternalId, e.ExternalId }).ToHashSet()
					.Where(id => !exclusions.Any(e => e.InternalId == id.InternalId))
					.Select(id => id.ExternalId)
					.AsQueryable())
				.ValidateWith(new AmbiguousNodeReferenceDisplayIdInputValidator());
			Description(NodeTypeMember.Property.Description, target => target.Description, PropertyName.Description);

			hierarchyScopeFormula = hierarchyScopeFormula ?? (node => node.ParentNode?.TypeId == NodeTypeId.HierarchyScope 
										? node.ParentNode 
										: (node.ParentNode as ModelElement)?.Properties?[NodeTypeMember.Property.HierarchyScope]?.Value);
			var hierarchyScopeMapping = Property(NodeTypeMember.Property.HierarchyScope, target => target.HierarchyScope, PropertyName.HierarchyScope)
				.Formula(hierarchyScopeFormula);
			if (readOnlyHierarchyScope != null)
			{
				hierarchyScopeMapping.ReadOnly(readOnlyHierarchyScope.Value);
			}

			// Validators
			ValidateWith(new NodeVisibilityValidator());
			ValidateWith(new AmbiguousNodeReferenceValidator());

			// Additional permitted operations
			PermitNodeOperation(ModellerOperationType.Export);
			PermitFolderOperation(ModellerOperationType.Export);
		}
	}
}
