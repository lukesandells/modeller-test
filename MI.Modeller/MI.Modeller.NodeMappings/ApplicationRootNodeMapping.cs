﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings
{
	public class ApplicationRootNodeMapping : RootNodeMapping<RootNode>
	{
		public ApplicationRootNodeMapping()
		{
			Type(NodeTypeId.Root);
			SecondaryFolder<HierarchyScope>(NodeTypeMember.Folder.Enterprises, FolderName.Enterprises, permittedType =>
			{
				permittedType.CanAddIf((rootNode, hs) => hs.EquipmentLevel == EquipmentLevel.Enterprise);
				permittedType.CanAddNewIf((rootNode, @params) => TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(
					@params[0]) == EquipmentLevel.Enterprise);
			});
		}
	}
}
