﻿using MI.Framework;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Resources
{
	public class EquipmentClassNodeMapping : HierarchyScopedObjectNodeMapping<EquipmentClass, ResourceDefinitionElement>
	{
		public EquipmentClassNodeMapping()
		{
			// Type information
			Type(NodeTypeId.EquipmentClass, ObjectName.EquipmentClass)
				.PreFetchWith(PreFetchStrategy.ModelElement)
				.InitialiseWith
					.Property(NodeTypeMember.Property.EquipmentLevel, target => target.EquipmentLevel, PropertyName.EquipmentLevel);

			// Properties
			DisplayIdMapping.Prefix(@params => ExtensibleEnum.GetEnumValueDisplayName((EquipmentLevel)@params[0]).Replace(" ", string.Empty) + ".Class.");

			// Folders
			PrimaryFolder<EquipmentClass>(NodeTypeMember.Folder.EquipmentClasses, permittedType =>
			{
				permittedType.AddWith((@class, subclass) => @class.AddSubclass(subclass));
				permittedType.RemoveWith((@class, subclass) => @class.RemoveSubclass(subclass));
				permittedType.CanAddIf((@class, subclass) => @class.CanAddSubclass(subclass));
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});
			
			InverseFolder<Equipment>(NodeTypeMember.Folder.Equipment, FolderName.Equipment, NodeTypeMember.Folder.EquipmentClasses);

			// Node events
			OnCopy((original, copy) => ((EquipmentClass)original.DesignObject).Properties
				.ForEach(property => property.CopyTo((EquipmentClass)copy.DesignObject)));
		}
	}
}
