﻿using System.Linq;
using MI.Framework;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Resources
{
	public class MaterialDefinitionNodeMapping : HierarchyScopedObjectNodeMapping<MaterialDefinition, ResourceDefinitionElement>
	{
		public MaterialDefinitionNodeMapping()
		{
			// Type information
			Type(NodeTypeId.MaterialDefinition, ObjectName.MaterialDefinition)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			// Properties
			Property(NodeTypeMember.Property.AssemblyRelationship, o => o.AssemblyRelationship, PropertyName.AssemblyRelationship)
				.ApplicableIf(o => o.PrimaryFolder.Nodes.Any());
			Property(NodeTypeMember.Property.AssemblyType, o => o.AssemblyType, PropertyName.AssemblyType)
				.ApplicableIf(o => o.PrimaryFolder.Nodes.Any());

			// Folders
			PrimaryFolder<MaterialDefinition>(NodeTypeMember.Folder.MaterialAssemblyElements, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddAssemblyElement(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveAssemblyElement(child));
				permittedType.CanAddIf((parent, child) => parent.CanAdd(child));
			});

			SecondaryFolder<MaterialClass>(NodeTypeMember.Folder.MaterialClasses, FolderName.MaterialClasses, false, true, permittedType =>
			{
				permittedType.AddWith((member, @class) => @class.AddMember(member));
				permittedType.RemoveWith((member, @class) => @class.RemoveMember(member));
				permittedType.CanAddIf((parent, child) => child.CanAddMember(parent));
			});

			// Node events
			OnCopy((original, copy) => ((MaterialDefinition)original.DesignObject).Properties
				.ForEach(property => property.CopyTo((MaterialDefinition)copy.DesignObject)));
		}
	}
}
