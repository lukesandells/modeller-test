﻿using MI.Framework;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Resources
{
	public class PersonnelClassNodeMapping : HierarchyScopedObjectNodeMapping<PersonnelClass, ResourceDefinitionElement>
	{
		public PersonnelClassNodeMapping()
		{
			// Type information
			Type(NodeTypeId.PersonnelClass, ObjectName.PersonnelClass)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			// Properties

			// Folders
			PrimaryFolder<PersonnelClass>(NodeTypeMember.Folder.PersonnelClasses, permittedType =>
			{
				permittedType.AddWith((@class, subclass) => @class.AddSubclass(subclass));
				permittedType.RemoveWith((@class, subclass) => @class.RemoveSubclass(subclass));
				permittedType.CanAddIf((@class, subclass) => @class.CanAdd(subclass));
			});

			// Node events
			OnCopy((original, copy) => ((PersonnelClass)original.DesignObject).Properties
				.ForEach(property => property.CopyTo((PersonnelClass)copy.DesignObject)));
		}
	}
}
