﻿using MI.Framework;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Resources
{
	public class PhysicalAssetClassNodeMapping : HierarchyScopedObjectNodeMapping<PhysicalAssetClass, ResourceDefinitionElement>
	{
		public PhysicalAssetClassNodeMapping()
		{
			// Type information
			Type(NodeTypeId.PhysicalAssetClass, ObjectName.PhysicalAssetClass)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			// Properties

			// Folders
			PrimaryFolder<PhysicalAssetClass>(NodeTypeMember.Folder.PhysicalAssetClasses, permittedType =>
			{
				permittedType.AddWith((@class, subclass) => @class.AddSubclass(subclass));
				permittedType.RemoveWith((@class, subclass) => @class.RemoveSubclass(subclass));
				permittedType.CanAddIf((@class, subclass) => @class.CanAddSubclass(subclass));
			});

			// Node events
			OnCopy((original, copy) => ((PhysicalAssetClass)original.DesignObject).Properties
				.ForEach(property => property.CopyTo((PhysicalAssetClass)copy.DesignObject)));
		}
	}
}
