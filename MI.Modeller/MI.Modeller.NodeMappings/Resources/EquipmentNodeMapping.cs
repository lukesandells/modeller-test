﻿using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Persistence;
using MI.Modeller.Domain;
using MI.Modeller.Domain.Validation;

namespace MI.Modeller.NodeMappings.Resources
{
	public class EquipmentNodeMapping : HierarchyScopedObjectNodeMapping<Equipment, ResourceDefinitionElement>
	{
		public EquipmentNodeMapping()
		{
			// Type information
			Type(NodeTypeId.Equipment, ObjectName.Equipment)
				.PreFetchWith(PreFetchStrategy.ModelElement)
				.InitialiseWith
					.Property(NodeTypeMember.Property.EquipmentLevel, target => target.EquipmentLevel, PropertyName.EquipmentLevel);

			// Properties
			DisplayIdMapping.Prefix(@params => ExtensibleEnum.GetEnumValueDisplayName((EquipmentLevel)@params[0]).Replace(" ", string.Empty) + ".");

			// Folders
			PrimaryFolder<Equipment>(NodeTypeMember.Folder.Equipment, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddChild(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveChild(child));
				permittedType.CanAddIf((parent, child) => parent.CanAddChild(child));
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});

			SecondaryFolder<EquipmentClass>(NodeTypeMember.Folder.EquipmentClasses, FolderName.EquipmentClasses, false, true, permittedType =>
			{
				permittedType.AddWith((member, @class) => @class.AddMember(member));
				permittedType.RemoveWith((member, @class) => @class.RemoveMember(member));
				permittedType.CanAddIf((member, @class) => @class.CanAddMember(member));
			});
			
			// Node events
			OnCopy((original, copy) => ((Equipment)original.DesignObject).Properties
				.ForEach(property => property.CopyTo((Equipment)copy.DesignObject)));
		}
	}
}
