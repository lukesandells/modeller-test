﻿using System.Linq;
using MI.Framework;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Resources
{
	public class MaterialClassNodeMapping : HierarchyScopedObjectNodeMapping<MaterialClass, ResourceDefinitionElement>
	{
		public MaterialClassNodeMapping()
		{
			// Type information
			Type(NodeTypeId.MaterialClass, ObjectName.MaterialClass)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			// Properties
			Property(NodeTypeMember.Property.AssemblyRelationship, o => o.AssemblyRelationship, PropertyName.AssemblyRelationship)
				.ApplicableIf(o => o.AllFolders.Single(f => f.MemberId == NodeTypeMember.Folder.MaterialAssemblyElements).Nodes.Any());
			Property(NodeTypeMember.Property.AssemblyType, o => o.AssemblyType, PropertyName.AssemblyType)
				.ApplicableIf(o => o.AllFolders.Single(f => f.MemberId == NodeTypeMember.Folder.MaterialAssemblyElements).Nodes.Any());

			// Folders
			SecondaryFolder<MaterialClass>(NodeTypeMember.Folder.MaterialSubclasses, FolderName.MaterialSubclasses, true, true, permittedType =>
			{
				permittedType.AddWith((@class, subclass) => @class.AddSubclass(subclass));
				permittedType.RemoveWith((@class, subclass) => @class.RemoveSubclass(subclass));
				permittedType.CanAddIf((@class, subclass) => @class.CanAdd(subclass));
			});
			SecondaryFolder<MaterialClass>(NodeTypeMember.Folder.MaterialAssemblyElements, FolderName.MaterialAssemblyElement, true, true, permittedType =>
			{
				permittedType.AddWith((@class, element) => @class.AddAssemblyElement(element));
				permittedType.RemoveWith((@class, element) => @class.RemoveAssemblyElement(element));
				permittedType.CanAddIf((@class, element) => @class.CanAdd(element));
			});

			InverseFolder<MaterialDefinition>(NodeTypeMember.Folder.MaterialDefinitions, FolderName.MaterialDefinitions, NodeTypeMember.Folder.MaterialClasses);

			// Node events
			OnCopy((original, copy) => ((MaterialClass)original.DesignObject).Properties
				.ForEach(property => property.CopyTo((MaterialClass)copy.DesignObject)));
		}
	}
}
