﻿using MI.Framework;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.NodeMappings.Processes
{
	public abstract class ProcessDefinitionNodeMapping<TProcess> : 
		HierarchyScopedObjectNodeMapping<TProcess, ProcessDefinitionElement> where TProcess : HierarchyScopedObject, IProcessDefinition<TProcess>, IWithBasicProperties<TProcess>
	{
		protected ProcessDefinitionNodeMapping()
		{
			// Properties 
			Property(NodeTypeMember.Property.Duration, o => o.Duration, PropertyName.Duration);
			Property(NodeTypeMember.Property.ProcessType, o => o.ProcessType, PropertyName.ProcessType);
			Property(NodeTypeMember.Property.PublishedDate, o => o.PublishedDate, PropertyName.PublishedDate);
			Property(NodeTypeMember.Property.Version, o => o.Version, PropertyName.Version);

			// Folders
			SecondaryFolder<MaterialSpecification<TProcess>>(NodeTypeMember.Folder.MaterialSpecifications,
				FolderName.MaterialSpecifications,
				true,
				false,
				permittedType =>
				{
					permittedType.AddWith((@class, element) => @class.AddMaterialSpecification(element));
					permittedType.RemoveWith((@class, element) => @class.RemoveMaterialSpecification(element));
				});
			SecondaryFolder<EquipmentSpecification<TProcess>>(NodeTypeMember.Folder.EquipmentSpecifications,
				FolderName.EquipmentSpecifications,
				true,
				false,
				permittedType =>
				{
					permittedType.AddWith((@class, element) => @class.AddEquipmentSpecification(element));
					permittedType.RemoveWith((@class, element) => @class.RemoveEquipmentSpecification(element));
				});
			SecondaryFolder<PersonnelSpecification<TProcess>>(NodeTypeMember.Folder.PersonnelSpecifications,
				FolderName.PersonnelSpecifications,
				true,
				false,
				permittedType =>
				{
					permittedType.AddWith((@class, element) => @class.AddPersonnelSpecification(element));
					permittedType.RemoveWith((@class, element) => @class.RemovePersonnelSpecification(element));
				});
			SecondaryFolder<PhysicalAssetSpecification<TProcess>>(NodeTypeMember.Folder.PhysicalAssetSpecifications,
				FolderName.PhysicalAssetSpecifications,
				true,
				false,
				permittedType =>
				{
					permittedType.AddWith((@class, element) => @class.AddPhysicalAssetSpecification(element));
					permittedType.RemoveWith((@class, element) => @class.RemovePhysicalAssetSpecification(element));
				});

			// Node events
			OnCopy((original, copy) => ((TProcess) original.DesignObject).Properties.ForEach(property => property.CopyTo((TProcess) copy.DesignObject)));
		}
	}
}
