﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.WorkMaster
{
	/// <summary>
	/// PhysicalAssetSpecificationNodeMapping for WorkMaster
	/// </summary>
	public class PhysicalAssetSpecificationNodeMapping:PhysicalAssetSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>
	{
		public PhysicalAssetSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.WorkMasterPhysicalAssetSpecification, ObjectName.PhysicalAssetSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
