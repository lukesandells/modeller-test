﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.WorkMaster
{
	/// <summary>
	/// EquipmentSpecificationNodeMapping for WorkMaster
	/// </summary>
	public class EquipmentSpecificationNodeMapping:EquipmentSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>
	{
		public EquipmentSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.WorkMasterEquipmentSpecification, ObjectName.EquipmentSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
