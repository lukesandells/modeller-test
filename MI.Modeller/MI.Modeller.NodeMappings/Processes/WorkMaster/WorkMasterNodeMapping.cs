﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.WorkMaster
{
	public class WorkMasterNodeMapping : ProcessDefinitionNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>
	{
		public WorkMasterNodeMapping()
		{
			// Type information
			Type(NodeTypeId.WorkMaster, ObjectName.WorkMaster)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			Property(NodeTypeMember.Property.OperationsSegment, o => o.OperationsSegment, PropertyName.OperationsSegment);

			// Folders
			PrimaryFolder<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>(NodeTypeMember.Folder.WorkMasters, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddChild(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveChild(child));
				permittedType.CanAddIf((parent, child) => parent.CanAdd(child));
			});
		}
	}
}
