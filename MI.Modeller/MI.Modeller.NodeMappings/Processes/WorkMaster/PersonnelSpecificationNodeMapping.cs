﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.WorkMaster
{
	/// <summary>
	/// PersonnelSpecificationNodeMapping for WorkMaster
	/// </summary>
	public class PersonnelSpecificationNodeMapping:PersonnelSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>
	{
		public PersonnelSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.WorkMasterPersonnelSpecification, ObjectName.PersonnelSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
