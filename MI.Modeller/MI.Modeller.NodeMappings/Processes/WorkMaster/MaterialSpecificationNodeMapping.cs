﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.WorkMaster
{
	/// <summary>
	/// MaterialSpecificationNodeMapping for WorkMaster
	/// </summary>
	public class MaterialSpecificationNodeMapping:MaterialSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>
	{
		public MaterialSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.WorkMasterMaterialSpecification, ObjectName.MaterialSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
