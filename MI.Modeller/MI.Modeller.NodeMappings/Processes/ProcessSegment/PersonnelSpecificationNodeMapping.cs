﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.ProcessSegment
{
	/// <summary>
	/// PersonnelSpecificationNodeMapping for ProcessSegment
	/// </summary>
	public class PersonnelSpecificationNodeMapping:PersonnelSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.ProcessSegment>
	{
		public PersonnelSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.ProcessSegmentPersonnelSpecification, ObjectName.PersonnelSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
