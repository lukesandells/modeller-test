﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.ProcessSegment
{
	public class ProcessSegmentNodeMapping : ProcessDefinitionNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.ProcessSegment>
	{
		public ProcessSegmentNodeMapping()
		{
			// Type information
			Type(NodeTypeId.ProcessSegment, ObjectName.ProcessSegment)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			// Folders
			PrimaryFolder<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.ProcessSegment>(NodeTypeMember.Folder.ProcessSegments, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddChild(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveChild(child));
				permittedType.CanAddIf((parent, child) => parent.CanAdd(child));
			});
		}
	}
}
