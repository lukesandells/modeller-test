﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.ProcessSegment
{
	/// <summary>
	/// EquipmentSpecificationNodeMapping for ProcessSegment
	/// </summary>
	public class EquipmentSpecificationNodeMapping:EquipmentSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.ProcessSegment>
	{
		public EquipmentSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.ProcessSegmentEquipmentSpecification, ObjectName.EquipmentSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
