﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.ProcessSegment
{
	/// <summary>
	/// PhysicalAssetSpecificationNodeMapping for ProcessSegment
	/// </summary>
	public class PhysicalAssetSpecificationNodeMapping:PhysicalAssetSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.ProcessSegment>
	{
		public PhysicalAssetSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.ProcessSegmentPhysicalAssetSpecification, ObjectName.PhysicalAssetSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
