﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.ProcessSegment
{
	/// <summary>
	/// MaterialSpecificationNodeMapping for ProcessSegment
	/// </summary>
	public class MaterialSpecificationNodeMapping:MaterialSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.ProcessSegment>
	{
		public MaterialSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.ProcessSegmentMaterialSpecification, ObjectName.MaterialSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
