﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.OperationsDefinition
{
	/// <summary>
	/// MaterialSpecificationNodeMapping for OperationsSegment
	/// </summary>
	public class MaterialSpecificationNodeMapping:MaterialSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.OperationsSegment>
	{
		public MaterialSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.OperationsSegmentMaterialSpecification, ObjectName.MaterialSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
