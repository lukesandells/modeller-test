﻿using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.OperationsDefinition
{
	public class OperationsDefinitionNodeMapping: HierarchyScopedObjectNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.OperationsDefinition, ModelElement>
	{
		public OperationsDefinitionNodeMapping() : base()
		{
			// Type information
			Type(NodeTypeId.OperationsDefinition, ObjectName.OperationsDefinition)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			Property(NodeTypeMember.Property.BillOfResourcesId, o => o.BillOfResourcesId, PropertyName.BillOfResourcesId);
			Property(NodeTypeMember.Property.ProcessType, o => o.ProcessType, PropertyName.ProcessType);
			Property(NodeTypeMember.Property.WorkDefinitionId, o => o.WorkDefinitionId, PropertyName.WorkDefinitionId);

			// Folders
			PrimaryFolder<OperationsSegment>(NodeTypeMember.Folder.OperationsSegments, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddOperationsSegment(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveOperationsSegment(child));
			});
		}
	}
}
