﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.OperationsDefinition
{
	/// <summary>
	/// PersonnelSpecificationNodeMapping for OperationsSegment
	/// </summary>
	public class PersonnelSpecificationNodeMapping:PersonnelSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.OperationsSegment>
	{
		public PersonnelSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.OperationsSegmentPersonnelSpecification, ObjectName.PersonnelSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
