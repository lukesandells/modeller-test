﻿using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.OperationsDefinition
{
	public class OperationsSegmentNodeMapping : ProcessDefinitionNodeMapping<OperationsSegment>
	{
		public OperationsSegmentNodeMapping()
		{
			// Type information
			Type(NodeTypeId.OperationsSegment, ObjectName.OperationsSegment)
				.PreFetchWith(PreFetchStrategy.ModelElement);

			// Folders
			PrimaryFolder<OperationsSegment>(NodeTypeMember.Folder.OperationsSegments, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddChild(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveChild(child));
				permittedType.CanAddIf((parent, child) => parent.CanAdd(child));
			});
			SecondaryFolder<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.WorkMaster>(NodeTypeMember.Folder.WorkMasters, FolderName.WorkMasters, false, true, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.OperationsSegment, false);
			});
		}
	}
}
