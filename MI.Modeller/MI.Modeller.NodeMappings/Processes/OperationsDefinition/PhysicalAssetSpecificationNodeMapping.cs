﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.OperationsDefinition
{
	/// <summary>
	/// PhysicalAssetSpecificationNodeMapping for OperationsSegment
	/// </summary>
	public class PhysicalAssetSpecificationNodeMapping:PhysicalAssetSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.OperationsSegment>
	{
		public PhysicalAssetSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.OperationsSegmentPhysicalAssetSpecification, ObjectName.PhysicalAssetSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
