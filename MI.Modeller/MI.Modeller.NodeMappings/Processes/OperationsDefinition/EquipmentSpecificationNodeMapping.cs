﻿using MI.Modeller.Persistence;
using MI.Modeller.Domain;

namespace MI.Modeller.NodeMappings.Processes.OperationsDefinition
{
	/// <summary>
	/// EquipmentSpecificationNodeMapping for OperationsSegment
	/// </summary>
	public class EquipmentSpecificationNodeMapping:EquipmentSpecificationNodeMapping<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.OperationsSegment>
	{
		public EquipmentSpecificationNodeMapping()
		{
			// Type information
			Type(NodeTypeId.OperationsSegmentEquipmentSpecification, ObjectName.EquipmentSpecification)
				.PreFetchWith(PreFetchStrategy.ModelElement);
		}
	}
}
