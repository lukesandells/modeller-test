﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.NodeMappings.Processes
{
	/// <summary>
	/// Node mapping for an Personnel Specification
	/// </summary>
	/// <typeparam name="TProcess">The type of process this specification is for</typeparam>
	public abstract class PersonnelSpecificationNodeMapping<TProcess>: 
		ResourceSpecificationNodeMapping<PersonnelSpecification<TProcess>, TProcess>
		where TProcess: class, IProcessDefinition<TProcess>
	{
		/// <summary>
		/// Function to find the unavailable IDs for new specifications
		/// </summary>
		private static readonly Func<FolderPath, IEnumerable<PersonnelSpecification<TProcess>>, IQueryable<string>> _unavailableDisplayIdMapping =
			(path, exclusions) => PersistenceContext.Current.Session.Query<EquipmentSpecification<TProcess>>()
				.WhereVisibleTo(path.ParentOfType<HierarchyScope>())
				// Execute DB query and filter the rest in memory because the number of exclusions may exceed the available number of SQL parameters
				.Select(e => new { e.InternalId, e.ExternalId })
				// Rest in memory
				.ToHashSet()
				.Union(path.ParentOfType<TProcess>().PersonnelSpecifications
						.Select(e => new { e.InternalId, e.ExternalId }))
				.Where(id => !exclusions.Any(e => e.InternalId == id.InternalId))
				.Select(id => id.ExternalId)
				.AsQueryable();

		/// <summary>
		/// Initializes a new PersonnelSpecificationNodeMapping
		/// </summary>
		protected PersonnelSpecificationNodeMapping() : base(_unavailableDisplayIdMapping)
		{
			// Properties
			Property(NodeTypeMember.Property.PersonnelClass, o => o.PersonnelClass, PropertyName.PersonnelClass);
			Property(NodeTypeMember.Property.PersonnelUse, o => o.PersonnelUse, PropertyName.PersonnelUse);
		}
	}
}
