﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.NodeMappings.Processes
{
	public abstract class ResourceSpecificationNodeMapping<TResoruceSpecification, TProcessDefinition>:
		HierarchyScopedObjectNodeMapping<TResoruceSpecification, SpecificationDefinitionElement>
		where TProcessDefinition : class, IProcessDefinition<TProcessDefinition> 
		where TResoruceSpecification : ResourceSpecification<TResoruceSpecification, TProcessDefinition>
	{
		/// <summary>
		/// A specification's intial Hierarchy Scope is set to the work master's Hierarchy Scope, it can be overridden.
		/// </summary>
		// ReSharper has this because it wants to make sure there's only one per generic type. This is readonly so it will always be the same
		// ReSharper disable once StaticMemberInGenericType
		private static readonly Func<SpecificationDefinitionElement, object> _hierarchyScopeFormula = 
			node => node.Properties[NodeTypeMember.Property.HierarchyScope].Value 
			?? (node.ParentNode?.TypeId == NodeTypeId.HierarchyScope
			? node.ParentNode 
			: node.ParentNode?.IsDefinitionNode ?? false
				? node.ParentNode.Cast<ModelElement>()?.Properties?[NodeTypeMember.Property.HierarchyScope]?.Value
			    : null);

		protected ResourceSpecificationNodeMapping(Func<FolderPath, IEnumerable<TResoruceSpecification>, IQueryable<string>> unavailableDisplayIdMapping): base(_hierarchyScopeFormula, unavailableDisplayIdMapping, false)
		{
			// Properties
			Property(NodeTypeMember.Property.Quantity, o => o.Quantity, PropertyName.Quantity);
		}
	}
}
