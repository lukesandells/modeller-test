﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.NodeMappings.Processes
{
	/// <summary>
	/// Node mapping for an Material Specification
	/// </summary>
	/// <typeparam name="TProcess">The type of process this specification is for</typeparam>
	public abstract class MaterialSpecificationNodeMapping<TProcess>: 
		ResourceSpecificationNodeMapping<MaterialSpecification<TProcess>, TProcess>
		where TProcess: class, IProcessDefinition<TProcess>
	{

		/// <summary>
		/// Function to find the unavailable IDs for new specifications
		/// </summary>
		private static readonly Func<FolderPath, IEnumerable<MaterialSpecification<TProcess>>, IQueryable<string>> _unavailableDisplayIdMapping =
			(path, exclusions) => PersistenceContext.Current.Session.Query<EquipmentSpecification<TProcess>>()
				.WhereVisibleTo(path.ParentOfType<HierarchyScope>())
				// Execute DB query and filter the rest in memory because the number of exclusions may exceed the available number of SQL parameters
				.Select(e => new { e.InternalId, e.ExternalId })
				.ToHashSet()
				.Union(path.ParentOfType<TProcess>().Root.AllMaterialSpecifications
						.Select(e => new { e.InternalId, e.ExternalId }))
				.Where(id => !exclusions.Any(e => e.InternalId == id.InternalId))
				.Select(id => id.ExternalId)
				.AsQueryable();
		/// <summary>
		/// Initializes a new MaterialSpecificationNodeMapping
		/// </summary>
		protected MaterialSpecificationNodeMapping() : base(_unavailableDisplayIdMapping)
		{
			// Properties
			Property(NodeTypeMember.Property.MaterialDefinition, o => o.MaterialDefinition, PropertyName.MaterialDefinition);
			Property(NodeTypeMember.Property.MaterialClass, o => o.MaterialClass, PropertyName.MaterialClass);
			Property(NodeTypeMember.Property.MaterialUse, o => o.MaterialUse, PropertyName.MaterialUse);
			Property(NodeTypeMember.Property.AssemblyRelationship, o => o.AssemblyRelationship, PropertyName.AssemblyRelationship);
			Property(NodeTypeMember.Property.AssemblyType, o => o.AssemblyType, PropertyName.AssemblyType);


			// Folders
			PrimaryFolder<MaterialSpecification<TProcess>>(NodeTypeMember.Folder.MaterialAssemblyElements, permittedType =>
			{
				permittedType.AddWith((@class, element) => @class.AddAssemblyElement(element));
				permittedType.RemoveWith((@class, element) => @class.AddAssemblyElement(element));
				permittedType.CanAddIf((@class, element) => @class.CanAdd(element));
			});
		}
	}
}
