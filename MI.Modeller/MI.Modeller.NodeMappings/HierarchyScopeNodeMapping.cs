﻿using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.Persistence;
using MI.Modeller.Domain;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.NodeMappings
{
	public class HierarchyScopeNodeMapping : DefinitionNodeMapping<HierarchyScope, ModelElement>
	{
		public HierarchyScopeNodeMapping()
		{
			// Type information
			Type(NodeTypeId.HierarchyScope, ObjectName.HierarchyScope)
				.PreFetchWith(PreFetchStrategy.ModelElement)
				.InitialiseWith
					.Property(NodeTypeMember.Property.EquipmentLevel, target => target.EquipmentLevel, PropertyName.EquipmentLevel);
			LinkAs(NodeTypeId.Equipment, hs => hs.DefiningEquipment);

			// Properties
			DisplayId(NodeTypeMember.Property.DisplayId, target => target.ExternalId, PropertyName.ID)
				.Prefix(@params => ExtensibleEnum.GetEnumValueDisplayName((EquipmentLevel)@params[0]).Replace(" ", string.Empty) + ".")
				.FindUnavailableWith(path => PersistenceContext.Current.Session.Query<ModelElement>()
					.Where(element => element.TypeId == NodeTypeId.HierarchyScope)
					.Where(element => !element.ParentFolder.IsRecycleBin)
					.Select(element => ((HierarchyScope)element.DesignObject).ExternalId))
				.GloballyUnique(true);
			Description(NodeTypeMember.Property.Description, target => target.Description, PropertyName.Description);

			// Additional permitted operations
			PermitNodeOperation(ModellerOperationType.Export);
			PermitFolderOperation(ModellerOperationType.Export);

			// Folders
			PrimaryFolder<HierarchyScope>(NodeTypeMember.Folder.HierarchyScopesAndEquipment, permittedType =>
			{
				permittedType.AddWith((parent, child) => parent.AddChild(child));
				permittedType.RemoveWith((parent, child) => parent.RemoveChild(child));
				permittedType.CanAddIf((parent, child) => parent.CanAddChild(child));
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.HierarchyScope));
			}).AddPermittedType<Equipment>(permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});
			
			SecondaryFolder<EquipmentClass>(NodeTypeMember.Folder.EquipmentClasses, FolderName.EquipmentClasses, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});

			SecondaryFolder<PhysicalAssetClass>(NodeTypeMember.Folder.PhysicalAssetClasses, FolderName.PhysicalAssetClasses, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<PersonnelClass>(NodeTypeMember.Folder.PersonnelClasses, FolderName.PersonnelClasses, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<MaterialClass>(NodeTypeMember.Folder.MaterialClasses, FolderName.MaterialClasses, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<MaterialDefinition>(NodeTypeMember.Folder.MaterialDefinitions, FolderName.MaterialDefinitions, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<OperationsDefinition>(NodeTypeMember.Folder.OperationsDefinitions, FolderName.OperationsDefinitions, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<WorkMaster>(NodeTypeMember.Folder.WorkMasters, FolderName.WorkMasters, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<ProcessSegment>(NodeTypeMember.Folder.ProcessSegments, FolderName.ProcessSegments, true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			SecondaryFolder<Model>(NodeTypeMember.Folder.Models, FolderName.Models, true, false, permittedType =>
			{
				//permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
			});

			// Node events
			OnCopy((original, copy) => (((HierarchyScope)original.DesignObject)).DefiningEquipment.Properties
				.ForEach(property => property.CopyTo(((HierarchyScope)copy.DesignObject).DefiningEquipment)));
		}
	}
}
