﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.TestConsole
{
	public class ExampleCode
	{

		public void OnAppStart()
		{
			UnitLibrary.Current = new UneceRecommendation20Library();
			NodeType.AddMappingAssembly(typeof(ApplicationRootNodeMapping).Assembly);
			Database.Connect("modeller.db", false, false); // create if not exists, not replace
			// create node potentially
		}

		public void LoadTreeView()
		{
			using (var scope = new PersistenceScope())
			{
				var rootNode = scope.Query<RootNode>().SingleOrDefault();
				if (rootNode == null)
				{
					rootNode = new RootNode(NodeTypeId.Root);
					scope.Save(rootNode);
				}
				foreach (var folder in rootNode.AllFolders)
				{
					// add node
				}
				scope.Done();
			}
		}

		public void GetListOfThingsToAdd(BrowserFolder folder)
		{
			//folder.
			// check init parameters for if there's an equipment level
		}

		public IEnumerable<OperationType> GetPermittedOperations(BrowserNode node, BrowserNode draggedNode = null)
		{
			if (draggedNode == null)
			{
				return node.PermittedOperations; // right click
			}
			return node.PermittedOperationsTo(draggedNode).Keys; // drag
		}

		public void CreateNewNode(BrowserFolder folder, Guid nodeTypeId, string externalId = null, EquipmentLevel? equipmentLevel = null)
		{
			// init paramters
			//NodeType.WithId(nodeTypeId)
			//	.Configuration.Initialiser.Properties.First().MemberId == NodeTypeMember.Property.EquipmentLevel;

			//var initParams = 
			if (!folder.CanAddNew(NodeType.WithId(nodeTypeId), new object[] { }))
			{
				throw new Exception();
			}
			using (var scope = new PersistenceScope())
			{
				var newNode = folder.Parent.AddNew(NodeType.WithId(nodeTypeId),
					folder,
					externalId,
					new object[]
					{
						equipmentLevel
					});
				scope.Save(newNode);
				scope.Done();
			}
		}

		public void CreateNewNode(BrowserNode node, Guid nodeType, string externalId = null, EquipmentLevel? equipmentLevel = null)
		{
			//if(!node.CanAddNew())
		}

		public void GetAttributes(DefinitionNode node)
		{
			node.Properties[Guid.NewGuid()].Value = "123";
		}

		public void GetUnderlyingObject(BrowserNode node)
		{
			if (node is ModelElement)
			{
				if (node.TypeId == NodeTypeId.HierarchyScope)
				{
					var hs = ((ModelElement) node).DesignObject;
				}
			}
		}
	}
}
