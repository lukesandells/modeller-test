﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;
using MI.Modeller.Persistence;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.ObjectBrowsing.Validation;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.TestConsole
{
	static class Program
	{
		static void Main(string[] args)
		{
			UnitLibrary.Current = new UneceRecommendation20Library();
			NodeType.AddMappingAssembly(typeof(ApplicationRootNodeMapping).Assembly);
			PropertyValueSerialiser.Add(quantity => quantity.Serialise().ToString(),
				s => SerialisedQuantity.Parse(s).Deserialise());
			PropertyValueSerialiser.Add(value => value.Serialise().ToString(), s => SerialisedValue.Parse(s).Deserialise());
			Database.Connect("test.db", true, false);
			NodeTransaction.Log = PersistentTransactionLog.Load();

			// Setup data
			Guid entId;
			Guid areaId;
			Guid matSpecId;
			Guid matClassId;
			Guid linkId;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var rootNode = new RootNode(NodeTypeId.Root);
				rootNode.Save();

				NodeTransaction transaction;
				var enterprise = (ModelElement) rootNode.AddNewAndPersist(NodeType.For(typeof(HierarchyScope)), "Enterprise",
					new object[] {EquipmentLevel.Enterprise});
				entId = enterprise.Id;

				var materialClass = (ModelElement)enterprise.AddNewAndPersist(NodeType.For(typeof(MaterialDefinition)), "MC");
				matClassId = materialClass.Id;

				var assembly = (ModelElement)enterprise.AddNewAndPersist(NodeType.For(typeof(MaterialDefinition)), "assembly");

				//var link = assembly.AddAndPersistTo(materialClass.AllFolders.Single(f => f.MemberId == NodeTypeMember.Folder.MaterialAssemblyElements));
				var link = assembly.AddAndPersistTo(materialClass);
				linkId = link.Id;
				scope.Done();
			}

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var node = scope.FindById<BrowserNode>(linkId);
				node.DeleteAndPersist(true);
				scope.Done();
			}
			Console.Write("Done");
			Console.ReadLine();

		}

		

	}

	class NodeCache
	{
		/// <summary>
		/// The instance of the ModellerNodeCache
		/// </summary>
		public static NodeCache Instance { get; private set; }

		public RootNode RootNode { get; }

		private IDictionary<BrowserNode, int> _loadedNodes = new Dictionary<BrowserNode, int>();

		private NodeCache(RootNode rootNode)
		{
			RootNode = rootNode;
		}

		public static void Initialise(RootNode rootNode)
		{
			if (Instance != null)
			{
				throw new Exception("Cache already initialised");
			}
			rootNode = ArgumentValidation.AssertNotNull(rootNode, nameof(rootNode));
			Instance = new NodeCache(rootNode);
		}

		public void OpenNode(BrowserNode node)
		{
			if (!_loadedNodes.ContainsKey(node))
			{
				_loadedNodes.Add(node, 0);
			}
			_loadedNodes[node]++;
		}

		public void Reattatch(PersistenceScope scope)
		{
			scope.Attach(RootNode);
			if (_loadedNodes.Any())
			{
				_loadedNodes.Keys.ForEach(scope.Attach);
				_loadedNodes.Keys.Where(k => k.IsDefinitionNode).SelectMany(k => k.Cast<DefinitionNode>().InboundLinks).ForEach(scope.Attach);
				foreach (var linkNode in _loadedNodes.Keys.Where(k => k.IsDefinitionNode).SelectMany(k => k.Cast<DefinitionNode>().PropertyLinksFolder.Nodes))
				{
					scope.Attach(linkNode);
					scope.Attach(linkNode.LinkedDefinitionNode);
				}
				//foreach (var loadedNodesKey in _loadedNodes.Keys)
				//{
					
				//	scope.Attach(loadedNodesKey);
				//	if (loadedNodesKey.Is<DefinitionNode>(out var definitionNode))
				//	{
				//		definitionNode.InboundLinks.Select(l => l.FromDefinitionNode).ForEach(scope.Attach);
				//		foreach (var linkNode in definitionNode.PropertyLinksFolder.Nodes
				//		) //NonEmptyNonInverseFolders.SelectMany(f => f.Nodes.OfType<LinkNode>()))
				//		{
				//			scope.Attach(linkNode);
				//			scope.Attach(linkNode.LinkedDefinitionNode);
				//		}
				//	}
				//}
			}
		}
	}
}
