﻿using System;

namespace MI.Modeller.Domain
{
	public static class NodeTypeId
	{
		public static Guid Root = new Guid("e3d58f16-2392-4295-9715-a0a8194baa50");
		public static Guid Model = new Guid("c573c83c-9d54-4130-aa11-a46114186d75");
		public static Guid Page = new Guid("af765475-50bf-4dea-8c00-bf81fb17c8c4");
		public static Guid HierarchyScope = new Guid("6be89cff-0361-47af-8cb8-a41a8bf51fe1");
		public static Guid Equipment = new Guid("e5470961-5234-42a6-abdb-94f35cc0efb1");
		public static Guid EquipmentClass = new Guid("9274b6f7-047e-4027-963c-d24840fa248b");
		public static Guid PhysicalAssetClass = new Guid("E692160A-F48D-4E4D-984F-E135E0832DB1");
		public static Guid PersonnelClass =  new Guid("23F4AC16-92F0-4AD1-9FE1-1F649174512C");
		public static Guid MaterialDefinition = new Guid("6D556975-7CB6-465C-804E-6BB5F5B3D1CF");
		public static Guid MaterialClass = new Guid("B6260978-EA9C-47A4-9ADC-A8809FECD67C");
		public static Guid WorkMaster = new Guid("2BC0AD5E-CDFB-4634-825A-E75075161D5D");
		public static Guid ProcessSegment = new Guid("56DD848F-1377-49A1-894D-F5E82BC5BC4D");
		public static Guid OperationsSegment = new Guid("B2AF987F-4AB2-4D10-A70C-01B317026313");
		public static Guid OperationsDefinition = new Guid("5EEF1115-01AA-40B3-BF23-61464A0F5002");
		public static Guid WorkMasterEquipmentSpecification = new Guid("1145E210-9BF8-40FD-A3DB-FDAC2A1C3FC9");
		public static Guid WorkMasterMaterialSpecification = new Guid("18380FC8-0088-4E4C-A9A6-804357EEEDCB");
		public static Guid WorkMasterPhysicalAssetSpecification = new Guid("D5ED1998-D421-4335-A640-4A1D5F88CFFA");
		public static Guid WorkMasterPersonnelSpecification = new Guid("89B4A9BA-4958-44E9-BB14-AA00F1F62E77");
		public static Guid OperationsSegmentEquipmentSpecification = new Guid("FEE209C9-0178-406E-A7AA-56B8BFB81E8D");
		public static Guid OperationsSegmentMaterialSpecification = new Guid("87BF4C5B-A3F3-4EE5-A874-8104347A034C");
		public static Guid OperationsSegmentPhysicalAssetSpecification = new Guid("61FA9643-931C-461E-93C7-62310C26A11D");
		public static Guid OperationsSegmentPersonnelSpecification = new Guid("25C0C48B-6A9A-41F9-847C-86D7058F8AD7");
		public static Guid ProcessSegmentEquipmentSpecification = new Guid("54AED4A1-8DE3-4864-9B8A-D508E96DDFA1");
		public static Guid ProcessSegmentMaterialSpecification = new Guid("AE4A9F0A-5213-4540-821C-C54838A8C898");
		public static Guid ProcessSegmentPhysicalAssetSpecification = new Guid("709B5591-4676-44B5-9D82-7B3A29A7BC12");
		public static Guid ProcessSegmentPersonnelSpecification = new Guid("C2F2BCCD-CC05-4106-9CD8-73F243B4BD3B");
	}
}
