﻿using MI.Framework.ObjectBrowsing;

namespace MI.Modeller.Domain
{
	/// <summary>
	/// Defines a type of operation that can be performed against a node.
	/// </summary>
	public class ModellerOperationType : OperationType
	{

		/// <summary>
		/// Initializes a new ModellerOperationType
		/// </summary>
		/// <param name="name"></param>
		protected ModellerOperationType(string name) : base(name) { }

		/// <summary>
		/// Export of a node or folder to a specified file form.
		/// </summary>
		public static OperationType Export { get; } = new ModellerOperationType("Export");
	}
}
