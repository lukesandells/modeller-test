﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Domain
{
	public class ResourceDefinitionElement : ModelElement
	{
		public virtual IEnumerable<SpecificationDefinitionElement> LinkedSpecifications => InboundLinks.Where(l => l.FromDefinitionNode != null).Select(l => l.FromDefinitionNode).Where(n => n.TargetObject.GetType().HasInterface(typeof(IResourceTransaction))).Cast<SpecificationDefinitionElement>();

		public virtual IEnumerable<ProcessDefinitionElement> LinkedProcesses => LinkedSpecifications.Select(n => n.ParentNode).Cast<ProcessDefinitionElement>();
	}
}
