﻿using System;
using System.Collections.Generic;
using MI.Framework;
using MI.Framework.Validation;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Validation;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Domain.Validation
{
	/// <summary>
	/// Validates the visibility of definition nodes linked from other definition nodes.
	/// </summary>
	public class NodeVisibilityValidator : INodeValidator
	{
		/// <summary>
		/// The node visibility validation result type.
		/// </summary>
		private class NodeVisibilityValidationResultType : NodeValidationResultType
		{
			/// <summary>
			/// The severity of the validation result.
			/// </summary>
			public override NodeValidationResultSeverity Severity => NodeValidationResultSeverity.Error;

			/// <summary>
			/// Suggests possible resolutions for the type of validation result.
			/// </summary>
			public override IEnumerable<NodeValidationResultResolution> SuggestResolutions(NodeValidationResult validationResult)
			{
				// There are no automatic resolutions to this problem
				return new NodeValidationResultResolution[] { };
			}
		}

		/// <summary>
		/// The types of validation results produced by the validator.  Returned types must derive from the <see cref="NodeValidationResultType"/> class.
		/// </summary>
		public IEnumerable<Type> ResultTypes => new[] { typeof(NodeVisibilityValidationResultType) };

		/// <summary>
		/// The trigger condition for validating a node with the validator.
		/// </summary>
		/// <remarks>
		/// This validator validates that a linked definition node of a link node is visible from the source definition node of that link node.
		/// It must therefore be triggered whenever a new link node is created because establishment of links to definition nodes that are 
		/// not visible from the source node is not prevented by the application on dragging and dropping.  This validator must also be triggered
		/// any time the path of either a link node or definition node changes because such an action may render a linked definition node out of 
		/// the field of visibility of the link's source definition node.
		/// </remarks>
		public INodeValidationTriggerCondition TriggerCondition => NodeValidationTriggerCondition.When.NodeOfType.LinkNodeTo<ModelElement>().IsAddedNew
			|| NodeValidationTriggerCondition.When.NodeOfType.Any<ModelElement>().PathChanged;

		/// <summary>
		/// Validates the given node, adding <see cref="NodeValidationResult"/> objects to whatever nodes are required to reflect the validation result.
		/// </summary>
		/// <param name="node">The node to be validated.</param>
		public void ValidateNode(BrowserNode node)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			
			// If the node is a link node, need to check if its linked definition node is visible to the link node's parent.
			// Don't need to check new property links because the application should prevent creation of property links to 
			// definition nodes that are not visible to the node to which the corresponding property belongs.
			if (node.Is<LinkNode>(out var linkNode) && !linkNode.IsPropertyLink)
			{
				var sourceHierarchyScope = HierarchyScope.Of(linkNode.ParentNode.Cast<ModelElement>().DesignObject);
				if (!linkNode.LinkedDefinitionNode.Cast<ModelElement>().DesignObject.IsVisibleFrom(sourceHierarchyScope))
				{
					var displayMessage = string.Format(ValidationResultMessage.ModelElementNotVisible, linkNode.Type.DisplayName,
						linkNode.DisplayId, linkNode.ParentNode.Type.DisplayName, linkNode.ParentNode.DisplayId, sourceHierarchyScope.ExternalId);
					linkNode.ApplyValidationResult<NodeVisibilityValidationResultType>(displayMessage);
				}
			}

			// If the node is a definition node, check if the node is visible to its inbound links
			if (node.Is<DefinitionNode>(out var definitionNode))
			{
				definitionNode.InboundLinks.ForEach(n => ValidateNode(n));
			}
		}
	}
}
