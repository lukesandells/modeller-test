﻿using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Validation;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Domain.Validation
{
	/// <summary>
	/// Validates a node display ID to check whether the new value causes node in an ascendant hierarchy scope to have an ambigous node reference.
	/// </summary>
	/// <remarks>
	/// It can be assumed that the <see cref="DisplayIdInputValidator"/> has run before this validator because it is automatically added 
	/// to the validators for the display ID property before the property mapping is returned, against which this validator is then configured.
	/// </remarks>
	public class AmbiguousNodeReferenceDisplayIdInputValidator : InputValidator<string, string>
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="AmbiguousNodeReferenceDisplayIdInputValidator"/> class.
		/// </summary>
		public AmbiguousNodeReferenceDisplayIdInputValidator()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="AmbiguousNodeReferenceDisplayIdInputValidator"/> class.
		/// </summary>
		public AmbiguousNodeReferenceDisplayIdInputValidator(InputValidationPredicate<string> predicate)
			: base(predicate)
		{
		}

		/// <summary>
		/// Validates the given input value.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input value to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input as an instance of the appropriate type; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, string input, out string validated, out string displayMessage)
		{
			var node = property.ParentNodes.Single();
			var hierarchyScopeNode = HierarchyScopeNodeOf(node);

			// Only need to check inbound links with a linked definition node in a hierarchy scope that is a descendant of the
			// link's source node's hierarchy scope because node IDs in a hierarchy scope and its ascendants are guaranteed to 
			// be unique and therefore cannot be ambiguous.  Only descendant nodes in different hierarchy scopes may have the 
			// same type and ID.
			var ambiguousLink = hierarchyScopeNode.Descendants.OfType<ModelElement>()
				.Where(n => n.TargetObject is HierarchyScopedObject)
				.SelectMany(n => n.InboundLinks)
				.Where(ln => hierarchyScopeNode.IsDescendantOf(HierarchyScopeNodeOf(ln.FromDefinitionNode)))
				.Where(ln => ln.LinkedDefinitionNode != node && ln.Type == node.Type && ln.DisplayId == input)
				.FirstOrDefault();

			if (ambiguousLink != null)
			{
				displayMessage = string.Format(ValidationResultMessage.DiplayIdCausesAmbiguousObjectReference, ambiguousLink.Type.DisplayName,
					ambiguousLink.DisplayId, ambiguousLink.LinkedDefinitionNode.ParentFolder);
				validated = null;
				return false;
			}

			validated = input;
			displayMessage = null;
			return true;
		}

		/// <summary>
		/// Returns the hierarchy scope node for the given node.
		/// </summary>
		/// <param name="element">The node for which to get the hierarchy scope node.</param>
		/// <returns>The hierarchy scope node for the given node.</returns>
		private ModelElement HierarchyScopeNodeOf(DefinitionNode element)
		{
			return element.TypeId == NodeTypeId.HierarchyScope
				? element.Cast<ModelElement>()
				: element.Properties[NodeTypeMember.Property.HierarchyScope].CorrespondingLinkNode.LinkedDefinitionNode.Cast<ModelElement>();
		}
	}
}
