﻿using System;
using System.Linq;
using System.Collections.Generic;
using MI.Framework;
using MI.Framework.Validation;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Validation;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Domain.Validation
{
	/// <summary>
	/// Validates the visibility of definition nodes linked from other definition nodes.
	/// </summary>
	public class AmbiguousNodeReferenceValidator : INodeValidator
	{
		/// <summary>
		/// The node visibility validation result type.
		/// </summary>
		private class AmbiguousNodeReferenceValidationResultType : NodeValidationResultType
		{
			/// <summary>
			/// The severity of the validation result.
			/// </summary>
			public override NodeValidationResultSeverity Severity => NodeValidationResultSeverity.Error;

			/// <summary>
			/// Suggests possible resolutions for the type of validation result.
			/// </summary>
			public override IEnumerable<NodeValidationResultResolution> SuggestResolutions(NodeValidationResult validationResult)
			{
				// There are no automatic resolutions to this problem
				return new NodeValidationResultResolution[] { };
			}
		}

		/// <summary>
		/// The types of validation results produced by the validator.  Returned types must derive from the <see cref="NodeValidationResultType"/> class.
		/// </summary>
		public IEnumerable<Type> ResultTypes => new[] { typeof(AmbiguousNodeReferenceValidationResultType) };

		/// <summary>
		/// The trigger condition for validating a node with the validator.
		/// </summary>
		/// <remarks>
		/// <para>There are two trigger conditions defined for this validator.  The first trigger condition is the creation of any new 
		/// link node to a <see cref="ModelElement"/> node or any new model element node. A new link node to any model element may have 
		/// an ambiguous reference. Also, a new model element node in a given location may make an existing link node's reference to 
		/// another model element node ambiguous.</para>
		/// 
		/// <para>The other trigger condition for this validator is change of folder path of a model element node or any link node to a 
		/// model element node.  An existing link node's reference may become ambiguous in a new location.  Also, moving a model element 
		/// node to a new location may make an existing link node's reference to another model element node ambiguous.</para>
		/// </remarks>
		public INodeValidationTriggerCondition TriggerCondition => NodeValidationTriggerCondition.When.NodeOfType.Any<ModelElement>().IsAddedNew
			|| NodeValidationTriggerCondition.When.NodeOfType.Any<ModelElement>().PathChanged;

		/// <summary>
		/// Validates the given node, adding <see cref="NodeValidationResult"/> objects to whatever nodes are required to reflect the validation result.
		/// </summary>
		/// <param name="node">The node to be validated.</param>
		public void ValidateNode(BrowserNode node)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			
			// Interested in new or moved link nodes that reference hierarchy scoped objects to see whether any other definition node
			// in the link's source node's hierarchy scope is of the same type with the same ID as the linked definition node.
			if (node.Is<LinkNode>(out var linkNode) && linkNode.TargetObject is HierarchyScopedObject 
				&& linkNode.LinkedDefinitionNode.IsDescendantOf(linkNode.FromDefinitionNode))
			{
				// Determine the hierarchy scope of the link node's source definition node
				var linkSourceHierarchyScopeNode = HierarchyScopeNodeOf(linkNode.FromDefinitionNode);

				// Only need to perform this check if the linked definition node's hierarchy scope is a descendant of the source node's 
				// hierarchy scope because if it is an ascendant, then the reference cannot be ambiguous because node IDs in a hierarchy 
				// scope and its ascendants are guaranteed to be unique.  Only descendant nodes in different hierarchy scopes may have 
				// the same type and ID.
				if (HierarchyScopeNodeOf(linkNode.LinkedDefinitionNode).IsDescendantOf(linkSourceHierarchyScopeNode))
				{
					var ambiguousDefinitionNode = linkSourceHierarchyScopeNode.Descendants.OfType<ModelElement>()
						.FirstOrDefault(n => n != linkNode.LinkedDefinitionNode
							&& n.Type == linkNode.Type
							&& n.DisplayId == linkNode.DisplayId);

					if (ambiguousDefinitionNode != null)
					{
						linkNode.ApplyValidationResult<AmbiguousNodeReferenceValidationResultType>(
							string.Format(ValidationResultMessage.AmbiguousObjectReference, linkNode.Type.DisplayName,
								linkNode.DisplayId, linkNode.ParentNode.Type.DisplayName, linkNode.ParentNode.DisplayId,
								ambiguousDefinitionNode.ParentFolder));
					}
				}
			}

			// Also interested in any new or moved definition nodes to see whether any other definition nodes in the definition node's 
			// hierarchy scope has an inbound link referencing an object of the same type with the same ID.
			if (node.Is<DefinitionNode>(out var definitionNode) && definitionNode.TargetObject is HierarchyScopedObject)
			{
				// Get the hierarchy scope of the definition node being validated
				var hierarchyScopeNode = HierarchyScopeNodeOf(definitionNode);

				// Only need to check inbound links with a linked definition node in a hierarchy scope that is a descendant of the
				// link's source node's hierarchy scope because node IDs in a hierarchy scope and its ascendants are guaranteed to 
				// be unique and therefore cannot be ambiguous.  Only descendant nodes in different hierarchy scopes may have the 
				// same type and ID.
				var ambiguousLinks = hierarchyScopeNode.Descendants.OfType<ModelElement>()
					.Where(n => n.TargetObject is HierarchyScopedObject)
					.SelectMany(n => n.InboundLinks)
					.Where(ln => hierarchyScopeNode.IsDescendantOf(HierarchyScopeNodeOf(ln.FromDefinitionNode)))
					.Where(ln => ln.LinkedDefinitionNode != definitionNode && ln.Type == definitionNode.Type && ln.DisplayId == definitionNode.DisplayId);

				ambiguousLinks.ForEach(ambiguousLink => ambiguousLink.ApplyValidationResult<AmbiguousNodeReferenceValidationResultType>(
					string.Format(ValidationResultMessage.AmbiguousObjectReference, ambiguousLink.Type.DisplayName,
						ambiguousLink.DisplayId, ambiguousLink.ParentNode.Type.DisplayName, ambiguousLink.ParentNode.DisplayId,
						node.ParentFolder)));
			}
		}

		/// <summary>
		/// Returns the hierarchy scope node for the given node.
		/// </summary>
		/// <param name="element">The node for which to get the hierarchy scope node.</param>
		/// <returns>The hierarchy scope node for the given node.</returns>
		private ModelElement HierarchyScopeNodeOf(DefinitionNode element)
		{
			return element.TypeId == NodeTypeId.HierarchyScope ? (ModelElement)element 
				: (ModelElement)element.Properties[NodeTypeMember.Property.HierarchyScope].Value;
		}
	}
}
