﻿using MI.Framework.Validation;

namespace MI.Modeller.Domain
{
	public class ConnectorOccurrence : Occurrence
	{
		public virtual ObjectOccurrence From => _from;
		private ObjectOccurrence _from;

		public virtual ObjectOccurrence To => _to;
		private ObjectOccurrence _to;

		protected ConnectorOccurrence()
		{
		}

		internal ConnectorOccurrence(Page page, ModelElement element, ObjectOccurrence from, ObjectOccurrence to)
			: base(page, element)
		{
			_from = ArgumentValidation.AssertNotNull(from, nameof(from));
			_to = ArgumentValidation.AssertNotNull(to, nameof(to));
		}
	}
}
