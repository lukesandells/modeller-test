﻿using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Domain
{
	/// <summary>
	/// Represents an element that may appear as zero or more occurrences (see <see cref="Occurrence"/> on a <see cref="Model"/>.
	/// </summary>
	public class ModelElement : DefinitionNode
	{
		/// <summary>
		/// Gets the <see cref="DesignObject"/> corresponding to this model element.
		/// </summary>
		public virtual DesignObject DesignObject { get => (DesignObject)base.TargetObject; set => base.SetTargetObject(value); }

		/// <summary>
		/// The occurrences of this model element node over all model nodes.
		/// </summary>
		public virtual IEnumerable<Occurrence> Occurrences => _occurrences;
		private ISet<Occurrence> _occurrences = new HashSet<Occurrence>();

		/// <summary>
		/// Adds the given occurrence to this model element.
		/// </summary>
		/// <param name="occurrence">The occurrence to add.</param>
		protected internal virtual void AddOccurrence(Occurrence occurrence)
		{
			// No validation checks required because only invoked internally
			_occurrences.Add(occurrence);
		}

		/// <summary>
		/// Removes the given occurrence from this model element.
		/// </summary>
		/// <param name="occurrence">The occurrence to remove.</param>
		protected internal virtual void RemoveOccurrence(Occurrence occurrence)
		{
			// No validation checks required because only invoked internally
			_occurrences.Remove(occurrence);
		}

	}
}
