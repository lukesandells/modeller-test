﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Validation;

namespace MI.Modeller.Domain
{
	/// <summary>
	/// Represents a model (diagram) with zero or more pages.
	/// </summary>
	public class Model : DefinitionNode
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="Model"/> class.
		/// </summary>
		protected Model()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Model"/> class.
		/// </summary>
		/// <param name="displayId">The name of the new model.</param>
		public Model(string displayId)
		{
			base.DisplayId = ArgumentValidation.AssertNotNullOrEmpty(displayId, nameof(displayId));
		}
	}
}
