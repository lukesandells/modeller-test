﻿using System.ComponentModel;

namespace MI.Modeller.Domain
{
    public enum ModelType
    {
        [Description("Sales and Operations Planning")]
        SalesAndOperationsPlanningModel,
        [Description("Operations Definition")]
        OperationsDefinitionModel,
        [Description("Work Definition")]
        WorkDefinitionModel,
        [Description("Unknown")]
        Unknown
    }
}