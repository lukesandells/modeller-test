﻿using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.OperationsManagement.Domain.Core;

namespace MI.Modeller.Domain
{
	/// <summary>
	/// Represents a ModelElement that represents a process
	/// </summary>
	public class ProcessDefinitionElement : ModelElement
	{
		#region  Fields/Properties

		/// <summary>
		/// The resources involved in this process
		/// </summary>
		public virtual IEnumerable<ResourceDefinitionElement> LeafProcessResources => LeafSpecificationNodes.Where(s => !s.Descendants.Any(d => d.IsDefinitionNode)).SelectMany(s => s.Resources);

		/// <summary>
		/// The specification folders of this process
		/// </summary>
		public virtual IEnumerable<BrowserFolder> SpecificationFolders => AllFolders
			.Where(f => f.MemberId == NodeTypeMember.Folder.MaterialSpecifications 
				|| f.MemberId == NodeTypeMember.Folder.PersonnelSpecifications 
				|| f.MemberId == NodeTypeMember.Folder.PhysicalAssetSpecifications 
				|| f.MemberId == NodeTypeMember.Folder.EquipmentSpecifications);

		public virtual IEnumerable<BrowserFolder> DescendantSpecificationFolders => Descendants.Where(d => d.Is<ProcessDefinitionElement>()).Cast<ProcessDefinitionElement>().SelectMany(n => n.SpecificationFolders);

		/// <summary>
		/// The leaf nodes of specifications within this process
		/// </summary>
		public virtual IEnumerable<SpecificationDefinitionElement> LeafSpecificationNodes => SpecificationFolders.Union(DescendantSpecificationFolders).SelectMany(f => f.Nodes.Cast<SpecificationDefinitionElement>()
																			.Where(n => !n.Descendants.Any(d => d.IsDefinitionNode) && n.TargetObject.GetType().HasInterface(typeof(IResourceTransaction))));


		/// <summary>
		/// All specifications within this process
		/// </summary>
		public virtual IEnumerable<SpecificationDefinitionElement> AllSpecificationNodes => SpecificationFolders.Union(DescendantSpecificationFolders).SelectMany(f => f.Nodes.Cast<SpecificationDefinitionElement>());

		#endregion
	}
}