﻿using System;

namespace MI.Modeller.Domain
{
	public static class NodeTypeMember
	{
		public static class Folder
		{
			public static Guid Enterprises { get; } = new Guid("cbf74d15-f184-4163-b963-98103bc29cc8");
			public static Guid HierarchyScopesAndEquipment { get; } = new Guid("c11af952-33cd-48a1-8f89-19ead0e80249");
			public static Guid Equipment { get; } = new Guid("ee1711fc-ec38-4184-9572-814444b99439");
			public static Guid EquipmentClasses { get; } = new Guid("fb6dc2a0-5d30-4d54-bfa4-49a1f0f6ad97");
			public static Guid Models { get; } = new Guid("cb7344b7-27f8-4bd4-a642-7f53d6393b02");
			public static Guid Pages { get; } = new Guid("b210125f-fa39-442d-a4d9-543e8daacfdd");
			public static Guid PhysicalAssetClasses { get; } = new Guid("ADF26456-2758-48D4-8A30-6D41138A6CE2");
			public static Guid PersonnelClasses { get; } = new Guid("A50F4EFD-CE59-4C3E-80E9-28937C28BE42");
			public static Guid MaterialClasses { get; } = new Guid("75B52AA4-D470-4E5C-AC95-7AD836178EA6");
			public static Guid MaterialDefinitions { get; } = new Guid("B5CC244C-6C5A-4FEA-A0F8-BC91E5D96C70");
			public static Guid MaterialSubclasses { get; } = new Guid("900D0D2A-8C55-4451-97D7-72EC7AA390FC");
			public static Guid MaterialAssemblyElements { get; } = new Guid("E8EBEFCE-5406-474A-9EF0-5FF9D56DB0B8");
			public static Guid WorkMasters { get; } = new Guid("13A427D0-D117-4879-95B6-9D97D628CE52");
			public static Guid MaterialSpecifications { get; } = new Guid("2D475AF6-74BA-418D-8E25-30B66B2C1EDE");
			public static Guid EquipmentSpecifications { get; } = new Guid("26103992-A45E-421B-BDEA-40A28C8628A1");
			public static Guid PersonnelSpecifications { get; } = new Guid("D1680D34-3806-4028-ADF3-98B5B1AB0408");
			public static Guid PhysicalAssetSpecifications { get; } = new Guid("4B4001B5-C84C-4B8D-B266-F93172076868");
			public static Guid ProcessSegments { get; } = new Guid("B80507EF-E35E-454C-9FC2-3E03F4F26F36");
			public static Guid OperationsSegments { get; } = new Guid("13B11134-66DD-4F42-A3CF-30EABCCC6B22");
			public static Guid OperationsDefinitions { get; } = new Guid("B3EBD453-76EE-4487-909A-90D5B8A798BE");
		}

		public static class Property
		{
			public static Guid DisplayId { get; } = new Guid("28863ffd-6667-4c6d-955b-c6d1c484f464");
			public static Guid Description { get; } = new Guid("f0d89bfa-64b4-41d9-a859-f93e1df2815f");
			public static Guid EquipmentLevel { get; } = new Guid("88eef99f-e64d-4b71-bba2-e6296e8f6093");
			public static Guid HierarchyScope { get; } = new Guid("cb0f604f-b913-4b3d-a3a1-f2f1f9340e8c");
			public static Guid Model { get; } = new Guid("e4b41f0a-d7ab-4ba0-abdb-4ca1dbd155eb");
			public static Guid Duration { get; } = new Guid("44AE0164-208C-4EED-989E-7967B05D80ED");
			public static Guid OperationsSegment { get; } = new Guid("4B15FFD8-3EDB-4FCC-AAE6-28E7968A6530");
			public static Guid ProcessType { get; } = new Guid("D031DCC3-14A4-4997-B918-0CDEEA06D456");
			public static Guid PublishedDate { get; } = new Guid("27F2112D-864F-4EEE-A138-09266C9F9219");
			public static Guid Version { get; } = new Guid("30B37344-A41D-49F5-8DEB-5EA527D81523");
			public static Guid BillOfResourcesId { get; } = new Guid("52E7FEED-190C-49B4-A472-A21F7E4A1448");
			public static Guid WorkDefinitionId { get; } = new Guid("06C42CC2-6D39-4E68-8267-34F2E7458572");
			public static Guid Quantity { get; } = new Guid("7AB44A6D-8BF3-4B8F-AC15-A6C51143FF86");
			public static Guid Equipment { get; } = new Guid("76A43EA8-7C79-4BF7-9A92-D44AA05A849F");
			public static Guid EquipmentClass { get; } = new Guid("217D16C1-B3F0-445C-BFC4-C79D97ED26C9");
			public static Guid EquipmentUse { get; } = new Guid("1DC3BF2F-3208-41ED-97BD-2DAC7C69B36A");
			public static Guid MaterialDefinition { get; } = new Guid("94E26EFB-2119-4221-B55B-FEE4C0947D2B");
			public static Guid MaterialClass { get; } = new Guid("E76163FA-2EEE-400B-AFF9-55F533BD19A3");
			public static Guid MaterialUse { get; } = new Guid("606630A5-BE90-4608-8EAF-291EBB6035F2");
			public static Guid AssemblyType { get; } = new Guid("3B8D9B08-4607-4CCF-849D-017CC59F587D");
			public static Guid AssemblyRelationship { get; } = new Guid("3547CFCB-E3B1-4C2F-A3A5-6FE07FA8B60F");
			public static Guid PersonnelClass { get; } = new Guid("6C915156-452C-4EE8-92D1-8F03C9795F8E");
			public static Guid PersonnelUse { get; } = new Guid("F63180D5-1EBC-460B-8FE0-380B12305CEA");
			public static Guid PhysicalAssetClass { get; } = new Guid("D60F00C0-CCE4-46FA-AF32-D811FF20DBDC");
			public static Guid PhysicalAssetUse { get; } = new Guid("887DF00F-B888-4743-8F2C-22F6D310E5C9");
		}
	}
}
