﻿using System;
using MI.Framework.Validation;

namespace MI.Modeller.Domain
{
	public abstract class Occurrence
	{
		public virtual Guid Id => _id;
		private Guid _id = Guid.NewGuid();

		public virtual Page Page => _page;
		private Page _page;

		public virtual ModelElement ModelElement => _modelElement;
		private ModelElement _modelElement;

		protected Occurrence()
		{
		}

		protected Occurrence(Page page, ModelElement element)
		{
			_page = ArgumentValidation.AssertNotNull(page, nameof(page));
			_modelElement = ArgumentValidation.AssertNotNull(element, nameof(element));
		}
	}
}
