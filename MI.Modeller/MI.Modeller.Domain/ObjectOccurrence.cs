﻿using System.Collections.Generic;
using MI.Framework.Validation;

namespace MI.Modeller.Domain
{
	public class ObjectOccurrence : Occurrence
	{
		public virtual IEnumerable<ConnectorOccurrence> InboundConnectors => _inboundConnectors;
		private ISet<ConnectorOccurrence> _inboundConnectors = new HashSet<ConnectorOccurrence>();

		public virtual IEnumerable<ConnectorOccurrence> OutboundConnectors => _outboundConnectors;
		private ISet<ConnectorOccurrence> _outboundConnectors = new HashSet<ConnectorOccurrence>();

		public virtual ObjectOccurrence Parent { get; set; }
		private ObjectOccurrence _parent;

		protected ObjectOccurrence()
		{
		}

		internal ObjectOccurrence(Page page, ModelElement element, ObjectOccurrence parent)
			: base(page, element)
		{
			_parent = parent;
		}

		protected internal virtual void AddConnector(ConnectorOccurrence connector)
		{
			connector = ArgumentValidation.AssertNotNull(connector, nameof(connector));

			if (connector.From == this)
			{
				_outboundConnectors.Add(connector);
			}
			else if (connector.To == this)
			{
				_inboundConnectors.Add(connector);
			}
		}

		protected internal virtual void RemoveConnector(ConnectorOccurrence connector)
		{
			connector = ArgumentValidation.AssertNotNull(connector, nameof(connector));

			if (connector.From == this)
			{
				_outboundConnectors.Remove(connector);
			}
			else if (connector.To == this)
			{
				_inboundConnectors.Remove(connector);
			}
		}
	}
}
