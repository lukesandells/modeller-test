﻿using MI.Framework;
using MI.OperationsManagement.Domain.Core;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Domain
{
	public class SpecificationDefinitionElement: ModelElement
	{
		public virtual ProcessDefinitionElement Process => ParentNode.Is<ProcessDefinitionElement>() 
			? ParentNode.As<ProcessDefinitionElement>()
			: (ParentNode.As<SpecificationDefinitionElement>()).Process;

		public virtual IEnumerable<ResourceDefinitionElement> Resources => PropertyLinksFolder.Nodes
			.Where(n => n.CorrespondingPropertyConfiguration.NativeType.HasInterface(typeof(IResourceObject<>)))
			.Select(n => n.LinkedDefinitionNode.Cast<ResourceDefinitionElement>());
	}
}
