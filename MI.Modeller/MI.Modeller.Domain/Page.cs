﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Validation;

namespace MI.Modeller.Domain
{
	/// <summary>
	/// Represents a page on a model.
	/// </summary>
	public class Page : DefinitionNode
	{
		/// <summary>
		/// The <see cref="ModelElement"/> occurrences (see <see cref="Occurrence"/>) on this page.
		/// </summary>
		public virtual IEnumerable<Occurrence> Occurrences => _occurrences;
		private ISet<Occurrence> _occurrences = new HashSet<Occurrence>();

		/// <summary>
		/// Initialises a new instance of the <see cref="Page"/> class.
		/// </summary>
		protected Page()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Page"/> class.
		/// </summary>
		/// <param name="displayId">The name of the new page.</param>
		public Page(string displayId)
		{
			base.DisplayId = ArgumentValidation.AssertNotNullOrEmpty(displayId, nameof(displayId));
		}

		/// <summary>
		/// Adds a given <see cref="ModelElement"/> to this page as an <see cref="ObjectOccurrence"/>, nested within 
		/// a given parent <see cref="ObjectOccurrence"/>.
		/// </summary>
		/// <param name="element">The element to add to this page.</param>
		/// <param name="parent">The <see cref="ObjectOccurrence"/> in which the new occurrence is nested.</param>
		/// <returns>The new object occurrence.</returns>
		public virtual ObjectOccurrence AddElementAsObject(ModelElement element, ObjectOccurrence parent)
		{
			element = ArgumentValidation.AssertNotNull(element, nameof(element));
			var occurrence = new ObjectOccurrence(this, element, parent);
			_occurrences.Add(occurrence);
			element.AddOccurrence(occurrence);
			return occurrence;
		}

		/// <summary>
		/// Adds a given <see cref="ModelElement"/> to this page as an <see cref="ObjectOccurrence"/>.
		/// </summary>
		/// <param name="element">The element to add to this page.</param>
		/// <returns>The new object occurrence.</returns>
		public virtual ObjectOccurrence AddElementAsObject(ModelElement element)
		{
			return AddElementAsObject(element, null);
		}

		/// <summary>
		/// Adds a given <see cref="ModelElement"/> to this page as a <see cref="ConnectorOccurrence"/> between two object occurrences
		/// (see <see cref="ObjectOccurrence"/>).
		/// </summary>
		/// <param name="element">The element to add to this page.</param>
		/// <returns>The new connector occurrence.</returns>
		public virtual ConnectorOccurrence AddElementAsConnector(ModelElement element, ObjectOccurrence from, ObjectOccurrence to)
		{
			element = ArgumentValidation.AssertNotNull(element, nameof(element));
			from = ArgumentValidation.AssertNotNull(from, nameof(from));
			to = ArgumentValidation.AssertNotNull(to, nameof(to));

			var occurrence = new ConnectorOccurrence(this, element, from, to);
			from.AddConnector(occurrence);
			to.AddConnector(occurrence);

			_occurrences.Add(occurrence);
			element.AddOccurrence(occurrence);
			return occurrence;
		}

		/// <summary>
		/// Removes the identified occurrence from this model.
		/// </summary>
		/// <param name="occurrenceId">Identifies the occurrence to remove.</param>
		public virtual void RemoveOccurrence(Guid occurrenceId)
		{
			RemoveOccurrence(_occurrences.Where(o => o.Id == occurrenceId).Single());
		}

		/// <summary>
		/// Removes the given occurrence.
		/// </summary>
		/// <param name="occurrence">The occurrence to remove.</param>
		public virtual void RemoveOccurrence(Occurrence occurrence)
		{
			if (!_occurrences.Contains(occurrence))
			{
				throw new ArgumentException(ExceptionMessage.ItemNotFoundInCollection);
			}

			var connector = occurrence as ConnectorOccurrence;
			if (connector != null)
			{
				connector.From.RemoveConnector(connector);
				connector.To.RemoveConnector(connector);
			}
			_occurrences.Remove(occurrence);
			occurrence.ModelElement.RemoveOccurrence(occurrence);
		}
	}
}
