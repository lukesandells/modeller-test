﻿using System;
using System.Linq.Expressions;
using MI.Framework;
using MI.Framework.Persistence.NHibernate;
using MI.Framework.Persistence.NHibernate.Types;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Impl.CustomizersImpl;
using UncertaintyType = MI.Modeller.Persistence.Types.UncertaintyType;

namespace MI.Modeller.Persistence.Mappings
{
	/// <summary>
	/// Helper class for NHibernate mappings.
	/// </summary>
	internal static class Map
	{
		/// <summary>
		/// Maps a quantity property.
		/// </summary>
		/// <typeparam name="TEntity">The type of entity with the quantity property.</typeparam>
		/// <param name="mapping">The NHibernate mapping.</param>
		/// <param name="quantityProperty">The quantity property expression.</param>
		/// <param name="tableName">The name of the table to store the quantity elements.</param>
		/// <param name="foreignKeyColumnName">The name of the foreign key column in the quantity element table
		/// (see <paramref name="tableName"/>) referring to the object with the quantity property.</param>
		public static void WithQuantity<TEntity>(PropertyContainerCustomizer<TEntity> mapping, Expression<Func<TEntity, Quantity>> quantityProperty, 
			string tableName, string foreignKeyColumnName)
		{
			mapping = ArgumentValidation.AssertNotNull(mapping, nameof(mapping));
			mapping.Component(quantityProperty, component =>
			{
				// Access as NoSetter to prevent a copy occurring on the setter because this causes NH to think that
				// the quantity has been changed on Flush and it re-saves the quantity elements, even though the copy of the
				// quantity is equal to copied quantity (i.e. == operator and .Equals() both return true).  NH must be
				// doing an Object.ReferenceEquals to see whether the object is dirty.
				component.Access(Accessor.NoSetter);
				component.Map<string, QuantityElement>("_elements", map =>
				{
					map.Table(tableName);
					map.Key(key => key.Column(foreignKeyColumnName));
					map.Access(Accessor.Field);
					map.Lazy(CollectionLazy.NoLazy);
					// Subselect to prevent N+1 selects on quantity elements when lazy-loading collections of objects with a quantity
					map.Fetch(CollectionFetchMode.Subselect); 
					map.Cascade(Cascade.All);
				},
				key => key.Element(element => element.Formula("Key")),
				rel => rel.Component(quantityElement =>
				{
					quantityElement.Property(o => o.Key, property =>
					{
						property.Access(Accessor.NoSetter);
						property.NotNullable(true);
					});
					quantityElement.Property(o => o.DataType, property =>
					{
						property.Type<ExtensibleEnumType<QuantityDataType>>();
						property.Access(Accessor.NoSetter);
					});
					quantityElement.Property("_unitDecimal", property =>
					{
						property.Type<UnitDecimalType>();
						property.Access(Accessor.Field);
						property.Columns(column => column.Name("Quantity"), column => column.Name("Unit"));
					});
					quantityElement.Property(o => o.Uncertainty, property =>
					{
						property.Type<UncertaintyType>();
						property.Access(Accessor.NoSetter);
						property.Columns(
							column => column.Name("StandardUncertainty"),
							column => column.Name("CoverageFactor"),
							column => column.Name("ConfidenceLevel"),
							column => column.Name("Accuracy"),
							column => column.Name("Precision")
						);
					});
				}));
			});

			Index.DeclareIfNotDeclared("IDX_" + tableName + "_" + foreignKeyColumnName + "_Key", IndexType.Unique, tableName,
				new string[] { foreignKeyColumnName, "Key" });
		}

		/// <summary>
		/// Maps a value property.
		/// </summary>
		/// <typeparam name="TEntity">The type of entity with the value property.</typeparam>
		/// <param name="mapping">The NHibernate mapping.</param>
		/// <param name="quantityProperty">The value property expression.</param>
		/// <param name="tableName">The name of the table to store the value elements.</param>
		/// <param name="foreignKeyColumnName">The name of the foreign key column in the value element table
		/// (see <paramref name="tableName"/>) referring to the object with the value property.</param>
		public static void WithValue<TEntity>(PropertyContainerCustomizer<TEntity> mapping, Expression<Func<TEntity, Value>> valueProperty,
			string tableName, string foreignKeyColumnName)
		{
			mapping = ArgumentValidation.AssertNotNull(mapping, nameof(mapping));
			mapping.Component(valueProperty, component =>
			{
				// Access as NoSetter to prevent a copy occurring on the setter because this causes NH to think that
				// the value has been changed on Flush and it re-saves the value elements, even though the copy of the
				// value is equal to copied value (i.e. == operator and .Equals() both return true).  NH must be
				// doing an Object.ReferenceEquals to see whether the object is dirty.
				component.Access(Accessor.NoSetter);
				component.Map<string, ValueElement>("_elements", map =>
				{
					map.Table(tableName);
					map.Key(key => key.Column(foreignKeyColumnName));
					map.Access(Accessor.Field);
					map.Lazy(CollectionLazy.NoLazy);
					// Subselect to prevent N+1 selects on value elements when lazy-loading collections of objects with a value (e.g. properties)
					map.Fetch(CollectionFetchMode.Subselect); 
					map.Cascade(Cascade.All);
				},
				key => key.Element(element => element.Formula("Key")),
				rel => rel.Component(valueElement =>
				{
					valueElement.Property(o => o.Key, property =>
					{
						property.Access(Accessor.NoSetter);
						property.NotNullable(true);
					});
					valueElement.Property(o => o.DataType, property =>
					{
						property.Type<ExtensibleEnumType<ValueDataType>>();
						property.Access(Accessor.NoSetter);
					});
					valueElement.Property("_stringValue", property =>
					{
						property.Access(Accessor.Field);
						property.Column("StringValue");
					});
					valueElement.Property("_decimalValue", property =>
					{
						property.Access(Accessor.Field);
						property.Column("DecimalValue");
					});
					valueElement.Property("_doubleValue", property =>
					{
						property.Access(Accessor.Field);
						property.Column("DoubleValue");
					});
					valueElement.Property("_booleanValue", property =>
					{
						property.Access(Accessor.Field);
						property.Column("BooleanValue");
					});
					valueElement.Property("_dateTimeValue", property =>
					{
						property.Access(Accessor.Field);
						property.Column("DateTimeValue");
					});
					valueElement.Property("_timeSpanValue", property =>
					{
						property.Access(Accessor.Field);
						property.Column("TimeSpanValue");
					});
					valueElement.Property("_unit", property =>
					{
						property.Type<UnitType>();
						property.Access(Accessor.Field);
						property.Column("Unit");
					});
					valueElement.Property("_cultureInfo", property =>
					{
						property.Access(Accessor.Field);
						property.Column("Language");
					});
					valueElement.Property("_uncertainty", property =>
					{
						property.Type<UncertaintyType>();
						property.Access(Accessor.Field);
						property.Columns(
							column => column.Name("StandardUncertainty"),
							column => column.Name("CoverageFactor"),
							column => column.Name("ConfidenceLevel"),
							column => column.Name("Accuracy"),
							column => column.Name("Precision")
						);
					});
				}));
			});

			Index.DeclareIfNotDeclared("IDX_" + tableName + "_" + foreignKeyColumnName + "_Key", IndexType.Unique, tableName,
				new string[] { foreignKeyColumnName, "Key" });
		}

		/// <summary>
		/// Maps the quantity-specific resource transaction properties for a resource transaction.
		/// </summary>
		/// <typeparam name="TResourceTransaction">The type of resource transaction.</typeparam>
		/// <param name="mapping">The NHibernate mapping.</param>
		public static void WithQuantitySpecificResourceTransactionProperties<TResourceTransaction>(PropertyContainerCustomizer<TResourceTransaction> mapping)
			where TResourceTransaction : class, IResourceTransaction, IWithExternalId, 
				IWithResourceTransactionProperties<TResourceTransaction, QuantitySpecificResourceTransactionProperty<TResourceTransaction>>
		{
			mapping = ArgumentValidation.AssertNotNull(mapping, nameof(mapping));
			mapping.Component<WithResourceTransactionPropertiesMixIn<TResourceTransaction,
				QuantitySpecificResourceTransactionProperty<TResourceTransaction>>>("WithPropertiesMixIn", component =>
			{
				component.Access(Accessor.NoSetter);
				component.Parent("ObjectWithProperties", parent => parent.Access(Accessor.NoSetter));
				component.Property("AllowDuplicatePropertyIds", property =>
				{
					property.Column("AllowDuplicatePropertyIds");
					property.Access(Accessor.NoSetter);
				});
				component.Set(o => o.AllProperties, set =>
				{
					set.Key(k => k.Column("ParentObjectId"));
					set.Inverse(true);
					set.Access(Accessor.NoSetter);
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					set.BatchSize(100);
					set.Cascade(Cascade.All | Cascade.DeleteOrphans);
				}, rel => rel.OneToMany());
			});
		}

		/// <summary>
		/// Maps the basic properties of an object.
		/// </summary>
		/// <typeparam name="TOfObject">The type of object with basic properties.</typeparam>
		/// <param name="mapping">The NHibernate mapping.</param>
		public static void WithBasicProperties<TOfObject>(PropertyContainerCustomizer<TOfObject> mapping)
			where TOfObject : class, IWithBasicProperties<TOfObject>, IWithExternalId
		{
			mapping = ArgumentValidation.AssertNotNull(mapping, nameof(mapping));
			mapping.Component<WithBasicPropertiesMixIn<TOfObject>>("_withPropertiesMixIn", component =>
			{
				component.Access(Accessor.Field);
				component.Parent("ObjectWithProperties", parent => parent.Access(Accessor.NoSetter));
				component.Property("AllowDuplicatePropertyIds", property => property.Access(Accessor.NoSetter));
				component.Set(o => o.AllProperties, set =>
				{
					set.Key(k => k.Column("ParentObjectId"));
					set.Inverse(true);
					set.Fetch(CollectionFetchMode.Select);
					set.BatchSize(100);
					set.Cascade(Cascade.All | Cascade.DeleteOrphans);
					set.Access(Accessor.NoSetter);
				}, rel => rel.OneToMany());
			});
		}

		/// <summary>
		/// Maps an object as an assembly.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the assembly.</typeparam>
		/// <param name="mapping">The NHibernate mapping.</param>
		public static void AsAssembly<TElement>(PropertyContainerCustomizer<TElement> mapping)
			where TElement : class, IAssembly<TElement>, IWithAssemblyMixIn<TElement>
		{
			mapping = ArgumentValidation.AssertNotNull(mapping, nameof(mapping));
			mapping.Component<AssemblyMixIn<TElement>>("AssemblyMixIn", component =>
			{
				component.Lazy(false);
				component.Access(Accessor.NoSetter);
				component.Parent("Assembly", parent => parent.Access(Accessor.NoSetter));
				component.Property(o => o.AssemblyType, property => property.Type<ExtensibleEnumType<AssemblyType>>());
				component.Property(o => o.AssemblyRelationship, property => property.Type<ExtensibleEnumType<AssemblyRelationship>>());
				
				component.Set(o => o.AssemblyElements, set =>
				{
					set.Table(typeof(TElement).GetNonGenericTypeName() + "AssemblyElement");
					set.Key(k => k.Column("AssemblyId"));
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
					// NH will inspect the entire hierarchy on each cascaded operation.
					set.Cascade(Cascade.None);
					set.Access(Accessor.NoSetter);
				}, rel => rel.ManyToMany(map => { map.Column("ElementId"); }));
				
				component.Set(o => o.ParentAssemblies, set =>
				{
					set.Table(typeof(TElement).GetNonGenericTypeName() + "AssemblyElement");
					set.Inverse(true);
					set.Key(k => k.Column("ElementId"));
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
					// NH will inspect the entire hierarchy on each cascaded operation.
					set.Cascade(Cascade.None);
					set.Access(Accessor.NoSetter);
				}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("AssemblyId"); }));

				// Defined as a bag so that the collection isn't initialised upon adding to the collection
				component.Bag<TElement>("_assemblyDescendants", bag => {
					bag.Table(typeof(TElement).GetNonGenericTypeName() + "AssemblyDescendant");
					bag.Key(k => k.Column("AscendantId"));
					bag.Lazy(CollectionLazy.Extra);
					bag.Fetch(CollectionFetchMode.Select);
					// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
					// NH will inspect the entire hierarchy on each cascaded operation.
					bag.Cascade(Cascade.None);
					bag.Access(Accessor.Field);
				}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("DescendantId")));

				// Defined as a bag so that the collection isn't initialised upon adding to the collection
				component.Bag<TElement>("_assemblyAscendants", bag => {
					bag.Table(typeof(TElement).GetNonGenericTypeName() + "AssemblyDescendant");
					bag.Key(k => k.Column("DescendantId"));
					bag.Inverse(true);
					bag.Lazy(CollectionLazy.Extra);
					bag.Fetch(CollectionFetchMode.Select);
					// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
					// NH will inspect the entire hierarchy on each cascaded operation.
					bag.Cascade(Cascade.None);
					bag.Access(Accessor.Field);
				}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));
			});

			Index.Declare("IDX_" + typeof(TElement).GetNonGenericTypeName() + "AssemblyElement_AssemblyId_ElementId", IndexType.Unique,
				typeof(TElement).GetNonGenericTypeName() + "AssemblyElement", new[] { "AssemblyId", "ElementId" });
			Index.Declare("IDX_" + typeof(TElement).GetNonGenericTypeName() + "AssemblyElement_ElementId_AssemblyId", IndexType.Unique,
				typeof(TElement).GetNonGenericTypeName() + "AssemblyElement", new[] { "ElementId", "AssemblyId" });
			Index.Declare("IDX_" + typeof(TElement).GetNonGenericTypeName() + "AssemblyDescendant_AscendantId_DescendantId", IndexType.Standard,
				typeof(TElement).GetNonGenericTypeName() + "AssemblyDescendant", new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_" + typeof(TElement).GetNonGenericTypeName() + "AssemblyDescendant_DescendantId_AscendantId", IndexType.Standard,
				typeof(TElement).GetNonGenericTypeName() + "AssemblyDescendant", new[] { "DescendantId", "AscendantId" });
		}

		/// <summary>
		/// Maps an object as a process definition.
		/// </summary>
		/// <typeparam name="TProcessDefinition">The type of proces definition.</typeparam>
		/// <param name="mapping">The NHibernate mapping.</param>
		public static void AsProcessDefinition<TProcessDefinition>(PropertyContainerCustomizer<TProcessDefinition> mapping)
			where TProcessDefinition : HierarchyScopedObject, IProcessDefinition<TProcessDefinition>, IWithProcessDefinitionMixIn<TProcessDefinition>
		{
			mapping = ArgumentValidation.AssertNotNull(mapping, nameof(mapping));
			mapping.Component<ProcessDefinitionMixIn<TProcessDefinition>>("ProcessDefinitionMixIn", component =>
			{
				component.Access(Accessor.NoSetter);
				component.Parent("ProcessDefinition", parent => parent.Access(Accessor.NoSetter));
				component.Property(o => o.ProcessType, property => property.Type<ExtensibleEnumType<ProcessType>>());
				component.Property(o => o.Duration);
				component.Property(o => o.PublishedDate, property => property.Access(Accessor.NoSetter));
				component.Property(o => o.Version);

				component.ManyToOne(o => o.Parent, manyToOne =>
				{
					manyToOne.Column("ParentProcessDefinitionId");
					manyToOne.Access(Accessor.NoSetter);
					manyToOne.Fetch(FetchKind.Select);
					manyToOne.Cascade(Cascade.None);
					manyToOne.NotNullable(false);
				});

				component.ManyToOne(o => o.Root, manyToOne =>
				{
					manyToOne.Column("RootProcessDefinitionId");
					manyToOne.Access(Accessor.NoSetter);
					manyToOne.Fetch(FetchKind.Select);
					manyToOne.Cascade(Cascade.None);
					manyToOne.NotNullable(false);
				});

				// Defined as a bag so that the collection isn't initialised upon adding to the collection
				component.Bag(o => o.Descendants, bag => 
				{
					bag.Table(typeof(TProcessDefinition).GetNonGenericTypeName() + "Descendant");
					bag.Key(k => k.Column("AscendantId"));
					bag.Lazy(CollectionLazy.Extra);
					bag.Fetch(CollectionFetchMode.Select);
					// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
					// NH will inspect the entire hierarchy on each cascaded operation.
					bag.Cascade(Cascade.None);
					bag.Access(Accessor.NoSetter);
				}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("DescendantId")));

				// Defined as a bag so that the collection isn't initialised upon adding to the collection
				component.Bag(o => o.Ascendants, bag => 
				{
					bag.Table(typeof(TProcessDefinition).GetNonGenericTypeName() + "Descendant");
					bag.Key(k => k.Column("DescendantId"));
					bag.Inverse(true);
					bag.Lazy(CollectionLazy.Extra);
					bag.Fetch(CollectionFetchMode.Select);
					// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
					// NH will inspect the entire hierarchy on each cascaded operation.
					bag.Cascade(Cascade.None);
					bag.Access(Accessor.NoSetter);
				}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));

				// Defined as a bag so that the collection isn't initialised upon adding to the collection
				component.Bag(o => o.AllMaterialSpecifications, bag =>
				{
					bag.Key(k => k.Column("ResourceSpecificationProcessDefinitionId"));
					bag.Lazy(CollectionLazy.Extra);
					bag.Fetch(CollectionFetchMode.Select);
					// No need for cascade because resource specifications will be explicitly saved separately from the process definition
					bag.Cascade(Cascade.None);
					bag.Access(Accessor.Field); // Because the getter is a LINQ Distinct() operation
				}, rel => rel.OneToMany());

				component.Set(o => o.EquipmentSpecifications, set => 
				{
					set.Key(k => k.Column("ResourceSpecificationProcessDefinitionId"));
					set.Inverse(true);
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					// No need for cascade because resource specifications will be explicitly saved separately from the process definition
					set.Cascade(Cascade.None);
					set.Access(Accessor.NoSetter);
				}, r => r.OneToMany());

				component.Set(o => o.PhysicalAssetSpecifications, set => 
				{
					set.Key(k => k.Column("ResourceSpecificationProcessDefinitionId"));
					set.Inverse(true);
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					// No need for cascade because resource specifications will be explicitly saved separately from the process definition
					set.Cascade(Cascade.None);
					set.Access(Accessor.NoSetter);
				}, r => r.OneToMany());

				component.Set(o => o.PersonnelSpecifications, set => 
				{
					set.Key(k => k.Column("ResourceSpecificationProcessDefinitionId"));
					set.Inverse(true);
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					// No need for cascade because resource specifications will be explicitly saved separately from the process definition
					set.Cascade(Cascade.None);
					set.Access(Accessor.NoSetter);
				}, r => r.OneToMany());
			});

			Index.DeclareIfNotDeclared("IDX_DesignObject_ParentProcessDefinitionId", IndexType.Standard, "DesignObject", new[] { "ParentProcessDefinitionId" });
			Index.DeclareIfNotDeclared("IDX_DesignObject_RootProcessDefinitionId", IndexType.Standard, "DesignObject", new[] { "RootProcessDefinitionId" });
			Index.Declare("IDX_" + typeof(TProcessDefinition).GetNonGenericTypeName() + "Descendant_AscendantId_DescendantId", IndexType.Unique,
				typeof(TProcessDefinition).GetNonGenericTypeName() + "Descendant", new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_" + typeof(TProcessDefinition).GetNonGenericTypeName() + "Descendant_DescendantId_AscendantId", IndexType.Unique,
				typeof(TProcessDefinition).GetNonGenericTypeName() + "Descendant", new[] { "DescendantId", "AscendantId" });
		}
	}
}
