﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class PageMapping : SubclassMapping<Page>
	{
		public PageMapping()
		{
			Extends(typeof(DefinitionNode));
			DiscriminatorValue(NodeMetaType.Page);

			Set(o => o.Occurrences, set =>
			{
				set.Key(k => k.Column("ModelPageNodeId"));
				set.Access(Accessor.NoSetter);
				set.Fetch(CollectionFetchMode.Select);
				set.Cascade(Cascade.All);
				set.Inverse(true);
			}, rel => rel.OneToMany());
		}
	}
}
