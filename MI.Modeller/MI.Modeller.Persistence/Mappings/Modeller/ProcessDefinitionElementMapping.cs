﻿using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class ProcessDefinitionElementMapping : SubclassMapping<ProcessDefinitionElement>
	{
		public ProcessDefinitionElementMapping()
		{
			Extends(typeof(ModelElement));
			DiscriminatorValue(NodeMetaType.ProcessDefinitionElement);
		}
	}
}
