﻿using System;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public static class NodeMetaType
	{
		public static Guid ModelElement { get; } = new Guid("4b516a6b-41ae-445f-8a19-9688d84856fd");
		public static Guid ProcessDefinitionElement { get; } = new Guid("1C92B439-972F-4CA8-ACB6-08496E5716C8");
		public static Guid ResourceDefinitionElement { get; } = new Guid("4DFE7668-C918-4793-AA40-5709AE2B1945");
		public static Guid SpecificationDefinitionElement { get; } = new Guid("9F484FCF-5F6C-4A94-94AA-784292F4E714");
		public static Guid Model { get; } = new Guid("2a42a41a-8646-4910-8dda-486defa3962e");
		public static Guid Page { get; } = new Guid("8e3411fe-011f-4d9c-a665-4fd1a7335e26");
	}
}
