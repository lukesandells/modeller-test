﻿using MI.Framework.Persistence.NHibernate;
using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class ObjectOccurrenceMapping : SubclassMapping<ObjectOccurrence>
	{
		public ObjectOccurrenceMapping()
		{
			Extends(typeof(Occurrence));
			DiscriminatorValue(OccurrenceType.ObjectOccurrence);

			ManyToOne(o => o.Parent, manyToOne =>
			{
				manyToOne.Column("ParentOccurrenceId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Set(o => o.InboundConnectors, set =>
			{
				set.Key(k => k.Column("ToObjectOccurrenceId"));
				set.Access(Accessor.NoSetter);
				set.Fetch(CollectionFetchMode.Select);
				set.Cascade(Cascade.All | Cascade.DeleteOrphans);
				set.Inverse(true);
			}, rel => rel.OneToMany());

			Set(o => o.OutboundConnectors, set =>
			{
				set.Key(k => k.Column("FromObjectOccurrenceId"));
				set.Access(Accessor.NoSetter);
				set.Fetch(CollectionFetchMode.Select);
				set.Cascade(Cascade.All | Cascade.DeleteOrphans);
				set.Inverse(true);
			}, rel => rel.OneToMany());

			Index.Declare("IDX_Occurrence_ParentOccurrenceId", IndexType.Standard, nameof(Occurrence), new string[] { "ParentOccurrenceId" });
		}
	}
}
