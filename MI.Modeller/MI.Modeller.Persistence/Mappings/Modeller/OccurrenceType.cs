﻿namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public enum OccurrenceType
	{
		ObjectOccurrence = 1,
		ConnectorOccurrence = 2
	}
}
