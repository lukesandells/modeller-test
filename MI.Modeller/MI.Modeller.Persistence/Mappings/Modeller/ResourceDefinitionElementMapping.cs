﻿using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class ResourceDefinitionElementMapping : SubclassMapping<ResourceDefinitionElement>
	{
		public ResourceDefinitionElementMapping()
		{
			Extends(typeof(ModelElement));
			DiscriminatorValue(NodeMetaType.ResourceDefinitionElement);
		}
	}
}
