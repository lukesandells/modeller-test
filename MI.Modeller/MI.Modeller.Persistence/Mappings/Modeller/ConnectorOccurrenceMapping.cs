﻿using MI.Framework.Persistence.NHibernate;
using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class ConnectorOccurrenceMapping : SubclassMapping<ConnectorOccurrence>
	{
		public ConnectorOccurrenceMapping()
		{
			Extends(typeof(Occurrence));
			DiscriminatorValue(OccurrenceType.ConnectorOccurrence);

			ManyToOne(o => o.From, manyToOne =>
			{
				manyToOne.Column("FromObjectOccurrenceId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.To, manyToOne =>
			{
				manyToOne.Column("ToObjectOccurrenceId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Index.Declare("IDX_Occurrence_FromObjectOccurrenceId_ToObjectOccurrenceId", IndexType.Standard, nameof(Occurrence),
				new string[] { "FromObjectOccurrenceId", "ToObjectOccurrenceId" });
			Index.Declare("IDX_Occurrence_FromObjectOccurrenceId", IndexType.Standard, nameof(Occurrence), new string[] { "FromObjectOccurrenceId" });
			Index.Declare("IDX_Occurrence_ToObjectOccurrenceId", IndexType.Standard, nameof(Occurrence), new string[] { "ToObjectOccurrenceId" });
		}
	}
}
