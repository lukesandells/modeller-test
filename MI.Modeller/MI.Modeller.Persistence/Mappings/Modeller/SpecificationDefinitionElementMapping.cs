﻿using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class SpecificationDefinitionElementMapping : SubclassMapping<SpecificationDefinitionElement>
	{
		public SpecificationDefinitionElementMapping()
		{
			Extends(typeof(ModelElement));
			DiscriminatorValue(NodeMetaType.SpecificationDefinitionElement);
		}
	}
}
