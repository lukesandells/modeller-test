﻿using MI.Framework.Persistence.NHibernate;
using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class OccurrenceMapping : ClassMapping<Occurrence>
	{
		public OccurrenceMapping()
		{
			Abstract(true);
			Id(o => o.Id, id => id.Access(Accessor.NoSetter));

			Discriminator(discriminator => 
			{
				discriminator.Column("OccurrenceTypeId");
				discriminator.Type<NHibernate.Type.EnumType<OccurrenceType>>();
				discriminator.NotNullable(true);
			});

			DiscriminatorValue(null);

			ManyToOne(o => o.Page, manyToOne =>
			{
				manyToOne.Column("ModelPageNodeId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.ModelElement, manyToOne =>
			{
				manyToOne.Column("ModelElementNodeId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Index.Declare("IDX_Occurrence_ModelPageNodeId_ModelElementNodeId", IndexType.Standard, nameof(Occurrence),
				new string[] { "ModelPageNodeId", "ModelElementNodeId" });
			Index.Declare("IDX_Occurrence_ModelElementNodeId_ModelPageNodeId", IndexType.Standard, nameof(Occurrence),
				new string[] { "ModelElementNodeId", "ModelPageNodeId" });
		}
	}
}
