﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class ModelMapping : SubclassMapping<Model>
	{
		public ModelMapping()
		{
			DiscriminatorValue(NodeMetaType.Model);
		}
	}
}
