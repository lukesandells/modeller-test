﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence.NHibernate;
using MI.Modeller.Domain;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.Modeller
{
	public class ModelElementMapping : SubclassMapping<ModelElement>
	{
		public ModelElementMapping()
		{
			Extends(typeof(DefinitionNode));
			DiscriminatorValue(NodeMetaType.ModelElement);

			ManyToOne(o => o.DesignObject, manyToOne =>
			{
				manyToOne.Column("DesignObjectId");
				manyToOne.Access(Accessor.Property);
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.All | Cascade.DeleteOrphans | Cascade.ReAttach);
				manyToOne.NotNullable(false);
			});

			Set(o => o.Occurrences, set =>
			{
				set.Key(k => k.Column("ModelElementNodeId"));
				set.Access(Accessor.NoSetter);
				set.Fetch(CollectionFetchMode.Select);
				set.Cascade(Cascade.All | Cascade.DeleteOrphans | Cascade.ReAttach);
				set.Inverse(true);
			}, rel => rel.OneToMany());

			Index.Declare("IDX_BrowserNode_DesignObjectId", IndexType.Unique, nameof(BrowserNode), new string[] { "DesignObjectId" });
		}
	}
}
