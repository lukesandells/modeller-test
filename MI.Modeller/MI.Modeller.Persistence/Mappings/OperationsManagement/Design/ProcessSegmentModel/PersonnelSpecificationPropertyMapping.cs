﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class PersonnelSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<PersonnelSpecification<ProcessSegment>>
	{
		public PersonnelSpecificationPropertyMapping() : base(PropertyType.ProcessSegmentPersonnelSpecificationProperty)
		{
		}
	}
}
