﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class MaterialSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<MaterialSpecification<ProcessSegment>>
	{
		public MaterialSpecificationPropertyMapping() : base(PropertyType.ProcessSegmentMaterialSpecificationProperty)
		{
		}
	}
}
