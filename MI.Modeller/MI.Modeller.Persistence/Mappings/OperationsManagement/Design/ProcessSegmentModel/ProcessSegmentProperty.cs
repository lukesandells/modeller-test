﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class ProcessSegmentPropertyMapping : BasicPropertyMapping<ProcessSegment>
	{
		public ProcessSegmentPropertyMapping() : base(PropertyType.ProcessSegmentProperty)
		{
		}
	}
}
