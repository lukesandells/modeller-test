﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class PhysicalAssetSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<PhysicalAssetSpecification<ProcessSegment>>
	{
		public PhysicalAssetSpecificationPropertyMapping() : base(PropertyType.ProcessSegmentPhysicalAssetSpecificationProperty)
		{
		}
	}
}
