﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class ProcessSegmentMapping : SubclassMapping<ProcessSegment>
	{
		public ProcessSegmentMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.ProcessSegment);

			Mappings.Map.WithBasicProperties(this);
			Mappings.Map.AsProcessDefinition(this);
		}
	}
}
