﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class PersonnelSpecificationMapping : PersonnelSpecificationMapping<ProcessSegment>
	{
		public PersonnelSpecificationMapping() : base(DesignObjectType.ProcessSegmentPersonnelSpecification)
		{
		}
	}
}
