﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class MaterialSpecificationMapping : MaterialSpecificationMapping<ProcessSegment>
	{
		public MaterialSpecificationMapping() : base(DesignObjectType.ProcessSegmentMaterialSpecification)
		{
		}
	}
}
