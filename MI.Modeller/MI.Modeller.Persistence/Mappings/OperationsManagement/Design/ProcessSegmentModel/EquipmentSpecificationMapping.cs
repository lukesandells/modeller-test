﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class EquipmentSpecificationMapping : EquipmentSpecificationMapping<ProcessSegment>
	{
		public EquipmentSpecificationMapping() : base(DesignObjectType.ProcessSegmentEquipmentSpecification)
		{
		}
	}
}
