﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.ProcessSegmentModel
{
	public class PhysicalAssetSpecificationMapping : PhysicalAssetSpecificationMapping<ProcessSegment>
	{
		public PhysicalAssetSpecificationMapping() : base(DesignObjectType.ProcessSegmentPhysicalAssetSpecification)
		{
		}
	}
}
