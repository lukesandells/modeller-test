﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design
{
	public abstract class EquipmentSpecificationMapping<TOfObject> : SubclassMapping<EquipmentSpecification<TOfObject>>
		where TOfObject : class, IProcessDefinition<TOfObject>
	{
		protected EquipmentSpecificationMapping(DesignObjectType designObjectType)
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(designObjectType);

			Property(o => o.EquipmentUse);

			Mappings.Map.WithQuantity(this, o => o.Quantity, "DesignObjectQuantityElement", "ParentObjectId");
			Mappings.Map.WithQuantitySpecificResourceTransactionProperties(this);

			ManyToOne(o => o.ProcessDefinition, manyToOne =>
			{
				manyToOne.Column("ResourceSpecificationProcessDefinitionId");
				manyToOne.Access(Accessor.Property);
				// Fetch by select because it's a rare edge case that a resource specification be loaded before its owning process definition
				manyToOne.Fetch(FetchKind.Select);
				// No cascade because process definition will be saved/updated independently
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.Equipment, manyToOne =>
			{
				manyToOne.Column("EquipmentId");
				manyToOne.Access(Accessor.Property);
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.EquipmentClass, manyToOne =>
			{
				manyToOne.Column("EquipmentClassId");
				manyToOne.Access(Accessor.Property);
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Index.DeclareIfNotDeclared("IDX_DesignObject_ResourceSpecificationProcessDefinitionId", IndexType.Standard, "DesignObject",
				new[] { "ResourceSpecificationProcessDefinitionId" });
			Index.DeclareIfNotDeclared("IDX_DesignObject_EquipmentId", IndexType.Standard, "DesignObject", new[] { "EquipmentId" });
			Index.DeclareIfNotDeclared("IDX_DesignObject_EquipmentClassId", IndexType.Standard, "DesignObject", new[] { "EquipmentClassId" });
		}
	}
}
