﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.MaterialModel
{
	/// <summary>
	/// Mapping for the Material Class Properties
	/// </summary>
	public class MaterialClassPropertyMapping : BasicPropertyMapping<MaterialClass>
	{
		/// <summary>
		/// Initializes a new MaterialPropertyMapping
		/// </summary>
		public MaterialClassPropertyMapping() : base(PropertyType.MaterialClassProperty)
		{
		}
	}
}
