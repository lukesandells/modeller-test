﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.MaterialModel
{
	/// <summary>
	/// Mapping for the Material Definition Properties
	/// </summary>
	public class MaterialDefinitionPropertyMapping : BasicPropertyMapping<MaterialDefinition>
	{
		/// <summary>
		/// Initializes a new MaterialPropertyMapping
		/// </summary>
		public MaterialDefinitionPropertyMapping() : base(PropertyType.MaterialDefinitionProperty)
		{
		}
	}
}
