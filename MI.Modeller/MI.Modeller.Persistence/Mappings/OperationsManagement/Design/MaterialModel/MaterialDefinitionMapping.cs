﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.MaterialModel
{
	public class MaterialDefinitionMapping : SubclassMapping<MaterialDefinition>
	{
		public MaterialDefinitionMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.MaterialDefinition);

			Mappings.Map.AsAssembly(this);
			Mappings.Map.WithBasicProperties(this);

			Set(o => o.MaterialClasses, set =>
			{
				set.Table("MaterialClassMember");
				set.Key(k => k.Column("MaterialDefinitionId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("MaterialClassId"); }));
			
			Index.Declare("IDX_MaterialClassMember_MaterialDefinitionId_MaterialClassId", IndexType.Standard, "MaterialClassMember",
				new[] { "MaterialDefinitionId", "MaterialClassId" });
		}
	}
}
