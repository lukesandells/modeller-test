﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design
{
	public class DesignObjectMapping : ClassMapping<DesignObject>
	{
		public DesignObjectMapping()
		{
			Abstract(true);
			Id(o => o.InternalId, id =>
			{
				id.Access(Accessor.Property);
				id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 100 }));
			});

			Discriminator(discriminator => 
			{
				discriminator.Column("DesignObjectTypeId");
				discriminator.Force(true);
				discriminator.Type<NHibernate.Type.EnumType<DesignObjectType>>();
				discriminator.NotNullable(true);
			});

			DiscriminatorValue(null);

			Property(o => o.ExternalId, property => { property.NotNullable(true); property.Access(Accessor.Property); });
			Property(o => o.Description, property => { property.NotNullable(true); property.Access(Accessor.Property); });

			Index.Declare("IDX_DesignObject_ExternalId_DesignObjectTypeId", IndexType.Standard, "DesignObject",
				new string[] { "ExternalId", "DesignObjectTypeId" });
		}
	}
}
