﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.Resources
{
	/// <summary>
	/// Mapping for persisting PhysicalAssetClass objects
	/// </summary>
	public class PhysicalAssetClassMapping : SubclassMapping<PhysicalAssetClass>
	{
		/// <summary>
		/// Initializes a new PhysicalAssetClassMapping
		/// </summary>
		public PhysicalAssetClassMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.PhysicalAssetClass);

			Mappings.Map.WithBasicProperties(this);

			Set(o => o.Manufacturers,
				set =>
				{
					set.Table("PhysicalAssetManufacturer");
					set.Key(k => k.Column("PhysicalAssetClassId"));
					set.Fetch(CollectionFetchMode.Select);
					set.Cascade(Cascade.All);
					set.Access(Accessor.NoSetter);
				}, rel => rel.Element(element => { element.Column("Manufacturer"); }));

			Set(o => o.Subclasses, set =>
			{
				set.Table("PhysicalAssetClassSubclass");
				set.Key(k => k.Column("SuperclassId"));
				// Collection managed by Parents collection because fewer members => more performant
				set.Inverse(true);
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(map => { map.Column("SubclassId"); }));

			Set(o => o.SuperClasses, set =>
			{
				set.Table("PhysicalAssetClassSubclass");
				set.Key(k => k.Column("SubclassId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("SuperclassId"); }));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Descendants, bag => {
				bag.Table("PhysicalAssetClassDescendant");
				bag.Key(k => k.Column("AscendantId"));
				bag.Inverse(true);
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("DescendantId")));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Ascendants, bag => {
				bag.Table("PhysicalAssetClassDescendant");
				bag.Key(k => k.Column("DescendantId"));
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));

			Index.Declare("IDX_PhysicalAssetClassSubclass_SuperclassId_SubclassId", IndexType.Standard, "PhysicalAssetClassSubclass",
				new[] { "SuperclassId", "SubclassId" });
			Index.Declare("IDX_PhysicalAssetClassSubclass_SubclassId_SuperclassId", IndexType.Standard, "PhysicalAssetClassSubclass",
				new[] { "SubclassId", "SuperclassId" });
			Index.Declare("IDX_PhysicalAssetClassDescendant_AscendantId_DescendantId", IndexType.Standard, "PhysicalAssetClassDescendant",
				new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_PhysicalAssetClassDescendant_DescendantId_AscendantId", IndexType.Standard, "PhysicalAssetClassDescendant",
				new[] { "DescendantId", "AscendantId" });
		}
	}
}
