﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.Resources
{
	/// <summary>
	/// Mapping for persisting PersonnelClass objects
	/// </summary>
	public class PersonnelClassMapping : SubclassMapping<PersonnelClass>
	{
		/// <summary>
		/// Initializes a new PersonnelClassMapping
		/// </summary>
		public PersonnelClassMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.PersonnelClass);

			Mappings.Map.WithBasicProperties(this);

			Set(o => o.Subclasses, set =>
			{
				set.Table("PersonnelClassSubclass");
				set.Key(k => k.Column("SuperclassId"));
				// Collection managed by Parents collection because fewer members => more performant
				set.Inverse(true);
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(map => { map.Column("SubclassId"); }));

			Set(o => o.SuperClasses, set =>
			{
				set.Table("PersonnelClassSubclass");
				set.Key(k => k.Column("SubclassId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("SuperclassId"); }));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Descendants, bag => {
				bag.Table("PersonnelClassDescendant");
				bag.Key(k => k.Column("AscendantId"));
				bag.Inverse(true);
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("DescendantId")));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Ascendants, bag => {
				bag.Table("PersonnelClassDescendant");
				bag.Key(k => k.Column("DescendantId"));
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));

			Index.Declare("IDX_PersonnelClassSubclass_SuperclassId_SubclassId", IndexType.Standard, "PersonnelClassSubclass",
				new[] { "SuperclassId", "SubclassId" });
			Index.Declare("IDX_PersonnelClassSubclass_SubclassId_SuperclassId", IndexType.Standard, "PersonnelClassSubclass",
				new[] { "SubclassId", "SuperclassId" });
			Index.Declare("IDX_PersonnelClassDescendant_AscendantId_DescendantId", IndexType.Standard, "PersonnelClassDescendant",
				new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_PersonnelClassDescendant_DescendantId_AscendantId", IndexType.Standard, "PersonnelClassDescendant",
				new[] { "DescendantId", "AscendantId" });
		}
	}
}
