﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.Resources
{
	/// <summary>
	/// Mapping for the PhysicalAsset Class Properties
	/// </summary>
	public class PhysicalAssetPropertyMapping : BasicPropertyMapping<PhysicalAssetClass>
	{
		/// <summary>
		/// Initializes a new PhysicalAssetPropertyMapping
		/// </summary>
		public PhysicalAssetPropertyMapping() : base(PropertyType.PhysicalAssetClassProperty)
		{
		}
	}
}
