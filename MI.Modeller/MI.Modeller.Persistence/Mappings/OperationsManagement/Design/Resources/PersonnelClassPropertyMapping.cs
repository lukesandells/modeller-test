﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.Resources
{
	/// <summary>
	/// Mapping for the Personnel Class Properties
	/// </summary>
	public class PersonnelPropertyMapping : BasicPropertyMapping<PersonnelClass>
	{
		/// <summary>
		/// Initializes a new PersonnelPropertyMapping
		/// </summary>
		public PersonnelPropertyMapping() : base(PropertyType.PersonnelClassProperty)
		{
		}
	}
}
