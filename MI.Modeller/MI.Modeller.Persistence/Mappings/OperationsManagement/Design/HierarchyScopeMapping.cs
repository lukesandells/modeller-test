﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design
{
	public class HierarchyScopeMapping : SubclassMapping<HierarchyScope>
	{
		public HierarchyScopeMapping()
		{
			Extends(typeof(DesignObject));
			DiscriminatorValue(DesignObjectType.HierarchyScope);

			// Mapped as a many-to-one rather than a one-to-one, even though one-to-one is the natural type of relationship because
			// we can't link the objects by primary key (because they're in the same table, so the two objects can't have the same
			// primary key value) and we can't use a property-ref because multiple equipment objects may have the same hierarchy scope ID.
			// So the only way to make this work is for a hierarchy scope to have a DefiningEquipmentId field that references the 
			// equipment record that defines the hierarchy scope.  
			//
			// Note: This results in an extra UPDATE being run on creation of a hierarchy scope because there is a foreign key
			// constraint cycle between hierarchy scope and equipment.  Have to populate the foreign key in one direction with NULL
			// on the original INSERT, and then set to the correct ID after the other object has been inserted.
			ManyToOne(o => o.DefiningEquipment, manyToOne => {
				manyToOne.Column("DefiningEquipmentId");
				manyToOne.Access(Accessor.NoSetter);
				// Eager loading this because key properties of a hierarchy scope are drawn from its defining equipment.
				manyToOne.Lazy(LazyRelation.NoLazy);
				manyToOne.Fetch(FetchKind.Join);
				manyToOne.Cascade(Cascade.Persist | Cascade.DeleteOrphans | Cascade.ReAttach);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.Parent, manyToOne => 
			{
				manyToOne.Column("ParentHierarchyScopeId");
				manyToOne.Access(Accessor.NoSetter);
				manyToOne.Fetch(FetchKind.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Set(o => o.Children, set => {
				set.Key(k => k.Column("ParentHierarchyScopeId"));
				set.Inverse(true);
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.OneToMany());

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Descendants, bag => {
				bag.Table("HierarchyScopeDescendant");
				bag.Key(k => k.Column("AscendantId"));
				bag.Inverse(true);
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.Field);
			}, rel => rel.ManyToMany(map => map.Column("DescendantId")));
			
			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Ascendants, bag => {
				bag.Table("HierarchyScopeDescendant");
				bag.Key(k => k.Column("DescendantId"));
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.Field);
			}, rel => rel.ManyToMany(map => map.Column("AscendantId")));
			
			Index.Declare("IDX_DesignObject_DefiningEquipmentId", IndexType.Standard, "DesignObject", new[] { "DefiningEquipmentId" });
			Index.Declare("IDX_HierarchyScopeDescendant_AscendantId_DescendantId", IndexType.Unique, "HierarchyScopeDescendant", 
				new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_HierarchyScopeDescendant_DescendantId_AscendantId", IndexType.Unique, "HierarchyScopeDescendant",
				new[] { "DescendantId", "AscendantId" });
		}
	}
}
