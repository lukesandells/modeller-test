﻿using MI.Framework.Persistence.NHibernate;
using MI.Framework.Persistence.NHibernate.Types;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.EquipmentModel
{
	public class EquipmentClassMapping : SubclassMapping<EquipmentClass>
	{
		public EquipmentClassMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.EquipmentClass);

			Property(o => o.EquipmentLevel, property => property.Type<ExtensibleEnumType<EquipmentLevel>>());
			Mappings.Map.WithBasicProperties(this);

			Set(o => o.Subclasses, set =>
			{
				set.Table("EquipmentClassSubclass");
				set.Key(k => k.Column("SuperclassId"));
				// Collection managed by Parents collection because fewer members => more performant
				set.Inverse(true);
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(map => { map.Column("SubclassId"); }));

			Set(o => o.SuperClasses, set =>
			{
				set.Table("EquipmentClassSubclass");
				set.Key(k => k.Column("SubclassId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("SuperclassId"); }));

			Set(o => o.Members, set =>
			{
				set.Table("EquipmentClassMember");
				set.Inverse(true);
				set.Key(k => k.Column("EquipmentClassId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("EquipmentId"); }));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Descendants, bag => {
				bag.Table("EquipmentClassDescendant");
				bag.Key(k => k.Column("AscendantId"));
				bag.Inverse(true);
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("DescendantId")));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Ascendants, bag => {
				bag.Table("EquipmentClassDescendant");
				bag.Key(k => k.Column("DescendantId"));
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));

			Index.Declare("IDX_EquipmentClassSubclass_SuperclassId_SubclassId", IndexType.Unique, "EquipmentClassSubclass", 
				new[] { "SuperclassId", "SubclassId" });
			Index.Declare("IDX_EquipmentClassSubclass_SubclassId_SuperclassId", IndexType.Unique, "EquipmentClassSubclass",
				new[] { "SubclassId", "SuperclassId" });
			Index.Declare("IDX_EquipmentClassDescendant_AscendantId_DescendantId", IndexType.Standard, "EquipmentClassDescendant",
				new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_EquipmentClassDescendant_DescendantId_AscendantId", IndexType.Standard, "EquipmentClassDescendant",
				new[] { "DescendantId", "AscendantId" });
		}
	}
}
