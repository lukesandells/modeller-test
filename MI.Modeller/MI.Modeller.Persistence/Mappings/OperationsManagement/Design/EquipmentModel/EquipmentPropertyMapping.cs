﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.EquipmentModel
{
	public class EquipmentPropertyMapping : BasicPropertyMapping<Equipment>
	{
		public EquipmentPropertyMapping() : base(PropertyType.EquipmentProperty)
		{
		}
	}
}
