﻿using MI.Framework.Persistence.NHibernate;
using MI.Framework.Persistence.NHibernate.Types;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.EquipmentModel
{
	public class EquipmentMapping : SubclassMapping<Equipment>
	{
		public EquipmentMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.Equipment);

			Property(o => o.EquipmentLevel, property => property.Type<ExtensibleEnumType<EquipmentLevel>>());
			Mappings.Map.WithBasicProperties(this);
			
			Set(o => o.Children, set => 
			{
				set.Table("EquipmentChild");
				set.Key(k => k.Column("ParentId"));
				// Collection managed by Parents collection because fewer members => more performant
				set.Inverse(true);
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None); 
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(map => { map.Column("ChildId"); }));

			Set(o => o.Parents, set => 
			{
				set.Table("EquipmentChild");
				set.Key(k => k.Column("ChildId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("ParentId"); }));

			Set(o => o.EquipmentClasses, set =>
			{
				set.Table("EquipmentClassMember");
				set.Key(k => k.Column("EquipmentId"));
				set.Fetch(CollectionFetchMode.Select);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				set.Cascade(Cascade.None);
				set.Access(Accessor.NoSetter);
			}, rel => rel.ManyToMany(manyToMany => { manyToMany.Column("EquipmentClassId"); }));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Descendants, bag => {
				bag.Table("EquipmentDescendant");
				bag.Key(k => k.Column("AscendantId"));
				bag.Inverse(true);
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.None);
				bag.Access(Accessor.Field);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("DescendantId")));

			// Defined as a bag so that the collection isn't initialised upon adding to the collection
			Bag(o => o.Ascendants, bag => {
				bag.Table("EquipmentDescendant");
				bag.Key(k => k.Column("DescendantId"));
				bag.Fetch(CollectionFetchMode.Select);
				// Lazy="extra" so that Contains operation can run without loading the entire collection
				bag.Lazy(CollectionLazy.Extra);
				// No cascades between objects of the same type in potentially large hierarchies don't perform well because 
				// NH will inspect the entire hierarchy on each cascaded operation.
				bag.Cascade(Cascade.ReAttach);
				bag.Access(Accessor.Field);
			}, rel => rel.ManyToMany(manyToMany => manyToMany.Column("AscendantId")));

			Index.Declare("IDX_EquipmentChild_ParentId_ChildId", IndexType.Unique, "EquipmentChild", new[] { "ParentId", "ChildId" });
			Index.Declare("IDX_EquipmentChild_ChildId_ParentId", IndexType.Unique, "EquipmentChild", new[] { "ChildId", "ParentId" });
			Index.Declare("IDX_EquipmentDescendant_AscendantId_DescendantId", IndexType.Standard, "EquipmentDescendant", 
				new[] { "AscendantId", "DescendantId" });
			Index.Declare("IDX_EquipmentDescendant_DescendantId_AscendantId", IndexType.Standard, "EquipmentDescendant", 
				new[] { "DescendantId", "AscendantId" });
			Index.Declare("IDX_EquipmentClassMember_EquipmentId_EquipmentClassId", IndexType.Standard, "EquipmentClassMember",
				new[] { "EquipmentId", "EquipmentClassId" });
			Index.Declare("IDX_EquipmentClassMember_EquipmentClassId_EquipmentId", IndexType.Standard, "EquipmentClassMember",
				new[] { "EquipmentClassId", "EquipmentId" });
		}
	}
}
