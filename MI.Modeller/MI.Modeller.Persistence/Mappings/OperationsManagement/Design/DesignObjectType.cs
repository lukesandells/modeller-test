﻿namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design
{
	public enum DesignObjectType
	{
		// Hierarchy scope model
		HierarchyScope = 1,

		// Equipment model
		Equipment = 2,
		EquipmentClass = 3,

		// Work master model
		WorkMaster = 4,
		WorkMasterMaterialSpecification = 5,
		WorkMasterEquipmentSpecification = 6,
		WorkMasterPhysicalAssetSpecification = 7,
		WorkMasterPersonnelSpecification = 8,

		// Other Resources
		PersonnelClass = 9,
		PhysicalAssetClass = 10,

	    // Material model
		MaterialClass = 11,
		MaterialDefinition = 12,

		// Process Segment model
		ProcessSegment = 13,
		ProcessSegmentMaterialSpecification = 14,
		ProcessSegmentEquipmentSpecification = 15,
		ProcessSegmentPhysicalAssetSpecification = 16,
		ProcessSegmentPersonnelSpecification = 17,

		// OperationsDefinition model
		OperationsSegment = 18,
		OperationsSegmentMaterialSpecification = 19,
		OperationsSegmentEquipmentSpecification = 20,
		OperationsSegmentPhysicalAssetSpecification = 21,
		OperationsSegmentPersonnelSpecification = 22,
		OperationsDefinition = 23,
		OperationsMaterialBill = 24,
		OperationsMaterialBillItem = 25
	}
}
