﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class PhysicalAssetSpecificationMapping : PhysicalAssetSpecificationMapping<OperationsSegment>
	{
		public PhysicalAssetSpecificationMapping() : base(DesignObjectType.OperationsSegmentPhysicalAssetSpecification)
		{
		}
	}
}
