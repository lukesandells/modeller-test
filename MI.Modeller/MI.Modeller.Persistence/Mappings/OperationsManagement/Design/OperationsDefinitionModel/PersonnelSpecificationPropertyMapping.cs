﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class PersonnelSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<PersonnelSpecification<OperationsSegment>>
	{
		public PersonnelSpecificationPropertyMapping() : base(PropertyType.OperationsSegmentPersonnelSpecificationProperty)
		{
		}
	}
}
