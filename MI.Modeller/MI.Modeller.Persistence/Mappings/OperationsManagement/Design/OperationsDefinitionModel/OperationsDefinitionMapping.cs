﻿using MI.Framework.Persistence.NHibernate.Types;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class OperationsDefinitionMapping: SubclassMapping<OperationsDefinition>
	{

		public OperationsDefinitionMapping()
		{
			Lazy(false);
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.OperationsDefinition);

			Property(o => o.BillOfResourcesId);
			Property(o => o.ProcessType, property => property.Type<ExtensibleEnumType<ProcessType>>());
			Property(o => o.WorkDefinitionId);

			Set(o => o.OperationsSegments,
				set =>
				{
					set.Key(k => k.Column("OperationsDefinitionId"));
					set.Inverse(true);
					set.Lazy(CollectionLazy.Lazy);
					set.Fetch(CollectionFetchMode.Select);
					// No need for cascade, operations segments will be saved seperately
					set.Cascade(Cascade.None);
					set.Access(Accessor.NoSetter);
				}, r => r.OneToMany());

			//Set(o => o.OperationsMaterialBills,
			//	set =>
			//	{
			//		set.Key(k => k.Column("OperationsDefinitionId"));
			//		set.Inverse(true);
			//		set.Lazy(CollectionLazy.Lazy);
			//		set.Fetch(CollectionFetchMode.Select);
			//		// No need for cascade, material bills will be saved seperately
			//		set.Cascade(Cascade.None);
			//		set.Access(Accessor.NoSetter);
			//	}, r => r.OneToMany());

		}

	}
}
