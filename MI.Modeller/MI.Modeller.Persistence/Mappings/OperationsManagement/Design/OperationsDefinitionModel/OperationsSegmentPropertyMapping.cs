﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class OperationsSegmentPropertyMapping : BasicPropertyMapping<OperationsSegment>
	{
		public OperationsSegmentPropertyMapping() : base(PropertyType.OperationsSegmentProperty)
		{
		}
	}
}
