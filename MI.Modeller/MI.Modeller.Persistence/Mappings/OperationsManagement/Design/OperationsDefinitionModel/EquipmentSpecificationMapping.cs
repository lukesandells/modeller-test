﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class EquipmentSpecificationMapping : EquipmentSpecificationMapping<OperationsSegment>
	{
		public EquipmentSpecificationMapping() : base(DesignObjectType.OperationsSegmentEquipmentSpecification)
		{
		}
	}
}
