﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class MaterialSpecificationMapping : MaterialSpecificationMapping<OperationsSegment>
	{
		public MaterialSpecificationMapping() : base(DesignObjectType.OperationsSegmentMaterialSpecification)
		{
		}
	}
}
