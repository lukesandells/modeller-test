﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class OperationsSegmentMapping : SubclassMapping<OperationsSegment>
	{
		public OperationsSegmentMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.OperationsSegment);

			Mappings.Map.WithBasicProperties(this);
			Mappings.Map.AsProcessDefinition(this);

			ManyToOne(o => o.OperationsDefinition, manyToOne =>
			{
				manyToOne.Column("OperationsSegmentOperationsDefinitionId");
				manyToOne.Access(Accessor.Property);
				manyToOne.Fetch(FetchKind.Select);
				// No cascade because definition will be saved/updated independently
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Index.DeclareIfNotDeclared("IDX_DesignObject_OperationsSegmentOperationsDefinitionId", IndexType.Standard, "DesignObject",
				new[] { "OperationsSegmentOperationsDefinitionId" });
		}
	}
}
