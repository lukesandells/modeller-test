﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class PersonnelSpecificationMapping : PersonnelSpecificationMapping<OperationsSegment>
	{
		public PersonnelSpecificationMapping() : base(DesignObjectType.OperationsSegmentPersonnelSpecification)
		{
		}
	}
}
