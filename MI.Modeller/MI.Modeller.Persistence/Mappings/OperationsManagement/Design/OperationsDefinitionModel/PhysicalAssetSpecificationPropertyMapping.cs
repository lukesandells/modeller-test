﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.OperationsDefinitionModel
{
	public class PhysicalAssetSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<PhysicalAssetSpecification<OperationsSegment>>
	{
		public PhysicalAssetSpecificationPropertyMapping() : base(PropertyType.OperationsSegmentPhysicalAssetSpecificationProperty)
		{
		}
	}
}
