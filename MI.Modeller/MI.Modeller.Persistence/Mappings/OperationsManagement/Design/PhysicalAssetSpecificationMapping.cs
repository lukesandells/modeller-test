﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design
{
	public abstract class PhysicalAssetSpecificationMapping<TOfObject> : SubclassMapping<PhysicalAssetSpecification<TOfObject>>
		where TOfObject : class, IProcessDefinition<TOfObject>
	{
		protected PhysicalAssetSpecificationMapping(DesignObjectType designObjectType)
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(designObjectType);

			Property(o => o.PhysicalAssetUse);

			Mappings.Map.WithQuantity(this, o => o.Quantity, "DesignObjectQuantityElement", "ParentObjectId");
			Mappings.Map.WithQuantitySpecificResourceTransactionProperties(this);

			ManyToOne(o => o.ProcessDefinition, manyToOne =>
			{
				manyToOne.Column("ResourceSpecificationProcessDefinitionId");
				manyToOne.Access(Accessor.Property);
				// Fetch by select because it's a rare edge case that a resource specification be loaded before its owning process definition
				manyToOne.Fetch(FetchKind.Select);
				// No cascade because process definition will be saved/updated independently
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.PhysicalAssetClass, manyToOne =>
			{
				manyToOne.Column("PhysicalAssetClassId");
				manyToOne.Access(Accessor.Property);
				manyToOne.Fetch(FetchKind.Select);
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});
			
			Index.DeclareIfNotDeclared("IDX_DesignObject_PhysicalAssetClassId", IndexType.Standard, "DesignObject", new[] { "PhysicalAssetClassId" });

			Index.DeclareIfNotDeclared("IDX_DesignObject_ResourceSpecificationProcessDefinitionId", IndexType.Standard, "DesignObject", 
				new[] { "ResourceSpecificationProcessDefinitionId" });
		}
	}
}
