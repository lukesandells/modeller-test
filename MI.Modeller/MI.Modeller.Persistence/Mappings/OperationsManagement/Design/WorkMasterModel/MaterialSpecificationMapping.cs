﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class MaterialSpecificationMapping : MaterialSpecificationMapping<WorkMaster>
	{
		public MaterialSpecificationMapping() : base(DesignObjectType.WorkMasterMaterialSpecification)
		{
		}
	}
}
