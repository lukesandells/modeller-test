﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class PhysicalAssetSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<PhysicalAssetSpecification<WorkMaster>>
	{
		public PhysicalAssetSpecificationPropertyMapping() : base(PropertyType.WorkMasterPhysicalAssetSpecificationProperty)
		{
		}
	}
}
