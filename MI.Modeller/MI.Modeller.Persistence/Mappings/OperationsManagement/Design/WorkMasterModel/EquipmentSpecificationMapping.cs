﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class EquipmentSpecificationMapping : EquipmentSpecificationMapping<WorkMaster>
	{
		public EquipmentSpecificationMapping() : base(DesignObjectType.WorkMasterEquipmentSpecification)
		{
		}
	}
}
