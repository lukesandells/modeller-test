﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class WorkMasterMapping : SubclassMapping<WorkMaster>
	{
		public WorkMasterMapping()
		{
			Extends(typeof(HierarchyScopedObject));
			DiscriminatorValue(DesignObjectType.WorkMaster);

			Mappings.Map.WithBasicProperties(this);
			Mappings.Map.AsProcessDefinition(this);
		}
	}
}
