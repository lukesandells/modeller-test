﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class PersonnelSpecificationMapping : PersonnelSpecificationMapping<WorkMaster>
	{
		public PersonnelSpecificationMapping() : base(DesignObjectType.WorkMasterPersonnelSpecification)
		{
		}
	}
}
