﻿using MI.Modeller.Persistence.Mappings.OperationsManagement.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class EquipmentSpecificationPropertyMapping : QuantitySpecificResourceTransactionPropertyMapping<EquipmentSpecification<WorkMaster>>
	{
		public EquipmentSpecificationPropertyMapping() : base(PropertyType.WorkMasterEquipmentSpecificationProperty)
		{
		}
	}
}
