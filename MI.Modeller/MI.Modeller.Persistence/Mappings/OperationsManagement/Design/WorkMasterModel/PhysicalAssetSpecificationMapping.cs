﻿using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design.WorkMasterModel
{
	public class PhysicalAssetSpecificationMapping : PhysicalAssetSpecificationMapping<WorkMaster>
	{
		public PhysicalAssetSpecificationMapping() : base(DesignObjectType.WorkMasterPhysicalAssetSpecification)
		{
		}
	}
}
