﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Design
{
	public class HierarchyScopedObjectMapping : SubclassMapping<HierarchyScopedObject>
	{
		public HierarchyScopedObjectMapping()
		{
			Abstract(true);
			Extends(typeof(DesignObject));
			DiscriminatorValue(null);

			// No setter strategy on this because we don't want the hierarchy scope validation logic to run on load from the DB
			ManyToOne(o => o.HierarchyScope, oneToMany => 
			{
				oneToMany.Column("HierarchyScopeId");
				oneToMany.Access(Accessor.NoSetter);
				oneToMany.Lazy(LazyRelation.NoLazy);
				oneToMany.Fetch(FetchKind.Join);
				oneToMany.Cascade(Cascade.None);
				oneToMany.NotNullable(false);
			});

			Index.Declare("IDX_DesignObject_HierarchyScope_ExternalId_DesignObjectTypeId", IndexType.Standard, "DesignObject", 
				new string[] { "HierarchyScopeId", "ExternalId", "DesignObjectTypeId" });
		}
	}
}
