﻿using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Core
{
	public abstract class BasicPropertyMapping<TObjectWithProperties> : SubclassMapping<BasicProperty<TObjectWithProperties>>
		where TObjectWithProperties : class, IWithBasicProperties<TObjectWithProperties>, IWithExternalId
	{
		protected BasicPropertyMapping(PropertyType propertyType)
		{
			Lazy(false);
			Extends(typeof(Property));
			DiscriminatorValue(propertyType);

			ManyToOne(o => o.ParentObject, manyToOne =>
			{
				manyToOne.Column("ParentObjectId");
				manyToOne.Access(Accessor.Property);
				// Fetch by select because it's a rare edge case that a property be loaded independently from its parent
				manyToOne.Fetch(FetchKind.Select);
				// No cascade because properties will be saved by cascading from the parent object
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			ManyToOne(o => o.ParentProperty, manyToOne =>
			{
				manyToOne.Column("ParentPropertyId");
				manyToOne.Access(Accessor.Property);
				// Fetch by select because it's a rare edge case that a property be loaded independently from its parent
				manyToOne.Fetch(FetchKind.Select);
				// No cascade because properties will be saved by cascading from the parent object
				manyToOne.Cascade(Cascade.None);
				manyToOne.NotNullable(false);
			});

			Index.DeclareIfNotDeclared("IDX_Property_ParentObjectId", IndexType.Standard, nameof(Property), new[] { "ParentObjectId" });
			Index.DeclareIfNotDeclared("IDX_Property_ParentPropertyId", IndexType.Standard, nameof(Property), new[] { "ParentPropertyId" });
		}
	}
}
