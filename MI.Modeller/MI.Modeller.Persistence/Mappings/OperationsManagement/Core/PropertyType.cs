﻿namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Core
{
	public enum PropertyType
	{
		EquipmentProperty = 1,
		EquipmentClassProperty = 2,
		WorkMasterProperty = 3,
		WorkMasterMaterialSpecificationProperty = 4,
		WorkMasterEquipmentSpecificationProperty = 5,
		WorkMasterPhysicalAssetSpecificationProperty = 6,
		WorkMasterPersonnelSpecificationProperty = 7,
		PersonnelClassProperty = 8,
		PhysicalAssetClassProperty = 9,
		MaterialClassProperty = 10,
		MaterialDefinitionProperty = 11,
		ProcessSegmentProperty = 12,
		ProcessSegmentMaterialSpecificationProperty = 13,
		ProcessSegmentEquipmentSpecificationProperty = 14,
		ProcessSegmentPhysicalAssetSpecificationProperty = 15,
		ProcessSegmentPersonnelSpecificationProperty = 16,
		OperationsSegmentProperty = 17,
		OperationsSegmentMaterialSpecificationProperty = 18,
		OperationsSegmentEquipmentSpecificationProperty = 19,
		OperationsSegmentPhysicalAssetSpecificationProperty = 20,
		OperationsSegmentPersonnelSpecificationProperty = 21,
	}
}
