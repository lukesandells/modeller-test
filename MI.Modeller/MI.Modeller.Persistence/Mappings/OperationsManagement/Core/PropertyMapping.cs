﻿using MI.OperationsManagement.Domain.Core.ObjectProperties;
using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Modeller.Persistence.Mappings.OperationsManagement.Core
{
	public class PropertyMapping : ClassMapping<Property>
	{
		public PropertyMapping()
		{
			Abstract(true);
			Lazy(false);
			Id(o => o.InternalId, id =>
			{
				id.Access(Accessor.Property);
				id.Generator(Generators.HighLow, g => g.Params(new { max_lo = 1000 }));
			});

			Discriminator(discriminator => 
			{
				discriminator.Column("PropertyTypeId");
				discriminator.Type<NHibernate.Type.EnumType<PropertyType>>();
				discriminator.NotNullable(true);
			});

			DiscriminatorValue(null);

			Property(o => o.ExternalId, property => property.NotNullable(true));
			Property(o => o.Description, property => property.NotNullable(true));

			Mappings.Map.WithValue(this, o => o.Value, "PropertyValueElement", "PropertyId");
		}
	}
}
