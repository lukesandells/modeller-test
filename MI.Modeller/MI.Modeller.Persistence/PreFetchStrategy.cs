﻿using System.Linq;
using NHibernate;
using NHibernate.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Persistence.NHibernate;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.Modeller.Persistence.Mappings.OperationsManagement.Design;
using MI.Modeller.Domain;

namespace MI.Modeller.Persistence
{
	/// <summary>
	/// Object pre-fetch strategies.
	/// </summary>
	public static class PreFetchStrategy
	{
		/// <summary>
		/// Strategy for pre-fetching model elements.
		/// </summary>
		public static ITargetObjectPreFetchStrategy ModelElement { get; } = new ModelElementPreFetchStrategy();

		/// <summary>
		/// Strategy for pre-fetching model elements.
		/// </summary>
		/// <remarks>
		/// <para>A pre-fetch strategy must use NHibernate as the persistence provider because data should be fetched using NHibernate futures,
		/// which results in all queries (nodes and target objects and their associations) being combined into a minimal set of optimal
		/// queries, which are sent to the database as a single batch and the results returned in a single batch.  The way in which 
		/// NHibernate combines the queries into a minimal set of queries to fetch the required data and then "hydrate" the retrieved 
		/// object hierarchies is significantly more efficient than separately executing each query that retrieves each portion of the data
		/// required to be pre-fecthed.  Native .NET LINQ provides no standard expression for signalling the query provider that the 
		/// query results should be deferred until all required queries have been issued.</para>
		/// 
		/// <para>A pre-fetch strategy must also be specifically dependent upon using NHibernate as the persistence provider because it requires 
		/// specific LINQ expressions to signal which associations of an object should be fetched such that the objects returned in 
		/// the query results have those associations populated (known as fetch joins).  There is no native .NET LINQ syntax for fetch joins.
		/// Therefore an NHibernate-specific expression must be used.</para>
		/// 
		/// <para>Furthermore, the model element pre-fetch strategy will for some node operations pre-fetch design object property collections, 
		/// which are exposed using property mix-ins, which are not publicly visible.  Therefore, HQL must be used to query the property 
		/// collections, which again is NHibernate-specific.</para>
		/// </remarks>
		private class ModelElementPreFetchStrategy : ITargetObjectPreFetchStrategy
		{
			/// <summary>
			/// Pre-fetch target objects of model elements corresponding to descendants of the given node.
			/// </summary>
			/// <param name="node">The node for which model elements shall be pre-fetched.</param>
			/// <param name="operationType">The type of node operation for which to pre-fetch.</param>
			public void PreFetchTargetObjects(ParentNode node, OperationType operationType)
			{
				using (var scope = new PersistenceScope(TransactionOption.Supported))
				{
					// Get the distinct set of design objects held in the descendants.  HQL needed here
					// because the design object discriminator is not defined in the domain and therefore
					// cannot be queried using NHibernate-independent LINQ.
					var descendantDesignObjectTypes = NHibernateSession.Current.CreateQuery(
						@"select distinct designObject.class from ModelElement modelElement join modelElement.DesignObject designObject 
						  where :node in elements(modelElement.Ascendants)")
						.SetParameter("node", node)
						.List().Cast<DesignObjectType>();

					if (descendantDesignObjectTypes.Contains(DesignObjectType.HierarchyScope))
					{
						PreFetchHierarchyScopes(NHibernateSession.Current, node, operationType);
					}

					if (descendantDesignObjectTypes.Contains(DesignObjectType.Equipment))
					{
						PreFetchEquipment(NHibernateSession.Current, node, operationType);
					}

					if (descendantDesignObjectTypes.Contains(DesignObjectType.EquipmentClass))
					{
						PreFetchEquipmentClasses(NHibernateSession.Current, node, operationType);
					}

					var remainingDesignObjectTypes = new[] { DesignObjectType.HierarchyScope, DesignObjectType.Equipment, DesignObjectType.EquipmentClass };
					if (descendantDesignObjectTypes.Except(remainingDesignObjectTypes).Count() > 0)
					{
						// Then fetch the remaining hierarchy scoped objects
						OtherDescendantHierarchyScopedObjects(NHibernateSession.Current, node)
							.ToFuture();
					}

					scope.Done();
				}
			}

			/// <summary>
			/// Returns a queryable collection of the descendant hierarchy scopes for the given node.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The node for which to retrieve the descendant hierarchy scopes.</param>
			/// <returns>A queryable collection of the descendant hierarchy scopes for the given node.</returns>
			private IQueryable<HierarchyScope> DescendantHierarchyScopes(ISession session, ParentNode node)
			{
				return (from hierarchyScope in session.Query<HierarchyScope>()
						join modelElement in session.Query<ModelElement>() on hierarchyScope equals modelElement.DesignObject
						where modelElement.Ascendants.Contains(node)
						select hierarchyScope);
			}

			/// <summary>
			/// Returns a queryable collection of the defining equipment of the descendant hierarchy scopes for the given node.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The node for which to retrieve the descendant hierarchy scope defining equipment.</param>
			/// <returns>A queryable collection of the defining equipment of the descendant hierarchy scopes for the given node.</returns>
			private IQueryable<Equipment> DescendantHierarchyScopeDefiningEquipment(ISession session, ParentNode node)
			{
				return (from hierarchyScope in session.Query<HierarchyScope>()
						join modelElement in session.Query<ModelElement>() on hierarchyScope equals modelElement.DesignObject
						where modelElement.Ascendants.Contains(node)
						select hierarchyScope.DefiningEquipment);
			}

			/// <summary>
			/// Returns a queryable collection of the descendant equipment for the given node.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The node for which to retrieve the descendant equipment.</param>
			/// <returns>A queryable collection of the descendant equipment for the given node.</returns>
			private IQueryable<Equipment> DescendantEquipment(ISession session, ParentNode node)
			{
				return (from equipment in session.Query<Equipment>()
						join modelElement in session.Query<ModelElement>() on equipment equals modelElement.DesignObject
						where modelElement.Ascendants.Contains(node)
						select equipment);
			}

			/// <summary>
			/// Returns a queryable collection of the descendant equipment classes for the given node.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The node for which to retrieve the descendant equipment classes.</param>
			/// <returns>A queryable collection of the descendant equipment classes for the given node.</returns>
			private IQueryable<EquipmentClass> DescendantEquipmentClasses(ISession session, ParentNode node)
			{
				return (from equipmentClass in session.Query<EquipmentClass>()
						join modelElement in session.Query<ModelElement>() on equipmentClass equals modelElement.DesignObject
						where modelElement.Ascendants.Contains(node)
						select equipmentClass);
			}

			/// <summary>
			/// Returns a queryable collection of the descendant hierarchy scoped objects that are not covered by specific pre-fetch queries.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The node for which to retrieve the descendant hierarchy scoped objects.</param>
			/// <returns>A queryable collection of the descendant hierarchy scoped objects that are not covered by specific pre-fetch queries.</returns>
			private IQueryable<HierarchyScopedObject> OtherDescendantHierarchyScopedObjects(ISession session, ParentNode node)
			{
				return (from hso in session.Query<HierarchyScopedObject>()
						join modelElement in session.Query<ModelElement>() on hso equals modelElement.DesignObject
						join hierarchyScope in session.Query<HierarchyScope>() on hso.HierarchyScope equals hierarchyScope
						where modelElement.Ascendants.Contains(node)
						&& hso != hierarchyScope.DefiningEquipment
						&& !(hso is Equipment)
						&& !(hso is EquipmentClass)
						select hso);
			}

			/// <summary>
			/// Pre-fetches the hierarchy scopes.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The ascendant node.</param>
			/// <param name="operationType">The type of operation for which to pre-fetch.</param>
			private void PreFetchHierarchyScopes(ISession session, ParentNode node, OperationType operationType)
			{
				// Hierarchy scopes - parent and defining equipment
				DescendantHierarchyScopes(session, node)
					.Fetch(hierarchyScope => hierarchyScope.DefiningEquipment)
					.Fetch(hierarchyScope => hierarchyScope.Parent)
					.ToFuture();

				// Fetching relationships between design objects not needed for a copy operation because
				// all new nodes are being created and the structure/hierarchy of the source nodes is 
				// determined using the relationships between the nodes.
				if (operationType != OperationType.CopyTo)
				{
					// Hierarchy scopes - children
					DescendantHierarchyScopes(session, node)
						.FetchMany(hierarchyScope => hierarchyScope.Children)
						.ToFuture();

					// Hierarchy scopes - ascendants
					DescendantHierarchyScopes(session, node)
						.FetchMany(hierarchyScope => hierarchyScope.Ascendants)
						.ToFuture();

					// Hierarchy scope - descendants
					DescendantHierarchyScopes(session, node)
						.FetchMany(hierarchyScope => hierarchyScope.Descendants)
						.ToFuture();

					// Hierarchy scopes - defining equipment - parents
					DescendantHierarchyScopeDefiningEquipment(session, node)
						.FetchMany(equipment => equipment.Parents)
						.ToFuture();

					// Hierarchy scopes - defining equipment - children
					DescendantHierarchyScopeDefiningEquipment(session, node)
						.FetchMany(equipment => equipment.Children)
						.ToFuture();

					// Hierarchy scopes - defining equipment - ascendants
					DescendantHierarchyScopeDefiningEquipment(session, node)
						.FetchMany(equipment => equipment.Ascendants)
						.ToFuture();

					// Hierarchy scope - defining equipment - descendants
					DescendantHierarchyScopeDefiningEquipment(session, node)
						.FetchMany(equipment => equipment.Descendants)
						.ToFuture();

					// Hierarchy scopes - defining equipment - classes
					DescendantHierarchyScopeDefiningEquipment(session, node)
						.FetchMany(equipment => equipment.EquipmentClasses)
						.ToFuture();
				}

				// Hierarchy scopes - defining equipment - properties
				if (operationType == OperationType.CopyTo)
				{
					session.CreateQuery(
						@"select hierarchyScope.DefiningEquipment 
						  from ModelElement modelElement, HierarchyScope hierarchyScope
						  left join fetch hierarchyScope.DefiningEquipment._withPropertiesMixIn.AllProperties property 
						  left join fetch property.Value._elements 
						  where :node in elements(modelElement.Ascendants) and hierarchyScope = modelElement.DesignObject")
						.SetParameter("node", node)
						.Future<Equipment>();
				}
			}

			/// <summary>
			/// Pre-fetches the equipment.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The ascendant node.</param>
			/// <param name="operationType">The type of operation for which to pre-fetch.</param>
			private void PreFetchEquipment(ISession session, ParentNode node, OperationType operationType)
			{
				// Fetching relationships between design objects not needed for a copy operation because
				// all new nodes are being created and the structure/hierarchy of the source nodes is 
				// determined using the relationships between the nodes.
				if (operationType != OperationType.CopyTo)
				{
					// Equipment - children
					DescendantEquipment(session, node)
					.FetchMany(equipment => equipment.Children)
					.ToFuture();

					// Equipment - parents
					DescendantEquipment(session, node)
						.FetchMany(equipment => equipment.Parents)
						.ToFuture();

					// Equipment - ascendants
					DescendantEquipment(session, node)
						.FetchMany(equipment => equipment.Ascendants)
						.ToFuture();

					// Equipment - descendants
					DescendantEquipment(session, node)
						.FetchMany(equipment => equipment.Descendants)
						.ToFuture();

					// Equipment - classes
					DescendantEquipment(session, node)
						.FetchMany(equipment => equipment.EquipmentClasses)
						.ToFuture();
				}

				// Equipment - properties
				if (operationType == OperationType.CopyTo)
				{
					session.CreateQuery(
						@"select equipment 
						  from ModelElement modelElement, Equipment equipment 
						  left join fetch equipment._withPropertiesMixIn.AllProperties property 
						  left join fetch property.Value._elements 
						  where :node in elements(modelElement.Ascendants) and equipment = modelElement.DesignObject")
						.SetParameter("node", node)
						.Future<Equipment>();
				}
			}

			/// <summary>
			/// Pre-fetches the equipment classes.
			/// </summary>
			/// <param name="session">The NHibernate session.</param>
			/// <param name="node">The ascendant node.</param>
			/// <param name="operationType">The type of operation for which to pre-fetch.</param>
			private void PreFetchEquipmentClasses(ISession session, ParentNode node, OperationType operationType)
			{
				// Fetching relationships between design objects not needed for a copy operation because
				// all new nodes are being created and the structure/hierarchy of the source nodes is 
				// determined using the relationships between the nodes.
				if (operationType != OperationType.CopyTo)
				{
					// Equipment classes - members
					DescendantEquipmentClasses(session, node)
					.FetchMany(equipmentClass => equipmentClass.Members)
					.ToFuture();

					// Equipment classes - superclasses
					DescendantEquipmentClasses(session, node)
						.FetchMany(equipmentClass => equipmentClass.SuperClasses)
						.ToFuture();

					// Equipment classes - ascendants
					DescendantEquipmentClasses(session, node)
						.FetchMany(equipmentClass => equipmentClass.Ascendants)
						.ToFuture();

					// Equipment classes - descendants
					DescendantEquipmentClasses(session, node)
						.FetchMany(equipmentClass => equipmentClass.Descendants)
						.ToFuture();

					// Equipment classes - member equipment
					DescendantEquipmentClasses(session, node)
						.FetchMany(equipmentClass => equipmentClass.Members)
						.ToFuture();
				}

				// Equipment classes - properties
				if (operationType == OperationType.CopyTo)
				{
					session.CreateQuery(
						@"select equipmentClass 
						  from ModelElement modelElement, EquipmentClass equipmentClass
						  left join fetch equipmentClass._withPropertiesMixIn.AllProperties property 
						  left join fetch property.Value._elements 
						  where :node in elements(modelElement.Ascendants) and equipmentClass = modelElement.DesignObject")
						.SetParameter("node", node)
						.Future<EquipmentClass>();
				}
			}
		}
	}
}
