﻿using System;
using System.Data;
using System.Data.Common;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;
using NHibernate;
using NHibernate.Engine;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

namespace MI.Modeller.Persistence.Types
{
	/// <summary>
	/// Represents the persistent type for <see cref="MI.OperationsManagement.Domain.Core.DataTypes.Uncertainty" />.
	/// </summary>
	public class UncertaintyType : IUserType
	{
		/// <summary>
		/// The list of SQL types to which the <see cref="MI.OperationsManagement.Domain.Core.DataTypes.Uncertainty"/> type maps.
		/// </summary>
		private SqlType[] _sqlTypes = new SqlType[]
		{
			NHibernateUtil.Double.SqlType,
			NHibernateUtil.Double.SqlType,
			NHibernateUtil.Double.SqlType,
			NHibernateUtil.Double.SqlType,
			NHibernateUtil.Double.SqlType
		};

		/// <summary>
		/// Tests two <see cref="MI.OperationsManagement.Domain.Core.DataTypes.Uncertainty" /> values for equality.
		/// </summary>
		/// <param name="x">The <see cref="MI.OperationsManagement.Domain.Core.DataTypes.Uncertainty"/> value on the left hand side of the equality operator.</param>
		/// <param name="y">The <see cref="MI.OperationsManagement.Domain.Core.DataTypes.Uncertainty"/> value on the right hand side of the equality operator.</param>
		/// <returns>A <see cref="Boolean"/> indicating whether the two <see cref="UnitDecimal"/> values are equal.</returns>
		public new bool Equals(object x, object y)
		{
			return (Uncertainty)x == (Uncertainty)y;
		}

		/// <summary>
		/// Get a hashcode for the instance, consistent with persistence "equality".
		/// </summary>
		/// <param name="x">The object for which to get the hashcode.</param>
		/// <returns>The hashcode for the given object.</returns>
		public int GetHashCode(object x)
		{
			if (x != null)
			{
				return x.GetHashCode();
			}

			return 0;
		}

		/// <summary>
		/// Creates a deep copy of a <see cref="UnitDecimal"/> value.
		/// </summary>
		/// <param name="value">The <see cref="UnitDecimal"/> value to be copied.</param>
		/// <returns>A deep copy of the given <see cref="UnitDecimal"/> value.</returns>
		public object DeepCopy(object value)
		{
			return ((Uncertainty)value)?.Copy();
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="UnitDecimal"/> type is mutable.
		/// </summary>
		/// <remarks>
		/// The <see cref="UnitDecimal"/> type is not mutable so this method always returns <c>false</c>.
		/// </remarks>
		public bool IsMutable
		{
			get { return false; }
		}

		/// <summary>
		/// Performs a read of a <see cref="UnitDecimal"/> value from an implementation of <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="rs">An implmentation of <see cref="IDataReader"/> containing the <see cref="UnitDecimal"/> value to be read.</param>
		/// <param name="names">The array of field names to map to the <see cref="UnitDecimal"/> value.</param>
		/// <param name="session">The session for which the operation is done. Allows access to
		/// <c>Factory.Dialect</c> and <c>Factory.ConnectionProvider.Driver</c> for adjusting to
		/// database or data provider capabilities.</param>
		/// <param name="owner">The containing entity.</param>
		/// <returns>The <see cref="UnitDecimal"/> value read from the <see cref="IDataReader"/> implementation.</returns>
		public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
		{
			// Get the field values from the database
			var standardUncertainty = (float?)NHibernateUtil.Single.NullSafeGet(rs, names[0], session);
			var coverageFactor = (float?)NHibernateUtil.Single.NullSafeGet(rs, names[1], session);
			var confidenceLevel = (float?)NHibernateUtil.Single.NullSafeGet(rs, names[2], session);
			var accuracy = (float?)NHibernateUtil.Single.NullSafeGet(rs, names[3], session);
			var precision = (float?)NHibernateUtil.Single.NullSafeGet(rs, names[4], session);

			// Evaluate field nulls to see which type of uncertainty
			if (standardUncertainty.HasValue && coverageFactor.HasValue && confidenceLevel.HasValue)
			{
				// Expanded uncertainty
				return Uncertainty.ExpandedUncertainty(coverageFactor.Value, confidenceLevel.Value, standardUncertainty.Value);
			}
			else if (accuracy.HasValue && precision.HasValue)
			{
				// Accuracy and precision
				return Uncertainty.AccuracyAndPrecision(accuracy.Value, precision.Value);
			}

			return null;
		}

		/// <summary>
		/// Performs a write of a <see cref="UnitDecimal"/> value to the parameters of an implementation of <see cref="DbCommand"/>.
		/// </summary>
		/// <param name="cmd">An implementation of <see cref="DbCommand"/> representing a write command to be executed against the database.</param>
		/// <param name="value">The <see cref="UnitDecimal"/> value to be written to the database.</param>
		/// <param name="index">The command parameter index.</param>
		/// <param name="session">The session for which the operation is done. Allows access to
		/// <c>Factory.Dialect</c> and <c>Factory.ConnectionProvider.Driver</c> for adjusting to
		/// database or data provider capabilities.</param>
		public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
		{
			cmd = ArgumentValidation.AssertNotNull(cmd, "cmd");

			if (value == null)
			{
				((IDbDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
			}
			else
			{
				var expandedUncertainty = value as ExpandedUncertainty;
				var accuracyAndPrecision = value as AccuracyAndPrecision;
				if (expandedUncertainty != null)
				{
					((IDbDataParameter)cmd.Parameters[index]).Value = expandedUncertainty.StandardUncertainty;
					((IDbDataParameter)cmd.Parameters[index + 1]).Value = expandedUncertainty.CoverageFactor;
					((IDbDataParameter)cmd.Parameters[index + 2]).Value = expandedUncertainty.ConfidenceLevel;
				}
				else if(accuracyAndPrecision != null)
				{
					((IDbDataParameter)cmd.Parameters[index + 3]).Value = accuracyAndPrecision.Accuracy;
					((IDbDataParameter)cmd.Parameters[index + 4]).Value = accuracyAndPrecision.Precision;
				}
				else if (value != null)
				{
					throw new ArgumentOutOfRangeException(ExceptionMessages.UnknownTypeOfUncertainty);
				}
			}
		}

		/// <summary>
		/// Gets the .NET type represented by this NHibernate type.
		/// </summary>
		public Type ReturnedType
		{
			get { return typeof(Uncertainty); }
		}

		/// <summary>
		/// Gets the SQL types to which the <see cref="UnitDecimal"/> type maps.
		/// </summary>
		public SqlType[] SqlTypes
		{
			get { return _sqlTypes; }
		}

		/// <summary>
		/// Called when an entity is read from the second-level cache.
		/// </summary>
		/// <param name="cached">The cached object.</param>
		/// <param name="owner">The owning object.</param>
		/// <returns>A copy of the entity, reconstituted from the cache.</returns>
		public object Assemble(object cached, object owner)
		{
			return DeepCopy(cached);
		}

		/// <summary>
		/// Called when an entity is written to the second-level cache.
		/// </summary>
		/// <param name="value">The object to be cached.</param>
		/// <returns>A copy of the object to be stored in the cache.</returns>
		public object Disassemble(object value)
		{
			return DeepCopy(value);
		}

		/// <summary>
		/// During merge, replace the existing (target) value in the entity we are merging to with a new 
		/// (original) value from the detached entity we are merging. For immutable objects, or null 
		/// values, it is safe to simply return the first parameter. For mutable objects, it is safe to 
		/// return a copy of the first parameter. However, since composite user types often define component 
		/// values, it might make sense to recursively replace component values in the target object.
		/// </summary>
		/// <param name="original">The new value.</param>
		/// <param name="target">The existing value.</param>
		/// <param name="owner">The owning entity.</param>
		/// <returns>The replacement value.</returns>
		public object Replace(object original, object target, object owner)
		{
			// Immuatable type, so okay to return original
			return original;
		}
	}
}
