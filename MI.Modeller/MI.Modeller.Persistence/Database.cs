﻿using System.Reflection;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.Persistence;
using MI.Framework.Persistence.NHibernate;
using MI.Framework.Validation;
using NHibernate.Cfg;

namespace MI.Modeller.Persistence
{
	/// <summary>
	/// Represents the SQLite database.
	/// </summary>
	public static class Database
	{
		/// <summary>
		/// Connects to the SQLite database.
		/// </summary>
		/// <param name="databasePath">The database path.</param>
		/// <param name="replaceIfExists">Indicates whether to delete the database file if it exists.</param>
		/// <param name="logSqlInConsole">Indicates whether to log SQL to the console.</param>
		public static void Connect(string databasePath, bool replaceIfExists, bool logSqlInConsole)
		{
			SqliteConfiguration.ConnectionString connectionString = new SqliteConfiguration.ConnectionString()
			{
				BinaryGuid = true,
				Compress = false,
				DatabasePath = ArgumentValidation.AssertNotNullOrEmpty(databasePath, nameof(databasePath)),
				FailIfMissing = false,
				New = false,
				Pooling = true,
				Version = 3,
				UseUtf16Encoding = true,
				CacheSize = 10000
			};

			SqliteConfiguration cfg = new SqliteConfiguration(connectionString, logSqlInConsole);
			cfg.AddMappingAssembly(typeof(BrowserNodeMapping).Assembly);
			cfg.AddMappingAssembly(Assembly.GetExecutingAssembly());
			cfg.CompileMappings();
			cfg.CreateDatabase(replaceIfExists, logSqlInConsole);

			Environment.Properties[Environment.MaxFetchDepth] = "3";
			
			PersistenceContext.SetProvider(new NHibernateProvider());
			NHibernateProvider.Configure(cfg);
		}
	}
}
