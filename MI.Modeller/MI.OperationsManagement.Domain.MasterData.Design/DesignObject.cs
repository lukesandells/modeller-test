﻿using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;

namespace MI.OperationsManagement.Domain.MasterData.Design
{
	/// <summary>
	/// Base class for all objects defined in the MI operations management library.
	/// </summary>
	public abstract class DesignObject : IWithExternalId
	{
		/// <summary>
		/// The internal identifier.
		/// </summary>
		public virtual long InternalId { get; protected set; }
		
		/// <summary>
		/// The external identifier.
		/// </summary>
		public virtual string ExternalId { get => _externalId; set => _externalId = value; }
		private string _externalId = string.Empty;

		/// <summary>
		/// The description of the object.
		/// </summary>
		public virtual string Description { get; set; } = string.Empty;

		/// <summary>
		/// Initialises a new instance of the <see cref="DesignObject"/> class.
		/// </summary>
		protected DesignObject()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DesignObject"/> class.
		/// </summary>
		/// <param name="externalId">The external identifier of the new manufacturing object.</param>
		protected DesignObject(string externalId)
		{
			_externalId = ArgumentValidation.AssertNotNullOrEmpty(externalId, nameof(externalId));
		}

		/// <summary>
		/// Returns a <see cref="Boolean"/> stipulating whether the design object is visible from the given hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope for which visibility shall be evaluated.</param>
		/// <returns><c>true</c> if the design object is visible from the given hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool IsVisibleFrom(HierarchyScope hierarchyScope)
		{
			// Hierarchy scopes are always visible from all other Hierarchy Scopes
			if (this is HierarchyScope)
			{
				return true;
			}
			var hierarchyScopedObject = this as HierarchyScopedObject;
			return hierarchyScopedObject == null ? true
				: hierarchyScopedObject.HierarchyScope.IsDescendantOf(hierarchyScope)
				|| hierarchyScopedObject.HierarchyScope.IsAscendantOf(hierarchyScope)
				|| hierarchyScopedObject.HierarchyScope == hierarchyScope;
		}

		/// <summary>
		/// Returns a string representation of the design object.
		/// </summary>
		/// <returns>A string representation of the design object.</returns>
		public override string ToString()
		{
			return GetType().GetNonGenericTypeName() + ": " + ExternalId;
		}
	}
}
