﻿using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.MasterData.Design
{
	/// <summary>
	/// Base class for any object scoped with a <see cref="Design.HierarchyScope"/>.
	/// </summary>
	public abstract class HierarchyScopedObject : DesignObject
	{
		/// <summary>
		/// The hierarchy scope of this <see cref="HierarchyScopedObject"/>.
		/// </summary>
		public virtual HierarchyScope HierarchyScope { get => _hierarchyScope; set => _hierarchyScope = value; }
		private HierarchyScope _hierarchyScope;

		/// <summary>
		/// Initialises a new instance of the <see cref="HierarchyScopedObject"/> class.
		/// </summary>
		protected HierarchyScopedObject()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="HierarchyScopedObject"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new object.</param>
		protected HierarchyScopedObject(HierarchyScope hierarchyScope)
		{
			_hierarchyScope = ArgumentValidation.AssertNotNull(hierarchyScope, nameof(hierarchyScope));
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="HierarchyScopedObject"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new object.</param>
		/// <param name="externalId">The external identifier of the new object.</param>
		protected HierarchyScopedObject(HierarchyScope hierarchyScope, string externalId) 
			: base(externalId)
		{
			_hierarchyScope = ArgumentValidation.AssertNotNull(hierarchyScope, nameof(hierarchyScope));
		}
	}
}
