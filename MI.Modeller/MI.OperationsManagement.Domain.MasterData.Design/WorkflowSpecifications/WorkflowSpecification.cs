﻿//using System.Collections.Generic;
//using MI.Framework.Validation;

//namespace MI.Modeller.Domain.Client
//{
//    /// <summary>
//    ///     Class WorkflowSpecification.
//    /// </summary>
//    public class WorkflowSpecification
//    {
//        #region  Fields

//        /// <summary>
//        ///     Gets the connections.
//        /// </summary>
//        /// <value>The connections.</value>
//        public virtual ISet<WorkflowSpecificationConnection> Connections { get; set; }

//        /// <summary>
//        ///     Gets the description.
//        /// </summary>
//        /// <value>The description.</value>
//        public virtual string Description { get; protected set; }

//        /// <summary>
//        ///     Gets the hierarchy scope.
//        /// </summary>
//        /// <value>The hierarchy scope.</value>
//        public virtual HierarchyScope HierarchyScope { get; protected set; }

//        /// <summary>
//        ///     Gets the identifier.
//        /// </summary>
//        /// <value>The identifier.</value>
//        public virtual string Id { get; protected set; }

//        /// <summary>
//        ///     Gets the nodes.
//        /// </summary>
//        /// <value>The nodes.</value>
//        public virtual ISet<WorkflowSpecificationNode> Nodes { get; set; }

//        /// <summary>
//        ///     Gets the version.
//        /// </summary>
//        /// <value>The version.</value>
//        public virtual string Version { get; protected set; }

//        #endregion

//        #region Constructors

//        /// <summary>
//        ///     Initializes a new instance of the <see cref="WorkflowSpecification" /> class.
//        /// </summary>
//        /// <param name="id">The identifier.</param>
//        /// <param name="version">The version.</param>
//        /// <param name="hierarchyScope">The hierarchy scope.</param>
//        /// <param name="description">The description.</param>
//        /// <param name="nodes">The nodes.</param>
//        /// <param name="connections">The connections.</param>
//        public WorkflowSpecification(string id,
//            string version = default(string),
//            HierarchyScope hierarchyScope = default(HierarchyScope),
//            string description = null,
//            ISet<WorkflowSpecificationNode> nodes = null,
//            ISet<WorkflowSpecificationConnection> connections = null)
//        {
//            ArgumentValidation.AssertNotNull(id,
//                nameof(id));
//            Id = id;
//            Version = version;
//            HierarchyScope = hierarchyScope;
//            Description = description;

//            Nodes = nodes ?? new HashSet<WorkflowSpecificationNode>();
//            Connections = connections ?? new HashSet<WorkflowSpecificationConnection>();
//        }

//        #endregion
//    }
//}