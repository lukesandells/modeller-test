﻿namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	/// Specifies behaviour for the <see cref="EquipmentLevelExtensions.PermittedChildren(EquipmentLevel, PermittedFor)"/>
	/// and <see cref="EquipmentLevelExtensions.PermittedParents(EquipmentLevel, PermittedFor)"/> methods.
	/// </summary>
	public enum PermittedFor
	{
		/// <summary>
		/// Permitted for <see cref="Equipment"/>.
		/// </summary>
		Equipment,

		/// <summary>
		/// Permitted for <see cref="HierarchyScope"/>.
		/// </summary>
		HierarchyScope
	}
}