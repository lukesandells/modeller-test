﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	///     Class PhysicalAssetClass.
	///     NOTE: PhysicalAssetId has been removed from PhysicalAsset Class. Although the PhysicalAsset ID's
	///     are present on the B2MML, the design of this application will ensure that the relationship is uni-directional, with
	///     the physicalAsset
	///     holding reference to the physicalAsset classes only.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
	/// </seealso>
	public class PhysicalAssetClass : HierarchyScopedObject, IResourceObject<PhysicalAssetClass>

    {
		#region  Fields

	    /// <summary>
	    ///     The subclasses
	    /// </summary>
	    public virtual IEnumerable<PhysicalAssetClass> Subclasses => _subclasses;
	    private readonly ISet<PhysicalAssetClass> _subclasses = new HashSet<PhysicalAssetClass>();

	    /// <summary>
	    ///     The superclasses
	    /// </summary>
	    public virtual IEnumerable<PhysicalAssetClass> SuperClasses => _superClasses;
		private readonly ISet<PhysicalAssetClass> _superClasses = new HashSet<PhysicalAssetClass>();

		/// <summary>
		/// All ascendants of the physicalAsset class.
		/// </summary>
		/// <remarks>
		/// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<PhysicalAssetClass> Ascendants => _ascendants;
		private ICollection<PhysicalAssetClass> _ascendants = new Collection<PhysicalAssetClass>();

		/// <summary>
		/// All descendants of the physicalAsset class.
		/// </summary>
		/// <remarks>
		/// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<PhysicalAssetClass> Descendants => _descendants;
		private ICollection<PhysicalAssetClass> _descendants = new Collection<PhysicalAssetClass>();

	    /// <summary>
	    ///     The manufactuers of this physical ass
	    /// </summary>
		private readonly ISet<string> _manufacturers = new HashSet<string>();

        /// <summary>
        ///     Gets the manufacturers.
        /// </summary>
        /// <value>The manufacturers.</value>
        public virtual IEnumerable<string> Manufacturers => _manufacturers;

		/// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<PhysicalAssetClass> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<PhysicalAssetClass> _withPropertiesMixIn;

        /// <summary>
        /// The descendant properties of the physical asset class.
        /// </summary>
        public virtual IEnumerable<BasicProperty<PhysicalAssetClass>> AllProperties => WithPropertiesMixIn.AllProperties;

        /// <summary>
        /// The properties directly belonging to the physical asset class.
        /// </summary>
        public virtual IEnumerable<BasicProperty<PhysicalAssetClass>> Properties => WithPropertiesMixIn.Properties;

		#endregion

		#region Constructors

		/// <summary>
		///     Initializes a new instance of the <see cref="PhysicalAssetClass" /> class.
		/// </summary>
		protected PhysicalAssetClass()
		{
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<PhysicalAssetClass>(this);
		}

		/// <summary>
		///     Initializes a new instance of the <see cref="PhysicalAssetClass" /> class.
		/// </summary>
		/// <param name="externalId">The identifier.</param>
		/// <param name="hierarchyScope">The hierarchy scope.</param>
		public PhysicalAssetClass(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
        {
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<PhysicalAssetClass>(this);
        }

		#endregion

		#region Members

		/// <summary>
	    /// Returns whether a given physicalAsset class is an ascendant of the physicalAsset class.
	    /// </summary>
	    /// <param name="physicalAssetClass">The physicalAsset class to evaluate as a potential ascendant.</param>
	    /// <returns><c>true</c> if the given physicalAsset class is an ascendant of the physicalAsset class; otherwise <c>false</c>.</returns>
	    public virtual bool IsAscendantOf(PhysicalAssetClass physicalAssetClass)
	    {
		    physicalAssetClass = ArgumentValidation.AssertNotNull(physicalAssetClass, nameof(physicalAssetClass));
		    return physicalAssetClass._ascendants.Contains(this);
	    }

	    /// <summary>
	    /// Returns whether a given physicalAsset class is a descendant of the physicalAsset class.
	    /// </summary>
	    /// <param name="physicalAssetClass">The physicalAsset class to evaluate as a potential descendant.</param>
	    /// <returns><c>true</c> if the given physicalAsset class is a descendant of the physicalAsset class; otherwise <c>false</c>.</returns>
	    public virtual bool IsDescendantOf(PhysicalAssetClass physicalAssetClass)
	    {
		    return _ascendants.Contains(physicalAssetClass);
	    }

	    /// <summary>
	    /// Returns a <see cref="bool"/> stipulating whether the given physicalAsset class can be added as a subclass of the physicalAsset class.
	    /// </summary>
	    /// <param name="physicalAssetClass">The physicalAsset class to evaluate.</param>
	    /// <returns><c>true</c> if the given physicalAsset class can be added as a subclass of the physicalAsset class; otherwise <c>false</c>.</returns>
	    public virtual bool CanAddSubclass(PhysicalAssetClass physicalAssetClass)
	    {
		    physicalAssetClass = ArgumentValidation.AssertNotNull(physicalAssetClass, nameof(physicalAssetClass));
		    return physicalAssetClass != this
		           && !physicalAssetClass.IsDescendantOf(physicalAssetClass)
		           && !physicalAssetClass.IsAscendantOf(physicalAssetClass);
	    }

	    /// <summary>
	    /// Adds the given physicalAssetDefinition class as a subclass of the physicalAssetDefinition class.
	    /// </summary>
	    /// <param name="subclass">The physicalAssetDefinition class to add as a subclass.</param>
	    public virtual void AddSubclass(PhysicalAssetClass subclass)
	    {
		    if (!CanAddSubclass(subclass))
		    {
			    throw new InvalidOperationException(ExceptionMessage.CannotAddPhysicalAssetSubclass);
		    }

		    ObjectHierarchy.ManyToMany.AddElement(this, subclass,
			    getChildrenFunc: (pac) => pac._subclasses,
			    getParentsFunc: (pac) => pac._superClasses,
			    getAscendantsFunc: (pac) => pac._ascendants,
			    getDescendantsFunc: (pac) => pac._descendants);
	    }

	    /// <summary>
	    /// Removes the given physicalAssetDefinition subclass from the physicalAssetDefinition class.
	    /// </summary>
	    /// <param name="subclass">The subclass to remove.</param>
	    public virtual void RemoveSubclass(PhysicalAssetClass subclass)
	    {
		    ObjectHierarchy.ManyToMany.RemoveElement(this, subclass,
			    getChildrenFunc: (pac) => pac._subclasses,
			    getParentsFunc: (pac) => pac._superClasses,
			    getAscendantsFunc: (pac) => pac._ascendants,
			    getDescendantsFunc: (pac) => pac._descendants);
	    }

		/// <summary>
		/// Adds a new property to the phyasical asset class.
		/// Returns whether the phsyical asset class has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the phsyical asset class has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
		{
			return WithPropertiesMixIn.HasPropertyWithId(externalId);
		}
        /// <summary>
        /// Adds a new property to the phsyical asset class under a given parent property.
        /// </summary>
        /// <param name="externalId">The external ID of the new property.</param>
        /// <param name="value">The value of the new property.</param>
        /// <param name="parentProperty">The parent property under which to add the new property.</param>
        public virtual BasicProperty<PhysicalAssetClass> AddProperty(string externalId, Value value = null, BasicProperty<PhysicalAssetClass> parentProperty = null)
		{
			return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
		}

		/// <summary>
		/// Removes the given property from this property.
		/// </summary>
		/// <param name="property">The property to remove.</param>
		public virtual void RemoveProperty(BasicProperty<PhysicalAssetClass> property)
		{
			WithPropertiesMixIn.RemoveProperty(property);
		}

        /// <summary>
        /// Adds a manufacturer of this Phyiscal Asset Class
        /// </summary>
        /// <param name="manufacturer">The manufacturer to add</param>
        public virtual void AddManufacturer(string manufacturer)
        {
            _manufacturers.Add(manufacturer);
        }

        /// <summary>
        /// Removes a manufacturer of this Phyiscal Asset Class
        /// </summary>
        /// <param name="manufacturer">The manufacturer to remove</param>
        public virtual void RemoveManufacturer(string manufacturer)
        {
            _manufacturers.Remove(manufacturer);
        }

        #endregion
    }
}