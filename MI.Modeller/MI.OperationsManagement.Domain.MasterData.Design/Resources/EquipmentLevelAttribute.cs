﻿using System;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	/// Apply to members of the <see cref="EquipmentLevel"/> enumeration to specify parents and specialisations.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class EquipmentLevelAttribute : Attribute
	{
		/// <summary>
		/// The specified parents of the <see cref="EquipmentLevel"/>. If the equipment level is a specialisation of another equipment level
		/// and specifies no parents, it inherits the parents of the equipment level it specialises.
		/// </summary>
		public EquipmentLevel[] SpecifiedParents { get; set; } = new EquipmentLevel[] { };

		/// <summary>
		/// Indicates whether the <see cref="EquipmentLevel"/> specifies parents.
		/// </summary>
		public bool SpecifiesParents => this.SpecifiedParents.Length > 0;

		/// <summary>
		/// Indicates the <see cref="EquipmentLevel"/> is a speciialisation of the given equipment level.
		/// </summary>
		public EquipmentLevel SpecialisationOf { get => _specialisationOf.Value; set => _specialisationOf = value; }
		private EquipmentLevel? _specialisationOf;

		/// <summary>
		/// Indicates whether the <see cref="EquipmentLevel"/> is a specialisation of another equipment level.
		/// </summary>
		public bool IsSpecialisation => _specialisationOf.HasValue;

		/// <summary>
		/// Indicates whether parents are inherited from any equipment level this equipment level specialises.
		/// </summary>
		public bool InheritsParents { get; set; }

		/// <summary>
		/// Indicates whether children are inherited from any equipment level this equipment level specialises.
		/// </summary>
		public bool InheritsChildren { get; set; }

		/// <summary>
		/// Indicates whether the <see cref="EquipmentLevel"/> is permitted for hierarchy scopes.
		/// </summary>
		public bool PermittedForHierarchyScopes { get; set; } = true;
	}
}
