﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	///     Class MaterialDefinition.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.IComplexHierarchy{MI.Modeller.Domain.Client.MaterialDefinition}"</cref>
	/// </seealso>
	public class MaterialDefinition : HierarchyScopedObject, IResourceObject<MaterialDefinition>, IAssembly<MaterialDefinition>, IWithAssemblyMixIn<MaterialDefinition>

    {
		#region  Fields

		/// <summary>
	    /// Mix-in providing assembly behaviour.
	    /// </summary>
	    public virtual AssemblyMixIn<MaterialDefinition> AssemblyMixIn => _assemblyMixIn;
	    private AssemblyMixIn<MaterialDefinition> _assemblyMixIn;

	    /// <summary>
	    /// The material Definitiones contained within the material Definition assembly.
	    /// </summary>
	    public virtual IEnumerable<MaterialDefinition> AssemblyElements => AssemblyMixIn.AssemblyElements;

	    /// <summary>
	    /// The material Definitiones assemblies containing the material Definition.
	    /// </summary>
	    public virtual IEnumerable<MaterialDefinition> ParentAssemblies => AssemblyMixIn.ParentAssemblies;

	    /// <summary>
	    /// Indicates whether the relationship between elements of the assembly is permanent or transient.
	    /// </summary>
	    public virtual ExtensibleEnum<AssemblyRelationship>? AssemblyRelationship
	    {
		    get => AssemblyMixIn.AssemblyRelationship;
		    set => AssemblyMixIn.AssemblyRelationship = value;
	    }

	    /// <summary>
	    /// Indicates whether the assembly is a physical object containing its elements, or if the assembly is a logical grouping of elements
	    /// that do not together form a larger object.
	    /// </summary>
	    public virtual ExtensibleEnum<AssemblyType>? AssemblyType { get => AssemblyMixIn.AssemblyType; set => AssemblyMixIn.AssemblyType = value; }

	    /// <summary>
	    /// Indicates whether the material Definition is a root material Definition (i.e. has no parents).
	    /// </summary>
	    public virtual bool IsRootAssembly => AssemblyMixIn.IsRootAssembly;

		/// <summary>
		///     The material classes
		/// </summary>
		private readonly ISet<MaterialClass> _materialClasses = new HashSet<MaterialClass>();

        /// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<MaterialDefinition> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<MaterialDefinition> _withPropertiesMixIn;

        /// <summary>
        /// The descendant properties of the material definition.
        /// </summary>
        public virtual IEnumerable<BasicProperty<MaterialDefinition>> AllProperties => WithPropertiesMixIn.AllProperties;

        /// <summary>
        /// The properties directly belonging to the material definition.
        /// </summary>
        public virtual IEnumerable<BasicProperty<MaterialDefinition>> Properties => WithPropertiesMixIn.Properties;

        /// <summary>
        ///     Gets the material class ids.
        /// </summary>
        /// <value>The material class ids.</value>
        public virtual IEnumerable<MaterialClass> MaterialClasses => _materialClasses;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="MaterialDefinition" /> class.
		/// </summary>
		public MaterialDefinition()
		{
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<MaterialDefinition>(this);
			_assemblyMixIn = new AssemblyMixIn<MaterialDefinition>(this);
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="MaterialDefinition" /> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material definition.</param>
		/// <param name="externalId">The external identifierr of the new material definition.</param>
		public MaterialDefinition(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
        {
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<MaterialDefinition>(this);
	        _assemblyMixIn = new AssemblyMixIn<MaterialDefinition>(this);
		}

		#endregion

		#region Members

		/// <summary>
	    /// Adds a material Definition to the assembly.
	    /// </summary>
	    /// <param name="element">The material Definition to add.</param>
	    public virtual void AddAssemblyElement(MaterialDefinition element)
	    {
		    AssemblyMixIn.AddAssemblyElement(element);
	    }

	    /// <summary>
	    /// Removes an material Definition from the assembly.
	    /// </summary>
	    /// <param name="element">The material Definition to remove.</param>
	    public virtual void RemoveAssemblyElement(MaterialDefinition element)
	    {
		    AssemblyMixIn.RemoveAssemblyElement(element);
	    }

	    /// <summary>
	    /// Returns whether a given material definition is an ascendant of the material definition.
	    /// </summary>
	    /// <param name="materialDefinition">The material definition to evaluate as a potential ascendant.</param>
	    /// <returns><c>true</c> if the given material definition is an ascendant of the material definition; otherwise <c>false</c>.</returns>
	    public virtual bool IsAssemblyAscendantOf(MaterialDefinition materialDefinition)
	    {
		    materialDefinition = ArgumentValidation.AssertNotNull(materialDefinition, nameof(materialDefinition));
		    return _assemblyMixIn.IsAssemblyAscendantOf(materialDefinition);
	    }

	    /// <summary>
	    /// Returns whether a given material definition is a descendant of the material definition.
	    /// </summary>
	    /// <param name="materialDefinition">The material definition to evaluate as a potential descendant.</param>
	    /// <returns><c>true</c> if the given material definition is a descendant of the material definition; otherwise <c>false</c>.</returns>
	    public virtual bool IsAssemblyDescendantOf(MaterialDefinition materialDefinition)
	    {
		    return _assemblyMixIn.IsAssemblyDescendantOf(materialDefinition);
	    }

		/// <summary>
		/// Returns a <see cref="Boolean"/> stipulating whether the given material definition can be added as a subdefinition of the material definition.
		/// </summary>
		/// <param name="materialDefinition">The material definition to evaluate.</param>
		/// <returns><c>true</c> if the given material definition can be added as a subdefinition of the material definition; otherwise <c>false</c>.</returns>
		public virtual bool CanAdd(MaterialDefinition materialDefinition)
		{
			materialDefinition = ArgumentValidation.AssertNotNull(materialDefinition, nameof(materialDefinition));
			return _assemblyMixIn.CanAdd(materialDefinition);
		}

		/// <summary>
		/// Adds this material to the given <see cref="MaterialClass"/>.
		/// </summary>
		/// <param name="materialClass">The material class to which to add the material.</param>
		public virtual void AddToClass(MaterialClass materialClass)
		{
			AddToClass(materialClass, true, false);
		}

		/// <summary>
		/// Adds this material to the given <see cref="MaterialClass"/>.
		/// </summary>
		/// <param name="materialClass">The material class to which to add the material.</param>
		/// <param name="addMaterialToClass">Stipulates whether to add the material to the given material class.</param>
		/// <param name="isPrevalidated">Stipulates whether the operation has been prevalidated and therefore whether validation
		/// shall be performed before the operation is performed.</param>
		protected internal virtual void AddToClass(MaterialClass materialClass, bool addMaterialToClass, bool isPrevalidated)
		{
			materialClass = ArgumentValidation.AssertNotNull(materialClass, nameof(materialClass));
			if (!isPrevalidated && !materialClass.CanAddMember(this))
			{
				throw new InvalidOperationException(ExceptionMessage.CannotAddMaterialDefinitionToMaterialClass);
			}

			if (addMaterialToClass)
			{
				materialClass.AddMember(this, false, true);
			}

			_materialClasses.Add(materialClass);
		}

		/// <summary>
		///     Removes the material class.
		/// </summary>
		/// <param name="materialClass">The material class.</param>
		public virtual void RemoveFromMaterialClass(MaterialClass materialClass)
        {
            materialClass = ArgumentValidation.AssertNotNull(materialClass,
                nameof(materialClass));
            if (_materialClasses.Contains(materialClass))
            {
                _materialClasses.Remove(materialClass);
            }
            if (materialClass.Members.Contains(this))
            {
                materialClass.RemoveMember(this);
            }
        }

        /// <summary>
        /// Adds a new property to the material definition.
        /// Returns whether the material class has a property with a given external ID.
        /// </summary>
        /// <param name="externalId">The external ID for which to search.</param>
        /// <returns><c>true</c> if the material class has a property with the given external ID; otherwise <c>false</c>.</returns>
        public virtual bool HasPropertyWithId(string externalId)
        {
            return WithPropertiesMixIn.HasPropertyWithId(externalId);
        }

        /// <summary>
        /// Adds a new property to the material definition under a given parent property.
        /// </summary>
        /// <param name="externalId">The external ID of the new property.</param>
        /// <param name="value">The value of the new property.</param>
        /// <param name="parentProperty">The parent property under which to add the new property.</param>
        public virtual BasicProperty<MaterialDefinition> AddProperty(string externalId, Value value = null, BasicProperty<MaterialDefinition> parentProperty = null)
        {
            return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
        }

        /// <summary>
        /// Removes the given property from this property.
        /// </summary>
        /// <param name="property">The property to remove.</param>
        public virtual void RemoveProperty(BasicProperty<MaterialDefinition> property)
        {
            WithPropertiesMixIn.RemoveProperty(property);
        }

        #endregion
    }
}