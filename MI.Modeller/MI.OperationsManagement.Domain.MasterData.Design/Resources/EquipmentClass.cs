﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	/// A class of equipment.
	/// </summary>
	public class EquipmentClass : HierarchyScopedObject, IResourceObject<EquipmentClass>
    {
		/// <summary>
		/// The equipment subclasses belonging to the equipment class.
		/// </summary>
		public virtual IEnumerable<EquipmentClass> Subclasses => _subclasses;
		private readonly ISet<EquipmentClass> _subclasses = new HashSet<EquipmentClass>();

		/// <summary>
		/// The equipment classes to which the equipment class belongs.
		/// </summary>
		public virtual IEnumerable<EquipmentClass> SuperClasses => _superclasses;
		private readonly ISet<EquipmentClass> _superclasses = new HashSet<EquipmentClass>();

		/// <summary>
		/// All ascendants of the equipment class.
		/// </summary>
		/// <remarks>
		/// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<EquipmentClass> Ascendants => _ascendants;
		private ICollection<EquipmentClass> _ascendants = new Collection<EquipmentClass>();

		/// <summary>
		/// All descendants of the equipment class.
		/// </summary>
		/// <remarks>
		/// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<EquipmentClass> Descendants => _descendants;
		private ICollection<EquipmentClass> _descendants = new Collection<EquipmentClass>();

		/// <summary>
		/// The equipment members of the equipment class.
		/// </summary>
		public virtual IEnumerable<Equipment> Members => _members;
		private readonly ISet<Equipment> _members = new HashSet<Equipment>();

		/// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<EquipmentClass> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<EquipmentClass> _withPropertiesMixIn;

		/// <summary>
		/// The descendant properties of the equipment class.
		/// </summary>
		public virtual IEnumerable<BasicProperty<EquipmentClass>> AllProperties => WithPropertiesMixIn.AllProperties;

        /// <summary>
        /// The properties directly belonging to the equipment class.
        /// </summary>
        public virtual IEnumerable<BasicProperty<EquipmentClass>> Properties => WithPropertiesMixIn.Properties;

		/// <summary>
		/// The equipment level of the equipment class.
		/// </summary>
		public virtual ExtensibleEnum<EquipmentLevel> EquipmentLevel { get => _equipmentLevel; set => _equipmentLevel = value; }
		private ExtensibleEnum<EquipmentLevel> _equipmentLevel;
		
        /// <summary>
        /// Initialises a new instance of the <see cref="EquipmentClass"/> class.
        /// </summary>
        protected EquipmentClass()
		{
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<EquipmentClass>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="EquipmentClass" /> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment class..</param>
		/// <param name="externalId">The external identifier of the new equipmet class.</param>
		/// <param name="equipmentLevel">The euipment level of the new equipment class.</param>
		public EquipmentClass(HierarchyScope hierarchyScope, string externalId, ExtensibleEnum<EquipmentLevel> equipmentLevel) 
			: base(hierarchyScope, externalId)
        {
			_equipmentLevel = equipmentLevel;
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<EquipmentClass>(this);
        }
		
		/// <summary>
		/// Returns whether a given equipment class is an ascendant of the equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given equipment class is an ascendant of the equipment class; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(EquipmentClass equipmentClass)
		{
			equipmentClass = ArgumentValidation.AssertNotNull(equipmentClass, nameof(equipmentClass));
			return equipmentClass._ascendants.Contains(this);
		}

		/// <summary>
		/// Returns whether a given equipment class is a descendant of the equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given equipment class is a descendant of the equipment class; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(EquipmentClass equipmentClass)
		{
			return _ascendants.Contains(equipmentClass);
		}

		/// <summary>
		/// Returns a <see cref="Boolean"/> stipulating whether the given equipment class can be added as a subclass of the 
		/// equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class to evaluate.</param>
		/// <returns><c>true</c> if the given equipment class can be added as a subclass of the equipment class; otherwise <c>false</c>.</returns>
		public virtual bool CanAddSubclass(EquipmentClass equipmentClass)
		{
			equipmentClass = ArgumentValidation.AssertNotNull(equipmentClass, nameof(equipmentClass));
			return equipmentClass != this 
				&& !IsDescendantOf(equipmentClass) 
				&& !Subclasses.Contains(equipmentClass)
				&& equipmentClass.EquipmentLevel == EquipmentLevel;
		}

		/// <summary>
		/// Adds the given equipment class as a subclass of the equipment class.
		/// </summary>
		/// <param name="subclass">The equipment class to add as a subclass.</param>
		public virtual void AddSubclass(EquipmentClass subclass)
        {
			if (!CanAddSubclass(subclass))
			{
				throw new InvalidOperationException(ExceptionMessage.CannotAddEquipmentSubclass);
			}

            ObjectHierarchy.ManyToMany.AddElement(this, subclass, 
				getChildrenFunc: (ec) => ec._subclasses, 
				getParentsFunc: (ec) => ec._superclasses, 
				getAscendantsFunc: (ec) => ec._ascendants,
				getDescendantsFunc: (ec) => ec._descendants);
        }

        /// <summary>
        /// Removes the given equipment subclass from the equipment class.
        /// </summary>
        /// <param name="subclass">The subclass to remove.</param>
        public virtual void RemoveSubclass(EquipmentClass subclass)
        {
            ObjectHierarchy.ManyToMany.RemoveElement(this, subclass, 
				getChildrenFunc: (ec) => ec._subclasses, 
				getParentsFunc: (ec) => ec._superclasses, 
				getAscendantsFunc: (ec) => ec._ascendants,
				getDescendantsFunc: (ec) => ec._descendants);
        }

		/// <summary>
		/// Returns whether the equipment can be added to the given equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class to evaluate.</param>
		/// <returns><c>true</c> if the equipment can be added to the given equipment class; otherwise <c>false</c>.</returns>
		public virtual bool CanAddMember(Equipment equipment)
		{
			equipment = ArgumentValidation.AssertNotNull(equipment, nameof(equipment));
			return !Members.Contains(equipment) && equipment.EquipmentLevel == EquipmentLevel;
		}

		/// <summary>
		/// Adds an equipment member to the equipment class.
		/// </summary>
		/// <param name="equipment">The equipment to add to the equipment class.</param>
		public virtual void AddMember(Equipment equipment)
		{
			AddMember(equipment, true, false);
		}

		/// <summary>
		/// Adds an equipment member to the equipment class.
		/// </summary>
		/// <param name="equipment">The equipment to add to the equipment class.</param>
		/// <param name="addClassToEquipment">Stipulates whether to add the equipment class to the given equipment.</param>
		/// <param name="isPrevalidated">Stipulates whether the operation has been prevalidated and therefore whether validation
		/// shall be performed before the operation is performed.</param>
		protected internal virtual void AddMember(Equipment equipment, bool addClassToEquipment, bool isPrevalidated)
        {
			equipment = ArgumentValidation.AssertNotNull(equipment, nameof(equipment));
            if (!isPrevalidated && !CanAddMember(equipment))
			{
				throw new ArgumentException(ExceptionMessage.CannotAddEquipmentToEquipmentClass, nameof(equipment));
			}

			if (addClassToEquipment)
			{
				equipment.AddToClass(this, false, true);
			}

			_members.Add(equipment);
        }

        /// <summary>
        /// Removes an existing equipment member from the equipment class.
        /// </summary>
        /// <param name="equipment">The existing equipment member to remove from the equipment class.</param>
        public virtual void RemoveMember(Equipment equipment)
        {
            if (!_members.Contains(equipment))
            {
				throw new InvalidOperationException(ExceptionMessage.ItemNotFoundInCollection);
            }

            _members.Remove(equipment);
            equipment.RemoveFromClass(this, false);
        }

        /// <summary>
        /// Adds a new property to the equipment class.
        /// Returns whether the equipment class has a property with a given external ID.
        /// </summary>
        /// <param name="externalId">The external ID for which to search.</param>
        /// <returns><c>true</c> if the equipment class has a property with the given external ID; otherwise <c>false</c>.</returns>
        public virtual bool HasPropertyWithId(string externalId)
        {
            return WithPropertiesMixIn.HasPropertyWithId(externalId);
        }

        /// <summary>
        /// Adds a new property to the equipment class under a given parent property.
        /// </summary>
        /// <param name="externalId">The external ID of the new property.</param>
        /// <param name="value">The value of the new property.</param>
        /// <param name="parentProperty">The parent property under which to add the new property.</param>
        public virtual BasicProperty<EquipmentClass> AddProperty(string externalId, Value value = null, BasicProperty<EquipmentClass> parentProperty = null)
        {
            return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
        }

        /// <summary>
        /// Removes the given property from this property.
        /// </summary>
        /// <param name="property">The property to remove.</param>
        public virtual void RemoveProperty(BasicProperty<EquipmentClass> property)
        {
            WithPropertiesMixIn.RemoveProperty(property);
        }
    }
}