﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	///     Class MaterialClass.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.IComplexHierarchy{MI.Modeller.Domain.Client.MaterialClass}"</cref>
	/// </seealso>
	public class MaterialClass : HierarchyScopedObject, IResourceObject<MaterialClass>, IAssembly<MaterialClass>, IWithAssemblyMixIn<MaterialClass>
    {
		#region  Fields

		/// <summary>
	    /// Mix-in providing assembly behaviour.
	    /// </summary>
	    public virtual AssemblyMixIn<MaterialClass> AssemblyMixIn => _assemblyMixIn;
	    private AssemblyMixIn<MaterialClass> _assemblyMixIn;

	    /// <summary>
	    /// The material classes contained within the material class assembly.
	    /// </summary>
	    public virtual IEnumerable<MaterialClass> AssemblyElements => AssemblyMixIn.AssemblyElements;

		/// <summary>
		/// The material classes assemblies containing the material class.
		/// </summary>
		public virtual IEnumerable<MaterialClass> ParentAssemblies => AssemblyMixIn.ParentAssemblies;

	    /// <summary>
	    /// Indicates whether the relationship between elements of the assembly is permanent or transient.
	    /// </summary>
	    public virtual ExtensibleEnum<AssemblyRelationship>? AssemblyRelationship
	    {
		    get => AssemblyMixIn.AssemblyRelationship;
		    set => AssemblyMixIn.AssemblyRelationship = value;
	    }

	    /// <summary>
	    /// Indicates whether the assembly is a physical object containing its elements, or if the assembly is a logical grouping of elements
	    /// that do not together form a larger object.
	    /// </summary>
	    public virtual ExtensibleEnum<AssemblyType>? AssemblyType { get => AssemblyMixIn.AssemblyType; set => AssemblyMixIn.AssemblyType = value; }

		/// <summary>
		/// Indicates whether the material class is a root material class (i.e. has no parents).
		/// </summary>
		public virtual bool IsRootAssembly => AssemblyMixIn.IsRootAssembly;

	    /// <summary>
	    ///     The subclasses
	    /// </summary>
	    public virtual IEnumerable<MaterialClass> Subclasses => _subclasses;
		private readonly ISet<MaterialClass> _subclasses = new HashSet<MaterialClass>();

	    /// <summary>
	    ///     The superclasses
	    /// </summary>
	    public virtual IEnumerable<MaterialClass> SuperClasses => _superclasses;
		private readonly ISet<MaterialClass> _superclasses = new HashSet<MaterialClass>();

	    /// <summary>
	    /// All ascendants of the material class.
	    /// </summary>
	    /// <remarks>
	    /// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
	    /// </remarks>
	    public virtual IEnumerable<MaterialClass> Ascendants => _ascendants;
		private ICollection<MaterialClass> _ascendants = new Collection<MaterialClass>();

	    /// <summary>
	    /// All descendants of the material class.
	    /// </summary>
	    /// <remarks>
	    /// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
	    /// </remarks>
	    public virtual IEnumerable<MaterialClass> Descendants => _descendants;
		private ICollection<MaterialClass> _descendants = new Collection<MaterialClass>();

		/// <summary>
		/// The material definitions belonging to the material class.
		/// </summary>
		private readonly ISet<MaterialDefinition> _members = new HashSet<MaterialDefinition>();

		/// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<MaterialClass> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<MaterialClass> _withPropertiesMixIn;

	    /// <summary>
	    /// The descendant properties of the materialDefinition class.
	    /// </summary>
	    public virtual IEnumerable<BasicProperty<MaterialClass>> AllProperties => WithPropertiesMixIn.AllProperties;

	    /// <summary>
	    /// The properties directly belonging to the materialDefinition class.
	    /// </summary>
	    public virtual IEnumerable<BasicProperty<MaterialClass>> Properties => WithPropertiesMixIn.Properties;

        /// <summary>
        ///     Gets the materialDefinition class properties.
        /// </summary>
        //public virtual IList<Property<MaterialClass>> MaterialClassProperties => _materialClassProperties;

        /// <summary>
        ///     The Material Definitions which implement this class
        /// </summary>
        public virtual IEnumerable<MaterialDefinition> Members => _members;

        /// <summary>
        ///     Gets the superclass.
        /// </summary>
        /// <value>The superclass.</value>
        public virtual IEnumerable<MaterialClass> Superclasses => _superclasses;

        #endregion

        #region Constructors

        /// <summary>
        ///     Parameterless constructor for persistence
        /// </summary>
        protected MaterialClass()
        {
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<MaterialClass>(this);
	        _assemblyMixIn = new AssemblyMixIn<MaterialClass>(this);
		}

        /// <summary>
        /// Initializes a new instance of the <see cref="MaterialClass" /> class.
        /// </summary>
        /// <param name="externalId">The identifier of the new materialDefinition class.</param>
        /// <param name="hierarchyScope">The hierarchy scope of the new materialDefinition class.</param>
        public MaterialClass(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
        {
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<MaterialClass>(this);
	        _assemblyMixIn = new AssemblyMixIn<MaterialClass>(this);
		}

		#endregion

		#region Members

	    /// <summary>
	    /// Adds a material class to the assembly.
	    /// </summary>
	    /// <param name="element">The material class to add.</param>
	    public virtual void AddAssemblyElement(MaterialClass element)
	    {
		    AssemblyMixIn.AddAssemblyElement(element);
	    }

	    /// <summary>
	    /// Removes an material class from the assembly.
	    /// </summary>
	    /// <param name="element">The material class to remove.</param>
	    public virtual void RemoveAssemblyElement(MaterialClass element)
	    {
		    AssemblyMixIn.RemoveAssemblyElement(element);
	    }

	    /// <summary>
	    /// Returns whether a given material class is an ascendant of the material class.
	    /// </summary>
	    /// <param name="materialClass">The material class to evaluate as a potential ascendant.</param>
	    /// <returns><c>true</c> if the given material class is an ascendant of the material class; otherwise <c>false</c>.</returns>
	    public virtual bool IsAscendantOf(MaterialClass materialClass)
	    {
		    materialClass = ArgumentValidation.AssertNotNull(materialClass, nameof(materialClass));
		    return materialClass._ascendants.Contains(this);
	    }

		/// <summary>
		/// Returns whether a given material class is an assembly ascendant of the material class.
		/// </summary>
		/// <param name="materialClass">The material class to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given material class is an ascendant of the material class; otherwise <c>false</c>.</returns>
		public virtual bool IsAssemblyAscendantOf(MaterialClass materialClass)
	    {
		    materialClass = ArgumentValidation.AssertNotNull(materialClass, nameof(materialClass));
		    return _assemblyMixIn.IsAssemblyAscendantOf(materialClass);
	    }

		/// <summary>
		/// Returns whether a given material class is a descendant of the material class.
		/// </summary>
		/// <param name="materialClass">The material class to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given material class is a descendant of the material class; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(MaterialClass materialClass)
		{
			return _ascendants.Contains(materialClass);
		}

	    /// <summary>
	    /// Returns whether a given material class is an assembly descendant of the material class.
	    /// </summary>
	    /// <param name="materialClass">The material class to evaluate as a potential descendant.</param>
	    /// <returns><c>true</c> if the given material class is a descendant of the material class; otherwise <c>false</c>.</returns>
	    public virtual bool IsAssemblyDescendantOf(MaterialClass materialClass)
	    {
		    return _assemblyMixIn.IsAssemblyDescendantOf(materialClass);
	    }

		/// <summary>
		/// Returns a <see cref="Boolean"/> stipulating whether the given material class can be added as a subclass of the material class.
		/// </summary>
		/// <param name="materialClass">The material class to evaluate.</param>
		/// <returns><c>true</c> if the given material class can be added as a subclass of the material class; otherwise <c>false</c>.</returns>
		public virtual bool CanAdd(MaterialClass materialClass)
	    {
		    materialClass = ArgumentValidation.AssertNotNull(materialClass, nameof(materialClass));
		    return materialClass != this
		           && !materialClass.IsDescendantOf(materialClass)
		           && !materialClass.IsAscendantOf(materialClass)
				   && _assemblyMixIn.CanAdd(materialClass);
	    }

		/// <summary>
		/// Adds the given materialDefinition class as a subclass of the materialDefinition class.
		/// </summary>
		/// <param name="subclass">The materialDefinition class to add as a subclass.</param>
		public virtual void AddSubclass(MaterialClass subclass)
	    {
		    if (!CanAdd(subclass))
		    {
			    throw new InvalidOperationException(ExceptionMessage.CannotAddMaterialSubclass);
		    }

		    ObjectHierarchy.ManyToMany.AddElement(this, subclass,
			    getChildrenFunc: (mc) => mc._subclasses,
			    getParentsFunc: (mc) => mc._superclasses,
			    getAscendantsFunc: (mc) => mc._ascendants,
			    getDescendantsFunc: (mc) => mc._descendants);
	    }

	    /// <summary>
	    /// Removes the given materialDefinition subclass from the materialDefinition class.
	    /// </summary>
	    /// <param name="subclass">The subclass to remove.</param>
	    public virtual void RemoveSubclass(MaterialClass subclass)
	    {
		    ObjectHierarchy.ManyToMany.RemoveElement(this, subclass,
			    getChildrenFunc: (mc) => mc._subclasses,
			    getParentsFunc: (mc) => mc._superclasses,
			    getAscendantsFunc: (mc) => mc._ascendants,
			    getDescendantsFunc: (mc) => mc._descendants);
	    }

		/// <summary>
		/// Returns whether the material can be added to the given material class.
		/// </summary>
		/// <param name="material">The material to evaluate.</param>
		/// <returns><c>true</c> if the material can be added to the given material class; otherwise <c>false</c>.</returns>
		public virtual bool CanAddMember(MaterialDefinition material)
		{
			material = ArgumentValidation.AssertNotNull(material, nameof(material));
			return !Members.Contains(material);
		}

		/// <summary>
		/// Adds an material member to the material class.
		/// </summary>
		/// <param name="material">The material to add to the material class.</param>
		public virtual void AddMember(MaterialDefinition material)
		{
			AddMember(material, true, false);
		}

		/// <summary>
		/// Adds an material member to the material class.
		/// </summary>
		/// <param name="material">The material to add to the material class.</param>
		/// <param name="addClassToMaterial">Stipulates whether to add the material class to the given material.</param>
		/// <param name="isPrevalidated">Stipulates whether the operation has been prevalidated and therefore whether validation
		/// shall be performed before the operation is performed.</param>
		protected internal virtual void AddMember(MaterialDefinition material, bool addClassToMaterial, bool isPrevalidated)
		{
			material = ArgumentValidation.AssertNotNull(material, nameof(material));
			if (!isPrevalidated && !CanAddMember(material))
			{
				throw new ArgumentException(ExceptionMessage.CannotAddMaterialDefinitionToMaterialClass, nameof(material));
			}

			if (addClassToMaterial)
			{
				material.AddToClass(this, false, true);
			}

			_members.Add(material);
		}

		/// <summary>
		/// Removes an existing materialDefinition member from the materialDefinition class.
		/// </summary>
		/// <param name="material">The existing materialDefinition member to remove from the materialDefinition class.</param>
		public virtual void RemoveMember(MaterialDefinition material)
	    {
		    if (!_members.Contains(material))
		    {
			    throw new InvalidOperationException(ExceptionMessage.ItemNotFoundInCollection);
		    }

		    _members.Remove(material);
		    material.RemoveFromMaterialClass(this);
	    }

		/// <summary>
		/// Adds a new property to the materialDefinition class class.
		/// Returns whether the materialDefinition class has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the materialDefinition class has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
	    {
	        return WithPropertiesMixIn.HasPropertyWithId(externalId);
	    }

	    /// <summary>
	    /// Adds a new property to the materialDefinition class under a given parent property.
	    /// </summary>
	    /// <param name="externalId">The external ID of the new property.</param>
	    /// <param name="value">The value of the new property.</param>
	    /// <param name="parentProperty">The parent property under which to add the new property.</param>
	    public virtual BasicProperty<MaterialClass> AddProperty(string externalId, Value value = null, BasicProperty<MaterialClass> parentProperty = null)
	    {
	        return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
	    }

	    /// <summary>
	    /// Removes the given property from this property.
	    /// </summary>
	    /// <param name="property">The property to remove.</param>
	    public virtual void RemoveProperty(BasicProperty<MaterialClass> property)
	    {
	        WithPropertiesMixIn.RemoveProperty(property);
	    }

        #endregion
    }
}