﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	///     Class PersonnelClass.
	///     NOTE: PersonID has been removed from Personnel Class. Although the Personnel ID's
	///     are present on the B2MML, the design of this application will ensure that the relationship is uni-directional, with
	///     the personnel
	///     holding reference to the personnel classes only.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
	/// </seealso>
	public class PersonnelClass : HierarchyScopedObject, IResourceObject<PersonnelClass>

    {
		#region  Fields

	    /// <summary>
	    ///     The subclasses
	    /// </summary>
	    public virtual IEnumerable<PersonnelClass> Subclasses => _subclasses;
	    private readonly ISet<PersonnelClass> _subclasses = new HashSet<PersonnelClass>();

	    /// <summary>
	    ///     The superclasses
	    /// </summary>
	    public virtual IEnumerable<PersonnelClass> SuperClasses => _superclasses;
		private readonly ISet<PersonnelClass> _superclasses = new HashSet<PersonnelClass>();

	    /// <summary>
	    /// All ascendants of the personnel class.
	    /// </summary>
	    /// <remarks>
	    /// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
	    /// </remarks>
	    public virtual IEnumerable<PersonnelClass> Ascendants => _ascendants;
		private ICollection<PersonnelClass> _ascendants = new Collection<PersonnelClass>();

	    /// <summary>
	    /// All descendants of the personnel class.
	    /// </summary>
	    /// <remarks>
	    /// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
	    /// </remarks>
	    public virtual IEnumerable<PersonnelClass> Descendants => _descendants;
		private ICollection<PersonnelClass> _descendants = new Collection<PersonnelClass>();

		/// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<PersonnelClass> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<PersonnelClass> _withPropertiesMixIn;

        /// <summary>
        /// The descendant properties of the personnel class.
        /// </summary>
        public virtual IEnumerable<BasicProperty<PersonnelClass>> AllProperties => WithPropertiesMixIn.AllProperties;

        /// <summary>
        /// The properties directly belonging to the personnel class.
        /// </summary>
        public virtual IEnumerable<BasicProperty<PersonnelClass>> Properties => WithPropertiesMixIn.Properties;

        #endregion

        #region Constructors

        /// <summary>
        ///     Parameterless constructor for persistence
        /// </summary>
        protected PersonnelClass()
        {
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<PersonnelClass>(this);
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="PersonnelClass" /> class.
        /// </summary>
        /// <param name="externalId">The identifier.</param>
        /// <param name="hierarchyScope">The hierarchy scope.</param>
        public PersonnelClass(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
        {
	        _withPropertiesMixIn = new WithBasicPropertiesMixIn<PersonnelClass>(this);
		}

		#endregion

		#region Members

		/// <summary>
	    /// Returns whether a given personnel class is an ascendant of the personnel class.
	    /// </summary>
	    /// <param name="personnelClass">The personnel class to evaluate as a potential ascendant.</param>
	    /// <returns><c>true</c> if the given personnel class is an ascendant of the personnel class; otherwise <c>false</c>.</returns>
	    public virtual bool IsAscendantOf(PersonnelClass personnelClass)
	    {
		    personnelClass = ArgumentValidation.AssertNotNull(personnelClass, nameof(personnelClass));
		    return personnelClass._ascendants.Contains(this);
	    }

	    /// <summary>
	    /// Returns whether a given personnel class is a descendant of the personnel class.
	    /// </summary>
	    /// <param name="personnelClass">The personnel class to evaluate as a potential descendant.</param>
	    /// <returns><c>true</c> if the given personnel class is a descendant of the personnel class; otherwise <c>false</c>.</returns>
	    public virtual bool IsDescendantOf(PersonnelClass personnelClass)
	    {
		    return _ascendants.Contains(personnelClass);
	    }

	    /// <summary>
	    /// Returns a <see cref="Boolean"/> stipulating whether the given personnel class can be added as a subclass of the personnel class.
	    /// </summary>
	    /// <param name="personnelClass">The personnel class to evaluate.</param>
	    /// <returns><c>true</c> if the given personnel class can be added as a subclass of the personnel class; otherwise <c>false</c>.</returns>
	    public virtual bool CanAdd(PersonnelClass personnelClass)
	    {
		    personnelClass = ArgumentValidation.AssertNotNull(personnelClass, nameof(personnelClass));
		    return personnelClass != this
		           && !personnelClass.IsDescendantOf(personnelClass)
		           && !personnelClass.IsAscendantOf(personnelClass);
	    }

	    /// <summary>
	    /// Adds the given personnelDefinition class as a subclass of the personnelDefinition class.
	    /// </summary>
	    /// <param name="subclass">The personnelDefinition class to add as a subclass.</param>
	    public virtual void AddSubclass(PersonnelClass subclass)
	    {
		    if (!CanAdd(subclass))
		    {
			    throw new InvalidOperationException(ExceptionMessage.CannotAddPersonnelSubclass);
		    }

		    ObjectHierarchy.ManyToMany.AddElement(this, subclass,
			    getChildrenFunc: (pc) => pc._subclasses,
			    getParentsFunc: (pc) => pc._superclasses,
			    getAscendantsFunc: (pc) => pc._ascendants,
			    getDescendantsFunc: (pc) => pc._descendants);
	    }

	    /// <summary>
	    /// Removes the given personnelDefinition subclass from the personnelDefinition class.
	    /// </summary>
	    /// <param name="subclass">The subclass to remove.</param>
	    public virtual void RemoveSubclass(PersonnelClass subclass)
	    {
		    ObjectHierarchy.ManyToMany.RemoveElement(this, subclass,
			    getChildrenFunc: (pc) => pc._subclasses,
			    getParentsFunc: (pc) => pc._superclasses,
			    getAscendantsFunc: (pc) => pc._ascendants,
			    getDescendantsFunc: (pc) => pc._descendants);
	    }

		/// <summary>
		/// Adds a new property to the personnel class.
		/// Returns whether the personnel class has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the personnel class has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
        {
            return WithPropertiesMixIn.HasPropertyWithId(externalId);
        }

        /// <summary>
        /// Adds a new property to the personnel class under a given parent property.
        /// </summary>
        /// <param name="externalId">The external ID of the new property.</param>
        /// <param name="value">The value of the new property.</param>
        /// <param name="parentProperty">The parent property under which to add the new property.</param>
        public virtual BasicProperty<PersonnelClass> AddProperty(string externalId, Value value = null, BasicProperty<PersonnelClass> parentProperty = null)
        {
            return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
        }

        /// <summary>
        /// Removes the given property from this property.
        /// </summary>
        /// <param name="property">The property to remove.</param>
        public virtual void RemoveProperty(BasicProperty<PersonnelClass> property)
        {
            WithPropertiesMixIn.RemoveProperty(property);
        }

        #endregion
    }
}