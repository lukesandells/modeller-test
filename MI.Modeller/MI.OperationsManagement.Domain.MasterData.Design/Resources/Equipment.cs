﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	/// Represents an element of equipment.
	/// </summary>
	public class Equipment : HierarchyScopedObject, IResourceObject<Equipment>
	{
		/// <summary>
		/// The hierarchy scope for the equipment.
		/// </summary>
		public override HierarchyScope HierarchyScope
		{
			get => base.HierarchyScope;
			set
			{
				// Validate the equipment level of the new hierarchy scope
				if (value != null)
				{
					if (!value.EquipmentLevel.PermitsChild(EquipmentLevel, PermittedFor.Equipment))
					{
						throw new OperationsManagementException(string.Format(ExceptionMessage.Equipment_InvalidHierarchyScopeEquipmentLevel,
							EquipmentLevel, value.EquipmentLevel));
					}

					value.DefiningEquipment.AddChild(this);
				}

				if (base.HierarchyScope != null)
				{
					base.HierarchyScope.DefiningEquipment.RemoveChild(this);
				}

				base.HierarchyScope = value;
			}
		}
		
        /// <summary>
        /// The equipment comprising this equipment.
        /// </summary>
        public virtual IEnumerable<Equipment> Children => _children;
		private readonly ISet<Equipment> _children = new HashSet<Equipment>();

		/// <summary>
		/// All ascendants of the equipment.
		/// </summary>
		/// <remarks>
		/// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<Equipment> Ascendants => _ascendants;
		private ICollection<Equipment> _ascendants = new Collection<Equipment>();

		/// <summary>
		/// All descendants of the equipment.
		/// </summary>
		/// <remarks>
		/// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<Equipment> Descendants => _descendants;
		private ICollection<Equipment> _descendants = new Collection<Equipment>();

		/// <summary>
		/// Indicates whether this equipment defines a hierarchy scope.
		/// </summary>
		public virtual bool DefinesHierarchyScope => HierarchyScope.DefiningEquipment == this;

		/// <summary>
		/// The equipment classes to which this equipment belongs.
		/// </summary>
		public virtual IEnumerable<EquipmentClass> EquipmentClasses => _equipmentClasses;
		private readonly ISet<EquipmentClass> _equipmentClasses = new HashSet<EquipmentClass>();

		/// <summary>
		/// Gets the equipment level.
		/// </summary>
		public virtual ExtensibleEnum<EquipmentLevel> EquipmentLevel { get => _equipmentLevel; set => _equipmentLevel = value;  }
		private ExtensibleEnum<EquipmentLevel> _equipmentLevel;

		/// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<Equipment> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<Equipment> _withPropertiesMixIn;

		/// <summary>
		/// The descendant properties of the equipment.
		/// </summary>
		public virtual IEnumerable<BasicProperty<Equipment>> AllProperties => WithPropertiesMixIn.AllProperties;

		/// <summary>
		/// The properties directly belonging to the equipment.
		/// </summary>
		public virtual IEnumerable<BasicProperty<Equipment>> Properties => WithPropertiesMixIn.Properties;
		
		/// <summary>
		/// Gets the equipment containing the equipment.
		/// </summary>
		public virtual IEnumerable<Equipment> Parents => _parents;
		private readonly ISet<Equipment> _parents = new HashSet<Equipment>();

		/// <summary>
		/// Initialises a new instance of the <see cref="Equipment"/> class.
		/// </summary>
		protected Equipment()
		{
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<Equipment>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Equipment"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment.</param>
		/// <remarks>This constructor is used only to create a hierarchy scope</remarks>
		internal Equipment(HierarchyScope hierarchyScope)
			: base(hierarchyScope)
		{
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<Equipment>(this);
		}

        /// <summary>
        /// Initialises a new instance of the <see cref="Equipment" /> class.
        /// </summary>
        /// <param name="hierarchyScope">The hierarchy scope.</param>
        /// <param name="externalId">The identifier.</param>
        /// <param name="equipmentLevel">The equipment level.</param>
        public Equipment(HierarchyScope hierarchyScope, string externalId, ExtensibleEnum<EquipmentLevel> equipmentLevel) 
			: base(hierarchyScope, externalId)
        {
            _equipmentLevel = equipmentLevel;
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<Equipment>(this);
		}

		/// <summary>
		/// Returns whether a given equipment object is an ascendant of the equipment.
		/// </summary>
		/// <param name="equipment">The equipment to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given equipment object is an ascendant of the equipment; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(Equipment equipment)
		{
			equipment = ArgumentValidation.AssertNotNull(equipment, nameof(equipment));
			return equipment._ascendants.Contains(this);
		}

		/// <summary>
		/// Returns whether a given equipment object is a descendant of the equipment.
		/// </summary>
		/// <param name="equipment">The equipment to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given equipment object is a descendant of the equipment; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(Equipment equipment)
		{
			return _ascendants.Contains(equipment);
		}

		/// <summary>
		/// Returns whether the equipment can be promoted to a hierarchy scope.
		/// </summary>
		/// <returns><c>true</c> if the equipment can be promoted to a hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool CanPromoteToHierarchyScope()
		{
			return HierarchyScope.EquipmentLevel.PermitsChild(EquipmentLevel, PermittedFor.HierarchyScope);
		}

		/// <summary>
		/// Promotes the equipment to a hierarchy scope.
		/// </summary>
		/// <returns>The new hierarchy scope.</returns>
		public virtual HierarchyScope PromoteToHierarchyScope()
		{
			if (!CanPromoteToHierarchyScope())
			{
				throw new InvalidOperationException(ExceptionMessage.CannotAddChildHierarchyScope);
			}
			var hierarchyScope = new HierarchyScope(this);
			HierarchyScope.AddChild(hierarchyScope);
			base.HierarchyScope = hierarchyScope;
			return hierarchyScope;
		}

		/// <summary>
		/// Returns whether the equipment has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the equipment has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
		{
			return WithPropertiesMixIn.HasPropertyWithId(externalId);
		}

		/// <summary>
		/// Adds a new property to the equipment under a given parent property.
		/// </summary>
		/// <param name="externalId">The external ID of the new property.</param>
		/// <param name="value">The value of the new property.</param>
		/// <param name="parentProperty">The parent property under which to add the new property.</param>
		public virtual BasicProperty<Equipment> AddProperty(string externalId, Value value = null, BasicProperty<Equipment> parentProperty = null)
		{
			return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
		}

		/// <summary>
		/// Removes the given property from this property.
		/// </summary>
		/// <param name="property">The property to remove.</param>
		public virtual void RemoveProperty(BasicProperty<Equipment> property)
		{
			WithPropertiesMixIn.RemoveProperty(property);
		}

		/// <summary>
		/// Returns whether the given equipment can be added to the equipment as a child element.
		/// </summary>
		/// <param name="equipment">The equipment to evaluate.</param>
		/// <returns><c>true</c> if the given equipment can be added to the equipment as a child element; otherwise <c>false</c>.</returns>
		public virtual bool CanAddChild(Equipment equipment)
		{
			equipment = ArgumentValidation.AssertNotNull(equipment, nameof(equipment));
			return equipment != this
				&& !IsDescendantOf(equipment)
				&& !Children.Contains(equipment)
				&& EquipmentLevel.PermitsChild(equipment.EquipmentLevel, PermittedFor.Equipment);
		}

		/// <summary>
		/// Adds the given equipment as a child element of the equipment.
		/// </summary>
		/// <param name="equipment">The equipment to add as a child element.</param>
		public virtual void AddChild(Equipment equipment)
        {
			equipment = ArgumentValidation.AssertNotNull(equipment, nameof(equipment));
			if (!CanAddChild(equipment))
			{
				throw new InvalidOperationException(ExceptionMessage.CannotAddEquipmentToEquipment);
			}

			ObjectHierarchy.ManyToMany.AddElement(this, equipment, 
				getChildrenFunc: (e) => e._children, 
				getParentsFunc: (e) => e._parents, 
				getAscendantsFunc: (e) => e._ascendants,
				getDescendantsFunc: (e) => e._descendants);
		}

		/// <summary>
		/// Adds this equipment to the given <see cref="EquipmentClass"/>.
		/// </summary>
		/// <param name="equipmentClass">The equipment class to which to add the equipment.</param>
		public virtual void AddToClass(EquipmentClass equipmentClass)
		{
			AddToClass(equipmentClass, true, false);
		}

		/// <summary>
		/// Adds this equipment to the given <see cref="EquipmentClass"/>.
		/// </summary>
		/// <param name="equipmentClass">The equipment class to which to add the equipment.</param>
		/// <param name="addEquipmentToClass">Stipulates whether to add the equipment to the given equipment class.</param>
		/// <param name="isPrevalidated">Stipulates whether the operation has been prevalidated and therefore whether validation
		/// shall be performed before the operation is performed.</param>
		protected internal virtual void AddToClass(EquipmentClass equipmentClass, bool addEquipmentToClass, bool isPrevalidated)
		{
			equipmentClass = ArgumentValidation.AssertNotNull(equipmentClass, nameof(equipmentClass));
			if (!isPrevalidated && !equipmentClass.CanAddMember(this))
			{
				throw new InvalidOperationException(ExceptionMessage.CannotAddEquipmentToEquipmentClass);
			}

			if (addEquipmentToClass)
			{
				equipmentClass.AddMember(this, false, true);
			}

			_equipmentClasses.Add(equipmentClass);
		}

		/// <summary>
		/// Removes the given child element of equipment from the equipment.
		/// </summary>
		/// <param name="child">The child element to remove.</param>
		public virtual void RemoveChild(Equipment child)
		{
			ObjectHierarchy.ManyToMany.RemoveElement(this, child, 
				getChildrenFunc: (e) => e._children, 
				getParentsFunc: (e) => e._parents, 
				getAscendantsFunc: (e) => e._ascendants,
				getDescendantsFunc: (e) => e._descendants);
		}

		/// <summary>
		/// Removes this equipment from the given equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class from which to remove the equipment.</param>
		public virtual void RemoveFromClass(EquipmentClass equipmentClass)
		{
			RemoveFromClass(equipmentClass, true);
		}

		/// <summary>
		/// Removes this equipment from the given equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class from which to remove the equipment.</param>
		/// <param name="removeEquipmentFromClass">Stipulates whether to remove the equipment class members.</param>
		protected internal virtual void RemoveFromClass(EquipmentClass equipmentClass, bool removeEquipmentFromClass)
        {
			equipmentClass = ArgumentValidation.AssertNotNull(equipmentClass, nameof(equipmentClass));
			if (removeEquipmentFromClass)
			{
				equipmentClass.RemoveMember(this);
			}

			_equipmentClasses.Remove(equipmentClass);
        }
	}
}