using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
    /// <summary>
    /// Indicates the level of equipment or a hierarchy scope as per ISA-95 Part 1.
    /// </summary>
    public enum EquipmentLevel
    {
		/// <summary>
		/// Enterprise-level equipment or hierarchy scope.
		/// </summary>
		//[PermanentGuid("1e18c596-6e5b-435a-b68b-8729538fa272")]
		Enterprise,

		/// <summary>
		/// Site-level equipment or hierarchy scope.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { Enterprise })]
		//[PermanentGuid("3b024718-f03c-4509-916d-a5a49f010dd1")]
		Site,

		/// <summary>
		/// Area-level equipment or hierarchy scope.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { Site })]
		//[PermanentGuid("b9973008-993f-40f9-b5e1-423fe12aac0f")]
		Area,

		/// <summary>
		/// Work center-level equipment or hierarchy scope.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { Area })]
		//[PermanentGuid("ec93246f-37a2-461b-9916-16f7eb332a0b")]
		WorkCenter,

		/// <summary>
		/// Work center-level equipment or hierarchy scope for batch processes.
		/// </summary>
		[EquipmentLevel(SpecialisationOf = WorkCenter, InheritsChildren = false, InheritsParents = true)]
		//[PermanentGuid("4fb59e87-0e99-4a86-9612-ad2241c1d7d9")]
		ProcessCell,

		/// <summary>
		/// Work center-level equipment or hierarchy scope for discrete processes.
		/// </summary>
		[EquipmentLevel(SpecialisationOf = WorkCenter, InheritsChildren = false, InheritsParents = true)]
		//[PermanentGuid("0b07ac5e-ec67-4c76-9443-74fdd8309a73")]
		ProductionLine,

		/// <summary>
		/// Work center-level equipment or hierarchy scope for continuous processes.
		/// </summary>
		[EquipmentLevel(SpecialisationOf = WorkCenter, InheritsChildren = false, InheritsParents = true)]
		//[PermanentGuid("f5fe1040-8e60-4645-818a-e24c9f00e044")]
		ProductionUnit,

		/// <summary>
		/// Work center-level equipment or hierarchy scope used for storage.
		/// </summary>
		[EquipmentLevel(SpecialisationOf = WorkCenter, InheritsChildren = false, InheritsParents = true)]
		//[PermanentGuid("f36939ce-eef5-4001-9c34-97462f14218a")]
		StorageZone,

		/// <summary>
		/// Work unit-level equipment or hierarchy scope.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { WorkCenter })]
		//[PermanentGuid("e5fbb746-8c7c-4c7e-989f-6f21acff8143")]
		WorkUnit,

		/// <summary>
		/// Work unit-level equipment or hierarchy scope for batch or continuous processes.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { ProcessCell, ProductionUnit }, SpecialisationOf = WorkUnit, InheritsChildren = true, InheritsParents = true)]
		//[PermanentGuid("515e1f72-2d98-42a9-805b-44082d993bac")]
		Unit,

		/// <summary>
		/// Work unit-level equipment or hierarchy scope for discrete processes.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { ProductionLine }, SpecialisationOf = WorkUnit, InheritsChildren = true, InheritsParents = true)]
		//[PermanentGuid("11d75c88-7190-4f2d-b8d8-a719d2560295")]
		WorkCell,

		/// <summary>
		/// Work unit-level equipment or hierarchy scope for storage.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { StorageZone, ProductionLine, ProcessCell, ProductionUnit }, SpecialisationOf = WorkUnit, InheritsChildren = true, InheritsParents = true)]
		//[PermanentGuid("5f5d7682-3d53-4b63-a0af-40a9ccfba21e")]
		StorageUnit,

		/// <summary>
		/// Equipment module-level equipment.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { WorkUnit }, PermittedForHierarchyScopes = false)]
		//[PermanentGuid("29325d77-9d7a-46dd-a191-06fa9d4b080f")]
		EquipmentModule,

		/// <summary>
		/// Control module-level equipment.
		/// </summary>
		[EquipmentLevel(SpecifiedParents = new EquipmentLevel[] { EquipmentModule }, PermittedForHierarchyScopes = false)]
		//[PermanentGuid("be504580-7935-468e-94c3-78ca62955921")]
		ControlModule,

		/// <summary>
		/// Other equipment level, defined by the end user organisation.
		/// </summary>
		[OtherValue]
		//[PermanentGuid("ce194a45-28a8-49a8-afb2-0304d9117c14")]
		Other
	}
}