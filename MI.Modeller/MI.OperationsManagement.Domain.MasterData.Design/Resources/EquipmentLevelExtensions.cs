﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Collections;
using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.Resources
{
	/// <summary>
	/// Defines extension methods for the <see cref="EquipmentLevel"/> enumeration.
	/// </summary>
	public static class EquipmentLevelExtensions
	{
		/// <summary>
		/// Builds or gets the cache of <see cref="EquipmentLevelAttribute"/> attributes.
		/// </summary>
		private static IDictionary<EquipmentLevel, EquipmentLevelAttribute> EquipmentLevelAttributes
		{
			get
			{
				lock (((ICollection)_equipmentLevelAttributes).SyncRoot)
				{
					if (_equipmentLevelAttributes.Count == 0)
					{
						var keyValuePairs = Enum.GetNames(typeof(EquipmentLevel)).Select(name => new
						{
							Key = (EquipmentLevel)Enum.Parse(typeof(EquipmentLevel), name),
							Value = typeof(EquipmentLevel).GetField(name).GetCustomAttribute<EquipmentLevelAttribute>()
						});

						foreach (var keyValuePair in keyValuePairs)
						{
							_equipmentLevelAttributes[keyValuePair.Key] = keyValuePair.Value;
						}
					}
				}

				return _equipmentLevelAttributes;
			}
		}

		/// <summary>
		/// Cache of <see cref="EquipmentLevelAttribute"/> attributes for improving performance.
		/// </summary>
		private static IDictionary<EquipmentLevel, EquipmentLevelAttribute> _equipmentLevelAttributes
			= new Dictionary<EquipmentLevel, EquipmentLevelAttribute>();

		/// <summary>
		/// Cached dictionary of immediate parents by equipment level for improving performance.
		/// </summary>
		private static IDictionary<EquipmentLevel, IEnumerable<EquipmentLevel>> _immediateParents = 
			new Dictionary<EquipmentLevel, IEnumerable<EquipmentLevel>>();

		/// <summary>
		/// Cached dictionary of permitted parents by equipment level for improving performance.
		/// </summary>
		private static IDictionary<(EquipmentLevel, PermittedFor), IEnumerable<EquipmentLevel>> _permittedParents = 
			new Dictionary<(EquipmentLevel, PermittedFor), IEnumerable<EquipmentLevel>>();

		/// <summary>
		/// Cached dictionary of immediate children by equipment level.
		/// </summary>
		private static IDictionary<EquipmentLevel, IEnumerable<EquipmentLevel>> _immediateChildren = 
			new Dictionary<EquipmentLevel, IEnumerable<EquipmentLevel>>();

		/// <summary>
		/// Cached dictionary of permitted children by equipment level for improving performance.
		/// </summary>
		private static IDictionary<(EquipmentLevel, PermittedFor), IEnumerable<EquipmentLevel>> _permittedChildren = 
			new Dictionary<(EquipmentLevel, PermittedFor), IEnumerable<EquipmentLevel>>();

		/// <summary>
		/// Returns the <see cref="EquipmentLevel"/> of which this equipment level is a specialisation, if any.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The equipment level of which this equipment level is a specialisation.</returns>
		public static EquipmentLevel? SpecialisationOf(this EquipmentLevel equipmentLevel)
		{
			var attribute = EquipmentLevelAttributes[equipmentLevel];
			return attribute != null && attribute.IsSpecialisation ? attribute.SpecialisationOf : (EquipmentLevel?)null;
		}

		/// <summary>
		/// Returns the <see cref="EquipmentLevel"/> of which this equipment level is a specialisation, if any.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The equipment level of which this equipment level is a specialisation.</returns>
		public static EquipmentLevel? SpecialisationOf(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return ((EquipmentLevel)equipmentLevel).SpecialisationOf();
		}

		/// <summary>
		/// Returns the specialisations of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The specialisations of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> Specialisations(this EquipmentLevel equipmentLevel)
		{
			foreach (var keyValuePair in EquipmentLevelAttributes)
			{
				if (keyValuePair.Value != null && keyValuePair.Value.IsSpecialisation && keyValuePair.Value.SpecialisationOf == equipmentLevel)
				{
					yield return keyValuePair.Key;
				}
			}
		}

		/// <summary>
		/// Returns the specialisations of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The specialisations of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> Specialisations(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return ((EquipmentLevel)equipmentLevel).Specialisations();
		}

		/// <summary>
		/// Returns the immediate parents of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The immediate parents of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> ImmediateParents(this EquipmentLevel equipmentLevel)
		{
			lock (((ICollection)_immediateParents).SyncRoot)
			{
				if (!_immediateParents.ContainsKey(equipmentLevel))
				{
					var parents = new HashSet<EquipmentLevel>();

					if (equipmentLevel == EquipmentLevel.Other)
					{
						// If "Other", then any equipment level is permitted
						//parents = new HashSet<EquipmentLevel>(Enum.GetNames(typeof(EquipmentLevel)).Select(name => (EquipmentLevel)Enum.Parse(typeof(EquipmentLevel), name)));
						parents = EquipmentLevelAttributes.Keys.ToHashSet();
					}
					else
					{
						// Start with any specified parents
						if (equipmentLevel.SpecifiesParents())
						{
							parents = new HashSet<EquipmentLevel>(equipmentLevel.SpecifiedParents());

							// Do any of the parents have specialisations that inherit children?
							foreach (var parent in parents.ToList())
							{
								parents.UnionWith(parent.Specialisations().Where(s => s.InheritsChildren()));
							}
						}

						// Is it a specialisation and inherits parents?
						if (equipmentLevel.SpecialisationOf().HasValue && equipmentLevel.InheritsParents())
						{
							parents.UnionWith(equipmentLevel.SpecialisationOf().Value.ImmediateParents());
						}
					}

					_immediateParents[equipmentLevel] = parents;
				}
			}

			return _immediateParents[equipmentLevel];
		}

		/// <summary>
		/// Returns the immediate parents of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The immediate parents of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> ImmediateParents(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return ((EquipmentLevel)equipmentLevel).ImmediateParents();
		}

		/// <summary>
		/// Returns the permitted parents of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <returns>The permitted parents of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> PermittedParents(this EquipmentLevel equipmentLevel, PermittedFor permittedFor)
		{
			lock (((ICollection)_permittedParents).SyncRoot)
			{
				if (!_permittedParents.ContainsKey((equipmentLevel, permittedFor)))
				{
					var permittedParents = new HashSet<EquipmentLevel>();

					// Equipment permits a parent at the same level
					if (permittedFor == PermittedFor.Equipment)
					{
						permittedParents.Add(equipmentLevel);
						permittedParents.UnionWith(equipmentLevel.Specialisations());
					}
					else
					{
						// If checking permitted parents for hierarchy scopes, the given equipment level must itself be permitted for hierarchy scopes
						if (!equipmentLevel.IsPermittedForHierarchyScopes())
						{
							throw new ArgumentException(ExceptionMessage.EquipmentLevelExtensions_GivenEquipmentLevelNotPermittedForHierarchyScopes);
						}
					}

					// Recursively add parents
					foreach (var parent in equipmentLevel.ImmediateParents().Where(p => p != equipmentLevel))
					{
						if (permittedFor == PermittedFor.Equipment || parent.IsPermittedForHierarchyScopes())
						{
							permittedParents.Add(parent);
							permittedParents.UnionWith(parent.PermittedParents(permittedFor));
						}
					}

					// Other always permitted as parent of Other
					if (equipmentLevel == EquipmentLevel.Other)
					{
						permittedParents.Add(EquipmentLevel.Other);
					}

					_permittedParents[(equipmentLevel, permittedFor)] = permittedParents.OrderBy(level => (int)level);
				}
			}
			
			return _permittedParents[(equipmentLevel, permittedFor)];
		}

		/// <summary>
		/// Returns the permitted parents of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <returns>The permitted parents of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> PermittedParents(this ExtensibleEnum<EquipmentLevel> equipmentLevel, PermittedFor permittedFor)
		{
			return ((EquipmentLevel)equipmentLevel).PermittedParents(permittedFor);
		}

		/// <summary>
		/// Returns whether this equipment level permits a parent of the given equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <param name="childEquipmentLevel">The parent equipment level.</param>
		/// <returns>True if a parent is permitted with the given equipment level, otherwise false.</returns>
		public static bool PermitsParent(this EquipmentLevel equipmentLevel, PermittedFor permittedFor, EquipmentLevel childEquipmentLevel)
		{
			return equipmentLevel.PermittedParents(permittedFor).Contains(childEquipmentLevel);
		}

		/// <summary>
		/// Returns whether this equipment level permits a parent of the given equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <param name="childEquipmentLevel">The parent equipment level.</param>
		/// <returns>True if a parent is permitted with the given equipment level, otherwise false.</returns>
		public static bool PermitsParent(this ExtensibleEnum<EquipmentLevel> equipmentLevel, PermittedFor permittedFor, EquipmentLevel childEquipmentLevel)
		{
			return equipmentLevel.PermittedParents(permittedFor).Contains(childEquipmentLevel);
		}

		/// <summary>
		/// Returns the immediate children of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The immediate children of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> ImmediateChildren(this EquipmentLevel equipmentLevel)
		{
			lock (((ICollection)_immediateChildren).SyncRoot)
			{
				if (!_immediateChildren.ContainsKey(equipmentLevel))
				{
					var children = new HashSet<EquipmentLevel>();

					if (equipmentLevel != EquipmentLevel.Other)
					{
						// Find all the equipment levels that have the given equipment level in the list of immediate parents
						foreach (var candidateChild in EquipmentLevelAttributes.Keys)
						{
							if (candidateChild.ImmediateParents().Contains(equipmentLevel))
							{
								children.Add(candidateChild);
							}
						}

						// Is the equipment level a specialisation and inherits parents?
						if (equipmentLevel.SpecialisationOf().HasValue && equipmentLevel.InheritsChildren())
						{
							children.UnionWith(equipmentLevel.SpecialisationOf().Value.ImmediateChildren());
						}
					}

					// Other always permitted as a child
					children.Add(EquipmentLevel.Other);

					_immediateChildren[equipmentLevel] = children;
				}
			}
			
			return _immediateChildren[equipmentLevel];
		}

		/// <summary>
		/// Returns the immediate children of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The immediate children of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> ImmediateChildren(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return ((EquipmentLevel)equipmentLevel).ImmediateChildren();
		}

		/// <summary>
		/// Returns the permitted children of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <returns>The permitted children of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> PermittedChildren(this EquipmentLevel equipmentLevel, PermittedFor permittedFor)
		{
			lock (((ICollection)_permittedChildren).SyncRoot)
			{
				if (!_permittedChildren.ContainsKey((equipmentLevel, permittedFor)))
				{
					var permittedChildren = new HashSet<EquipmentLevel>();

					// Equipment permits a child at the same level
					if (permittedFor == PermittedFor.Equipment)
					{
						permittedChildren.Add(equipmentLevel);
						permittedChildren.UnionWith(equipmentLevel.Specialisations());
					}
					else
					{
						// If checking permitted children for hierarchy scopes, the given equipment level must itself be permitted for hierarchy scopes
						if (!equipmentLevel.IsPermittedForHierarchyScopes())
						{
							throw new ArgumentException(ExceptionMessage.EquipmentLevelExtensions_GivenEquipmentLevelNotPermittedForHierarchyScopes);
						}
					}

					// Recursevly add children
					foreach (var child in equipmentLevel.ImmediateChildren().Where(c => c != equipmentLevel))
					{
						if (permittedFor == PermittedFor.Equipment || child.IsPermittedForHierarchyScopes())
						{
							permittedChildren.Add(child);
							foreach (var descendant in child.PermittedChildren(permittedFor))
							{
								permittedChildren.Add(descendant);
							}
						}
					}

					// Other always permitted as child of Other
					if (equipmentLevel == EquipmentLevel.Other)
					{
						permittedChildren.Add(EquipmentLevel.Other);
					}

					_permittedChildren[(equipmentLevel, permittedFor)] = permittedChildren.OrderBy(level => (int)level);
				}
			}

			return _permittedChildren[(equipmentLevel, permittedFor)];
		}

		/// <summary>
		/// Returns the permitted children of this equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <returns>The permitted children of this equipment level.</returns>
		public static IEnumerable<EquipmentLevel> PermittedChildren(this ExtensibleEnum<EquipmentLevel> equipmentLevel, PermittedFor permittedFor)
		{
			return ((EquipmentLevel)equipmentLevel).PermittedChildren(permittedFor);
		}

		/// <summary>
		/// Returns whether this equipment level permits a child of the given equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="childEquipmentLevel">The child equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <returns>True if a child is permitted with the given equipment level, otherwise false.</returns>
		public static bool PermitsChild(this EquipmentLevel equipmentLevel, ExtensibleEnum<EquipmentLevel> childEquipmentLevel, PermittedFor permittedFor)
		{
			return equipmentLevel.PermittedChildren(permittedFor).Contains((EquipmentLevel)childEquipmentLevel);
		}

		/// <summary>
		/// Returns whether this equipment level permits a child of the given equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <param name="childEquipmentLevel">The child equipment level.</param>
		/// <param name="permittedFor">Specifies the type of object for which the operation is performed.</param>
		/// <returns>True if a child is permitted with the given equipment level, otherwise false.</returns>
		public static bool PermitsChild(this ExtensibleEnum<EquipmentLevel> equipmentLevel, ExtensibleEnum<EquipmentLevel> childEquipmentLevel, PermittedFor permittedFor)
		{
			return ((EquipmentLevel)equipmentLevel).PermitsChild(childEquipmentLevel, permittedFor);
		}

		/// <summary>
		/// Returns whether the equipment level is permitted for for hierarchy scopes.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level is permitted for hierarchy scopes, otherwise false.</returns>
		public static bool IsPermittedForHierarchyScopes(this EquipmentLevel equipmentLevel)
		{
			var attribute = EquipmentLevelAttributes[equipmentLevel];
			return attribute != null ? attribute.PermittedForHierarchyScopes : true;
		}

		/// <summary>
		/// Returns whether the equipment level is permitted for for hierarchy scopes.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level is permitted for hierarchy scopes, otherwise false.</returns>
		public static bool IsPermittedForHierarchyScopes(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return ((EquipmentLevel)equipmentLevel).IsPermittedForHierarchyScopes();
		}

		/// <summary>
		/// Returns whether the equipment level is a work center.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level is a work center, otherwise false.</returns>
		public static bool IsWorkCenter(this EquipmentLevel equipmentLevel)
		{
			return equipmentLevel == EquipmentLevel.WorkCenter || EquipmentLevel.WorkCenter.Specialisations().Contains(equipmentLevel);
		}

		/// <summary>
		/// Returns whether the equipment level is a work center.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level is a work center, otherwise false.</returns>
		public static bool IsWorkCenter(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return equipmentLevel == EquipmentLevel.WorkCenter || EquipmentLevel.WorkCenter.Specialisations().Contains((EquipmentLevel)equipmentLevel);
		}

		/// <summary>
		/// Returns whether the equipment level is a work unit.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level is a work unit, otherwise false.</returns>
		public static bool IsWorkUnit(this EquipmentLevel equipmentLevel)
		{
			return equipmentLevel == EquipmentLevel.WorkUnit || EquipmentLevel.WorkUnit.Specialisations().Contains(equipmentLevel);
		}

		/// <summary>
		/// Returns whether the equipment level is a work unit.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level is a work unit, otherwise false.</returns>
		public static bool IsWorkUnit(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return equipmentLevel == EquipmentLevel.WorkUnit || EquipmentLevel.WorkUnit.Specialisations().Contains((EquipmentLevel)equipmentLevel);
		}

		/// <summary>
		/// Returns the default child equipment level for the equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The default child equipment level for the equipment level.</returns>
		public static EquipmentLevel DefaultChild(this EquipmentLevel equipmentLevel)
		{
			var children = equipmentLevel.ImmediateChildren();
			if (children.Count() == 1)
			{
				return children.First();
			}

			var childrenExcludingOther = children.Except(EquipmentLevel.Other);
			if (childrenExcludingOther.Count() == 1)
			{
				return childrenExcludingOther.First();
			}

			var unspecialisedChildren = childrenExcludingOther.Where(c => !c.SpecialisationOf().HasValue);
			if (unspecialisedChildren.Count() > 0)
			{
				return unspecialisedChildren.First();
			}

			return childrenExcludingOther.First();
		}

		/// <summary>
		/// Returns the default child equipment level for the equipment level.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The default child equipment level for the equipment level.</returns>
		public static EquipmentLevel DefaultChild(this ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			return ((EquipmentLevel)equipmentLevel).DefaultChild();
		}

		/// <summary>
		/// Returns whether the equipment level specifies parents.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level specifies parents, otherwise false.</returns>
		private static bool SpecifiesParents(this EquipmentLevel equipmentLevel)
		{
			var attribute = EquipmentLevelAttributes[equipmentLevel];
			return attribute != null ? attribute.SpecifiesParents : false;
		}

		/// <summary>
		/// Returns the equipment level's specified parents.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>The equipment level's specified parents.</returns>
		private static IEnumerable<EquipmentLevel> SpecifiedParents(this EquipmentLevel equipmentLevel)
		{
			var attribute = EquipmentLevelAttributes[equipmentLevel];
			return attribute != null ? attribute.SpecifiedParents : new EquipmentLevel[] { };
		}

		/// <summary>
		/// Returns whether the equipment level inherits parents.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level inherits parents, otherwise false.</returns>
		private static bool InheritsParents(this EquipmentLevel equipmentLevel)
		{
			var attribute = EquipmentLevelAttributes[equipmentLevel];
			return attribute != null ? attribute.InheritsParents : false;
		}

		/// <summary>
		/// Returns whether the equipment level inherits children.
		/// </summary>
		/// <param name="equipmentLevel">This equipment level.</param>
		/// <returns>True if the equipment level inherits children, otherwise false.</returns>
		private static bool InheritsChildren(this EquipmentLevel equipmentLevel)
		{
			var attribute = EquipmentLevelAttributes[equipmentLevel];
			return attribute != null ? attribute.InheritsChildren : false;
		}
	}
}
