﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Domain.MasterData.Design
{
	/// <summary>
	/// Scoping object for all manufacturing objects.
	/// </summary>
	public class HierarchyScope : DesignObject
    {
		/// <summary>
		/// The description of this hierachy scope.
		/// </summary>
		/// <remarks>
		/// The description of a hierarchy scope is drawn from that of its <see cref="DefiningEquipment"/> property.
		/// </remarks>
		public override string Description { get => DefiningEquipment.Description; set => DefiningEquipment.Description = value; }

		/// <summary>
		/// The external identifer of this hierachy scope.
		/// </summary>
		/// <remarks>
		/// The external identifier of a hierarchy scope is drawn from that of its <see cref="DefiningEquipment"/> property.
		/// </remarks>
		public override string ExternalId { get => DefiningEquipment.ExternalId; set => DefiningEquipment.ExternalId = value; }

		/// <summary>
		/// The child hierarchy scopes of this hierarchy scope.
		/// </summary>
		public virtual IEnumerable<HierarchyScope> Children => _children;
		private readonly ISet<HierarchyScope> _children = new HashSet<HierarchyScope>();

		/// <summary>
		/// The parent hierarchy scope of this hierarchy scope.
		/// </summary>
		public virtual HierarchyScope Parent => _parent;
		private HierarchyScope _parent;
		
		/// <summary>
		/// The <see cref="Equipment"/> defining this hierarchy scope.
		/// </summary>
		public virtual Equipment DefiningEquipment => _definingEquipment;
		private readonly Equipment _definingEquipment;

		/// <summary>
		/// All ascendants of the hierarchy scope.
		/// </summary>
		/// <remarks>
		/// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<HierarchyScope> Ascendants => _ascendants;
		private ICollection<HierarchyScope> _ascendants = new Collection<HierarchyScope>();

		/// <summary>
		/// All descendants of the hierarchy scope.
		/// </summary>
		/// <remarks>
		/// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
		/// </remarks>
		public virtual IEnumerable<HierarchyScope> Descendants => _descendants;
		private ICollection<HierarchyScope> _descendants = new Collection<HierarchyScope>();

		/// <summary>
		/// The <see cref="EquipmentLevel"/> of the hierarchy scope.
		/// </summary>
		/// <remarks>
		/// The equipment level of a hierarchy scope is drawn from that of its <see cref="DefiningEquipment"/> property.
		/// </remarks>
		public virtual ExtensibleEnum<EquipmentLevel> EquipmentLevel
		{
			get => DefiningEquipment.EquipmentLevel;
			set => DefiningEquipment.EquipmentLevel = value;
		}
        
        /// <summary>
        /// Indicates whether this hierarchy scope is a leaf in the hierarchy of hierarchy scopes.
        /// </summary>
        /// <value><c>true</c> if this instance is leaf; otherwise, <c>false</c>.</value>
        public virtual bool IsLeaf => _children.Count == 0;
		
		/// <summary>
		/// Initialises a new instance of the <see cref="HierarchyScope"/> class.
		/// </summary>
		protected HierarchyScope()
		{
			_definingEquipment = new Equipment(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="HierarchyScope"/> class.
		/// </summary>
		/// <param name="equipmentLevel">The equipment level for the new hierarchy scope.</param>
		internal HierarchyScope(ExtensibleEnum<EquipmentLevel> equipmentLevel)
		{
			_definingEquipment = new Equipment(this) { EquipmentLevel = equipmentLevel };
		}

        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchyScope"/> class.
        /// </summary>
        /// <param name="definingEquipment">The equipment which to define the new hierarchy scope.</param>
		/// <remarks>
		/// This constructor is used when promoting an equipment object to a hierarchy scope.
		/// </remarks>
        internal HierarchyScope(Equipment definingEquipment) 
        {
			_definingEquipment = ArgumentValidation.AssertNotNull(definingEquipment, nameof(definingEquipment));
		}

        /// <summary>
        /// Initialises a new instance of the <see cref="HierarchyScope" /> class.
        /// </summary>
        /// <param name="externalId">The external identifier of the new hierarchy scope.</param>
        /// <param name="equipmentLevel">The equipment level of the new hierarchy scope.</param>
        public HierarchyScope(string externalId, ExtensibleEnum<EquipmentLevel> equipmentLevel) 
			: base(externalId)
        {
            _definingEquipment = new Equipment(this, externalId, equipmentLevel);
		}

		/// <summary>
		/// Returns whether a given hierarchy scope is an ascendant of the hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given hierarchy scope object is an ascendant of the hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(HierarchyScope hierarchyScope)
		{
			hierarchyScope = ArgumentValidation.AssertNotNull(hierarchyScope, nameof(hierarchyScope));
			return hierarchyScope._ascendants.Contains(this);
		}

		/// <summary>
		/// Returns whether a given hierarchy scope is a descendant of the hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given hierarchy scope object is a descendant of the hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(HierarchyScope hierarchyScope)
		{
			return _ascendants.Contains(hierarchyScope);
		}

		/// <summary>
		/// Returns whether the given hierarchy scope can be added as a child of this hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to evaluate.</param>
		/// <returns><c>true</c> if the given hierarchy scope can be added as a child of this hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool CanAddChild(HierarchyScope hierarchyScope)
		{
			hierarchyScope = ArgumentValidation.AssertNotNull(hierarchyScope, nameof(hierarchyScope));
			return hierarchyScope != this
				&& !IsDescendantOf(hierarchyScope)
				&& !Children.Contains(hierarchyScope)
				&& EquipmentLevel.PermitsChild(hierarchyScope.EquipmentLevel, PermittedFor.HierarchyScope);
		}

		/// <summary>
		/// Returns whether the hierarchy scope contains the given hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to evaluate.</param>
		/// <returns><c>true</c> if the hierarchy scope contains the given hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool Contains(HierarchyScope hierarchyScope)
		{
			hierarchyScope = ArgumentValidation.AssertNotNull(hierarchyScope, nameof(hierarchyScope));
			return hierarchyScope._ascendants.Contains(this);
		}

		/// <summary>
		/// Returns whether the hierarchy scope is contained within the given hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to evaluate.</param>
		/// <returns><c>true</c> if the hierarchy scope is contained within the given hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool ContainedWithin(HierarchyScope hierarchyScope)
		{
			return _ascendants.Contains(hierarchyScope);
		}

		/// <summary>
		/// Returns whether the hierarchy scope equals or contains the given hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to evaluate.</param>
		/// <returns><c>true</c> if the hierarchy scope equals or contains the given hierarchy scope; otherwise <c>false</c>.</returns>
		public virtual bool EqualsOrContains(HierarchyScope hierarchyScope)
		{
			return this == hierarchyScope || Contains(hierarchyScope);
		}

		/// <summary>
		/// Add a hierarchy scope as a child of this hierarchy scope.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope to add as a child of this hierarchy scope.</param>
		public virtual void AddChild(HierarchyScope hierarchyScope)
        {
			hierarchyScope = ArgumentValidation.AssertNotNull(hierarchyScope, nameof(hierarchyScope));
			if (!CanAddChild(hierarchyScope))
			{
				throw new InvalidOperationException(ExceptionMessage.CannotPromoteToHierarchyScope);
			}

			ObjectHierarchy.OneToMany.AddElement(this, hierarchyScope,
				setParentFunc: (hs, value) => hs._parent = value,
				getChildrenFunc: (hs) => hs._children,
				getAscendantsFunc: (hs) => hs._ascendants,
				getDescendantsFunc: (hs) => hs._descendants);

			DefiningEquipment.AddChild(hierarchyScope.DefiningEquipment);
		}
		
        /// <summary>
        /// Removes the child.
        /// </summary>
        /// <param name="child">The child.</param>
        public virtual void RemoveChild(HierarchyScope child)
        {
			child = ArgumentValidation.AssertNotNull(child, nameof(child));
			ObjectHierarchy.OneToMany.RemoveElement(this, child,
				setParentFunc: (hs, value) => hs._parent = value, 
				getChildrenFunc: (hs) => hs._children, 
				getAscendantsFunc: (hs) => hs._ascendants,
				getDescendantsFunc: (hs) => hs._descendants);

			DefiningEquipment.RemoveChild(child.DefiningEquipment);
		}

		/// <summary>
		/// Returns the hierarchy scope of the given design object.
		/// </summary>
		/// <param name="designObject">The design object for which to get the hierarchy scope.</param>
		/// <returns>The hierarchy scope of the given design object.</returns>
		/// <remarks>
		/// If the given design object is a hierarchy scope, then this method returns the design object as a hierarchy scope.  If
		/// the given design object is a hierarchy scoped object, then this method returns the hierarchy scope of the design object.
		/// </remarks>
		public static HierarchyScope Of(DesignObject designObject)
		{
			designObject = ArgumentValidation.AssertNotNull(designObject, nameof(designObject));
			if (designObject is HierarchyScope hierarchyScope)
			{
				return hierarchyScope;
			}

			return ((HierarchyScopedObject)designObject).HierarchyScope;
		}
	}
}