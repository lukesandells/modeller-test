﻿//using System;
//using System.Collections.Generic;
//using System.Diagnostics.CodeAnalysis;
//using MI.Framework;

//namespace MI.Modeller.Domain.Client
//{
//    /// <summary>
//    ///     Class ResourceRelationshipNetwork.
//    /// </summary>
//    public class ResourceRelationshipNetwork
//    {
//        #region  Fields

//        /// <summary>
//        ///     Gets the description.
//        /// </summary>
//        /// <value>The description.</value>
//        public virtual string Description { get; protected set; }

//        /// <summary>
//        ///     Gets the hierarchy scope.
//        /// </summary>
//        /// <value>The hierarchy scope.</value>
//        public virtual HierarchyScope HierarchyScope { get; protected set; }

//        /// <summary>
//        ///     Gets the identifier.
//        /// </summary>
//        /// <value>The identifier.</value>
//        public virtual string Id { get; protected set; }

//        /// <summary>
//        ///     Gets the published date.
//        /// </summary>
//        /// <value>The published date.</value>
//        public virtual DateTime PublishedDate { get; protected set; }

//        /// <summary>
//        ///     Gets the relationship form.
//        /// </summary>
//        /// <value>The relationship form.</value>
//        public virtual ExtensibleEnum<ResourceRelationshipFormType>? RelationshipForm { get; protected set; }

//        /// <summary>
//        ///     Gets the type of the relationship.
//        /// </summary>
//        /// <value>The type of the relationship.</value>
//        public virtual ExtensibleEnum<ResourceRelationshipType>? RelationshipType { get; protected set; }

//        /// <summary>
//        ///     Gets the resource network connections.
//        /// </summary>
//        /// <value>The resource network connections.</value>
//        public virtual ISet<ResourceNetworkConnection> ResourceNetworkConnections { get; set; } = new HashSet<ResourceNetworkConnection>();

//        #endregion

//        #region Constructors

//        /// <summary>
//        ///     Initializes a new instance of the <see cref="ResourceRelationshipNetwork" /> class.
//        /// </summary>
//        /// <param name="id">The identifier.</param>
//        /// <param name="description">The description.</param>
//        /// <param name="hierarchyScope">The hierarchy scope.</param>
//        /// <param name="relationshipType">Type of the relationship.</param>
//        /// <param name="relationshipForm">The relationship form.</param>
//        /// <param name="publishedDate">The published date.</param>
//        /// <param name="resourceNetworkConnections">The resource network connections.</param>
//        [SuppressMessage("ReSharper", "VirtualMemberCallInConstructor")]
//        public ResourceRelationshipNetwork(string id = default(string),
//            string description = default(string),
//            HierarchyScope hierarchyScope = default(HierarchyScope),
//            ExtensibleEnum<ResourceRelationshipType>? relationshipType = null,
//            ExtensibleEnum<ResourceRelationshipFormType>? relationshipForm = null,
//            DateTime publishedDate = default(DateTime),
//            ISet<ResourceNetworkConnection> resourceNetworkConnections = null)
//        {
//            Id = id;
//            Description = description;
//            HierarchyScope = hierarchyScope;
//            RelationshipType = relationshipType;
//            RelationshipForm = relationshipForm;
//            PublishedDate = publishedDate;

//            ResourceNetworkConnections = resourceNetworkConnections ?? new HashSet<ResourceNetworkConnection>();
//        }

//        #endregion

//        #region Members


//        #endregion
//    }
//}