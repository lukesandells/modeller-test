using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ResourceRelationshipNetworks
{
    /// <summary>
    ///     Enum ResourceReferenceTypeValues. The allowed Types
    ///     that may be associated with the Resource.
    /// </summary>
    public enum ResourceReferenceType
    {
        /// <summary>
        ///     The personnel
        /// </summary>
        Personnel,

        /// <summary>
        ///     The personnel class
        /// </summary>
        PersonnelClass,

        /// <summary>
        ///     The equipment
        /// </summary>
        Equipment,

        /// <summary>
        ///     The equipment class
        /// </summary>
        EquipmentClass,

        /// <summary>
        ///     The material class
        /// </summary>
        MaterialClass,

        /// <summary>
        ///     The material definition
        /// </summary>
        MaterialDefinition,

        /// <summary>
        ///     The material lot
        /// </summary>
        MaterialLot,

        /// <summary>
        ///     The material sublot
        /// </summary>
        MaterialSublot,

        /// <summary>
        ///     The physical asset
        /// </summary>
        PhysicalAsset,

        /// <summary>
        ///     The physical asset class
        /// </summary>
        PhysicalAssetClass,

        /// <summary>
        ///     The work master
        /// </summary>
        WorkMaster,

        /// <summary>
        ///     The process segment
        /// </summary>
        ProcessSegment,

		/// <summary>
		///     The other
		/// </summary>
		[OtherValue]
		Other
    }
}