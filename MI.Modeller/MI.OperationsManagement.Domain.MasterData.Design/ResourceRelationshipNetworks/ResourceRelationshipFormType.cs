using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ResourceRelationshipNetworks
{
    /// <summary>
    ///     Enum ResourceRelationshipFormTypeValues. The allowed Types
    ///     that may be associated with the Resource Relationship.
    /// </summary>
    public enum ResourceRelationshipFormType
    {
        /// <summary>
        ///     The permanent
        /// </summary>
        Permanent,

        /// <summary>
        ///     The transient
        /// </summary>
        Transient,

		/// <summary>
		///     The other
		/// </summary>
		[OtherValue]
		Other
    }
}