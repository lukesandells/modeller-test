using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ResourceRelationshipNetworks
{
    /// <summary>
    ///     Enum ResourceRelationshipTypeValues. The allowed Types
    ///     that may be associated with the Resource Relationship.
    /// </summary>
    public enum ResourceRelationshipType
    {
        /// <summary>
        ///     The logical
        /// </summary>
        Logical,

        /// <summary>
        ///     The physical
        /// </summary>
        Physical,

		/// <summary>
		///     The other
		/// </summary>
		[OtherValue]
		Other
    }
}