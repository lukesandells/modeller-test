﻿using System;
using System.Collections.Generic;
using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// Applied to any process definition object.
	/// </summary>
	public interface IProcessDefinition<TProcessDefinition>
		where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>
	{
		/// <summary>
		/// The parent process definition.
		/// </summary>
		TProcessDefinition Parent { get; }

		/// <summary>
		/// The child process definitions.
		/// </summary>
		IEnumerable<TProcessDefinition> Children { get; }

		/// <summary>
		/// The process definition at the top of the process definition hierarchy to which the process definition belongs.
		/// </summary>
		TProcessDefinition Root { get; }

		/// <summary>
		/// Indicates whether the process definition is at the top of the process definition hierarchy to which it belongs.
		/// </summary>
		bool IsRoot { get; }

		/// <summary>
		/// The ascendant process definitions.
		/// </summary>
		IEnumerable<TProcessDefinition> Ascendants { get; }

		/// <summary>
		/// The descendant process definitions.
		/// </summary>
		IEnumerable<TProcessDefinition> Descendants { get; }

		/// <summary>
		/// Gets the descendant material specifications of the process definition.
		/// </summary>
		/// <remarks>
		/// Such a property is provided only for material specifications because the other types of resource specifications are all single-level.
		/// </remarks>
		IEnumerable<MaterialSpecification<TProcessDefinition>> AllMaterialSpecifications { get; }

		/// <summary>
		/// Gets the immediate material specifications of the process definition.
		/// </summary>
		IEnumerable<MaterialSpecification<TProcessDefinition>> MaterialSpecifications { get; }

		/// <summary>
		/// Gets the immediate equipment specifications of the process definition.
		/// </summary>
		IEnumerable<EquipmentSpecification<TProcessDefinition>> EquipmentSpecifications { get; }

		/// <summary>
		/// Gets the immediate physical asset specifications of the process definition.
		/// </summary>
		IEnumerable<PhysicalAssetSpecification<TProcessDefinition>> PhysicalAssetSpecifications { get; }

		/// <summary>
		/// Gets the immediate personnel specifications of the process definition.
		/// </summary>
		IEnumerable<PersonnelSpecification<TProcessDefinition>> PersonnelSpecifications { get; }

		/// <summary>
		/// Indicates the type of physical process the process definition represents.
		/// </summary>
		ExtensibleEnum<ProcessType>? ProcessType { get; set; }

		/// <summary>
		/// The typical process execution duration of the process definition.
		/// </summary>
		TimeSpan? Duration { get; set; }

		/// <summary>
		/// The date/time at which the process definition was created.
		/// </summary>
		DateTime PublishedDate { get; }

		/// <summary>
		/// The version of the process definition.
		/// </summary>
		string Version { get; set; }

		/// <summary>
		/// Returns whether a given process definition is an ascendant of the process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given process definition is an ascendant of the process definition; otherwise <c>false</c>.</returns>
		bool IsAscendantOf(TProcessDefinition processDefinition);

		/// <summary>
		/// Returns whether a given process definition is a descendant of the process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given process definition is a descendant of the process definition; otherwise <c>false</c>.</returns>
		bool IsDescendantOf(TProcessDefinition processDefinition);

		/// <summary>
		/// Returns whether the given process definition can be added as a child process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition to evaluate.</param>
		/// <returns><c>true</c> if the given process definition can be added as a child process definition; otherwise <c>false</c>.</returns>
		bool CanAdd(TProcessDefinition processDefinition);

		/// <summary>
		/// Adds a child process definition to the process definition.
		/// </summary>
		/// <param name="child">The child process definition to add.</param>
		void AddChild(TProcessDefinition child);

		/// <summary>
		/// Removes a child process definition from the process definition.
		/// </summary>
		/// <param name="child">The child process definition to remove.</param>
		void RemoveChild(TProcessDefinition child);

		/// <summary>
		/// Adds a new material specification to the process definition.
		/// </summary>
		/// <param name="materialSpecification">The material specification to add. Must not have been added to any other process definition.</param>
		/// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
		/// <c>null</c> to add the given material specification directly under the process definition.</param>
		void AddMaterialSpecification(MaterialSpecification<TProcessDefinition> materialSpecification,
			MaterialSpecification<TProcessDefinition> parentMaterialSpecification = null);

		/// <summary>
		/// Adds a new material specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		/// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
		/// <c>null</c> to add the given material specification directly under the process definition.</param>
		/// <returns>The new material specification.</returns>
		MaterialSpecification<TProcessDefinition> AddMaterialSpecification(HierarchyScope hierarchyScope, string externalId, 
			MaterialSpecification<TProcessDefinition> parentMaterialSpecification = null);

		/// <summary>
		/// Removes the given material specification from the process definition.
		/// </summary>
		/// <param name="materialSpecification">The material specification to remove.</param>
		/// <param name="removeEntirely">A value of <c>true</c> will remove the material specification entirely from the process definition, 
		/// removing it from all descendant material specifications.  A value of <c>false</c> will remove the material specification
		/// only from the <see cref="MaterialSpecifications"/> of the process definition.</param>
		void RemoveMaterialSpecification(MaterialSpecification<TProcessDefinition> materialSpecification, bool removeEntirely = false);

		/// <summary>
		/// Adds a new equipment specification to the process definition.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification to add. Must not have been added to any other process definition.</param>
		void AddEquipmentSpecification(EquipmentSpecification<TProcessDefinition> equipmentSpecification);

		/// <summary>
		/// Adds a new equipment specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
		/// <param name="externalId">The external ID of the new equipment specification.</param>
		/// <returns>The new equipment specification.</returns>
		EquipmentSpecification<TProcessDefinition> AddEquipmentSpecification(HierarchyScope hierarchyScope, string externalId);

		/// <summary>
		/// Removes the given equipment specification from the process definition.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification to remove.</param>
		void RemoveEquipmentSpecification(EquipmentSpecification<TProcessDefinition> equipmentSpecification);

		/// <summary>
		/// Adds a new physical asset specification to the process definition.
		/// </summary>
		/// <param name="physicalAssetSpecification">The physical asset specification to add. Must not have been added to any other 
		/// process definition.</param>
		void AddPhysicalAssetSpecification(PhysicalAssetSpecification<TProcessDefinition> physicalAssetSpecification);

		/// <summary>
		/// Adds a new physical asset specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
		/// <param name="externalId">The external ID of the new physical asset specification.</param>
		/// <returns>The new physical asset specification.</returns>
		PhysicalAssetSpecification<TProcessDefinition> AddPhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId);

        /// <summary>
        /// Removes the given material specification from the process definition.
        /// </summary>
        /// <param name="physicalAssetSpecification">The physical asset specification to remove.</param>
        void RemovePhysicalAssetSpecification(PhysicalAssetSpecification<TProcessDefinition> physicalAssetSpecification);

		/// <summary>
		/// Adds a new personnel specification directly to the process definition.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification to add. Must not have been added to any other process definition.</param>
		void AddPersonnelSpecification(PersonnelSpecification<TProcessDefinition> personnelSpecification);

		/// <summary>
		/// Adds a new personnel specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
		/// <param name="externalId">The external ID of the new personnel specification.</param>
		/// <returns>The new personnel specification.</returns>
		PersonnelSpecification<TProcessDefinition> AddPersonnelSpecification(HierarchyScope hierarchyScope, string externalId);

		/// <summary>
		/// Removes the given personnel specification from the process definition.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification to remove.</param>
		void RemovePersonnelSpecification(PersonnelSpecification<TProcessDefinition> personnelSpecification);
	}
}
