﻿using System;
using System.Collections.Generic;
using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
    /// <summary>
    ///     Class OperationsDefinition.
    ///     non-collection properties are readonly
    ///     for collection properties only the Add method is exposed
    ///     All paremeters are copied by value for collections
    /// </summary>
    /// <seealso>
    ///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
    /// </seealso>
    public class OperationsDefinition : HierarchyScopedObject
	{
        #region  Fields

	    /// <summary>
	    ///     Gets the bill of resources identifier.
	    /// </summary>
	    /// <value>The bill of resources identifier.</value>
	    private string _billofResourcesId;
	    public string BillOfResourcesId
	    {
	        get => _billofResourcesId;
            set => _billofResourcesId = value;
	    }

		/// <summary>
		///     Gets the operations material bills.
		/// </summary>
		/// <value>The operations material bills.</value>
		public IEnumerable<OperationsMaterialBill> OperationsMaterialBills => _operationsMaterialBills;
		private readonly ISet<OperationsMaterialBill> _operationsMaterialBills = new HashSet<OperationsMaterialBill>();

		/// <summary>
		///     Gets the operations segments.
		/// </summary>
		/// <value>The operations segments.</value>
		public IEnumerable<OperationsSegment> OperationsSegments => _operationsSegments;
		private readonly ISet<OperationsSegment> _operationsSegments = new HashSet<OperationsSegment>();

		/// <summary>
		///     Gets the type of the operations.
		/// </summary>
		/// <value>The type of the operations.</value>
		private ExtensibleEnum<ProcessType>? _processType;
	    public ExtensibleEnum<ProcessType>? ProcessType
	    {
	        get => _processType;
            set => _processType = value;
	    }

        /// <summary>
        ///     Gets the work definition identifier.
        /// </summary>
        /// <value>The work definition identifier.</value>
        public string WorkDefinitionId { get; set; }

		#endregion

		#region Constructors

		/// <summary>
		///     Initializes a new instance of the <see cref="OperationsDefinition" /> class.
		/// </summary>
		protected OperationsDefinition()
		{
		}

		/// <summary>
		///     Initializes a new instance of the <see cref="OperationsDefinition" /> class.
		/// </summary>
		/// <param name="hierarchyScope">The HierarchyScope of this object</param>
		/// <param name="externalId">The ExternalId of this object</param>
		public OperationsDefinition(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
        {
        }

        #endregion

        #region Members

        /// <summary>
        ///     Adds the operations segment.
        /// </summary>
        /// <param name="operationsSegment">The operations segment.</param>
        public void AddOperationsSegment(OperationsSegment operationsSegment)
        {
            if (!_operationsSegments.Contains(operationsSegment))
            {
                _operationsSegments.Add(operationsSegment);
	            if (operationsSegment.OperationsDefinition != this)
	            {
		            operationsSegment.OperationsDefinition = this;
	            }
            }
        }

        /// <summary>
        ///     Removes the operations segment.
        /// </summary>
        /// <param name="operationsSegment">The operations segment.</param>
        public void RemoveOperationsSegment(OperationsSegment operationsSegment)
        {
	        if (!_operationsSegments.Contains(operationsSegment))
	        {
		        throw new InvalidOperationException(ExceptionMessage.ItemNotFoundInCollection);
	        }
			_operationsSegments.Remove(operationsSegment);
            operationsSegment.OperationsDefinition = null;
        }

		/// <summary>
		/// Adds an OperationsMaterialBill to this OperationsDefinition
		/// </summary>
		/// <param name="operationsMaterialBill">The OperationsMaterialBill to add</param>
		public void AddOperationsMaterialBill(OperationsMaterialBill operationsMaterialBill)
		{
			_operationsMaterialBills.Add(operationsMaterialBill);
		}

		/// <summary>
		/// Removes an OperationsMaterialBill from this OperationsDefinition
		/// </summary>
		/// <param name="operationsMaterialBill">The OperationsMaterialBill to remove</param>
		public void RemoveOperationsMaterialBill(OperationsMaterialBill operationsMaterialBill)
		{
			_operationsMaterialBills.Remove(operationsMaterialBill);
		}

		#endregion
	}
}