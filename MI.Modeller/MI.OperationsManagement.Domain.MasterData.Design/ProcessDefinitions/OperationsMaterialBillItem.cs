﻿using System;
using System.Collections.Generic;
using MI.Framework;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	///     Class OperationsMaterialBillItem.
	///     non-collection properties are readonly
	///     for collection properties only the Add method is exposed
	///     All paremeters are copied by value for collections
	/// </summary>
	public class OperationsMaterialBillItem : HierarchyScopedObject
	{
        #region  Fields

        /// <summary>
        /// The child bill items of this OperationsMaterialBillItem.
        /// </summary>
        public virtual IEnumerable<OperationsMaterialBillItem> Children => _children;
	    private readonly ISet<OperationsMaterialBillItem> _children = new HashSet<OperationsMaterialBillItem>();

        /// <summary>
        /// All ascendants of this OperationsMaterialBillItem.
        /// </summary>
        public virtual IEnumerable<OperationsMaterialBillItem> Ascendants => _ascendants;
	    private ICollection<OperationsMaterialBillItem> _ascendants = new HashSet<OperationsMaterialBillItem>();

        /// <summary>
        /// The descendants of this OperationsMaterialBillItem.
        /// </summary>
        public virtual IEnumerable<OperationsMaterialBillItem> Descendants => _descendants;
	    private ICollection<OperationsMaterialBillItem> _descendants = new HashSet<OperationsMaterialBillItem>();

        /// <summary>
        /// The parent bill item of this hierarchy scope.
        /// </summary>
        public virtual OperationsMaterialBillItem Parent => _parent;
	    private OperationsMaterialBillItem _parent;

        /// <summary>
        ///     The assembly relationship type
        /// </summary>
        /// <value>The type of the assembly relationship.</value>
	    public virtual ExtensibleEnum<AssemblyRelationship>? AssemblyRelationshipType
	    {
	        get => _assemblyRelationshipType;
            set => _assemblyRelationshipType = value;
	    }
	    private ExtensibleEnum<AssemblyRelationship>? _assemblyRelationshipType;

        /// <summary>
        ///     The assembly type
        /// </summary>
        /// <value>The type of the assembly.</value>
	    public virtual ExtensibleEnum<AssemblyType>? AssemblyType
	    {
	        get => _assemblyType;
            set => _assemblyType = value;
	    }
	    private ExtensibleEnum<AssemblyType>? _assemblyType;

        /// <summary>
        ///     The material class identifier
        /// </summary>
        /// <value>The material class identifier.</value>
	    public virtual MaterialClass MaterialClass
	    {
	        get => _materialClass;
            set => _materialClass = value;
	    }
	    private MaterialClass _materialClass;

        /// <summary>
        ///     The material definition identifier
        /// </summary>
        /// <value>The material definition identifier.</value>
	    public virtual MaterialDefinition MaterialDefinition
	    {
	        get => _materialDefinition;
	        set => _materialDefinition = value;
        }
	    private MaterialDefinition _materialDefinition;

        /// <summary>
        ///     Gets the material specification IDs.
        /// </summary>
        /// <value>The material specification IDs.</value>
	    public virtual IEnumerable<MaterialSpecification<OperationsSegment>> MaterialSpecifications => _materialSpecifications;
	    private readonly ISet<MaterialSpecification<OperationsSegment>> _materialSpecifications = new HashSet<MaterialSpecification<OperationsSegment>>();

        /// <summary>
        ///     Gets the quantities.
        /// </summary>
        /// <value>The quantities.</value>
        public virtual Quantity Quantity { get => _quantity; set => _quantity = value; }
	    private Quantity _quantity;

        /// <summary>
        ///     The use type
        /// </summary>
        /// <value>The type of the use.</value>
	    public virtual string UseType
	    {
	        get => _useType;
            set => _useType = value;
        }
	    private string _useType;

	    /// <summary>
	    /// The Operations Material Bill this Item belongs to
	    /// </summary>
	    public OperationsMaterialBill OperationsMaterialBill
	    {
	        get => _operationsMaterialBill;
	        set => _operationsMaterialBill = value;
	    }
        private OperationsMaterialBill _operationsMaterialBill;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="OperationsMaterialBillItem" /> class.
        /// </summary>
        /// <param name="hierarchyScope">The HierarchyScope of this OperationsMaterialBillItem</param>
        /// <param name="externalId">The ExternalId of this OperationsMaterialBillItem</param>
        public OperationsMaterialBillItem(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
		}

        #endregion

        #region Members

	    /// <summary>
	    /// Add a bill item as a child of this bill itme
	    /// </summary>
	    /// <param name="item">The bill item to add as a child of this bill item.</param>
	    public virtual void AddChild(OperationsMaterialBillItem item)
	    {
	        if (item.OperationsMaterialBill != this.OperationsMaterialBill)
	        {
                // What should happen if this is the case? Should we automatically re-assign?
	            throw new NotImplementedException();
	        }
	        ObjectHierarchy.OneToMany.AddElement(this, item,
	            setParentFunc: (i, value) => i._parent = value,
	            getChildrenFunc: (i) => i._children,
	            getAscendantsFunc: (i) => i._ascendants,
	            getDescendantsFunc: (i) => i._descendants);
	    }

	    /// <summary>
	    /// Removes the child.
	    /// </summary>
	    /// <param name="item">The child.</param>
	    public virtual void RemoveChild(OperationsMaterialBillItem item)
	    {
	        ObjectHierarchy.OneToMany.RemoveElement(this, item,
	            setParentFunc: (i, value) => i._parent = value,
	            getChildrenFunc: (i) => i._children,
	            getAscendantsFunc: (i) => i._ascendants,
	            getDescendantsFunc: (i) => i._descendants);
	    }

	    /// <summary>
	    /// Adds a material specification to this Operations Material Bill Item
	    /// </summary>
	    /// <param name="materialSpecification"></param>
	    public virtual void AddMaterialSpecification(MaterialSpecification<OperationsSegment> materialSpecification)
	    {
	        _materialSpecifications.Add(materialSpecification);
	    }

        /// <summary>
        /// Removes a material specification to this Operations Material Bill Item
        /// </summary>
        /// <param name="materialSpecification"></param>
        public virtual void RemoveMaterialSpecification(MaterialSpecification<OperationsSegment> materialSpecification)
	    {
	        _materialSpecifications.Remove(materialSpecification);
	    }

        #endregion
    }
}