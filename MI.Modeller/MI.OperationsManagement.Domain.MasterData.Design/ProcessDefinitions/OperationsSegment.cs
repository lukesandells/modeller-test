﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	///     Class OperationsSegment.ad
	///     All properties in this class are readonly.
	///     All collection type properties are wrapped, and only the Addxxx methods are exposed. No duplicate values may be
	///     added
	/// </summary>
	public class OperationsSegment : HierarchyScopedObject, IProcessDefinition<OperationsSegment>, IWithBasicProperties<OperationsSegment>, IWithProcessDefinitionMixIn<OperationsSegment>
	{
		#region  Fields

		/// <summary>
		/// The process definition mix-in.
		/// </summary>
		public virtual ProcessDefinitionMixIn<OperationsSegment> ProcessDefinitionMixIn => _processDefinitionMixIn;
		private readonly ProcessDefinitionMixIn<OperationsSegment> _processDefinitionMixIn;

		/// <summary>
		/// The "with basic properites" mix-in.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<OperationsSegment> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<OperationsSegment> _withPropertiesMixIn;

	    /// <summary>
	    /// The parent operations segment.
	    /// </summary>
	    public virtual OperationsSegment Parent => ProcessDefinitionMixIn.Parent;

	    /// <summary>
	    /// The child operations segments.
	    /// </summary>
	    public virtual IEnumerable<OperationsSegment> Children => ProcessDefinitionMixIn.Children;

		/// <summary>
		/// The ascendant operations segments.
		/// </summary>
		public virtual IEnumerable<OperationsSegment> Ascendants => ProcessDefinitionMixIn.Ascendants;

		/// <summary>
		/// The descendant operations segments.
		/// </summary>
		public virtual IEnumerable<OperationsSegment> Descendants => ProcessDefinitionMixIn.Descendants;

	    /// <summary>
	    /// Indicates the type of physical process the operations segment represents.
	    /// </summary>
	    public virtual ExtensibleEnum<ProcessType>? ProcessType
		{
			get => ProcessDefinitionMixIn.ProcessType;
			set => ProcessDefinitionMixIn.ProcessType = value;
		}

	    /// <summary>
	    /// The typical process execution duration of the operations segment.
	    /// </summary>
	    public virtual TimeSpan? Duration { get => ProcessDefinitionMixIn.Duration; set => ProcessDefinitionMixIn.Duration = value; }

	    /// <summary>
	    /// The date/time at which the operations segment was created.
	    /// </summary>
	    public virtual DateTime PublishedDate => ProcessDefinitionMixIn.PublishedDate;

	    /// <summary>
	    /// The operations segment version.
	    /// </summary>
	    public virtual string Version { get => ProcessDefinitionMixIn.Version; set => ProcessDefinitionMixIn.Version = value; }

	    /// <summary>
	    /// Gets the descendant material specifications of the operations segment.
	    /// </summary>
	    /// <remarks>
	    /// Descendants are provided only for material specifications because the other types of resource specifications are single-level.
	    /// </remarks>
	    public virtual IEnumerable<MaterialSpecification<OperationsSegment>> AllMaterialSpecifications =>
	        ProcessDefinitionMixIn.AllMaterialSpecifications;

	    /// <summary>
	    /// Gets the immediate material specifications of the operations segment.
	    /// </summary>
	    public virtual IEnumerable<MaterialSpecification<OperationsSegment>> MaterialSpecifications => ProcessDefinitionMixIn.MaterialSpecifications;

	    /// <summary>
	    /// Gets the immediate equipment specifications of the operations segment.
	    /// </summary>
	    public virtual IEnumerable<EquipmentSpecification<OperationsSegment>> EquipmentSpecifications => ProcessDefinitionMixIn.EquipmentSpecifications;

	    /// <summary>
	    /// Gets the immediate physical asset specifications of the operations segment.
	    /// </summary>
	    public virtual IEnumerable<PhysicalAssetSpecification<OperationsSegment>> PhysicalAssetSpecifications => ProcessDefinitionMixIn.PhysicalAssetSpecifications;

	    /// <summary>
	    /// Gets the immediate personnel specifications of the process definition.
	    /// </summary>
	    public virtual IEnumerable<PersonnelSpecification<OperationsSegment>> PersonnelSpecifications => ProcessDefinitionMixIn.PersonnelSpecifications;

	    /// <summary>
	    /// The operations segment at the top of the operations segment hierarchy to which the operations segment belongs.
	    /// </summary>
	    public virtual OperationsSegment Root => ProcessDefinitionMixIn.Root;

	    /// <summary>
	    /// Indicates whether the operations segment is at the top of the operations segment hierarchy to which it belongs.
	    /// </summary>
	    public virtual bool IsRoot => ProcessDefinitionMixIn.IsRoot;

	    /// <summary>
	    /// The descendant properties of the operations segment.
	    /// </summary>
	    public virtual IEnumerable<BasicProperty<OperationsSegment>> AllProperties => WithPropertiesMixIn.AllProperties;

	    /// <summary>
	    /// Gets the immediate properties of the operations segment.
	    /// </summary>
	    public virtual IEnumerable<BasicProperty<OperationsSegment>> Properties => WithPropertiesMixIn.Properties;

        /// <summary>
        /// The process segments backing store
        /// </summary>
	    private readonly ISet<ProcessSegment> _processSegments = new HashSet<ProcessSegment>();

        /// <summary>
        ///     Gets the process segment ID's.
        /// </summary>
        /// <value>The process segment ID's.</value>
        public virtual IEnumerable<ProcessSegment> ProcessSegments => _processSegments;

	    /// <summary>
	    ///     The segment dependencies backing store.
	    /// </summary>
	    private readonly ISet<SegmentDependency<OperationsSegment>> _segmentDependencies = new HashSet<SegmentDependency<OperationsSegment>>();

	    /// <summary>
        ///     Gets the segment dependencies.
        /// </summary>
        /// <value>The segment dependencies.</value>
        public virtual IEnumerable<SegmentDependency<OperationsSegment>> SegmentDependencies => _segmentDependencies;

        /// <summary>
        /// Backing field for the operations definition
        /// </summary>
	    private OperationsDefinition _operationsDefinition;

	    /// <summary>
	    /// The operations definition this ProcessSegment sits in
	    /// </summary>
	    public virtual OperationsDefinition OperationsDefinition
	    {
	        get => _operationsDefinition;
	        set
	        {
		        if (_operationsDefinition?.OperationsSegments.Contains(this) ?? false)
		        {
			        _operationsDefinition?.RemoveOperationsSegment(this);
		        }
	            _operationsDefinition = value;
		        if (!_operationsDefinition?.OperationsSegments.Contains(this) ?? true)
		        {
			        _operationsDefinition?.AddOperationsSegment(this);
		        }
			}
	    }

		#endregion

		/// <summary>
		/// Initialises a new instance of the <see cref="OperationsSegment" /> class.
		/// </summary>
		protected OperationsSegment()
		{
			_processDefinitionMixIn = new ProcessDefinitionMixIn<OperationsSegment>(this);
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<OperationsSegment>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="OperationsSegment" /> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new operations segment.</param>
		/// <param name="externalId">The external ID of the new operations segment.</param>
		public OperationsSegment(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
            _processDefinitionMixIn = new ProcessDefinitionMixIn<OperationsSegment>(this);
            _withPropertiesMixIn = new WithBasicPropertiesMixIn<OperationsSegment>(this);
		}

        #region Members
		
        /// <summary>
        ///     Adds the process segment. No duplicates or null Values are permitted.
        /// </summary>
        /// <param name="processSegment">The process segment identifier.</param>
        /// <exception cref="ArgumentNullException">processSegmentID</exception>
        /// <exception cref="ArgumentException"></exception>
        public virtual void AddProcessSegment(ProcessSegment processSegment) => _processSegments.Add(processSegment);

        /// <summary>
        ///     Adds the segment dependency. No duplicates or null Values are permitted.
        /// </summary>
        /// <param name="segmentDependency">The segment dependency.</param>
        /// <exception cref="ArgumentNullException">segmentDependency</exception>
        /// <exception cref="ArgumentException"></exception>
        public virtual void AddSegmentDependency(SegmentDependency<OperationsSegment> segmentDependency)
            => _segmentDependencies.Add(segmentDependency);

	    /// <summary>
	    ///     Removes the segment dependency. No duplicates or null Values are permitted.
	    /// </summary>
	    /// <param name="segmentDependency">The segment dependency.</param>
	    /// <exception cref="ArgumentNullException">segmentDependency</exception>
	    /// <exception cref="ArgumentException"></exception>
	    public virtual void RemoveSegmentDependency(SegmentDependency<OperationsSegment> segmentDependency)
	        => _segmentDependencies.Remove(segmentDependency);


		/// <summary>
		/// Returns whether a given operations segment is an ascendant of the operations segment.
		/// </summary>
		/// <param name="operationsSegment">The operations segment to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given operations segment is an ascendant of the operations segment; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(OperationsSegment operationsSegment)
		{
			return ProcessDefinitionMixIn.IsAscendantOf(operationsSegment);
		}

		/// <summary>
		/// Returns whether a given operations segment is a descendant of the operations segment.
		/// </summary>
		/// <param name="operationsSegment">The operations segment to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given operations segment is a descendant of the operations segment; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(OperationsSegment operationsSegment)
		{
			return ProcessDefinitionMixIn.IsDescendantOf(operationsSegment);
		}

		/// <summary>
		/// Returns whether the given operations segment can be added as a child operations segment.
		/// </summary>
		/// <param name="operationsSegment">The operations segment to evaluate.</param>
		/// <returns><c>true</c> if the given operations segment can be added as a child operations segment; otherwise <c>false</c>.</returns>
		public virtual bool CanAdd(OperationsSegment operationsSegment)
		{
			return ProcessDefinitionMixIn.CanAdd(operationsSegment);
		}

		/// <summary>
		/// Adds a child operations segment to the operations segment.
		/// </summary>
		/// <param name="child">The child operations segment to add.</param>
		public virtual void AddChild(OperationsSegment child)
	    {
	        ProcessDefinitionMixIn.AddChild(child);
	    }

	    /// <summary>
	    /// Removes a child operations segment from the operations segment.
	    /// </summary>
	    /// <param name="child">The child operations segment to remove.</param>
	    public virtual void RemoveChild(OperationsSegment child)
	    {
	        ProcessDefinitionMixIn.RemoveChild(child);
	    }

	    /// <summary>
	    /// Adds a new material specification to the operations segment.
	    /// </summary>
	    /// <param name="materialSpecification">The material specification to add. Must not have been added to any other operations segment.</param>
	    /// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
	    /// <c>null</c> to add the given material specification directly under the operations segment.</param>
	    public virtual void AddMaterialSpecification(MaterialSpecification<OperationsSegment> materialSpecification, MaterialSpecification<OperationsSegment> parentMaterialSpecification = null)
	    {
	        ProcessDefinitionMixIn.AddMaterialSpecification(materialSpecification, parentMaterialSpecification);
	    }

	    /// <summary>
	    /// Adds a new material specification to the operations segment.
	    /// </summary>
	    /// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
	    /// <param name="externalId">The external ID of the new material specification.</param>
	    /// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
	    /// <c>null</c> to add the given material specification directly under the operations segment.</param>
	    public virtual MaterialSpecification<OperationsSegment> AddMaterialSpecification(HierarchyScope hierarchyScope, string externalId, MaterialSpecification<OperationsSegment> parentMaterialSpecification = null)
	    {
	        return ProcessDefinitionMixIn.AddMaterialSpecification(hierarchyScope, externalId, parentMaterialSpecification);
	    }

	    /// <summary>
	    /// Removes the given material specification from the operations segment.
	    /// </summary>
	    /// <param name="materialSpecification">The material specification to remove.</param>
	    /// <param name="removeEntirely">A value of <c>true</c> will remove the material specification entirely from the operations segment, 
	    /// removing it from all descendant material specifications.  A value of <c>false</c> will remove the material specification
	    /// only from the <see cref="MaterialSpecifications"/> of the operations segment.</param>
	    public virtual void RemoveMaterialSpecification(MaterialSpecification<OperationsSegment> materialSpecification, bool removeEntirely = false)
	    {
	        ProcessDefinitionMixIn.RemoveMaterialSpecification(materialSpecification, removeEntirely);
	    }

	    /// <summary>
	    /// Adds a new equipment specification to the operations segment.
	    /// </summary>
	    /// <param name="equipmentSpecification">The equipment specification to add. Must not have been added to any other operations segment.</param>
	    public virtual void AddEquipmentSpecification(EquipmentSpecification<OperationsSegment> equipmentSpecification)
	    {
	        ProcessDefinitionMixIn.AddEquipmentSpecification(equipmentSpecification);
	    }

	    /// <summary>
	    /// Adds a new equipment specification to the operations segment.
	    /// </summary>
	    /// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
	    /// <param name="externalId">The external ID of the new equipment specification.</param>
	    /// <returns>The new equipment specification.</returns>
	    public virtual EquipmentSpecification<OperationsSegment> AddEquipmentSpecification(HierarchyScope hierarchyScope, string externalId)
	    {
	        return ProcessDefinitionMixIn.AddEquipmentSpecification(hierarchyScope, externalId);
	    }

	    /// <summary>
	    /// Removes the given equipment specification from the operations segment.
	    /// </summary>
	    /// <param name="equipmentSpecification">The equipment specification to remove.</param>
	    public virtual void RemoveEquipmentSpecification(EquipmentSpecification<OperationsSegment> equipmentSpecification)
	    {
	        ProcessDefinitionMixIn.RemoveEquipmentSpecification(equipmentSpecification);
	    }

	    /// <summary>
	    /// Adds a new physical asset specification to the operations segment.
	    /// </summary>
	    /// <param name="physicalAssetSpecification">The physical asset specification to add. Must not have been added to any other 
	    /// operations segment.</param>
	    public virtual void AddPhysicalAssetSpecification(PhysicalAssetSpecification<OperationsSegment> physicalAssetSpecification)
	    {
	        ProcessDefinitionMixIn.AddPhysicalAssetSpecification(physicalAssetSpecification);
	    }

	    /// <summary>
	    /// Adds a new physical asset specification to the operations segment.
	    /// </summary>
	    /// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
	    /// <param name="externalId">The external ID of the new physical asset specification.</param>
	    /// <returns>The new physical asset specification.</returns>
	    public virtual PhysicalAssetSpecification<OperationsSegment> AddPhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId)
	    {
	        return ProcessDefinitionMixIn.AddPhysicalAssetSpecification(hierarchyScope, externalId);
	    }

        /// <summary>
        /// Removes the given material specification from the operations segment.
        /// </summary>
        /// <param name="physicalAssetSpecification">The physical asset specification to remove.</param>
        public virtual void RemovePhysicalAssetSpecification(PhysicalAssetSpecification<OperationsSegment> physicalAssetSpecification)
	    {
	        ProcessDefinitionMixIn.RemovePhysicalAssetSpecification(physicalAssetSpecification);
	    }

	    /// <summary>
	    /// Adds a new personnel specification directly to the operations segment.
	    /// </summary>
	    /// <param name="personnelSpecification">The personnel specification to add. Must not have been added to any other operations segment.</param>
	    public virtual void AddPersonnelSpecification(PersonnelSpecification<OperationsSegment> personnelSpecification)
	    {
	        ProcessDefinitionMixIn.AddPersonnelSpecification(personnelSpecification);
	    }

	    /// <summary>
	    /// Adds a new personnel specification to the operations segment.
	    /// </summary>
	    /// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
	    /// <param name="externalId">The external ID of the new personnel specification.</param>
	    /// <returns>The new personnel specification.</returns>
	    public virtual PersonnelSpecification<OperationsSegment> AddPersonnelSpecification(HierarchyScope hierarchyScope, string externalId)
	    {
	        return ProcessDefinitionMixIn.AddPersonnelSpecification(hierarchyScope, externalId);
	    }

	    /// <summary>
	    /// Removes the given personnel specification from the operations segment.
	    /// </summary>
	    /// <param name="personnelSpecification">The personnel specification to remove.</param>
	    public virtual void RemovePersonnelSpecification(PersonnelSpecification<OperationsSegment> personnelSpecification)
	    {
	        ProcessDefinitionMixIn.RemovePersonnelSpecification(personnelSpecification);
	    }

	    /// <summary>
	    /// Adds a new property to the object, optionally under a given parent property.
	    /// </summary>
	    /// <param name="externalId">The external ID of the new property.</param>
	    /// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
	    /// <param name="parentProperty">The parent property under which to add the new property, or <c>null</c> to add directly
	    /// under the parent object.</param>
	    /// <returns>The newly added property.</returns>
	    public virtual BasicProperty<OperationsSegment> AddProperty(string externalId, Value value = null, BasicProperty<OperationsSegment> parentProperty = null)
	    {
	        return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
	    }

	    /// <summary>
	    /// Returns whether the operations segment has a property with a given external ID.
	    /// </summary>
	    /// <param name="externalId">The external ID for which to search.</param>
	    /// <returns><c>true</c> if the operations segment has a property with the given external ID; otherwise <c>false</c>.</returns>
	    public virtual bool HasPropertyWithId(string externalId)
	    {
	        return WithPropertiesMixIn.HasPropertyWithId(externalId);
	    }

	    /// <summary>
	    /// Removes the given property from the operations segment.
	    /// </summary>
	    /// <param name="property">The property to remove.</param>
	    public virtual void RemoveProperty(BasicProperty<OperationsSegment> property)
	    {
	        WithPropertiesMixIn.RemoveProperty(property);
	    }

	    #endregion
    }
}