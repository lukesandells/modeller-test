﻿using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// Specifies a physical asset class for a process definition (see <see cref="IProcessDefinition{TProcessDefinition}"/>).
	/// </summary>
	public class PhysicalAssetSpecification<TOfObject> : ResourceSpecification<PhysicalAssetSpecification<TOfObject>, TOfObject>
		where TOfObject : class, IProcessDefinition<TOfObject>
	{
		/// <summary>
		/// The specified physical asset class.
		/// </summary>
		public virtual PhysicalAssetClass PhysicalAssetClass { get; set; }

		/// <summary>
		/// Specifies how the physical asset resource is used.
		/// </summary>
		public virtual string PhysicalAssetUse
		{
			get => _physicalAssetUse;
			set => _physicalAssetUse = value ?? string.Empty;
		}
		private string _physicalAssetUse = string.Empty;

		/// <summary>
		/// Initialises a new instance of the <see cref="PhysicalAssetSpecification{TOfObject}"/> class.
		/// </summary>
		protected PhysicalAssetSpecification() {}

		/// <summary>
		/// Initialises a new instance of the <see cref="PhysicalAssetSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
		/// <param name="externalId">The external ID of the new physcial asset specification.</param>
		public PhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="PhysicalAssetSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
		/// <param name="externalId">The external ID of the new physical asset specification.</param>
		/// <param name="quantity">The quantity specified by the new physical asset specification.</param>
		public PhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId, Quantity quantity)
			: base(hierarchyScope, externalId, quantity)
		{
		}
	}
}