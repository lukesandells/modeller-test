﻿namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	///     Interface for objects with a process definition mix in
	/// </summary>
	/// <typeparam name="TProcess">The type of process</typeparam>
	public interface IWithProcessDefinitionMixIn<TProcess> where TProcess : HierarchyScopedObject, IProcessDefinition<TProcess>, IWithProcessDefinitionMixIn<TProcess>
	{
		#region  Fields/Properties

		/// <summary>
		///     The process definition mix in of this process
		/// </summary>
		ProcessDefinitionMixIn<TProcess> ProcessDefinitionMixIn { get; }

		#endregion
	}
}