﻿using System;
using System.Collections.Generic;
using MI.Framework;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	///     Class ProcessSegment.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ModelObject"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Domain.Client.ISimpleHierarchy{MI.Modeller.Domain.Client.ProcessSegment}"</cref>
	/// </seealso>
	/// Class ProcessSegment.
	/// All properties in this class are readonly.
	/// All collection type properties are wrapped, and only the Addxxx methods are exposed. No duplicate values may be
	/// added
	public class ProcessSegment : HierarchyScopedObject, IProcessDefinition<ProcessSegment>, IWithBasicProperties<ProcessSegment>, IWithProcessDefinitionMixIn<ProcessSegment>
    {
		#region  Fields

		/// <summary>
		/// The process definition mix-in.
		/// </summary>
		public virtual ProcessDefinitionMixIn<ProcessSegment> ProcessDefinitionMixIn => _processDefinitionMixIn;
		private readonly ProcessDefinitionMixIn<ProcessSegment> _processDefinitionMixIn;

		/// <summary>
		/// The "with basic properites" mix-in.
		/// </summary>
		protected virtual WithBasicPropertiesMixIn<ProcessSegment> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<ProcessSegment> _withPropertiesMixIn;

        /// <summary>
        /// The parent process segment.
        /// </summary>
        public virtual ProcessSegment Parent => ProcessDefinitionMixIn.Parent;

        /// <summary>
        /// The child process segments.
        /// </summary>
        public virtual IEnumerable<ProcessSegment> Children => ProcessDefinitionMixIn.Children;

		/// <summary>
		/// The ascendant process segments.
		/// </summary>
		public virtual IEnumerable<ProcessSegment> Ascendants => ProcessDefinitionMixIn.Ascendants;

		/// <summary>
		/// The descendant process segments.
		/// </summary>
		public virtual IEnumerable<ProcessSegment> Descendants => ProcessDefinitionMixIn.Descendants;

		/// <summary>
		/// Indicates the type of physical process the process segment represents.
		/// </summary>
		public virtual ExtensibleEnum<ProcessType>? ProcessType { get => ProcessDefinitionMixIn.ProcessType; set => ProcessDefinitionMixIn.ProcessType = value; }

        /// <summary>
        /// The typical process execution duration of the process segment.
        /// </summary>
        public virtual TimeSpan? Duration { get => ProcessDefinitionMixIn.Duration; set => ProcessDefinitionMixIn.Duration = value; }

        /// <summary>
        /// The date/time at which the process segment was created.
        /// </summary>
        public virtual DateTime PublishedDate => ProcessDefinitionMixIn.PublishedDate;

        /// <summary>
        /// The process segment version.
        /// </summary>
        public virtual string Version { get => ProcessDefinitionMixIn.Version; set => ProcessDefinitionMixIn.Version = value; }

        /// <summary>
        /// Gets the descendant material specifications of the process segment.
        /// </summary>
        /// <remarks>
        /// Descendants are provided only for material specifications because the other types of resource specifications are single-level.
        /// </remarks>
        public virtual IEnumerable<MaterialSpecification<ProcessSegment>> AllMaterialSpecifications =>
            ProcessDefinitionMixIn.AllMaterialSpecifications;

        /// <summary>
        /// Gets the immediate material specifications of the process segment.
        /// </summary>
        public virtual IEnumerable<MaterialSpecification<ProcessSegment>> MaterialSpecifications => ProcessDefinitionMixIn.MaterialSpecifications;

        /// <summary>
        /// Gets the immediate equipment specifications of the process segment.
        /// </summary>
        public virtual IEnumerable<EquipmentSpecification<ProcessSegment>> EquipmentSpecifications => ProcessDefinitionMixIn.EquipmentSpecifications;

        /// <summary>
        /// Gets the immediate physical asset specifications of the process segment.
        /// </summary>
        public virtual IEnumerable<PhysicalAssetSpecification<ProcessSegment>> PhysicalAssetSpecifications => ProcessDefinitionMixIn.PhysicalAssetSpecifications;

        /// <summary>
        /// Gets the immediate personnel specifications of the process definition.
        /// </summary>
        public virtual IEnumerable<PersonnelSpecification<ProcessSegment>> PersonnelSpecifications => ProcessDefinitionMixIn.PersonnelSpecifications;

        /// <summary>
        /// The process segment at the top of the process segment hierarchy to which the process segment belongs.
        /// </summary>
        public virtual ProcessSegment Root => ProcessDefinitionMixIn.Root;

        /// <summary>
        /// Indicates whether the process segment is at the top of the process segment hierarchy to which it belongs.
        /// </summary>
        public virtual bool IsRoot => ProcessDefinitionMixIn.IsRoot;

        /// <summary>
        /// The descendant properties of the process segment.
        /// </summary>
        public virtual IEnumerable<BasicProperty<ProcessSegment>> AllProperties => _withPropertiesMixIn.AllProperties;

        /// <summary>
        /// Gets the immediate properties of the process segment.
        /// </summary>
        public virtual IEnumerable<BasicProperty<ProcessSegment>> Properties => _withPropertiesMixIn.Properties;

        /// <summary>
        /// Backing field for Segment dependencies 
        /// </summary>
        private readonly ISet<SegmentDependency<ProcessSegment>> _segmentDependencies = new HashSet<SegmentDependency<ProcessSegment>>();

        /// <summary>
        ///     Gets the segment dependencies.
        /// </summary>
        /// <value>The segment dependencies.</value>
        public virtual IEnumerable<SegmentDependency<ProcessSegment>> SegmentDependencies => _segmentDependencies;

		#endregion

		/// <summary>
		/// Initialises a new instance of the <see cref="ProcessSegment" /> class.
		/// </summary>
		protected ProcessSegment()
		{
			_processDefinitionMixIn = new ProcessDefinitionMixIn<ProcessSegment>(this);
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<ProcessSegment>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ProcessSegment" /> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new process segment.</param>
		/// <param name="externalId">The external ID of the new process segment.</param>
		public ProcessSegment(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
		    _processDefinitionMixIn = new ProcessDefinitionMixIn<ProcessSegment>(this);
		    _withPropertiesMixIn = new WithBasicPropertiesMixIn<ProcessSegment>(this);
        }



        #region Members

        /// <summary>
        ///     Adds the segment dependency. No duplicates or null Values are permitted.
        /// </summary>
        /// <param name="segmentDependency">The segment dependency.</param>
        /// <exception cref="ArgumentNullException">segmentDependency</exception>
        /// <exception cref="ArgumentException"></exception>
        public virtual void AddSegmentDependency(SegmentDependency<ProcessSegment> segmentDependency)
            => _segmentDependencies.Add(segmentDependency);

        /// <summary>
        ///     Removes the segment dependency. No duplicates or null Values are permitted.
        /// </summary>
        /// <param name="segmentDependency">The segment dependency.</param>
        /// <exception cref="ArgumentNullException">segmentDependency</exception>
        /// <exception cref="ArgumentException"></exception>
        public virtual void RemoveSegmentDependency(SegmentDependency<ProcessSegment> segmentDependency)
            => _segmentDependencies.Remove(segmentDependency);

		/// <summary>
		/// Returns whether a given process segment is an ascendant of the process segment.
		/// </summary>
		/// <param name="processSegment">The process segment to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given process segment is an ascendant of the process segment; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(ProcessSegment processSegment)
		{
			return ProcessDefinitionMixIn.IsAscendantOf(processSegment);
		}

		/// <summary>
		/// Returns whether a given process segment is a descendant of the process segment.
		/// </summary>
		/// <param name="processSegment">The process segment to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given process segment is a descendant of the process segment; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(ProcessSegment processSegment)
		{
			return ProcessDefinitionMixIn.IsDescendantOf(processSegment);
		}

		/// <summary>
		/// Returns whether the given process segment can be added as a child process segment.
		/// </summary>
		/// <param name="processSegment">The process segment to evaluate.</param>
		/// <returns><c>true</c> if the given process segment can be added as a child process segment; otherwise <c>false</c>.</returns>
		public virtual bool CanAdd(ProcessSegment processSegment)
		{
			return ProcessDefinitionMixIn.CanAdd(processSegment);
		}

		/// <summary>
		/// Adds a child process segment to the process segment.
		/// </summary>
		/// <param name="child">The child process segment to add.</param>
		public virtual void AddChild(ProcessSegment child)
        {
            ProcessDefinitionMixIn.AddChild(child);
        }

        /// <summary>
        /// Removes a child process segment from the process segment.
        /// </summary>
        /// <param name="child">The child process segment to remove.</param>
        public virtual void RemoveChild(ProcessSegment child)
        {
            ProcessDefinitionMixIn.RemoveChild(child);
        }

        /// <summary>
        /// Adds a new material specification to the process segment.
        /// </summary>
        /// <param name="materialSpecification">The material specification to add. Must not have been added to any other process segment.</param>
        /// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
        /// <c>null</c> to add the given material specification directly under the process segment.</param>
        public virtual void AddMaterialSpecification(MaterialSpecification<ProcessSegment> materialSpecification, MaterialSpecification<ProcessSegment> parentMaterialSpecification = null)
        {
            ProcessDefinitionMixIn.AddMaterialSpecification(materialSpecification, parentMaterialSpecification);
        }

        /// <summary>
        /// Adds a new material specification to the process segment.
        /// </summary>
        /// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
        /// <param name="externalId">The external ID of the new material specification.</param>
        /// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
        /// <c>null</c> to add the given material specification directly under the process segment.</param>
        public virtual MaterialSpecification<ProcessSegment> AddMaterialSpecification(HierarchyScope hierarchyScope, string externalId, MaterialSpecification<ProcessSegment> parentMaterialSpecification = null)
        {
            return ProcessDefinitionMixIn.AddMaterialSpecification(hierarchyScope, externalId, parentMaterialSpecification);
        }

        /// <summary>
        /// Removes the given material specification from the process segment.
        /// </summary>
        /// <param name="materialSpecification">The material specification to remove.</param>
        /// <param name="removeEntirely">A value of <c>true</c> will remove the material specification entirely from the process segment, 
        /// removing it from all descendant material specifications.  A value of <c>false</c> will remove the material specification
        /// only from the <see cref="MaterialSpecifications"/> of the process segment.</param>
        public virtual void RemoveMaterialSpecification(MaterialSpecification<ProcessSegment> materialSpecification, bool removeEntirely = false)
        {
            ProcessDefinitionMixIn.RemoveMaterialSpecification(materialSpecification, removeEntirely);
        }

        /// <summary>
        /// Adds a new equipment specification to the process segment.
        /// </summary>
        /// <param name="equipmentSpecification">The equipment specification to add. Must not have been added to any other process segment.</param>
        public virtual void AddEquipmentSpecification(EquipmentSpecification<ProcessSegment> equipmentSpecification)
        {
            ProcessDefinitionMixIn.AddEquipmentSpecification(equipmentSpecification);
        }

        /// <summary>
        /// Adds a new equipment specification to the process segment.
        /// </summary>
        /// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
        /// <param name="externalId">The external ID of the new equipment specification.</param>
        /// <returns>The new equipment specification.</returns>
        public virtual EquipmentSpecification<ProcessSegment> AddEquipmentSpecification(HierarchyScope hierarchyScope, string externalId)
        {
            return ProcessDefinitionMixIn.AddEquipmentSpecification(hierarchyScope, externalId);
        }

        /// <summary>
        /// Removes the given equipment specification from the process segment.
        /// </summary>
        /// <param name="equipmentSpecification">The equipment specification to remove.</param>
        public virtual void RemoveEquipmentSpecification(EquipmentSpecification<ProcessSegment> equipmentSpecification)
        {
            ProcessDefinitionMixIn.RemoveEquipmentSpecification(equipmentSpecification);
        }

        /// <summary>
        /// Adds a new physical asset specification to the process segment.
        /// </summary>
        /// <param name="physicalAssetSpecification">The physical asset specification to add. Must not have been added to any other 
        /// process segment.</param>
        public virtual void AddPhysicalAssetSpecification(PhysicalAssetSpecification<ProcessSegment> physicalAssetSpecification)
        {
            ProcessDefinitionMixIn.AddPhysicalAssetSpecification(physicalAssetSpecification);
        }

        /// <summary>
        /// Adds a new physical asset specification to the process segment.
        /// </summary>
        /// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
        /// <param name="externalId">The external ID of the new physical asset specification.</param>
        /// <returns>The new physical asset specification.</returns>
        public virtual PhysicalAssetSpecification<ProcessSegment> AddPhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId)
        {
            return ProcessDefinitionMixIn.AddPhysicalAssetSpecification(hierarchyScope, externalId);
        }

        /// <summary>
        /// Removes the given material specification from the process segment.
        /// </summary>
        /// <param name="physicalAssetSpecification">The physical asset specification to remove.</param>
        public virtual void RemovePhysicalAssetSpecification(PhysicalAssetSpecification<ProcessSegment> physicalAssetSpecification)
        {
            ProcessDefinitionMixIn.RemovePhysicalAssetSpecification(physicalAssetSpecification);
        }

        /// <summary>
        /// Adds a new personnel specification directly to the process segment.
        /// </summary>
        /// <param name="personnelSpecification">The personnel specification to add. Must not have been added to any other process segment.</param>
        public virtual void AddPersonnelSpecification(PersonnelSpecification<ProcessSegment> personnelSpecification)
        {
            ProcessDefinitionMixIn.AddPersonnelSpecification(personnelSpecification);
        }

        /// <summary>
        /// Adds a new personnel specification to the process segment.
        /// </summary>
        /// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
        /// <param name="externalId">The external ID of the new personnel specification.</param>
        /// <returns>The new personnel specification.</returns>
        public virtual PersonnelSpecification<ProcessSegment> AddPersonnelSpecification(HierarchyScope hierarchyScope, string externalId)
        {
            return ProcessDefinitionMixIn.AddPersonnelSpecification(hierarchyScope, externalId);
        }

        /// <summary>
        /// Removes the given personnel specification from the process segment.
        /// </summary>
        /// <param name="personnelSpecification">The personnel specification to remove.</param>
        public virtual void RemovePersonnelSpecification(PersonnelSpecification<ProcessSegment> personnelSpecification)
        {
            ProcessDefinitionMixIn.RemovePersonnelSpecification(personnelSpecification);
        }

        /// <summary>
        /// Adds a new property to the object, optionally under a given parent property.
        /// </summary>
        /// <param name="externalId">The external ID of the new property.</param>
        /// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
        /// <param name="parentProperty">The parent property under which to add the new property, or <c>null</c> to add directly
        /// under the parent object.</param>
        /// <returns>The newly added property.</returns>
        public virtual BasicProperty<ProcessSegment> AddProperty(string externalId, Value value = null, BasicProperty<ProcessSegment> parentProperty = null)
        {
            return _withPropertiesMixIn.AddProperty(externalId, value, parentProperty);
        }

        /// <summary>
        /// Returns whether the process segment has a property with a given external ID.
        /// </summary>
        /// <param name="externalId">The external ID for which to search.</param>
        /// <returns><c>true</c> if the process segment has a property with the given external ID; otherwise <c>false</c>.</returns>
        public virtual  bool HasPropertyWithId(string externalId)
        {
            return _withPropertiesMixIn.HasPropertyWithId(externalId);
        }

        /// <summary>
        /// Removes the given property from the process segment.
        /// </summary>
        /// <param name="property">The property to remove.</param>
        public virtual void RemoveProperty(BasicProperty<ProcessSegment> property)
        {
            _withPropertiesMixIn.RemoveProperty(property);
        }

        #endregion
    }
}