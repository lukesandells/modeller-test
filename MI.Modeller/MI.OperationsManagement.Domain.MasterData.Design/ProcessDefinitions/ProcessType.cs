using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
    /// <summary>
    /// Indicates a type of physical process.
    /// </summary>
    public enum ProcessType
    {
        /// <summary>
        ///     The production
        /// </summary>
        Production,

        /// <summary>
        ///     The maintenance
        /// </summary>
        Maintenance,

        /// <summary>
        ///     The quality
        /// </summary>
        Quality,

        /// <summary>
        ///     The inventory
        /// </summary>
        Inventory,

        /// <summary>
        ///     The mixed
        /// </summary>
        Mixed,

        /// <summary>
        ///     The other
        /// </summary>
		[OtherValue]
        Other
    }
}