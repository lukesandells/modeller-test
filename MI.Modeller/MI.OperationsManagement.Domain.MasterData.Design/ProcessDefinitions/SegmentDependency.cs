﻿using System.Collections.Generic;
using MI.Framework;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	///     Class SegmentDependency.
	/// </summary>
	public class SegmentDependency<TProcessDefinition>: DesignObject 
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>
    {
        #region  Fields

        /// <summary>
        ///     Gets the dependency.
        /// </summary>
        /// <value>The dependency.</value>
        public virtual ExtensibleEnum<DependencyType>? DependencyType
        {
            get => _dependencyType;
            set => _dependencyType = value;
        }
        private ExtensibleEnum<DependencyType>? _dependencyType;

        /// <summary>
        ///     Gets the process segment identifier.
        /// </summary>
        /// <value>The process segment identifier.</value>
        public virtual TProcessDefinition Process
        {
            get => _process;
            set => _process = value;
        }
        private TProcessDefinition _process;

        /// <summary>
	    ///     Gets the timing factors.
	    /// </summary>
	    /// <value>The timing factors.</value>
	    public virtual Value TimingFactor { get => _timingFactor; set => _timingFactor = value; }
		private Value _timingFactor;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="SegmentDependency{TProcessDefinition}" /> class.
        /// </summary>
        /// <param name="externalId">The identifier.</param>
        /// <exception cref="System.ArgumentException">
        ///     Segment DependencyType must have exactly one of ({nameof(segmentId)} or
        ///     {nameof(ProcessSegmentId)})
        /// </exception>
        public SegmentDependency(string externalId) : base(externalId)
        { }

        #endregion
    }
}