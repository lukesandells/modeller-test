﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// This class is mixed in with any object that requires resource specifications (see 
	/// <see cref="ResourceSpecification{TResourceSpecification, TOfObject}"/> ) in order to 
	/// meet that requirement.
	/// </summary>
	/// <typeparam name="TProcessDefinition">The type of object with resource specifications.</typeparam>
	/// <remarks>
	/// Any object using this mix-in to provide resource specifications must implement <see cref="IProcessDefinition{TProcessDefinition}"/> and
	/// delegate all calls to this interface to this mix-in.
	/// </remarks>
	public class ProcessDefinitionMixIn<TProcessDefinition> : IProcessDefinition<TProcessDefinition>
		where TProcessDefinition : HierarchyScopedObject, IProcessDefinition<TProcessDefinition>, IWithProcessDefinitionMixIn<TProcessDefinition>
	{
		/// <summary>
		/// The process definition containing the mix-in.
		/// </summary>
		protected TProcessDefinition ProcessDefinition => _processDefinition;
		private TProcessDefinition _processDefinition;

		/// <summary>
		/// The set of all material specifications belonging to the mix-in.
		/// </summary>
		/// <remarks>
		/// This property returns the set of unique/distinct material specifications belonging to the mix-in.  The underlying/backing collection
		/// will contain duplicate elements where material specifications have multiple parents and are therefore accessible via multiple paths.
		/// </remarks>
		public IEnumerable<MaterialSpecification<TProcessDefinition>> AllMaterialSpecifications => _allMaterialSpecifications.Distinct();
		private ICollection<MaterialSpecification<TProcessDefinition>> _allMaterialSpecifications = 
			new Collection<MaterialSpecification<TProcessDefinition>>();

		/// <summary>
		/// The set of material specifications directly belonging to the mix-in.
		/// </summary>
		public IEnumerable<MaterialSpecification<TProcessDefinition>> MaterialSpecifications => AllMaterialSpecifications
			.Where(ms => ms.IsProcessDefinitionLevel);

		/// <summary>
		/// The set of equipment specifications.
		/// </summary>
		public IEnumerable<EquipmentSpecification<TProcessDefinition>> EquipmentSpecifications => _equipmentSpecifications;
		private ISet<EquipmentSpecification<TProcessDefinition>> _equipmentSpecifications =
			new HashSet<EquipmentSpecification<TProcessDefinition>>();

		/// <summary>
		/// The set of physical asset specifications.
		/// </summary>
		public IEnumerable<PhysicalAssetSpecification<TProcessDefinition>> PhysicalAssetSpecifications => _physicalAssetSpecifications;
		private ISet<PhysicalAssetSpecification<TProcessDefinition>> _physicalAssetSpecifications =
			new HashSet<PhysicalAssetSpecification<TProcessDefinition>>();

		/// <summary>
		/// The set of personnel specifications.
		/// </summary>
		public IEnumerable<PersonnelSpecification<TProcessDefinition>> PersonnelSpecifications => _personnelSpecifications;
		private ISet<PersonnelSpecification<TProcessDefinition>> _personnelSpecifications =
			new HashSet<PersonnelSpecification<TProcessDefinition>>();

		/// <summary>
		/// The parent process definition.
		/// </summary>
		public TProcessDefinition Parent => _parent;

		private TProcessDefinition _parent;

		/// <summary>
		/// The child process definitions.
		/// </summary>
		public IEnumerable<TProcessDefinition> Children => _descendants.Where(pd => pd.Parent == _processDefinition);

		/// <summary>
		/// The ascendant process definitions.
		/// </summary>
		public IEnumerable<TProcessDefinition> Ascendants => _ascendants;
		private ICollection<TProcessDefinition> _ascendants = new Collection<TProcessDefinition>();

		/// <summary>
		/// The descendant process definitions.
		/// </summary>
		public IEnumerable<TProcessDefinition> Descendants => _descendants;
		private ICollection<TProcessDefinition> _descendants = new Collection<TProcessDefinition>();

		/// <summary>
		/// The root process definition.
		/// </summary>
		public TProcessDefinition Root => _root;
		private TProcessDefinition _root;

		/// <summary>
		/// Indicates whether the process definition is at the top of the process definition hierarchy to which it belongs.
		/// </summary>
		public bool IsRoot => _processDefinition == Root;

		/// <summary>
		/// Indicates the type of physical process the process definition represents.
		/// </summary>
		public ExtensibleEnum<ProcessType>? ProcessType { get => _processType; set => _processType = value; }
		private ExtensibleEnum<ProcessType>? _processType;

		/// <summary>
		/// The expected duration of the physical process the process definition represents.
		/// </summary>
		public TimeSpan? Duration { get => _duration; set => _duration = value; }
		private TimeSpan? _duration;

		/// <summary>
		/// The date/time at which the process definition was created.
		/// </summary>
		public DateTime PublishedDate => _publishedDate;
		private DateTime _publishedDate = DateTime.UtcNow;

		/// <summary>
		/// The process definition version.
		/// </summary>
		public string Version { get; set; }

		/// <summary>
		/// Initialises a new instance of the <see cref="ProcessDefinitionMixIn{TObjectWithResourceSpecifications}"/> class.
		/// </summary>
		protected ProcessDefinitionMixIn()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ProcessDefinitionMixIn{TObjectWithResourceSpecifications}"/> class.
		/// </summary>
		/// <param name="processDefinition">The process definition containing the mix-in.</param>
		/// <param name="getProcessDefinitionMixInFunc">Function returning the process definition mix-in for a given process definition.</param>
		public ProcessDefinitionMixIn(TProcessDefinition processDefinition)
		{
			_processDefinition = ArgumentValidation.AssertNotNull(processDefinition, nameof(processDefinition));
			_root = _processDefinition;
		}

		/// <summary>
		/// Returns the process definition mix-in of the given process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition for which to get the process definition mix-in.</param>
		/// <returns>The process definition mix-in of the given process definition.</returns>
		private ProcessDefinitionMixIn<TProcessDefinition> MixInOf(TProcessDefinition processDefinition)
		{
			return processDefinition.ProcessDefinitionMixIn;
		}

		/// <summary>
		/// Returns whether a given process definition is an ascendant of the process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given process definition is an ascendant of the process definition; otherwise <c>false</c>.</returns>
		public bool IsAscendantOf(TProcessDefinition processDefinition)
		{
			processDefinition = ArgumentValidation.AssertNotNull(processDefinition, nameof(processDefinition));
			return MixInOf(processDefinition)._ascendants.Contains(ProcessDefinition);
		}

		/// <summary>
		/// Returns whether a given process definition is a descendant of the process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given process definition is a descendant of the process definition; otherwise <c>false</c>.</returns>
		public bool IsDescendantOf(TProcessDefinition processDefinition)
		{
			return _ascendants.Contains(processDefinition);
		}

		/// <summary>
		/// Returns whether the given process definition can be added as a child process definition.
		/// </summary>
		/// <param name="processDefinition">The process definition to evaluate.</param>
		/// <returns><c>true</c> if the given process definition can be added as a child process definition; otherwise <c>false</c>.</returns>
		public bool CanAdd(TProcessDefinition processDefinition)
		{
			processDefinition = ArgumentValidation.AssertNotNull(processDefinition, nameof(processDefinition));
			return processDefinition != ProcessDefinition && !IsDescendantOf(processDefinition) && !IsAscendantOf(processDefinition);
		}

		/// <summary>
		/// Adds a child process definition to the process definition.
		/// </summary>
		/// <param name="child">The child process definition to add.</param>
		public virtual void AddChild(TProcessDefinition child)
		{
			// All child process definitions must be in the same hierarchy scope or contained within the hierarchy scope of its parent
			child = ArgumentValidation.AssertNotNull(child, nameof(child));
			if (!_processDefinition.HierarchyScope.EqualsOrContains(child.HierarchyScope))
			{
				throw new ArgumentException(ExceptionMessage.ChildProcessDefinitionMustBeInSameOrContainedHierarchyScopeAsParent);
			}

			ObjectHierarchy.OneToMany.AddElement(_processDefinition, child,
				setParentFunc: (pd, value) => MixInOf(pd)._parent = value,
				getChildrenFunc: (pd) => MixInOf(pd).Children,
				getAscendantsFunc: (pd) => MixInOf(pd)._ascendants,
				getDescendantsFunc: (pd) => MixInOf(pd)._descendants);
			MixInOf(child)._root = Root;
		}

		/// <summary>
		/// Removes a child process from the process definition.
		/// </summary>
		/// <param name="child">The child process definition to remove.</param>
		public virtual void RemoveChild(TProcessDefinition child)
		{
			ObjectHierarchy.OneToMany.RemoveElement(_processDefinition, child,
				setParentFunc: (pd, value) => MixInOf(pd)._parent = value,
				getChildrenFunc : (pd) => MixInOf(pd).Children,
				getAscendantsFunc: (pd) => MixInOf(pd)._ascendants,
				getDescendantsFunc: (pd) => MixInOf(pd)._descendants);
			MixInOf(child)._root = child;
		}

		/// <summary>
		/// Adds a new material specification to the process definition.
		/// </summary>
		/// <param name="materialSpecification">The material specification to add. Must not have been added to any other process definition.</param>
		/// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
		/// <c>null</c> to add the given material specification directly under the process definition.</param>
		public void AddMaterialSpecification(MaterialSpecification<TProcessDefinition> materialSpecification,
			MaterialSpecification<TProcessDefinition> parentMaterialSpecification = null)
		{
			// Check the material specification doesn't already belong to an object
			materialSpecification = ArgumentValidation.AssertNotNull(materialSpecification, nameof(materialSpecification));
			if (materialSpecification.ProcessDefinition != null && materialSpecification.ProcessDefinition != _processDefinition)
			{
				throw new ArgumentException(ExceptionMessage.MaterialSpecificationBelongsToAnotherProcessDefinition);
			}

			// Check the material specification isn't already a direct child of this process definition
			if (materialSpecification.IsProcessDefinitionLevel && parentMaterialSpecification == null)
			{
				throw new ArgumentException(ExceptionMessage.MaterialSpecificationAlreadyChildOfProcessDefinition);
			}

			// Check the material specification ID is unique within the process definition hierarchy
			if (Root.AllMaterialSpecifications.Any(ms => ms.ExternalId == materialSpecification.ExternalId && materialSpecification != ms))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.ProcessDefinitionHierarchyContainsMaterialSpecificationWithSameExternalId,
					materialSpecification.ExternalId));
			}

			// Add the material specification to the process definition
			materialSpecification.IsProcessDefinitionLevel = parentMaterialSpecification == null;
			materialSpecification.ProcessDefinition = _processDefinition;
			_allMaterialSpecifications.Add(materialSpecification);
			materialSpecification.AssemblyElements
				.SelectHierarchy(element => element.AssemblyElements)
				.Where(element => !AllMaterialSpecifications.Contains(element))
				.ForEach(element => _allMaterialSpecifications.Add(element));

			// Add to the parent material specification if provided
			if (parentMaterialSpecification != null)
			{
				parentMaterialSpecification.AddAssemblyElement(materialSpecification, false);
			}
		}

		/// <summary>
		/// Adds a new material specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		/// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
		/// <c>null</c> to add the given material specification directly under the process definition.</param>
		/// <returns>The new material specification.</returns>
		public MaterialSpecification<TProcessDefinition> AddMaterialSpecification(HierarchyScope hierarchyScope, string externalId, MaterialSpecification<TProcessDefinition> parentMaterialSpecification = null)
		{
			var materialSpecification = new MaterialSpecification<TProcessDefinition>(hierarchyScope, externalId);
			AddMaterialSpecification(materialSpecification, parentMaterialSpecification);
			return materialSpecification;
		}

		/// <summary>
		/// Removes the given material specification from the process definition.
		/// </summary>
		/// <param name="materialSpecification">The material specification to remove.</param>
		/// <param name="removeEntirely">A value of <c>true</c> will remove the material specification entirely from the process definition, 
		/// removing it from all its parent assemblies.  A value of <c>false</c> will remove the material specification
		/// only from the <see cref="MaterialSpecifications"/> of the process definition.</param>
		public void RemoveMaterialSpecification(MaterialSpecification<TProcessDefinition> materialSpecification, bool removeEntirely = false)
		{
			materialSpecification = ArgumentValidation.AssertNotNull(materialSpecification, nameof(materialSpecification));
			materialSpecification.IsProcessDefinitionLevel = false;
			if (removeEntirely)
			{
				materialSpecification.ParentAssemblies.ForEach(parent => parent.RemoveAssemblyElement(materialSpecification));
			}

			// Recursively remove material specification assembly elements
			if (materialSpecification.ParentAssemblies.Count() == 0)
			{
				materialSpecification.ProcessDefinition = null;
			}

			_allMaterialSpecifications.Remove(materialSpecification);
			materialSpecification.AssemblyElements
				.SelectHierarchy(element => element.AssemblyElements)
				.ForEach(element => RemoveMaterialSpecification(element));
		}

		/// <summary>
		/// Adds a new equipment specification to the process definition.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification to add. Must not have been added to any other object.</param>
		/// <param name="parentEquipmentSpecification">The parent equipment specification to receive the given equipment specification, or 
		/// <c>null</c> to add the given equipment specification directly under the process definition.</param>
		public void AddEquipmentSpecification(EquipmentSpecification<TProcessDefinition> equipmentSpecification)
		{
			// Check the equipment specification doesn't already belong to an object
			equipmentSpecification = ArgumentValidation.AssertNotNull(equipmentSpecification, nameof(equipmentSpecification));
			if (equipmentSpecification.ProcessDefinition != null)
			{
				throw new ArgumentException(ExceptionMessage.EquipmentSpecificationNotNew);
			}

			// Check the equipment specification ID is unique within the process definition
			if (EquipmentSpecifications.Any(es => es.ExternalId == equipmentSpecification.ExternalId))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.ProcessDefinitionContainsEquipmentSpecificationWithSameExternalId,
					equipmentSpecification.ExternalId));
			}

			// Add the equipment specification
			equipmentSpecification.ProcessDefinition = _processDefinition;
			_equipmentSpecifications.Add(equipmentSpecification);
		}

		/// <summary>
		/// Adds a new equipment specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
		/// <param name="externalId">The external ID of the new equipment specification.</param>
		/// <returns>The new equipment specification.</returns>
		public EquipmentSpecification<TProcessDefinition> AddEquipmentSpecification(HierarchyScope hierarchyScope, string externalId)
		{
			var equipmentSpecification = new EquipmentSpecification<TProcessDefinition>(hierarchyScope, externalId);
			AddEquipmentSpecification(equipmentSpecification);
			return equipmentSpecification;
		}

		/// <summary>
		/// Removes the given equipment specification from the process definition.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification to remove.</param>
		public void RemoveEquipmentSpecification(EquipmentSpecification<TProcessDefinition> equipmentSpecification)
		{
			equipmentSpecification = ArgumentValidation.AssertNotNull(equipmentSpecification, nameof(equipmentSpecification));
			equipmentSpecification.ProcessDefinition = null;
			_equipmentSpecifications.Remove(equipmentSpecification);
		}

		/// <summary>
		/// Adds a new physical asset specification to the process definition.
		/// </summary>
		/// <param name="physicalAssetSpecification">The physical asset specification to add. Must not have been added to any other 
		/// process definition.</param>
		public void AddPhysicalAssetSpecification(PhysicalAssetSpecification<TProcessDefinition> physicalAssetSpecification)
		{
			// Check the physical asset specification doesn't already belong to an object
			physicalAssetSpecification = ArgumentValidation.AssertNotNull(physicalAssetSpecification, nameof(physicalAssetSpecification));
			if (physicalAssetSpecification.ProcessDefinition != null)
			{
				throw new ArgumentException(ExceptionMessage.PhysicalAssetSpecificationNotNew);
			}

			// Check the physical asset specification ID is unique within the process definition
			if (EquipmentSpecifications.Any(es => es.ExternalId == physicalAssetSpecification.ExternalId))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.ProcessDefinitionContainsPhysicalAssetSpecificationWithSameExternalId,
					physicalAssetSpecification.ExternalId));
			}

			// Add the physical asset specification
			physicalAssetSpecification.ProcessDefinition = _processDefinition;
			_physicalAssetSpecifications.Add(physicalAssetSpecification);
		}

		/// <summary>
		/// Adds a new physical asset specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
		/// <param name="externalId">The external ID of the new physical asset specification.</param>
		/// <returns>The new physical asset specification.</returns>
		public PhysicalAssetSpecification<TProcessDefinition> AddPhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId)
		{
			var physicalAssetSpecification = new PhysicalAssetSpecification<TProcessDefinition>(hierarchyScope, externalId);
			AddPhysicalAssetSpecification(physicalAssetSpecification);
			return physicalAssetSpecification;
		}

		/// <summary>
		/// Removes the given material specification from the process definition.
		/// </summary>
		/// <param name="physicalAsetSpecification">The physical asset specification to remove.</param>
		public void RemovePhysicalAssetSpecification(PhysicalAssetSpecification<TProcessDefinition> physicalAssetSpecification)
		{
			physicalAssetSpecification = ArgumentValidation.AssertNotNull(physicalAssetSpecification, nameof(physicalAssetSpecification));
			physicalAssetSpecification.ProcessDefinition = null;
			_physicalAssetSpecifications.Remove(physicalAssetSpecification);
		}

		/// <summary>
		/// Adds a new personnel specification directly to the process definition.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification to add. Must not have been added to any other process definition.</param>
		public void AddPersonnelSpecification(PersonnelSpecification<TProcessDefinition> personnelSpecification)
		{
			// Check the personnel specification doesn't already belong to an object
			personnelSpecification = ArgumentValidation.AssertNotNull(personnelSpecification, nameof(personnelSpecification));
			if (personnelSpecification.ProcessDefinition != null)
			{
				throw new ArgumentException(ExceptionMessage.PersonnelSpecificationNotNew);
			}

			// Check the personnel specification ID is unique within the process definition
			if (EquipmentSpecifications.Any(es => es.ExternalId == personnelSpecification.ExternalId))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.ProcessDefinitionContainsPersonnelSpecificationWithSameExternalId,
					personnelSpecification.ExternalId));
			}

			// Add the personnel specification
			personnelSpecification.ProcessDefinition = _processDefinition;
			_personnelSpecifications.Add(personnelSpecification);
		}

		/// <summary>
		/// Adds a new personnel specification to the process definition.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
		/// <param name="externalId">The external ID of the new personnel specification.</param>
		/// <returns>The new personnel specification.</returns>
		public PersonnelSpecification<TProcessDefinition> AddPersonnelSpecification(HierarchyScope hierarchyScope, string externalId)
		{
			var personnelSpecification = new PersonnelSpecification<TProcessDefinition>(hierarchyScope, externalId);
			_personnelSpecifications.Add(personnelSpecification);
			return personnelSpecification;
		}

		/// <summary>
		/// Removes the given personnel specification from the process definition.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification to remove.</param>
		public void RemovePersonnelSpecification(PersonnelSpecification<TProcessDefinition> personnelSpecification)
		{
			personnelSpecification = ArgumentValidation.AssertNotNull(personnelSpecification, nameof(personnelSpecification));
			personnelSpecification.ProcessDefinition = null;
			_personnelSpecifications.Remove(personnelSpecification);
		}
	}
}
