﻿using System.Collections.Generic;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	///     Class OperationsMaterialBill.
	///     non-collection properties are readonly
	///     for collection properties only the Add method is exposed
	///     All paremeters are copied by value for collections
	/// </summary>
	public class OperationsMaterialBill : HierarchyScopedObject
	{
        #region  Fields

		/// <summary>
		///     Gets the operations material bill items.
		/// </summary>
		/// <value>The operations material bill items.</value>
		public virtual IEnumerable<OperationsMaterialBillItem> OperationsMaterialBillItems => _operationsMaterialBillItems;
		private readonly ISet<OperationsMaterialBillItem> _operationsMaterialBillItems = new HashSet<OperationsMaterialBillItem>();

		#endregion

		#region Constructors

		/// <summary>
		///     Initializes a new instance of the <see cref="OperationsMaterialBill" /> class.
		/// </summary>
		/// <param name="hierarchyScope">The HierarchyScope of this OperationsMaterialBill</param>
		/// <param name="externalId">The ExternalId of this OperationsMaterialBill</param>
		public OperationsMaterialBill(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
		}

		#endregion

		#region Members

		/// <summary>
		/// Adds an Operations Material Bill Item to this Operations Material Bill
		/// </summary>
		/// <param name="item">The Operations Material Bill Item to add</param>
		public void AddOperationsMaterialBillItem(OperationsMaterialBillItem item)
		{
			_operationsMaterialBillItems.Add(item);
		}

		/// <summary>
		/// Removes an Operations Material Bill Item from this Operations Material Bill
		/// </summary>
		/// <param name="item">The Operations Material Bill Item to remove</param>
		public void RemoveOperationsMaterialBillItem(OperationsMaterialBillItem item)
		{
			_operationsMaterialBillItems.Remove(item);
		}

		#endregion
	}
}