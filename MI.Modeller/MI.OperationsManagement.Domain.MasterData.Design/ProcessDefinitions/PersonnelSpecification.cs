﻿using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// Specifies a personnel class for a process definition (see <see cref="IProcessDefinition{TProcessDefinition}"/>).
	/// </summary>
	public class PersonnelSpecification<TOfObject> : ResourceSpecification<PersonnelSpecification<TOfObject>, TOfObject>
		where TOfObject : class, IProcessDefinition<TOfObject>
	{
		/// <summary>
		/// The specified personnel class.
		/// </summary>
		public virtual PersonnelClass PersonnelClass { get; set; }

		/// <summary>
		/// Specifies how the personnel resource is used.
		/// </summary>
		public virtual string PersonnelUse
		{
			get => _personnelUse;
			set => _personnelUse = value ?? string.Empty;
		}
		private string _personnelUse = string.Empty;

		/// <summary>
		/// Initialises a new instance of the <see cref="PersonnelSpecification{TOfObject}"/> class.
		/// </summary>
		protected PersonnelSpecification()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="PersonnelSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
		/// <param name="externalId">The external ID of the new personnel specification.</param>
		public PersonnelSpecification(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="PersonnelSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
		/// <param name="externalId">The external ID of the new personnel specification.</param>
		/// <param name="quantity">The quantity specified by the new personnel specification.</param>
		public PersonnelSpecification(HierarchyScope hierarchyScope, string externalId, Quantity quantity)
			: base(hierarchyScope, externalId, quantity)
		{
		}
	}
}