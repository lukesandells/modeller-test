﻿using System;
using System.Collections.Generic;
using MI.Framework;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// A workflow-aware definition of a physical process.
	/// </summary>
	public class WorkMaster : HierarchyScopedObject, IProcessDefinition<WorkMaster>, IWithBasicProperties<WorkMaster>, IWithProcessDefinitionMixIn<WorkMaster>
	{
		/// <summary>
		/// The process definition mix-in.
		/// </summary>
		public virtual ProcessDefinitionMixIn<WorkMaster> ProcessDefinitionMixIn => _processDefinitionMixIn;
		private readonly ProcessDefinitionMixIn<WorkMaster> _processDefinitionMixIn;

		/// <summary>
		/// The "with basic properites" mix-in.
		/// </summary>
		protected WithBasicPropertiesMixIn<WorkMaster> WithPropertiesMixIn => _withPropertiesMixIn;
		private readonly WithBasicPropertiesMixIn<WorkMaster> _withPropertiesMixIn;

		/// <summary>
		/// The parent work master.
		/// </summary>
		public virtual WorkMaster Parent => ProcessDefinitionMixIn.Parent;

		/// <summary>
		/// The child work masters.
		/// </summary>
		public virtual IEnumerable<WorkMaster> Children => ProcessDefinitionMixIn.Children;

		/// <summary>
		/// The ascendant work masters.
		/// </summary>
		public virtual IEnumerable<WorkMaster> Ascendants => ProcessDefinitionMixIn.Ascendants;

		/// <summary>
		/// The descendant work masters.
		/// </summary>
		public virtual IEnumerable<WorkMaster> Descendants => ProcessDefinitionMixIn.Descendants;

		/// <summary>
		/// Indicates the type of physical process the work master represents.
		/// </summary>
		public virtual ExtensibleEnum<ProcessType>? ProcessType { get => ProcessDefinitionMixIn.ProcessType; set => ProcessDefinitionMixIn.ProcessType = value; }

		/// <summary>
		/// The typical process execution duration of the work master.
		/// </summary>
		public virtual TimeSpan? Duration { get => ProcessDefinitionMixIn.Duration; set => ProcessDefinitionMixIn.Duration = value; }

		/// <summary>
		/// The date/time at which the work master was created.
		/// </summary>
		public virtual DateTime PublishedDate => ProcessDefinitionMixIn.PublishedDate;

		/// <summary>
		/// The work master version.
		/// </summary>
		public virtual string Version { get => ProcessDefinitionMixIn.Version; set => ProcessDefinitionMixIn.Version = value; }

		/// <summary>
		/// Gets the descendant material specifications of the work master.
		/// </summary>
		/// <remarks>
		/// Descendants are provided only for material specifications because the other types of resource specifications are single-level.
		/// </remarks>
		public virtual IEnumerable<MaterialSpecification<WorkMaster>> AllMaterialSpecifications => 
			ProcessDefinitionMixIn.AllMaterialSpecifications;

		/// <summary>
		/// Gets the immediate material specifications of the work master.
		/// </summary>
		public virtual IEnumerable<MaterialSpecification<WorkMaster>> MaterialSpecifications => ProcessDefinitionMixIn.MaterialSpecifications;

		/// <summary>
		/// Gets the immediate equipment specifications of the work master.
		/// </summary>
		public virtual IEnumerable<EquipmentSpecification<WorkMaster>> EquipmentSpecifications => ProcessDefinitionMixIn.EquipmentSpecifications;

		/// <summary>
		/// Gets the immediate physical asset specifications of the work master.
		/// </summary>
		public virtual IEnumerable<PhysicalAssetSpecification<WorkMaster>> PhysicalAssetSpecifications => ProcessDefinitionMixIn.PhysicalAssetSpecifications;

		/// <summary>
		/// Gets the immediate personnel specifications of the process definition.
		/// </summary>
		public virtual IEnumerable<PersonnelSpecification<WorkMaster>> PersonnelSpecifications => ProcessDefinitionMixIn.PersonnelSpecifications;

		/// <summary>
		/// The work master at the top of the work master hierarchy to which the work master belongs.
		/// </summary>
		public virtual WorkMaster Root => ProcessDefinitionMixIn.Root;

		/// <summary>
		/// Indicates whether the work master is at the top of the work master hierarchy to which it belongs.
		/// </summary>
		public virtual bool IsRoot => ProcessDefinitionMixIn.IsRoot;

		/// <summary>
		/// The descendant properties of the work master.
		/// </summary>
		public virtual IEnumerable<BasicProperty<WorkMaster>> AllProperties => WithPropertiesMixIn.AllProperties;

		/// <summary>
		/// Gets the immediate properties of the work master.
		/// </summary>
		public virtual IEnumerable<BasicProperty<WorkMaster>> Properties => WithPropertiesMixIn.Properties;

		/// <summary>
		/// The corresponding operations segment for the work master.
		/// </summary>
		public virtual OperationsSegment OperationsSegment { get; set; }

		/// <summary>
		/// The corresponding process segment for the work master.
		/// </summary>
		public virtual ProcessSegment ProcessSegment { get; set; }

		/// <summary>
		/// The workflow specification describing the operational workflow of the work master.
		/// </summary>
		//public virtual WorkflowSpecification WorkflowSpecification { get; set; }
		
		/// <summary>
		/// Initialises a new instance of the <see cref="WorkMaster"/> class.
		/// </summary>
		protected WorkMaster()
		{
			_processDefinitionMixIn = new ProcessDefinitionMixIn<WorkMaster>(this);
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<WorkMaster>(this);
		}

        /// <summary>
        /// Initialises a new instance of the <see cref="WorkMaster"/> class.
        /// </summary>
        /// <param name="hierarchyScope">The hierarchy scope.</param>
        /// <param name="externalId">The external identifier.</param>
        public WorkMaster(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
        {
			_processDefinitionMixIn = new ProcessDefinitionMixIn<WorkMaster>(this);
			_withPropertiesMixIn = new WithBasicPropertiesMixIn<WorkMaster>(this);
		}

		/// <summary>
		/// Returns whether a given work master is an ascendant of the work master.
		/// </summary>
		/// <param name="workMaster">The work master to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given work master is an ascendant of the work master; otherwise <c>false</c>.</returns>
		public virtual bool IsAscendantOf(WorkMaster workMaster)
		{
			return ProcessDefinitionMixIn.IsAscendantOf(workMaster);
		}

		/// <summary>
		/// Returns whether a given work master is a descendant of the work master.
		/// </summary>
		/// <param name="workMaster">The work master to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given work master is a descendant of the work master; otherwise <c>false</c>.</returns>
		public virtual bool IsDescendantOf(WorkMaster workMaster)
		{
			return ProcessDefinitionMixIn.IsDescendantOf(workMaster);
		}

		/// <summary>
		/// Returns whether the given work master can be added as a child work master.
		/// </summary>
		/// <param name="workMaster">The work master to evaluate.</param>
		/// <returns><c>true</c> if the given work master can be added as a child work master; otherwise <c>false</c>.</returns>
		public virtual bool CanAdd(WorkMaster workMaster)
		{
			return ProcessDefinitionMixIn.CanAdd(workMaster);
		}

		/// <summary>
		/// Adds a child work master to the work master.
		/// </summary>
		/// <param name="child">The child work master to add.</param>
		public virtual void AddChild(WorkMaster child)
        {
			ProcessDefinitionMixIn.AddChild(child);
        }

		/// <summary>
		/// Removes a child work master from the work master.
		/// </summary>
		/// <param name="child">The child work master to remove.</param>
		public virtual void RemoveChild(WorkMaster child)
        {
			ProcessDefinitionMixIn.RemoveChild(child);
        }

		/// <summary>
		/// Adds a new material specification to the work master.
		/// </summary>
		/// <param name="materialSpecification">The material specification to add. Must not have been added to any other work master.</param>
		/// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
		/// <c>null</c> to add the given material specification directly under the work master.</param>
		public virtual void AddMaterialSpecification(MaterialSpecification<WorkMaster> materialSpecification, MaterialSpecification<WorkMaster> parentMaterialSpecification = null)
		{
			ProcessDefinitionMixIn.AddMaterialSpecification(materialSpecification, parentMaterialSpecification);
		}

		/// <summary>
		/// Adds a new material specification to the work master.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		/// <param name="parentMaterialSpecification">The parent material specification to receive the given material specification, or 
		/// <c>null</c> to add the given material specification directly under the work master.</param>
		public virtual MaterialSpecification<WorkMaster> AddMaterialSpecification(HierarchyScope hierarchyScope, string externalId, 
			MaterialSpecification<WorkMaster> parentMaterialSpecification = null)
		{
			return ProcessDefinitionMixIn.AddMaterialSpecification(hierarchyScope, externalId, parentMaterialSpecification);
		}

		/// <summary>
		/// Removes the given material specification from the work master.
		/// </summary>
		/// <param name="materialSpecification">The material specification to remove.</param>
		/// <param name="removeEntirely">A value of <c>true</c> will remove the material specification entirely from the work master, 
		/// removing it from all descendant material specifications.  A value of <c>false</c> will remove the material specification
		/// only from the <see cref="MaterialSpecifications"/> of the work master.</param>
		public virtual void RemoveMaterialSpecification(MaterialSpecification<WorkMaster> materialSpecification, bool removeEntirely = false)
		{
			ProcessDefinitionMixIn.RemoveMaterialSpecification(materialSpecification, removeEntirely);
		}

		/// <summary>
		/// Adds a new equipment specification to the work master.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification to add. Must not have been added to any other work master.</param>
		public virtual void AddEquipmentSpecification(EquipmentSpecification<WorkMaster> equipmentSpecification)
		{
			ProcessDefinitionMixIn.AddEquipmentSpecification(equipmentSpecification);
		}

		/// <summary>
		/// Adds a new equipment specification to the work master.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
		/// <param name="externalId">The external ID of the new equipment specification.</param>
		/// <returns>The new equipment specification.</returns>
		public virtual EquipmentSpecification<WorkMaster> AddEquipmentSpecification(HierarchyScope hierarchyScope, string externalId)
		{
			return ProcessDefinitionMixIn.AddEquipmentSpecification(hierarchyScope, externalId);
		}

		/// <summary>
		/// Removes the given equipment specification from the work master.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification to remove.</param>
		public virtual void RemoveEquipmentSpecification(EquipmentSpecification<WorkMaster> equipmentSpecification)
		{
			ProcessDefinitionMixIn.RemoveEquipmentSpecification(equipmentSpecification);
		}

		/// <summary>
		/// Adds a new physical asset specification to the work master.
		/// </summary>
		/// <param name="physicalAssetSpecification">The physical asset specification to add. Must not have been added to any other 
		/// work master.</param>
		public virtual void AddPhysicalAssetSpecification(PhysicalAssetSpecification<WorkMaster> physicalAssetSpecification)
		{
			ProcessDefinitionMixIn.AddPhysicalAssetSpecification(physicalAssetSpecification);
		}

		/// <summary>
		/// Adds a new physical asset specification to the work master.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new physical asset specification.</param>
		/// <param name="externalId">The external ID of the new physical asset specification.</param>
		/// <returns>The new physical asset specification.</returns>
		public virtual PhysicalAssetSpecification<WorkMaster> AddPhysicalAssetSpecification(HierarchyScope hierarchyScope, string externalId)
		{
			return ProcessDefinitionMixIn.AddPhysicalAssetSpecification(hierarchyScope, externalId);
		}

		/// <summary>
		/// Removes the given material specification from the work master.
		/// </summary>
		/// <param name="physicalAssetSpecification">The physical asset specification to remove.</param>
		public virtual void RemovePhysicalAssetSpecification(PhysicalAssetSpecification<WorkMaster> physicalAssetSpecification)
		{
			ProcessDefinitionMixIn.RemovePhysicalAssetSpecification(physicalAssetSpecification);
		}

		/// <summary>
		/// Adds a new personnel specification directly to the work master.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification to add. Must not have been added to any other work master.</param>
		public virtual void AddPersonnelSpecification(PersonnelSpecification<WorkMaster> personnelSpecification)
		{
			ProcessDefinitionMixIn.AddPersonnelSpecification(personnelSpecification);
		}

		/// <summary>
		/// Adds a new personnel specification to the work master.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new personnel specification.</param>
		/// <param name="externalId">The external ID of the new personnel specification.</param>
		/// <returns>The new personnel specification.</returns>
		public virtual PersonnelSpecification<WorkMaster> AddPersonnelSpecification(HierarchyScope hierarchyScope, string externalId)
		{
			return ProcessDefinitionMixIn.AddPersonnelSpecification(hierarchyScope, externalId);
		}

		/// <summary>
		/// Removes the given personnel specification from the work master.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification to remove.</param>
		public virtual void RemovePersonnelSpecification(PersonnelSpecification<WorkMaster> personnelSpecification)
		{
			ProcessDefinitionMixIn.RemovePersonnelSpecification(personnelSpecification);
		}

		/// <summary>
		/// Adds a new property to the object, optionally under a given parent property.
		/// </summary>
		/// <param name="externalId">The external ID of the new property.</param>
		/// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
		/// <param name="parentProperty">The parent property under which to add the new property, or <c>null</c> to add directly
		/// under the parent object.</param>
		/// <returns>The newly added property.</returns>
		public virtual BasicProperty<WorkMaster> AddProperty(string externalId, Value value = null, BasicProperty<WorkMaster> parentProperty = null)
		{
			return WithPropertiesMixIn.AddProperty(externalId, value, parentProperty);
		}

		/// <summary>
		/// Returns whether the work master has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the work master has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
		{
			return WithPropertiesMixIn.HasPropertyWithId(externalId);
		}

		/// <summary>
		/// Removes the given property from the work master.
		/// </summary>
		/// <param name="property">The property to remove.</param>
		public virtual void RemoveProperty(BasicProperty<WorkMaster> property)
		{
			WithPropertiesMixIn.RemoveProperty(property);
		}
	}
}