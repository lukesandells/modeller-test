﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// Specifies equipment or an equipment class for a process definition (see <see cref="IProcessDefinition{TProcessDefinition}"/>).
	/// </summary>
	public class EquipmentSpecification<TOfObject> : ResourceSpecification<EquipmentSpecification<TOfObject>, TOfObject>
		where TOfObject : class, IProcessDefinition<TOfObject>
	{
		/// <summary>
		/// The specified equipment.
		/// </summary>
		public virtual Equipment Equipment { get; set; }
		
        /// <summary>
        /// The specified equipment class.
        /// </summary>
        public virtual EquipmentClass EquipmentClass { get; set; }

		/// <summary>
		/// Specifies how the equipment is used.
		/// </summary>
		public virtual string EquipmentUse
		{
			get => _equipmentUse;
			set => _equipmentUse = value ?? string.Empty;
	    }
		private string _equipmentUse = string.Empty;

        /// <summary>
        /// Initialises a new instance of the <see cref="EquipmentSpecification{TOfObject}"/> class.
        /// </summary>
        protected EquipmentSpecification()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="EquipmentSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
		/// <param name="externalId">The external ID of the new equipment specification.</param>
		public EquipmentSpecification(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="EquipmentSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new equipment specification.</param>
		/// <param name="externalId">The external ID of the new equipment specification.</param>
		/// <param name="quantity">The quantity specified by the new equipment specification.</param>
		public EquipmentSpecification(HierarchyScope hierarchyScope, string externalId, Quantity quantity)
			: base(hierarchyScope, externalId, quantity)
		{
		}
	}
}