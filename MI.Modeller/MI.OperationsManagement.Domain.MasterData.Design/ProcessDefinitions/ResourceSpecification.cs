﻿using System.Collections.Generic;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// Generic base class for all resource specification objects.
	/// </summary>
	/// <typeparam name="TResourceSpecification">The type of resource specification.</typeparam>
	/// <typeparam name="TProcessDefinition">The type of process definition.</typeparam>
	public abstract class ResourceSpecification<TResourceSpecification, TProcessDefinition> : HierarchyScopedObject, IResourceTransaction,
		IWithResourceTransactionProperties<TResourceSpecification, QuantitySpecificResourceTransactionProperty<TResourceSpecification>>
		where TResourceSpecification : ResourceSpecification<TResourceSpecification, TProcessDefinition>
		where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>
	{
		/// <summary>
		/// Mix-in for objects with properties.
		/// </summary>
		protected internal virtual WithResourceTransactionPropertiesMixIn<TResourceSpecification,
			QuantitySpecificResourceTransactionProperty<TResourceSpecification>> WithPropertiesMixIn => _withPropertiesMixIn;
		private WithResourceTransactionPropertiesMixIn<TResourceSpecification, 
			QuantitySpecificResourceTransactionProperty<TResourceSpecification>> _withPropertiesMixIn;

		/// <summary>
		/// The process definition to which the resource specification belongs.
		/// </summary>
		public virtual TProcessDefinition ProcessDefinition { get; protected internal set; }

		/// <summary>
		/// The specified quantity.
		/// </summary>
		public virtual Quantity Quantity { get => _quantity; set => _quantity = value?.Copy(); }
		private Quantity _quantity;

		/// <summary>
		/// The specified spatial definition.
		/// </summary>
		public virtual SpatialDefinition SpatialDefinition { get; set; }

		/// <summary>
		/// The descendant properties of the resource specification.
		/// </summary>
		public virtual IEnumerable<QuantitySpecificResourceTransactionProperty<TResourceSpecification>> AllProperties => 
			WithPropertiesMixIn.AllProperties;
		
		/// <summary>
		/// The properties directly belonging to the resource specification.
		/// </summary>
		public virtual IEnumerable<QuantitySpecificResourceTransactionProperty<TResourceSpecification>> Properties => WithPropertiesMixIn.Properties;

		/// <summary>
		/// Initialises a new instance of the <see cref="ResourceSpecification{TResourceSpecification, TProcessDefinition}"/> class.
		/// </summary>
		protected ResourceSpecification()
		{
			_withPropertiesMixIn = new WithResourceTransactionPropertiesMixIn<TResourceSpecification, 
				QuantitySpecificResourceTransactionProperty<TResourceSpecification>>((TResourceSpecification)this, true);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ResourceSpecification{TResourceSpecification, TProcessDefinition}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new resource specification.</param>
		/// <param name="externalId">The external ID of the new resource specification.</param>
		protected ResourceSpecification(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
			_withPropertiesMixIn = new WithResourceTransactionPropertiesMixIn<TResourceSpecification,
				QuantitySpecificResourceTransactionProperty<TResourceSpecification>>((TResourceSpecification)this, true);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ResourceSpecification{TResourceSpecification, TProcessDefinition}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new resource specification.</param>
		/// <param name="externalId">The external ID of the new resource specification.</param>
		/// <param name="quantity">The quantity specifiedi by the new resource specification.</param>
		protected ResourceSpecification(HierarchyScope hierarchyScope, string externalId, Quantity quantity)
			: base(hierarchyScope, externalId)
		{
			_withPropertiesMixIn = new WithResourceTransactionPropertiesMixIn<TResourceSpecification,
				QuantitySpecificResourceTransactionProperty<TResourceSpecification>>((TResourceSpecification)this, true);
			_quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
		}

		/// <summary>
		/// Adds a new resource transaction property corresponding to the given resource object property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the corresponding basic property belongs.</typeparam>
		/// <param name="correpondingResourceObjectProperty">The type of corresponding resource object property.</param>
		/// <param name="value">The value of the new property.</param>
		/// <returns>The newly added property.</returns>
		public virtual QuantitySpecificResourceTransactionProperty<TResourceSpecification> AddProperty<TResourceObject>(
			BasicProperty<TResourceObject> correpondingResourceObjectProperty, Value value)
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			return WithPropertiesMixIn.AddProperty(correpondingResourceObjectProperty, value);
		}

		/// <summary>
		/// Adds a new resource transaction property corresponding to the given resource object property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the corresponding basic property belongs.</typeparam>
		/// <param name="correpondingResourceObjectProperty">The type of corresponding resource object property.</param>
		/// <param name="value">The value of the new property.</param>
		/// <param name="resourceQuantity">The quantity of the resource object specified by the resource specification that must have the given value.</param>
		/// <returns>The newly added property.</returns>
		public virtual QuantitySpecificResourceTransactionProperty<TResourceSpecification> AddProperty<TResourceObject>(
			BasicProperty<TResourceObject> correpondingResourceObjectProperty, Value value, Quantity resourceQuantity)
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			var property = WithPropertiesMixIn.AddProperty(correpondingResourceObjectProperty, value);
			property.ResourceQuantity = resourceQuantity;
			return property;
		}

		/// <summary>
		/// Gets the set of resource transaction properties corresponding to the given resource object property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the given property belongs.</typeparam>
		/// <param name="resourceObjectProperty">The resource object property for which to find the corresponding resource transaction properties.</param>
		/// <returns>Tthe set of resource transaction properties corresponding to the given resource object property.</returns>
		public virtual IEnumerable<QuantitySpecificResourceTransactionProperty<TResourceSpecification>> PropertiesCorrespondingTo<TResourceObject>(
			BasicProperty<TResourceObject> resourceObjectProperty)
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			return WithPropertiesMixIn.PropertiesCorrespondingTo(resourceObjectProperty);
		}

		/// <summary>
		/// Returns whether the resource specification has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the resource specification has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
		{
			return WithPropertiesMixIn.HasPropertyWithId(externalId);
		}

		/// <summary>
		/// Removes the given property from the resource specification.
		/// </summary>
		/// <param name="property">The property to remove.</param>
		public virtual void RemoveProperty(QuantitySpecificResourceTransactionProperty<TResourceSpecification> property)
		{
			WithPropertiesMixIn.RemoveProperty(property);
		}
	}
}