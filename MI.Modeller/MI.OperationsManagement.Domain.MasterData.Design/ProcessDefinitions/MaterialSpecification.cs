﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
	/// <summary>
	/// Specifies use of a material for a process definition.
	/// </summary>
	public class  MaterialSpecification<TProcessDefinition> 
		: ResourceSpecification<MaterialSpecification<TProcessDefinition>, TProcessDefinition>, IAssembly<MaterialSpecification<TProcessDefinition>>, IWithAssemblyMixIn<MaterialSpecification<TProcessDefinition>>
		where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>
    {
		/// <summary>
		/// Mix-in providing assembly behaviour.
		/// </summary>
		public virtual AssemblyMixIn<MaterialSpecification<TProcessDefinition>> AssemblyMixIn => _assemblyMixIn;
		private readonly AssemblyMixIn<MaterialSpecification<TProcessDefinition>> _assemblyMixIn;

		/// <summary>
		/// The material specifications contained within the material specification assembly.
		/// </summary>
		public virtual IEnumerable<MaterialSpecification<TProcessDefinition>> AssemblyElements => AssemblyMixIn.AssemblyElements;
		
		/// <summary>
		/// The material specification assemblies containing the material specification.
		/// </summary>
		public virtual IEnumerable<MaterialSpecification<TProcessDefinition>> ParentAssemblies => AssemblyMixIn.ParentAssemblies;

		/// <summary>
		/// Indicates whether the relationship between elements of the assembly is permanent or transient.
		/// </summary>
		public virtual ExtensibleEnum<AssemblyRelationship>? AssemblyRelationship
		{
			get => AssemblyMixIn.AssemblyRelationship;
			set => AssemblyMixIn.AssemblyRelationship = value;
		}

        /// <summary>
        /// Indicates whether the assembly is a physical object containing its elements, or if the assembly is a logical grouping of elements
		/// that do not together form a larger object.
        /// </summary>
        public virtual ExtensibleEnum<AssemblyType>? AssemblyType { get => AssemblyMixIn.AssemblyType; set => AssemblyMixIn.AssemblyType = value; }

		/// <summary>
		/// Indicates whether the material specification is a root material specification (i.e. has no parents).
		/// </summary>
		public virtual bool IsRootAssembly => AssemblyMixIn.IsRootAssembly;

		/// <summary>
		/// Indicates whether the material specification is a direct child of the process definition to which it belongs.
		/// </summary>
		public virtual bool IsProcessDefinitionLevel { get; protected internal set; }

		/// <summary>
		/// The specified <see cref="MaterialClass"/>.
		/// </summary>
		public virtual MaterialClass MaterialClass { get; set; }

        /// <summary>
        /// The specified <see cref="MaterialDefinition"/>.
        /// </summary>
        public virtual MaterialDefinition MaterialDefinition { get; set; }

        /// <summary>
        /// The specified material use.
        /// </summary>
        public virtual ExtensibleEnum<MaterialUse>? MaterialUse { get => _materialUse; set => _materialUse = value; }
		private ExtensibleEnum<MaterialUse>? _materialUse;

		/// <summary>
        /// Initialises a new instance of the <see cref="MaterialSpecification{TOfObject}"/> class.
        /// </summary>
        protected MaterialSpecification()
		{
			_assemblyMixIn = new AssemblyMixIn<MaterialSpecification<TProcessDefinition>>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="MaterialSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		public MaterialSpecification(HierarchyScope hierarchyScope, string externalId) 
			: base(hierarchyScope, externalId)
		{
			_assemblyMixIn = new AssemblyMixIn<MaterialSpecification<TProcessDefinition>>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="MaterialSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		/// <param name="quantity">The quantity specified by the new material specification.</param>
		public MaterialSpecification(HierarchyScope hierarchyScope, string externalId, Quantity quantity)
			: base(hierarchyScope, externalId, quantity)
		{
			_assemblyMixIn = new AssemblyMixIn<MaterialSpecification<TProcessDefinition>>(this);
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="MaterialSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		/// <param name="materialUse">The material use specified by the new material specification.</param>
		public MaterialSpecification(HierarchyScope hierarchyScope, string externalId, MaterialUse materialUse)
			: this(hierarchyScope, externalId)
		{
			_materialUse = materialUse;
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="MaterialSpecification{TOfObject}"/> class.
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope of the new material specification.</param>
		/// <param name="externalId">The external ID of the new material specification.</param>
		/// <param name="materialUse">The material use specified by the new material specification.</param>
		/// <param name="quantity">The quantity specified by the new material specification.</param>
		public MaterialSpecification(HierarchyScope hierarchyScope, string externalId, MaterialUse materialUse, Quantity quantity)
			: this(hierarchyScope, externalId, quantity)
		{
			_materialUse = materialUse;
		}

		/// <summary>
		/// Returns whether a given material specification is an ascendant of the material specification.
		/// </summary>
		/// <param name="element">The material specification to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given material specification is an ascendant of the material specification; otherwise <c>false</c>.</returns>
		public virtual bool IsAssemblyAscendantOf(MaterialSpecification<TProcessDefinition> element)
		{
			return AssemblyMixIn.IsAssemblyAscendantOf(element);
		}

		/// <summary>
		/// Returns whether a given material specification is a descendant of the material specification.
		/// </summary>
		/// <param name="element">The material specification to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given material specification is a descendant of the material specification; otherwise <c>false</c>.</returns>
		public virtual bool IsAssemblyDescendantOf(MaterialSpecification<TProcessDefinition> element)
		{
			return AssemblyMixIn.IsAssemblyDescendantOf(element);
		}

		/// <summary>
		/// Returns whether the given element can be added as a child element.
		/// </summary>
		/// <param name="element">The element to evaluate.</param>
		/// <returns><c>true</c> if the given element can be added as a child element; otherwise <c>false</c>.</returns>
		public virtual bool CanAdd(MaterialSpecification<TProcessDefinition> element)
		{
			return AssemblyMixIn.CanAdd(element);
		}

		/// <summary>
		/// Adds a material specification to the assembly.
		/// </summary>
		/// <param name="element">The material specification to add.</param>
		public virtual void AddAssemblyElement(MaterialSpecification<TProcessDefinition> element)
		{
			AddAssemblyElement(element, true);
		}

		/// <summary>
		/// Adds a material specification to the assembly.
		/// </summary>
		/// <param name="element">The material specification to add.</param>
		/// <param name="addToProcessDefinition">Stipulates whether to add the given material specification to the process definition.</param>
		protected internal virtual void AddAssemblyElement(MaterialSpecification<TProcessDefinition> element, bool addToProcessDefinition)
		{
			if (ProcessDefinition != null && addToProcessDefinition)
			{
				if (element.ProcessDefinition != null
					&& !ProcessDefinition.IsAscendantOf(element.ProcessDefinition)
					&& ProcessDefinition != element.ProcessDefinition)
				{
					throw new ArgumentException(ExceptionMessage.MaterialSpecificationMustContainElementsInSameProcessDefinitionHierarchy);
				}

				ProcessDefinition.AddMaterialSpecification(element, this);
			}
			else
			{
				AssemblyMixIn.AddAssemblyElement(element);
			}
		}

		/// <summary>
		/// Removes an material specification from the assembly.
		/// </summary>
		/// <param name="element">The material specification to remove.</param>
		public virtual void RemoveAssemblyElement(MaterialSpecification<TProcessDefinition> element)
		{
			if (!element.ParentAssemblies.Any())
			{
				ProcessDefinition.RemoveMaterialSpecification(element);
			}

			AssemblyMixIn.RemoveAssemblyElement(element);
		}
	}
}