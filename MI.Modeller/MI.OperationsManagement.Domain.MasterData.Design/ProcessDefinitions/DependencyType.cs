using MI.Framework;

namespace MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions
{
    /// <summary>
    ///     The allowable DependencyType Types
    /// </summary>
    public enum DependencyType
    {
        /// <summary>
        ///     The not follow
        /// </summary>
        NotFollow,

        /// <summary>
        ///     The possible parallel
        /// </summary>
        PossibleParallel,

        /// <summary>
        ///     The not in parallel
        /// </summary>
        NotInParallel,

        /// <summary>
        ///     At start
        /// </summary>
        AtStart,

        /// <summary>
        ///     The after start
        /// </summary>
        AfterStart,

        /// <summary>
        ///     The after end
        /// </summary>
        AfterEnd,

        /// <summary>
        ///     The no later after start
        /// </summary>
        NoLaterAfterStart,

        /// <summary>
        ///     The no earlier after start
        /// </summary>
        NoEarlierAfterStart,

        /// <summary>
        ///     The no later after end
        /// </summary>
        NoLaterAfterEnd,

        /// <summary>
        ///     The no earlier after end
        /// </summary>
        NoEarlierAfterEnd,

		/// <summary>
		///     The other
		/// </summary>
		[OtherValue]
		Other
    }
}