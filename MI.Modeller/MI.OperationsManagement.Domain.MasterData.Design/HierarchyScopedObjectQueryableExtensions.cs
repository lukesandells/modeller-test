﻿using System.Linq;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.MasterData.Design
{
	/// <summary>
	/// Extension methods for <see cref="IQueryable{T}"/> of hierarchy scoped objects (see <see cref="HierarchyScopedObject"/>).
	/// </summary>
	public static class HierarchyScopedObjectQueryableExtensions
	{
		/// <summary>
		/// Returns the subset of hierarchy scoped objects that are visible to the hierarchy scope from a given set of 
		/// candidate hierarchy scoped objects.
		/// </summary>
		/// <typeparam name="T">The type of hierarchy scoped object.</typeparam>
		/// <param name="candidates">The set of candidate hierarchy scoped objects to evaluate for visibility from the hierarchy scope.</param>
		/// <returns>The subset of hierarchy scoped objects that are visible to the hierarchy scope from a given set of 
		/// candidate hierarchy scoped objects.</returns>
		public static IQueryable<T> WhereVisibleTo<T>(this IQueryable<T> candidates, HierarchyScope hierarchyScope)
			where T : HierarchyScopedObject
		{
			candidates = ArgumentValidation.AssertNotNull(candidates, nameof(candidates));
			if (hierarchyScope == null)
			{
				return new T[] { }.AsQueryable();
			}

			return candidates.Where(hso => hso.HierarchyScope.Ascendants.Any(a => a.ExternalId == hierarchyScope.ExternalId)
				|| hso.HierarchyScope.Descendants.Any(d => d.ExternalId == hierarchyScope.ExternalId)
				|| hso.HierarchyScope.ExternalId == hierarchyScope.ExternalId);
		}
	}
}
