﻿using MI.Modeller.Domain;
using System.Collections.Generic;

namespace MI.OperationsManagement.Serializer.B2mml
{
	/// <summary>
	/// Interface for exporting nodes. Each exporter will map the exportNode and populate any children as appropriate
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IExporter<out T>
	{
		/// <summary>
		/// Export the given node.
		/// </summary>
		/// <param name="exportNode">The node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported</param>
		/// <returns></returns>
		T Export(ModelElement exportNode, IEnumerable<ModelElement> exportScope);
	}
}
