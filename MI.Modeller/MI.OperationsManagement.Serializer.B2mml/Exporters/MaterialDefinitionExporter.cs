﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the MaterialDefinition class
	/// </summary>
	public class MaterialDefinitionExporter : IExporter<MaterialDefinitionType>
	{
		/// <summary>
		/// The mapper used for mapping materialDefinition
		/// </summary>
		private readonly IMapper<MaterialDefinition, MaterialDefinitionType> _mapper = new MaterialDefinitionToMaterialDefinitionTypeMapper();

		/// <summary>
		/// Exports all MaterialDefinition nodes within the scope.
		/// </summary>
		/// <param name="materialDefinitionNode">The materialDefinition node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child materialDefinition not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="MaterialDefinitionType"/> for the materialDefinition in scope. Null if none are found.</returns>
		public MaterialDefinitionType Export(ModelElement materialDefinitionNode, IEnumerable<ModelElement> exportScope)
		{
			if (!materialDefinitionNode.Is(NodeMetaType.DefinitionNode<ModelElement, MaterialDefinition>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			var mapAsReference = materialDefinitionNode.IsLinkNode || !exportScope.Contains(materialDefinitionNode);
			return _mapper.Map((MaterialDefinition)materialDefinitionNode.TargetObject, mapAsReference);
		}
	}
}
