﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the HierarchyScope class
	/// </summary>
	public class HierarchyScopeExporter : IExporter<HierarchyScopeType>
	{
		/// <summary>
		/// The mapper used for mapping hierarchyScope
		/// </summary>
		private readonly IMapper<HierarchyScope, HierarchyScopeType> _mapper = new HierarchyScopeToHierarchyScopeTypeMapper();

		/// <summary>
		/// Exports all HierarchyScope nodes within the scope.
		/// </summary>
		/// <param name="hierarchyScopeNode">The hierarchyScope node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child hierarchyScope not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="HierarchyScopeType"/> for the hierarchyScope in scope. Null if none are found.</returns>
		public HierarchyScopeType Export(ModelElement hierarchyScopeNode, IEnumerable<ModelElement> exportScope)
		{
			if (!hierarchyScopeNode.Is(NodeMetaType.DefinitionNode<ModelElement, HierarchyScope>())
			    && !hierarchyScopeNode.Is(NodeMetaType.DefinitionNode<ModelElement, HierarchyScope>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			return MapHierarchyScope(hierarchyScopeNode, exportScope.ToArray());
		}

		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		private HierarchyScopeType MapHierarchyScope(BrowserNode hierarchyScopeNode, IEnumerable<ModelElement> exportScope)
		{
			var mapAsReference = hierarchyScopeNode.IsLinkNode || !exportScope.Contains(hierarchyScopeNode);
			var returnType = _mapper.Map((HierarchyScope) hierarchyScopeNode.TargetObject, false); // Actual mapping is never by ref for Hierarchy Scope
			if (!mapAsReference && hierarchyScopeNode.Is<ModelElement>(out var modelElement))
			{
				returnType.HierarchyScope = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, HierarchyScope>()))
					.Select(n => MapHierarchyScope(n, exportScope)).ToArray();
			}
			return returnType;
		}
	}
}
