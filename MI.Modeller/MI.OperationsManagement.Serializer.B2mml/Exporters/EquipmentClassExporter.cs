﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the EquipmentClass class
	/// </summary>
	public class EquipmentClassExporter : IExporter<EquipmentClassType>
	{
		/// <summary>
		/// The mapper used for mapping equipmentClass
		/// </summary>
		private readonly IMapper<EquipmentClass, EquipmentClassType> _mapper = new EquipmentClassToEquipmentClassTypeMapper();

		/// <summary>
		/// Exports all EquipmentClass nodes within the scope.
		/// </summary>
		/// <param name="equipmentClassNode">The equipmentClass node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child equipmentClass not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="EquipmentClassType"/> for the equipmentClass in scope. Null if none are found.</returns>
		public EquipmentClassType Export(ModelElement equipmentClassNode, IEnumerable<ModelElement> exportScope)
		{
			if (!equipmentClassNode.Is(NodeMetaType.DefinitionNode<ModelElement, EquipmentClass>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			return MapEquipmentClass(equipmentClassNode, exportScope.ToArray());
		}

		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		private EquipmentClassType MapEquipmentClass(BrowserNode equipmentClassNode, IEnumerable<ModelElement> exportScope)
		{
			var mapAsReference = equipmentClassNode.IsLinkNode || !exportScope.Contains(equipmentClassNode);
			var returnType = _mapper.Map((EquipmentClass)equipmentClassNode.TargetObject, mapAsReference);
			if (!mapAsReference && equipmentClassNode.Is<ModelElement>(out var modelElement))
			{
				returnType.ContainedEquipmentClass = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, EquipmentClass>()))
					.Select(n => MapEquipmentClass(n, exportScope)).ToArray();
			}
			return returnType;
		}
	}
}
