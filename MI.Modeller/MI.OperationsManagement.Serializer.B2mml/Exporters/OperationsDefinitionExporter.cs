﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;
// ReSharper disable PossibleMultipleEnumeration

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	public class OperationsDefinitionExporter: IExporter<OperationsDefinitionType>
	{
		/// <summary>
		/// Mapper for operations definitions
		/// </summary>
		private readonly OperationsDefinitionToOperationsDefinitionTypeMapper _operationsDefinitionMapper = new OperationsDefinitionToOperationsDefinitionTypeMapper();

		/// <summary>
		/// Mapper for operations segments
		/// </summary>
		private readonly OperationsSegmentToOperationsSegmentTypeMapper _operationsSegmentMapper = new OperationsSegmentToOperationsSegmentTypeMapper();

		/// <summary>
		/// Exporter for the OperationsDefinition class
		/// </summary>
		public OperationsDefinitionType Export(ModelElement exportNode, IEnumerable<ModelElement> exportScope)
		{
			if (!exportNode.Is(NodeMetaType.DefinitionNode<ModelElement, OperationsDefinition>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			var mapAsReference = exportNode.IsLinkNode || !exportScope.Contains(exportNode);
			var returnType = _operationsDefinitionMapper.Map((OperationsDefinition)exportNode.TargetObject, mapAsReference);
			if (!mapAsReference && exportNode.Is<ModelElement>(out var modelElement))
			{
				returnType.OperationsSegment = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, OperationsSegment>()))
					.Select(n => MapOperationsSegment((ModelElement) n, exportScope)).ToArray();
				returnType.OperationsMaterialBill = MapOperationsMaterialBill(modelElement, exportScope);
			}
			return returnType;
		}

		private OperationsMaterialBillType[] MapOperationsMaterialBill(ModelElement modelElement, IEnumerable<ModelElement> exportScope)
		{
			// TODO when implemented
			return null;
		}

		private OperationsSegmentType MapOperationsSegment(ModelElement operationsSegment, IEnumerable<ModelElement> exportScope)
		{
			var mapAsReference = operationsSegment.IsLinkNode || !exportScope.Contains(operationsSegment);
			var returnType = _operationsSegmentMapper.Map((OperationsSegment) operationsSegment.TargetObject, mapAsReference);
			if (!mapAsReference)
			{
				returnType.OperationsSegment = operationsSegment.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, OperationsSegment>()))
					.Select(n => MapOperationsSegment((ModelElement)n, exportScope)).ToArray();
				//returnType.MaterialSpecification = operationsSegment
			}
			return returnType;
		}
	}
}
