﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the PersonnelClass class
	/// </summary>
	public class PersonnelClassExporter : IExporter<PersonnelClassType>
	{
		/// <summary>
		/// The mapper used for mapping personnelClass
		/// </summary>
		private readonly IMapper<PersonnelClass, PersonnelClassType> _mapper = new PersonnelClassToPersonnelClassTypeMapper();

		/// <summary>
		/// Exports all PersonnelClass nodes within the scope.
		/// </summary>
		/// <param name="personnelClassNode">The personnelClass node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child personnelClass not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="PersonnelClassType"/> for the personnelClass in scope. Null if none are found.</returns>
		public PersonnelClassType Export(ModelElement personnelClassNode, IEnumerable<ModelElement> exportScope)
		{
			if (!personnelClassNode.Is(NodeMetaType.DefinitionNode<ModelElement, PersonnelClass>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			var mapAsReference = personnelClassNode.IsLinkNode || !exportScope.Contains(personnelClassNode);
			var returnType = _mapper.Map((PersonnelClass)personnelClassNode.TargetObject, mapAsReference);
			return returnType;
		}
	}
}
