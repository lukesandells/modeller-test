﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the PhysicalAssetClass class
	/// </summary>
	public class PhysicalAssetClassExporter : IExporter<PhysicalAssetClassType>
	{
		/// <summary>
		/// The mapper used for mapping physicalAssetClass
		/// </summary>
		private readonly IMapper<PhysicalAssetClass, PhysicalAssetClassType> _mapper = new PhysicalAssetClassToPhysicalAssetClassTypeMapper();

		/// <summary>
		/// Exports all PhysicalAssetClass nodes within the scope.
		/// </summary>
		/// <param name="physicalAssetClassNode">The physicalAssetClass node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child physicalAssetClass not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="PhysicalAssetClassType"/> for the physicalAssetClass in scope. Null if none are found.</returns>
		public PhysicalAssetClassType Export(ModelElement physicalAssetClassNode, IEnumerable<ModelElement> exportScope)
		{
			if (!physicalAssetClassNode.Is(NodeMetaType.DefinitionNode<ModelElement, PhysicalAssetClass>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			return MapPhysicalAssetClass(physicalAssetClassNode, exportScope.ToArray());
		}

		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		private PhysicalAssetClassType MapPhysicalAssetClass(BrowserNode physicalAssetClassNode, IEnumerable<ModelElement> exportScope)
		{
			var mapAsReference = physicalAssetClassNode.IsLinkNode || !exportScope.Contains(physicalAssetClassNode);
			var returnType = _mapper.Map((PhysicalAssetClass)physicalAssetClassNode.TargetObject, mapAsReference);
			if (!mapAsReference && physicalAssetClassNode.Is<ModelElement>(out var modelElement))
			{
				returnType.ContainedPhysicalAssetClass = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, PhysicalAssetClass>()))
					.Select(n => MapPhysicalAssetClass(n, exportScope)).ToArray();
			}
			return returnType;
		}
	}
}
