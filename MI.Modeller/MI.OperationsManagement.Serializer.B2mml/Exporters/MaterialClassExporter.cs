﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the MaterialClass class
	/// </summary>
	public class MaterialClassExporter : IExporter<MaterialClassType>
	{
		/// <summary>
		/// The mapper used for mapping materialClass
		/// </summary>
		private readonly IMapper<MaterialClass, MaterialClassType> _mapper = new MaterialClassToMaterialClassTypeMapper();

		/// <summary>
		/// Exports all MaterialClass nodes within the scope.
		/// </summary>
		/// <param name="materialClassNode">The materialClass node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child materialClass not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="MaterialClassType"/> for the materialClass in scope. Null if none are found.</returns>
		public MaterialClassType Export(ModelElement materialClassNode, IEnumerable<ModelElement> exportScope)
		{
			if (!materialClassNode.Is(NodeMetaType.DefinitionNode<ModelElement, MaterialClass>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			var mapAsReference = materialClassNode.IsLinkNode || !exportScope.Contains(materialClassNode);
			return _mapper.Map((MaterialClass)materialClassNode.TargetObject, mapAsReference);
		}
	}
}
