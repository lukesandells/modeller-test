﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the Equipment class
	/// </summary>
	public class EquipmentExporter:IExporter<EquipmentType>
	{
		/// <summary>
		/// The mapper used for mapping equipment
		/// </summary>
		private readonly IMapper<Equipment, EquipmentType> _mapper = new EquipmentToEquipmentTypeMapper();

		/// <summary>
		/// Exports all Equipment nodes within the scope.
		/// </summary>
		/// <param name="equipmentNode">The equipment node to export</param>
		/// <param name="exportScope">The scope of what needs to be exported. Any child equipment not in scope will only be mapped by reference</param>
		/// <returns>An <see cref="EquipmentInformationType"/> for the equipment in scope. Null if none are found.</returns>
		public EquipmentType Export(ModelElement equipmentNode, IEnumerable<ModelElement> exportScope)
		{
			if (!equipmentNode.Is(NodeMetaType.DefinitionNode<ModelElement, Equipment>())
				&& !equipmentNode.Is(NodeMetaType.DefinitionNode<ModelElement, HierarchyScope>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			return MapEquipment(equipmentNode, exportScope.ToArray());
		}

		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		private EquipmentType MapEquipment(BrowserNode equipmentNode, IEnumerable<ModelElement> exportScope)
		{
			// Could be the defining equipment if this is a Hierarchy Scope
			var equipmentToMap = equipmentNode.TargetObject as Equipment ?? ((HierarchyScope) equipmentNode.TargetObject).DefiningEquipment;
			var mapAsReference = equipmentNode.IsLinkNode || !exportScope.Contains(equipmentNode);
			var returnType = _mapper.Map(equipmentToMap, mapAsReference);
			if (!mapAsReference && equipmentNode.Is<ModelElement>(out var modelElement))
			{
				returnType.Equipment = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, Equipment>()))
					.Select(n => MapEquipment(n, exportScope)).ToArray();
			}
			return returnType;
		}
	}
}
