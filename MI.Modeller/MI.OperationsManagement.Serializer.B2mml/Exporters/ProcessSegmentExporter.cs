﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the ProcessSegment class
	/// </summary>
	public class ProcessSegmentExporter : IExporter<ProcessSegmentType>
	{
		/// <summary>
		/// Mapper for work Masters
		/// </summary>
		private readonly IMapper<ProcessSegment, ProcessSegmentType> _processSegmentMapper = new ProcessSegmentToProcessSegmentTypeMapper();

		/// <summary>
		/// Mapper for material specifications
		/// </summary>
		private readonly MaterialSpecificationToMaterialSegmentSpecificationTypeMapper<ProcessSegment> _materialSpecificationMapper = new MaterialSpecificationToMaterialSegmentSpecificationTypeMapper<ProcessSegment>();

		/// <summary>
		/// Mapper for equipment specifications
		/// </summary>
		private readonly EquipmentSpecificationToEquipmentSegmentSpecificationTypeMapper<ProcessSegment> _equipmentSpecificationMapper = new EquipmentSpecificationToEquipmentSegmentSpecificationTypeMapper<ProcessSegment>();

		/// <summary>
		/// Mapper for physical asset specfications
		/// </summary>
		private readonly PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapper<ProcessSegment> _physicalAssetSpecificationMapper = new PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapper<ProcessSegment>();

		/// <summary>
		/// Mapper for personnel specifications
		/// </summary>
		private readonly PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapper<ProcessSegment> _personnelSpecificationMapper = new PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapper<ProcessSegment>();

		/// <summary>
		/// Exports a node into a ProcessSegmentTYpe
		/// </summary>
		/// <param name="exportNode">The node to export. Must be for a ProcessSegment</param>
		/// <param name="exportScope">The scope of what is to be exported</param>
		/// <returns></returns>
		public ProcessSegmentType Export(ModelElement exportNode, IEnumerable<ModelElement> exportScope)
		{
			if (!exportNode.Is(NodeMetaType.DefinitionNode<ModelElement, ProcessSegment>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			return MapProcessSegment(exportNode, exportScope.ToArray());
		}

		/// <summary>
		/// Maps an individual work master and it's components
		/// </summary>
		/// <param name="exportNode">The work master node to be exported</param>
		/// <param name="exportScope">The scope of what is to be exported</param>
		/// <returns></returns>
		private ProcessSegmentType MapProcessSegment(ModelElement exportNode, ModelElement[] exportScope)
		{
			var mapAsReference = exportNode.IsLinkNode || !exportScope.Contains(exportNode);
			var returnType = _processSegmentMapper.Map((ProcessSegment)exportNode.TargetObject, mapAsReference);
			if (!mapAsReference && exportNode.Is<ModelElement>(out var modelElement))
			{
				returnType.MaterialSegmentSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, MaterialSpecification<ProcessSegment>>()))
					.Select(n => _materialSpecificationMapper.Map((MaterialSpecification<ProcessSegment>)n.TargetObject, false)).ToArray();
				returnType.EquipmentSegmentSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, EquipmentSpecification<ProcessSegment>>()))
					.Select(n => _equipmentSpecificationMapper.Map((EquipmentSpecification<ProcessSegment>)n.TargetObject)).ToArray();
				returnType.PhysicalAssetSegmentSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, PhysicalAssetSpecification<ProcessSegment>>()))
					.Select(n => _physicalAssetSpecificationMapper.Map((PhysicalAssetSpecification<ProcessSegment>)n.TargetObject, false)).ToArray();
				returnType.PersonnelSegmentSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, PersonnelSpecification<ProcessSegment>>()))
					.Select(n => _personnelSpecificationMapper.Map((PersonnelSpecification<ProcessSegment>)n.TargetObject)).ToArray();
				returnType.ProcessSegment = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, ProcessSegment>()))
					.Select(n => MapProcessSegment((ModelElement)n, exportScope)).ToArray();
			}
			return returnType;
		}
	}
}
