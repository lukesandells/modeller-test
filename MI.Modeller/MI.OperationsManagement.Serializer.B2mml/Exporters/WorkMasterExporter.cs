﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Exporters
{
	/// <summary>
	/// Exporter for the WorkMaster class
	/// </summary>
	public class WorkMasterExporter :IExporter<WorkMasterType>
	{
		/// <summary>
		/// Mapper for work Masters
		/// </summary>
		private readonly IMapper<WorkMaster, WorkMasterType> _workMasterMapper = new WorkMasterToWorkMasterTypeMapper();
		
		/// <summary>
		/// Mapper for material specifications
		/// </summary>
		private readonly IMapper<MaterialSpecification<WorkMaster>, OpMaterialSpecificationType> _materialSpecificationMapper = new MaterialSpecificationToOpMaterialSpecificationTypeMapper<WorkMaster>();
		
		/// <summary>
		/// Mapper for equipment specifications
		/// </summary>
		private readonly EquipmentSpecificationToOpEquipmentSpecificationTypeMapper<WorkMaster> _equipmentSpecificationMapper = new EquipmentSpecificationToOpEquipmentSpecificationTypeMapper<WorkMaster>();
		
		/// <summary>
		/// Mapper for physical asset specfications
		/// </summary>
		private readonly PhysicalAssetSpecificationToOpPhysicalAssetSpecificationTypeMapper<WorkMaster> _physicalAssetSpecificationMapper = new PhysicalAssetSpecificationToOpPhysicalAssetSpecificationTypeMapper<WorkMaster>();
		
		/// <summary>
		/// Mapper for personnel specifications
		/// </summary>
		private readonly PersonnelSpecificationToOpPersonnelSpecificationTypeMapper<WorkMaster> _personnelSpecificationMapper = new PersonnelSpecificationToOpPersonnelSpecificationTypeMapper<WorkMaster>();

		/// <summary>
		/// Exports a node into a WorkMasterTYpe
		/// </summary>
		/// <param name="exportNode">The node to export. Must be for a WorkMaster</param>
		/// <param name="exportScope">The scope of what is to be exported</param>
		/// <returns></returns>
		public WorkMasterType Export(ModelElement exportNode, IEnumerable<ModelElement> exportScope)
		{
			if (!exportNode.Is(NodeMetaType.DefinitionNode<ModelElement, WorkMaster>()))
			{
				throw new ArgumentException(ExportMessages.ExportNodeNotOfExpectedType);
			}
			return MapWorkMaster(exportNode, exportScope.ToArray());
		}

		/// <summary>
		/// Maps an individual work master and it's components
		/// </summary>
		/// <param name="exportNode">The work master node to be exported</param>
		/// <param name="exportScope">The scope of what is to be exported</param>
		/// <returns></returns>
		private WorkMasterType MapWorkMaster(ModelElement exportNode, ModelElement[] exportScope)
		{
			var mapAsReference = exportNode.IsLinkNode || !exportScope.Contains(exportNode);
			var returnType = _workMasterMapper.Map((WorkMaster) exportNode.TargetObject, mapAsReference);
			if (!mapAsReference && exportNode.Is<ModelElement>(out var modelElement))
			{
				returnType.MaterialSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, MaterialSpecification<WorkMaster>>()))
					.Select(n => MapMaterialSpecification(n, exportScope)).ToArray();
				returnType.EquipmentSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, EquipmentSpecification<WorkMaster>>()))
					.Select(n => _equipmentSpecificationMapper.Map((EquipmentSpecification<WorkMaster>)n.TargetObject)).ToArray();
				returnType.PhysicalAssetSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, PhysicalAssetSpecification<WorkMaster>>()))
					.Select(n => _physicalAssetSpecificationMapper.Map((PhysicalAssetSpecification<WorkMaster>)n.TargetObject, false)).ToArray();
				returnType.PersonnelSpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, PersonnelSpecification<WorkMaster>>()))
					.Select(n => _personnelSpecificationMapper.Map((PersonnelSpecification<WorkMaster>)n.TargetObject)).ToArray();
				returnType.WorkMaster = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, WorkMaster>()))
					.Select(n => MapWorkMaster((ModelElement) n, exportScope)).ToArray();
			}
			return returnType;
		}

		/// <summary>
		/// Maps a material specification node to a OpMaterialSpecificationType
		/// </summary>
		/// <param name="materialSpecificationNode">The material specification node to be exported</param>
		/// <param name="exportScope">The scope of what is to be exported</param>
		/// <returns></returns>
		private OpMaterialSpecificationType MapMaterialSpecification(BrowserNode materialSpecificationNode, ModelElement[] exportScope)
		{
			var mapAsReference = materialSpecificationNode.IsLinkNode || !exportScope.Contains(materialSpecificationNode);
			var returnType = _materialSpecificationMapper.Map((MaterialSpecification<WorkMaster>)materialSpecificationNode.TargetObject, mapAsReference);
			if (!mapAsReference && materialSpecificationNode.Is<ModelElement>(out var modelElement))
			{
				returnType.AssemblySpecification = modelElement.NonEmptyNonSystemFolders
					.SelectMany(f => f.Nodes)
					.Where(d => d.Is(NodeMetaType.Any<ModelElement, MaterialSpecification<WorkMaster>>()))
					.Select(n => MapMaterialSpecification(n, exportScope)).ToArray();
			}
			return returnType;
		}
	}
}
