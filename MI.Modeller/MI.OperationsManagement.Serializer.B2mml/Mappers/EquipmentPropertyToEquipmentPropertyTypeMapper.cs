﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentPropertyToEquipmentPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.EquipmentProperty, EquipmentPropertyType}"</cref>
	/// </seealso>
	public class EquipmentPropertyToEquipmentPropertyTypeMapper : IMapper<BasicProperty<Equipment>, EquipmentPropertyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment property.
		/// </summary>
		/// <param name="equipmentProperty">The equipment property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>EquipmentPropertyType.</returns>
		public EquipmentPropertyType Map(BasicProperty<Equipment> equipmentProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(equipmentProperty,
                nameof(equipmentProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = equipmentProperty.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = equipmentProperty.ExternalId
                };

            var b2mmlDescription = equipmentProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipmentProperty.Description
                    }
                };

	        var b2mmlValue = equipmentProperty.Value == null
				? null
				: equipmentProperty.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var b2mmlProperty = equipmentProperty.SubProperties.MapToArray(this, false);

            return new EquipmentPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                EquipmentProperty = b2mmlProperty
            };
        }

        #endregion
    }
}