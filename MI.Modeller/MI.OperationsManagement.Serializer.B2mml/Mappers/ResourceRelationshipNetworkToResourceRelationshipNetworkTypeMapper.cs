﻿//using System;
//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.MapperExtensions;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceRelationshipNetwork,
//    ///         ResourceRelationshipNetworkType}"
//    ///     </cref>
//    /// </seealso>
//    public class ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapper :
//        IMapper<ResourceRelationshipNetwork, ResourceRelationshipNetworkType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified resource relationship network.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <returns>ResourceRelationshipNetworkType.</returns>
//        public ResourceRelationshipNetworkType Map(ResourceRelationshipNetwork resourceRelationshipNetwork)
//        {
//            ArgumentValidation.AssertNotNull(resourceRelationshipNetwork,
//                nameof(resourceRelationshipNetwork));

//            var b2mmlId = resourceRelationshipNetwork.Id == null
//                ? null
//                : new IdentifierType
//                {
//                    Value = resourceRelationshipNetwork.Id
//                };

//            var b2mmlDescription = resourceRelationshipNetwork.Description == null
//                ? null
//                : new[]
//                {
//                    new DescriptionType
//                    {
//                        Value = resourceRelationshipNetwork.Description
//                    }
//                };

//            var b2mmlHierarchyScope = resourceRelationshipNetwork.HierarchyScope == null
//                ? null
//                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(resourceRelationshipNetwork.HierarchyScope);

//            var b2mmlRelationshipType = resourceRelationshipNetwork.RelationshipType == null
//                ? null
//                : new ResourceRelationshipTypeToRelationshipTypeTypeMapper().Map(
//                    resourceRelationshipNetwork.RelationshipType);

//            var b2mmlRelationshipForm = resourceRelationshipNetwork.RelationshipForm == null
//                ? null
//                : new ResourceRelationshipFormTypeToRelationshipFormTypeMapper().Map(
//                    resourceRelationshipNetwork.RelationshipForm);

//            var b2mmlPublishedDate = resourceRelationshipNetwork.PublishedDate == default(DateTime)
//                ? default(PublishedDateType)
//                : new PublishedDateType
//                {
//                    Value = resourceRelationshipNetwork.PublishedDate
//                };

//            var b2mmlNetworkConnection =
//                resourceRelationshipNetwork.ResourceNetworkConnections.MapToArray(
//                    new ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper());

//            return new ResourceRelationshipNetworkType
//            {
//                ID = b2mmlId,
//                Description = b2mmlDescription,
//                HierarchyScope = b2mmlHierarchyScope,
//                RelationshipType = b2mmlRelationshipType,
//                RelationshipForm = b2mmlRelationshipForm,
//                PublishedDate = b2mmlPublishedDate,
//                ResourceNetworkConnection = b2mmlNetworkConnection
//            };
//        }

//        #endregion
//    }
//}