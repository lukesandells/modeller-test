﻿//using System;
//using System.Linq;
//using MI.Framework.Extensions;
//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.Extensions;
//using MI.Modeller.B2mml.Mappers.Resources;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class WorkMasterFromWorkMasterTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>"MI.Modeller.B2mml.Mappers.IMapper{WorkMasterType, MI.Modeller.Domain.Client.WorkMaster}"</cref>
//    /// </seealso>
//    /// <seealso>
//    ///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{WorkMasterType, MI.Modeller.Domain.Client.WorkMaster}"</cref>
//    /// </seealso>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{WorkMasterType, MI.Modeller.Domain.Client.WorkMaster}"
//    ///     </cref>
//    /// </seealso>
//    public class WorkMasterFromWorkMasterTypeMapper : IMapper<WorkMasterType, WorkMaster>
//    {
//        #region  Fields

//        /// <summary>
//        ///     The workspace
//        /// </summary>
//        private readonly IWorkspace _workspace;

//        #endregion

//        #region Constructors

//        /// <summary>
//        ///     Constructs a new WorkMasterFromWorkMasterTypeMapper
//        /// </summary>
//        /// <param name="workspace">The workspace.</param>
//        public WorkMasterFromWorkMasterTypeMapper(IWorkspace workspace)
//        {
//            _workspace = ArgumentValidation.AssertNotNull(workspace,
//                nameof(workspace));
//        }

//        #endregion

//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified b2 MML work master type.
//        /// </summary>
//        /// <param name="b2mmlWorkMasterType">Type of the b2 MML work master.</param>
//        /// <returns>WorkMaster.</returns>
//        /// <exception cref="System.Exception"></exception>
//        public WorkMaster Map(WorkMasterType b2mmlWorkMasterType)
//        {
//            ArgumentValidation.AssertNotNull(b2mmlWorkMasterType,
//                nameof(b2mmlWorkMasterType));
//            ArgumentValidation.AssertNotNull(b2mmlWorkMasterType.HierarchyScope,
//                nameof(b2mmlWorkMasterType.HierarchyScope));

//            var id = b2mmlWorkMasterType.ID?.Value;

//            var version = b2mmlWorkMasterType.Version?.Value;

//            var description = !b2mmlWorkMasterType.Description.IsNullOrEmpty()
//                ? b2mmlWorkMasterType.Description.FirstOrDefault()
//                    ?.Value
//                : null;

//            var hierarchyScope = _workspace.FindById<HierarchyScope>(null,
//                b2mmlWorkMasterType.HierarchyScope.EquipmentID.Value);

//            if (hierarchyScope == null)
//            {
//                throw new Exception(string.Format(Messages.HierarchyScopeWithId0CannotBeFound,
//                    b2mmlWorkMasterType.HierarchyScope.EquipmentID));
//            }

//            var workType = b2mmlWorkMasterType.ProcessType == null
//                ? null
//                : new WorkTypeFromWorkTypeTypeMapper().Map(b2mmlWorkMasterType.ProcessType);

//            var workDefinitionType = b2mmlWorkMasterType.WorkDefinitionType == null
//                ? null
//                : new WorkDefinitionTypeFromWorkDefinitionTypeTypeMapper().Map(b2mmlWorkMasterType.WorkDefinitionType);

//            // The B2mml schema defines the duration as an extension of the xs:duration class which uses the standard duration format
//            var duration = b2mmlWorkMasterType.Duration == null
//                ? default(TimeSpan)
//                : TimeSpan.Parse(b2mmlWorkMasterType.Duration);

//            var publishDate = b2mmlWorkMasterType.PublishedDate?.Value ?? default(DateTime);

//            var operationsDefinitionId = b2mmlWorkMasterType.OperationsDefinitionID?.Value;

//            var operationsSegmentId = b2mmlWorkMasterType.OperationsSegmentID?.Value;

//            var parameters = b2mmlWorkMasterType.Parameter.MapToSet(new ParameterFromParameterTypeMapper());

//            var personnelSpecifications =
//                b2mmlWorkMasterType.PersonnelSpecification?.MapToSet(
//                    new PersonnelSpecificationFromOpPersonnelSpecificationTypeMapper(_workspace,
//                        id));

//            var equipmentSpecifications =
//                b2mmlWorkMasterType.EquipmentSpecification?.MapToSet(
//                    new EquipmentSpecificationFromOpEquipmentSpecificationTypeMapper(_workspace,
//                        id));

//            var physicalAssetSpecifications =
//                b2mmlWorkMasterType.PhysicalAssetSpecification?.MapToArray(
//                    new PhysicalAssetSpecificationFromOpPhysicalAssetSpecificationTypeMapper(_workspace,
//                        id));

//            var workflowSpecifications = b2mmlWorkMasterType.WorkflowSpecificationID.IsNullOrEmpty()
//                ? null
//                : b2mmlWorkMasterType.WorkflowSpecificationID.Where(e => e?.Value != null)
//                    .Select(e => e.Value)
//                    .ToList();

//            var existingWorkMaster = _workspace.FindById<WorkMaster>(hierarchyScope.EquipmentId,
//                id);

//            // map the children passing the new object
//            var children = b2mmlWorkMasterType.WorkMaster.MapToSet(this);

//            if (existingWorkMaster == null)
//            {
//                var loadedWorkMaster = new WorkMaster(id,
//                    hierarchyScope,
//                    version,
//                    workType,
//                    workDefinitionType,
//                    duration,
//                    publishDate,
//                    operationsDefinitionId,
//                    operationsSegmentId,
//                    description,
//                    parameters,
//                    personnelSpecifications,
//                    equipmentSpecifications,
//                    physicalAssetSpecifications,
//                    workflowSpecifications,
//                    internalId: existingWorkMaster?.InternalId ?? default(Guid));
//                _workspace.Add(loadedWorkMaster);
//                var loadedMaterialSpecifications =
//                b2mmlWorkMasterType.MaterialSpecification?.MapToSet(
//                    new MaterialSpecificationFromOpMaterialSpecificationTypeMapper(_workspace,
//                        id,
//                        loadedWorkMaster));
//                if (loadedMaterialSpecifications != null)
//                {
//                    foreach (var materialSpec in
//                        loadedMaterialSpecifications)
//                    {
//                        loadedWorkMaster.AddMaterialSpecification(materialSpec);
//                    }
//                }
//                foreach (var child in children)
//                {
//                    loadedWorkMaster.AddChild(child);
//                }
//                return loadedWorkMaster;
//            }
//            existingWorkMaster.Version = version;
//            existingWorkMaster.ProcessType = workType;
//            existingWorkMaster.WorkDefinitionType = workDefinitionType;
//            existingWorkMaster.Duration = duration;
//            existingWorkMaster.PublishedDate = publishDate;
//            existingWorkMaster.OperationsDefinitionId = operationsDefinitionId;
//            existingWorkMaster.OperationsSegmentId = operationsSegmentId;
//            existingWorkMaster.Description = description;
//            existingWorkMaster.Parameters = parameters;
//            existingWorkMaster.PersonnelSpecifications = personnelSpecifications;
//            foreach (var specification in equipmentSpecifications)
//            {
//                existingWorkMaster.AddEquipmentSpecification(specification);
//            }
//            existingWorkMaster.PhysicalAssetSpecifications = physicalAssetSpecifications;
//            var materialSpecifications =
//                b2mmlWorkMasterType.MaterialSpecification?.MapToSet(
//                    new MaterialSpecificationFromOpMaterialSpecificationTypeMapper(_workspace,
//                        id,
//                        existingWorkMaster));
//            if (materialSpecifications != null)
//            {
//                foreach (var materialSpec in materialSpecifications)
//                {
//                    existingWorkMaster.AddMaterialSpecification(materialSpec);
//                }
//            }
//            existingWorkMaster.WorkflowSpecificationIds = workflowSpecifications;
//            return existingWorkMaster;
//        }

//        #endregion
//    }
//}