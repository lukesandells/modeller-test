﻿namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
    /// <summary>
    ///     Standard interface for mapping to and from B2mml Types
    /// </summary>
    /// <typeparam name="TF"></typeparam>
    /// <typeparam name="T">The class to map to</typeparam>
    public interface IMapper<in TF, out T>
    {
        #region Members

	    /// <summary>
	    ///     Maps an object of type F to Type F
	    /// </summary>
	    /// <param name="o">The object to map</param>
	    /// <param name="mapAsReference">If an object is mapped as reference then only the ID and Hierarchy Scope are populated</param>
	    /// <returns></returns>
	    T Map(TF o, bool mapAsReference);

        #endregion
    }
}