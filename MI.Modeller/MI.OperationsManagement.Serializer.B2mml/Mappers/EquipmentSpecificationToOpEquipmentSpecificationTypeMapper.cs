using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentSpecificationToB2mmlOpEquipmentSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IB2mmlMapper{MI.Modeller.Domain.Client.EquipmentSpecification,
	///         OpEquipmentSpecificationType}"
	///     </cref>
	/// </seealso>
	public class EquipmentSpecificationToOpEquipmentSpecificationTypeMapper<TOfObject> :
        IMapper<EquipmentSpecification<TOfObject>, OpEquipmentSpecificationType>
        where TOfObject : class, IProcessDefinition<TOfObject>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment specification.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>OpEquipmentSpecificationType.</returns>
		public OpEquipmentSpecificationType Map(EquipmentSpecification<TOfObject> equipmentSpecification, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(equipmentSpecification,
                nameof(equipmentSpecification));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			// id always filled
			var b2mmlId =new IdentifierType
                {
                    Value = equipmentSpecification.ExternalId
                };

	        var b2mmlEquipmentId = equipmentSpecification.Equipment == null
		        ? null
		        : new[]
		        {
			        new EquipmentIDType
			        {
				        Value = equipmentSpecification.Equipment.ExternalId
			        }
		        };

			var b2mmlClassId = equipmentSpecification.EquipmentClass == null
                ? null
                : new[]
                {
                    new EquipmentClassIDType
                    {
                        Value = equipmentSpecification.EquipmentClass.ExternalId
                    }
                };

            var b2mmlDescription = string.IsNullOrEmpty(equipmentSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipmentSpecification.Description
                    }
                };

            var b2mmlUse = string.IsNullOrEmpty(equipmentSpecification.EquipmentUse)
                ? null
                : new EquipmentUseType
                {
                    Value = equipmentSpecification.EquipmentUse
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlQuantity = equipmentSpecification.Quantity == null
				? null
				: equipmentSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlGeospatialDesignation = equipmentSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    equipmentSpecification.SpatialDefinition);

            var b2mmlHierarchyScope = equipmentSpecification.HierarchyScope == null
                ? null
                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(equipmentSpecification.HierarchyScope);

            var b2mmlProperties =
                equipmentSpecification.Properties.MapToArray(
                    new EquipmentSpecificationPropertyToOpEquipmentSpecificationPropertyTypeMapper<TOfObject>(),
                    false);

            return new OpEquipmentSpecificationType
            {
				ID = b2mmlId,
                EquipmentID = b2mmlEquipmentId,
                EquipmentClassID = b2mmlClassId,
                Description = b2mmlDescription,
                SpatialLocation = b2mmlGeospatialDesignation,
                HierarchyScope = b2mmlHierarchyScope,
                EquipmentSpecificationProperty = b2mmlProperties,
                EquipmentUse = b2mmlUse,
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}