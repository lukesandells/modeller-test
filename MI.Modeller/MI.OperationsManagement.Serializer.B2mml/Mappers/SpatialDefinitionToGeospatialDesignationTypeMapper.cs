﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class SpatialDefinitionToGeospatialDesignationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IB2mmlMapper{MI.Modeller.Domain.Client.SpatialLocation,
	///         GeospatialDesignationType}"
	///     </cref>
	/// </seealso>
	public class SpatialDefinitionToGeospatialDesignationTypeMapper :
        IMapper<SpatialDefinition, GeospatialDesignationType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified geospatial designation.
		/// </summary>
		/// <param name="geospatialDesignation">The geospatial designation.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>GeospatialDesignationType.</returns>
		public GeospatialDesignationType Map(SpatialDefinition geospatialDesignation, bool mapAsReference= false)
        {
            ArgumentValidation.AssertNotNull(geospatialDesignation,
                nameof(geospatialDesignation));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var srid = geospatialDesignation.Srid == default(string)
                ? default(IdentifierType)
                : new IdentifierType
                {
                    Value = geospatialDesignation.Srid
                };

            var sridAuthority = geospatialDesignation.SridAuthority == null
                ? default(IdentifierType)
                : new IdentifierType
                {
                    Value = geospatialDesignation.SridAuthority
                };

            var format = geospatialDesignation.Format == null
                ? default(DescriptionType)
                : new DescriptionType
                {
                    Value = geospatialDesignation.Format
                };

            var value = geospatialDesignation.Value == null
                ? default(ValueStringType)
                : new ValueStringType
                {
                    Value = geospatialDesignation.Value
                };

            return new GeospatialDesignationType
            {
                SRID = srid,
                SRIDAuthority = sridAuthority,
                Format = format,
                Value = value
            };
        }

        #endregion
    }
}