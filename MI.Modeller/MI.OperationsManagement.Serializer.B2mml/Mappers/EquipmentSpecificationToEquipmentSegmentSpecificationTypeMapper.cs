using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentSpecificationToB2mmlEquipmentSegmentSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.EquipmentSpecification,
	///         EquipmentSegmentSpecificationType}"
	///     </cref>
	/// </seealso>
	public class EquipmentSpecificationToEquipmentSegmentSpecificationTypeMapper<TProcessDefinition> :
        IMapper<EquipmentSpecification<TProcessDefinition>, EquipmentSegmentSpecificationType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment specification.
		/// </summary>
		/// <param name="equipmentSpecification">The equipment specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>EquipmentSegmentSpecificationType.</returns>
		public EquipmentSegmentSpecificationType Map(EquipmentSpecification<TProcessDefinition> equipmentSpecification, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(equipmentSpecification,
                nameof(equipmentSpecification));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = equipmentSpecification.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = equipmentSpecification.ExternalId
                };

	        var b2mmlEquipmentId = equipmentSpecification.Equipment?.ExternalId == null
		        ? null
		        : new EquipmentIDType
		        {
			        Value = equipmentSpecification.Equipment.ExternalId
		        };

			var b2mmlClassId = equipmentSpecification.EquipmentClass?.ExternalId == null
                ? null
                : new EquipmentClassIDType
                {
                    Value = equipmentSpecification.EquipmentClass.ExternalId
                };

            var b2mmlDescription = string.IsNullOrEmpty(equipmentSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipmentSpecification.Description
                    }
                };

            var b2mmlUse = string.IsNullOrEmpty(equipmentSpecification.EquipmentUse)
                ? null
                : new EquipmentUseType
                {
                    Value = equipmentSpecification.EquipmentUse
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlQuantity = equipmentSpecification.Quantity == null
				? null
				: equipmentSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlGeospatialDesignation = equipmentSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    equipmentSpecification.SpatialDefinition,
		            false);

            var b2mmlHierarchyScope = equipmentSpecification.HierarchyScope == null
                ? null
                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(equipmentSpecification.HierarchyScope);

            var b2mmlProperties =
                equipmentSpecification.Properties.MapToArray(
                    new EquipmentSpecificationPropertyToEquipmentSegmentSpecificationPropertyTypeMapper<TProcessDefinition>(),
                    false);

            return new EquipmentSegmentSpecificationType
            {
				ID = b2mmlId,
                EquipmentID = b2mmlEquipmentId,
                EquipmentClassID = b2mmlClassId,
                Description = b2mmlDescription,
                GeospatialDesignation = b2mmlGeospatialDesignation,
                HierarchyScope = b2mmlHierarchyScope,
                EquipmentSegmentSpecificationProperty = b2mmlProperties,
                EquipmentUse = b2mmlUse,
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}