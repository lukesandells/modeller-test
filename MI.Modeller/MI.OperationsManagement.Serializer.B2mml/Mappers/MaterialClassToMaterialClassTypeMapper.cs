using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialClassToMaterialClassTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.MaterialClass, MaterialClassType}"</cref>
	/// </seealso>
	public class MaterialClassToMaterialClassTypeMapper : IMapper<MaterialClass, MaterialClassType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material class.
		/// </summary>
		/// <param name="materialClass">The material class.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>MaterialClassType.</returns>
		public MaterialClassType Map(MaterialClass materialClass, bool mapAsReference)
        {
            var b2mmlId = materialClass.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = materialClass.ExternalId
                };

	        var b2mmlHierarchyScope = materialClass.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(materialClass.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new MaterialClassType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(materialClass.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialClass.Description
                    }
                };

            var b2mmlMaterialClassProperty =
                materialClass.Properties.MapToArray(
                    new MaterialClassPropertyToMaterialClassPropertyTypeMapper(),
                    false);

            var b2mmlMaterialDefinitionId = !materialClass.Members.Any()
                ? null
                : materialClass.Members.Select(e => new MaterialDefinitionIDType
                    {
                        Value = e.ExternalId
                    })
                    .ToArray();

            var b2mmlAssemblyClassId = !materialClass.Subclasses.Any()
                ? null
                : materialClass.Subclasses.Select(e => new MaterialClassIDType
                    {
                        Value = e.ExternalId
                })
                    .ToArray();

            var b2mmlAssemblyType = materialClass.AssemblyType == null
                ? null
                : new AssemblyTypeToAssemblyTypeTypeMapper().Map(materialClass.AssemblyType.Value);

            var b2mmlAssemblyRelationship = materialClass.AssemblyRelationship == null
                ? null
                : new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper().Map(materialClass.AssemblyRelationship.Value, false);

            return new MaterialClassType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                AssemblyClassID = b2mmlAssemblyClassId,
                AssemblyRelationship = b2mmlAssemblyRelationship,
                AssemblyType = b2mmlAssemblyType,
                HierarchyScope = b2mmlHierarchyScope,
                MaterialDefinitionID = b2mmlMaterialDefinitionId,
                MaterialClassProperty = b2mmlMaterialClassProperty
            };
        }

        #endregion
    }
}