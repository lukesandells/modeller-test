﻿//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.Extensions;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IB2mmlMapper{MI.Modeller.Domain.Client.WorkflowSpecificationNode,
//    ///         WorkflowSpecificationNodeType}"
//    ///     </cref>
//    /// </seealso>
//    public class WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper :
//        IMapper<WorkflowSpecificationNode, WorkflowSpecificationNodeType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified workflow specification node.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <returns>WorkflowSpecificationNodeType.</returns>
//        public WorkflowSpecificationNodeType Map(WorkflowSpecificationNode workflowSpecificationNode)
//        {
//            ArgumentValidation.AssertNotNull(workflowSpecificationNode,
//                nameof(workflowSpecificationNode));

//            // id always filled
//            var b2mmlId = new IdentifierType
//            {
//                Value = workflowSpecificationNode.Id
//            };

//            var b2mmlDescription = workflowSpecificationNode.Description == null
//                ? null
//                : new[]
//                {
//                    new DescriptionType
//                    {
//                        Value = workflowSpecificationNode.Description
//                    }
//                };

//            var b2mmlNodeType = workflowSpecificationNode.NodeType == null
//                ? null
//                : new IdentifierType
//                {
//                    Value = workflowSpecificationNode.NodeType
//                };

//            var b2mmlWorkflowSpecification = workflowSpecificationNode.WorkflowSpecification == null
//                ? null
//                : new WorkflowSpecificationToWorkflowSpecificationTypeMapper().Map(
//                    workflowSpecificationNode.WorkflowSpecification);

//            // collection properties in the domain are never null, even if set to null in ctor
//            // they are defaulted to an empty list. So no need to check empty
//            var b2mmlProperty =
//                workflowSpecificationNode.Properties.MapToArray(
//                    new WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper());

//            return new WorkflowSpecificationNodeType
//            {
//                ID = b2mmlId,
//                Description = b2mmlDescription,
//                NodeType = b2mmlNodeType,
//                Property = b2mmlProperty,
//                WorkflowSpecification = b2mmlWorkflowSpecification
//            };
//        }

//        #endregion
//    }
//}