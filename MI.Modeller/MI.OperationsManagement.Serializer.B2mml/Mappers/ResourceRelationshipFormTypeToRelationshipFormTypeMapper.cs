﻿//using MI.Framework;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class ResourceRelationshipFormTypeToRelationshipFormTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceRelationshipFormType,
//    ///         RelationshipFormType}"
//    ///     </cref>
//    /// </seealso>
//    public class ResourceRelationshipFormTypeToRelationshipFormTypeMapper :
//        IMapper<ExtensibleEnum<ResourceRelationshipFormType>, RelationshipFormType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified data type.
//        /// </summary>
//        /// <param name="dataType">Type of the data.</param>
//        /// <returns>RelationshipFormType.</returns>
//        public RelationshipFormType Map(ExtensibleEnum<ResourceRelationshipFormType> dataType)
//        {
//            return new RelationshipFormType
//            {
//                Value = dataType.ToString(),
//                OtherValue = dataType.HasOtherValue
//                    ? dataType.ToString()
//                    : null
//            };
//        }

//        #endregion
//    }
//}