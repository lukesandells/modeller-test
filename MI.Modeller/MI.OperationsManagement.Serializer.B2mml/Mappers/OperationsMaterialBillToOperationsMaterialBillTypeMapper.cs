﻿using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class OperationsMaterialBillToOperationsMaterialBillTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.OperationsMaterialBill,
	///         OperationsMaterialBillType}"
	///     </cref>
	/// </seealso>
	public class OperationsMaterialBillToOperationsMaterialBillTypeMapper :
        IMapper<OperationsMaterialBill, OperationsMaterialBillType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified operations material bill item.
		/// </summary>
		/// <param name="operationsMaterialBillItem">The operations material bill item.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>OperationsMaterialBillType.</returns>
		public OperationsMaterialBillType Map(OperationsMaterialBill operationsMaterialBillItem, bool mapAsReference)
        {
            ArgumentValidation.AssertNotNull(operationsMaterialBillItem,
                nameof(operationsMaterialBillItem));

            var b2mmlId = new IdentifierType
            {
                Value = operationsMaterialBillItem.ExternalId
            };

	        var b2mmlHierarchyScope = operationsMaterialBillItem.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(operationsMaterialBillItem.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new OperationsMaterialBillType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(operationsMaterialBillItem.Description)
                ? null
                : new DescriptionType
                {
                    Value = operationsMaterialBillItem.Description
                };

            return new OperationsMaterialBillType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                HierarchyScope = b2mmlHierarchyScope
            };
        }

        #endregion
    }
}