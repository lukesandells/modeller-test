using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialSpecificationPropertyToB2mmlMaterialSegmentSpecificationPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IB2mmlMapper{MI.Modeller.Domain.Client.MaterialSpecificationProperty,
	///         MaterialSegmentSpecificationPropertyType}"
	///     </cref>
	/// </seealso>
	public class MaterialSpecificationPropertyToMaterialSegmentSpecificationPropertyTypeMapper<TProcessDefinition> :
        IMapper<QuantitySpecificResourceTransactionProperty<MaterialSpecification<TProcessDefinition>>, MaterialSegmentSpecificationPropertyType>
        where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material specification.
		/// </summary>
		/// <param name="materialSpecification">The material specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>MaterialSegmentSpecificationPropertyType.</returns>
		public MaterialSegmentSpecificationPropertyType Map(QuantitySpecificResourceTransactionProperty<MaterialSpecification<TProcessDefinition>> materialSpecification, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(materialSpecification,
                nameof(materialSpecification));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = materialSpecification.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = materialSpecification.ExternalId
                };

            var b2mmlDescription = materialSpecification.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialSpecification.Description
                    }
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlValue = materialSpecification.Value == null
				? null
				: materialSpecification.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);
            var b2mmlQuantity = materialSpecification.ResourceQuantity?.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);
            var b2mmlProperty = materialSpecification.SubProperties?.MapToArray(this, false);

            return new MaterialSegmentSpecificationPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                Quantity = b2mmlQuantity,
                MaterialSegmentSpecificationProperty = b2mmlProperty
            };
        }

        #endregion
    }
}