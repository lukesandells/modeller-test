﻿using System;
using MI.Framework;
using MI.OperationsManagement.Domain.Core;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
    /// <summary>
    ///     Class AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper.
    /// </summary>
    /// <seealso>
    ///     <cref>
    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.AssemblyRelationshipType,
    ///         AssemblyRelationshipType}"
    ///     </cref>
    /// </seealso>
    public class AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper :
        IMapper<ExtensibleEnum<AssemblyRelationship>, AssemblyRelationshipType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified assembly type.
		/// </summary>
		/// <param name="assemblyType">Type of the assembly.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>AssemblyRelationshipType.</returns>
		public AssemblyRelationshipType Map(ExtensibleEnum<AssemblyRelationship> assemblyType, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }
            return new AssemblyRelationshipType
            {
                Value = ((AssemblyRelationship)assemblyType).ToString(),
                OtherValue = assemblyType.HasOtherValue
                    ? assemblyType.ToString()
                    : null
            };
        }

        #endregion
    }
}