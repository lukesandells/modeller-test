﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class OperationsMaterialBillItemToOperationsMaterialBillItemTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.OperationsMaterialBillItem,
	///         OperationsMaterialBillItemType}"
	///     </cref>
	/// </seealso>
	public class OperationsMaterialBillItemToOperationsMaterialBillItemTypeMapper :
        IMapper<OperationsMaterialBillItem, OperationsMaterialBillItemType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified operations material bill item.
		/// </summary>
		/// <param name="operationsMaterialBillItem">The operations material bill item.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>OperationsMaterialBillItemType.</returns>
		public OperationsMaterialBillItemType Map(OperationsMaterialBillItem operationsMaterialBillItem, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(operationsMaterialBillItem,
                nameof(operationsMaterialBillItem));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = new IdentifierType
            {
                Value = operationsMaterialBillItem.ExternalId
            };

            var b2mmlDescription = string.IsNullOrEmpty(operationsMaterialBillItem.Description)
                ? null
                : new DescriptionType
                {
                    Value = operationsMaterialBillItem.Description
                };

            var b2mmlMaterialClassId = operationsMaterialBillItem.MaterialClass?.ExternalId == null
                ? null
                : new[]
                {
                    new MaterialClassIDType
                    {
                        Value = operationsMaterialBillItem.MaterialClass.ExternalId
                    }
                };

            var b2mmlMaterialDefinitionId = operationsMaterialBillItem.MaterialDefinition == null
                ? null
                : new[]
                {
                    new MaterialDefinitionIDType
                    {
                        Value = operationsMaterialBillItem.MaterialDefinition.ExternalId
                    }
                };

            var b2mmlUseType = operationsMaterialBillItem.UseType == null
                ? null
                : new MaterialUseType
                {
                    Value = operationsMaterialBillItem.UseType
                };

            var b2mmlAssemblyType = operationsMaterialBillItem.AssemblyType == null
                ? null
                : new AssemblyTypeToAssemblyTypeTypeMapper().Map(operationsMaterialBillItem.AssemblyType.Value, false);

            var b2mmlAssemblyRelationshipType = operationsMaterialBillItem.AssemblyRelationshipType == null
                ? null
                : new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper().Map(operationsMaterialBillItem.AssemblyRelationshipType.Value, false);

            var b2mmlMaterialSpecificationIds = new HashSet<IdentifierType>();
            foreach (var b2mmlMaterialSpecificationId in
                operationsMaterialBillItem.MaterialSpecifications.Select(v => new IdentifierType
                {
                    Value = v.ExternalId
                }))
            {
                b2mmlMaterialSpecificationIds.Add(b2mmlMaterialSpecificationId);
            }

            var b2mmlQuantity = operationsMaterialBillItem.Quantity == null
				? null
				: operationsMaterialBillItem.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlAssembly = operationsMaterialBillItem.Children.MapToArray(this, false);

            return new OperationsMaterialBillItemType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                MaterialClassID = b2mmlMaterialClassId,
                MaterialDefinitionID = b2mmlMaterialDefinitionId,
                UseType = b2mmlUseType,
                AssemblyBillOfMaterialItem = b2mmlAssembly,
                AssemblyType = b2mmlAssemblyType,
                AssemblyRelationship = b2mmlAssemblyRelationshipType,
                MaterialSpecificationID = b2mmlMaterialSpecificationIds.Count == 0
                    ? null
                    : b2mmlMaterialSpecificationIds.ToArray(),
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}