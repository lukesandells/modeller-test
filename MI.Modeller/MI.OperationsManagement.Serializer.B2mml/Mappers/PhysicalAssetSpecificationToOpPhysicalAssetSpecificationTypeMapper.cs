using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PhysicalAssetSpecificationToB2mmlOpPhysicalAssetSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PhysicalAssetSpecification,
	///         OpPhysicalAssetSpecificationType}"
	///     </cref>
	/// </seealso>
	public class PhysicalAssetSpecificationToOpPhysicalAssetSpecificationTypeMapper<TProcessDefinition> :
        IMapper<PhysicalAssetSpecification<TProcessDefinition>, OpPhysicalAssetSpecificationType>
        where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified physicalAsset specification.
		/// </summary>
		/// <param name="physicalAssetSpecification">The physicalAsset specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>OpPhysicalAssetSpecificationType.</returns>
		public OpPhysicalAssetSpecificationType Map(PhysicalAssetSpecification<TProcessDefinition> physicalAssetSpecification, bool mapAsReference)
        {
            ArgumentValidation.AssertNotNull(physicalAssetSpecification,
                nameof(physicalAssetSpecification));

            // id always filled
	        var b2mmlId = physicalAssetSpecification.ExternalId == null
		        ? null
		        : new IdentifierType
		        {
			        Value = physicalAssetSpecification.ExternalId
		        };

	        var b2mmlHierarchyScope = physicalAssetSpecification.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(physicalAssetSpecification.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new OpPhysicalAssetSpecificationType {
					HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
				};
	        }

			var b2mmlClassId = physicalAssetSpecification.PhysicalAssetClass?.ExternalId == null
                ? null
                : new[]
                {
                    new PhysicalAssetClassIDType
                    {
                        Value = physicalAssetSpecification.PhysicalAssetClass.ExternalId
                    }
                };

            var b2mmlDescription = string.IsNullOrEmpty(physicalAssetSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = physicalAssetSpecification.Description
                    }
                };

            var b2mmlUse = string.IsNullOrEmpty(physicalAssetSpecification.PhysicalAssetUse)
                ? null
                : new PhysicalAssetUseType
                {
                    Value = physicalAssetSpecification.PhysicalAssetUse
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlQuantity = physicalAssetSpecification.Quantity == null
				? null
				: physicalAssetSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlGeospatialDesignation = physicalAssetSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    physicalAssetSpecification.SpatialDefinition,
                    false);

            var b2mmlProperties =
                physicalAssetSpecification.Properties.MapToArray(
                    new PhysicalAssetSpecificationPropertyToOpPhysicalAssetSpecificationPropertyTypeMapper<TProcessDefinition>(),
                    false);

            return new OpPhysicalAssetSpecificationType
            {
                ID = b2mmlId,
                PhysicalAssetClassID = b2mmlClassId,
                Description = b2mmlDescription,
                SpatialLocation = b2mmlGeospatialDesignation,
                HierarchyScope = b2mmlHierarchyScope,
                PhysicalAssetSpecificationProperty = b2mmlProperties,
                PhysicalAssetUse = b2mmlUse,
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}