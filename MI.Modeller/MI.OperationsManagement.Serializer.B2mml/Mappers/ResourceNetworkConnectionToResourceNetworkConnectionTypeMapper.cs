﻿//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.MapperExtensions;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceNetworkConnection,
//    ///         ResourceNetworkConnectionType}"
//    ///     </cref>
//    /// </seealso>
//    public class ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper :
//        IMapper<ResourceNetworkConnection, ResourceNetworkConnectionType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified resource network connection.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        /// <returns>ResourceNetworkConnectionType.</returns>
//        public ResourceNetworkConnectionType Map(ResourceNetworkConnection resourceNetworkConnection)
//        {
//            ArgumentValidation.AssertNotNull(resourceNetworkConnection,
//                nameof(resourceNetworkConnection));

//            var b2mmlId = new IdentifierType
//            {
//                Value = resourceNetworkConnection.ExternalId
//            };

//            var b2mmlDescription = resourceNetworkConnection.Description == null
//                ? null
//                : new[]
//                {
//                    new DescriptionType
//                    {
//                        Value = resourceNetworkConnection.Description
//                    }
//                };

//            var b2mmlResourceNetworkConnectionId = resourceNetworkConnection.ResourceNetworkConnectionId == null
//                ? null
//                : new ResourceNetworkConnectionIDType
//                {
//                    Value = resourceNetworkConnection.ResourceNetworkConnectionId
//                };

//            var b2mmlFromResourceReference = resourceNetworkConnection.FromResourceReference == null
//                ? null
//                : new ResourceReferenceToResourceReferenceTypeMapper().Map(
//                    resourceNetworkConnection.FromResourceReference);

//            var b2mmlToResourceReference = resourceNetworkConnection.ToResourceReference == null
//                ? null
//                : new ResourceReferenceToResourceReferenceTypeMapper().Map(resourceNetworkConnection.ToResourceReference);

//            var b2mmlConnectionProperty =
//                resourceNetworkConnection.ConnectionProperties?.MapToArray(
//                    new BasicPropertyToResourcePropertyTypeMapper());

//            return new ResourceNetworkConnectionType
//            {
//                ID = b2mmlId,
//                Description = b2mmlDescription,
//                ConnectionProperty = b2mmlConnectionProperty,
//                FromResourceReference = b2mmlFromResourceReference,
//                ToResourceReference = b2mmlToResourceReference,
//                ResourceNetworkConnectionID = b2mmlResourceNetworkConnectionId
//            };
//        }

//        #endregion
//    }
//}