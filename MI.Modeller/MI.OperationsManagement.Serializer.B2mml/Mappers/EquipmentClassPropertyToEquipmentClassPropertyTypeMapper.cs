﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentClassPropertyToEquipmentClassPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.EquipmentClassProperty,
	///         EquipmentClassPropertyType}"
	///     </cref>
	/// </seealso>
	public class EquipmentClassPropertyToEquipmentClassPropertyTypeMapper :
        IMapper<BasicProperty<EquipmentClass>, EquipmentClassPropertyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment property.
		/// </summary>
		/// <param name="equipmentClassProperty">The equipment property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>EquipmentClassPropertyType.</returns>
		public EquipmentClassPropertyType Map(BasicProperty<EquipmentClass> equipmentClassProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(equipmentClassProperty,
                nameof(equipmentClassProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = equipmentClassProperty.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = equipmentClassProperty.ExternalId
                };

            var b2mmlDescription = equipmentClassProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipmentClassProperty.Description
                    }
                };

            var b2mmlValue = equipmentClassProperty.Value == null
				? null
				: equipmentClassProperty.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var b2mmlProperty = equipmentClassProperty.SubProperties.MapToArray(this, false);

            return new EquipmentClassPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                EquipmentClassProperty = b2mmlProperty
            };
        }

        #endregion
    }
}