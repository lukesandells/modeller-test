﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class QuantityElementToQuantityValueTypeMapper.
	/// </summary>
	/// <seealso cref="IMapper{TF,T}" />
	public class QuantityElementToQuantityValueTypeMapper : IMapper<SerialisedQuantity.Element, QuantityValueType>
    {
		#region  Interface Implementations

		/// <summary>
		/// Maps a serialised quantity element to B2MML.
		/// </summary>
		/// <param name="element">The quantity element.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>QuantityValueType.</returns>
		public QuantityValueType Map(SerialisedQuantity.Element element, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(element, nameof(element));
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			// Map the quantity string
			var quantityString = new QuantityStringType();
            if (!string.IsNullOrEmpty(element.Value))
            {
                quantityString.Value = element.Value;
            }

            // Map the other areas
            var quantityValueType = new QuantityValueType
            {
                QuantityString = quantityString
            };

            if (!string.IsNullOrEmpty(element.DataType))
            {
                quantityValueType.DataType = new DataTypeType
                {
                    Value = element.DataType
                };
            }

            if (!string.IsNullOrEmpty(element.Key))
            {
                quantityValueType.Key = new IdentifierType
                {
                    Value = element.Key
                };
            }

            if (!string.IsNullOrEmpty(element.UnitCode))
            {
                quantityValueType.UnitOfMeasure = new UnitOfMeasureType()
                {
                    Value = element.UnitCode
                };
            }

            // Map the uncertainty depending on type
            if (element.Uncertainty != null)
            {
				if (element.Uncertainty != null)
				{
					if (element.Uncertainty.UncertaintyType == UncertaintyType.AccuracyAndPrecision)
					{
						if (!string.IsNullOrEmpty(element.Uncertainty.Accuracy))
						{
							quantityValueType.Accuracy = new DataTypeType
							{
								Value = element.Uncertainty.Accuracy
							};
						}
						if (!string.IsNullOrEmpty(element.Uncertainty.Precision))
						{
							quantityValueType.Precision = new DataTypeType
							{
								Value = element.Uncertainty.Precision
							};
						}
					}
					else if (element.Uncertainty.UncertaintyType == UncertaintyType.ExpandedUncertainty)
					{
						if (!string.IsNullOrEmpty(element.Uncertainty.ExpandedUncertainty))
						{
							quantityValueType.ExpandedUncertainty = new DataTypeType
							{
								Value = element.Uncertainty.ExpandedUncertainty
							};
						}
						if (!string.IsNullOrEmpty(element.Uncertainty.ConfidenceLevel))
						{
							quantityValueType.LevelOfConfidence = new DataTypeType
							{
								Value = element.Uncertainty.ConfidenceLevel
							};
						}
						if (!string.IsNullOrEmpty(element.Uncertainty.CoverageFactor))
						{
							quantityValueType.CoverageFactor = new DataTypeType
							{
								Value = element.Uncertainty.CoverageFactor
							};
						}
					}
				}
            }

            return quantityValueType;
        }

        #endregion
    }
}