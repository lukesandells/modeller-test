﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class WorkMasterToWorkMasterTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.WorkMaster, WorkMasterType}"
	///     </cref>
	/// </seealso>
	public class WorkMasterToWorkMasterTypeMapper : IMapper<WorkMaster, WorkMasterType>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified work master.
		/// </summary>
		/// <param name="workMaster">The work master.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>WorkMasterType.</returns>
		public WorkMasterType Map(WorkMaster workMaster, bool mapAsReference)
        {
            ArgumentValidation.AssertNotNull(workMaster,
                nameof(workMaster));

            // id always filled
            var b2mmlId = workMaster.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = workMaster.ExternalId
                };

	        var b2mmlHierarchyScope = workMaster.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(workMaster.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new WorkMasterType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlVersion = workMaster.Version == null
                ? null
                : new VersionType
                {
                    Value = workMaster.Version
                };

            var b2mmlDescription = string.IsNullOrEmpty(workMaster.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = workMaster.Description
                    }
                };

            var b2mmlWorkType = workMaster.ProcessType == null
                ? null
                : new ProcessTypeToWorkTypeTypeMapper().Map(workMaster.ProcessType, false);

            var b2mmlDuration = workMaster.Duration == default(TimeSpan)
                ? null
                : workMaster.Duration.ToString();

            var b2mmlPublishedDate = workMaster.PublishedDate == default(DateTime)
                ? null
                : new PublishedDateType
                {
                    Value = workMaster.PublishedDate
                };

            var b2mmlOperationsDefinitionId = workMaster.OperationsSegment?.OperationsDefinition?.ExternalId == null
                ? null
                : new OperationsDefinitionIDType
                {
                    Value = workMaster.OperationsSegment.OperationsDefinition.ExternalId
                };

            var b2mmlOperationsSegmentId = workMaster.OperationsSegment?.ExternalId == null
                ? null
                : new OperationsSegmentIDType
                {
                    Value = workMaster.OperationsSegment.ExternalId
                };

            // mapped arrays
            var b2mmlParameter = workMaster.Properties?.MapToArray(new BasicPropertyToParameterTypeMapper<WorkMaster>(), false);
	        var b2mmlProcessSegmentId = workMaster.ProcessSegment == null
		        ? null
		        : new[]
		        {
			        new IdentifierType
			        {
				        Value = workMaster.ProcessSegment.ExternalId
			        }
		        };


			return new WorkMasterType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Duration = b2mmlDuration,
                HierarchyScope = b2mmlHierarchyScope,
                OperationsDefinitionID = b2mmlOperationsDefinitionId,
                OperationsSegmentID = b2mmlOperationsSegmentId,
                Parameter = b2mmlParameter,
                PublishedDate = b2mmlPublishedDate,
                //WorkflowSpecificationID = b2mmlWorkflowSpecificationIds,
                Version = b2mmlVersion,
                WorkType = b2mmlWorkType,
				ProcessSegmentID = b2mmlProcessSegmentId
            };
        }

        #endregion
    }
}