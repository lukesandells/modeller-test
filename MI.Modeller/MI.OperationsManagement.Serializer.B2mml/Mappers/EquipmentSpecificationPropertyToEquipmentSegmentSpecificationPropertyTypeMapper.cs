using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentSpecificationPropertyToB2mmlEquipmentSegmentSpecificationPropertyTypeMapper.
	/// </summary>
	/// <seealso
	///     cref="IMapper{TF,T}" />
	public class EquipmentSpecificationPropertyToEquipmentSegmentSpecificationPropertyTypeMapper<TProcessDefinition> :
        IMapper<QuantitySpecificResourceTransactionProperty<EquipmentSpecification<TProcessDefinition>>, EquipmentSegmentSpecificationPropertyType>
        where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment specification property.
		/// </summary>
		/// <param name="equipmentSpecificationProperty">The equipment specification property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>EquipmentSegmentSpecificationPropertyType.</returns>
		public EquipmentSegmentSpecificationPropertyType Map(
            QuantitySpecificResourceTransactionProperty<EquipmentSpecification<TProcessDefinition>> equipmentSpecificationProperty, bool mapAsReference = false)

		{
            ArgumentValidation.AssertNotNull(equipmentSpecificationProperty,
                nameof(equipmentSpecificationProperty));

			if (mapAsReference)
			{
				throw new ArgumentException(ExportMessages.NeverMapByReference);
			}

			// id always filled
			var b2mmlId = new IdentifierType
            {
                Value = equipmentSpecificationProperty.ExternalId
            };

            var b2mmlDescription = equipmentSpecificationProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipmentSpecificationProperty.Description
                    }
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlValue = equipmentSpecificationProperty.Value?.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);
            var b2mmlQuantity = equipmentSpecificationProperty.ResourceQuantity == null
				? null
				: equipmentSpecificationProperty.ResourceQuantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);
            var b2mmlProperty = equipmentSpecificationProperty.SubProperties.MapToArray(this, false);

            return new EquipmentSegmentSpecificationPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                Quantity = b2mmlQuantity,
                EquipmentSegmentSpecificationProperty = b2mmlProperty
            };
        }

        #endregion
    }
}