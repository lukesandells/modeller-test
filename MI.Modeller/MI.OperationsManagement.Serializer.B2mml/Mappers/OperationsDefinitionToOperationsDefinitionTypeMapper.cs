﻿using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class OperationsDefinitionToOperationsDefinitionTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.OperationsDefinition,
	///         OperationsDefinitionType}"
	///     </cref>
	/// </seealso>
	public class OperationsDefinitionToOperationsDefinitionTypeMapper :
        IMapper<OperationsDefinition, OperationsDefinitionType>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified operations definition.
		/// </summary>
		/// <param name="operationsDefinition">The operations definition.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>OperationsDefinitionType.</returns>
		public OperationsDefinitionType Map(OperationsDefinition operationsDefinition, bool mapAsReference)
        {
            ArgumentValidation.AssertNotNull(operationsDefinition,
                nameof(operationsDefinition));

            // id always filled
            var b2mmlId = operationsDefinition.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = operationsDefinition.ExternalId
                };

	        var b2mmlHierarchyScope = operationsDefinition.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(operationsDefinition.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new OperationsDefinitionType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(operationsDefinition.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = operationsDefinition.Description
                    }
                };

            var b2mmlOperationsType = operationsDefinition.ProcessType == null
                ? null
                : new ProcessTypeToOperationsTypeTypeMapper().Map(operationsDefinition.ProcessType.Value);

            var b2mmlWorkDefinitionId = operationsDefinition.WorkDefinitionId == null
                ? null
                : new IdentifierType
                {
                    Value = operationsDefinition.WorkDefinitionId
                };

            var b2mmlBillOfResourcesId = operationsDefinition.BillOfResourcesId == null
                ? null
                : new BillOfResourcesIDType
                {
                    Value = operationsDefinition.BillOfResourcesId
                };

            return new OperationsDefinitionType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                OperationsType = b2mmlOperationsType,
                HierarchyScope = b2mmlHierarchyScope,
                WorkDefinitionID = b2mmlWorkDefinitionId,
                BillOfResourcesID = b2mmlBillOfResourcesId
            };
        }

        #endregion
    }
}