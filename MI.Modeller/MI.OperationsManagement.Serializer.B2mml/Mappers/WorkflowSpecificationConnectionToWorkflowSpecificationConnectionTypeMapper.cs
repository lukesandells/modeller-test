﻿//using System.Linq;
//using MI.Framework;
//using MI.Framework.Validation;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.WorkflowSpecificationConnection,
//    ///         WorkflowSpecificationConnectionType}"
//    ///     </cref>
//    /// </seealso>
//    public class WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper :
//        IMapper<WorkflowSpecificationConnection, WorkflowSpecificationConnectionType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified workflow specification connection.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <returns>WorkflowSpecificationConnectionType.</returns>
//        public WorkflowSpecificationConnectionType Map(WorkflowSpecificationConnection workflowSpecificationConnection)
//        {
//            ArgumentValidation.AssertNotNull(workflowSpecificationConnection,
//                nameof(workflowSpecificationConnection));

//            // id always filled
//            var b2mmlId = new IdentifierType
//            {
//                Value = workflowSpecificationConnection.ExternalId
//            };

//            var b2mmlDescription = workflowSpecificationConnection.Description == null
//                ? null
//                : new[]
//                {
//                    new DescriptionType
//                    {
//                        Value = workflowSpecificationConnection.Description
//                    }
//                };

//            var b2mmlConnectionType = workflowSpecificationConnection.ConnectionType == null
//                ? null
//                : new IdentifierType
//                {
//                    Value = workflowSpecificationConnection.ConnectionType
//                };

//            // collection properties in the domain are never null, even if set to null in ctor
//            // they are defaulted to an empty list. So no need to check empty
//            var b2mmlFromNodeId = workflowSpecificationConnection.FromNodeIDs?.Where(n => n != null)
//                .Select(n => new DescriptionType
//                {
//                    Value = n
//                })
//                .ToArray();

//            var b2mmlToNodeId = workflowSpecificationConnection.ToNodeIDs?.Where(n => n != null)
//                .Select(n => new DescriptionType
//                {
//                    Value = n
//                })
//                .ToArray();

//            var b2mmlProperty =
//                workflowSpecificationConnection.Properties.MapToArray(
//                    new WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper());

//            return new WorkflowSpecificationConnectionType
//            {
//                ID = b2mmlId,
//                Description = b2mmlDescription,
//                ConnectionType = b2mmlConnectionType,
//                FromNodeID = b2mmlFromNodeId.IsNullOrEmpty()
//                    ? null
//                    : b2mmlFromNodeId,
//                ToNodeID = b2mmlToNodeId.IsNullOrEmpty()
//                    ? null
//                    : b2mmlToNodeId,
//                Property = b2mmlProperty
//            };
//        }

//        #endregion
//    }
//}