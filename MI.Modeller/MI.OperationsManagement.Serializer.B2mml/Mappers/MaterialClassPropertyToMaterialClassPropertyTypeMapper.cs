﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialClassPropertyToMaterialClassPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.MaterialClassProperty,
	///         MaterialClassPropertyType}"
	///     </cref>
	/// </seealso>
	public class MaterialClassPropertyToMaterialClassPropertyTypeMapper :
        IMapper<BasicProperty<MaterialClass>, MaterialClassPropertyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material property.
		/// </summary>
		/// <param name="materialClassProperty">The material property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>MaterialClassPropertyType.</returns>
		public MaterialClassPropertyType Map(BasicProperty<MaterialClass> materialClassProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(materialClassProperty,
                nameof(materialClassProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = materialClassProperty.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = materialClassProperty.ExternalId
                };

            var b2mmlDescription = materialClassProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialClassProperty.Description
                    }
                };

            var b2mmlValue = materialClassProperty.Value == null
				? null
				: materialClassProperty.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var b2mmlProperty = materialClassProperty.SubProperties.MapToArray(this, false);

            return new MaterialClassPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                MaterialClassProperty = b2mmlProperty
            };
        }

        #endregion
    }
}