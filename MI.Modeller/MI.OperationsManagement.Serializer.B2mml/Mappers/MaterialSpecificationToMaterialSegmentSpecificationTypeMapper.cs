using System.Linq;
using MI.Framework;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialSpecificationToB2mmlMaterialSegmentSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.MaterialSpecification,
	///         MaterialSegmentSpecificationType}"
	///     </cref>
	/// </seealso>
	public class MaterialSpecificationToMaterialSegmentSpecificationTypeMapper<TProcessDefinition> :
        IMapper<MaterialSpecification<TProcessDefinition>, MaterialSegmentSpecificationType>
        where TProcessDefinition : class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material specification.
		/// </summary>
		/// <param name="materialSpecification">The material specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>MaterialSegmentSpecificationType.</returns>
		public MaterialSegmentSpecificationType Map(MaterialSpecification<TProcessDefinition> materialSpecification, bool mapAsReference)
        {
            var hierarchyScope = materialSpecification.HierarchyScope == null
                ? null
                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(materialSpecification.HierarchyScope);

	        var id = materialSpecification.ExternalId == null
		        ? null
		        : new IdentifierType
				{
			        Value = materialSpecification.ExternalId
		        };

	        if (mapAsReference)
	        {
		        return new MaterialSegmentSpecificationType
		        {
			        HierarchyScope = hierarchyScope,
					ID = id
		        };
	        }

			var assemblyType = materialSpecification.AssemblyType == null
                ? null
                : new AssemblyTypeType
                {
                    Value = ((AssemblyType) materialSpecification.AssemblyType).ToString(),
                    OtherValue = ((ExtensibleEnum<AssemblyType>) materialSpecification.AssemblyType).HasOtherValue
                        ? ((ExtensibleEnum<AssemblyType>) materialSpecification.AssemblyType).ToString()
                        : null
                };
            if (assemblyType != null && (bool) materialSpecification.AssemblyType?.HasOtherValue)
            {
                assemblyType.OtherValue = materialSpecification.AssemblyType.ToString();
            }

            var materialClassId = materialSpecification.MaterialClass == null
                ? null
                : new MaterialClassIDType
                {
                    Value = materialSpecification.MaterialClass.ExternalId
                };

            var materialUse = materialSpecification.MaterialUse == null
                ? null
                : new MaterialUseType
                {
                    Value = ((MaterialUse) materialSpecification.MaterialUse).ToString(),
                    OtherValue = ((ExtensibleEnum<MaterialUse>)materialSpecification.MaterialUse).HasOtherValue
                        ? ((ExtensibleEnum<MaterialUse>)materialSpecification.MaterialUse).ToString()
                        : null
                };

            if ((materialUse != null) && (bool) materialSpecification.MaterialUse?.HasOtherValue)
            {
                materialUse.OtherValue = materialSpecification.MaterialUse.ToString();
            }

            var description = string.IsNullOrEmpty(materialSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialSpecification.Description
                    }
                };

            var materialDefinitionId = materialSpecification.MaterialDefinition == null
                ? null
                : new MaterialDefinitionIDType
                {
                    Value = materialSpecification.MaterialDefinition?.ExternalId
                };

            var geospatialDesignation = materialSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    materialSpecification.SpatialDefinition,
                    false);

            var materialSpecificationProperties =
                materialSpecification.Properties.MapToArray(
                    new MaterialSpecificationPropertyToMaterialSegmentSpecificationPropertyTypeMapper<TProcessDefinition>(),
                    false);

            var assemblySpecificationId = !materialSpecification.AssemblyElements.Any()
                ? null
                : materialSpecification.AssemblyElements.Select(c => new IdentifierType
                    {
                        Value = c.ExternalId
                    })
                    .ToArray();

            var assemblyRelationship = materialSpecification.AssemblyRelationship == null
                ? null
                : new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper().Map(
                    materialSpecification.AssemblyRelationship.Value,
                    false);

            var quantities = materialSpecification.Quantity == null
				? null
				: materialSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            return new MaterialSegmentSpecificationType
            {
                HierarchyScope = hierarchyScope,
                AssemblyType = assemblyType,
                MaterialClassID = materialClassId,
                MaterialUse = materialUse,
                Description = description,
                ID = id,
                MaterialDefinitionID = materialDefinitionId,
                GeospatialDesignation = geospatialDesignation,
                MaterialSegmentSpecificationProperty = materialSpecificationProperties,
                AssemblySpecificationID = assemblySpecificationId,
                AssemblyRelationship = assemblyRelationship,
                Quantity = quantities,
            };
        }

        #endregion
    }
}