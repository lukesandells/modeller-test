﻿using System;
using System.Linq;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class OperationsSegmentToOperationsSegmentTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.OperationsSegment, OperationsSegmentType}"
	///     </cref>
	/// </seealso>
	public class OperationsSegmentToOperationsSegmentTypeMapper : IMapper<OperationsSegment, OperationsSegmentType>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified operations segment.
		/// </summary>
		/// <param name="operationsSegment">The operations segment.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>OperationsSegmentType.</returns>
		public OperationsSegmentType Map(OperationsSegment operationsSegment, bool mapAsReference)
        {
            ArgumentValidation.AssertNotNull(operationsSegment,
                nameof(operationsSegment));

            // id always filled
            var b2mmlId = operationsSegment.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = operationsSegment.ExternalId
                };

	        var b2mmlHierarchyScope = operationsSegment.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(operationsSegment.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new OperationsSegmentType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(operationsSegment.Description)
                ? null
                : new DescriptionType
                {
                    Value = operationsSegment.Description
                };

            var b2mmlOperationsType = operationsSegment.ProcessType == null
                ? null
                : new ProcessTypeToOperationsTypeTypeMapper().Map(operationsSegment.ProcessType.Value);

            var b2mmlDuration = operationsSegment.Duration == default(TimeSpan)
                ? null
                : operationsSegment.Duration.ToString();

            var b2MmlprocessSegmentId = !operationsSegment.ProcessSegments.Any()
                ? null
                : operationsSegment.ProcessSegments.Select(e => new ProcessSegmentIDType
                    {
                        Value = e.ExternalId
                    })
                    .ToArray();

            // mapped arrays
            var b2mmlParameter = operationsSegment.Properties.MapToArray(new BasicPropertyToParameterTypeMapper<OperationsSegment>(), false);
            var b2mmlSegmentDependency =
                operationsSegment.SegmentDependencies.MapToArray(new SegmentDependencyToSegmentDependencyTypeMapper<OperationsSegment>(), false);

            return new OperationsSegmentType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                OperationsType = b2mmlOperationsType,
                HierarchyScope = b2mmlHierarchyScope,
                Duration = b2mmlDuration,
                ProcessSegmentID = b2MmlprocessSegmentId,
                Parameter = b2mmlParameter,
                SegmentDependency = b2mmlSegmentDependency,
            };
        }

        #endregion
    }
}