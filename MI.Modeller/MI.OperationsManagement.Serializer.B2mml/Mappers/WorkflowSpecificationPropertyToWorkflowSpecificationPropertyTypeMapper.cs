﻿//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.Extensions;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.WorkflowSpecificationProperty,
//    ///         WorkflowSpecificationPropertyType}"
//    ///     </cref>
//    /// </seealso>
//    public class WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper :
//        IMapper<BasicProperty<WorkflowSpecification>, WorkflowSpecificationPropertyType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified workflow specification property.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        /// <returns>WorkflowSpecificationPropertyType.</returns>
//        public WorkflowSpecificationPropertyType Map(BasicProperty<WorkflowSpecification> workflowSpecificationProperty)
//        {
//            ArgumentValidation.AssertNotNull(workflowSpecificationProperty,
//                nameof(workflowSpecificationProperty));

//            var b2mmlId = workflowSpecificationProperty.ExternalId == null
//                ? null
//                : new IdentifierType
//                {
//                    Value = workflowSpecificationProperty.ExternalId
//                };

//            var b2mmlDescription = workflowSpecificationProperty.Description == null
//                ? null
//                : new[]
//                {
//                    new DescriptionType
//                    {
//                        Value = workflowSpecificationProperty.Description
//                    }
//                };

//            // collection properties in the domain are never null, even if set to null in ctor
//            // they are defaulted to an empty list. So no need to check empty
//            var b2mmlValue = workflowSpecificationProperty.Value.Elements.MapToArray(new ValueElementToValueTypeMapper());
//            var b2mmlProperty = workflowSpecificationProperty.SubProperties.MapToArray(this);

//            return new WorkflowSpecificationPropertyType
//            {
//                ID = b2mmlId,
//                Description = b2mmlDescription,
//                Value = b2mmlValue,
//                Property = b2mmlProperty
//            };
//        }

//        #endregion
//    }
//}