using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PersonnelSpecificationToB2mmlPersonnelSegmentSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PersonnelSpecification,
	///         PersonnelSegmentSpecificationType}"
	///     </cref>
	/// </seealso>
	public class PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapper<TProcessDefinition> :
        IMapper<PersonnelSpecification<TProcessDefinition>, PersonnelSegmentSpecificationType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified personnel specification.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>PersonnelSegmentSpecificationType.</returns>
		public PersonnelSegmentSpecificationType Map(PersonnelSpecification<TProcessDefinition> personnelSpecification, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(personnelSpecification,
                nameof(personnelSpecification));

            // id always filled
            var b2mmlId = personnelSpecification.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = personnelSpecification.ExternalId
                };

            var b2mmlClassId = personnelSpecification.PersonnelClass?.ExternalId == null
                ? null
                : new PersonnelClassIDType
                {
                    Value = personnelSpecification.PersonnelClass?.ExternalId
                };

            var b2mmlDescription = string.IsNullOrEmpty(personnelSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = personnelSpecification.Description
                    }
                };

            var b2mmlUse = string.IsNullOrEmpty(personnelSpecification.PersonnelUse)
                ? null
                : new PersonnelUseType
                {
                    Value = personnelSpecification.PersonnelUse
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlQuantity = personnelSpecification.Quantity == null
				? null
				: personnelSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlHierarchyScope = personnelSpecification.HierarchyScope == null
                ? null
                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(personnelSpecification.HierarchyScope);

            var b2mmlProperties =
                personnelSpecification.Properties.MapToArray(
                    new PersonnelSpecificationPropertyToPersonnelSegmentSpecificationPropertyTypeMapper<TProcessDefinition>(),
                    false);

            return new PersonnelSegmentSpecificationType
            {
                ID = b2mmlId,
                PersonnelClassID = b2mmlClassId,
                Description = b2mmlDescription,
                HierarchyScope = b2mmlHierarchyScope,
                PersonnelSegmentSpecificationProperty = b2mmlProperties,
                PersonnelUse = b2mmlUse,
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}