using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PersonnelSpecificationToB2mmlOpPersonnelSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PersonnelSpecification,
	///         OpPersonnelSpecificationType}"
	///     </cref>
	/// </seealso>
	public class PersonnelSpecificationToOpPersonnelSpecificationTypeMapper<TProcessDefinition> :
        IMapper<PersonnelSpecification<TProcessDefinition>, OpPersonnelSpecificationType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified personnel specification.
		/// </summary>
		/// <param name="personnelSpecification">The personnel specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>OpPersonnelSpecificationType.</returns>
		public OpPersonnelSpecificationType Map(PersonnelSpecification<TProcessDefinition> personnelSpecification, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(personnelSpecification,
                nameof(personnelSpecification));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = personnelSpecification.ExternalId == null
		        ? null
		        : new IdentifierType
		        {
			        Value = personnelSpecification.ExternalId
		        };

            var b2mmlClassId = personnelSpecification.PersonnelClass?.ExternalId == null
                ? null
                : new[]
                {
                    new PersonnelClassIDType
                    {
                        Value = personnelSpecification.PersonnelClass.ExternalId
                    }
                };

            var b2mmlDescription = string.IsNullOrEmpty(personnelSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = personnelSpecification.Description
                    }
                };

            var b2mmlUse = string.IsNullOrEmpty(personnelSpecification.PersonnelUse)
                ? null
                : new PersonnelUseType
                {
                    Value = personnelSpecification.PersonnelUse
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlQuantity = personnelSpecification.Quantity == null
				? null
				: personnelSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlSpatialLocation = personnelSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    personnelSpecification.SpatialDefinition,
                    false);

            var b2mmlHierarchyScope = personnelSpecification.HierarchyScope == null
                ? null
                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(personnelSpecification.HierarchyScope);

            var b2mmlProperties =
                personnelSpecification.Properties.MapToArray(
                    new PersonnelSpecificationPropertyToOpPersonnelSpecificationPropertyTypeMapper<TProcessDefinition>(),
                    false);

            return new OpPersonnelSpecificationType
            {
                ID = b2mmlId,
                PersonnelClassID = b2mmlClassId,
                Description = b2mmlDescription,
                SpatialLocation = b2mmlSpatialLocation,
                HierarchyScope = b2mmlHierarchyScope,
                PersonnelSpecificationProperty = b2mmlProperties,
                PersonnelUse = b2mmlUse,
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}