using System;
using MI.Framework;
using MI.OperationsManagement.Domain.Core;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class AssemblyTypeToAssemblyTypeTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.AssemblyType, AssemblyTypeType}"
	///     </cref>
	/// </seealso>
	public class AssemblyTypeToAssemblyTypeTypeMapper : IMapper<ExtensibleEnum<AssemblyType>, AssemblyTypeType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified assembly type.
		/// </summary>
		/// <param name="assemblyType">Type of the assembly.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>AssemblyTypeType.</returns>
		public AssemblyTypeType Map(ExtensibleEnum<AssemblyType> assemblyType, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }
			return new AssemblyTypeType
            {
                Value = ((AssemblyType) assemblyType).ToString(),
                OtherValue = assemblyType.HasOtherValue
                    ? assemblyType.ToString()
                    : default(string)
            };
        }

        #endregion
    }
}