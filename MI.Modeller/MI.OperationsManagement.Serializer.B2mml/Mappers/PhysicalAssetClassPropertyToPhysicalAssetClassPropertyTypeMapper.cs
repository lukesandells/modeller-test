﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PhysicalAssetClassPropertyToPhysicalAssetClassPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PhysicalAssetClassProperty,
	///         PhysicalAssetClassPropertyType}"
	///     </cref>
	/// </seealso>
	public class PhysicalAssetClassPropertyToPhysicalAssetClassPropertyTypeMapper :
        IMapper<BasicProperty<PhysicalAssetClass>, PhysicalAssetClassPropertyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified physicalAsset property.
		/// </summary>
		/// <param name="physicalAssetClassProperty">The physicalAsset property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.Should always be false</param>
		/// <returns>PhysicalAssetClassPropertyType.</returns>
		public PhysicalAssetClassPropertyType Map(BasicProperty<PhysicalAssetClass> physicalAssetClassProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(physicalAssetClassProperty,
                nameof(physicalAssetClassProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = physicalAssetClassProperty.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = physicalAssetClassProperty.ExternalId
                };

            var b2mmlDescription = physicalAssetClassProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = physicalAssetClassProperty.Description
                    }
                };

            var b2mmlValue = physicalAssetClassProperty.Value == null
				? null
				: physicalAssetClassProperty.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var b2mmlProperty = physicalAssetClassProperty.SubProperties.MapToArray(this, false);

            return new PhysicalAssetClassPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                PhysicalAssetClassProperty = b2mmlProperty
            };
        }

        #endregion
    }
}