using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PhysicalAssetSpecificationPropertyToB2mmlPhysicalAssetSegmentSpecificationPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PhysicalAssetSpecificationProperty,
	///         PhysicalAssetSegmentSpecificationPropertyType}"
	///     </cref>
	/// </seealso>
	public class PhysicalAssetSpecificationPropertyToPhysicalAssetSegmentSpecificationPropertyTypeMapper<TProcessDefinition> :
        IMapper<QuantitySpecificResourceTransactionProperty<PhysicalAssetSpecification<TProcessDefinition>>, PhysicalAssetSegmentSpecificationPropertyType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified physical asset specification property.
		/// </summary>
		/// <param name="physicalAssetSpecificationProperty">The physical asset specification property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>PhysicalAssetSegmentSpecificationPropertyType.</returns>
		public PhysicalAssetSegmentSpecificationPropertyType Map(
            QuantitySpecificResourceTransactionProperty<PhysicalAssetSpecification<TProcessDefinition>> physicalAssetSpecificationProperty, bool mapAsReference = false)

		{
            ArgumentValidation.AssertNotNull(physicalAssetSpecificationProperty,
                nameof(physicalAssetSpecificationProperty));

			if (mapAsReference)
			{
				throw new ArgumentException(ExportMessages.NeverMapByReference);
			}

			// id always filled
			var b2mmlId = new IdentifierType
            {
                Value = physicalAssetSpecificationProperty.ExternalId
            };

            var b2mmlDescription = physicalAssetSpecificationProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = physicalAssetSpecificationProperty.Description
                    }
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlValue = physicalAssetSpecificationProperty.Value?.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);
            var b2mmlQuantity = physicalAssetSpecificationProperty.ResourceQuantity?.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);
            var b2mmlProperty = physicalAssetSpecificationProperty.SubProperties.MapToArray(this, false);

            return new PhysicalAssetSegmentSpecificationPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                Quantity = b2mmlQuantity,
                PhysicalAssetSegmentSpecificationProperty = b2mmlProperty
            };
        }

        #endregion
    }
}