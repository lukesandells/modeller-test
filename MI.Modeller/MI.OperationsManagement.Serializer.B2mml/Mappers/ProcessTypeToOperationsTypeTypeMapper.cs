﻿using System;
using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class ProcessTypeToOperationsTypeTypeMapper.
	/// </summary>
	/// <seealso cref="IMapper{TF,T}" />
	public class ProcessTypeToOperationsTypeTypeMapper : IMapper<ExtensibleEnum<ProcessType>, OperationsTypeType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified operation type.
		/// </summary>
		/// <param name="operationsType">Type of the operation.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>OperationsTypeType.</returns>
		public OperationsTypeType Map(ExtensibleEnum<ProcessType> operationsType, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			return new OperationsTypeType
            {
                Value = ((ProcessType) operationsType).ToString(),
                OtherValue = operationsType.HasOtherValue
                    ? operationsType.ToString()
                    : null
            };
        }

        #endregion
    }
}