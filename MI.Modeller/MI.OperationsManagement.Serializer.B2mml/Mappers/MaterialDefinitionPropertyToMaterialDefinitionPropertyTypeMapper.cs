﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialDefinitionPropertyToMaterialDefinitionPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.MaterialDefinitionProperty,
	///         MaterialDefinitionPropertyType}"
	///     </cref>
	/// </seealso>
	public class MaterialDefinitionPropertyToMaterialDefinitionPropertyTypeMapper :
        IMapper<BasicProperty<MaterialDefinition>, MaterialDefinitionPropertyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material definition.
		/// </summary>
		/// <param name="materialDefinition">The material definition.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>MaterialDefinitionPropertyType.</returns>
		public MaterialDefinitionPropertyType Map(BasicProperty<MaterialDefinition> materialDefinition, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(materialDefinition,
                nameof(materialDefinition));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = materialDefinition.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = materialDefinition.ExternalId
                };

            var b2mmlDescription = materialDefinition.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialDefinition.Description
                    }
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlValue = materialDefinition.Value == null
				? null
				: materialDefinition.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var b2mmlProperty = materialDefinition.SubProperties.MapToArray(this, false);

            return new MaterialDefinitionPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                MaterialDefinitionProperty = b2mmlProperty
            };
        }

        #endregion
    }
}