﻿using System;
using MI.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
    /// <summary>
    ///     Class DependencyTypeToDependencyTypeMapper.
    /// </summary>
    /// <seealso>
    ///     <cref>B2mmlMappers.IB2mmlMapper{Domain.Client.DependencyType, DependencyType}</cref>
    /// </seealso>
    public class DependencyTypeToDependencyTypeMapper : IMapper<ExtensibleEnum<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType>, DependencyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified dependency type.
		/// </summary>
		/// <param name="dependencyType">Type of the dependency.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>DependencyType.</returns>
		public DependencyType Map(ExtensibleEnum<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType> dependencyType, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			return new DependencyType
            {
                Value = ((OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType) dependencyType).ToString(),
                OtherValue = dependencyType.HasOtherValue
                    ? dependencyType.ToString()
                    : default(string)
            };
        }

        #endregion
    }
}