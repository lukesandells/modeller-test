﻿using System;
using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentLevelToEquipmentElementLevelTypeMapper.
	/// </summary>
	/// <seealso
	///     cref="IMapper{TF,T}" />
	public class EquipmentLevelToEquipmentElementLevelTypeMapper :
        IMapper<ExtensibleEnum<EquipmentLevel>, EquipmentElementLevelType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment level type.
		/// </summary>
		/// <param name="equipmentLevel">Type of the equipment level.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>EquipmentElementLevelType.</returns>
		public EquipmentElementLevelType Map(ExtensibleEnum<EquipmentLevel> equipmentLevel, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			return new EquipmentElementLevelType
            {
                Value = ((EquipmentLevel)equipmentLevel).ToString(),
                OtherValue = equipmentLevel.HasOtherValue ? equipmentLevel.ToString() : null
            };
        }

        #endregion
    }
}