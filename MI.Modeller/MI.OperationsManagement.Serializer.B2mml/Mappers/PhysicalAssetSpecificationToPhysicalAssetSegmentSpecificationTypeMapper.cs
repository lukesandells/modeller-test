using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PhysicalAssetSpecificationToB2mmlPhysicalAssetSegmentSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PhysicalAssetSpecification,
	///         PhysicalAssetSegmentSpecificationType}"
	///     </cref>
	/// </seealso>
	public class PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapper<TOfObject> :
        IMapper<PhysicalAssetSpecification<TOfObject>, PhysicalAssetSegmentSpecificationType>
        where TOfObject : class, IProcessDefinition<TOfObject>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified physical asset specification.
		/// </summary>
		/// <param name="physicalAssetSpecification">The physical asset specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>PhysicalAssetSegmentSpecificationType.</returns>
		public PhysicalAssetSegmentSpecificationType Map(PhysicalAssetSpecification<TOfObject> physicalAssetSpecification, bool mapAsReference)
		{
            ArgumentValidation.AssertNotNull(physicalAssetSpecification,
                nameof(physicalAssetSpecification));

            var b2mmlId = physicalAssetSpecification.ExternalId == null
                ? null
                : new IdentifierType()
                {
                    Value = physicalAssetSpecification.ExternalId
                };

			var b2mmlHierarchyScope = physicalAssetSpecification.HierarchyScope == null
				? null
				: new HierarchyScopeToHierarchyScopeTypeMapper().Map(physicalAssetSpecification.HierarchyScope);

			if (mapAsReference)
			{
				return new PhysicalAssetSegmentSpecificationType
				{
					HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
				};
			}

			var b2mmlClassId = physicalAssetSpecification.PhysicalAssetClass?.ExternalId == null
                ? null
                : new PhysicalAssetClassIDType
                {
                    Value = physicalAssetSpecification.PhysicalAssetClass.ExternalId
                };

            var b2mmlDescription = string.IsNullOrEmpty(physicalAssetSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = physicalAssetSpecification.Description
                    }
                };

            var b2mmlUse = string.IsNullOrEmpty(physicalAssetSpecification.PhysicalAssetUse)
                ? null
                : new PhysicalAssetUseType
                {
                    Value = physicalAssetSpecification.PhysicalAssetUse
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlQuantity = physicalAssetSpecification.Quantity == null
				? null
				: physicalAssetSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            var b2mmlGeospatialDesignation = physicalAssetSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    physicalAssetSpecification.SpatialDefinition);

            var b2mmlProperties =
                physicalAssetSpecification.Properties.MapToArray(
                    new PhysicalAssetSpecificationPropertyToPhysicalAssetSegmentSpecificationPropertyTypeMapper<TOfObject>(),
                    false);

            return new PhysicalAssetSegmentSpecificationType
            {
                ID = b2mmlId,
                PhysicalAssetClassID = b2mmlClassId,
                Description = b2mmlDescription,
                GeospatialDesignation = b2mmlGeospatialDesignation,
                HierarchyScope = b2mmlHierarchyScope,
                PhysicalAssetSegmentSpecificationProperty = b2mmlProperties,
                PhysicalAssetUse = b2mmlUse,
                Quantity = b2mmlQuantity
            };
        }

        #endregion
    }
}