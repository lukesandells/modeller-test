using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialDefinitionToMaterialDefinitionTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.MaterialDefinition,
	///         MaterialDefinitionType}"
	///     </cref>
	/// </seealso>
	public class MaterialDefinitionToMaterialDefinitionTypeMapper : IMapper<MaterialDefinition, MaterialDefinitionType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material definition.
		/// </summary>
		/// <param name="materialDefinition">The material definition.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>MaterialDefinitionType.</returns>
		public MaterialDefinitionType Map(MaterialDefinition materialDefinition, bool mapAsReference)
        {
            var b2mmlId = materialDefinition.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = materialDefinition.ExternalId
                };

	        var b2mmlHierarchyScope = materialDefinition.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(materialDefinition.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new MaterialDefinitionType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(materialDefinition.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialDefinition.Description
                    }
                };

            

            var b2mmlMaterialDefinitionProperty =
                materialDefinition.Properties.MapToArray(
                    new MaterialDefinitionPropertyToMaterialDefinitionPropertyTypeMapper(),
                    false);

            var b2mmlMaterialClassId = !materialDefinition.MaterialClasses.Any()
                ? null
                : materialDefinition.MaterialClasses.Select(e => new MaterialClassIDType
                    {
                        Value = e.ExternalId
                    })
                    .ToArray();

            var b2mmlAssemblyDefinitionId = !materialDefinition.AssemblyElements.Any()
                ? null
                : materialDefinition.AssemblyElements.Select(e => new MaterialDefinitionIDType
                    {
                        Value = e.ExternalId
                })
                    .ToArray();

            var b2mmlAssemblyType = materialDefinition.AssemblyType == null
                ? null
                : new AssemblyTypeToAssemblyTypeTypeMapper().Map(materialDefinition.AssemblyType.Value, false);

            var b2mmlAssemblyRelationship = materialDefinition.AssemblyRelationship == null
                ? null
                : new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper().Map(
                    materialDefinition.AssemblyRelationship.Value,
                    false);

            return new MaterialDefinitionType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                AssemblylDefinitionID = b2mmlAssemblyDefinitionId,
                AssemblyRelationship = b2mmlAssemblyRelationship,
                AssemblyType = b2mmlAssemblyType,
                HierarchyScope = b2mmlHierarchyScope,
                MaterialClassID = b2mmlMaterialClassId,
                MaterialDefinitionProperty = b2mmlMaterialDefinitionProperty,
            };
        }

        #endregion
    }
}