﻿using System;
using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class ProcessTypeToWorkTypeTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ProcessType, WorkTypeType}"
	///     </cref>
	/// </seealso>
	public class ProcessTypeToWorkTypeTypeMapper : IMapper<ExtensibleEnum<ProcessType>?, WorkTypeType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified work type.
		/// </summary>
		/// <param name="workType">Type of the work.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>WorkTypeType.</returns>
		public WorkTypeType Map(ExtensibleEnum<ProcessType>? workType, bool mapAsReference = false)
        {
			if (mapAsReference)
			{
				throw new ArgumentException(ExportMessages.NeverMapByReference);
			}
			return new WorkTypeType
			{
				Value = ((ProcessType)workType).ToString(),
				OtherValue = workType?.HasOtherValue ?? false ? workType.ToString() : null
            };
        }

        #endregion
    }
}