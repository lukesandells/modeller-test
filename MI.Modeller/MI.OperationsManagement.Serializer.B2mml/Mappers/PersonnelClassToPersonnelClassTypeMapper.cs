using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PersonnelClassToPersonnelClassTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PersonnelClass, PersonnelClassType}"</cref>
	/// </seealso>
	public class PersonnelClassToPersonnelClassTypeMapper : IMapper<PersonnelClass, PersonnelClassType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified personnel class.
		/// </summary>
		/// <param name="personnelClass">The personnel class.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>PersonnelClassType.</returns>
		public PersonnelClassType Map(PersonnelClass personnelClass, bool mapAsReference)
        {
            var b2mmlId = personnelClass.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = personnelClass.ExternalId
                };

	        var b2mmlHierarchyScope = personnelClass.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(personnelClass.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new PersonnelClassType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(personnelClass.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = personnelClass.Description
                    }
                };

           
            var b2mmlPersonnelClassProperty =
                personnelClass.Properties.MapToArray(
                    new PersonnelClassPropertyToPersonnelClassPropertyTypeMapper(),
                    false);

            return new PersonnelClassType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                HierarchyScope = b2mmlHierarchyScope,
                PersonnelClassProperty = b2mmlPersonnelClassProperty
            };
        }

        #endregion
    }
}