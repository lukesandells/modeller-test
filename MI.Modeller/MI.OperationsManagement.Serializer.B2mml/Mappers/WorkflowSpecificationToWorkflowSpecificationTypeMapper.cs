﻿//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.Extensions;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationToWorkflowSpecificationTypeMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.WorkflowSpecification,
//    ///         WorkflowSpecificationType}"
//    ///     </cref>
//    /// </seealso>
//    public class WorkflowSpecificationToWorkflowSpecificationTypeMapper :
//        IMapper<WorkflowSpecification, WorkflowSpecificationType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified workflow specification.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <returns>WorkflowSpecificationType.</returns>
//        public WorkflowSpecificationType Map(WorkflowSpecification workflowSpecification)
//        {
//            ArgumentValidation.AssertNotNull(workflowSpecification,
//                nameof(workflowSpecification));

//            // id always filled
//            var b2mmlId = new IdentifierType
//            {
//                Value = workflowSpecification.Id
//            };

//            var b2mmlVersion = workflowSpecification.Version == null
//                ? null
//                : new VersionType
//                {
//                    Value = workflowSpecification.Version
//                };

//            var b2mmlDescription = workflowSpecification.Description == null
//                ? null
//                : new[]
//                {
//                    new DescriptionType
//                    {
//                        Value = workflowSpecification.Description
//                    }
//                };

//            var b2mmlHierarchyScope = workflowSpecification.HierarchyScope == null
//                ? null
//                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(workflowSpecification.HierarchyScope);

//            // collection properties in the domain are never null, even if set to null in ctor
//            // they are defaulted to an empty list. So no need to check empty
//            var b2mmlNode =
//                workflowSpecification.Nodes.MapToArray(
//                    new WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper());
//            var b2mmlConnection =
//                workflowSpecification.Connections.MapToArray(
//                    new WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper());

//            return new WorkflowSpecificationType
//            {
//                ID = b2mmlId,
//                Version = b2mmlVersion,
//                Description = b2mmlDescription,
//                Connection = b2mmlConnection,
//                HierarchyScope = b2mmlHierarchyScope,
//                Node = b2mmlNode
//            };
//        }

//        #endregion
//    }
//}