using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentToEquipmentTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.Equipment, EquipmentType}"</cref>
	/// </seealso>
	public class EquipmentToEquipmentTypeMapper : IMapper<Equipment, EquipmentType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment.
		/// </summary>
		/// <param name="equipment">The equipment.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>EquipmentType.</returns>
		public EquipmentType Map(Equipment equipment, bool mapAsReference)
        {
            var b2mmlId = equipment.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = equipment.ExternalId
                };

	        var b2mmlHierarchyScope = equipment.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(equipment.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new EquipmentType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(equipment.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipment.Description
                    }
                };

            var b2mmlEquipmentLevel = new EquipmentLevelToEquipmentElementLevelTypeMapper().Map(equipment.EquipmentLevel);

            var b2mmlEquipmentProperty =
                equipment.Properties?.MapToArray(new EquipmentPropertyToEquipmentPropertyTypeMapper(), false);

            var b2mmlEquipmentClassId = !equipment.EquipmentClasses.Any()
                ? null
                : equipment.EquipmentClasses.Select(e => new EquipmentClassIDType
                    {
                        Value = e.ExternalId
                    })
                    .ToArray();

            return new EquipmentType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                HierarchyScope = b2mmlHierarchyScope,
                EquipmentLevel = b2mmlEquipmentLevel,
                EquipmentProperty = b2mmlEquipmentProperty,
                EquipmentClassID = b2mmlEquipmentClassId
            };
        }

        #endregion
    }
}