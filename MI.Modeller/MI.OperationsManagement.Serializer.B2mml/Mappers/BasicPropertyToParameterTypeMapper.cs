﻿using System;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class for mapping Paramaters from the domain model to B2mml
	/// </summary>
	public class BasicPropertyToParameterTypeMapper<TOfObject> 
        : IMapper<BasicProperty<TOfObject>, ParameterType>
        where TOfObject : class, IWithBasicProperties<TOfObject>, IWithExternalId
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps a single Parameter from the domain model to B2mml
		/// </summary>
		/// <param name="property">The property to map</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>a domain model representation of the properties</returns>
		public ParameterType Map(BasicProperty<TOfObject> property, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = new IdentifierType
            {
                Value = property.ExternalId
            };

            var b2mmlDescription = property.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = property.Description
                    }
                };

            var values = property.Value == null
				? null
				: property.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var children = property.SubProperties.MapToArray(this, false);

            return new ParameterType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = values,
                Parameter = children
            };
        }

        #endregion
    }
}