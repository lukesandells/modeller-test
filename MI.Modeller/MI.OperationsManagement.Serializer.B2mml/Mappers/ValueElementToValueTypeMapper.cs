﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class QuantityElementToQuantityValueTypeMapper.
	/// </summary>
	/// <seealso cref="IMapper{TF,T}" />
	public class ValueElementToValueTypeMapper : IMapper<SerialisedValue.Element, ValueType>
    {
		#region  Interface Implementations

		/// <summary>
		/// Maps the specified serialised value element.
		/// </summary>
		/// <param name="value">The serialised value element.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>ValueType.</returns>
		public ValueType Map(SerialisedValue.Element value, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(value, nameof(value));
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			// Map the value string
			var valueString = new ValueStringType();

            if (!string.IsNullOrEmpty(value.Value))
            {
                valueString.Value = value.Value;
            }

            if (!string.IsNullOrEmpty(value.CurrencyCode))
            {
                valueString.currencyCodeListVersionID = value.CurrencyCode;
            }

            if (!string.IsNullOrEmpty(value.LanguageCode))
            {
                valueString.languageID = value.LanguageCode;
            }

            if (!string.IsNullOrEmpty(value.LanguageCode))
            {
                valueString.languageID = value.LanguageCode;
            }

            // Map other elements
            var valueType = new ValueType
            {
                ValueString = valueString
            };

            if (!string.IsNullOrEmpty(value.DataType))
            {
                valueType.DataType = new DataTypeType
                {
                    Value = value
                        .DataType
                };
            }

            if (!string.IsNullOrEmpty(value.Key))
            {
                valueType.Key = new IdentifierType
                {
                    Value = value
                        .Key
                };
            }

            if (!string.IsNullOrEmpty(value.Key))
            {
                valueType.Key = new IdentifierType
                {
                    Value = value
                        .Key
                };
            }

            if (!string.IsNullOrEmpty(value.UnitCode))
            {
                valueType.UnitOfMeasure = new UnitOfMeasureType()
                {
                    Value = value
                        .UnitCode
                };
            }
            
            // Map the uncertainty depending on type
            if (value.Uncertainty != null)
            {
                if (value.Uncertainty.UncertaintyType == UncertaintyType.AccuracyAndPrecision)
                {
                    if (!string.IsNullOrEmpty(value.Uncertainty.Accuracy))
                    {
                        valueType.Accuracy = new DataTypeType
                        {
                            Value = value.Uncertainty.Accuracy
                        };
                    }
                    if (!string.IsNullOrEmpty(value.Uncertainty.Precision))
                    {
                        valueType.Precision = new DataTypeType
                        {
                            Value = value.Uncertainty.Precision
                        };
                    }
                }
                if (value.Uncertainty.UncertaintyType == UncertaintyType.ExpandedUncertainty)
                {
                    if (!string.IsNullOrEmpty(value.Uncertainty.ExpandedUncertainty))
                    {
                        valueType.ExpandedUncertainty = new DataTypeType
                        {
                            Value = value.Uncertainty.ExpandedUncertainty
                        };
                    }
                    if (!string.IsNullOrEmpty(value.Uncertainty.ConfidenceLevel))
                    {
                        valueType.LevelOfConfidence = new DataTypeType
                        {
                            Value = value.Uncertainty.ConfidenceLevel
                        };
                    }
                    if (!string.IsNullOrEmpty(value.Uncertainty.CoverageFactor))
                    {
                        valueType.CoverageFactor = new DataTypeType
                        {
                            Value = value.Uncertainty.CoverageFactor
                        };
                    }
                }
            }
            return valueType;
        }

        #endregion
    }
}