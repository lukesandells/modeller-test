﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PersonnelSpecificationPropertyToB2mmlPersonnelSegmentSpecificationPropertyTypeMapper.
	/// </summary>
	/// <seealso
	///     cref="IMapper{TF,T}" />
	public class PersonnelSpecificationPropertyToPersonnelSegmentSpecificationPropertyTypeMapper<TProcessDefinition> :
        IMapper<QuantitySpecificResourceTransactionProperty<PersonnelSpecification<TProcessDefinition>>, PersonnelSegmentSpecificationPropertyType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified personnel specification property.
		/// </summary>
		/// <param name="personnelSpecificationProperty">The personnel specification property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>PersonnelSegmentSpecificationPropertyType.</returns>
		public PersonnelSegmentSpecificationPropertyType Map(
            QuantitySpecificResourceTransactionProperty<PersonnelSpecification<TProcessDefinition>> personnelSpecificationProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(personnelSpecificationProperty,
                nameof(personnelSpecificationProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			// id always filled
			var b2mmlId = new IdentifierType
            {
                Value = personnelSpecificationProperty.ExternalId
            };

            var b2mmlDescription = personnelSpecificationProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = personnelSpecificationProperty.Description
                    }
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlValue = personnelSpecificationProperty.Value?.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);
            var b2mmlQuantity = personnelSpecificationProperty.ResourceQuantity?.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);
            var b2mmlProperty = personnelSpecificationProperty.SubProperties.MapToArray(this, false);

            return new PersonnelSegmentSpecificationPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                Quantity = b2mmlQuantity,
                PersonnelSegmentSpecificationProperty = b2mmlProperty
            };
        }

        #endregion
    }
}