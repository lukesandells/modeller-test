﻿using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions
{
    /// <summary>
    ///     Class MapperExtensions.
    /// </summary>
    public static class MapperExtensions
    {
		#region Members

		/// <summary>
		///     Maps from a Readonly collection of Domain objects, into an array of B2mml Objects
		///     using the specificed mapping instance
		/// </summary>
		/// <typeparam name="TIn">Input Type (Domain Object).</typeparam>
		/// <typeparam name="TOut">Output type (B2mml Object).</typeparam>
		/// <param name="input">The input collection.</param>
		/// <param name="mapper">The mapper.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated</param>
		/// <returns>TOut[] Array.</returns>
		public static TOut[] MapToArray<TIn, TOut>(this IEnumerable<TIn> input, IMapper<TIn, TOut> mapper, bool mapAsReference)
        {
            var enumerable = input as TIn[] ?? input?.ToArray();
            if (input == null || !enumerable.Any())
            {
                return null;
            }

            ArgumentValidation.AssertNotNull(mapper,
                nameof(mapper));

            var b2mmlArray = new TOut[enumerable.Length];
            foreach (var item in enumerable.Select((val,
                i) => new
            {
                i,
                val
            }))
            {
                b2mmlArray[item.i] = mapper.Map(item.val, mapAsReference);
            }
            return b2mmlArray;
        }

		/// <summary>
		///     Maps from an array of B2mml objects to a set of domain objects.
		/// </summary>
		/// <typeparam name="TIn">Input Type (B2mml)</typeparam>
		/// <typeparam name="TOut">Output Type (Domain)</typeparam>
		/// <param name="input">The input.</param>
		/// <param name="mapper">The mapper.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated</param>
		/// <returns>List&lt;TOut&gt;.</returns>
		public static ISet<TOut> MapToSet<TIn, TOut>(this TIn[] input, IMapper<TIn, TOut> mapper, bool mapAsReference)
        {
            if (input.IsNullOrEmpty())
            {
                return new HashSet<TOut>();
            }

            ArgumentValidation.AssertNotNull(mapper,
                nameof(mapper));

            var output = new HashSet<TOut>();

            foreach (var item in input)
            {
                if (item != null)
                {
                    output.Add(mapper.Map(item, mapAsReference));
                }
            }

            return output;
        }

	    /// <summary>
	    ///     Maps from an array of B2mml objects to a list of domain objects.
	    /// </summary>
	    /// <typeparam name="TIn">Input Type (B2mml)</typeparam>
	    /// <typeparam name="TOut">Output Type (Domain)</typeparam>
	    /// <param name="input">The input.</param>
	    /// <param name="mapper">The mapper.</param>
	    /// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated</param>
	    /// <returns>List&lt;TOut&gt;.</returns>
	    public static IList<TOut> MapToList<TIn, TOut>(this TIn[] input, IMapper<TIn, TOut> mapper, bool mapAsReference)
        {
            if (input.IsNullOrEmpty())
            {
                return new List<TOut>();
            }

            ArgumentValidation.AssertNotNull(mapper,
                nameof(mapper));

            var output = new List<TOut>();

            foreach (var item in input)
            {
                if (item != null)
                {
                    output.Add(mapper.Map(item, mapAsReference));
                }
            }

            return output;
        }

        #endregion
    }
}