using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialSpecificationToB2mmlOpMaterialSpecificationTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IB2mmlMapper{MI.Modeller.Domain.Client.MaterialSpecification,
	///         OpMaterialSpecificationType}"
	///     </cref>
	/// </seealso>
	public class MaterialSpecificationToOpMaterialSpecificationTypeMapper<TProcessDefinition> 
        : IMapper<MaterialSpecification<TProcessDefinition>, OpMaterialSpecificationType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified material specification.
		/// </summary>
		/// <param name="materialSpecification">The material specification.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>OpPersonnelSpecificationType.</returns>
		public OpMaterialSpecificationType Map(MaterialSpecification<TProcessDefinition> materialSpecification, bool mapAsReference)
        {
            var hierarchyScope = materialSpecification.HierarchyScope == null
                ? null
                : new HierarchyScopeToHierarchyScopeTypeMapper().Map(materialSpecification.HierarchyScope);

	        var id = materialSpecification.ExternalId == null
		        ? null
		        : new IdentifierType
		        {
			        Value = materialSpecification.ExternalId
		        };

	        if (mapAsReference)
	        {
		        return new OpMaterialSpecificationType
		        {
			        HierarchyScope = hierarchyScope,
					ID = id
		        };
	        }

			var assemblyType = materialSpecification.AssemblyType == null
                ? null
                : new AssemblyTypeType
                {
                    Value = ((AssemblyType) materialSpecification.AssemblyType).ToString()
                };

            if (assemblyType != null && (bool) materialSpecification.AssemblyType?.HasOtherValue)
            {
                assemblyType.OtherValue = materialSpecification.AssemblyType.ToString();
            }

            var materialClassId = materialSpecification.MaterialClass == null
                ? null
                : new[]
                {
                    new MaterialClassIDType
                    {
                        Value = materialSpecification.MaterialClass.ExternalId
                    }
                };

            var materialUse = materialSpecification.MaterialUse == null
                ? null
                : new MaterialUseType
                {
                    Value = ((MaterialUse) materialSpecification.MaterialUse).ToString()
                };

            if (materialUse != null && (bool) materialSpecification.MaterialUse?.HasOtherValue)
            {
                materialUse.OtherValue = materialSpecification.MaterialUse.ToString();
            }

            var description = string.IsNullOrEmpty(materialSpecification.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialSpecification.Description
                    }
                };

            var materialDefinitionId = materialSpecification.MaterialDefinition == null
                ? null
                : new[]
                {
                    new MaterialDefinitionIDType
                    {
                        Value = materialSpecification.MaterialDefinition.ExternalId
                    }
                };

            var spatialLocation = materialSpecification.SpatialDefinition == null
                ? null
                : new SpatialDefinitionToGeospatialDesignationTypeMapper().Map(
                    materialSpecification.SpatialDefinition,
                    false);

            var materialSpecificationProperties = materialSpecification.Properties.MapToArray(
                new MaterialSpecificationPropertyToOpMaterialSpecificationPropertyTypeMapper<TProcessDefinition>(),
                false);

            var assemblyRelationship = materialSpecification.AssemblyRelationship == null
                ? null
                : new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper().Map(materialSpecification
		            .AssemblyRelationship.Value,
		            false);

            var quantities = materialSpecification.Quantity == null
				? null
				: materialSpecification.Quantity.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);

            return new OpMaterialSpecificationType
            {
                HierarchyScope = hierarchyScope,
                AssemblyType = assemblyType,
                MaterialClassID = materialClassId,
                MaterialUse = materialUse,
                Description = description,
                ID = id,
                MaterialDefinitionID = materialDefinitionId,
                SpatialLocation = spatialLocation,
                MaterialSpecificationProperty = materialSpecificationProperties,
                AssemblyRelationship = assemblyRelationship,
                Quantity = quantities,
            };
        }

        #endregion
    }
}