﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PersonnelClassPropertyToPersonnelClassPropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PersonnelClassProperty,
	///         PersonnelClassPropertyType}"
	///     </cref>
	/// </seealso>
	public class PersonnelClassPropertyToPersonnelClassPropertyTypeMapper :
        IMapper<BasicProperty<PersonnelClass>, PersonnelClassPropertyType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified personnel property.
		/// </summary>
		/// <param name="personnelClassProperty">The personnel property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>PersonnelClassPropertyType.</returns>
		public PersonnelClassPropertyType Map(BasicProperty<PersonnelClass> personnelClassProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(personnelClassProperty,
                nameof(personnelClassProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = personnelClassProperty.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = personnelClassProperty.ExternalId
                };

            var b2mmlDescription = personnelClassProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = personnelClassProperty.Description
                    }
                };

            var b2mmlValue = personnelClassProperty.Value == null
				? null
				: personnelClassProperty.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            var b2mmlProperty = personnelClassProperty.SubProperties.MapToArray(this, false);

            return new PersonnelClassPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                PersonnelClassProperty = b2mmlProperty
            };
        }

        #endregion
    }
}