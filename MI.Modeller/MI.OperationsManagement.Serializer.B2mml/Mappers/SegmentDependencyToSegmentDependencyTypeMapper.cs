﻿using System;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class SegmentDependencyToSegmentDependencyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.SegmentDependency, SegmentDependencyType}"
	///     </cref>
	/// </seealso>
	public class SegmentDependencyToSegmentDependencyTypeMapper<TProcessType> 
        : IMapper<SegmentDependency<TProcessType>, SegmentDependencyType>
        where TProcessType: class, IProcessDefinition<TProcessType>, IWithExternalId
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified segment dependency.
		/// </summary>
		/// <param name="segmentDependency">The segment dependency.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>SegmentDependencyType.</returns>
		public SegmentDependencyType Map(SegmentDependency<TProcessType> segmentDependency, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = new IdentifierType
            {
                Value = segmentDependency.ExternalId
            };

            var b2mmlDescription = string.IsNullOrEmpty(segmentDependency.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = segmentDependency.Description
                    }
                };

            // The XSD has a choice of a product segment, process segment or a segment id (a plain IdentifierType), map accordingly
            // There will only be one according to the XSD despite being coded as an array
            IdentifierType dependencyItem = null;
            if (segmentDependency.Process != null)
            {
                if (typeof(ProcessSegment).IsAssignableFrom(typeof(TProcessType)))
                {
                    dependencyItem = new ProcessSegmentIDType
                    {
                        Value = segmentDependency.Process.ExternalId
                    };
                }
                else
                {
                    dependencyItem = new IdentifierType
                    {
                        Value = segmentDependency.Process.ExternalId
                    };
                }
            }

            var b2mmlDependencyItems = dependencyItem == null
                ? null
                : new[]
                {
                    dependencyItem
                };

            var b2mmlDepedencyType = segmentDependency.DependencyType == null
                ? null
                : new DependencyTypeToDependencyTypeMapper().Map(segmentDependency.DependencyType.Value);

            var b2mmlTimingFactors = segmentDependency.TimingFactor?.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

            return new SegmentDependencyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Items = b2mmlDependencyItems,
                Dependency = b2mmlDepedencyType,
                TimingFactor = b2mmlTimingFactors
            };
        }

        #endregion
    }
}