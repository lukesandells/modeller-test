﻿using System;
using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.ResourceRelationshipNetworks;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class ResourceRelationshipTypeToRelationshipTypeTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceRelationshipType,
	///         RelationshipTypeType}"
	///     </cref>
	/// </seealso>
	public class ResourceRelationshipTypeToRelationshipTypeTypeMapper :
        IMapper<ExtensibleEnum<ResourceRelationshipType>, RelationshipTypeType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified data type.
		/// </summary>
		/// <param name="dataType">Type of the data.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>RelationshipTypeType.</returns>
		public RelationshipTypeType Map(ExtensibleEnum<ResourceRelationshipType> dataType, bool mapAsReference = false)
        {
	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			return new RelationshipTypeType
            {
                Value = ((ResourceRelationshipType) dataType).ToString(),
                OtherValue = dataType.HasOtherValue
                    ? dataType.ToString()
                    : null
            };
        }

        #endregion
    }
}