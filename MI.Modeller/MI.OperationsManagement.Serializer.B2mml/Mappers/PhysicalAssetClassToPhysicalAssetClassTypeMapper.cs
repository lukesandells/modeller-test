using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class PhysicalAssetClassToPhysicalAssetClassTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.PhysicalAssetClass, PhysicalAssetClassType}"</cref>
	/// </seealso>
	public class PhysicalAssetClassToPhysicalAssetClassTypeMapper : IMapper<PhysicalAssetClass, PhysicalAssetClassType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified physicalAsset class.
		/// </summary>
		/// <param name="physicalAssetClass">The physicalAsset class.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>PhysicalAssetClassType.</returns>
		public PhysicalAssetClassType Map(PhysicalAssetClass physicalAssetClass, bool mapAsReference)
        {
            var b2mmlId = physicalAssetClass.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = physicalAssetClass.ExternalId
                };

	        var b2mmlHierarchyScope = physicalAssetClass.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(physicalAssetClass.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new PhysicalAssetClassType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(physicalAssetClass.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = physicalAssetClass.Description
                    }
                };

            var b2mmlManufacturers = !physicalAssetClass.Manufacturers.Any()
                  ? null
                  : physicalAssetClass.Manufacturers.Select(
                          e => new NameType
                          {
                              Value = e
                          })
                      .ToArray();


            var b2mmlPhysicalAssetClassProperty =
                physicalAssetClass.Properties.MapToArray(
                    new PhysicalAssetClassPropertyToPhysicalAssetClassPropertyTypeMapper(),
                    false);

            return new PhysicalAssetClassType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                HierarchyScope = b2mmlHierarchyScope,
                PhysicalAssetClassProperty = b2mmlPhysicalAssetClassProperty,
                Manufacturer = b2mmlManufacturers
            };
        }

        #endregion
    }
}