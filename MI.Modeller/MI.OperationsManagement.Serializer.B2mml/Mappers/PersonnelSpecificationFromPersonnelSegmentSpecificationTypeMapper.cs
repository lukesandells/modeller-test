﻿//using System;
//using System.Linq;
//using MI.Framework.MapperExtensions;
//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.MapperExtensions;
//using MI.Modeller.B2mml.Mappers.Resources;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class B2mmlToPersonnelSpecificationMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{PersonnelSegmentSpecificationType,
//    ///         MI.Modeller.Domain.Client.PersonnelSpecification}"
//    ///     </cref>
//    /// </seealso>
//    /// <seealso cref="IMapper{TF,T}" />
//    public class PersonnelSpecificationFromPersonnelSegmentSpecificationTypeMapper :
//        IMapper<PersonnelSegmentSpecificationType, PersonnelSpecification>
//    {
//        #region  Fields

//        /// <summary>
//        ///     The master or segment identifier
//        /// </summary>
//        private readonly string _masterOrSegmentId;

//        /// <summary>
//        ///     The workspace
//        /// </summary>
//        private readonly IWorkspace _workspace;

//        #endregion

//        #region Constructors

//        /// <summary>
//        ///     Constructs a new PersonnelSpecificationFromPersonnelSegmentSpecificationTypeMapper
//        /// </summary>
//        /// <param name="workspace">The workspace to lookup objects in</param>
//        /// <param name="masterOrSegmentId">The master or segment identifier.</param>
//        public PersonnelSpecificationFromPersonnelSegmentSpecificationTypeMapper(IWorkspace workspace,
//            string masterOrSegmentId)
//        {
//            _masterOrSegmentId = ArgumentValidation.AssertNotNull(masterOrSegmentId,
//                nameof(masterOrSegmentId));

//            _workspace = ArgumentValidation.AssertNotNull(workspace,
//                nameof(workspace));
//        }

//        #endregion

//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified b2 MML personnel specification type.
//        /// </summary>
//        /// <param name="b2mmlPersonnelSpecificationType">Type of the b2 MML personnel specification.</param>
//        /// <returns>PersonnelSpecification.</returns>
//        public PersonnelSpecification Map(PersonnelSegmentSpecificationType b2mmlPersonnelSpecificationType)
//        {
//            ArgumentValidation.AssertNotNull(b2mmlPersonnelSpecificationType,
//                nameof(b2mmlPersonnelSpecificationType));

//            var personnelClassId = b2mmlPersonnelSpecificationType.PersonnelClassID?.Value;

//            var personId = b2mmlPersonnelSpecificationType.PersonID?.Value;

//            var description = !b2mmlPersonnelSpecificationType.Description.IsNullOrEmpty()
//                ? b2mmlPersonnelSpecificationType.Description.FirstOrDefault()
//                    ?.Value
//                : null;

//            var personnelUse = b2mmlPersonnelSpecificationType.PersonnelUse?.Value;

//            var quantities =
//                b2mmlPersonnelSpecificationType.Quantity.MapToSet(new QuantityFromQuantityValueTypeMapper());

//            // This needs to be added later. Missing in B2MML. A bug has been created
//            var geospatialDesignation = default(GeospatialDesignation);

//            var hierarchyScope = _workspace.FindById<HierarchyScope>(null,
//                b2mmlPersonnelSpecificationType.HierarchyScope.EquipmentID.Value);

//            if (hierarchyScope == null)
//            {
//                throw new Exception(string.Format(Messages.HierarchyScopeWithId0CannotBeFound,
//                    b2mmlPersonnelSpecificationType.HierarchyScope.EquipmentID));
//            }

//            var personnelSpecificationProperties =
//                b2mmlPersonnelSpecificationType.PersonnelSegmentSpecificationProperty.MapToSet(
//                    new PersonnelSpecificationPropertyFromPersonnelSegmentSpecificationPropertyTypeMapper());

//            var loadedSpecification = new PersonnelSpecification(_masterOrSegmentId,
//                personId,
//                personnelClassId,
//                description,
//                personnelUse,
//                quantities,
//                // ReSharper disable once ExpressionIsAlwaysNull
//                geospatialDesignation,
//                hierarchyScope,
//                personnelSpecificationProperties);

//            return loadedSpecification;
//        }

//        #endregion
//    }
//}