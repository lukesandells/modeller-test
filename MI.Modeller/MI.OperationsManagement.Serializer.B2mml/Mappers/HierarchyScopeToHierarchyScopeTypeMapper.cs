﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Converts Hierarchy Scopes between the domain model and B2mml
	/// </summary>
	public class HierarchyScopeToHierarchyScopeTypeMapper : IMapper<HierarchyScope, HierarchyScopeType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Creates a new domain model HierarchyScope based on a B2mml HierarchyScopeType
		/// </summary>
		/// <param name="hierarchyScope">the HierarchyScope to convert into the domain model</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false. 
		///  Use the Hierarchy Scope Exporter if you want to build the full tree</param>
		/// <returns></returns>
		public HierarchyScopeType Map(HierarchyScope hierarchyScope, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(hierarchyScope,
                nameof(hierarchyScope));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var equipmentId = hierarchyScope.ExternalId == null
                ? default(EquipmentIDType)
                : new EquipmentIDType
                {
                    Value = hierarchyScope.ExternalId
                };

            var equipmentElementLevel = new EquipmentLevelToEquipmentElementLevelTypeMapper().Map(hierarchyScope.EquipmentLevel);

            return new HierarchyScopeType
            {
                EquipmentID = equipmentId,
                EquipmentLevel = equipmentElementLevel
            };
        }
        
        #endregion
    }
}