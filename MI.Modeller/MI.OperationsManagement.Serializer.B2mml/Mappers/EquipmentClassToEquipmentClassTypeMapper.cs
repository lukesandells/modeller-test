using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class EquipmentClassToEquipmentClassTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.EquipmentClass, EquipmentClassType}"</cref>
	/// </seealso>
	public class EquipmentClassToEquipmentClassTypeMapper : IMapper<EquipmentClass, EquipmentClassType>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified equipment class.
		/// </summary>
		/// <param name="equipmentClass">The equipment class.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
		/// <returns>EquipmentClassType.</returns>
		public EquipmentClassType Map(EquipmentClass equipmentClass, bool mapAsReference)
        {
            var b2mmlId = equipmentClass.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = equipmentClass.ExternalId
                };

	        var b2mmlHierarchyScope = equipmentClass.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(equipmentClass.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new EquipmentClassType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(equipmentClass.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = equipmentClass.Description
                    }
                };

	        var b2mmlEquipment = !equipmentClass.Members.Any()
		        ? null
		        : equipmentClass.Members.Select(m => new EquipmentIDType
					{
						Value = m.ExternalId
					}).ToArray();

            var b2mmlEquipmentLevel = new EquipmentLevelToEquipmentElementLevelTypeMapper().Map(equipmentClass.EquipmentLevel);

            var b2mmlEquipmentClassProperty =
                equipmentClass.Properties.MapToArray(
                    new EquipmentClassPropertyToEquipmentClassPropertyTypeMapper(),
                    false);

            return new EquipmentClassType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                HierarchyScope = b2mmlHierarchyScope,
                EquipmentLevel = b2mmlEquipmentLevel,
                EquipmentClassProperty = b2mmlEquipmentClassProperty,
				EquipmentID = b2mmlEquipment
            };
        }

        #endregion
    }
}