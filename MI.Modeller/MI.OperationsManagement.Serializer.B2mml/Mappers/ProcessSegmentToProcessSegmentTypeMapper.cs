﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class ProcessSegmentToProcessSegmentTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IB2mmlMapper{MI.Modeller.Domain.Client.ProcessSegment, ProcessSegmentType}"
	///     </cref>
	/// </seealso>
	public class ProcessSegmentToProcessSegmentTypeMapper : IMapper<ProcessSegment, ProcessSegmentType>

    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified process segment.
		/// </summary>
		/// <param name="processSegment">The process segment.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated.</param>
	    /// <returns>ProcessSegmentType.</returns>
	    public ProcessSegmentType Map(ProcessSegment processSegment, bool mapAsReference)
        {
            ArgumentValidation.AssertNotNull(processSegment,
                nameof(processSegment));

            var b2mmlId = processSegment.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = processSegment.ExternalId
                };

	        var b2mmlHierarchyScope = processSegment.HierarchyScope == null
		        ? null
		        : new HierarchyScopeToHierarchyScopeTypeMapper().Map(processSegment.HierarchyScope);

	        if (mapAsReference)
	        {
		        return new ProcessSegmentType
		        {
			        HierarchyScope = b2mmlHierarchyScope,
					ID = b2mmlId
		        };
	        }

			var b2mmlDescription = string.IsNullOrEmpty(processSegment.Description)
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = processSegment.Description
                    }
                };

            var b2mmlOperationsType = processSegment.ProcessType == null
                ? null
                : new ProcessTypeToOperationsTypeTypeMapper().Map(processSegment.ProcessType.Value);

            var b2mmlPublishedDate = processSegment.PublishedDate == default(DateTime)
                ? null
                : new PublishedDateType
                {
                    Value = processSegment.PublishedDate
                };

            var b2mmlDuration = processSegment.Duration == default(TimeSpan)
                ? null
                : processSegment.Duration.ToString();

            // mapped arrays
            var b2mmlParameter = processSegment.Properties.MapToArray(new BasicPropertyToParameterTypeMapper<ProcessSegment>(), false);
            var b2mmlSegmentDependency =
                processSegment.SegmentDependencies.MapToArray(new SegmentDependencyToSegmentDependencyTypeMapper<ProcessSegment>(), false);

            return new ProcessSegmentType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                OperationsType = b2mmlOperationsType,
                HierarchyScope = b2mmlHierarchyScope,
                PublishedDate = b2mmlPublishedDate,
                Duration = b2mmlDuration,
                Parameter = b2mmlParameter,
                SegmentDependency = b2mmlSegmentDependency,
            };
        }

        #endregion
    }
}