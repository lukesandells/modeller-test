﻿//using MI.Framework;

//namespace MI.Modeller.B2mml.Mappers
//{
//    /// <summary>
//    ///     Class ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper.
//    /// </summary>
//    /// <seealso>
//    ///     <cref>
//    ///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceReferenceType,
//    ///         ResourceReferenceTypeType}"
//    ///     </cref>
//    /// </seealso>
//    public class ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper :
//        IMapper<ExtensibleEnum<Domain.Client.ResourceReferenceType>, ResourceReferenceTypeType>
//    {
//        #region  Interface Implementations

//        /// <summary>
//        ///     Maps the specified data type.
//        /// </summary>
//        /// <param name="dataType">Type of the data.</param>
//        /// <returns>ResourceReferenceTypeType.</returns>
//        public ResourceReferenceTypeType Map(ExtensibleEnum<Domain.Client.ResourceReferenceType> dataType)
//        {
//            return new ResourceReferenceTypeType
//            {
//                Value = ((Domain.Client.ResourceReferenceType) dataType).ToString(),
//                OtherValue = dataType.HasOtherValue
//                    ? dataType.ToString()
//                    : null
//            };
//        }

//        #endregion
//    }
//}