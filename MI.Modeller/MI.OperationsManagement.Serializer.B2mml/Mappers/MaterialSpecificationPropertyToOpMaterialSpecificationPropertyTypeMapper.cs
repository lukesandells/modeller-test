﻿using System;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class MaterialSpecificationPropertyToB2mmlOpMaterialSpecificationPropertyTypeMapper.
	/// </summary>
	/// <seealso
	///     cref="IMapper{TF,T}" />
	public class MaterialSpecificationPropertyToOpMaterialSpecificationPropertyTypeMapper<TProcessDefinition> :
        IMapper<QuantitySpecificResourceTransactionProperty<MaterialSpecification<TProcessDefinition>>, OpMaterialSpecificationPropertyType>
        where TProcessDefinition: class, IProcessDefinition<TProcessDefinition>
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified personnel specification property.
		/// </summary>
		/// <param name="materialSpecification">The personnel specification property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>OpMaterialSpecificationPropertyType.</returns>
		public OpMaterialSpecificationPropertyType Map(QuantitySpecificResourceTransactionProperty<MaterialSpecification<TProcessDefinition>> materialSpecification, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(materialSpecification,
                nameof(materialSpecification));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = materialSpecification.ExternalId == null
                ? null
                : new IdentifierType
                {
                    Value = materialSpecification.ExternalId
                };

            var b2mmlDescription = materialSpecification.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = materialSpecification.Description
                    }
                };

            // collection properties in the domain are never null, even if set to null in ctor
            // they are defaulted to an empty list. So no need to check empty
            var b2mmlValue = materialSpecification.Value?.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);
            var b2mmlQuantity = materialSpecification.ResourceQuantity?.Serialise().Elements.MapToArray(new QuantityElementToQuantityValueTypeMapper(), false);
            var b2mmlProperty = materialSpecification.SubProperties?.MapToArray(this, false);

            return new OpMaterialSpecificationPropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
                Quantity = b2mmlQuantity,
                MaterialSpecificationProperty = b2mmlProperty
            };
        }

        #endregion
    }
}