﻿using System;
using System.Linq;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.ObjectProperties;
using MI.OperationsManagement.Serializer.B2mml.Mappers.Extensions;

namespace MI.OperationsManagement.Serializer.B2mml.Mappers
{
	/// <summary>
	///     Class BasicPropertyToResourcePropertyTypeMapper.
	/// </summary>
	/// <seealso>
	///     <cref>
	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceProperty,
	///         ResourcePropertyType}"
	///     </cref>
	/// </seealso>
	public class BasicPropertyToResourcePropertyTypeMapper<TOfObject> 
        : IMapper<BasicProperty<TOfObject>, ResourcePropertyType>
        where TOfObject : class, IWithBasicProperties<TOfObject>, IWithExternalId
    {
		#region  Interface Implementations

		/// <summary>
		///     Maps the specified resource property.
		/// </summary>
		/// <param name="resourceProperty">The resource property.</param>
		/// <param name="mapAsReference">If mapped as reference only the HierarchyScope and ID are populated. Should always be false</param>
		/// <returns>ResourcePropertyType.</returns>
		public ResourcePropertyType Map(BasicProperty<TOfObject> resourceProperty, bool mapAsReference = false)
        {
            ArgumentValidation.AssertNotNull(resourceProperty,
                nameof(resourceProperty));

	        if (mapAsReference)
	        {
		        throw new ArgumentException(ExportMessages.NeverMapByReference);
	        }

			var b2mmlId = new IdentifierType
            {
                Value = resourceProperty.ExternalId
            };

            var b2mmlDescription = resourceProperty.Description == null
                ? null
                : new[]
                {
                    new DescriptionType
                    {
                        Value = resourceProperty.Description
                    }
                };

            var b2mmlValue = resourceProperty.Value == null
				? null
				: resourceProperty.Value.Serialise().Elements.MapToArray(new ValueElementToValueTypeMapper(), false);

	        var b2mmlChildren = !resourceProperty.SubProperties.Any()
		        ? null
		        : resourceProperty.SubProperties.MapToArray(this, false);


			return new ResourcePropertyType
            {
                ID = b2mmlId,
                Description = b2mmlDescription,
                Value = b2mmlValue,
				ResourceProperty = b2mmlChildren
            };
        }

        #endregion
    }
}