﻿//using MI.Framework.Validation;
//using MI.Modeller.B2mml.Mappers.Extensions;
//using MI.Modeller.Domain.Client;

//namespace MI.Modeller.B2mml.Mappers
//{
//	/// <summary>
//	///     Class ResourceReferenceToResourceReferenceTypeMapper.
//	/// </summary>
//	/// <seealso>
//	///     <cref>
//	///         "MI.Modeller.Client.B2mmlMappers.IMapper{MI.Modeller.Domain.Client.ResourceReference,
//	///         ResourceReferenceType}"
//	///     </cref>
//	/// </seealso>
//	public class ResourceReferenceToResourceReferenceTypeMapper : IMapper<ResourceReference, ResourceReferenceType>
//	{
//		#region  Interface Implementations

//		/// <summary>
//		///     Maps the specified resource reference.
//		/// </summary>
//		/// <param name="resourceReference">The resource reference.</param>
//		/// <returns>ResourceReferenceType.</returns>
//		public ResourceReferenceType Map(ResourceReference resourceReference)
//		{
//			ArgumentValidation.AssertNotNull(resourceReference,
//				nameof(resourceReference));

//			var b2mmlId = resourceReference.Id == null
//				? null
//				: new IdentifierType
//				{
//					Value = resourceReference.Id
//				};

//			var b2mmlResourceId = resourceReference.ResourceId == null
//				? null
//				: new ResourceIDType
//				{
//					Value = resourceReference.ResourceId
//				};

//			var b2mmlResourceType = resourceReference.ResourceType == null
//				? null
//				: new ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper().Map(resourceReference.ResourceType);

//			var b2mmlProperty =
//				resourceReference.ResourceProperties.MapToArray(new BasicPropertyToResourcePropertyTypeMapper());

//			return new ResourceReferenceType
//			{
//				ID = b2mmlId,
//				ResourceID = b2mmlResourceId,
//				ResourceType = b2mmlResourceType,
//				ResourceProperty = b2mmlProperty
//			};
//		}

//		#endregion
//	}
//}