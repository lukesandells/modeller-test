﻿namespace MI.Modeller.Client.MasterShapes
{
    /// <summary>
    /// Class HierarchyScopeMasterShape.
    /// </summary>
    /// <seealso><cref>"MI.Modeller.Client.MasterShapes.MasterShape"</cref></seealso>
    internal class HierarchyScopeMasterShape : MasterShape
    {
        #region Static Fields and Constants

        /// <summary>
        /// The equipment level cell name
        /// </summary>
        public const string C_EQUIPMENT_LEVEL_CELL_NAME = "User.EquipmentLevel";

        /// <summary>
        /// The c equipment sub level cell name
        /// </summary>
        public const string C_SUB_LEVEL_CELL_NAME = "User.SubLevel";

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="HierarchyScopeMasterShape"/> class.
        /// </summary>
        public HierarchyScopeMasterShape() : base("Hierarchy Scope") {}

        #endregion
    }
}