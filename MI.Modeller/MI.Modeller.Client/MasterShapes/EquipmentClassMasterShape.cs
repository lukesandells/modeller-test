﻿namespace MI.Modeller.Client.MasterShapes
{
    public class EquipmentClassMasterShape : MasterShape
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The equipment level cell name
        /// </summary>
        public const string C_LEVEL_CELL_NAME = "User.Level";

        /// <summary>
        /// The c equipment level cell name
        /// </summary>
        public const string C_EQUIPMENT_LEVEL_CELL_NAME = "User.EquipmentLevel";

        /// <summary>
        /// The c equipment sub level cell name
        /// </summary>
        public const string C_SUB_LEVEL_CELL_NAME = "User.SubLevel";
    

        #endregion

        #region Constructors

        public EquipmentClassMasterShape() : base("Equipment Class") {}

        #endregion
    }
}