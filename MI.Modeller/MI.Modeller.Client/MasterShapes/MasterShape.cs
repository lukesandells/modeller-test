﻿using System.Diagnostics.CodeAnalysis;

namespace MI.Modeller.Client.MasterShapes
{
    /// <summary>
    ///     Represents a Master Shape
    /// </summary>
    public class MasterShape
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The equipment class master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape EquipmentClassMasterShape = new EquipmentClassMasterShape();

        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape EquipmentMasterShape = new MasterShape("Equipment");

        /// <summary>
        ///     The Hierarchy Scope master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape HierarchyScopeMasterShape = new HierarchyScopeMasterShape();

        /// <summary>
        ///     The material class master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape MaterialClassMasterShape = new MasterShape("Material Class");

        /// <summary>
        ///     The material definition master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape MaterialDefinitionMasterShape = new MasterShape("Material Definition");

        /// <summary>
        ///     The material specification master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape MaterialSpecificationMasterShape = new MasterShape("Material Specification");

        /// <summary>
        ///     The Operations Definition master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape OperationsDefinitionMasterShape = new MasterShape("Operations Definition");

        /// <summary>
        ///     The operations segment master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape OperationsSegmentMasterShape = new MasterShape("Operations Segment");

	    /// <summary>
	    ///     The personnel class master shape
	    /// </summary>
	    [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
	    public static readonly MasterShape PhysicalAsssetClassMasterShape = new MasterShape("Physical Asset Class");

		/// <summary>
		///     The personnel class master shape
		/// </summary>
		[SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape PersonnelClassMasterShape = new PersonnelClassMasterShape();

        /// <summary>
        ///     The process segment master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape ProcessSegmentMasterShape = new MasterShape("Process Segment");

        /// <summary>
        ///     The specification master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape SpecificationMasterShape = new MasterShape("Specification Connector");

        /// <summary>
        ///     The Work Master master shape
        /// </summary>
        [SuppressMessage("Microsoft.Security", "CA2104:DoNotDeclareReadOnlyMutableReferenceTypes")]
        public static readonly MasterShape WorkMasterMasterShape = new WorkMasterMasterShape();

        #endregion

        #region  Fields

        /// <summary>
        ///     The name
        /// </summary>
        public readonly string Name;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="MasterShape" /> class.
        /// </summary>
        /// <param name="name">The name.</param>
        protected MasterShape(string name)
        {
            Name = name;
        }

        #endregion

        #region Members

        /// <summary>
        ///     Returns the master shape name
        /// </summary>
        /// <returns>The name of the master shape</returns>
        public override string ToString()
        {
            return Name;
        }

        #endregion
    }
}