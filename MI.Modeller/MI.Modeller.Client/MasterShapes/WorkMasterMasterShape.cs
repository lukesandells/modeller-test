﻿using System.Collections.Generic;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;

namespace MI.Modeller.Client.MasterShapes
{
    /// <summary>
    /// Class WorkMasterMasterShape.
    /// </summary>
    /// <seealso><cref>"MI.Modeller.Client.MasterShapes.MasterShape"</cref></seealso>
    public class WorkMasterMasterShape : MasterShape
    {

        /// <summary>
        /// Gets the work type dictionary.
        /// </summary>
        /// <value>The work type dictionary.</value>
        public static IDictionary<ProcessType, string> WorkTypeDictionary => new Dictionary<ProcessType, string>
        {
            {ProcessType.Production, "P"},
            {ProcessType.Quality, "Q"},
            {ProcessType.Maintenance, "M"},
            {ProcessType.Inventory, "I"}
        };
        /// <summary>
        /// The c level cell name
        /// </summary>
        public const string C_LEVEL_CELL_NAME = "User.Level";
        /// <summary>
        /// The c work type cell name
        /// </summary>
        public const string C_WORK_TYPE_CELL_NAME = "User.ProcessType";
        /// <summary>
        /// The c work definition type cell name
        /// </summary>
        public const string C_WORK_DEFINITION_TYPE_CELL_NAME = "Prop.WorkDefinitionType";
        /// <summary>
        /// The c duration cell name
        /// </summary>
        public const string C_DURATION_CELL_NAME = "Prop.Duration";
        /// <summary>
        /// The c operations definition identifier cell name
        /// </summary>
        public const string C_OPERATIONS_DEFINITION_ID_CELL_NAME = "Prop.OperationsDefinitionID";
        /// <summary>
        /// The c operations segment identifier cell name
        /// </summary>
        public const string C_OPERATIONS_SEGMENT_ID_CELL_NAME = "Prop.OperationsSegmentID";
        /// <summary>
        /// The c workflow specification identifier cell name
        /// </summary>
        public const string C_WORKFLOW_SPECIFICATION_ID_CELL_NAME = "Prop.WorkflowSpecificationID";


        /// <summary>
        /// Initializes a new instance of the <see cref="WorkMasterMasterShape"/> class.
        /// </summary>
        public WorkMasterMasterShape() : base("Work Master")
        {
        }
    }
}