﻿namespace MI.Modeller.Client.MasterShapes
{
    public class EquipmentMasterShape : MasterShape
    {
        #region Static Fields and Constants

        /// <summary>
        ///     The equipment level cell name
        /// </summary>
        public const string C_EQUIPMENT_LEVEL_CELL_NAME = "User.Level";


        /// <summary>
        /// The c equipment sub level cell name
        /// </summary>
        public const string C_SUB_LEVEL_CELL_NAME = "User.WorkCenterType";

        #endregion

        #region Constructors

        public EquipmentMasterShape() : base("Equipment") { }

        #endregion
    }
}