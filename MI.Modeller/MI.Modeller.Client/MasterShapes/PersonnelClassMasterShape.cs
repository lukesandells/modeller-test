﻿namespace MI.Modeller.Client.MasterShapes
{
    public class PersonnelClassMasterShape : MasterShape
    {
        #region Constructors

        public PersonnelClassMasterShape() : base("Personnel Class") {}

        #endregion
    }
}