﻿using System;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Domain;

namespace MI.Modeller.Client.ShapeMappers
{
	/// <summary>
	///     Factory to return the appropriate mapper for a shape
	/// </summary>
	public interface IShapeFactory
	{
		IModellerShape CreateNewShapeOffPage(ModelElement modelElement, IModellerPresenter modellerPresenter);
		NodeType GetDomainTypeFromShape(IModellerShape shape);

		/// <summary>
		///  Indicates whether a shape is currently being updated
		/// </summary>
		bool ShapeUpdateInProgress { get; }

		/// <summary>
		///     Gets the occurence from shape.
		/// </summary>
		/// <param name="shape">The shape.</param>
		ObjectOccurrence GetOccurenceForShape(IModellerShape shape);
		bool IsModellerShape(IModellerShape shape);

		/// <summary>
		///     Gets the shape from a model element. Will also map the occurence's ModelElement
		/// </summary>
		/// <param name="occurrence">The occurrence for this shape.</param>
		/// <param name="shape">The shape.</param>
		IModellerShape MapOccurrenceToShape(Occurrence occurrence, IModellerShape shape);

		/// <summary>
		/// Maps a given model element to a shape
		/// </summary>
		/// <param name="target"></param>
		/// <param name="shape"></param>
		IModellerShape MapModelElementToShape(ModelElement target, IModellerShape shape);
	}
}