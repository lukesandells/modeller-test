﻿namespace MI.Modeller.Client.ShapeMappers
{
	/// <summary>
	///     Factory for dealing with connections
	/// </summary>
	public interface IConnectorFactory
	{
		#region Other Methods

		/// <summary>
		///     Creates a conenctor on the page for the given connector
		/// </summary>
		/// <param name="connector">The connector</param>
		void CreateConnector(IModellerPage page, IModellerConnector connector);

		/// <summary>
		///     Creates or updates the domain connector the connection corresponds to
		/// </summary>
		/// <param name="connenctor">The connector</param>
		void CreateOrUpdateOccurrence(IModellerConnector connenctor);

		/// <summary>
		///     Deteles the occurrence that a connector corresponds to
		/// </summary>
		/// <param name="connector">The connector</param>
		void DeleteOccurrence(IModellerConnector connector);

		#endregion
	}
}