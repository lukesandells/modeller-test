﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MI.Modeller.Domain.Client;
using MI.Modeller.Client.Visio2016.Resources;
using Microsoft.Office.Interop.Visio;
using MI.Framework.Validation;

namespace MI.Modeller.Client.Visio2016.ShapeMappers
{
    class MaterialSpecificationFromShapeMapper : IShapeToDomainMapper<MaterialSpecification>
    {

        public static string MaterialUseCellName = "User.MaterialUse";
        public static readonly string DescriptionCellName = "Prop.Description";
        public static readonly string OtherMaterialUseCellName = "Prop.OtherMaterialUse";
        public static readonly string AssemblyTypeCellName = "Prop.AssemblyType";
        public static readonly string AssemblyRelationshipCellName = "Prop.AssemblyRelationship";
        public static readonly string QuantityCellName = "Prop.Quantity";

        private ShapeData ShapeData;

        /// <summary>
        /// The material usetype dictionary
        /// </summary>
        private static readonly Dictionary<string, MaterialUseTypeValue> MaterialUseTypeDictionary = new Dictionary
            <string, MaterialUseTypeValue>()
            {
                {"0", MaterialUseTypeValue.Consumed},
                {"1", MaterialUseTypeValue.Produced},
                {"2", MaterialUseTypeValue.Consumable},
                {"3", MaterialUseTypeValue.ReplacedAsset},
                {"4", MaterialUseTypeValue.ReplacementAsset},
                {"5", MaterialUseTypeValue.Sample},
                {"6", MaterialUseTypeValue.ReturnedSample},
                {"7", MaterialUseTypeValue.Carrier},
                {"8", MaterialUseTypeValue.ReturnedCarrier},
                {"9", MaterialUseTypeValue.ByProductProduced},
                {"10", MaterialUseTypeValue.CoProductProduced},
                {"11", MaterialUseTypeValue.YieldProduced},
                {"12", MaterialUseTypeValue.Inventoried},
                {"13", MaterialUseTypeValue.Other}
            };

        private static readonly Dictionary<string, MaterialAssemblyTypeValue> MaterialAssemblyTypeDictionary = new Dictionary
            <string, MaterialAssemblyTypeValue>()
            {
                {"Physical", MaterialAssemblyTypeValue.Physical},
                {"Logical", MaterialAssemblyTypeValue.Logical},
                {"Other", MaterialAssemblyTypeValue.Other}
            };

        private static readonly Dictionary<string, AssemblyRelationshipTypeValue> AssemblyRelationshipTypeDictionary = new Dictionary
            <string, AssemblyRelationshipTypeValue>()
            {
                {"Permanent", AssemblyRelationshipTypeValue.Permanent},
                {"Transient", AssemblyRelationshipTypeValue.Transient},
                {"Other", AssemblyRelationshipTypeValue.Other}
            };


        public MaterialSpecification Map(Shape shape, Workspace workspace)
        {

            ArgumentValidation.AssertNotNull(shape,
                nameof(shape));
            shape.AssertIsOfMaster(MasterShape.MaterialSpecificationMasterShape);

            var materialUse = MapMaterialUse(shape);

            // id made up of Work Master\SegmentID<delimiter> MaterialId/Class<delimiter> Use
            // TODO - make sure this covers all possibilities
            var fromShape = shape.GetConnectedShapes()
                .FirstOrDefault(s => s.IsOfMaster(MasterShape.WorkMasterMasterShape));
            var toShape = shape.GetConnectedShapes()
                .FirstOrDefault(s => s.IsOfMaster(MasterShape.MaterialDefinitionMasterShape));

            // get the Shape Data
            ShapeData = new ShapeData(shape);


            // get all the registered values for the shape
        //    DefinedShapeProperties = shape.ShapeData(new List<string>
        //{
        //    DescriptionCellName,
        //    OtherMaterialUseCellName,
        //    AssemblyTypeCellName,
        //    AssemblyRelationshipCellName,
        //    QuantityCellName
        //}).ToDictionary(e => e.Key, e=>e.Value);

            // get all the other properties

            //	Must be connected between a Material Definition and an Operations/ Process Segment, Work Master or another Material Definition
            // TODO - validation on the TO shape and FROM Shape

            //string id =  Work Master\SegmentID<delimiter> MaterialId/Class<delimiter> Use
            //TODO - Need a central spot for delimiter
            string id = $"{toShape?.Text}.{fromShape?.Text}.{materialUse}";

            // check that the from shape is a material definition
            var materialDefinitionId = fromShape.IsOfMaster(MasterShape.MaterialDefinitionMasterShape)
                ? fromShape?.Text
                : null;

            var materialClassId = fromShape.IsOfMaster(MasterShape.MaterialClassMasterShape)
                ? fromShape?.Text
                : null;

            var materialAssemblyType = MapMaterialAssemblyType(shape);
            var assemblyRelationshipType = MapAssemblyRelationshipType(shape);

            // the hierarchy scope must be mapped because we need the ID and the equipment level
            var hierarchyScopeShape = shape.FirstLevelParent(MasterShape.HierarchyScopeMasterShape);
            var hierarchyScope = (hierarchyScopeShape == null)
                ? null
                : (HierarchyScope) MapperFactory.GetDomainMapperForShape(hierarchyScopeShape)
                    .Map(hierarchyScopeShape, workspace);


            //string materialClassId = default(string),
            //string materialDefinitionId = default(string),
            //MaterialUseType materialUse = default(MaterialUseType),
            //GeospatialDesignation geospatialDesignation = null,
            //MaterialAssemblyType assemblyType = null,
            //AssemblyRelationshipType assemblyRelationship = null,
            //HierarchyScope HierarchyScope = default(HierarchyScope),
            //string description = default(string),
            //ICollection< Quantity > quantities = null,
            //ICollection<MaterialSpecificationProperty> materialSpecificationProperties = null,
            //ICollection< MaterialSpecification > children = null)
            return new MaterialSpecification(id,
                materialClassId,
                materialDefinitionId,
                materialUse,
                null,
                materialAssemblyType,
                assemblyRelationshipType,
                hierarchyScope,
                ShapeData[DescriptionCellName].Value,
                ShapeUtilities.MapQuantityStringToQuantities(ShapeData[QuantityCellName].Value)
                    .ToList());
            //TODO - need to get the additional proprties
<<<<<<< HEAD
           
        }

        /// <summary>
        /// Determines wheather a Material Specification should be mapped for a given diagram
        /// </summary>
        /// <param name="diagram">The diagram being mapped</param>
        /// <param name="shape">The shape being mapped</param>
        /// <returns></returns>
        public bool MapForDiagram(Diagram diagram,
            Shape shape)
        {
            shape.AssertIsOfMaster(MasterShape.MaterialSpecificationMasterShape);
            // Specifications are not mapped on their own, always called from the Work Master or Segment being mapped

            // TODO - this must be removed and set to False
            return true;  // Testing To be deleted 
            return false;
=======
            
            // Hierarchy Scopes are maintained in the Hierarchy Editor. This is only used as an element of object e.g. a Work Master
            //var level = new EquipmentLevelType(shape.Cells[HierarchyScopeMasterShape.LevelCellName].Formula);
            //return new HierarchyScope(shape.Text, level);


            //Shape materialSpecificationShape = shape.FirstLevelParent(MasterShape.HierarchyScopeMasterShape);
            //var HierarchyScope = materialSpecificationShape == null
            //    ? null
            //    : new HierarchyScopeFromShapeMapper().Map(materialSpecificationShape);
            //// Place holder to get base functionality going
            //return new MaterialSpecification(shape.Text, null, HierarchyScope);
>>>>>>> 87401b142968f677c64f9add06d3432ba07f7c95
        }

        /// <summary>
        /// Maps the material use.
        /// </summary>
        /// <param name="shape">The shape.</param>
        /// <returns>MI.Modeller.Domain.Client.MaterialUseType.</returns>
        private Domain.Client.MaterialUseType MapMaterialUse(Shape shape)
        {
            var otherValue = ShapeData.Where(e => e.Key == OtherMaterialUseCellName)
                .Select(e => e.Value.Value)
                .FirstOrDefault();

            // get the material Use
            return
                new Domain.Client.MaterialUseType(
                    MaterialUseTypeDictionary[shape.CellsU[MaterialUseCellName].Formula],
                    otherValue);
        }

        private MaterialAssemblyType MapMaterialAssemblyType(Shape shape)
        {
            // get the Assembly Type
            var selectedValue = ShapeData.Where(e => e.Key == AssemblyTypeCellName)
                .Select(e => e.Value.Value)
                .FirstOrDefault();

            if (MaterialAssemblyTypeDictionary.Any(e => e.Key == selectedValue))
            {
                return new MaterialAssemblyType(MaterialAssemblyTypeDictionary.Where(e => e.Key == selectedValue)
                    .Select(e => e.Value)
                    .FirstOrDefault());
            }

            return new MaterialAssemblyType(MaterialAssemblyTypeValue.Other,
                selectedValue);
        }


        private Domain.Client.AssemblyRelationshipType MapAssemblyRelationshipType(Shape shape)
        {
            // get the Assembly Type
            var selectedValue = ShapeData.Where(e => e.Key == AssemblyTypeCellName)
                .Select(e => e.Value.Value)
                .FirstOrDefault();

            if (AssemblyRelationshipTypeDictionary.Any(e => e.Key == selectedValue))
            {
                return
                    new Domain.Client.AssemblyRelationshipType(
                        AssemblyRelationshipTypeDictionary.Where(e => e.Key == selectedValue)
                            .Select(e => e.Value)
                            .FirstOrDefault());
            }

            return new Domain.Client.AssemblyRelationshipType(AssemblyRelationshipTypeValue.Other,
                selectedValue);
        }

     
    }
}
