﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MI.Modeller.Client.ShapeMappers
{
    internal class ShapeUtilities
    {
        /// for shape data that may hold any property, use the appropriate function which will return Quantity or Value objects and has mandatory Type
        /// Note this is to be used for shape data that specifically hold Quantities ONLY
        /// Maps A quantity string to quantities.
        /// <summary>
        ///     Incoming string is of format
        ///     Quantity(1,"ea") or
        ///     (1,"ea") or
        ///     (1,ea) or
        ///     1,ea
        ///     or combinations of above. Multiple quantities may be chained by ';'. E.g.
        ///     (1,"ea");(400,"bottles");20,"kg";5,"things"
        ///     Quantity objects are always returned with Data Type = Quantity
        /// </summary>
        /// <param name="quantityString">The quantity string.</param>
        /// <returns>IEnumerable&lt;Quantity&gt;.</returns>
        public static IEnumerable<Quantity> MapStringToQuantities(string quantityString)
        {
            ArgumentValidation.AssertNotNull(quantityString,
                nameof(quantityString));

            // pattern below is for Quantity(1,"ea"); repeating
            // where initial string "Quantity" is optional
            // open and closing braces are optional
            // Regex below will be quite flexible
            var pattern = @"([q|quantity])\(?(?<Quantity>(\d+([.|,]\d+)?)),""?(?<Unit>([^\(\);""]*))""?\)?;?";
            // get all matches and create a quantity domain object for ech one
            foreach (Match m in Regex.Matches(quantityString,
                pattern,
                RegexOptions.IgnoreCase))
            {
                yield return new Quantity(null,
                    m.Groups["Quantity"].Value,
                    DataType.Quantity,
                    new UnitOfMeasure(m.Groups["Unit"].Value));
            }
        }
    }
}