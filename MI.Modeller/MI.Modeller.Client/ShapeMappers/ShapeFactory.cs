﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Client.MasterShapes;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Client.ShapeMappers
{
	/// <summary>
	///     Factory to return the appropriate mapper for a shape
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.ShapeMappers.IShapeFactory"</cref>
	/// </seealso>
	public abstract class ShapeFactory : IShapeFactory
	{
        #region Static Fields and Constants

        /// <summary>
        ///     The type to master shapes
        /// </summary>
        private static readonly Dictionary<MasterShape, Guid> _typeToMasterShapes = new Dictionary
            <MasterShape, Guid>
            { // Go by ID rather than NodeType.For in order to get this to work for unit testing
                {MasterShape.HierarchyScopeMasterShape, NodeTypeId.HierarchyScope},
                {MasterShape.EquipmentClassMasterShape,  NodeTypeId.EquipmentClass},
                {MasterShape.EquipmentMasterShape,  NodeTypeId.Equipment },
                {MasterShape.WorkMasterMasterShape, NodeTypeId.WorkMaster},
				{MasterShape.PersonnelClassMasterShape, NodeTypeId.PersonnelClass},
				{MasterShape.PhysicalAsssetClassMasterShape, NodeTypeId.PhysicalAssetClass},
				{MasterShape.MaterialDefinitionMasterShape, NodeTypeId.MaterialDefinition},
				{MasterShape.MaterialClassMasterShape, NodeTypeId.MaterialClass},
				{MasterShape.OperationsDefinitionMasterShape, NodeTypeId.OperationsDefinition},
				{MasterShape.OperationsSegmentMasterShape, NodeTypeId.OperationsSegment},
				{MasterShape.ProcessSegmentMasterShape, NodeTypeId.ProcessSegment}
			};

		#endregion

		#region  Interface Implementations

		/// <summary>
		///  Indicates whether a shape is currently being updated
		/// </summary>
		public bool ShapeUpdateInProgress { get; private set; }

		/// <summary>
		///     Gets the occurence from shape.
		/// </summary>
		/// <param name="shape">The shape.</param>
		public ObjectOccurrence GetOccurenceForShape(IModellerShape shape)
        {
	        ObjectOccurrence occurrence;
	        using (var scope = new PersistenceScope(TransactionOption.Required))
	        {
				occurrence = scope
					.Query<ObjectOccurrence>()
					.SingleOrDefault(o => o.Id == shape.OccurrenceId);
		        if (occurrence != null)
		        {
			        scope.Attach(occurrence.ModelElement);
		        }
		        scope.Done();
	        }
	        return occurrence;
        }

		/// <summary>
		///     Gets the shape from a model element. WIll also map the occurence's ModelElement
		/// </summary>
		/// <param name="occurrence">The shape's occurrece.</param>
		/// <param name="shape">The shape.</param>
		public IModellerShape MapOccurrenceToShape(Occurrence occurrence,
            IModellerShape shape)
		{
			ShapeUpdateInProgress = true;
			shape.OccurrenceId = occurrence.Id;
			shape.ModelElementId = occurrence.ModelElement.Id;
			return MapModelElementToShape(occurrence.ModelElement, shape);
		}

		/// <summary>
		/// Maps a ModelElement to a shape
		/// </summary>
		/// <param name="target">The target modelElement</param>
		/// <param name="shape"></param>
		/// <returns></returns>
		public IModellerShape MapModelElementToShape(ModelElement target, IModellerShape shape)
		{
			shape.IdText = target.DisplayId;
			shape.DescriptionText = target .Description;
			// TODO this doesnt feel right
			if (target.DesignObject is Equipment)
			{
				var classes = ((Equipment)target.DesignObject).EquipmentClasses.Select(c => c.ExternalId);
				if (classes.Count() > 1)
				{
					shape.ClassText = ((Equipment)target.DesignObject).EquipmentClasses.Select(c => c.ExternalId).Aggregate((i, j) => i + "," + j);
				}
				else
				{
					shape.ClassText = ((Equipment)target.DesignObject).EquipmentClasses.FirstOrDefault()?.ExternalId;
				}
			}
			else if (target.DesignObject is MaterialDefinition)
			{
				var classes = ((MaterialDefinition)target.DesignObject).MaterialClasses.Select(c => c.ExternalId);
				if (classes.Count() > 1)
				{
					shape.ClassText = ((MaterialDefinition)target.DesignObject).MaterialClasses.Select(c => c.ExternalId).Aggregate((i, j) => i + "," + j);
				}
				else
				{
					shape.ClassText = ((MaterialDefinition)target.DesignObject).MaterialClasses.FirstOrDefault()?.ExternalId;
				}
			}
			else
			{
				shape.ClassText = "";
			}
			if (NodeType.WithId(target.TypeId)
				.Configuration.Properties.ContainsKey(NodeTypeMember.Property.EquipmentLevel))
			{
				shape.EquipmentLevelText = ExtensibleEnum.GetEnumValueDisplayName(target.Properties[NodeTypeMember.Property.EquipmentLevel].Value);
				shape.EquipmentLevel = target.Properties[NodeTypeMember.Property.EquipmentLevel].Value.ToString();
			}
			if (NodeType.WithId(target.TypeId)
				.Configuration.Properties.ContainsKey(NodeTypeMember.Property.ProcessType))
			{
				shape.ProcessType = target.Properties[NodeTypeMember.Property.ProcessType].Value?.ToString();
			}
			ShapeUpdateInProgress = false;
			return shape;
		}

		/// <summary>
        ///     Determines whether [is modeller shape] [the specified shape].
        /// </summary>
        /// <param name="shape">The shape.</param>
        /// <returns>System.Boolean.</returns>
        public bool IsModellerShape(IModellerShape shape)
        {
            return _typeToMasterShapes.Keys.Select(k => k.Name)
                .Contains(shape.MasterName);
        }

        /// <summary>
        ///     Gets the domain type from shape.
        /// </summary>
        /// <param name="shape">The shape.</param>
        /// <returns>System.Type.</returns>
        public NodeType GetDomainTypeFromShape(IModellerShape shape)
        {
            return _typeToMasterShapes.Keys.FirstOrDefault(k => k.Name == shape.MasterName) != null
                ? NodeType.WithId(_typeToMasterShapes[_typeToMasterShapes.Keys.First(k => k.Name == shape.MasterName)])
                :  null;
        }

		#endregion

		#region Members

		/// <summary>
		///     Creates the Shape
		/// </summary>
		/// <param name="modelElement">The model element to create the shape for</param>
		/// <param name="modellerPresenter">The modeller presenter</param>
		/// <returns>MI.Modeller.Client.IModellerShape.</returns>
		public abstract IModellerShape CreateNewShapeOffPage(ModelElement modelElement,
            IModellerPresenter modellerPresenter);

        #endregion
    }
}