﻿using System.Collections.Generic;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Client.Resources
{
    public static class EquipmentLevelLabelLookup
    {
        /// <summary>
        ///     Lookup between Equipment Level enum and their ordering index/label
        /// </summary>
        private static readonly IDictionary<EquipmentLevel, string> _equipmentLevelLabels = new Dictionary
            <EquipmentLevel, string>
            {
                {EquipmentLevel.Enterprise, "Enterprise"},
                {EquipmentLevel.Site, "Site"},
                {EquipmentLevel.Area, "Area"},
                {EquipmentLevel.WorkCenter, "Work Center"},
                {EquipmentLevel.ProcessCell, "Process Cell"},
                {EquipmentLevel.ProductionLine, "Production Line"},
                {EquipmentLevel.ProductionUnit, "Production Unit"},
                {EquipmentLevel.StorageZone, "Storage Zone"},
                {EquipmentLevel.WorkUnit, "Work Unit"},
                {EquipmentLevel.Unit, "Unit"},
                {EquipmentLevel.WorkCell, "Work Cell"},
                {EquipmentLevel.StorageUnit, "Storgage Unit"},
                {EquipmentLevel.ControlModule, "Control Module"},
                {EquipmentLevel.EquipmentModule, "Equipment Module"},
                {EquipmentLevel.Other, "Other"}
            };

        /// <summary>
        /// Lookup the label for an equipment Level
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static string Lookup(EquipmentLevel type)
        {
            return _equipmentLevelLabels[type];
        }
    }
}
