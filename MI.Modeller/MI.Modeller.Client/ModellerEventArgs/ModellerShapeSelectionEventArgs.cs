﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Client.ModellerEventArgs
{
    /// <summary>
    /// Class ModellerShapeSelectionEventArgs.
    /// </summary>
    /// <seealso><cref>"System.EventArgs"</cref></seealso>
    public class ModellerShapeSelectionEventArgs : System.EventArgs
    {
        #region  Fields

        /// <summary>
        /// Gets the argument.
        /// </summary>
        /// <value>The argument.</value>
        public string Argument { get; }

        /// <summary>
        /// Gets the selected modeller shapes.
        /// </summary>
        /// <value>The selected modeller shapes.</value>
        public IEnumerable<IModellerShape> SelectedModellerShapes { get; }


        #endregion

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellerShapeSelectionEventArgs"/> class.
        /// </summary>
        /// <param name="argument">The argument.</param>
        /// <param name="selectedModellerShapes">The selected modeller shapes.</param>
        public ModellerShapeSelectionEventArgs( string argument, IEnumerable<IModellerShape> selectedModellerShapes )
        {
            Argument = argument;

            SelectedModellerShapes = selectedModellerShapes?.Select(e => e) ?? new HashSet<IModellerShape>();

        }
    }
}