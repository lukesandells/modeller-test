﻿using System;
using MI.Framework.Validation;

namespace MI.Modeller.Client.ModellerEventArgs
{
    public class ModellerDocumentEventArgs: EventArgs
    {
        #region  Fields

        public IModellerDocument Document { get; }

        #endregion

        #region Constructors

        public ModellerDocumentEventArgs(IModellerDocument document)
        {
            ArgumentValidation.AssertNotNull(document,
                nameof(document));
            Document = document;
        }

        #endregion
    }
}