﻿using System;

namespace MI.Modeller.Client.ModellerEventArgs
{
	/// <summary>
	///     Class ModellerShapeEventArgs.
	/// </summary>
	/// <seealso>
	///     <cref>"System.EventArgs"</cref>
	/// </seealso>
	public class ModellerShapeEventArgs : EventArgs
	{
		#region  Fields

		/// <summary>
		///     The event argument
		/// </summary>
		/// <value>The argument.</value>
		public string Argument { get; set; }

		/// <summary>
		///     The modeller shape this event is for
		/// </summary>
		/// <value>The modeller shape.</value>
		public IModellerShape ModellerShape { get; set; }

		/// <summary>
		///     The modeller page this event is for
		/// </summary>
		public IModellerPage ModellerPage { get; set; }

		/// <summary>
		///     The modeller document this event is for
		/// </summary>
		public IModellerDocument ModellerDocument { get; set; }

		#endregion
	}
}