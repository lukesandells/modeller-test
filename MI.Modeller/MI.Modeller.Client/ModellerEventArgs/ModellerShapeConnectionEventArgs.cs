﻿using System;
using MI.Framework.Validation;

namespace MI.Modeller.Client.ModellerEventArgs
{
    /// <summary>
    ///     Class ModellerShapeEventArgs.
    /// </summary>
    /// <seealso>
    ///     <cref>"System.EventArgs"</cref>
    /// </seealso>
    public class ModellerShapeConnectionEventArgs : EventArgs
    {
        #region  Fields

        /// <summary>
        ///     Gets or sets the modeller shape.
        /// </summary>
        /// <value>The modeller shape.</value>
        public IModellerConnector Connector { get; }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ModellerShapeConnectionEventArgs"/> class.
        /// </summary>
        public ModellerShapeConnectionEventArgs(
	        IModellerConnector connector)
        {
	        Connector = connector;
        }

        #endregion
    }
}