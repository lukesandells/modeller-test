﻿namespace MI.Modeller.Client.ModellerEventArgs
{
	/// <summary>
	/// Arguments for page related events
	/// </summary>
	public class ModellerPageEventArgs
	{
		#region  Fields/Properties

		/// <summary>
		/// The page this event is for
		/// </summary>
		public IModellerPage Page { get; }

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs a new ModellerPageEventArgs
		/// </summary>
		/// <param name="page">The page this event is for</param>
		public ModellerPageEventArgs(IModellerPage page)
		{
			Page = page;
		}

		#endregion
	}
}