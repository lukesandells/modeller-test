﻿using System;
using System.Collections;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.ModellerEventArgs;
using MI.Modeller.Domain;

namespace MI.Modeller.Client
{
    /// <summary>
    ///     Interface IModellerDocument
    /// </summary>
    public interface IModellerDocument
    {
        #region  Fields

        /// <summary>
        ///     Gets the document identifier.
        /// </summary>
        /// <value>The document identifier.</value>
        int DocumentId { get; }

        /// <summary>
        ///     The file path to this document
        /// </summary>
        string FilePath { get; }

        /// <summary>
        ///     The internal ID of this document
        /// </summary>
        Guid ModelInternalId { get; set; }

        /// <summary>
        ///     Gets the type of the model.
        /// </summary>
        /// <value>The type of the model.</value>
        ModelType ModelType { get; }

        /// <summary>
        ///     Gets the selection.
        /// </summary>
        /// <value>The selection.</value>
        IEnumerable<IModellerShape> Selection { get; }

        /// <summary>
        ///     Gets the shapes.
        /// </summary>
        /// <value>The shapes.</value>
        IEnumerable<IModellerShape> Shapes { get; }

        /// <summary>
        ///     Gets the title.
        /// </summary>
        /// <value>The title.</value>
        string Title { get; set; }

        /// <summary>
        /// Gets the base document as it's native type
        /// </summary>
        object BaseDocument { get; }

        /// <summary>
        /// Indicates whetather or not this document is a modeller document
        /// </summary>
        bool IsModellerDocument { get; }

		/// <summary>
		/// The pages within this document
		/// </summary>
	    IEnumerable<IModellerPage> Pages { get; }

		/// <summary>
		/// The connectors present in this document
		/// </summary>
	    IEnumerable<IModellerConnector> Connectors { get; }

	    #endregion

        #region Members

        /// <summary>
        ///     Deselects all.
        /// </summary>
        void DeselectAll();

        /// <summary>
        ///     Occurs when [hover over error shape].
        /// </summary>
        event EventHandler<ModellerShapeEventArgs> HoverOverErrorShape;

        /// <summary>
        ///     Selects all.
        /// </summary>
        void SelectAll();

        #endregion

		/// <summary>
		/// Adds a page to the document
		/// </summary>
		/// <param name="page">The page to add</param>
	    void AddPage(Page page);
    }
}