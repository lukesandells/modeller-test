﻿using System.Collections.Generic;

namespace MI.Modeller.Client
{
	/// <summary>
	/// A selection of shapes within modellers
	/// </summary>
	public interface IModellerSelection: IEnumerable<IModellerShape>
	{
		/// <summary>
		/// Returns the selection as a dataobject
		/// </summary>
		/// <returns></returns>
		IModellerDataObject AsDataObject();

		/// <summary>
		/// Arrange the shapes within this selection
		/// </summary>
		void AutoArrange();
	}
}
