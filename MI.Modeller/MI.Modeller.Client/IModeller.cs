﻿using System;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.ModellerEventArgs;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain;

namespace MI.Modeller.Client
{
	/// <summary>
	///     Interface IModeller
	/// </summary>
	public interface IModeller
	{
		#region Other Methods

		/// <summary>
		///     Creates and queues an undo unit for a given transaction
		/// </summary>
		/// <param name="nodeTransaction">The node transaction to create an undo unit for</param>
		/// <param name="description">A description of the event that this undo unit is for</param>
		void CreateAndQueueUndoUnit(NodeTransaction nodeTransaction, string description);

		/// <summary>
		///     Save the given document in the location specified by the document's path
		/// </summary>
		/// <param name="document"></param>
		void SaveDocument(IModellerDocument document);

		#endregion

		#region  Fields

		/// <summary>
		/// The current open documents
		/// </summary>
		IEnumerable<IModellerDocument> OpenDocuments { get; }

		/// <summary>
		///     The currently active document
		/// </summary>
		IModellerDocument ActiveDocument { get; }

		/// <summary>
		///     Factory for creating new shapes on the page
		/// </summary>
		IShapeFactory ShapeFactory { get; }

		/// <summary>
		/// The currently active page
		/// </summary>
		IModellerPage ActivePage { get; }

		#endregion

		#region Members

		/// <summary>
		///     Occurs when [connection added].
		/// </summary>
		event EventHandler<ModellerShapeConnectionEventArgs> ConnectionAdded;

		/// <summary>
		///     Occurs when [connection removed].
		/// </summary>
		event EventHandler<ModellerShapeConnectionEventArgs> ConnectionRemoved;

		/// <summary>
		///     Creates a new ObjectBrowserView
		/// </summary>
		/// <param name="modelElementTreeFilter">The filter to apply to the tree</param>
		/// <param name="parent">The form this control is placed in</param>
		/// <param name="allowMultipleSelections">Whether or not multiple nodes may be selected</param>
		/// <param name="rootNodes"></param>
		/// <returns></returns>
		IModellerTreeView CreateObjectBrowserView(ModelElementTreeFilter modelElementTreeFilter, ISimpleForm parent, bool allowMultipleSelections, IEnumerable<Guid> rootNodes);

		/// <summary>
		///     Creates the form with the embedded ModellerTreeView
		/// </summary>
		/// <param name="rootNodes"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		ITreeViewForm CreateModellerTreeViewForm(IEnumerable<Guid> rootNodes, ModelElementTreeFilter filter);

		/// <summary>
		///     Create a new simple form
		/// </summary>
		/// <param name="title">The forms title</param>
		/// <returns></returns>
		ISimpleForm CreateSimpleForm(string title);

		/// <summary>
		///     Occurs when a new document is created
		/// </summary>
		event EventHandler<ModellerDocumentEventArgs> DocumentCreated;

		/// <summary>
		///     Occurs when [document opened].
		/// </summary>
		event EventHandler<ModellerDocumentEventArgs> DocumentOpened;

		/// <summary>
		///     Occurs when [document closed].
		/// </summary>
		event EventHandler<ModellerDocumentEventArgs> DocumentClosed;

		/// <summary>
		///     Occurs when [document saved].
		/// </summary>
		event EventHandler<ModellerDocumentEventArgs> DocumentSaved;

		/// <summary>
		///     Occurs when a new page is created
		/// </summary>
		event EventHandler<ModellerPageEventArgs> PageCreated;

		/// <summary>
		///     Occurs when a new page is changed
		/// </summary>
		event EventHandler<ModellerPageEventArgs> PageChanged;

		/// <summary>
		///     Occurs when a new page is deleted
		/// </summary>
		event EventHandler<ModellerPageEventArgs> PageDeleted;

		/// <summary>
		///     Occurs when [selection changed].
		/// </summary>
		event EventHandler<ModellerShapeSelectionEventArgs> SelectionChanged;

		/// <summary>
		///     Occurs when [shape added].
		/// </summary>
		event EventHandler<ModellerShapeEventArgs> ShapeAdded;

		/// <summary>
		///     Occurs when [shape deleted].
		/// </summary>
		event EventHandler<ModellerShapeEventArgs> ShapeDeleted;

		/// <summary>
		///     Occurs when [shape event triggered].
		/// </summary>
		event EventHandler<ModellerShapeEventArgs> ShapeEventTriggered;

		/// <summary>
		///     Occurs when [shut down].
		/// </summary>
		event EventHandler ShutDown;

		#endregion

		/// <summary>
		/// Connects two occurrence
		/// </summary>
		/// <param name="from">The occurrence to connect from</param>
		/// <param name="to">The occurrence to conenct to</param>
		/// <param name="direction">The direction to draw the connection</param>
		void Connect(Occurrence from, Occurrence to, ConnectionDirection direction);

		/// <summary>
		/// Select the given shapes and return it as a selection
		/// </summary>
		/// <param name="shapes">The shapes to select</param>
		/// <returns></returns>
		IModellerSelection SelectShapes(IEnumerable<IModellerShape> shapes);

		/// <summary>
		/// Deselect the given shapes and return it as a selection
		/// </summary>
		/// <param name="shapes">The shapes to deselect</param>
		/// <returns></returns>
		IModellerSelection DeselectShapes(IEnumerable<IModellerShape> shapes);

		/// <summary>
		/// Forces the given document to be closed without saving
		/// </summary>
		/// <param name="document">The document to close</param>
		void ForceClose(IModellerDocument document);

		/// <summary>
		/// Open the given model
		/// </summary>
		/// <param name="model">The model to open</param>
		void OpenModel(Model model);

		/// <summary>
		/// Displays a message for the user
		/// </summary>
		/// <param name="message">The message to display</param>
		void DisplayMessage(string message);

		/// <summary>
		/// Selects a given model elements
		/// </summary>
		/// <param name="modelElements">The model elements to select</param>
		void SelectObjects(IEnumerable<ModelElement> modelElements);
	}
}