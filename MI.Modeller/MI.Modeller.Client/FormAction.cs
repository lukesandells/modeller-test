﻿namespace MI.Modeller.Client
{
    /// <summary>
    ///     Enum FormAction
    /// </summary>
    public enum FormAction
    {
        /// <summary>
        ///     The create
        /// </summary>
        Create,

        /// <summary>
        ///     The update
        /// </summary>
        Update,

        /// <summary>
        ///     The delete
        /// </summary>
        Delete,

        /// <summary>
        ///     The display
        /// </summary>
        Display,
        /// <summary>
        /// The load
        /// </summary>
        Load
    }
}