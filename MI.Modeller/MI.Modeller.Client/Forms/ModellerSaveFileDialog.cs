﻿using System.Windows.Forms;

namespace MI.Modeller.Client.Forms
{
	/// <summary>
	/// Wrapper for the Save File Dialog
	/// </summary>
	public class ModellerSaveFileDialog: ISaveFileDialog
	{
		/// <summary>
		/// The save file dialog which will be displayed
		/// </summary>
		private readonly SaveFileDialog _dialog = new SaveFileDialog();

		/// <summary>
		/// The extension being saved
		/// </summary>
		public string Extension
		{
			set => _dialog.DefaultExt = value;
		}

		/// <summary>
		/// The file name selected by the user
		/// </summary>
		public string FileName
		{
			get => _dialog.FileName;
			set => _dialog.FileName = value;
		}

		/// <summary>
		/// Filter for file names
		/// </summary>
		public string Filter
		{
			get => _dialog.Filter;
			set => _dialog.Filter = value;
		}

		/// <summary>
		/// Shows the dialog and returns the result
		/// </summary>
		/// <returns></returns>
		public ModellerDialogResult ShowDialog()
		{
			if (_dialog.ShowDialog() == DialogResult.OK)
			{
				return ModellerDialogResult.OK;
			}
			return ModellerDialogResult.Cancel;
		}
	}
}
