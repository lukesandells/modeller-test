﻿namespace MI.Modeller.Client.Forms
{
    /// <summary>
    ///     Modeller enumerator for dialog results
    /// </summary>
    public enum ModellerDialogResult
    {
        OK,
        Cancel
    }
}