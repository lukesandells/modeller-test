﻿using System;
using System.ComponentModel;
using MI.Modeller.Client.UserControls;

namespace MI.Modeller.Client.Forms
{
    /// <summary>
    ///     Interface ISimpleFormEvents
    /// </summary>
    public interface ISimpleForm
    {
        #region  Fields

        /// <summary>
        ///     Gets or sets the title.
        /// </summary>
        /// <value>The title.</value>
        string Title { get; set; }

        #endregion

        #region Members

        /// <summary>
        ///     Occurs when [ok click].
        /// </summary>
        event EventHandler<CancelEventArgs> OkClicked;

        /// <summary>
        ///     The tree view control to be used
        /// </summary>
        /// <param name="control">The tree view control to be used</param>
        void SetUserControl(IModellerTreeView control);

        /// <summary>
        ///     Shows the dialog.
        /// </summary>
        /// <returns>DialogResult.</returns>
        ModellerDialogResult ShowDialog();

        #endregion
    }
}