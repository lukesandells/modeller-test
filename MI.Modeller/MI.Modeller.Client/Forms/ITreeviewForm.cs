﻿using System;

namespace MI.Modeller.Client.Forms
{
	public interface ITreeViewForm
    {
		#region  Fields

		/// <summary>
		/// The id of the selected node
		/// </summary>
	    Guid SelectedNodeId { get; }

	    /// <summary>
	    /// The name for the new model
	    /// </summary>
	    string NewModelName { get; }

		#endregion

		#region Members

		/// <summary>
		/// Shows the form as a dialog and returns the result
		/// </summary>
		/// <returns></returns>
		ModellerDialogResult ShowDialog();

        #endregion
    }
}