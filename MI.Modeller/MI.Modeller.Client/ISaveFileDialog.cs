﻿using MI.Modeller.Client.Forms;

namespace MI.Modeller.Client {
	/// <summary>
	/// Interface for the save file dialog
	/// </summary>
	public interface ISaveFileDialog
	{
		/// <summary>
		/// Shows the dialog and returns the dialog result
		/// </summary>
		/// <returns></returns>
		ModellerDialogResult ShowDialog();

		/// <summary>
		/// Sets the extension to use
		/// </summary>
		string Extension { set; }

		/// <summary>
		/// The file path that has been selected
		/// </summary>
		string FileName { get; set; }

		/// <summary>
		/// Filter for the file names
		/// </summary>
		string Filter { get; set; }
	}
}