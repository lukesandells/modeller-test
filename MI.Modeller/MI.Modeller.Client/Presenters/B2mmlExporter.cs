﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Client.Forms;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Exporters;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using MI.Modeller.Client.UserControls;

// ReSharper disable PossibleMultipleEnumeration

namespace MI.Modeller.Client.Presenters
{
	// ReSharper disable once InconsistentNaming
	public class B2mmlExporter
	{
		private readonly ISaveFileDialog _saveFileDialog;

		/// <summary>
		/// The serialisation helper to create the XML
		/// </summary>
		private readonly ISerializationHelper _serializationHelper = new XmlSerializationHelper();

		/// <summary>
		/// Hierarchy scope mapper. Needed to populate HierarchyScope on the information types
		/// </summary>
		private static readonly IMapper<HierarchyScope, HierarchyScopeType> _hierarchyScopeMapper = new HierarchyScopeToHierarchyScopeTypeMapper();

		private const string C_DATETIME_FORMAT = "o";

		public B2mmlExporter (ISaveFileDialog saveFileDialog)
		{
			_saveFileDialog = saveFileDialog;
		}

		/// <summary>
		/// Creates an export file of the given serialised elements
		/// </summary>
		/// <param name="filePath">The file path for the zip file</param>
		/// <param name="serialisedElements">A dictionary of path to the serialised element at that path</param>
		public void CreateExportFile(string filePath, IEnumerable<KeyValuePair<string, string>> serialisedElements)
		{
			// Create the zip file stream
			using (Stream stream = File.Create(filePath))
			{
				// And create the archive
				using (var zipArchive = new ZipArchive(stream, ZipArchiveMode.Create, false, null))
				{
					// We then create an entry for each element
					foreach (var element in serialisedElements)
					{
						// Use the key as the entry path
						var entry = zipArchive.CreateEntry(element.Key);
						// And write the value to the entry
						using (var writer = new StreamWriter(entry.Open(), new UTF8Encoding()))
						{
							writer.Write(element.Value);
						}
					}
				}
			}
		}

		/// <summary>
		/// Exports the selected nodes
		/// </summary>
		public void Export(IEnumerable<TargetFolderId> selectedFolderIds, IEnumerable<Guid> selectedNodeIds)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var selectedNodes = scope.Query<DefinitionNode>()
					.Where(f => selectedFolderIds.Select(fp => fp.ParentNodeId).Contains(f.Id))
					.ToArray()
					.Union(scope.Query<BrowserNode>().Where(n => selectedNodeIds.Contains(n.Id)))
					.Distinct()
					.Where(n => n.Is<DefinitionNode>())
					.Select(n => n.Cast<DefinitionNode>());
				_saveFileDialog.Extension = ".zip";
				_saveFileDialog.Filter = "Zip (*.zip)|*.zip";
				if (_saveFileDialog.ShowDialog() == ModellerDialogResult.OK)
				{
					// We will create an export for each hierarchy scope of the top level selected nodes and for the selected types within there
					var nodesToExport = selectedNodes.WhereTopLevelAndNotInverseLinks()
						.GroupBy(n => HierarchyScope.Of((DesignObject) n.TargetObject))
						.ToDictionary(g2 => g2.Key, g2 => g2.ToLookup(n => n.Type));

					// The scope of what needs to be exported is all desendants of what is selected
					var exportScope = selectedNodes.SelectMany(n => n.Descendants).Where(d => d.Is<ModelElement>()).Select(n => n.Cast<ModelElement>());

					// Do the actual export for each of the nodes which are to be exported
					IEnumerable<KeyValuePair<string, string>> serialisedElements = nodesToExport.SelectMany(g => ExportHierarchyScope(g.Key, g.Value, exportScope));

					// And create the export file
					CreateExportFile(_saveFileDialog.FileName, serialisedElements.Where(e => !string.IsNullOrEmpty(e.Value)));
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Handles the export of a given Hierarchy Scope
		/// </summary>
		/// <param name="hierarchyScopeToExport">The Hierarchy Scope to export</param>
		/// <param name="lookup">A lookup of the nodes for the given node types to export</param>
		/// <param name="exportScope">The scope of what is to be exported</param>
		/// <param name="basePath">The base path for the files that will be created. Start with empty string and then gets called recursively for contained Hierarchy Scopes</param>
		/// <returns></returns>
		private IEnumerable<KeyValuePair<string, string>> ExportHierarchyScope(HierarchyScope hierarchyScopeToExport, ILookup<NodeType, DefinitionNode> lookup, IEnumerable<ModelElement> exportScope, string basePath = null)
		{
			foreach (var grouping in lookup)
			{
				// Handle the export of this grouping grouping
				yield return new KeyValuePair<string, string>(
					basePath + hierarchyScopeToExport.ExternalId + "-" + grouping.Key.DisplayName + ".xml",
					Export(hierarchyScopeToExport, grouping.Key, grouping.ToArray(), exportScope));
				// And if it's a hierarchy scope then we also need to export it's children
				if (grouping.Key == NodeType.For(typeof(HierarchyScope)))
				{
					foreach (var childScopeToExport in grouping)
					{
						// If there are no other equipment defined then we need to explicity trigger the export of this Hierarchy Scope's defining equipment
						if (childScopeToExport.PrimaryFolder.Nodes.All(child => child.Type != NodeType.For(typeof(Equipment))))
						{
							yield return new KeyValuePair<string, string>(
								basePath + hierarchyScopeToExport.ExternalId + "-" + NodeType.For(typeof(Equipment)) + ".xml",
								Export(hierarchyScopeToExport, NodeType.For(typeof(Equipment)), grouping.ToArray(), exportScope));
						}
						// As above. Need to get the descendants where they are top level and group them by hierarchy scope and a lookup of nodes by their type
						var childNodesToExport = childScopeToExport.Descendants
							.Where(d => d.IsDefinitionNode && d.TargetObject is DesignObject)
							.Cast<DefinitionNode>()
							.WhereTopLevelAndNotInverseLinks()
							.GroupBy(n => HierarchyScope.Of((DesignObject)n.TargetObject))
							.ToDictionary(g2 => g2.Key, g2 => g2.ToLookup(n => n.Type));

						// And call this recursively using this hierarchy scope as the base path
						foreach (var keyValuePair in childNodesToExport
							.SelectMany(g => ExportHierarchyScope(g.Key, g.Value, exportScope, 
							// Child nodes in this scope are picked up here and will be exported at the same level. Otherwise append this scope's ID
							basePath + (g.Key == hierarchyScopeToExport ? "" : g.Key.ExternalId + "\\"))))
						{
							yield return keyValuePair;
						}
					}
				}
			}
		}

		/// <summary>
		/// Handles the export of a single node type for the nodes within it
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported.</param>
		/// <param name="nodeType">The node type that is being exported. Used to determine which kind of response will be generated</param>
		/// <param name="nodes">The nodes to export</param>
		/// <param name="scope">The scope of what is to be exported. Used by individual exporters to determine how the design object would be mapped</param>
		/// <returns></returns>
		private string Export(HierarchyScope hierarchyScope, NodeType nodeType, IEnumerable<DefinitionNode> nodes, IEnumerable<ModelElement> scope)
		{
			// Hierarchy Scope
			if (nodeType == NodeType.For(typeof(HierarchyScope)))
			{
				return ExportHierarchyScopeType(nodes.Cast<ModelElement>().Single());
			}
			// Resources
			if (nodeType == NodeType.For(typeof(Equipment)))
			{
				return ExportEquipment(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(EquipmentClass)))
			{
				return ExportEquipmentClass(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(MaterialDefinition)))
			{
				return ExportMaterialDefinition(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(MaterialClass)))
			{
				return ExportMaterialClass(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(PersonnelClass)))
			{
				return ExportPersonnelClass(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(PhysicalAssetClass)))
			{
				return ExportPhysicalAssetClass(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			// Processes
			if (nodeType == NodeType.For(typeof(WorkMaster)))
			{
				return ExportWorkMaster(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(ProcessSegment)))
			{
				return ExportProcessSegment(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			if (nodeType == NodeType.For(typeof(OperationsDefinition)))
			{
				return ExportOperationsDefinition(hierarchyScope, nodes.Cast<ModelElement>(), scope);
			}
			throw new NotImplementedException();
		}

		private string ExportHierarchyScopeType(ModelElement hierarchyScopeNode)
		{
			var xmlType = new HierarchyScopeInformationType
			{
				HierarchyScope = new HierarchyScopeExporter().Export(hierarchyScopeNode, new[] { hierarchyScopeNode }),
				ID = new IdentifierType
				{
					Value = "HierarchyScopeExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an equipment node into a serialised B2MML EquipmentInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="equipmentNodes">The equipment nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportEquipment(HierarchyScope hierarchyScope, IEnumerable<ModelElement> equipmentNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new EquipmentExporter();
			var xmlType = new EquipmentInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				Equipment = equipmentNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "EquipmentExport@" +DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an equipment class node into a serialised B2MML EquipmentInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="equipmentNodes">The equipment class nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportEquipmentClass(HierarchyScope hierarchyScope, IEnumerable<ModelElement> equipmentNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new EquipmentClassExporter();
			var xmlType = new EquipmentInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				EquipmentClass = equipmentNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "EquipmentClassExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an material class node into a serialised B2MML MaterialInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="materialNodes">The material class nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportMaterialDefinition(HierarchyScope hierarchyScope, IEnumerable<ModelElement> materialNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new MaterialDefinitionExporter();
			var xmlType = new MaterialInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				MaterialDefinition = materialNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "MaterialDefinitionExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an material class node into a serialised B2MML MaterialInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="materialNodes">The material class nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportMaterialClass(HierarchyScope hierarchyScope, IEnumerable<ModelElement> materialNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new MaterialClassExporter();
			var xmlType = new MaterialInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				MaterialClass = materialNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "MaterialClassExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an personnel class node into a serialised B2MML PersonnelInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="personnelNodes">The personnel class nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportPersonnelClass(HierarchyScope hierarchyScope, IEnumerable<ModelElement> personnelNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new PersonnelClassExporter();
			var xmlType = new PersonnelInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				PersonnelClass = personnelNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "PersonnelClassExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an physicalAsset class node into a serialised B2MML PhysicalAssetInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="physicalAssetNodes">The physicalAsset class nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportPhysicalAssetClass(HierarchyScope hierarchyScope, IEnumerable<ModelElement> physicalAssetNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new PhysicalAssetClassExporter();
			var xmlType = new PhysicalAssetInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				PhysicalAssetClass = physicalAssetNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "PhysicalAssetClassExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an workMaster node into a serialised B2MML WorkMasterInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="workMasterNodes">The workMaster nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportWorkMaster(HierarchyScope hierarchyScope, IEnumerable<ModelElement> workMasterNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new WorkMasterExporter();
			var xmlType = new WorkDefinitionInformationType
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				WorkMaster = workMasterNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "WorkMasterExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an processSegment node into a serialised B2MML ProcessSegmentInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="processSegmentNodes">The processSegment nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportProcessSegment(HierarchyScope hierarchyScope, IEnumerable<ModelElement> processSegmentNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new ProcessSegmentExporter();
			var xmlType = new ProcessSegmentInformationType()
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				ProcessSegment = processSegmentNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "ProcessSegmentExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

		/// <summary>
		/// Exports an operationsDefinition node into a serialised B2MML OperationsDefinitionInformationType element
		/// </summary>
		/// <param name="hierarchyScope">The hierarchy scope being exported</param>
		/// <param name="operationsDefinitionNodes">The operationsDefinition nodes to export</param>
		/// <param name="scope">The entire scope of what is to be exported</param>
		private string ExportOperationsDefinition(HierarchyScope hierarchyScope, IEnumerable<ModelElement> operationsDefinitionNodes, IEnumerable<ModelElement> scope)
		{
			var exporter = new OperationsDefinitionExporter();
			var xmlType = new OperationsDefinitionInformationType()
			{
				HierarchyScope = _hierarchyScopeMapper.Map(hierarchyScope, false),
				OperationsDefinition = operationsDefinitionNodes.WhereTopLevelAndNotInverseLinks().Select(n => exporter.Export(n, scope)).ToArray(),
				ID = new IdentifierType
				{
					Value = "OperationsDefinitionExport@" + DateTime.UtcNow.ToString(C_DATETIME_FORMAT)
				},
				PublishedDate = new PublishedDateType
				{
					Value = DateTime.UtcNow
				}
			};
			return _serializationHelper.Serialize(xmlType);
		}

	}
}
