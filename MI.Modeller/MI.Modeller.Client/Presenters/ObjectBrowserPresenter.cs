﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Windows.Forms;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	///     Presenter for the ModellerTreeView
	/// </summary>
	public class ObjectBrowserPresenter : NodeActivityListener, IObjectBrowserPresenter
	{
		#region Constructors

		/// <summary>
		///     Creates a new ObjectBrowserPresenter
		/// </summary>
		/// <param name="view">The view that this presenter is handling</param>
		/// <param name="modellerPresenter">The modeller presetner</param>
		public ObjectBrowserPresenter(IModellerTreeView view, IModellerPresenter modellerPresenter)
		{
			_view = ArgumentValidation.AssertNotNull(view, nameof(view));
			_modellerPresenter = ArgumentValidation.AssertNotNull(modellerPresenter, nameof(modellerPresenter));
			NodeActivityLog.AddListener<NodeActivityListener>(this);
		}

		#endregion

		#region  Interface Implementations

		/// <summary>
		///     Handle the selected node changed event
		/// </summary>
		public void HandleSelectedNodeChanged(object sender, NodeSelectionChangedEventArguments eventArgs)
		{
			// We need to surpress the node changed event when the presenter is initiating it as part of navigation
			if (!_supressSelectedNodeChanged)
			{
				_forwardsSelectedFoldersAndNodes.Clear();
				_backwardSelectedFoldersAndNodes.Add((eventArgs.NewSelectedFolderIds, eventArgs.NewSelectedNodeIds));
			}
		}

		/// <summary>
		///     Navigate back in the list of previously selected nodes
		/// </summary>
		// We're not enumerating the list, if we .ToList it then we're creating a new object that isn't in the list of nodes
		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		public void NavigateBack()
		{
			(var foldersToMove, var nodesToMove) = _backwardSelectedFoldersAndNodes.LastOrDefault();
			_forwardsSelectedFoldersAndNodes.Add((foldersToMove, nodesToMove));
			_backwardSelectedFoldersAndNodes.Remove((foldersToMove, nodesToMove));
			(var foldersToNavigateTo, var nodesToNavigateTo) = _backwardSelectedFoldersAndNodes.LastOrDefault();
			_supressSelectedNodeChanged = true;
			_view.SelectNodes(foldersToNavigateTo, nodesToNavigateTo);
			_supressSelectedNodeChanged = false;
		}

		/// <summary>
		///     Navigate forward in the list of previously selected nodes
		/// </summary>
		// We're not enumerating the list, if we .ToList it then we're creating a new object that isn't in the list of nodes
		[SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
		public void NavigateForward()
		{
			(var foldersToNavigateTo, var nodesToNavigateTo) = _forwardsSelectedFoldersAndNodes.LastOrDefault();
			_forwardsSelectedFoldersAndNodes.Remove((foldersToNavigateTo, nodesToNavigateTo));
			_backwardSelectedFoldersAndNodes.Add((foldersToNavigateTo, nodesToNavigateTo));
			_supressSelectedNodeChanged = true;
			_view.SelectNodes(foldersToNavigateTo, nodesToNavigateTo);
			_supressSelectedNodeChanged = false;
		}

		/// <summary>
		///     Gets the allowable right click actions for the nodes currently selected in the view
		/// </summary>
		/// <returns></returns>
		public IEnumerable<ContextAction> GetPermissibleOperations()
		{
			var selectedFolderIds = _view.SelectedFolderIds.ToArray();
			var selectedNodeIds = _view.SelectedNodeIds.ToArray();

			// Single folder selected - can add item
			if (selectedFolderIds.Length == 1 && !selectedNodeIds.Any())
			{
				var folderActions = ConstructFolderActions(selectedFolderIds.Single());
				return folderActions.ChildActions.Any() 
					? new[] {folderActions} 
					: new ContextAction[] { };
			}
			// Nodes selected, get actions that can be done to all
			if (!selectedFolderIds.Any() && selectedNodeIds.Any())
			{
				return ConstructBrowserNodeActions();
			}
			return new List<ContextAction>();
		}

		/// <summary>
		/// Updates the display ID of a definition node to the given new value
		/// </summary>
		/// <param name="nodeId"></param>
		/// <param name="newId"></param>
		/// <returns></returns>
		public string UpdateDisplayId(Guid nodeId, string newId)
		{
			string message;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var node = scope.FindById<DefinitionNode>(nodeId);
				node.Properties[NodeTypeMember.Property.DisplayId].ValidateInput(newId, out message);
				scope.Done();
			}
			return message;
		}

		/// <summary>
		///     Execute the given operation
		/// </summary>
		/// <param name="action">The action to perform</param>
		/// <param name="param">The parameters to execute the operation with</param>
		public void ExecuteOperation(OperationType action, object[] param = null)
		{
			var selectedFolderIds = _view.SelectedFolderIds.ToArray();
			var selectedNodeIds = _view.SelectedNodeIds.ToArray();

			if (action == OperationType.Delete)
			{
				DeleteNodes(selectedFolderIds, selectedNodeIds);
			}
			else if (action == ModellerOperationType.Export)
			{
				new B2mmlExporter(new ModellerSaveFileDialog()).Export(selectedFolderIds, selectedNodeIds);
			}
			else
			{
				throw new NotImplementedException();
			}
		}

		#endregion

		#region Other Methods

		/// <summary>
		///     For a list of model objects get the avialable operations
		/// </summary>
		/// <returns></returns>
		public IDictionary<string, EventHandler> AvailableOperations()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		///     Constructs the equipment level menu
		/// </summary>
		/// <param name="target">The target folder</param>
		/// <param name="nodeType">The type of node</param>
		/// <returns></returns>
		public IEnumerable<ContextAction> ConstructEquipmentLevelMenu(BrowserNode target, NodeType nodeType)
		{
			foreach (EquipmentLevel enumValue in Enum.GetValues(typeof(EquipmentLevel)))
			{
				target.CanAddNew(nodeType, new object[] {enumValue}, out var permittedFolders);
				foreach (var folder in permittedFolders)
				{
					if (target.CanAddNew(nodeType, new object[] {enumValue}))
					{
						yield return new ContextAction
						{
							Title = ExtensibleEnum.GetEnumValueDisplayName(enumValue),
							Action = () => { ConstructObject(new TargetFolderId(folder.Parent.Id, folder.MemberId), nodeType, enumValue); }
						};
					}
				}
			}
		}

		/// <summary>
		/// Triggers when a node has been deleted
		/// </summary>
		/// <param name="deletedNode">The deleted node</param>
		/// <param name="deletedFrom">The folder that the node has been deleted from</param>
		/// <param name="permanentlyDeleted"></param>
		public override void NodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
		{
			_view.RefreshNodes(new[] { deletedNode, deletedFrom.Parent}, new [] { deletedFrom });
		}

		/// <summary>
		///     Triggers when a node is added
		/// </summary>
		/// <param name="newNode">The node which has been added</param>
		public override void NodeAddedNew(BrowserNode newNode)
		{
			_view.RefreshNodes(new[] {newNode.ParentNode}, new[] {newNode.ParentFolder});
		}

		/// <summary>
		///     Triggers when a node is moved
		/// </summary>
		/// <param name="movedNode">The node which has been moved</param>
		/// <param name="movedFrom">The folder the node moved from</param>
		/// <param name="movedTo">The folder the node moved to</param>
		public override void NodeMoved(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
		{
			_view.RefreshNodes(new[] {movedNode, movedFrom.Parent, movedTo.Parent}, new[] {movedFrom, movedTo});
		}

		/// <summary>
		///     Triggers when a node's property is set
		/// </summary>
		/// <param name="node">The node which has been updated</param>
		/// <param name="memberId">The property memeber id which has been updated on the node</param>
		/// <param name="formerValue">The previous value of the member</param>
		public override void NodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
		{
			_view.RefreshNodes(new[] {node});
		}

		/// <summary>
		///     Constructs the add new action for the contents allowable in a node
		/// </summary>
		/// <param name="node">The node to create the add new action for</param>
		/// <returns></returns>
		private ContextAction ConstructAddNewAction(BrowserNode node)
		{
			var action = new ContextAction
			{
				Title = "Add New"
			};
			var childActions = new List<ContextAction>();
			node.CanAddNew(out var permittedTypes);
			foreach (var nodeType in permittedTypes.Distinct().Where(t => _view.Filter?.TargetNodeType == null || _view.Filter.TargetNodeType == t)
				// Models and pages must be created through Visio
				.Where(f => f != (NodeType.For(typeof(Model))) && f != NodeType.For(typeof(Page))))
			{

				if (nodeType.Configuration.Initialiser.Properties.Any())
				{
					var parameters = nodeType.Configuration.Initialiser.Properties.ToArray();
					if (parameters.Length == 1 && parameters.First()
						    .MemberId == NodeTypeMember.Property.EquipmentLevel)
					{
						childActions.Add(new ContextAction()
						{
							Title = nodeType.DisplayName,
							ChildActions = ConstructEquipmentLevelMenu(node, nodeType)
						});
					}
					else
					{
						throw new NotImplementedException();
					}
				}
				else
				{
					node.CanAddNew(nodeType, out var permittedFolders);
					var browserFolders = permittedFolders as BrowserFolder[] ?? permittedFolders.ToArray();
					if (browserFolders.Length > 1)
					{
						var folderChildActions = new List<ContextAction>();
						browserFolders.ForEach(f => folderChildActions.Add(ConstructAddNewAction(f, nodeType, true)));
						var folderAction = new ContextAction()
						{
							Title = nodeType.DisplayName,
							ChildActions = folderChildActions
						};
						childActions.Add(folderAction);
					}
					else
					{
						childActions.Add(ConstructAddNewAction(browserFolders.Single(), nodeType, false));
					}
				}
			}
			action.ChildActions = childActions;
			return action;
		}

		/// <summary>
		///     Constructs the new action for the allable contents in a folder
		/// </summary>
		/// <param name="folder">The folder to create the add new menu for</param>
		/// <param name="nodeType">The type of node to create</param>
		/// <param name="useFolderName">Whether or not to use the folder name</param>
		/// <returns></returns>
		private ContextAction ConstructAddNewAction(BrowserFolder folder, NodeType nodeType, bool useFolderName)
		{
			var action = new ContextAction
			{
				Title = useFolderName ? "To " + folder.DisplayName : nodeType.DisplayName,
				Action = () => ConstructObject(new TargetFolderId(folder.Parent.Id, folder.MemberId), nodeType)
			};


			return action;
		}

		/// <summary>
		///     Constructs the actions for a given node
		/// </summary>
		/// <returns></returns>
		private IEnumerable<ContextAction> ConstructBrowserNodeActions()
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var selectedNodes = GetSelectedNodes(scope).ToList();
				var allowableActions = new HashSet<OperationType>(selectedNodes.First().PermittedOperations.Where(a => _view.Filter?.PermittedOperations == null || ((bool) _view.Filter.PermittedOperations?.Contains(a))));
				// intersect with each other list to get operations that are in all
				selectedNodes.Skip(1).ForEach(n => allowableActions.IntersectWith(n.PermittedOperations));
				// TODO remove this block when implemented properly. Deleting a model needs to have an
				// undo to retrieve from the recycle bin
				if (selectedNodes.Any(n => n.Type == NodeType.For(typeof(Model))))
				{
					allowableActions.Remove(OperationType.Delete);
				}
				foreach (var a in allowableActions)
				{
					if (a == OperationType.AddNew)
					{
						if (selectedNodes.Count == 1 && selectedNodes.Single().IsDefinitionNode)
						{
							var addNew = ConstructAddNewAction(selectedNodes.Single());
							if (addNew.ChildActions?.Any() ?? false)
							{
								yield return addNew;
							} ;
						}
						else
						{
							break;
						}
					}
					else
					{
						yield return new ContextAction {Title = a.ToString(), Action = () => ExecuteOperation(a)};
					}
				}
				scope.Done();
			}
		}

		/// <summary>
		///     Constructs the action list for a given folder
		/// </summary>
		/// <param name="folderId">The id of the folder to create actions for</param>
		/// <returns></returns>
		private ContextAction ConstructFolderActions(TargetFolderId folderId)
		{
			if (_view.Filter?.PermittedOperations != null && !_view.Filter.PermittedOperations.Contains(OperationType.AddNew))
			{
				return null;
			}
			var action = new ContextAction { Title = "Add New" };
			var childActions = new List<ContextAction>();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var folder = scope.FindById<ParentNode>(folderId.ParentNodeId).AllFolders.Single(f => f.MemberId == folderId.FolderMemeberId);
				folder.CanAddNew(out var permittedTypes);
				foreach (var nodeType in permittedTypes.Distinct()
					// Models and pages must be created through Visio
					.Where(f => f != (NodeType.For(typeof(Model))) && f != NodeType.For(typeof(Page))))
				{

					if (nodeType.Configuration.Initialiser.Properties.Any())
					{
						var parameters = nodeType.Configuration.Initialiser.Properties.ToArray();
						if (parameters.Length == 1 && parameters.First().MemberId == NodeTypeMember.Property.EquipmentLevel)
						{
							childActions.Add(new ContextAction() {Title = nodeType.DisplayName, ChildActions = ConstructEquipmentLevelMenu(folder.Parent, nodeType)});
						}
						else
						{
							throw new NotImplementedException();
						}
					}
					else
					{
						folder.CanAddNew(out var permittedNodes);
						permittedNodes.ForEach(n => childActions.Add(ConstructAddNewAction(folder, n, false)));
					}
				}
				scope.Done();
			}
			action.ChildActions = childActions;
			return action;
		}

		/// <summary>
		///     Constructs an object for the given target
		/// </summary>
		/// <param name="targetFolderId">The target folder</param>
		/// <param name="nodeType">The typ of node to create</param>
		/// <param name="equipmentLevel">The equipment level if the target is a Hierarchy Scope, Equipment or Equipment Class</param>
		private void ConstructObject(TargetFolderId targetFolderId, NodeType nodeType, EquipmentLevel? equipmentLevel = null)
		{
			BrowserNode result;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var target = scope.FindById<ParentNode>(targetFolderId.ParentNodeId).AllFolders.Single(f => f.MemberId == targetFolderId.FolderMemeberId);
				if (nodeType.Id == NodeTypeId.Model || nodeType.Id == NodeTypeId.Page)
				{
					// we need to create the model or open the model and add the page
					throw new NotImplementedException();
				}
				using (var transaction = NodeTransaction.Begin())
				{
					result = target.Parent.AddNewAndPersist(nodeType,
						target,
						equipmentLevel != null
							? new object[] {equipmentLevel}
							: null);
					transaction.Complete();
					_modellerPresenter.CreateAndQueueUndoUnit(transaction,
						string.Format(UndoUnitDescriptions.AddNode,
							equipmentLevel == null
								? nodeType.DisplayName
								: ExtensibleEnum.GetEnumValueDisplayName(equipmentLevel.Value)));
				}
				_view.RefreshNodes(null, new[] { target });
				_view.SelectNode(result, true);
				scope.Done();
			}
		}

		/// <summary>
		///     Delete the given folders or nodes
		/// </summary>
		/// <param name="folderIds">The folders to delete all contents for</param>
		/// <param name="nodeIds">The nodes to delete</param>
		private void DeleteNodes(IEnumerable<TargetFolderId> folderIds, IEnumerable<Guid> nodeIds)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				using (var transction = NodeTransaction.Begin())
				{
					var nodes = GetSelectedNodes(scope).WhereTopLevelAndNotInverseLinks();
					nodes.DeleteAndPersist(Prefetch);
					transction.Complete();
					_modellerPresenter.CreateAndQueueUndoUnit(transction, UndoUnitDescriptions.DeleteNodes);
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Whether or not prefetching should be performed. Should only be false for unit testing
		/// </summary>
		public bool Prefetch { get; set; } = true;

		#endregion

		#region  Fields

		/// <summary>
		///     The view this presenter handles
		/// </summary>
		private readonly IModellerTreeView _view;

		/// <summary>
		///     The list of previously selected nodes
		/// </summary>
		private readonly IList<(IEnumerable<TargetFolderId>, IEnumerable<Guid>)> _backwardSelectedFoldersAndNodes = new List<(IEnumerable<TargetFolderId>, IEnumerable<Guid>)>();

		/// <summary>
		///     The forward list of previously selected nodes
		/// </summary>
		private readonly IList<(IEnumerable<TargetFolderId>, IEnumerable<Guid>)> _forwardsSelectedFoldersAndNodes = new List<(IEnumerable<TargetFolderId>, IEnumerable<Guid>)>();

		/// <summary>
		///     Whether or not we should supress the selected nodes changed.
		/// </summary>
		private bool _supressSelectedNodeChanged;

		
		private readonly IModellerPresenter _modellerPresenter;

		/// <summary>
		/// The list of permissible drag operations
		/// </summary>
		/// <param name="keyState"></param>
		/// <param name="targetNodeId">The id of the target node</param>
		/// <param name="targetFolderMemberId">The id of the target folder within the target node</param>
		/// <returns></returns>
		public IEnumerable<ContextAction> PermittedDragOperations(KeyState keyState, Guid targetNodeId, Guid? targetFolderMemberId = null)
		{
			IEnumerable<KeyValuePair<OperationType, IEnumerable<BrowserFolder>>> permittedOperations = new List<KeyValuePair<OperationType, IEnumerable<BrowserFolder>>>();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var selectedNodes = GetSelectedNodes(scope).ToArray();
				if (targetFolderMemberId == null)
				{
					var targetNode = scope.FindById<BrowserNode>(targetNodeId);
					permittedOperations = selectedNodes.SelectMany(n => n.PermittedOperationsTo(targetNode).ToArray())
						.Where(o => selectedNodes.All(n => n.PermittedOperationsTo(targetNode).Keys.ToArray().Contains(o.Key)))
						.Distinct()
						.ToArray();
				}
				if (targetFolderMemberId != null)
				{
					var targetFolder = scope.FindById<ParentNode>(targetNodeId).AllFolders.Single(f => f.MemberId == targetFolderMemberId);
					permittedOperations = selectedNodes.SelectMany(n => n.PermittedOperationsTo(targetFolder).ToArray())
						.Select(n => new KeyValuePair<OperationType, IEnumerable<BrowserFolder>>(n, new[] {targetFolder}))
						.Where(o => selectedNodes.All(n => n.PermittedOperationsTo(targetFolder)
						.Contains(o.Key)))
						.Distinct()
						.ToArray();
				}

				IEnumerable<ContextAction> actions = null;
				if ((keyState & KeyState.CtrlKey) == KeyState.CtrlKey && permittedOperations.Select(kvp => kvp.Key).Contains(OperationType.CopyTo))
				{
					actions = CreateContextActions(permittedOperations.Where(kvp => kvp.Key == OperationType.CopyTo), targetNodeId, targetFolderMemberId);
				}
				else if ((keyState & KeyState.ShiftKey) == KeyState.ShiftKey && permittedOperations.Select(kvp => kvp.Key).Contains(OperationType.MoveTo))
				{
					actions = CreateContextActions(permittedOperations.Where(kvp => kvp.Key == OperationType.MoveTo), targetNodeId, targetFolderMemberId);
				}
				else if ((keyState & KeyState.RightMouse) == KeyState.RightMouse)
				{
					actions = CreateContextActions(permittedOperations, targetNodeId, targetFolderMemberId);
				}
				if (actions == null)
				{
					if (permittedOperations.Select(kvp => kvp.Key).Contains(OperationType.AddTo))
					{
						actions = CreateContextActions(permittedOperations.Where(kvp => kvp.Key == OperationType.AddTo), targetNodeId, targetFolderMemberId);
					}
					else if (permittedOperations.Select(kvp => kvp.Key).Contains(OperationType.MoveTo))
					{
						actions = CreateContextActions(permittedOperations.Where(kvp => kvp.Key == OperationType.MoveTo), targetNodeId, targetFolderMemberId);
					}
					else if (permittedOperations.Select(kvp => kvp.Key).Contains(OperationType.CopyTo))
					{
						actions = CreateContextActions(permittedOperations.Where(kvp => kvp.Key == OperationType.CopyTo), targetNodeId, targetFolderMemberId);
					}
					else
					{
						actions = new ContextAction[] { };
					}
				}
				actions = actions.ToArray();// Evaluate now in scope
				scope.Done();
				return actions;
			}
		}

		/// <summary>
		/// Create the context action for a set of permitted operations
		/// </summary>
		/// <param name="permittedOperations">The permitted operations and their targets</param>
		/// <param name="browserNodeId">The target browser node id</param>
		/// <param name="targetFolderMemberId">The target folder id</param>
		/// <returns></returns>
		private IEnumerable<ContextAction> CreateContextActions(IEnumerable<KeyValuePair<OperationType, IEnumerable<BrowserFolder>>> permittedOperations, Guid browserNodeId, Guid? targetFolderMemberId)
		{
			foreach (var operation in permittedOperations)
			{
				var action = new ContextAction()
				{
					Title = operation.Key.ToString(),
					Operation =  operation.Key
				};
				if (operation.Value.Count() == 1)
				{
					action.Action = () => PerformDragOperation(operation.Key, browserNodeId, targetFolderMemberId);
				}
				else
				{
					action.ChildActions = operation.Value.Select(v => new ContextAction
					{
						Title = v.DisplayName,
						Action = () => PerformDragOperation(operation.Key, browserNodeId, v.MemberId)
					}).ToArray(); // evaluate now in scope
				}
				yield return action;
			}
		}

		private void PerformDragOperation(OperationType operation, Guid browserNodeId, Guid? targetFolderMemberId = null)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var targetNode = scope.FindById<DefinitionNode>(browserNodeId);
				var targetFolder = targetFolderMemberId != null 
					? targetNode.AllFolders.Single(f => f.MemberId == targetFolderMemberId) 
					: null;
				if (targetNode == null)
				{
					return;
				}
				var selectedNodes = GetSelectedNodes(scope);
				foreach (var selectedNode in selectedNodes)
				{
					if (operation == OperationType.AddTo)
					{
						if (targetFolder == null)
						{
							((DefinitionNode) selectedNode).AddAndPersistTo(targetNode);
						}
						else
						{
							((DefinitionNode)selectedNode).AddAndPersistTo(targetFolder);
						}
					}
					if (operation == OperationType.MoveTo)
					{
						if (targetFolder == null)
						{
							selectedNode.MoveTo(targetNode);
							selectedNode.Save();
							targetNode.Save();
						}
						else
						{
							selectedNode.MoveAndPersistTo(targetFolder);
						}
						selectedNode.Save();
					}
					if (operation == OperationType.CopyTo)
					{
						var newNode = targetFolder == null
							? selectedNode.CopyTo(targetNode)
							: selectedNode.CopyTo(targetFolder);
						newNode.Save();
					}
				}
				scope.Done();
			}
		}

		public IEnumerable<IModellerShape> GetDragDropCollection()
		{
			var selection = new List<IModellerShape>();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var selectedNodes = GetSelectedNodes(scope).Union(GetSelectedFolders(scope).SelectMany(f => f.Nodes)).ToArray();
				foreach (var target in selectedNodes)
				{
					if (target.Is<SpecificationDefinitionElement>(out var specification))
					{
						var specificationObjects = new List<IModellerShape>();
						if (!selectedNodes.Contains(specification.Process) && specification.Process.Occurrences.All(o => o.Page.Id != _modellerPresenter.ActivePage.PageInternalId))
						{
							specificationObjects.Add(_modellerPresenter.CreateNewShapeAndMapOccurrence(specification.Process));
						}
						foreach (var resource in specification.Resources.Where(r => selectedNodes.Contains(r) && r.Occurrences.All(o => o.Page.Id != _modellerPresenter.ActivePage.PageInternalId)))
						{
							specificationObjects.Add(_modellerPresenter.CreateNewShapeAndMapOccurrence(resource));
						}
						// If everything is already on the page, just add the resources again
						if (specificationObjects.Count == 0)
						{
							foreach (var resource in specification.Resources)
							{
								specificationObjects.Add(_modellerPresenter.CreateNewShapeAndMapOccurrence(resource));
							}
						}
						specificationObjects.ForEach(selection.Add);
					}
					else if (target.Is<ModelElement>())
					{
						selection.Add(_modellerPresenter.CreateNewShapeAndMapOccurrence(target));
					}
				}
				scope.Done();
			}
			//selection.AutoArrange();
			return selection;
		}

		private IEnumerable<BrowserNode> GetSelectedNodes(PersistenceScope scope)
		{
			return scope.Query<BrowserNode>().Where(n => _view.SelectedNodeIds.Contains(n.Id));
		}

		private IEnumerable<BrowserFolder> GetSelectedFolders(PersistenceScope scope)
		{
			return scope.Query<ParentNode>().ToArray()
				//.Where(n => _view.SelectedFolderIds.Select(i => i.ParentNodeId).Contains(n.Id))
				.SelectMany(n => n.AllFolders)
				.Where(f => _view.SelectedFolderIds.Select(i => i.ParentNodeId).Contains(f.Parent.Id)
						 && _view.SelectedFolderIds.Select(i => i.FolderMemeberId).Contains(f.MemberId));

		}

		#endregion
	}
}