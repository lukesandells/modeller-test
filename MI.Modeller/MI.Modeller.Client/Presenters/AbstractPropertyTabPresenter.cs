﻿using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	/// Abstract presenter for the property tab
	/// </summary>
	public abstract class AbstractPropertyTabPresenter<TProperty, TOfObject>: IPropertyTabPresenter<TProperty, TOfObject>
		where TOfObject : class, IWithProperties<TOfObject, TProperty>, IWithExternalId
		where TProperty : Property<TProperty, TOfObject>
	{
		/// <summary>
		/// The node this presenter is for
		/// </summary>
		protected BrowserNode Node;

		private IPropertyTabPresenter<TProperty, TOfObject> _propertyTabPresenterImplementation;

		private const string C_NEW_PROPERTY_NAME = "New.Property";

		/// <summary>
		/// Initialises a new AbstractPropertyTabPresenter
		/// </summary>
		/// <param name="node"></param>
		protected AbstractPropertyTabPresenter(BrowserNode node)
		{
			Node = ArgumentValidation.AssertNotNull(node, nameof(node));
		}

		public abstract TProperty AddProperty(string externalId = null, Value value = null, TProperty parentProperty = default(TProperty));

		/// <summary>
		/// Removes the given property
		/// </summary>
		/// <param name="property">The property to remove</param>
		public void RemoveProperty(TProperty property)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				((TOfObject)Node.TargetObject).RemoveProperty(property);
				Node.Update();
				scope.Done();
			}
		}

		/// <summary>
		/// Resave the node this presenter is resonsible for
		/// </summary>
		public void UpdateNode()
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				using (var transaction = NodeTransaction.Begin())
				{
					Node.Update();
					transaction.Complete();
					// TODO put transaction onto the stack
					scope.Done();
				}
			}
		}

		protected string GetNextPropertyId()
		{
			var candidateProperties = ((TOfObject) Node.TargetObject).AllProperties.Where(p => p.ExternalId.StartsWith(C_NEW_PROPERTY_NAME)).Select(p => p.ExternalId.ToString().Replace(C_NEW_PROPERTY_NAME, "").Replace(".", "").Trim());
			var properties = candidateProperties as string[] ?? candidateProperties?.ToArray();
			if (properties?.Length == 1 && properties.First() == "")
			{
				return C_NEW_PROPERTY_NAME + ".1";
			}
			var propertyNumbers = properties.Where(IsNumeric).ToArray();
			var a = propertyNumbers?.Select(int.Parse);
			if (propertyNumbers.Length == 0)
			{
				return C_NEW_PROPERTY_NAME;
			}
			return C_NEW_PROPERTY_NAME + "." + (a.Max() + 1);
		}

		private bool IsNumeric(string s)
		{
			int test;
			return int.TryParse(s, out test);
		}
	}
}
