﻿using System;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Domain;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	/// Presenter for the property pane
	/// </summary>
	public class PropertyPanePresenter : IPropertyPanePresenter
	{
		#region  Fields/Properties

		/// <summary>
		/// The modeller presenter to interact with
		/// </summary>
		private readonly IModellerPresenter _modellerPresenter;

		#endregion

		#region Constructors

		/// <summary>
		/// Initializes a new PropertyPanePresenter
		/// </summary>
		/// <param name="modellerPresenter">The modeller presenter to interact with</param>
		public PropertyPanePresenter(IModellerPresenter modellerPresenter)
		{
			_modellerPresenter = ArgumentValidation.AssertNotNull(modellerPresenter, nameof(modellerPresenter));
		}

		#endregion

		#region Other Methods

		/// <summary>
		/// Update the given property of a model element
		/// </summary>
		/// <param name="modelElementId">The model element to update</param>
		/// <param name="property">The property to update</param>
		/// <param name="value">The updated value</param>
		/// <returns>The validation result of the update</returns>
		public string UpdateProperty(Guid modelElementId, PropertyConfiguration property, object value)
		{
			string validationMessage;
			// Enums need to be resolved back from a display name into their type. Other types will have null returned
			if (property.NativeType.IsEnum || property.NativeType.IsExtensibleEnum())
			{
				value = ExtensibleEnum.GetEnumValueFromDisplayName(value.ToString(), property.NativeType) ?? value;
			}
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var modelElement = scope.FindById<ModelElement>(modelElementId);
				// Browser node values will need to be re-attached to the scope
				if (modelElement.Properties[property.MemberId].Configuration.IsNodeType && value != null)
				{
					value = scope.FindById<BrowserNode>(((BrowserNode) value).Id);
				}
				using (var transaction = NodeTransaction.Begin())
				{
					// Update the property
					if (modelElement.Properties[property.MemberId].ValidateInput(value, out validationMessage))
					{
						modelElement.Update();
						transaction.NodeChanges.PersistChanges();
						transaction.Commit();
						_modellerPresenter.CreateAndQueueUndoUnit(transaction, string.Format(UndoUnitDescriptions.ChangeProperty, new[] {property.DisplayName, modelElement.DisplayId}));
						transaction.Complete();
					}
					else
					{
						transaction.RollBack();
					}
				}
				scope.Done();
			}
			return validationMessage;
		}

		#endregion
	}
}