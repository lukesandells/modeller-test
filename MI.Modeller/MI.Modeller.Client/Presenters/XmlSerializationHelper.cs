﻿using System.IO;
using System.Xml.Serialization;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	///     Class XmlSerializationHelper.
	/// </summary>
	public class XmlSerializationHelper : ISerializationHelper
	{
		#region  Interface Implementations

		/// <summary>
		///     Checks if a given file exists
		/// </summary>
		/// <param name="filename">The filename.</param>
		/// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public bool FileExists(string filename)
		{
			return File.Exists(filename);
		}

		/// <summary>
		///     Reads the file.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="path">The path.</param>
		/// <returns>T.</returns>
		public T ReadFile<T>(string path)
		{
			if (!FileExists(path))
			{
				return default(T);
			}

			var file = File.Open(path, FileMode.OpenOrCreate);
			var reader = new StreamReader(file);
			T returnObject;
			try
			{
				returnObject = Deserialize<T>(reader.ReadToEnd());
			}
			finally
			{
				file.Close();
			}
			return returnObject;
		}

		/// <summary>
		///     Writes the file.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="path">The path.</param>
		/// <param name="objectToWrite">The object to write.</param>
		public void WriteFile<T>(string path, T objectToWrite)
		{
			var file = File.Open(path, FileMode.Create);
			var writer = new StreamWriter(file);
			try
			{
				writer.Write(Serialize(objectToWrite));
			}
			finally
			{
				writer.Flush();
				file.Close();
			}
		}

		/// <summary>
		///     Deserializes the specified s.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="s">The s.</param>
		/// <returns>T.</returns>
		public T Deserialize<T>(string s)
		{
			var serializer = new XmlSerializer(typeof(T));
			T result;

			using (TextReader reader = new StringReader(s))
			{
				result = (T) serializer.Deserialize(reader);
			}

			return result;
		}

		/// <summary>
		///     Serializes the specified o.
		/// </summary>
		/// <param name="o">The o.</param>
		/// <returns>System.String.</returns>
		public string Serialize(object o)
		{
			var xmlSerializer = new XmlSerializer(o.GetType());

			var textWriter = new StringWriter();
			xmlSerializer.Serialize(textWriter, o);
			return textWriter.ToString();
		}

		#endregion
	}
}