﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.Persistence;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	/// Presenter for the Basic Property Tab
	/// </summary>
	public class BasicPropertyTabPresenter<TOfObject>: AbstractPropertyTabPresenter<BasicProperty<TOfObject>, TOfObject> 
		where TOfObject : class, IWithBasicProperties<TOfObject>, IWithExternalId {
		public BasicPropertyTabPresenter(BrowserNode node) : base(node) { }

		/// <summary>
		///  Add a new child property to the given parent property
		/// </summary>
		/// <param name="externalId">The ID of the new property</param>
		/// <param name="value">The value of the property</param>
		/// <param name="parentProperty">The parent property</param>
		public override BasicProperty<TOfObject> AddProperty(string externalId = null, Value value = null, BasicProperty<TOfObject> parentProperty = null)
		{
			if (string.IsNullOrEmpty(externalId))
			{
				externalId = GetNextPropertyId();
			}
			BasicProperty<TOfObject> newProperty;

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				newProperty = ((TOfObject)Node.TargetObject).AddProperty(externalId, value, parentProperty);
				Node.Update();
				scope.Done();
			}

			return newProperty;
		}
	}
}
