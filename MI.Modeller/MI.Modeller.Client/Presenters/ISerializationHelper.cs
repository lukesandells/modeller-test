﻿namespace MI.Modeller.Client.Presenters
{
	public interface ISerializationHelper
	{
		#region Other Methods

		/// <summary>
		/// Indicates weather or not a file exists at the given path
		/// </summary>
		/// <param name="path">The path to check</param>
		/// <returns></returns>
		bool FileExists(string path);

		/// <summary>
		///     Reads the file.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="path">The path.</param>
		/// <returns>T.</returns>
		T ReadFile<T>(string path);

		/// <summary>
		///     Deserializes the specified s.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="s">The s.</param>
		/// <returns>T.</returns>
		T Deserialize<T>(string s);

		/// <summary>
		///     Serializes the specified o.
		/// </summary>
		/// <param name="o">The o.</param>
		/// <returns>System.String.</returns>
		string Serialize(object o);

		/// <summary>
		///     Writes the file.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="path">The path.</param>
		/// <param name="objectToWrite">The object to write.</param>
		void WriteFile<T>(string path, T objectToWrite);

		#endregion
	}
}