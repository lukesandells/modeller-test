﻿using System;
using MI.Framework.ObjectBrowsing;
using System.Collections.Generic;

namespace MI.Modeller.Client.Presenters.Interfaces
{
	/// <summary>
	///     Interface for Panel Presenters
	/// </summary>
	public interface IPanelPresenter

    {
        #region Members

        /// <summary>
        ///     Shows the node properties.
        /// </summary>
        /// <param name="treeNodes">The tree node.</param>
        void ShowNodeProperties(IEnumerable<Guid> treeNodes);

        #endregion
    }
}