﻿using System;

namespace MI.Modeller.Client.Presenters.Interfaces
{
	/// <summary>
	///     Enum representing the keystate for drag and drop operations. Values from standard DragEventArgs
	/// </summary>
	[Flags]
	public enum KeyState
	{
		LeftMouse = 1,
		RightMouse = 2,
		ShiftKey = 4,
		CtrlKey = 8,
		MiddleMouse = 16,
		AltKey = 32
	}
}