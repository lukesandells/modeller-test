﻿using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.Modeller.Client.Presenters.Interfaces
{
	/// <summary>
	/// Presenter for the property tab
	/// </summary>
	public interface IPropertyTabPresenter<TProperty, TOfObject>
		where TOfObject : class, IWithProperties<TOfObject, TProperty>, IWithExternalId
		where TProperty : Property<TProperty, TOfObject>
	{
		/// <summary>
		///  Add a new child property to the given parent property
		/// </summary>
		/// <param name="externalId">The ID of the new property</param>
		/// <param name="value">The value of the property</param>
		/// <param name="parentProperty">The parent property</param>
		TProperty AddProperty(string externalId = null, Value value = null, TProperty parentProperty = null);

		/// <summary>
		/// Removes the given property
		/// </summary>
		/// <param name="property">The property to remove</param>
		void RemoveProperty(TProperty property);

		/// <summary>
		/// Resave the node this presenter is resonsible for
		/// </summary>
		void UpdateNode();
	}
}
