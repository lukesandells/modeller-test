﻿using System;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.ViewInterfaces;

namespace MI.Modeller.Client.Presenters.Interfaces
{
	/// <summary>
	///     Presenter for the modeller tree view
	/// </summary>
	public interface IObjectBrowserPresenter
	{
		#region Other Methods

		/// <summary>
		///     Gets the allowable right click actions based on what is currently selected in the view
		/// </summary>
		/// <returns></returns>
		IEnumerable<ContextAction> GetPermissibleOperations();

		/// <summary>
		///     Updates a node's display ID. Retruns the error message if input is invalid
		/// </summary>
		/// <param name="nodeId">The node to update</param>
		/// <param name="newId">The new ID of the node</param>
		string UpdateDisplayId(Guid nodeId, string newId);

		#endregion

		#region Members

		/// <summary>
		///     Execute the given action
		/// </summary>
		/// <param name="action"></param>
		/// <param name="param"></param>
		void ExecuteOperation(OperationType action, object[] param = null);

		/// <summary>
		///     Handle the SelectedNodeChanged event
		/// </summary>
		void HandleSelectedNodeChanged(object sender, NodeSelectionChangedEventArguments eventArgs);

		/// <summary>
		///     Navigate back to the previously selected node
		/// </summary>
		void NavigateBack();

		/// <summary>
		///     Navigate forward
		/// </summary>
		void NavigateForward();

		#endregion

		/// <summary>
		/// Get the permitted drag operations for the currently selected node(s)
		/// </summary>
		/// <param name="keyState">the key state of the event</param>
		/// <param name="targetNodeId">The target browser node</param>
		/// <param name="targetFolderMemberId">The target browser folder within the target node</param>
		IEnumerable<ContextAction> PermittedDragOperations(KeyState keyState, Guid targetNodeId, Guid? targetFolderMemberId = null);

		/// <summary>
		/// Gets the collection of shapes for drag and drop operations
		/// </summary>
		/// <returns></returns>
		IEnumerable<IModellerShape> GetDragDropCollection();
	}
}