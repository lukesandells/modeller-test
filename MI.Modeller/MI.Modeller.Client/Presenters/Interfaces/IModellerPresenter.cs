﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Client.Presenters.Interfaces
{
	/// <summary>
	///     Interface IModellerPresenter
	/// </summary>
	public interface IModellerPresenter
	{
		#region  Fields

		/// <summary>
		///     Gets or sets a value indicating whether [drag in operation].
		/// </summary>
		/// <value><c>true</c> if [drag in operation]; otherwise, <c>false</c>.</value>
		bool DragInOperation { get; set; }

		/// <summary>
		///     Gets the shape factory.
		/// </summary>
		/// <value>The shape factory.</value>
		IShapeFactory ShapeFactory { get; }

		/// <summary>
		/// The currently active document
		/// </summary>
		IModellerDocument ActiveDocument { get; }

		IModellerPage ActivePage { get; }
		bool SupressSelectionChange { get; set; }

		#endregion

		#region Members

		/// <summary>
		///     Creates an object browser view
		/// </summary>
		/// <param name="modelElementTreeFilter">The tree filter to apply</param>
		/// <param name="rootNodes">The root nodes to display</param>
		/// <param name="allowMultipleSelection">Whether or not multiple objects may be selected</param>
		/// <returns></returns>
		IModellerTreeView CreateObjectBrowserView(ModelElementTreeFilter modelElementTreeFilter, IEnumerable<Guid> rootNodes, bool allowMultipleSelection = true);

		/// <summary>
		///     Create a new simple form
		/// </summary>
		/// <param name="title">The form's title</param>
		/// <returns></returns>
		ISimpleForm CreateSimpleForm(string title);

		/// <summary>
		///     Gets the model for a document
		/// </summary>
		/// <param name="document"></param>
		/// <returns></returns>
		Model GetModelForDocument(IModellerDocument document);

		/// <summary>
		///     Determines if a model is accessible
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		bool IsModelAccessible(Model model);

		/// <summary>
		///     Selects all occurrences.
		/// </summary>
		/// <param name="modelObject">The modeller object.</param>
		void SelectAllOccurrences(ModelElement modelObject);

		/// <summary>
		///     Deselects all occurrences.
		/// </summary>
		/// <param name="modelObject">The modeller object.</param>
		void DeselectAllOccurrences(ModelElement modelObject);

		/// <summary>
		///     Update's the shape's model object's description
		/// </summary>
		/// <param name="shape"></param>
		void UpdateModelObjectDescriptionFromShape(IModellerShape shape);

		/// <summary>
		///     Update's the shape's model object's externalId
		/// </summary>
		/// <param name="shape"></param>
		void UpdateModelObjectExternalIdFromShape(IModellerShape shape);

		/// <summary>
		///     Creates and queues an undo unit for a given transaction
		/// </summary>
		/// <param name="nodeTransaction">The node transaction to create an undo unit for</param>
		/// <param name="description">A description of the event that this undo unit is for</param>
		void CreateAndQueueUndoUnit(NodeTransaction nodeTransaction, string description);

		#endregion

		/// <summary>
		/// Creates an occurrence for the given shape and maps it
		/// </summary>
		/// <param name="shape">The shape to create the occurrence</param>
		/// <param name="target">The target object for this shape</param>
		Occurrence CreateOccurrenceAndMapShape(IModellerShape shape, ModelElement target);

		IModellerShape CreateNewShapeAndMapOccurrence(BrowserNode target);

		/// <summary>
		/// Select the given shapes and return it as a selection
		/// </summary>
		/// <param name="shapes">The shapes to select</param>
		/// <returns></returns>
		IModellerSelection SelectShapes(IEnumerable<IModellerShape> shapes);

		/// <summary>
		/// Creates the connections for the given occurrences
		/// </summary>
		/// <param name="occurrenceIds">The IDs of the occurrences to create connections for</param>
		void CreateConnectionsForOccurrences(IEnumerable<Guid?> occurrenceIds);

		/// <summary>
		/// Layout the occurrences
		/// </summary>
		/// <param name="occurrenceIds">The IDs of the occurences to layout</param>
		void LayoutOccurrences(IEnumerable<Guid?> occurrenceIds);

		/// <summary>
		/// Updates the model object (and pages/occurrences) from a given document
		/// </summary>
		/// <param name="document">The document to update the model to reflect</param>
		void UpdateModelFromDocument(IModellerDocument document);

		/// <summary>
		/// Updates a model page (and occrrences) from the page
		/// </summary>
		/// <param name="page">The page to update from</param>
		void UpdateModelFromPage(IModellerPage page);

		/// <summary>
		/// Updates an occurrence from a shape
		/// </summary>
		/// <param name="shape">The shape to update from</param>
		void UpdateOccurrenceFromShape(IModellerShape shape);

		/// <summary>
		/// Opens the given model
		/// </summary>
		/// <param name="modelId">The id of the model to open</param>
		void OpenModel(Guid modelId);
	}
}