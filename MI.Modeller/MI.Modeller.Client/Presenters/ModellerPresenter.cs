﻿using MI.Framework;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.MasterShapes;
using MI.Modeller.Client.ModellerEventArgs;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	///     Class ModellerPresenter.
	/// </summary>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.Presenters.Interfaces.IModellerPresenter"</cref>
	/// </seealso>
	/// <seealso>
	///     <cref>"MI.Modeller.Client.Presenters.Interfaces.IModellerPresenter"</cref>
	/// </seealso>
	public class ModellerPresenter : NodeActivityListener, IModellerPresenter
	{
		#region Constructors

		/// <summary>
		///     Gets the shape factory.
		/// </summary>
		/// <param name="modeller">The modeller.</param>
		/// <param name="shapeFactory">The shape factory.</param>
		/// <param name="hierarchyScopeSelectionTreeViewForm">The tree view form.</param>
		/// <value>The shape factory.</value>
		// public IShapeFactory ShapeFactory { get; } = new VisioShapeFactory();
		public ModellerPresenter(IModeller modeller, IShapeFactory shapeFactory, ITreeViewForm hierarchyScopeSelectionTreeViewForm = null)
		{
			_modeller = ArgumentValidation.AssertNotNull(modeller, nameof(modeller));
			shapeFactory = ArgumentValidation.AssertNotNull(shapeFactory, nameof(shapeFactory));

			_hierarchyScopeSelectionTreeViewForm = hierarchyScopeSelectionTreeViewForm;

			ShapeFactory = ArgumentValidation.AssertNotNull(shapeFactory, nameof(shapeFactory));

			NodeActivityLog.AddListener(this);

			// Events
			_modeller.ShapeAdded += HandleShapeAdded;
			_modeller.ShapeDeleted += HandleShapeDeleted;
			_modeller.SelectionChanged += HandleSelectionChanged;
			_modeller.ShutDown += HandleShutDown;
			_modeller.DocumentCreated += HandleDocumentCreated;
			_modeller.DocumentOpened += HandleDocumentOpened;
			_modeller.DocumentSaved += HandleDocumentSaved;
			_modeller.PageCreated += HandlePageAdded;
			_modeller.PageChanged += HandlePageChanged;
			_modeller.PageDeleted += HandlePageDeleted;
			_modeller.ConnectionAdded += HandleConnectionAdded;
			_modeller.ConnectionRemoved += HandleConnectionRemoved;
		}

		/// <summary>
		/// Handles when a page has been changed in a document
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandlePageChanged(object sender, ModellerPageEventArgs e)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var page = scope.FindById<Page>(e.Page.PageInternalId);
				// at this stage this is the only thing that can be updated that we care about
				page.DisplayId = e.Page.Title;
				scope.Done();
			}
		}

		/// <summary>
		/// Handles when a new page is added to a document
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandlePageAdded(object sender, ModellerPageEventArgs e)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var document = GetModelForDocument(e.Page.Document);
				var page = document.AddNewAndPersist(NodeType.For(typeof(Page)), e.Page.Title);
				e.Page.PageInternalId = page.Id;
				scope.Done();
			}
		}

		/// <summary>
		/// Handles when a new page is added to a document
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandlePageDeleted(object sender, ModellerPageEventArgs e)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var page = scope.FindById<Page>(e.Page.PageInternalId);
				page?.DeleteAndPersist(true);
				scope.Done();
			}
		}

		#endregion

		#region  Interface Implementations

		/// <summary>
		///     Gets the model for a document
		/// </summary>
		/// <param name="document"></param>
		/// <returns></returns>
		public Model GetModelForDocument(IModellerDocument document)
		{
			if (document == null || !document.IsModellerDocument)
			{
				return null;
			}
			Model model;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				model = scope.FindById<Model>(document.ModelInternalId);
				scope.Done();
			}
			return model;
		}

		/// <summary>
		///     Selects all occurrences.
		/// </summary>
		/// <param name="modelObject">The modeller object.</param>
		public void SelectAllOccurrences(ModelElement modelObject)
		{
			// Shift-selecting a node must select all the shapes representing that node - I.e. all the occurrences
			if (_modeller.ActiveDocument == null || SupressSelectionChange)
			{
				return;
			}

			// set the window selection to the occurrences
			SupressSelectionChange = true;
			_modeller.SelectShapes(_modeller.ActiveDocument.Shapes
				.Where(s => s.ModelElementId == modelObject.Id));
			SupressSelectionChange = false;
		}

		/// <summary>
		///     Deselects all occurrences.
		/// </summary>
		/// <param name="modelObject">The modeller object.</param>
		public void DeselectAllOccurrences(ModelElement modelObject)
		{
			// Shift-selecting a node must select all the shapes representing that node - I.e. all the occurrences
			if (_modeller.ActiveDocument == null || SupressSelectionChange)
			{
				return;
			}

			// set the window selection to the occurrences
			SupressSelectionChange = true;
			_modeller.DeselectShapes(_modeller.ActiveDocument.Shapes
				.Where(s => s.ModelElementId == modelObject.Id));
			SupressSelectionChange = false;
		}

		#endregion

		#region Members

		/// <summary>
		///     Handles the connection added.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerShapeConnectionEventArgs" /> instance containing the event data.</param>
		private void HandleConnectionAdded(object sender, ModellerShapeConnectionEventArgs e)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Return if the connection is incomplete or not a modeller shape
				if (e.Connector.ShapeFrom == null || e.Connector.ShapeTo == null)
				{
					return;
				}
				// Otherwise create a connector occurrence
				// We're interested in connections between processes and resources for now
				(var _, var process) = GetModelElementFromConnector(e.Connector, typeof(IProcessDefinition<>));
				if (process == null)
				{
					scope.Done();
					return;
				}
				(var resourceShape, var resource) = GetModelElementFromConnector(e.Connector, typeof(IResourceObject<>));
				var page = GetPageForModellerPage(e.Connector.ModellerPage);
				if (resource != null)
				{
					ConnectorOccurrence occurrence;
					// The hierarchy scope of  a specification is where the resource shape is dropped
					var hierarchyScope = GetHeirarchyScopeForShape(resourceShape);
					if (e.Connector.ConnectionOccurrenceId == null || scope.FindById<ConnectorOccurrence>(e.Connector.ConnectionOccurrenceId) == null)
					{
						occurrence = CreateNewSpecificationFromConnector(process, resource, e.Connector, page, hierarchyScope);
					}
					else
					{
						occurrence = scope.FindById<ConnectorOccurrence>(e.Connector.ConnectionOccurrenceId);
						// New connections get the connect added event twice - once for each end. If we're already up to date we don't need to change anything
						if ((occurrence.From.ModelElement.Id == e.Connector.ShapeFrom.ModelElementId &&
						     occurrence.To.ModelElement.Id == e.Connector.ShapeTo.ModelElementId)
							 // Or it's the reverse
							 || (occurrence.To.ModelElement.Id == e.Connector.ShapeFrom.ModelElementId &&
							     occurrence.From.ModelElement.Id == e.Connector.ShapeTo.ModelElementId))
						{
							scope.Done();
							return;
						}
						var specification = occurrence.ModelElement.Cast<SpecificationDefinitionElement>();
						// first check if it's a change in type. If so just create a new occurrence
						if (specification.Resources.All(r => r.Type != resource.Type))
						{
							occurrence = CreateNewSpecificationFromConnector(process, resource, e.Connector, page, hierarchyScope);
						} else {
							UpdateConnectorOccurrenceFromConnector(process, resource, occurrence);
						}
					}
					
					MapConection(e.Connector, occurrence);
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Updates the connector occurrence 
		/// </summary>
		/// <param name="process">The process to update to</param>
		/// <param name="resource">The resource to update to</param>
		/// <param name="occurrence">The occurrence to updae from</param>
		private void UpdateConnectorOccurrenceFromConnector(ModelElement process, ModelElement resource, ConnectorOccurrence occurrence)
		{
			var specification = occurrence.ModelElement.Cast<SpecificationDefinitionElement>();
			if (specification.Resources.All(r => r.Id != resource.Id))
			{
				// delete the existing link
				specification.InboundLinks.Where(l => l.Type == process.Type).ForEach(l => l.DeleteAndPersist(true));
				// update
				UpdateSpecificationNode(specification, resource);
			}
			if (specification.Process.Id != process.Id)
			{
				// Move to new process
				specification.MoveAndPersistTo(process.NonSystemFolders.Single(f => f.PermittedTypes.Contains(specification.Type)));
			}
		}

		/// <summary>
		/// Creates a new occurrence for a given specification
		/// </summary>
		/// <param name="process">The process to create an occurrence from</param>
		/// <param name="resource">The resource for the specification</param>
		/// <param name="connector">The connector that this is being created from</param>
		/// <param name="page">The page the connector is on</param>
		/// <param name="hierarchyScope">The hierarchy scope to create the specification in</param>
		/// <returns></returns>
		private ConnectorOccurrence CreateNewSpecificationFromConnector(ModelElement process, ModelElement resource, IModellerConnector connector, Page page, ModelElement hierarchyScope)
		{
			ModelElement specNode = process.As<ProcessDefinitionElement>().AllSpecificationNodes.FirstOrDefault(s => s.Resources.Contains(resource));
			if (specNode == null)
			{
				specNode = CreateSpecificationNode(hierarchyScope, process, resource);
				specNode.Properties[NodeTypeMember.Property.HierarchyScope].Value = hierarchyScope;
				if (specNode.Type.Configuration.Properties.ContainsKey(NodeTypeMember.Property.MaterialUse))
				{
					specNode.Properties[NodeTypeMember.Property.MaterialUse].Value = connector.ShapeFrom.ModelElementId == process.Id
						? MaterialUse.Consumed
						: MaterialUse.Produced;
				}
				specNode.Save();
			}
			var fromOcurrence = ShapeFactory.GetOccurenceForShape(connector.ShapeFrom);
			var toOccurrence = ShapeFactory.GetOccurenceForShape(connector.ShapeTo);
			return page.AddElementAsConnector(specNode, fromOcurrence, toOccurrence);
		}

		/// <summary>
		/// Maps a connection
		/// </summary>
		/// <param name="connector">The connector to map</param>
		/// <param name="occurrence">The occurrence to map onto the connector</param>
		private void MapConection(IModellerConnector connector, ConnectorOccurrence occurrence)
		{
			connector.ConnectionOccurrenceId = occurrence.Id;
			connector.ModelElementId = occurrence.ModelElement.Id;
			var specification = occurrence.ModelElement as SpecificationDefinitionElement;
			if (specification != null)
			{
				using (var scope = new PersistenceScope(TransactionOption.Required))
				{
					var direction = GetConnectionDirection(specification, connector.ShapeTo);
					connector.ConnectionDirection = direction;
					scope.Done();
				}
			}
		}

		/// <summary>
		/// Creates a new specification node
		/// </summary>
		/// <param name="hierarchyScope">The hierarcy scope of the specification</param>
		/// <param name="process">The process of the specification</param>
		/// <param name="resource">The reource of the specificaiton</param>
		/// <returns></returns>
		private ModelElement CreateSpecificationNode(ModelElement hierarchyScope, ModelElement process, ModelElement resource)
		{
			ModelElement specNode = null;
			if (resource.DesignObject is Equipment)
			{
				specNode = process.AddNewAndPersist(NodeType.For(typeof(EquipmentSpecification<>).MakeGenericType(process.DesignObject.GetType()))).Cast<ModelElement>();
				specNode.Properties[NodeTypeMember.Property.Equipment].Value = resource;
			}
			if (resource.DesignObject is EquipmentClass)
			{
				specNode = process.AddNewAndPersist(NodeType.For(typeof(EquipmentSpecification<>).MakeGenericType(process.DesignObject.GetType()))).Cast<ModelElement>();
				specNode.Properties[NodeTypeMember.Property.EquipmentClass].Value = resource;
			}
			if (resource.DesignObject is MaterialClass)
			{
				specNode = process.AddNewAndPersist(NodeType.For(typeof(MaterialSpecification<>).MakeGenericType(process.DesignObject.GetType()))).Cast<ModelElement>();
				specNode.Properties[NodeTypeMember.Property.MaterialClass].Value = resource;
			}
			if (resource.DesignObject is MaterialDefinition)
			{
				specNode = process.AddNewAndPersist(NodeType.For(typeof(MaterialSpecification<>).MakeGenericType(process.DesignObject.GetType()))).Cast<ModelElement>();
				specNode.Properties[NodeTypeMember.Property.MaterialDefinition].Value = resource;
			}
			if (resource.DesignObject is PersonnelClass)
			{
				specNode = process.AddNewAndPersist(NodeType.For(typeof(PersonnelSpecification<>).MakeGenericType(process.DesignObject.GetType()))).Cast<ModelElement>();
				specNode.Properties[NodeTypeMember.Property.PersonnelClass].Value = resource;
			}
			if (resource.DesignObject is PhysicalAssetClass)
			{
				specNode = process.AddNewAndPersist(NodeType.For(typeof(PhysicalAssetSpecification<>).MakeGenericType(process.DesignObject.GetType()))).Cast<ModelElement>();
				specNode.Properties[NodeTypeMember.Property.PhysicalAssetClass].Value = resource;
			}
			if (specNode != null)
			{
				specNode.Properties[NodeTypeMember.Property.HierarchyScope].Value = hierarchyScope;
				specNode.Save();
			}
			return specNode;
		}

		/// <summary>
		/// Updates the specification node to what is on the page
		/// </summary>
		/// <param name="specNode">The spec node to update</param>
		/// <param name="resource">The new attached resource</param>
		/// <returns></returns>
		private void UpdateSpecificationNode(ModelElement specNode, ModelElement resource)
		{
			if (resource.DesignObject is Equipment)
			{
				specNode.Properties[NodeTypeMember.Property.Equipment].Value = resource;
			}
			if (resource.DesignObject is EquipmentClass)
			{
				specNode.Properties[NodeTypeMember.Property.EquipmentClass].Value = resource;
			}
			if (resource.DesignObject is MaterialClass)
			{
				specNode.Properties[NodeTypeMember.Property.MaterialClass].Value = resource;
			}
			if (resource.DesignObject is MaterialDefinition)
			{
				specNode.Properties[NodeTypeMember.Property.MaterialDefinition].Value = resource;
			}
			if (resource.DesignObject is PersonnelClass)
			{
				specNode.Properties[NodeTypeMember.Property.PersonnelClass].Value = resource;
			}
			if (resource.DesignObject is PhysicalAssetClass)
			{
				specNode.Properties[NodeTypeMember.Property.PhysicalAssetClass].Value = resource;
			}
			specNode.Save();
		}

		/// <summary>
		/// Gets the model element from a connector for a given type
		/// </summary>
		/// <param name="connector">The connector to get the model element of</param>
		/// <param name="t">The type that is being looked for on the connector</param>
		/// <returns>The shape and the model element that maps to it</returns>
		private (IModellerShape, ModelElement) GetModelElementFromConnector(IModellerConnector connector, Type t)
		{
			var toOccurrence = ShapeFactory.GetOccurenceForShape(connector.ShapeTo);
			if (toOccurrence != null && toOccurrence.ModelElement.DesignObject.GetType().HasInterface(t))
			{
				return (connector.ShapeTo, toOccurrence.ModelElement);
			}
			var fromOccurrence = ShapeFactory.GetOccurenceForShape(connector.ShapeFrom);
			if (fromOccurrence != null && fromOccurrence.ModelElement.DesignObject.GetType().HasInterface(t))
			{
				return (connector.ShapeFrom, fromOccurrence.ModelElement);
			}
			return (null,null);
		}

		/// <summary>
		///     Handles the connection removed.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerShapeConnectionEventArgs" /> instance containing the event data.</param>
		private void HandleConnectionRemoved(object sender, ModellerShapeConnectionEventArgs e)
		{
			if (e.Connector.ConnectionOccurrenceId == null)
			{
				return;
			}
			// Always just delete the connection occurrence, doesn't matter if it's incomplete or moved, connection added will create the correct instance
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var occurrence = scope.FindById<ConnectorOccurrence>(e.Connector.ConnectionOccurrenceId);
				occurrence?.Page.RemoveOccurrence(occurrence);
				if (occurrence != null)
				{
					scope.Delete(occurrence);
				}
				occurrence?.Page.Update();
				occurrence?.ModelElement.Update();
				scope.Done();
			}
		}

		/// <summary>
		///     Handles the document created.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerDocumentEventArgs" /> instance containing the event data.</param>
		private void HandleDocumentCreated(object sender, ModellerDocumentEventArgs e)
		{
			_supressDocumentAlignment = true;
			var filter = new ModelElementTreeFilter(targetNodeType: NodeType.For(typeof(HierarchyScope)), additionalVisibleNodes: new [] { NodeType.For(typeof(Model))}, permittedOperations: new [] { OperationType.AddNew });
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var rootNodes = scope.Query<RootNode>().Single();//.NonEmptyNonSystemFolders.SelectMany(f => f.Nodes).Where(n => n.Type == NodeType.For(typeof(HierarchyScope))).Select(n => n.Id);
				var form = _hierarchyScopeSelectionTreeViewForm ?? _modeller.CreateModellerTreeViewForm(new [] { rootNodes.Id }, filter);
				if (ModellerDialogResult.OK == form.ShowDialog())
				{
						// Form has the selected Hierarcy Scope
						var model = scope.FindById<DefinitionNode>(form.SelectedNodeId).AddNewAndPersist(NodeType.For(typeof(Model)), form.NewModelName);
						model.Save();
						e.Document.ModelInternalId = model.Id;
						e.Document.Title = model.DisplayId;

						foreach (var page in e.Document.Pages)
						{
							var pageNode = model.AddNewAndPersist(NodeType.For(typeof(Page)), page.Title);
							pageNode.Save();
							page.PageInternalId = pageNode.Id;
						}

						_modeller.SaveDocument(_modeller.ActiveDocument);
					
				}
				else
				{
					_modeller.ForceClose(_modeller.ActiveDocument);
				}
				scope.Done();
			}
			_supressDocumentAlignment = false;
		}

		/// <summary>
		///     Handles the document opened.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerDocumentEventArgs" /> instance containing the event data.</param>
		private void HandleDocumentOpened(object sender, ModellerDocumentEventArgs e)
		{
			if (!e.Document.IsModellerDocument)
			{
				return;
			}
			try
			{
				var model = GetModelForDocument(e.Document);
				if (model != null)
				{
					UpdateDocumentFromModel(model, e.Document);
				}
			}
			catch (Exception)
			{
				// ignored
			}
		}

		/// <summary>
		///     Handles the document save event. Saves anything that's pending
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void HandleDocumentSaved(object sender, ModellerDocumentEventArgs e)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var model = GetModelForDocument(e.Document);
				UpdateDocumentFromModel(model, _modeller.OpenDocuments.SingleOrDefault(d => d.IsModellerDocument && d.ModelInternalId == model?.Id));
				scope.Flush();
				scope.Done();
			}
		}

		/// <summary>
		///  Creates the connections on a page for the given resource
		/// </summary>
		/// <param name="resoruceOccurrence">The occurrence of the resource to create connections for</param>
		/// <param name="page">the page to create them on</param>
		/// <param name="resourceObject">The resource object to create the connections for</param>
		private void CreateConnectionsOnPageForResource(Occurrence resoruceOccurrence, IModellerPage page, ResourceDefinitionElement resourceObject)
		{
			foreach (var specification in resourceObject.LinkedSpecifications)
			{
				foreach (var processOcurrence in specification.Process.Occurrences.Where(o => o.Page.Id == page.PageInternalId))
				{
					ConnectSpecification(processOcurrence, resoruceOccurrence, specification);
				}
			}
		}

		/// <summary>
		///  Creates the connections on a page for the given process
		/// </summary>
		/// <param name="occurrence">The occurrence of the process to create connections for</param>
		/// <param name="page">the page to create them on</param>
		/// <param name="processObject">The process object to create the connections for</param>
		private void CreateConnectionsOnPageForProcess(Occurrence occurrence, IModellerPage page, ProcessDefinitionElement processObject)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				processObject.LeafSpecificationNodes.ForEach(scope.Attach);
				foreach (var specification in processObject.LeafSpecificationNodes.Where(s => s.Resources.Any(r => r.Occurrences.Any(o => o.Page.Id == page.PageInternalId))))
				{
					foreach (var resourceOccurrence in specification.Resources.SelectMany(r => r.Occurrences.Where(o => o.Page.Id == page.PageInternalId)))
					{
						ConnectSpecification(occurrence, resourceOccurrence, specification);
					}
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Gets the direction for the specification
		/// </summary>
		/// <param name="specification">The specification to connect</param>
		/// <param name="shapeTo">Optional, the shape that's being connected to</param>
		/// <returns></returns>
		private ConnectionDirection GetConnectionDirection(SpecificationDefinitionElement specification, IModellerShape shapeTo = null)
		{
			var direction = ConnectionDirection.None;
			if (specification.Type.Configuration.Properties.ContainsKey(NodeTypeMember.Property.MaterialUse) && specification.Properties[NodeTypeMember.Property.MaterialUse].Value != null)
			{
				ExtensibleEnum<MaterialUse> materialUse = (ExtensibleEnum<MaterialUse>)specification.Properties[NodeTypeMember.Property.MaterialUse].Value;
				if (materialUse == MaterialUse.Consumed)
				{
					direction = shapeTo?.ModelElementId == specification.Process.Id 
						? ConnectionDirection.Forward 
						: ConnectionDirection.Reverse;
				}
				if (materialUse == MaterialUse.Produced)
				{
					direction = shapeTo?.ModelElementId == specification.Process.Id
						? ConnectionDirection.Reverse
						: ConnectionDirection.Forward;
				}
			}
			return direction;
		}

		private void ConnectSpecification(Occurrence processOccurrence, Occurrence resourceOccurrence, SpecificationDefinitionElement specification)
		{
			var direction = GetConnectionDirection(specification);
			_modeller.Connect(resourceOccurrence, processOccurrence, direction);
		}

		/// <summary>
		///     Updates the shape's model object's ID
		/// </summary>
		/// <param name="shape"></param>
		public void UpdateModelObjectExternalIdFromShape(IModellerShape shape)
		{
			if ((!_modeller.ActiveDocument?.IsModellerDocument ?? true) || !shape.IsModellerShape())
			{
				return;
			}
			if (DragInOperation)
			{
				return;
			}
			if (!_modeller.ActiveDocument?.IsModellerDocument ?? true)
			{
				return;
			}
			var modelObject = ShapeFactory.GetOccurenceForShape(shape)
				?.ModelElement;
			if (modelObject == null)
			{
				throw new Exception("Unable to find object for shape");
			}
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var modelElement = ShapeFactory.GetOccurenceForShape(shape)
				?.ModelElement;
				if (modelElement != null && modelElement.DisplayId != shape.IdText)
				{
					modelElement.DisplayId = shape.IdText;
					scope.Update(modelElement);
				}
				scope.Done();
			}
		}

		/// <summary>
		///     Creates and queues an undo unit for a given transaction
		/// </summary>
		/// <param name="nodeTransaction">The node transaction to create an undo unit for</param>
		/// <param name="description">A description of the event that this undo unit is for</param>
		public void CreateAndQueueUndoUnit(NodeTransaction nodeTransaction, string description)
		{
			_modeller.CreateAndQueueUndoUnit(nodeTransaction, description);
		}

		public Occurrence CreateOccurrenceAndMapShape(IModellerShape shape, ModelElement target)
		{
			Occurrence occurrence = null;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				if (shape.Page.Document?.IsModellerDocument ?? false)
				{
					var page = GetPageForModellerPage(shape.Page);
					if (page != null)
					{
						occurrence = page.AddElementAsObject(target);
						ShapeFactory.MapOccurrenceToShape(occurrence, shape);
					}
				}
				else
				{
					ShapeFactory.MapModelElementToShape(target, shape);
				}
				scope.Done();
			}
			return occurrence;
		}

		public IModellerShape CreateNewShapeAndMapOccurrence(BrowserNode target)
		{
			var shape = ShapeFactory.CreateNewShapeOffPage(target.Cast<ModelElement>(), this);
			CreateOccurrenceAndMapShape(shape, target.Cast<ModelElement>());
			return shape;
		}

		/// <summary>
		/// Select the given shapes and return it as a selection
		/// </summary>
		/// <param name="shapes">The shapes to select</param>
		/// <returns></returns>
		public IModellerSelection SelectShapes(IEnumerable<IModellerShape> shapes)
		{
			return _modeller.SelectShapes(shapes);
		}

		/// <summary>
		/// Creates the connections for the given occurrences
		/// </summary>
		/// <param name="occurrenceIds">The IDs of the occurrences to create connections for</param>
		public void CreateConnectionsForOccurrences(IEnumerable<Guid?> occurrenceIds)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				foreach (var id in occurrenceIds)
				{
					var occurrece = scope.FindById<ObjectOccurrence>(id);
					var page = _modeller.ActivePage;
					if (page.PageInternalId != occurrece.Page.Id)
					{
						throw new NotImplementedException();
					}
					CreateConnectionsForOccurrence(occurrece, page);
				}
				scope.Done();
			}
		}

		public void LayoutOccurrences(IEnumerable<Guid?> occurrenceIds)
		{
			if (SupressSelectionChange)
			{
				return;
			}
			var selection = _modeller.SelectShapes(_modeller.ActivePage.Shapes.Where(s => occurrenceIds.Contains(s.OccurrenceId)));
			if (selection.Count() > 1)
			{
				selection.AutoArrange();
			}
		}

		/// <summary>
		/// Updates the model object (and pages) from a given document
		/// </summary>
		/// <param name="document">The document to update the model to reflect</param>
		public void UpdateModelFromDocument(IModellerDocument document)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var model = GetModelForDocument(document);
				if (model != null)
				{
					model.DisplayId = document.Title;
					document.Pages.ForEach(UpdateModelFromPage);
					// remove pages that no longer exist
					model.Descendants.Where(n => n.Type == NodeType.For(typeof(Page))
											  && !document.Pages.Select(p => p.PageInternalId).Contains(n.Id))
									 .ForEach(n => n.Delete());
					model.Update();
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Updates a model page (and occrrences) from the page
		/// </summary>
		/// <param name="page">The page to update from</param>
		public void UpdateModelFromPage(IModellerPage page)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var model = GetPageForModellerPage(page);
				if (model != null)
				{
					model.DisplayId = page.Title;
					page.Shapes.ForEach(UpdateOccurrenceFromShape);
					// remove occurrences that no longer exist
					model.Occurrences.Where(o => !page.Shapes.Select(p => p.OccurrenceId).Contains(o.Id)).ForEach(model.RemoveOccurrence);
					model.Update();
				}
				else
				{
					// TODO should create error
					//page.Delete();
				}

				scope.Done();
			}
		}

		/// <summary>
		/// Updates an occurrence from a shape
		/// </summary>
		/// <param name="shape">The shape to update from</param>
		public void UpdateOccurrenceFromShape(IModellerShape shape)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var occurrece = scope.FindById<ObjectOccurrence>(shape.OccurrenceId);
				if (occurrece == null)
				{
					var modelElement = scope.FindById<ModelElement>(shape.ModelElementId);
					if (modelElement != null)
					{
						var page = scope.FindById<Page>(shape.Page.PageInternalId);
						occurrece = page.AddElementAsObject(modelElement);
					}
					
				}
				if (occurrece != null && !occurrece.ModelElement.IsDeleted)
				{
					ShapeFactory.MapOccurrenceToShape(occurrece, shape);
				} else
				{
					shape.ErrorMessage = "Cannot find object for shape";
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Updates a document (and it's pages) from a given model. 
		/// If the document is not currently open then nothing should occur and it will be updated next time it is opened.
		/// </summary>
		/// <param name="model">The model to update from</param>
		/// <param name="document">The document to update to</param>
		public void UpdateDocumentFromModel(Model model, IModellerDocument document)
		{
			if (document != null)
			{
				using (var scope = new PersistenceScope(TransactionOption.Required))
				{
					scope.Attach(model);
					if (model.DisplayId != document.Title)
					{
						document.Title = model.DisplayId;
						_modeller.SaveDocument(document);
					}
					// Add new pages
					model.Descendants.Where(d => d.Type == NodeType.For(typeof(Page)) && !document.Pages.Select(p => p.PageInternalId).Contains(d.Id))
						.ForEach(p => document.AddPage(p.As<Page>()));
					// Update all pages
					model.Descendants.Where(d => d.Type == NodeType.For(typeof(Page))).Select(n => n.TargetObject).Cast<Page>().ForEach(p => UpdatePageFromModel(p, document?.Pages.SingleOrDefault(dp => dp.PageInternalId == p.Id)));
					// remove deleted pages
					// TODO should create error
					//document.Pages
					//	.Where(p => !model.Descendants.Where(d => d.Type == NodeType.For(typeof(Page)))
					//	.Select(n => n.TargetObject).Cast<Page>()
					//	.Select(m => m.Id).Contains(p.PageInternalId))
					//	.ForEach(p => p.Delete());
					scope.Done();
				}
			}
		}

		/// <summary>
		/// Updates a page (and it's shapes) from the model page
		/// </summary>
		/// <param name="model">The page to update from</param>
		/// <param name="page">The page to update to</param>
		public void UpdatePageFromModel(Page model, IModellerPage page)
		{
			if (page != null)
			{
				using (var scope = new PersistenceScope(TransactionOption.Required))
				{
					page.Title = model.DisplayId;

					foreach (var shape in page.Shapes.Where(s => s.IsModellerShape()))
					{
						UpdateShapesFromOccurrence(shape);
					}
					foreach (var connector in page.Connectors.Where(c => c.ConnectionOccurrenceId != null))
					{
						UpdateConnectorFromOccurrence(connector);
					}
					scope.Done();
				}
			}
		}

		/// <summary>
		/// Updates a connector to it's occurence information
		/// </summary>
		/// <param name="connector">The connector to update</param>
		private void UpdateConnectorFromOccurrence(IModellerConnector connector)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var connectorOccurrence = scope.FindById<ConnectorOccurrence>(connector.ConnectionOccurrenceId);
				if (connectorOccurrence == null)
				{
					var pageModel = scope.FindById<Page>(connector.ModellerPage.PageInternalId);
					var occurrenceFrom = scope.FindById<ObjectOccurrence>(connector.ShapeFrom.OccurrenceId);
					var occurrenceTo = scope.FindById<ObjectOccurrence>(connector.ShapeFrom.OccurrenceId);
					var modelElement = scope.FindById<ModelElement>(connector.ModelElementId);
					if (modelElement == null)
					{
						scope.Done();
						return;
					}
					connectorOccurrence = pageModel.AddElementAsConnector(modelElement, occurrenceFrom, occurrenceTo);
				}
				MapConection(connector, connectorOccurrence);
				scope.Done();
			}
		}

		/// <summary>
		/// Updates a shape from it's occurrence
		/// </summary>
		/// <param name="shape">The shape to update</param>
		private void UpdateShapesFromOccurrence(IModellerShape shape)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				ObjectOccurrence occurrence = null;
				if (shape.OccurrenceId != null)
				{
					occurrence = scope.FindById<ObjectOccurrence>(shape.OccurrenceId);
				}
				if (occurrence == null)
				{
					var modelElement = scope.FindById<ModelElement>(shape.ModelElementId);
					if (modelElement == null)
					{
						shape.ErrorMessage = "Cannot find object for shape";
						scope.Done();
						return;
					}
					var page = scope.FindById<Page>(shape.Page.PageInternalId);
					occurrence = page.AddElementAsObject(modelElement);
				}
				shape.ErrorMessage = "";
				ShapeFactory.MapOccurrenceToShape(occurrence, shape);
				scope.Done();
			}
		}

		/// <summary>
		/// Opens a model
		/// </summary>
		/// <param name="modelId">The ID of the model to open</param>
		public void OpenModel(Guid modelId)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var model = scope.FindById<Model>(modelId);
				_modeller.OpenModel(model);
				scope.Done();
			}
		}

		/// <summary>
		///     Updates the shape's model object's description
		/// </summary>
		/// <param name="shape"></param>
		public void UpdateModelObjectDescriptionFromShape(IModellerShape shape)
		{
			if ((!_modeller.ActiveDocument?.IsModellerDocument ?? true) || !shape.IsModellerShape() || shape.OccurrenceId == null)
			{
				return;
			}
			if (DragInOperation)
			{
				return;
			}
			if (!_modeller.ActiveDocument?.IsModellerDocument ?? true)
			{
				return;
			}
			var modelElement = ShapeFactory.GetOccurenceForShape(shape)
				?.ModelElement;
			if (modelElement == null)
			{
				throw new Exception("Unable to find object for shape");
			}
			var existingDescription = modelElement.Description ?? "";
			if (existingDescription != shape.DescriptionText)
			{
				using (var scope = new PersistenceScope(TransactionOption.Required))
				{
					scope.Attach(modelElement);
					modelElement.Description = shape.DescriptionText;
					scope.Save(modelElement);
					scope.Done();
				}
			}
		}

		/// <summary>
		///     Handles the selection changed.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerShapeSelectionEventArgs" /> instance containing the event data.</param>
		private void HandleSelectionChanged(object sender, ModellerShapeSelectionEventArgs e)
		{
			if (DragInOperation || SupressSelectionChange)
			{
				return;
			}
			// get the selected shape, and display its properties
			// if multiple shapes are selected, the first one's properties are displayed
			// first shape in the list is the first shape selected in the diagram, i.e the first shape that the selection area encloses is always placed first
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				SupressSelectionChange = true;
				var modelElements = new List<ModelElement>();
				foreach (var shape in e.SelectedModellerShapes.ToArray())
				{
					if (shape.ModelElementId == null)
					{
						continue;
					}
					var modelElement = scope.FindById<ModelElement>(shape.ModelElementId);

					// modeller object is not null
					if (modelElement != null)
					{
						modelElements.Add(modelElement);
					}
				}
				_modeller.SelectObjects(modelElements);
				SupressSelectionChange = false;
				scope.Done();
			}
		}

		/// <summary>
		///     Handles the shape added.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerShapeEventArgs" /> instance containing the event data.</param>
		/// <exception cref="Exception">Object must be dropped inside a Hierarchy Scope Shape</exception>
		private void HandleShapeAdded(object sender, ModellerShapeEventArgs e)
		{
			if (!ShapeFactory.IsModellerShape(e.ModellerShape) || DragInOperation || (!_modeller.ActiveDocument?.IsModellerDocument ?? true) || e.ModellerShape.IsDeleted)
			{
				return;
			}

			// create scope to join with inner scopes when getting the occurrence
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var originalOccurrence = ShapeFactory.GetOccurenceForShape(e.ModellerShape);
				Occurrence currentOccurrence = originalOccurrence;
				if (originalOccurrence == null)
				{
					var modellerObject = MapNewModelObjectToShape(e.ModellerShape, e.ModellerPage);
					if (modellerObject != null)
					{
						currentOccurrence = CreateOccurrenceAndMapShape(e.ModellerShape, modellerObject);
					}
				}
				else
				{
					ShapeFactory.MapOccurrenceToShape(originalOccurrence, e.ModellerShape);
				}

				if (currentOccurrence != null)
				{
					CreateConnectionsForOccurrence(currentOccurrence, e.ModellerPage);
				}
				scope.Done();
			}
		}

		/// <summary>
		/// Creates the connections for a given occurrence
		/// </summary>
		/// <param name="occurrence"></param>
		/// <param name="page"></param>
		private void CreateConnectionsForOccurrence(Occurrence occurrence, IModellerPage page)
		{
			if (occurrence.ModelElement.Is<ProcessDefinitionElement>())
			{
				CreateConnectionsOnPageForProcess(occurrence, page, occurrence.ModelElement.Cast<ProcessDefinitionElement>());
			}
			if (occurrence.ModelElement.Is<ResourceDefinitionElement>())
			{
				CreateConnectionsOnPageForResource(occurrence, page, occurrence.ModelElement.Cast<ResourceDefinitionElement>());
			}
		}

		/// <summary>
		///     Gets the page domain object for a modeller page
		/// </summary>
		/// <param name="modellerPage">The age to </param>
		/// <returns></returns>
		private Page GetPageForModellerPage(IModellerPage modellerPage)
		{
			Page page;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				page = scope.FindById<Page>(modellerPage.PageInternalId);
				scope.Done();
			}
			return page;
		}

		/// <summary>
		///     Handles the shape deleted.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="ModellerShapeEventArgs" /> instance containing the event data.</param>
		private void HandleShapeDeleted(object sender, ModellerShapeEventArgs e)
		{
			if (!ShapeFactory.IsModellerShape(e.ModellerShape) || DragInOperation || _deleteInProgress || e.ModellerShape.IsDeleted || e.ModellerShape.OccurrenceId == null)
			{
				return;
			}
			// Flag to prevent delete being called when corresponding connectors are deleted
			_deleteInProgress = true;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var occurrece = scope.FindById<ObjectOccurrence>(e.ModellerShape.OccurrenceId);
				var page = GetPageForModellerPage(e.ModellerPage);
				if (occurrece != null)
				{
					// Remove any connector occurrences
					foreach (var o in page.Occurrences.Where(o => o != occurrece && o is ConnectorOccurrence && (((ConnectorOccurrence) o).From == occurrece || ((ConnectorOccurrence) o).To == occurrece)).ToArray())
					{
						e.ModellerDocument.Shapes.Where(s => s.OccurrenceId == o.Id).ForEach(s => s.Delete());
						page?.RemoveOccurrence(o);
						o.ModelElement.Update();
					}
					page?.RemoveOccurrence(occurrece);
				}
				page.Update();
				occurrece?.ModelElement.Update();
				scope.Done();
			}
			_deleteInProgress = false;
		}

		/// <summary>
		///     Handles the shut down.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		private void HandleShutDown(object sender, EventArgs e)
		{
			// detach and dispose
			_modeller.ShapeAdded -= HandleShapeAdded;
			_modeller.ShutDown -= HandleShutDown;
			_modeller.ShapeAdded -= HandleShapeAdded;
			_modeller.ShapeDeleted -= HandleShapeDeleted;
			_modeller.SelectionChanged -= HandleSelectionChanged;
			_modeller.ShutDown -= HandleShutDown;
			_modeller.DocumentCreated -= HandleDocumentCreated;
			_modeller.DocumentOpened -= HandleDocumentOpened;
			_modeller.DocumentSaved -= HandleDocumentSaved;
			_modeller.ConnectionAdded -= HandleConnectionAdded;
			_modeller.ConnectionRemoved -= HandleConnectionRemoved;
		}

		/// <summary>
		///     Maps the new modeller object to shape.
		/// </summary>
		/// <param name="shape">The shape.</param>
		/// <exception cref="Exception">Unable to determine the Hierarchy Scope required for the shape</exception>
		private ModelElement MapNewModelObjectToShape(IModellerShape shape, IModellerPage page)
		{
			ArgumentValidation.AssertNotNull(shape, nameof(shape));

			// step 1 try load the hierarchy scope the object has been dropped in
			ModelElement constructedObject = null;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				ModelElement hierarchyScope = GetHeirarchyScopeForShape(shape);
				var parentShape = shape.FirstLevelParent(shape.MasterName);
				if (parentShape == null && shape.IsOfMaster(MasterShape.OperationsSegmentMasterShape))
				{
					parentShape = shape.FirstLevelParent(MasterShape.OperationsDefinitionMasterShape);
				}
				ModelElement parent = null;
				if (parentShape != null)
				{
					parent = scope.FindById<ModelElement>(parentShape.ModelElementId);
				}

				if (hierarchyScope != null)
				{
					// Need to create new object of the required
					var nodeType = ShapeFactory.GetDomainTypeFromShape(shape);
					BrowserFolder targetFolder;
					if (nodeType == NodeType.For(typeof(HierarchyScope)))
					{
						targetFolder = hierarchyScope.PrimaryFolder;
					} else { 
						targetFolder= parent == null
							? hierarchyScope.NonSystemFolders.FirstOrDefault(f => f.PermittedTypes.Contains(nodeType))
							: parent.NonSystemFolders.FirstOrDefault(f => f.PermittedTypes.Contains(nodeType));
					}
					if (targetFolder != null)
					{
						if (!nodeType.Configuration.Initialiser.Properties.Any())
						{
							constructedObject = ConstructObject(targetFolder, nodeType).As<ModelElement>();
						}
						else if (nodeType.Configuration.Initialiser.Properties.Count() == 1 && nodeType.Configuration.Initialiser.Properties.Single().MemberId == NodeTypeMember.Property.EquipmentLevel)
						{
							EquipmentLevel level;
							if (Enum.TryParse(hierarchyScope.Properties[NodeTypeMember.Property.EquipmentLevel].Value?.ToString(), out level))
							{
								var targetEquipmentLevel = level.DefaultChild();
								constructedObject = ConstructObject(targetFolder, nodeType, targetEquipmentLevel).As<ModelElement>();
							}
						}
						else
						{
							throw new NotImplementedException();
						}
						CreateOccurrenceAndMapShape(shape, constructedObject);
					}
					else
					{
						// Todo this should be done differently
						_modeller.DisplayMessage($"{shape.MasterName} cannot be created as a direct child of a hierarchy scope or any object(s) in which the object has been placed.");
						shape.Delete();
					}
				}
				scope.Done();
			}
			return constructedObject;
		}
		/// <summary>
		/// Get the hierarchy scope for a given shape. This will either the be the Hierarchy Scope it is dropped into or the Hierarchy Scope of the document itself
		/// </summary>
		/// <param name="shape">The shape to find the Hierarchy Scope of</param>
		/// <returns></returns>
		private ModelElement GetHeirarchyScopeForShape(IModellerShape shape)
		{
			ModelElement hierarchyScope = null;

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Start with the hierarchy scope of the shape
				var hierarchyScopeShape = shape.FirstLevelParent(MasterShape.HierarchyScopeMasterShape);
				if (hierarchyScopeShape != null)
				{
					hierarchyScope = scope.FindById<ObjectOccurrence>(hierarchyScopeShape.OccurrenceId)?.ModelElement;
				}
				// if not found, get the hierarchy scope from the model
				if (hierarchyScope == null)
				{
					var model = GetModelForDocument(_modeller.ActiveDocument);
					if (model != null)
					{
						hierarchyScope = model.ParentNode.Cast<ModelElement>();
					}
				}
				// Special case, hierarchy scope for a hierarchy scope being created within a work unit needs to take the work unit's parent scope
				if (shape.IsOfMaster(MasterShape.HierarchyScopeMasterShape) && ((HierarchyScope) hierarchyScope?.DesignObject)?.EquipmentLevel == EquipmentLevel.WorkUnit)
				{
					hierarchyScope = hierarchyScope.ParentNode.Cast<ModelElement>();
				}

				// still nothing? now we throw our toys and spit the dummy
				if (hierarchyScope == null)
				{
					// TODO handle this correctly. For now ignoring as this throws issues when coping shapes that we don't know about
					//throw new Exception(Messages.UnableToDetermineTheHierarchyScopeRequiredForTheShape);
				}
				scope.Done();
			}
			return hierarchyScope;
		}

		/// <summary>
		///     Constructs an object for the given target
		/// </summary>
		/// <param name="target">The target folder</param>
		/// <param name="nodeType">The typ of node to create</param>
		/// <param name="equipmentLevel">The equipment level if the target is a Hierarchy Scope, Equipment or Equipment Class</param>
		private BrowserNode ConstructObject(BrowserFolder target, NodeType nodeType, EquipmentLevel? equipmentLevel = null)
		{
			BrowserNode result;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				//var target = scope.FindById<ParentNode>(targetFolderId.ParentNodeId).AllFolders.Single(f => f.MemberId == targetFolderId.FolderMemeberId);
				if (nodeType.Id == NodeTypeId.Model || nodeType.Id == NodeTypeId.Page)
				{
					// we need to create the model or open the model and add the page
					throw new NotImplementedException();
				}
				using (var transaction = NodeTransaction.Begin())
				{
					result = target.Parent.AddNewAndPersist(nodeType,
						target,
						equipmentLevel != null
							? new object[] { equipmentLevel }
							: null);
					transaction.Complete();
				}
				scope.Done();
				return result;
			}
		}

		/// <summary>
		/// Triggered when a node property is set, update the occurrences
		/// </summary>
		/// <param name="node"></param>
		/// <param name="memberId"></param>
		/// <param name="formerValue"></param>
		public override void NodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
		{
			AlignOpenDocumentsToNode(node);
		}

		private void AlignOpenDocumentsToNode(BrowserNode node)
		{
			if (_supressDocumentAlignment)
			{
				return;
			}
			if (node.Is<Page>())
			{
				var document = _modeller.OpenDocuments.SingleOrDefault(d => d.IsModellerDocument && d.ModelInternalId == node.ParentNode?.Id);
				UpdatePageFromModel(node.As<Page>(), document?.Pages.SingleOrDefault(p => p.PageInternalId == node.Id));
				return;
			}
			if (node.Is<Model>())
			{
				UpdateDocumentFromModel(node.As<Model>(), _modeller.OpenDocuments.SingleOrDefault(d => d.IsModellerDocument && d.ModelInternalId == node?.Id));
				return;
			}
			var modelElement = node.As<ModelElement>();
			if (modelElement == null)
			{
				return;
			}
			_modeller.OpenDocuments.SelectMany(d => d.Shapes.Where(s => s.ModelElementId == modelElement.Id)).ForEach(UpdateShapesFromOccurrence);
			_modeller.OpenDocuments.SelectMany(d => d.Connectors.Where(s => s.ModelElementId == modelElement.Id)).ForEach(UpdateConnectorFromOccurrence);
		}

		/// <summary>
		/// Invoked when a new node is created.
		/// </summary>
		/// <param name="newNode">The newly created node.</param>
		public override void NodeCreated(BrowserNode newNode)
		{
			if (newNode.ParentNode != null)
			{
				AlignOpenDocumentsToNode(newNode.ParentNode);
			}
		}

		/// <summary>
		/// Invoked when a node is deleted.
		/// </summary>
		/// <param name="deletedNode">The deleted node.</param>
		/// <param name="deletedFrom">The folder from which the node was deleted.</param>
		public override void NodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
		{
			if (deletedNode is ModelElement modelElement)
			{
				_modeller.OpenDocuments.SelectMany(d => d.Pages)
					.SelectMany(p => p.Shapes)
					.Where(s => s.ModelElementId == modelElement.Id)
					.ForEach(s => s.Delete());
			}
			if (deletedNode.ParentNode != null)
			{
				AlignOpenDocumentsToNode(deletedNode.ParentNode);
			}
		}

		/// <summary>
		/// Invoked when a new node is added to it's new parent.
		/// </summary>
		/// <param name="newNode">The newly added node.</param>
		public override void NodeAddedNew(BrowserNode newNode)
		{
			if (newNode.ParentNode != null)
			{
				AlignOpenDocumentsToNode(newNode.ParentNode);
			}
		}

		#endregion

		#region  Fields

		/// <summary>
		///     The modeller
		/// </summary>
		private readonly IModeller _modeller;

		/// <summary>
		///     The tree view form
		///     IMPORTANT - this is here to supporty IOC for testing. Otherwise we cannot test the methods without getting popups
		///     This is the Treeviewform that is used to select a Hierarchy Scope
		/// </summary>
		private readonly ITreeViewForm _hierarchyScopeSelectionTreeViewForm;
		
		/// <summary>
		/// The delete is in process
		/// </summary>
		private bool _deleteInProgress;

		/// <summary>
		/// Some actions will trigger other actions before we're ready for them so in some cases we need to supress the document alignment
		/// </summary>
		private bool _supressDocumentAlignment;

		/// <summary>
		/// Some actions will trigger other actions before we're ready for them so in some cases we need to supress selection change
		/// </summary>
		public bool SupressSelectionChange { get; set; }

		/// <summary>
		///     Gets or sets a value indicating whether [drag in operation].
		/// </summary>
		/// <value><c>true</c> if [drag in operation]; otherwise, <c>false</c>.</value>
		public bool DragInOperation { get; set; }

		/// <summary>
		///     The serialisation helper to be used for exporting models
		/// </summary>
		public ISerializationHelper SerializationHelper { get; set; }

		/// <summary>
		///     The shape mapper factory
		/// </summary>
		/// <value>The shape factory.</value>
		public IShapeFactory ShapeFactory { get; }

		/// <summary>
		/// The currently active document
		/// </summary>
		public IModellerDocument ActiveDocument => _modeller.ActiveDocument;

		/// <summary>
		/// The currently active page
		/// </summary>
		public IModellerPage ActivePage => _modeller.ActivePage;

		/// <summary>
		///     Determines if a model is accessible
		/// </summary>
		/// <param name="model"></param>
		/// <returns></returns>
		public bool IsModelAccessible(Model model)
		{
			return SerializationHelper.FileExists(FilePathFactory.GetModelFilePath(model));
		}

		/// <summary>
		///     Creates a new ModellerTreeView
		/// </summary>
		/// <param name="modelElementTreeFilter">The filter to apply</param>
		/// <param name="rootNodeIds">The root nodes for the tree</param>
		/// <param name="allowMultipleSelection">Whether or not multiple selections are allowed.</param>
		/// <returns></returns>
		public IModellerTreeView CreateObjectBrowserView(ModelElementTreeFilter modelElementTreeFilter, IEnumerable<Guid> rootNodeIds, bool allowMultipleSelection = true)
		{
			return _modeller.CreateObjectBrowserView(modelElementTreeFilter, null, allowMultipleSelection, rootNodeIds);
		}

		/// <summary>
		///     Creates a new simple form with the given titles
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		public ISimpleForm CreateSimpleForm(string title)
		{
			return _modeller.CreateSimpleForm(title);
		}

		#endregion
	}
}