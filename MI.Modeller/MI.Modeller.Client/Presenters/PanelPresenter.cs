﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Framework.Validation;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	///     Class for the presenter for the panel view containing the Hierarchy Tree
	/// </summary>
	public class PanelPresenter : IPanelPresenter
    {
        #region  Fields

        private readonly IPanelView _view;

        #endregion

        #region Constructors

        public PanelPresenter(IPanelView view)
        {
            _view = ArgumentValidation.AssertNotNull(view,
                nameof(view));
        }

        #endregion

        #region  Interface Implementations

        public void ShowNodeProperties(IEnumerable<Guid> treeNodeIds)
        {
	        using (var scope = new PersistenceScope(TransactionOption.Required))
	        {
		        var objectBrowserNodes = scope.Query<BrowserNode>().Where(n => treeNodeIds.Contains(n.Id));
		        if (objectBrowserNodes.Count() == 1)
		        {
			        var targetObject = objectBrowserNodes.First().As<ModelElement>() ?? objectBrowserNodes.First().As<LinkNode>()?.LinkedDefinitionNode.As<ModelElement>();
			        if (targetObject == null)
			        {
				        return;
			        }
					_view.SetupPropertyPane(targetObject);
		        }
		        else
		        {
			        _view.HidePropertyPane();
		        }
				scope.Done();
	        }
        }

        #endregion
    }
}