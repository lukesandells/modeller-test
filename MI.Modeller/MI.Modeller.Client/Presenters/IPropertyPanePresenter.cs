﻿using System;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Configuration;
using MI.Modeller.Domain;

namespace MI.Modeller.Client.Presenters
{
	/// <summary>
	/// Presenter for the property pane
	/// </summary>
	public interface IPropertyPanePresenter
	{
		/// <summary>
		/// Update the given property of a model element
		/// </summary>
		/// <param name="modelElementId">The id of the model element to update</param>
		/// <param name="property">The property to update</param>
		/// <param name="value">The updated value</param>
		/// <returns>The validation result of the update</returns>
		string UpdateProperty(Guid modelElementId, PropertyConfiguration property, object value);
	}
}