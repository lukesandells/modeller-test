using System;
using MI.Modeller.Client.Properties;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;

namespace MI.Modeller.Client.Presenters {
	public class FilePathFactory {

		private const string C_VISIO_DOCUMENT_EXTENSION = ".vsdx";
		/// <summary>
		/// Gets the path for a given document
		/// </summary>
		/// <returns></returns>
		public static string GetModelFilePath(Model model)
		{
			return $"{GetHierarchyScopeFolder((HierarchyScope) model.ParentNode.TargetObject)}\\{model.DisplayId}{C_VISIO_DOCUMENT_EXTENSION}";
		}

		private static string GetHierarchyScopeFolder(HierarchyScope hierarchyScope)
		{
			if (hierarchyScope.Parent == null)
			{
				return
					Environment.ExpandEnvironmentVariables(
						$"{Settings.Default.WorkspaceDirectory}\\{hierarchyScope.ExternalId}");
			}
			return
				Environment.ExpandEnvironmentVariables(
					$"{GetBaseHierarchyScopePath(hierarchyScope.Parent)}\\{hierarchyScope.ExternalId}");
		}

		/// <summary>
		///     The base file path for a given Hierarchy Scope
		/// </summary>
		/// <param name="hierarchyScope"></param>
		/// <returns></returns>
		public static string GetBaseHierarchyScopePath(HierarchyScope hierarchyScope)
		{
			return $"{GetHierarchyScopeFolder(hierarchyScope)}\\{hierarchyScope.ExternalId}";
		}
	}
}