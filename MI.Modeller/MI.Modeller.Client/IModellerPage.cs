﻿using System;
using System.Collections.Generic;

namespace MI.Modeller.Client
{
	/// <summary>
	///     Modeller interface for interacting with pages within a document
	/// </summary>
	public interface IModellerPage
	{
		#region  Fields/Properties

		/// <summary>
		///     The modeller document this page is in
		/// </summary>
		IModellerDocument Document { get; }

		/// <summary>
		///     The internal id of the page
		/// </summary>
		Guid PageInternalId { get; set; }

		IEnumerable<IModellerShape> Shapes { get; }
		IEnumerable<IModellerConnector> Connectors { get; }

		/// <summary>
		///     The title of the page
		/// </summary>
		string Title { get; set; }

		#endregion

		#region Other Methods

		/// <summary>
		///     Deletes this page
		/// </summary>
		void Delete();

		#endregion
	}
}