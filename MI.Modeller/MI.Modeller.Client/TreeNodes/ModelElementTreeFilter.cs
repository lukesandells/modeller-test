﻿using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Framework.Persistence;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using System.Linq;
using MI.Framework.Validation;

namespace MI.Modeller.Client.TreeNodes
{
	/// <summary>
	///     Tree filter specifically for ModelElements.
	/// </summary>
	public class ModelElementTreeFilter
    {
		#region  Fields

		/// <summary>
	    /// The HierarchyScope to filter the tree to
	    /// </summary>
	    private readonly HierarchyScope _hierarchyScopeToFilter;

	    /// <summary>
	    /// The target node type to filter to
	    /// </summary>
	    public readonly NodeType TargetNodeType;

		/// <summary>
		/// The permitted operations for this filter
		/// </summary>
	    public readonly IEnumerable<OperationType> PermittedOperations;

		/// <summary>
		/// Additional nodes types which will be visible
		/// </summary>
		private readonly IEnumerable<NodeType> _additionalVisibleNodes;

	    #endregion

		#region Constructors

	    /// <summary>
	    /// Initializes a new instance of the <see cref="ModelElementTreeFilter" /> struct.
	    /// </summary>
	    /// <param name="targetNodeType">The target node type to filter to</param>
	    /// <param name="hierarchyScopeToFilter">The HierarchyScope to filter the tree to</param>
	    /// <param name="additionalVisibleNodes">Additional nodes types which will be visible</param>
	    /// <param name="permittedOperations">Ther permitted operations</param>
	    public ModelElementTreeFilter(NodeType targetNodeType, HierarchyScope hierarchyScopeToFilter = null, IEnumerable<NodeType> additionalVisibleNodes = null, IEnumerable<OperationType> permittedOperations = null)
	    {
		    PermittedOperations = permittedOperations;
		    _additionalVisibleNodes = additionalVisibleNodes;
			_hierarchyScopeToFilter = hierarchyScopeToFilter;
		    TargetNodeType = ArgumentValidation.AssertNotNull(targetNodeType, nameof(targetNodeType));
	    }

		#endregion

		#region Members

		/// <summary>
		/// Indicates whether or not a node should be visisble with this filter
		/// </summary>
		/// <param name="node"></param>
		/// <returns></returns>
	    public bool IsVisible(BrowserNode node)
	    {
		    //  A node is visisble if it is selectable
		    var modelElement = node as ModelElement;
		    return modelElement != null && SelectableNodes().Any(s => s == modelElement)
				// or one of it's descendants is
				|| node.Descendants.Where(n => n.Is<ModelElement>()).Cast<ModelElement>().Any(d => SelectableNodes().Any(sn => sn == d))
				// or it's an additional type and one if it's parent is selectable
				|| ((_additionalVisibleNodes?.Any(nt => nt == node.Type) ?? false) 
					&& SelectableNodes().Any(n => n == node.ParentNode as ModelElement));
			// Contains here would read better but there is a bug within nhibernate
			// ProcessContains throws an error as it is not currently capable of correctly finding the associated range query when multiple
			// HqlRange are present in the tree.
		}

		/// <summary>
		/// Query to return all nodes which are selectable
		/// </summary>
	    public IQueryable<ModelElement> SelectableNodes()
	    {
			using (var scope = new PersistenceScope(TransactionOption.Required)) {
				// NHibernate LINQ provider doesn't support left joins, otherwise would left join to nodes
				// https://nhibernate.jira.com/browse/NH-2379

				IQueryable<DesignObject> selectableObjects = _hierarchyScopeToFilter == null 
					? scope.Query<DesignObject>() 
					: scope.Query<HierarchyScopedObject>().WhereVisibleTo(_hierarchyScopeToFilter);

				// Then narrow it down to the nodes that they contain
				var selectableNodes =
					scope.Query<ModelElement>()
					// contains would make this read nicer but see node above about bug within nhibernate
					.Where(n => selectableObjects.Any(d => d == n.DesignObject))
					.Where(n => n.TypeId == TargetNodeType.Id); // Use the ID because that's mapped
				scope.Done();
				return selectableNodes;
			}
	    }

		#endregion
	}
}