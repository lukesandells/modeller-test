﻿using System;

namespace MI.Modeller.Client
{
	/// <summary>
	/// Represents a connector between two shapes
	/// </summary>
	public interface IModellerConnector
    {
		/// <summary>
		/// The shape on one end of the connector
		/// </summary>
		IModellerShape ShapeFrom { get; }

		/// <summary>
		/// The shape on the other end of the conenctor
		/// </summary>
	    IModellerShape ShapeTo { get; }

	    /// <summary>
	    /// The occurrence ID of this connector
	    /// </summary>
	    Guid? ConnectionOccurrenceId { get; set; }

	    /// <summary>
	    /// The model element ID of this connector
	    /// </summary>
	    Guid? ModelElementId { get; set; }

		/// <summary>
		/// The page this connector is on
		/// </summary>
		IModellerPage ModellerPage { get; }

		/// <summary>
		/// The direction of this occurrence
		/// </summary>
		ConnectionDirection ConnectionDirection { set; }
	}
}
