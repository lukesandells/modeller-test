﻿using MI.Modeller.Client.MasterShapes;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace MI.Modeller.Client
{
	/// <summary>
	///     Interface for accessing shapes
	/// </summary>
	public interface IModellerShape
	{
		#region  Fields

		/// <summary>
		///     The class text that appears on the shape
		/// </summary>
		string ClassText { get; set; }

		/// <summary>
		///     The description text that appears on the shape
		/// </summary>
		string DescriptionText { get; set; }

		/// <summary>
		///     The EquipmentLevel text that appears on the shape
		/// </summary>
		string EquipmentLevelText { get; set; }

		/// <summary>
		///     Sets the ErrorMessage property on the shape
		/// </summary>
		string ErrorMessage { set; }

		/// <summary>
		///     Gets or sets a value indicating whether this instance has errors.
		/// </summary>
		/// <value><c>true</c> if this instance has errors; otherwise, <c>false</c>.</value>
		bool HasErrors { get; set; }

		/// <summary>
		///     The id text that appears on the shape
		/// </summary>
		string IdText { get; set; }

		/// <summary>
		///     Returns true if the underlying shape has been deleted
		/// </summary>
		bool IsDeleted { get; }

		/// <summary>
		///     The name of the shape's master shape
		/// </summary>
		string MasterName { get; }

		/// <summary>
		///     The occurrence ID for the shape
		/// </summary>
		Guid? OccurrenceId { get; set; }

		/// <summary>
		///     Sets the EquipmentLevel property on the shape
		/// </summary>
		string EquipmentLevel { set; }

		/// <summary>
		///  The page this shape is on
		/// </summary>
		IModellerPage Page { get; }

		/// <summary>
		/// The Id of the ModelElement this shape represents
		/// </summary>
		Guid? ModelElementId { get; set; }

		/// <summary>
		/// The type of process this shape is for
		/// </summary>
		string ProcessType { set; }

		#endregion

		#region Members

		/// <summary>
		///     Returns the shape as a data object for draging and dropping
		/// </summary>
		/// <returns></returns>
		IModellerDataObject AsDataObject();

		/// <summary>
		///     Asserts that this shape is of a given master
		/// </summary>
		/// <param name="master">The master shape to assert against</param>
		void AssertIsOfMaster(MasterShape master);

		/// <summary>
		///     Returns the Bounds of the shape as a Rect Object
		/// </summary>
		/// <returns>Rect.</returns>
		Rectangle Bounds();

		/// <summary>
		///     Gets the value of a specific cell
		/// </summary>
		/// <param name="key">The key of the cell</param>
		/// <returns></returns>
		string CellValue(string key);

		/// <summary>
		///     Delete the underlying shape
		/// </summary>
		void Delete();

		/// <summary>
		///     Gets all first level children of this shape
		/// </summary>
		/// <param name="master">The master to check against</param>
		/// <returns></returns>
		IEnumerable<IModellerShape> FirstLevelChildren(MasterShape master);

		/// <summary>
		///     Gets the lowest level parent of a given master for this shape
		/// </summary>
		/// <param name="master">The master to check against</param>
		/// <returns></returns>
		IModellerShape FirstLevelParent(MasterShape master);

		/// <summary>
		///     Gets the lowest level parent of a given master for this shape
		/// </summary>
		/// <param name="shapeMasterName">The name of the master to check against</param>
		IModellerShape FirstLevelParent(string shapeMasterName);

		/// <summary>
		///     Get an enumerations of connected connector shapes
		/// </summary>
		/// <returns></returns>
		IEnumerable<IModellerShape> GetConnectedConnectors();

		/// <summary>
		///     Gets an enumeration of the connected shapes
		/// </summary>
		/// <returns></returns>
		IEnumerable<IModellerShape> GetConnectedShapes();

		/// <summary>
		///     Determines whether [is modeller shape].
		/// </summary>
		/// <returns><c>true</c> if [is modeller shape]; otherwise, <c>false</c>.</returns>
		bool IsModellerShape();

		/// <summary>
		///     Returns if this shape is of a given master shape
		/// </summary>
		/// <param name="master">The master shape to check against</param>
		/// <returns></returns>
		bool IsOfMaster(MasterShape master);

		/// <summary>
		///     Sets a key's formaula
		/// </summary>
		/// <param name="key">The key to set</param>
		/// <param name="formula">The formula to set</param>
		void SetCellValue(string key, string formula);

		/// <summary>
		///     Sets the protected shape data value.
		///     This is for setting Shape Data that is of type Fixed List, with a single fixed Value
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="formula">The formula.</param>
		void SetProtectedShapeDataValue(string key, string formula);

		/// <summary>
		///     Gets the highest level parent of a given master for this shape
		/// </summary>
		/// <param name="master">The master to check against</param>
		/// <returns></returns>
		IModellerShape TopLevelParent(MasterShape master);

		/// <summary>
		///     Add this shape to the selection
		/// </summary>
		void Select();

		#endregion

		/// <summary>
		/// Create a connection from this shape to another shape
		/// </summary>
		/// <param name="shapeTo">The shape to connect to</param>
		/// <param name="connectionDirection">The direction to draw the arrow</param>
		void ConnectTo(IModellerShape shapeTo, ConnectionDirection connectionDirection);

		
	}
}