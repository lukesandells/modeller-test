﻿using System;
using System.Collections.Generic;
using MI.Modeller.Client.UserControls;

namespace MI.Modeller.Client.ViewInterfaces
{
	/// <summary>
	///     Event arguments for the node selection changed event
	/// </summary>
	public class NodeSelectionChangedEventArguments : EventArgs
    {
		#region  Fields

		/// <summary>
		///     The nodes that have been selected by the user
		/// </summary>
		public IEnumerable<Guid> NewSelectedNodeIds;

	    /// <summary>
	    ///     The nodes that have been selected by the user
	    /// </summary>
	    public IEnumerable<TargetFolderId> NewSelectedFolderIds;

		#endregion

		#region Constructors

		/// <summary>
		///     Creates a new NodeSelectionChangedEventArguments
		/// </summary>
		public NodeSelectionChangedEventArguments(IEnumerable<TargetFolderId> selectedFolderIds, IEnumerable<Guid> selectedNodeIds)
		{
			NewSelectedFolderIds = selectedFolderIds;
            NewSelectedNodeIds = selectedNodeIds;
        }

        #endregion
    }
}