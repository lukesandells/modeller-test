﻿using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.UserControls;

namespace MI.Modeller.Client.ViewInterfaces
{
    /// <summary>
    ///     Interface IBrowserObjectSelectorView
    /// </summary>
    public interface IBrowserObjectSelectorView : IUserControl
    {
        #region  Fields

        /// <summary>
        ///     Gets or sets the form action.
        /// </summary>
        /// <value>The form action.</value>
        FormAction FormAction { get; set; }

        /// <summary>
        ///     Gets or sets the parent.
        /// </summary>
        /// <value>The parent.</value>
        ISimpleForm ParentSimpleForm { get; set; }

        /// <summary>
        ///     Gets the selected workspace keys.
        /// </summary>
        /// <value>The selected workspace keys.</value>
        IEnumerable<BrowserNode> SelectedNodes { get; }

        /// <summary>
        ///     Gets or sets the modeller TreeView.
        /// </summary>
        /// <value>The modeller TreeView.</value>
        IModellerTreeView TreeView { get; set; }

        #endregion
    }
}