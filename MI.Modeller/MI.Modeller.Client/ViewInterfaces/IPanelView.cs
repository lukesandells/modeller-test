﻿using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Domain;

namespace MI.Modeller.Client.ViewInterfaces
{
	/// <summary>
	///     The view for the Hierarchy Scope Panel
	/// </summary>
	public interface IPanelView
    {
        #region  Fields

        /// <summary>
        ///     The tree view being presented
        /// </summary>
        IModellerTreeView TreeView { get; }

        #endregion

        #region Members

        /// <summary>
        ///     Hide the property pane
        /// </summary>
        void HidePropertyPane();

		/// <summary>
		///     Select the given nodes
		/// </summary>
		/// <param name="browserNodes">The nodes to select</param>
		void SelectObjects(IEnumerable<BrowserNode> browserNodes);

        #endregion
        /// <summary>
        /// Setup the properties panel
        /// </summary>
        /// <param name="modelElement"></param>
        void SetupPropertyPane(ModelElement modelElement);
    }
}