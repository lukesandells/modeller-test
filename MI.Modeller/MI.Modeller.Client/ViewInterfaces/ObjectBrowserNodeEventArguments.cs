﻿using MI.Framework.ObjectBrowsing;

namespace MI.Modeller.Client.ViewInterfaces
{
	/// <summary>
	///     Event arguments for BrowserNode events
	/// </summary>
	public class ObjectBrowserNodeEventArguments
    {
        #region  Fields

        /// <summary>
        ///     The Node this event is for
        /// </summary>
        public BrowserNode BrowserNode { get; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Constructs a new ObjectBrowserNodeEventArguments
        /// </summary>
        /// <param name="browserNode">The node the event is for</param>
        public ObjectBrowserNodeEventArguments(BrowserNode browserNode)
        {
            BrowserNode = browserNode;
        }

        #endregion
    }
}