﻿using System;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing;

namespace MI.Modeller.Client.ViewInterfaces
{
	public class ContextAction
	{
		public string Title { get; set; }
		public IEnumerable<ContextAction> ChildActions { get; set; }
		public Action Action { get; set; }
		public OperationType Operation { get; set; }
	}
}
