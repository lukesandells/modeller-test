﻿namespace MI.Modeller.Client
{
	/// <summary>
	/// Direction of a connection
	/// </summary>
	public enum ConnectionDirection
	{
		/// <summary>
		/// From the first side to the other
		/// </summary>
		Forward,
		/// <summary>
		/// From the second side to the first
		/// </summary>
		Reverse,
		/// <summary>
		/// No direction
		/// </summary>
		None
	}
}