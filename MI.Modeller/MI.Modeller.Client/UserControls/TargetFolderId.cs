﻿using System;

namespace MI.Modeller.Client.UserControls
{
	/// <summary>
	/// Struct for represented a selected folder in the object browser. 
	/// Created to be clear about the semantics because (Guid,Guid) is not clear as to which one is which.
	/// </summary>
	public struct TargetFolderId
	{
		/// <summary>
		/// The Id of the folder's parent node
		/// </summary>
		public Guid ParentNodeId { get; }

		/// <summary>
		/// The MemberId of the selected folder
		/// </summary>
		public Guid FolderMemeberId { get; }

		/// <summary>
		/// Constructs a new TargetFolderId
		/// </summary>
		/// <param name="parentNodeId">The Id of the folder's parent node</param>
		/// <param name="folderMemeberId">The MemberId of the selected folder</param>
		public TargetFolderId(Guid parentNodeId, Guid folderMemeberId)
		{
			ParentNodeId = parentNodeId;
			FolderMemeberId = folderMemeberId;
		}

		/// <summary>
		/// If this target folder id is equal to the other target folder id
		/// </summary>
		public bool Equals(TargetFolderId other)
		{
			return ParentNodeId.Equals(other.ParentNodeId) && FolderMemeberId.Equals(other.FolderMemeberId);
		}

		/// <summary>
		/// If this target folder id is equal to the other target folder id
		/// </summary>
		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is TargetFolderId && Equals((TargetFolderId) obj);
		}

		/// <summary>
		/// Gets a hash code for this target folder id
		/// </summary>
		public override int GetHashCode()
		{
			unchecked
			{
				return (ParentNodeId.GetHashCode() * 397) ^ FolderMemeberId.GetHashCode();
			}
		}

		/// <summary>
		/// If this target folder id is equal to the other target folder id
		/// </summary>
		public static bool operator ==(TargetFolderId a, TargetFolderId b)
		{
			return a.Equals(b);
		}

		/// <summary>
		/// If this target folder id is not equal to the other target folder id
		/// </summary>
		public static bool operator !=(TargetFolderId a, TargetFolderId b)
		{
			return !(a == b);
		}
	}
}