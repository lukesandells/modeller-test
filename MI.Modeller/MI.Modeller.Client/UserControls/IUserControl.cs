﻿namespace MI.Modeller.Client.UserControls
{
    /// <summary>
    ///     Interface for user controls
    /// </summary>
    public interface IUserControl {}
}