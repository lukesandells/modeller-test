﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.ViewInterfaces;
using System;
using System.Collections.Generic;
using MI.Modeller.Client.TreeNodes;

namespace MI.Modeller.Client.UserControls
{
	/// <summary>
	///     Interface for the tree view
	/// </summary>
	public interface IModellerTreeView : IUserControl
    {
		#region  Fields

		/// <summary>
		///     The nodes that have been selected
		/// </summary>
		IEnumerable<Guid> SelectedNodeIds { get; }

		/// <summary>
		/// The (ParentFolderID, FolderMemberId) of the selected folders
		/// </summary>
	    IEnumerable<TargetFolderId> SelectedFolderIds { get; }

		/// <summary>
		/// The filter for this tree view
		/// </summary>
	    ModelElementTreeFilter Filter { get; }

	    //IEnumerable<Guid> AllNodeIds { get; }

	    #endregion

        #region Members

        /// <summary>
        ///     Navigates to the node that defines a particular object
        /// </summary>
        /// <param name="nodeId">The ID of the node to navigate to</param>
        void NavigateToNode(Guid nodeId);

		/// <summary>
		/// Refreshes the nodes in the view
		/// </summary>
		/// <param name="updatedNodes">The nodes which have been updated</param>
		/// <param name="updatedFolders">The folders which have been updated</param>
		void RefreshNodes(IEnumerable<BrowserNode> updatedNodes = null, IEnumerable<BrowserFolder> updatedFolders = null);

        /// <summary>
        ///     Triggered when the selected node changes
        /// </summary>
        event EventHandler<NodeSelectionChangedEventArguments> SelectedNodeChanged;

        /// <summary>
        ///     Focus and select the node for a specific model Object
        /// </summary>
        /// <param name="node">The node to select</param>
        /// <param name="startEditForNewNode">Whether or not </param>
        void SelectNode(BrowserNode node,
            bool startEditForNewNode = false);

	    /// <summary>
	    ///     Selects a set of nodes
	    /// </summary>
	    /// <param name="folderIds">The folders to select</param>
	    /// <param name="nodeIds">The nodes to select</param>
	    void SelectNodes(IEnumerable<TargetFolderId> folderIds, IEnumerable<Guid> nodeIds);

        #endregion
    }
}