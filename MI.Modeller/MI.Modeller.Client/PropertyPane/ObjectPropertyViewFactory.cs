﻿using System;
using MI.Modeller.Client.Persistence.Interfaces;
using MI.Modeller.Client.PropertyPane.AttributeMappers;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Client.PropertyPane
{
    /// <summary>
    ///     Mapper for ModelObejcts to a view
    /// </summary>
    public static class ObjectPropertyViewFactory
    {
        #region Members

        public static ModelObjectPropertyView CreateModelObjectPropertyView(ModelObject modelObject,
            IPersistentWorkspace workspace)
        {
            if (modelObject.ObjectType == ObjectType.Equipment)
            {
                return EquipmentViewMapper.Map((Equipment) modelObject,
                    workspace);
            }
            if (modelObject.ObjectType == ObjectType.EquipmentClass)
            {
                return EquipmentClassViewMapper.Map((EquipmentClass) modelObject,
                    workspace);
            }
            if (modelObject.ObjectType == ObjectType.OperationsSegment)
            {
                return OperationsSegmentViewMapper.Map((OperationsSegment) modelObject,
                    workspace);
            }
            if (modelObject.ObjectType == ObjectType.WorkMaster)
            {
                return WorkMasterViewMapper.Map((WorkMaster) modelObject,
                    workspace);
            }
            if (modelObject.ObjectType == ObjectType.MaterialDefinition)
            {
                return MaterialDefinitionViewMapper.Map((MaterialDefinition) modelObject,
                    workspace);
            }
            if (modelObject.ObjectType == ObjectType.MaterialClass)
            {
                return MaterialClassViewMapper.Map((MaterialClass) modelObject,
                    workspace);
            }

            if (modelObject.ObjectType == ObjectType.MaterialSpecification)
            {
                return MaterialSpecificationViewMapper.Map((MaterialSpecification) modelObject,
                    workspace);
            }

            if (modelObject.ObjectType == ObjectType.EquipmentSpecification)
            {
                return EquipmentSpecificationViewMapper.Map((EquipmentSpecification)modelObject,
                    workspace);
            }
            if (modelObject.ObjectType == ObjectType.OperationsDefinition)
            {
                return OperationsDefinitionViewMapper.Map((OperationsDefinition)modelObject,
                    workspace);
            }
            throw new NotImplementedException();
            ;
        }

        #endregion
    }
}