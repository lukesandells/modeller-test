﻿namespace MI.Modeller.Client.PropertyPane
{
    /// <summary>
    /// The type of editor to use for a ModelObjectAttribute
    /// </summary>
    public enum EditorType
    {
        Text,
        ModelObject,
        DropDown
    }
}