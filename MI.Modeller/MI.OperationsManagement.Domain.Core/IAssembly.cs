﻿using System.Collections.Generic;
using MI.Framework;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Represents an assembly of objects that may itself belong to one or more assemblies.  Applied to any object that is an assembly.
	/// </summary>
	/// <typeparam name="TElement">The type of object contained within the assembly.</typeparam>
	public interface IAssembly<TElement>
	{
		/// <summary>
		/// Indicates whether the relationship between elements of the assembly is permanent or transient.
		/// </summary>
		/// <value>The assembly relationship.</value>
		ExtensibleEnum<AssemblyRelationship>? AssemblyRelationship { get; set; }

		/// <summary>
		/// Indicates whether the assembly is a physical object containing its elements, or if the assembly is a logical grouping of elements
		/// that do not together form a larger object.
		/// </summary>
		/// <value>The type of the assembly.</value>
		ExtensibleEnum<AssemblyType>? AssemblyType { get; set; }

		/// <summary>
		/// The elements contained within the assembly.
		/// </summary>
		IEnumerable<TElement> AssemblyElements { get; }

		/// <summary>
		/// The parent assemblies to which the assembly belongs.
		/// </summary>
		IEnumerable<TElement> ParentAssemblies { get; }

		/// <summary>
		/// Indicates whether the assembly is a root assembly (i.e. has no parents).
		/// </summary>
		bool IsRootAssembly { get; }

		/// <summary>
		/// Returns whether a given element is an ascendant of the element.
		/// </summary>
		/// <param name="element">The element to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given element is an ascendant of the element; otherwise <c>false</c>.</returns>
		bool IsAssemblyAscendantOf(TElement element);

		/// <summary>
		/// Returns whether a given element is a descendant of the element.
		/// </summary>
		/// <param name="element">The element to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given element is a descendant of the element; otherwise <c>false</c>.</returns>
		bool IsAssemblyDescendantOf(TElement element);

		/// <summary>
		/// Returns whether the given element can be added as a child element.
		/// </summary>
		/// <param name="element">The element to evaluate.</param>
		/// <returns><c>true</c> if the given element can be added as a child element; otherwise <c>false</c>.</returns>
		bool CanAdd(TElement element);

		/// <summary>
		/// Adds an element to the assembly.
		/// </summary>
		/// <param name="element">The element to add.</param>
		void AddAssemblyElement(TElement element);

		/// <summary>
		/// Removes an element from the assembly.
		/// </summary>
		/// <param name="element">The element to remove.</param>
		void RemoveAssemblyElement(TElement element);
	}
}
