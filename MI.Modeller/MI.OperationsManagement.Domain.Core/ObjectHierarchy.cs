﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Helper class for managing object hierarchies.
	/// </summary>
	public static class ObjectHierarchy
	{
		/// <summary>
		/// Helper class for managing one-to-many object hierarchies.
		/// </summary>
		public static class OneToMany
		{
			/// <summary>
			/// Adds a given child to a given parent in a one-to-many object hierarchy.
			/// </summary>
			/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
			/// <param name="parent">The parent to which to add the given child.</param>
			/// <param name="child">The child to add to the given parent.</param>
			/// <param name="setParentFunc">Action to set the parent of an element.</param>
			/// <param name="getChildrenFunc">Function returning the set of children of an element.</param>
			/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
			/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
			public static void AddElement<TElement>(TElement parent, TElement child, 
				Action<TElement, TElement> setParentFunc,
				Func<TElement, IEnumerable<TElement>> getChildrenFunc, 
				Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null, 
				Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
				where TElement : class
			{
				parent = ArgumentValidation.AssertNotNull(parent, nameof(parent));
				child = ArgumentValidation.AssertNotNull(child, nameof(child));
				setParentFunc = ArgumentValidation.AssertNotNull(setParentFunc, nameof(setParentFunc));
				getChildrenFunc = ArgumentValidation.AssertNotNull(getChildrenFunc, nameof(getChildrenFunc));
				ValidateAdd(parent, child, getChildrenFunc, getAscendantsFunc);

				(getChildrenFunc(parent) as ICollection<TElement>)?.Add(child);
				setParentFunc(child, parent);
				AddAscendants(parent, child, getAscendantsFunc, getDescendantsFunc);
				AddDescendants(parent, child, getAscendantsFunc, getDescendantsFunc);	
			}

			/// <summary>
			/// Removes a given child from a given parent in a one-to-many object hierarchy.
			/// </summary>
			/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
			/// <param name="parent">The parent from which to remove the given child.</param>
			/// <param name="child">The child to remove from the given parent.</param>
			/// <param name="setParentFunc">Action to set the parent of an element.</param>
			/// <param name="getChildrenFunc">Function returning the set of children of an element.</param>
			/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
			/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
			public static void RemoveElement<TElement>(TElement parent, TElement child, 
				Action<TElement, TElement> setParentFunc,
				Func<TElement, IEnumerable<TElement>> getChildrenFunc, 
				Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null, 
				Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
				where TElement : class
			{
				parent = ArgumentValidation.AssertNotNull(parent, nameof(parent));
				child = ArgumentValidation.AssertNotNull(child, nameof(child));
				setParentFunc = ArgumentValidation.AssertNotNull(setParentFunc, nameof(setParentFunc));
				getChildrenFunc = ArgumentValidation.AssertNotNull(getChildrenFunc, nameof(getChildrenFunc));
				ValidateRemove(parent, child, getChildrenFunc);

				(getChildrenFunc(parent) as ICollection<TElement>)?.Remove(child);
				setParentFunc(child, null);
				RemoveAscendants(parent, child, getAscendantsFunc, getDescendantsFunc);
				RemoveDescendants(parent, child, getAscendantsFunc, getDescendantsFunc);
			}
		}

		/// <summary>
		/// Helper class for managing many-to-many object hierarchies.
		/// </summary>
		public static class ManyToMany
		{
			/// <summary>
			/// Adds a given child to a given parent in a many-to-many object hierarchy.
			/// </summary>
			/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
			/// <param name="parent">The parent to which to add the given child.</param>
			/// <param name="child">The child to add to the given parent.</param>
			/// <param name="getParentsFunc">Function returning the set of parents of an element.</param>
			/// <param name="getChildrenFunc">Function returning the set of children of an element.</param>
			/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
			/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
			public static void AddElement<TElement>(TElement parent, TElement child,
				Func<TElement, IEnumerable<TElement>> getParentsFunc,
				Func<TElement, IEnumerable<TElement>> getChildrenFunc, 
				Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null, 
				Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
				where TElement : class
			{
				parent = ArgumentValidation.AssertNotNull(parent, nameof(parent));
				child = ArgumentValidation.AssertNotNull(child, nameof(child));
				getParentsFunc = ArgumentValidation.AssertNotNull(getParentsFunc, nameof(getParentsFunc));
				getChildrenFunc = ArgumentValidation.AssertNotNull(getChildrenFunc, nameof(getChildrenFunc));
				ValidateAdd(parent, child, getChildrenFunc, getAscendantsFunc);

				(getChildrenFunc(parent) as ICollection<TElement>)?.Add(child);
				(getParentsFunc(child) as ICollection<TElement>)?.Add(parent);
				AddAscendants(parent, child, getAscendantsFunc, getDescendantsFunc);
				AddDescendants(parent, child, getAscendantsFunc, getDescendantsFunc);
			}
			
			/// <summary>
			/// Removes a given child from a given parent in a many-to-many object hierarchy.
			/// </summary>
			/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
			/// <param name="parent">The parent from which to remove the given child.</param>
			/// <param name="child">The child to remove from the given parent.</param>
			/// <param name="getParentsFunc">Function returning the set of parents of an element.</param>
			/// <param name="getChildrenFunc">Function returning the set of children of an element.</param>
			/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
			/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
			public static void RemoveElement<TElement>(TElement parent, TElement child,
				Func<TElement, IEnumerable<TElement>> getParentsFunc = null,
				Func<TElement, IEnumerable<TElement>> getChildrenFunc = null, 
				Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null, 
				Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
				where TElement : class
			{
				parent = ArgumentValidation.AssertNotNull(parent, nameof(parent));
				child = ArgumentValidation.AssertNotNull(child, nameof(child));
				getChildrenFunc = ArgumentValidation.AssertNotNull(getChildrenFunc, nameof(getChildrenFunc));
				ValidateRemove(parent, child, getChildrenFunc);

				(getChildrenFunc(parent) as ICollection<TElement>)?.Remove(child);
				(getParentsFunc?.Invoke(child) as ICollection<TElement>)?.Remove(parent);
				RemoveAscendants(parent, child, getAscendantsFunc, getDescendantsFunc);
				RemoveDescendants(parent, child, getAscendantsFunc, getDescendantsFunc);
			}
		}

		/// <summary>
		/// Validates whether it is valid to add the given child to the given parent.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
		/// <param name="parent">The parent to which to add the given child.</param>
		/// <param name="child">The child to add to the given parent.</param>
		/// <param name="getChildrenFunc">Function returning the set of children of an element.</param>
		/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
		private static void ValidateAdd<TElement>(TElement parent, TElement child,
			Func<TElement, IEnumerable<TElement>> getChildrenFunc,
			Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null)
			where TElement : class
		{
			// Is the child the parent?
			if (child == parent)
			{
				throw new ArgumentException(ExceptionMessage.CannotAddObjectToItself);
			}

			// Is the given child already a child of the given parent?
			if (getChildrenFunc(parent).Contains(child))
			{
				throw new ArgumentException(ExceptionMessage.ItemAlreadyInCollection);
			}

			// Prevent cycles forming in the hierarchy.  Can't add child to parent where the parent is a child or descendant of the child.
			if (getAscendantsFunc?.Invoke(parent).Contains(child) ?? getChildrenFunc(child).Contains(parent))
			{
				throw new ArgumentException(ExceptionMessage.ParentIsDescendantOfChild);
			}
		}

		/// <summary>
		/// Validates whether it is valid to remove the given child from the given parent.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
		/// <param name="parent">The parent from which to remove the given child.</param>
		/// <param name="child">The child to remove from the given parent.</param>
		/// <param name="getChildrenFunc">Function returning the set of children of an element.</param>
		private static void ValidateRemove<TElement>(TElement parent, TElement child, 
			Func<TElement, IEnumerable<TElement>> getChildrenFunc) 
			where TElement : class
		{
			// Given child not a child of the given parent?
			if (!getChildrenFunc(parent).Contains(child))
			{
				throw new ArgumentException(ExceptionMessage.ItemNotFoundInCollection);
			}
		}

		/// <summary>
		/// Adds the given child and each of the child's descendants as descendants of the given parent and each of the parent's ascendants.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
		/// <param name="parent">The parent.</param>
		/// <param name="child">The child.</param>
		/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
		/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
		private static void AddDescendants<TElement>(TElement parent, TElement child,
			Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null,
			Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
			where TElement : class
		{
			// The child and each of the child's descendants have to be added as descendants of the parent and each of the parent's ascendants
			getAscendantsFunc?.Invoke(parent)?
				.Union(new TElement[] { parent })
				.ForEach(ascendant => getDescendantsFunc?.Invoke(child)?
					.Union(new TElement[] { child })
					.ForEach(descendant => (getDescendantsFunc(ascendant) as ICollection<TElement>)?.Add(descendant)));
		}

		/// <summary>
		/// Adds the given parent and each of the parent's ascendants as ascendants of the given child and each of the child's descendants.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
		/// <param name="parent">The parent.</param>
		/// <param name="child">The child.</param>
		/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
		/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
		private static void AddAscendants<TElement>(TElement parent, TElement child,
			Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null,
			Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
			where TElement : class
		{
			// The parent and each of the parent's ascendants have to be added as ascendants of the child and each of the child's descendants
			getDescendantsFunc?.Invoke(child)?
				.Union(new TElement[] { child })
				.ForEach(descendant => getAscendantsFunc?.Invoke(parent)?
					.Union(new TElement[] { parent })
					.ForEach(ascendant => (getAscendantsFunc(descendant) as ICollection<TElement>)?.Add(ascendant)));
		}

		/// <summary>
		/// Removes the given child and each of the child's descendants descendants of the given parent and each of the parent's ascendants.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
		/// <param name="parent">The parent.</param>
		/// <param name="child">The child.</param>
		/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
		/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
		private static void RemoveDescendants<TElement>(TElement parent, TElement child,
			Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null,
			Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
			where TElement : class
		{
			// The child and each of the child's descendants have to be removed as descendants of the parent and each of the parent's ascendants
			getAscendantsFunc?.Invoke(parent)?
				.Union(new TElement[] { parent })
				.ForEach(ascendant => getDescendantsFunc?.Invoke(child)?
					.Union(new TElement[] { child })
					.ForEach(descendant => (getDescendantsFunc(ascendant) as ICollection<TElement>)?.Remove(descendant)));
		}

		/// <summary>
		/// Removes the given parent and each of the parent's ascendants as ascendants of the given child and each of the child's descendants.
		/// </summary>
		/// <typeparam name="TElement">The type of element in the object hierarchy.</typeparam>
		/// <param name="parent">The parent.</param>
		/// <param name="child">The child.</param>
		/// <param name="getAscendantsFunc">Function returning the collection of ascendants of an element.</param>
		/// <param name="getDescendantsFunc">Function returning the collection of descendants of an element.</param>
		private static void RemoveAscendants<TElement>(TElement parent, TElement child,
			Func<TElement, IEnumerable<TElement>> getAscendantsFunc = null,
			Func<TElement, IEnumerable<TElement>> getDescendantsFunc = null)
			where TElement : class
		{
			// The parent and each of the parent's ascendants have to be removed as ascendants of the child and each of the child's descendants
			getDescendantsFunc?.Invoke(child)?
				.Union(new TElement[] { child })
				.ForEach(descendant => getAscendantsFunc?.Invoke(parent)?
					.Union(new TElement[] { parent })
					.ForEach(ascendant => (getAscendantsFunc(descendant) as ICollection<TElement>)?.Remove(ascendant)));
		}
	}
}
