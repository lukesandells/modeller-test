﻿using MI.Framework;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Indicates whether the relationship between elements of the assembly is permanent or transient.
	/// </summary>
	public enum AssemblyRelationship
    {
        /// <summary>
        /// The elements of the assembly permanently belong to the assembly.
        /// </summary>
        Permanent,

        /// <summary>
        /// The relationship between the elements of the assembly is transient.  The assembly is assembled and later disassembled.
        /// </summary>
        Transient,

		/// <summary>
		/// Other, defined externally.
		/// </summary>
		[OtherValue]
		Other
    }
}