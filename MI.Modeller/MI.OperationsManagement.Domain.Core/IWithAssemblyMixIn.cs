﻿namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Classes which utilise the AssemblyMixIn will need to implement this interface
	/// </summary>
	/// <typeparam name="TElement"></typeparam>
	public interface IWithAssemblyMixIn<TElement> where TElement : class, IAssembly<TElement>, IWithAssemblyMixIn<TElement>
	{
		/// <summary>
		/// The AssemblyMixIn responsible for managing this assembly
		/// </summary>
		AssemblyMixIn<TElement> AssemblyMixIn { get; }
	}
}
