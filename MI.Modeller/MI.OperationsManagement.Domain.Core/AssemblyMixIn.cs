﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Mix-in providing assembly behaviour.
	/// </summary>
	/// <typeparam name="TElement">The type of element in the assembly.</typeparam>
	public class AssemblyMixIn<TElement> : IAssembly<TElement>
		where TElement : class, IAssembly<TElement>, IWithAssemblyMixIn<TElement>
	{
		/// <summary>
		/// The assembly containing the mix-in.
		/// </summary>
		protected TElement Assembly => _assembly;
		private TElement _assembly;

		/// <summary>
		/// Indicates whether the relationship between elements of the assembly is permanent or transient.
		/// </summary>
		public ExtensibleEnum<AssemblyRelationship>? AssemblyRelationship { get => _assemblyRelationship; set => _assemblyRelationship = value; }
		private ExtensibleEnum<AssemblyRelationship>? _assemblyRelationship;

		/// <summary>
		/// Indicates whether the assembly is a physical object containing its elements, or if the assembly is a logical grouping of elements
		/// that do not together form a larger object.
		/// </summary>
		public ExtensibleEnum<AssemblyType>? AssemblyType { get => _assemblyType; set => _assemblyType = value; }
		private ExtensibleEnum<AssemblyType>? _assemblyType;

		/// <summary>
		/// The elements contained within the assembly.
		/// </summary>
		public IEnumerable<TElement> AssemblyElements => _assemblyElements;
		private ISet<TElement> _assemblyElements = new HashSet<TElement>();

		/// <summary>
		/// The assemblies containing the assembly.
		/// </summary>
		public IEnumerable<TElement> ParentAssemblies => _parentAssemblies;
		private ISet<TElement> _parentAssemblies = new HashSet<TElement>();

		/// <summary>
		/// Gets the ascendant assemblies of the assembly.
		/// </summary>
		/// <remarks>
		/// The ascendants will contain duplicate entries where ascendents are accessible via multiple paths.
		/// </remarks>
		private ICollection<TElement> _assemblyAscendants = new Collection<TElement>();

		/// <summary>
		/// Gets the descendant assemblies of the assembly.
		/// </summary>
		/// <remarks>
		/// The descendants will contain duplicate entries where descendants are accessible via multiple paths.
		/// </remarks>
		private ICollection<TElement> _assemblyDescendants = new Collection<TElement>();

		/// <summary>
		/// Indicates whether the assembly is a root assembly (i.e. has no parents).
		/// </summary>
		public bool IsRootAssembly => _parentAssemblies.Count == 0;

		/// <summary>
		/// Initialises a new instance of the <see cref="ProcessDefinitionMixIn{TObjectWithResourceSpecifications}"/> class.
		/// </summary>
		protected AssemblyMixIn()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ProcessDefinitionMixIn{TObjectWithResourceSpecifications}"/> class.
		/// </summary>
		/// <param name="assembly">The assembly containing the mix-in.</param>
		/// <param name="getAssemblyMixInFunc">Function returning the assembly mix-in for a given assembly.</param>
		public AssemblyMixIn(TElement assembly)
		{
			_assembly = ArgumentValidation.AssertNotNull(assembly, nameof(assembly));
		}

		/// <summary>
		/// Returns the assembly mix-in of the given assembly.
		/// </summary>
		/// <param name="assembly">The assembly for which to get the assembly mix-in.</param>
		/// <returns>The assembly mix-in of the given assembly.</returns>
		private AssemblyMixIn<TElement> MixInOf(TElement assembly)
		{
			return assembly.AssemblyMixIn;
		}

		/// <summary>
		/// Returns whether a given element is an ascendant of the element.
		/// </summary>
		/// <param name="element">The element to evaluate as a potential ascendant.</param>
		/// <returns><c>true</c> if the given element is an ascendant of the element; otherwise <c>false</c>.</returns>
		public bool IsAssemblyAscendantOf(TElement element)
		{
			element = ArgumentValidation.AssertNotNull(element, nameof(element));
			return MixInOf(element)._assemblyAscendants.Contains(Assembly);
		}

		/// <summary>
		/// Returns whether a given element is a descendant of the element.
		/// </summary>
		/// <param name="element">The element to evaluate as a potential descendant.</param>
		/// <returns><c>true</c> if the given element is a descendant of the element; otherwise <c>false</c>.</returns>
		public bool IsAssemblyDescendantOf(TElement element)
		{
			return _assemblyAscendants.Contains(element);
		}

		/// <summary>
		/// Returns whether the given element can be added as a child element.
		/// </summary>
		/// <param name="element">The element to evaluate.</param>
		/// <returns><c>true</c> if the given element can be added as a child element; otherwise <c>false</c>.</returns>
		public bool CanAdd(TElement element)
		{
			return element != _assembly
			       && !IsAssemblyDescendantOf(element)
			       && !AssemblyElements.Contains(element);
		}

		/// <summary>
		/// Adds an element to the assembly.
		/// </summary>
		/// <param name="element">The element to add.</param>
		public void AddAssemblyElement(TElement element)
		{
			if (!CanAdd(element))
			{
				throw new InvalidOperationException(ExceptionMessage.CannotAddAssemblyElement);
			}

			ObjectHierarchy.ManyToMany.AddElement(_assembly, element,
				getParentsFunc: (a) => MixInOf(a)._parentAssemblies, 
				getChildrenFunc: (a) => MixInOf(a)._assemblyElements, 
				getAscendantsFunc: (a) => MixInOf(a)._assemblyAscendants,
				getDescendantsFunc: (a) => MixInOf(a)._assemblyDescendants);
		}

		/// <summary>
		/// Removes an element from the assembly.
		/// </summary>
		/// <param name="element">The element to remove.</param>
		public void RemoveAssemblyElement(TElement element)
		{
			ObjectHierarchy.ManyToMany.RemoveElement(_assembly, element, 
				getParentsFunc: (e) => MixInOf(e)._parentAssemblies, 
				getChildrenFunc: (e) => MixInOf(e)._assemblyElements,
				getAscendantsFunc: (e) => MixInOf(e)._assemblyAscendants,
				getDescendantsFunc: (a) => MixInOf(a)._assemblyDescendants);
		}
	}
}
