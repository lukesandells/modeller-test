﻿using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// This class is mixed in with any object that requires basic properties (see <see cref="BasicProperty{TOfObject}"/>)
	/// in order to meet that requirement.
	/// </summary>
	/// <typeparam name="TObjectWithProperties">The type of object with properties.</typeparam>
	/// <remarks>
	/// Any object using this mix-in to provide basic properties must implement <see cref="IWithBasicProperties{TObjectWithProperties}"/> 
	/// and delegate all calls to this interface to this mix-in.
	/// </remarks>
	public class WithBasicPropertiesMixIn<TObjectWithProperties>
		: WithPropertiesMixIn<TObjectWithProperties, BasicProperty<TObjectWithProperties>>, IWithBasicProperties<TObjectWithProperties>
		where TObjectWithProperties : class, IWithBasicProperties<TObjectWithProperties>, IWithExternalId
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="WithBasicPropertiesMixIn{TObjectWithProperties}"/> class.
		/// </summary>
		protected WithBasicPropertiesMixIn()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="WithBasicPropertiesMixIn{TObjectWithProperties}"/> class.
		/// </summary>
		/// <param name="objectWithProperties">The object with properties to which the mix-in has been mixed in.</param>
		public WithBasicPropertiesMixIn(TObjectWithProperties objectWithProperties)
			: base(objectWithProperties, false)
		{
		}

		/// <summary>
		/// Adds a new property to the mix-in, optionally under a given parent property.
		/// </summary>
		/// <param name="externalId">The external ID of the new property.</param>
		/// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
		/// <param name="parentProperty">The parent property under which to add the new property, or <c>null</c> to add directly
		/// under the parent object.</param>
		public virtual BasicProperty<TObjectWithProperties> AddProperty(string externalId, Value value = null, 
			BasicProperty<TObjectWithProperties> parentProperty = null)
		{
			// Create and add the new property
			var property = new BasicProperty<TObjectWithProperties>(externalId, value);
			AddProperty(property, parentProperty);
			return property;
		}
	}
}
