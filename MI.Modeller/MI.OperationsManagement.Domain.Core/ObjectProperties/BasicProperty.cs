﻿using System.Linq;
using MI.Framework;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// Represents a basic property applied to all manufacturing objects, except resource transactions (see <see cref="IResourceTransaction"/>).
	/// </summary>
	/// <typeparam name="TOfObject"></typeparam>
	public class BasicProperty<TOfObject> : Property<BasicProperty<TOfObject>, TOfObject>
		where TOfObject : class, IWithBasicProperties<TOfObject>, IWithExternalId
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="BasicProperty{TOfObject}"/> class.
		/// </summary>
		protected BasicProperty()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="BasicProperty{TOfObject}"/> class.
		/// </summary>
		/// <param name="externalId">The external ID for the new basic property.</param>
		/// <param name="value">The value of the new basic property.</param>
		protected internal BasicProperty(string externalId, Value value)
			: base(externalId, value)
		{
		}

		/// <summary>
		/// Adds a new sub-property to this property.
		/// </summary>
		/// <param name="externalId">The external ID of the new sub-property.</param>
		/// <param name="value">The value of the new sub-property.</param>
		/// <returns>The newly added sub-property.</returns>
		public BasicProperty<TOfObject> AddSubProperty(string externalId, Value value)
		{
			return ParentObject.AddProperty(externalId, value, this);
		}

		/// <summary>
		/// Copies the property to another object with properties.
		/// </summary>
		/// <param name="destination">The object to receive the new copy.</param>
		/// <returns>The new copy.</returns>
		public BasicProperty<TOfObject> CopyTo(TOfObject destination)
		{
			var parentProperty = destination.AllProperties.Where(property => property.FullyQualifiedId == ParentProperty?.FullyQualifiedId)
				.SingleOrDefault() ?? ParentProperty?.CopyTo(destination);
			var copy = destination.AddProperty(ExternalId, Value, parentProperty);
			copy.Description = Description;
			SubProperties.ForEach(property => property.CopyTo(destination));
			return copy;
		}
	}
}
