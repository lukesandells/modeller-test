﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// Represents a property of an object (see <see cref="IWithProperties{TObjectWithProperties, TProperty}"/>).
	/// </summary>
	/// <remarks>
	/// This class is the non-generic base class for all property objects.  The <see cref="Property{TProperty, TOfObject}"/> class is the 
	/// generic base class for all property objects.
	/// </remarks>
	public abstract class Property : IWithExternalId
	{
		/// <summary>
		/// The internal identifier.
		/// </summary>
		public long InternalId { get; protected set; }

		/// <summary>
		/// The external identifier.
		/// </summary>
		public string ExternalId { get => _externalId; set => _externalId = value; }
		private string _externalId = string.Empty;

		/// <summary>
		/// The description of the object.
		/// </summary>
		public string Description { get; set; } = string.Empty;

		/// <summary>
		/// The property's value.
		/// </summary>
		public Value Value
		{
			get => _value;
			set
			{
				// As per the "Structured value types" section of ISA-95 Part 2, properties with sub-properties cannot have an assigned value
				if (HasSubProperties && value != null)
				{
					throw new OperationsManagementException(ExceptionMessage.PropertyWithSubPropertiesCannotHaveAssignedValue);
				}

				_value = value?.Copy();
			}
		}
		private Value _value;

		/// <summary>
		/// Indicates whether the property has sub-properties.
		/// </summary>
		public abstract bool HasSubProperties { get; }

		/// <summary>
		/// Initialises a new instance of the <see cref="Property"/> class.
		/// </summary>
		protected Property()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Property{TProperty, TOfObject}"/> class.
		/// </summary>
		/// <param name="externalId">The external ID for the new property.</param>
		/// <param name="value">The value of the new property. Value must be <c>null</c> for any property with sub-properties.</param>
		protected Property(string externalId, Value value)
		{
			_value = value?.Copy();
			_externalId = ArgumentValidation.AssertValidIdentifier(externalId, nameof(externalId));
		}
	}

	/// <summary>
	/// Represents a property of an object (see <see cref="IWithProperties{TObjectWithProperties, TProperty}"/>).
	/// </summary>
	/// <typeparam name="TProperty">The type of property.</typeparam>
	/// <typeparam name="TOfObject">The type of object to which the property belongs.</typeparam>
	/// <remarks>
	/// This class is the generic base class for all property objects.  The <see cref="Property"/> class is the 
	/// non-generic base class for all property objects.
	/// </remarks>
	public abstract class Property<TProperty, TOfObject> : Property
		where TProperty : Property<TProperty, TOfObject>
		where TOfObject : class, IWithProperties<TOfObject, TProperty>, IWithExternalId
	{
		/// <summary>
		/// The fully-qualified property ID that is guaranteed to be unique within the property's parent object.
		/// </summary>
		public string FullyQualifiedId => (ParentProperty != null ? ParentProperty.FullyQualifiedId + @"\" : string.Empty) 
			+ ExternalId.Replace(@"\", @"\\");

		/// <summary>
		/// The sub-properties of the property.
		/// </summary>
		public IEnumerable<TProperty> SubProperties => ParentObject == null ? _subProperties : ParentObject?.AllProperties
			.Where(p => p.ParentProperty == this) ?? (new TProperty[] { });
		private ISet<TProperty> _subProperties = new HashSet<TProperty>();

		/// <summary>
		/// The object to which the property belongs.
		/// </summary>
		public TOfObject ParentObject { get; protected internal set; }

		/// <summary>
		/// The property's parent property.
		/// </summary>
		public TProperty ParentProperty
		{
			get => _parentProperty;
			set
			{
				if (_parentProperty != value)
				{
					// Check if the given property has been moved from another parent property
					if (value != null && value.ParentObject != ParentObject)
					{
						throw new ArgumentException(ExceptionMessage.CannotMovePropertiesBetweenObjects);
					}

					_parentProperty = value;
				}
			}
		}
		private TProperty _parentProperty;

		/// <summary>
		/// The peer properties of the property.
		/// </summary>
		public IEnumerable<TProperty> PeerProperties =>
			(ParentProperty == null ? ParentObject.Properties : ParentProperty.SubProperties).Where(p => p != this);

		/// <summary>
		/// Indicates whether the property has sub-properties.
		/// </summary>
		public override bool HasSubProperties { get => SubProperties.Count() != 0; }

		/// <summary>
		/// Initialises a new instance of the <see cref="Property"/> class.
		/// </summary>
		protected Property()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Property{TProperty, TOfObject}"/> class.
		/// </summary>
		/// <param name="externalId">The external ID for the new property.</param>
		/// <param name="value">The value of the new property. Value must be <c>null</c> for any property with sub-properties.</param>
		protected Property(string externalId, Value value)
			: base(externalId, value)
		{
		}

		/// <summary>
		/// Returns whether the property has a sub-property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the property has a sub-property with the given external ID; otherwise <c>false</c>.</returns>
		public bool HasSubPropertyWithId(string externalId)
		{
			return SubProperties.Any(p => p.ExternalId == externalId);
		}

		/// <summary>
		/// Removes the given sub-property from this property.
		/// </summary>
		/// <param name="subProperty">The sub-property to remove.</param>
		public void RemoveSubProperty(TProperty subProperty)
		{
			// Assert that the parent contains the given sub-property
			subProperty = ArgumentValidation.AssertNotNull(subProperty, nameof(subProperty));
			if (!SubProperties.Contains(subProperty))
			{
				throw new OperationsManagementException(string.Format(
					ExceptionMessage.GivenPropertyIsNotSubProperty,
					subProperty.ExternalId, ExternalId, ParentObject.ExternalId));
			}

			ParentObject.RemoveProperty(subProperty);
		}
	}
}
