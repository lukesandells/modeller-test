﻿namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// A property of a resource transaction object (see <see cref="IResourceTransaction"/>), which states actual or required values
	/// of the corresponding property of the resource object corresponding to the resource transaction.
	/// </summary>
	/// <typeparam name="TOfObject">The type of resource transaction object.</typeparam>
	public class ResourceTransactionProperty<TOfObject> : ResourceTransactionPropertyBase<ResourceTransactionProperty<TOfObject>, TOfObject>
		where TOfObject : class, IResourceTransaction, IWithProperties<TOfObject, ResourceTransactionProperty<TOfObject>>, IWithExternalId
	{
	}
}
