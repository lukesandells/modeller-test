﻿using System.Collections.Generic;
using System.Linq;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// A property of a resource transaction object (see <see cref="IResourceTransaction"/>), which states actual or required values
	/// of the corresponding property of the resource object corresponding to the resource transaction.
	/// </summary>
	/// <typeparam name="TResourceTransactionProperty">The type of resource transaction property used by the resource transaction.</typeparam>
	/// <typeparam name="TOfObject">The type of resource transaction object.</typeparam>
	public abstract class ResourceTransactionPropertyBase<TResourceTransactionProperty, TOfObject> : Property<TResourceTransactionProperty, TOfObject>
		where TOfObject : class, IResourceTransaction, IWithProperties<TOfObject, TResourceTransactionProperty>, IWithExternalId
		where TResourceTransactionProperty : ResourceTransactionPropertyBase<TResourceTransactionProperty, TOfObject>
	{
		/// <summary>
		/// Finds the resource object property corresponding to the resource transaction property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object with the corresponding property.</typeparam>
		/// <param name="resourceObject">The resource object with the corresponding property.</param>
		/// <returns>The resource object property corresponding to the resource transaction property.</returns>
		public BasicProperty<TResourceObject> CorrespondingPropertyOf<TResourceObject>(TResourceObject resourceObject) 
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			// Get the path of resource transaction properties from the top-level property to this property
			var pathToThisProperty = new List<ResourceTransactionPropertyBase<TResourceTransactionProperty, TOfObject>>() { this };
			while (pathToThisProperty[0].ParentProperty != null)
			{
				pathToThisProperty.Insert(0, pathToThisProperty[0].ParentProperty);
			}

			// Walk the path down from the top-level property to the resource object property corresponding to this property
			BasicProperty<TResourceObject> resourceObjectProperty = null;
			foreach (var resourceTransactionProperty in pathToThisProperty)
			{
				resourceObjectProperty = (resourceTransactionProperty.ParentProperty == null ? resourceObject.Properties
					: resourceObjectProperty.SubProperties)
					.Where(p => p.ExternalId == resourceTransactionProperty.ExternalId)
					.Single();
			}

			return resourceObjectProperty;
		}
	}
}
