﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.Validation;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// This class is mixed in with any object that requires resource transaction properties in order fulfil that requirement.
	/// </summary>
	/// <typeparam name="TResourceTransaction">The type of resource transaction with the properties.</typeparam>
	/// <typeparam name="TProperty">The type of property.</typeparam>
	/// <remarks>
	/// Any object using this mix-in to provide properties must implement <see cref="IWithProperties{TResourceTransaction, TProperty}"/> and delegate
	/// all calls to this interface to this mix-in.
	/// </remarks>
	public class WithResourceTransactionPropertiesMixIn<TResourceTransaction, TProperty>
		: WithPropertiesMixIn<TResourceTransaction, TProperty>, IWithResourceTransactionProperties<TResourceTransaction, TProperty>
		where TResourceTransaction : class, IResourceTransaction, IWithResourceTransactionProperties<TResourceTransaction, TProperty>, IWithExternalId
		where TProperty : ResourceTransactionPropertyBase<TProperty, TResourceTransaction>, new()
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="WithResourceTransactionPropertiesMixIn{TResourceTransaction, TProperty}"/> class.
		/// </summary>
		protected WithResourceTransactionPropertiesMixIn()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="WithPropertiesMixIn{TResourceTransaction, TProperty}"/> class.
		/// </summary>
		/// <param name="objectWithProperties">The object with properties to which the mix-in has been mixed in.</param>
		/// <param name="allowDuplicatePropertyIds">Stipulates whether the mix-in permits multiple properties under the same parent with the same external ID.</param>
		public WithResourceTransactionPropertiesMixIn(TResourceTransaction objectWithProperties, bool allowDuplicatePropertyIds) 
			: base(objectWithProperties, allowDuplicatePropertyIds)
		{
		}

		/// <summary>
		/// Adds a new resource transaction property corresponding to the given resource object property, optionally under a given parent property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the corresponding basic property belongs.</typeparam>
		/// <param name="correpondingResourceObjectProperty">The type of corresponding resource object property.</param>
		/// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
		/// <returns>The newly added property.</returns>
		public virtual TProperty AddProperty<TResourceObject>(BasicProperty<TResourceObject> correpondingResourceObjectProperty, Value value) 
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			correpondingResourceObjectProperty = ArgumentValidation.AssertNotNull(correpondingResourceObjectProperty, 
				nameof(correpondingResourceObjectProperty));
			if (correpondingResourceObjectProperty.HasSubProperties)
			{
				throw new ArgumentException(ExceptionMessage.CannotAddResourceTransactionPropertyCorrespondingToResourceObjectPropertyWithSubProperties);
			}

			// Build the list of resource object properties for which we have to add new resource transaction properties
			var resourceObjectPropertyList = new List<BasicProperty<TResourceObject>>() { correpondingResourceObjectProperty };
			while (resourceObjectPropertyList[0].ParentProperty != null 
				&& PropertiesCorrespondingTo(resourceObjectPropertyList[0].ParentProperty).Count() == 0)
			{
				resourceObjectPropertyList.Insert(0, resourceObjectPropertyList[0].ParentProperty);
			}

			// Add a resource transaction property for each corresponding resource object property that needs to be added
			TProperty resourceTransactionProperty = null;
			TProperty parentResourceTransactionProperty = null;
			foreach (var resourceObjectProperty in resourceObjectPropertyList)
			{
				resourceTransactionProperty = new TProperty()
				{
					ExternalId = resourceObjectProperty.ExternalId,
					Description = resourceObjectProperty.Description,

					// Only set the value for the leaf-level property
					Value = resourceObjectProperty == resourceObjectPropertyList[resourceObjectPropertyList.Count - 1] ? value : null
				};

				// If resource object property is top-level, have to add a top-level resource transaction property; otherwise a sub-property.
				AddProperty(resourceTransactionProperty, parentResourceTransactionProperty);

				// This resource transaction property is now the parent of the next property in the path
				parentResourceTransactionProperty = resourceTransactionProperty;
			}

			return resourceTransactionProperty;
		}

		/// <summary>
		/// Gets the set of resource transaction properties corresponding to the given resource object property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the given property belongs.</typeparam>
		/// <param name="resourceObjectProperty">The resource object property for which to find the corresponding resource transaction properties.</param>
		/// <returns>Tthe set of resource transaction properties corresponding to the given resource object property.</returns>
		public virtual IEnumerable<TProperty> PropertiesCorrespondingTo<TResourceObject>(BasicProperty<TResourceObject> resourceObjectProperty)
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			resourceObjectProperty = ArgumentValidation.AssertNotNull(resourceObjectProperty, nameof(resourceObjectProperty));

			// Get the path of resource object properties from the top-level property to the given resource object property
			var pathToResourceObjectProperty = new List<BasicProperty<TResourceObject>>() { resourceObjectProperty };
			while (pathToResourceObjectProperty[0].ParentProperty != null)
			{
				pathToResourceObjectProperty.Insert(0, pathToResourceObjectProperty[0].ParentProperty);
			}

			for (var i = 0; i < pathToResourceObjectProperty.Count; i++)
			{
				TProperty resourceTransactionProperty = null;
				if (i == 0)
				{
					// Top-level resource object property
					if (pathToResourceObjectProperty.Count == 1)
					{
						// Top-level resource object property with no sub-properties
						return Properties.Where(p => p.ExternalId == pathToResourceObjectProperty[i].ExternalId);
					}
					else
					{
						// Top-level resource object property and it has sub-properties.  If a corresponding resource transaction property
						// exists, there must be just one because objects with sub-properties must be unique amongst their peers.
						resourceTransactionProperty = Properties.Where(p => p.ExternalId == pathToResourceObjectProperty[i + 1].ExternalId).SingleOrDefault();
					}
				}
				else if (i == pathToResourceObjectProperty.Count - 2)
				{
					// Second last resource object property, so the corresponding properties will be sub-properties of the corresponding 
					// resource transaction property, if they exist.
					return resourceTransactionProperty.SubProperties.Where(p => p.ExternalId == pathToResourceObjectProperty[i + 1].ExternalId);
				}
				else
				{
					// Mid-level resource object property with sub-properties.  If a corresponding resource transaction property
					// exists, there must be just one because objects with sub-properties must be unique amongst their peers.
					resourceTransactionProperty = resourceTransactionProperty.SubProperties
						.Where(p => p.ExternalId == pathToResourceObjectProperty[i + 1].ExternalId).Single();
				}
				
				// Couldn't find a corresponding resource transaction property, so return an empty collection
				if (resourceTransactionProperty == null)
				{
					return new TProperty[] { };
				}
			}

			// Will never execute - needed to satisfy the compiler that all code paths return a value
			return new TProperty[] { };
		}
	}
}