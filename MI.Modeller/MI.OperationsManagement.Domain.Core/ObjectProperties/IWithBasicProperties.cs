﻿using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// Applied to any object with basic properties (see <see cref="BasicProperty{TOfObject}"/>).
	/// </summary>
	/// <typeparam name="TObjectWithProperties">The type of object with properties.</typeparam>
	/// <remarks>
	/// Intended to be used with the <see cref="WithBasicPropertiesMixIn{TObjectWithProperties}"/> class.
	/// </remarks>
	public interface IWithBasicProperties<TObjectWithProperties> : IWithProperties<TObjectWithProperties, BasicProperty<TObjectWithProperties>>
		where TObjectWithProperties : class, IWithBasicProperties<TObjectWithProperties>, IWithExternalId
	{
		/// <summary>
		/// Adds a new property to the object, optionally under a given parent property.
		/// </summary>
		/// <param name="externalId">The external ID of the new property.</param>
		/// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
		/// <param name="parentProperty">The parent property under which to add the new property, or <c>null</c> to add directly
		/// under the parent object.</param>
		/// <returns>The newly added property.</returns>
		BasicProperty<TObjectWithProperties> AddProperty(string externalId, Value value = null, BasicProperty<TObjectWithProperties> parentProperty = null);
	}
}
