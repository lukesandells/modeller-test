﻿using System.Collections.Generic;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// Applied to any object with resource transactoin properties (see <see cref="ResourceTransactionPropertyBase{TResourceTransactionProperty, TOfObject}"/>).
	/// </summary>
	/// <typeparam name="TResourceTransaction">The type of resource transaction object.</typeparam>
	/// <typeparam name="TProperty">The type of resource transaction property.</typeparam>
	/// <remarks>
	/// Intended to be used with the <see cref="WithResourceTransactionPropertiesMixIn{TResourceTransaction, TProperty}"/> class.
	/// </remarks>
	public interface IWithResourceTransactionProperties<TResourceTransaction, TProperty> : IWithProperties<TResourceTransaction, TProperty>
		where TResourceTransaction : class, IResourceTransaction, IWithResourceTransactionProperties<TResourceTransaction, TProperty>, IWithExternalId
		where TProperty : ResourceTransactionPropertyBase<TProperty, TResourceTransaction>
	{
		/// <summary>
		/// Adds a new resource transaction property corresponding to the given resource object property, optionally under a given parent property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the corresponding basic property belongs.</typeparam>
		/// <param name="correpondingResourceObjectProperty">The type of corresponding resource object property.</param>
		/// <param name="value">The value of the new property.  A value of <c>null</c> must be used for properties with sub-properties.</param>
		/// <returns>The newly added property.</returns>
		TProperty AddProperty<TResourceObject>(BasicProperty<TResourceObject> correpondingResourceObjectProperty, Value value = null) 
			where TResourceObject : class, IResourceObject<TResourceObject>;

		/// <summary>
		/// Gets the set of resource transaction properties corresponding to the given resource object property.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the given property belongs.</typeparam>
		/// <param name="resourceObjectProperty">The resource object property for which to find the corresponding resource transaction properties.</param>
		/// <returns>Tthe set of resource transaction properties corresponding to the given resource object property.</returns>
		IEnumerable<TProperty> PropertiesCorrespondingTo<TResourceObject>(BasicProperty<TResourceObject> resourceObjectProperty)
			where TResourceObject : class, IResourceObject<TResourceObject>;
	}
}
