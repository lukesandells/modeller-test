﻿using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// This class is the base class for "with property mix-ins", which are mixed in with any object that requires 
	/// properties (see <see cref="Property{TProperty, TOfObject}"/> in order to meet that requirement.
	/// </summary>
	/// <typeparam name="TObjectWithProperties">The type of object with properties.</typeparam>
	/// <typeparam name="TProperty">The type of property.</typeparam>
	/// <remarks>
	/// Any object using this mix-in to provide properties must implement <see cref="IWithProperties{TObjectWithProperties, TProperty}"/> and delegate
	/// all calls to this interface to this mix-in.
	/// </remarks>
	public abstract class WithPropertiesMixIn<TObjectWithProperties, TProperty> : IWithProperties<TObjectWithProperties, TProperty>
		where TObjectWithProperties : class, IWithProperties<TObjectWithProperties, TProperty>, IWithExternalId
		where TProperty : Property<TProperty, TObjectWithProperties>
	{
		/// <summary>
		/// The object with the properties that are managed by the mix-in.
		/// </summary>
		protected TObjectWithProperties ObjectWithProperties => _objectWithProperties;
		private TObjectWithProperties _objectWithProperties;

		/// <summary>
		/// The set of descendant properties.
		/// </summary>
		public IEnumerable<TProperty> AllProperties => _allProperties;
		private ISet<TProperty> _allProperties = new HashSet<TProperty>();

		/// <summary>
		/// Stipulates whether the mix-in permits multiple properties under the same parent with the same external ID.
		/// </summary>
		protected bool AllowDuplicatePropertyIds => _allowDuplicatePropertyIds;
		private bool _allowDuplicatePropertyIds;

		/// <summary>
		/// Gets the set of the top-level properties within the mix-in.
		/// </summary>
		public IEnumerable<TProperty> Properties => AllProperties
			.Where(p => p.ParentProperty == null);

		/// <summary>
		/// Initialises a new instance of the <see cref="WithPropertiesMixIn{TObjectWithProperties, TProperty}"/> class.
		/// </summary>
		protected WithPropertiesMixIn()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="WithPropertiesMixIn{TObjectWithProperties, TProperty}"/> class.
		/// </summary>
		/// <param name="objectWithProperties">The object with properties to which the mix-in has been mixed in.</param>
		/// <param name="allowDuplicatePropertyIds">Stipulates whether the mix-in permits multiple properties under the same parent 
		/// with the same external ID.</param>
		protected WithPropertiesMixIn(TObjectWithProperties objectWithProperties, bool allowDuplicatePropertyIds)
		{
			_objectWithProperties = ArgumentValidation.AssertNotNull(objectWithProperties, nameof(objectWithProperties));
			_allowDuplicatePropertyIds = allowDuplicatePropertyIds;
		}

		/// <summary>
		/// Returns whether the mix-in has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the mix-in has a property with the given external ID; otherwise <c>false</c>.</returns>
		public virtual bool HasPropertyWithId(string externalId)
		{
			return Properties.Any(p => p.ExternalId == externalId);
		}

		/// <summary>
		/// Adds a given property under a given parent property.
		/// </summary>
		/// <param name="property">The property to add.</param>
		/// <param name="parentProperty">
		/// The parent property under which to add the given property, or otherwise <c>null</c> to add as 
		/// a top-level property of the parent object.
		/// </param>
		protected void AddProperty(TProperty property, TProperty parentProperty)
		{
			property = ArgumentValidation.AssertNotNull(property, nameof(property));
			
			// Get the peer properties of the new property
			var peerProperties = parentProperty == null ? Properties : parentProperty.SubProperties;

			// Adding as a sub-property of a descendant property?
			if (parentProperty != null)
			{
				// Get the peer properties of the parent property of the new property
				var parentPeerProperties = parentProperty.ParentProperty == null ? Properties : parentProperty.ParentProperty.SubProperties;

				// Given parent property can't belong to a different object
				if (parentProperty.ParentObject != _objectWithProperties)
				{
					throw new OperationsManagementException(ExceptionMessage.CannotMovePropertiesBetweenObjects);
				}

				// As per the "Structured value types" section of ISA-95 Part 2, properties with sub-properties cannot have an assigned value
				if (parentProperty.Value != null)
				{
					throw new OperationsManagementException(ExceptionMessage.PropertyWithSubPropertiesCannotHaveAssignedValue);
				}

				// Can't add a property that has the same ID as an existing property
				if (!_allowDuplicatePropertyIds && peerProperties.Any(p => p.ExternalId == property.ExternalId))
				{
					throw new OperationsManagementException(string.Format(ExceptionMessage.PropertyWithGivenIdAlreadyExists,
						_objectWithProperties.ExternalId, property.ExternalId));
				}

				// Properties with sub-properties must be unique within their peers
				if (parentPeerProperties.Count(p => p.ExternalId == parentProperty.ExternalId) > 1)
				{
					throw new OperationsManagementException(ExceptionMessage.PropertyWithSubPropertiesMustBeUniqueAmongstPeers);
				}
			}

			// Properties with sub-properties must be unique within their peers
			if (peerProperties.Any(p => p.HasSubProperties && p.ExternalId == property.ExternalId))
			{
				throw new OperationsManagementException(ExceptionMessage.PropertyWithSubPropertiesMustBeUniqueAmongstPeers);
			}

			// Can't have multiple peer properties with the same ID and value
			if (peerProperties.Any(p => p.ExternalId == property.ExternalId && p.Value == property.Value))
			{
				throw new OperationsManagementException(ExceptionMessage.PropertyIdAndValueMustBeUniqueAmongstPeers);
			}

			// Recursively add any sub-properties that have been defined on the new property
			property.SubProperties.ForEach(p => AddProperty(p, property));

			// Add the given property
			_allProperties.Add(property);
			property.ParentObject = _objectWithProperties;
			property.ParentProperty = parentProperty;
		}

		/// <summary>
		/// Removes the given property from the mix-in.
		/// </summary>
		/// <param name="property">The property to remove.</param>
		public virtual void RemoveProperty(TProperty property)
		{
			property = ArgumentValidation.AssertNotNull(property, nameof(property));

			// Assert that the parent contains the given sub-property
			if (!AllProperties.Contains(property))
			{
				throw new OperationsManagementException(string.Format(
					ExceptionMessage.GivenPropertyIsNotPropertyOfObject,
					property.ExternalId, _objectWithProperties.ExternalId));
			}

			// Recursively remove any sub-properties that have been defined on the new property
			property.SubProperties.ForEach(p => RemoveProperty(p));

			var propertyPeers = property.ParentProperty == null ? Properties	: property.ParentProperty.SubProperties;
			property.ParentObject = null;
			property.ParentProperty = null;
			_allProperties.Remove(property);
		}
	}
}
