﻿using System.Linq;
using MI.OperationsManagement.Domain.Core.DataTypes;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// A property of a resource transaction object (see <see cref="IResourceTransaction"/>), which states specified or required resource object
	/// property values for a specified quantity of the resource object corresponding to the resource transaction.
	/// </summary>
	/// <typeparam name="TOfObject">The type of resource transaction object.</typeparam>
	public class QuantitySpecificResourceTransactionProperty<TOfObject> : ResourceTransactionPropertyBase<QuantitySpecificResourceTransactionProperty<TOfObject>, TOfObject>
		where TOfObject : class, IResourceTransaction, IWithProperties<TOfObject, QuantitySpecificResourceTransactionProperty<TOfObject>>, IWithExternalId
	{
		/// <summary>
		/// The specified quantity of the corresponding resource object to which the condition applies.
		/// </summary>
		public virtual Quantity ResourceQuantity
		{
			get => _resourceQuantity;
			set
			{
				// Get the peer properties with the same ID
				var peerPropertiesWithSameId = PeerProperties.Where(p => p.ExternalId == ExternalId);

				// Can't have a null quantity (meaning the property pertains to the entirety of the resource) if other properties
				// exist corresponding to the same resource property (i.e. with the same property ID)
			    var quantitySpecificResourceTransactionProperties = peerPropertiesWithSameId as QuantitySpecificResourceTransactionProperty<TOfObject>[] ?? peerPropertiesWithSameId.ToArray();
			    if (value == null && quantitySpecificResourceTransactionProperties.FirstOrDefault() != null
					|| value != null && quantitySpecificResourceTransactionProperties.Any(p => p.ResourceQuantity == null))
				{
					throw new OperationsManagementException(ExceptionMessage.PropertyIdMustBeUniqueOrAllPropertiesWithIdMustSpecifyQuantity);
				}

				_resourceQuantity = value?.Copy();
			}
		}
		private Quantity _resourceQuantity;
	}
}
