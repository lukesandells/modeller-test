﻿using System;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// Base class for properties of resource transaction objects (see <see cref="IResourceTransaction"/>).
	/// </summary>
	/// <typeparam name="TProperty">The type of property.</typeparam>
	/// <typeparam name="TOfObject">The type of resource transaction object.</typeparam>
	public abstract class BaseResourceTransactionProperty<TProperty, TOfObject> : Property<TProperty, TOfObject>
		where TOfObject : class, IResourceTransaction, IWithProperties<TOfObject, TProperty>, IWithExternalId
		where TProperty : BaseResourceTransactionProperty<TProperty, TOfObject>
	{
		/// <summary>
		/// Returns the property of the resource object to which the resource transaction property corresponds.
		/// </summary>
		/// <typeparam name="TResourceObject">The type of resource object to which the corresponding basic property belongs.</typeparam>
		/// <param name="resourceObject">
		/// The corresponding resource object containing the property to which this resource transaction property corresponds.
		/// </param>
		/// <returns>
		/// The property of the resource object to which the resource transaction 
		/// property corresponds, or <c>null</c> if one cannot be found.
		/// </returns>
		public BasicProperty<TResourceObject> CorrespondingPropertyOf<TResourceObject>(TResourceObject resourceObject)
			where TResourceObject : class, IResourceObject<TResourceObject>
		{
			throw new NotImplementedException();
		}
	}
}
