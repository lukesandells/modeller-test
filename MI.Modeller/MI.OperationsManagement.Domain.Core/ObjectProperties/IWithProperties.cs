﻿using System.Collections.Generic;

namespace MI.OperationsManagement.Domain.Core.ObjectProperties
{
	/// <summary>
	/// Applied to any object with properties (see <see cref="Property{TProperty, TOfObject}"/>).
	/// </summary>
	/// <typeparam name="TObjectWithProperties">The type of object with properties.</typeparam>
	/// <typeparam name="TProperty">The type of property.</typeparam>
	/// <remarks>
	/// Intended to be used with the <see cref="WithPropertiesMixIn{TObjectWithProperties, TProperty}"/> class.
	/// </remarks>
	public interface IWithProperties<TObjectWithProperties, TProperty>
		where TObjectWithProperties : class, IWithProperties<TObjectWithProperties, TProperty>, IWithExternalId
		where TProperty : Property<TProperty, TObjectWithProperties>
	{
		/// <summary>
		/// Gets the collection of all properties of the object, including sub-properties.
		/// </summary>
		IEnumerable<TProperty> AllProperties { get; }

		/// <summary>
		/// Gets the immediate properties of the object.
		/// </summary>
		IEnumerable<TProperty> Properties { get; }

		/// <summary>
		/// Returns whether the object has a property with a given external ID.
		/// </summary>
		/// <param name="externalId">The external ID for which to search.</param>
		/// <returns><c>true</c> if the object has a property with the given external ID; otherwise <c>false</c>.</returns>
		bool HasPropertyWithId(string externalId);

		/// <summary>
		/// Removes the given property from the object.
		/// </summary>
		/// <param name="property">The property to remove.</param>
		void RemoveProperty(TProperty property);
	}
}
