﻿namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Applied to any object that has an external ID.
	/// </summary>
	public interface IWithExternalId
	{
		/// <summary>
		/// The external ID.
		/// </summary>
		string ExternalId { get; set; }
	}
}
