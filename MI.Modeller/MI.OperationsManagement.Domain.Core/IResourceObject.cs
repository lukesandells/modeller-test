﻿using MI.OperationsManagement.Domain.Core.ObjectProperties;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Marker interface applied to all material, equipment, physical asset or personnel resource objects.
	/// </summary>
	/// <typeparam name="TResourceObject">The type of resource object to which the interface is applied.</typeparam>
	public interface IResourceObject<TResourceObject> : IWithBasicProperties<TResourceObject>, IWithExternalId
		where TResourceObject : class, IWithBasicProperties<TResourceObject>, IWithExternalId
	{
	}
}
