using MI.Framework;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Indicates whether the assembly is a physical object containing its elements, or if the assembly is a logical grouping of elements
	/// that do not together form a larger object.
	/// </summary>
	public enum AssemblyType
    {
        /// <summary>
        /// The assembly represents a physical object.
        /// </summary>
        Physical,

        /// <summary>
        /// The assembly is a logical assembly of its elements.
        /// </summary>
        Logical,

		/// <summary>
		/// Other, defined externally.
		/// </summary>
		[OtherValue]
		Other
    }
}