﻿namespace MI.OperationsManagement.Domain.Core
{
    /// <summary>
    ///     Represents the geospatial location of a resource
    /// </summary>
    public class SpatialDefinition
    {
        #region  Fields

        /// <summary>
        ///     The format of the geospatial designation
        /// </summary>
        public virtual string Format { get; protected set; }

        /// <summary>
        ///     The SRID of this location
        /// </summary>
        public virtual string Srid { get; protected set; }

        /// <summary>
        ///     The authority issuing the srid
        /// </summary>
        public virtual string SridAuthority { get; protected set; }

        /// <summary>
        ///     The value of the geospatial designation
        /// </summary>
        public virtual string Value { get; protected set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Creates a new Geospatial designation
        /// </summary>
        /// <param name="srid">The SRID of this location</param>
        /// <param name="sridAuthority">The authority issuing the srid</param>
        /// <param name="format">The format of the geospatial designation</param>
        /// <param name="value">The value of the geospatial designation</param>
        public SpatialDefinition(string srid = null,
            string sridAuthority = null,
            string format = null,
            string value = null)
        {
            Srid = srid;
            SridAuthority = sridAuthority;
            Format = format;
            Value = value;
        }

        #endregion
    }
}