﻿using System;
using System.Runtime.Serialization;

namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// The exception that is thrown when a business rule is violated in the operations management library.
	/// </summary>
	[Serializable]
	public class OperationsManagementException : Exception
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="OperationsManagementException"/> class.
		/// </summary>
		public OperationsManagementException()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="OperationsManagementException"/> class.
		/// </summary>
		/// <param name="message">The exception message.</param>
		public OperationsManagementException(string message)
			: base(message)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="OperationsManagementException"/> class.
		/// </summary>
		/// <param name="message">The exception message.</param>
		/// <param name="innerException">An exception to be wrapped by the new exception.</param>
		public OperationsManagementException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="OperationsManagementException"/> class.
		/// </summary>
		/// <param name="info">Serialisation information.</param>
		/// <param name="context">Streaming context.</param>
		public OperationsManagementException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}
