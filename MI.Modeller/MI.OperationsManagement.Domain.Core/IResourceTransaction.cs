﻿namespace MI.OperationsManagement.Domain.Core
{
	/// <summary>
	/// Marker interface applied to all resource transaction objects.
	/// </summary>
	public interface IResourceTransaction
	{
	}
}
