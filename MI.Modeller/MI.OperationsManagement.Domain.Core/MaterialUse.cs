using MI.Framework;

namespace MI.OperationsManagement.Domain.Core
{
    /// <summary>
    ///     Enum MaterialUseTypeValues
    /// </summary>
    public enum MaterialUse
    {
        /// <summary>
        ///     The consumed
        /// </summary>
        Consumed,

        /// <summary>
        ///     The produced
        /// </summary>
        Produced,

        /// <summary>
        ///     The consumable
        /// </summary>
        Consumable,

        /// <summary>
        ///     The replaced asset
        /// </summary>
        ReplacedAsset,

        /// <summary>
        ///     The replacement asset
        /// </summary>
        ReplacementAsset,

        /// <summary>
        ///     The sample
        /// </summary>
        Sample,

        /// <summary>
        ///     The resurned sample
        /// </summary>
        ReturnedSample,

        /// <summary>
        ///     The carrier
        /// </summary>
        Carrier,

        /// <summary>
        ///     The returned carrier
        /// </summary>
        ReturnedCarrier,

        /// <summary>
        ///     The by product produced
        /// </summary>
        ByProductProduced,

        /// <summary>
        ///     The co product produced
        /// </summary>
        CoProductProduced,

        /// <summary>
        ///     The yield produced
        /// </summary>
        YieldProduced,

        /// <summary>
        ///     The inventoried
        /// </summary>
        Inventoried,

		/// <summary>
		///     The other
		/// </summary>
		[OtherValue]
		Other
    }
}