﻿using MI.Framework.Units;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	public static class AsMeasureExtensions
	{
		public static Measure AsMeasure(this decimal number)
		{
			return number;
		}

		public static Measure AsMeasure(this int number)
		{
			return number;
		}

		public static Measure AsMeasure(this double number)
		{
			return ((decimal)number).AsMeasure();
		}

		public static Measure AsMeasure(this UnitDecimal unitDecimal)
		{
			return new Measure(unitDecimal);
		}

		public static Measure AsMeasure(this UnitDecimal unitDecimal, Uncertainty uncertainty)
		{
			return new Measure(unitDecimal, uncertainty);
		}


	}
}
