﻿using MI.Framework.Units;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Extension methods for the <see cref="UnitDecimal"/> class.
	/// </summary>
	public static class AsQuantityExtensions
	{
		public static DecimalQuantity AsQuantity(this decimal number)
		{
			return ((UnitDecimal)number).AsQuantity();
		}

		public static SingleQuantity AsQuantity(this decimal number, string key)
		{
			return ((UnitDecimal)number).AsQuantity(key);
		}

		public static DecimalQuantity AsQuantity(this int number)
		{
			return ((UnitDecimal)number).AsQuantity();
		}

		public static SingleQuantity AsQuantity(this int number, string key)
		{
			return ((UnitDecimal)number).AsQuantity(key);
		}

		public static DecimalQuantity AsQuantity(this double number)
		{
			return ((UnitDecimal)number).AsQuantity();
		}

		public static SingleQuantity AsQuantity(this double number, string key)
		{
			return ((UnitDecimal)number).AsQuantity(key);
		}

		public static DecimalQuantity AsQuantity(this UnitDecimal unitDecimal)
		{
			return new DecimalQuantity(unitDecimal);
		}

		public static DecimalQuantity AsQuantity(this UnitDecimal unitDecimal, Uncertainty uncertainty)
		{
			return new DecimalQuantity(unitDecimal, uncertainty);
		}

		public static SingleQuantity AsQuantity(this UnitDecimal unitDecimal, string key)
		{
			return new SingleQuantity(unitDecimal, key: key);
		}

		public static SingleQuantity AsQuantity(this UnitDecimal unitDecimal, string key = "",
			QuantityDataType dataType = QuantityDataType.Decimal, Uncertainty uncertainty = null)
		{
			return new SingleQuantity(unitDecimal, key, dataType, uncertainty);
		}
	}
}
