﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using MI.Framework;
using MI.Framework.Units;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents a value, comprising one or more elements (see <see cref="QuantityElement"/>), each with a value of a given <see cref="QuantityDataType"/>.
	/// </summary>
	public class Quantity : IEnumerable<QuantityElement>, ICopyable<Quantity>
	{
		public IEnumerable<QuantityElement> Elements => _elements.Values;

		private IDictionary<string, QuantityElement> _elements = new Dictionary<string, QuantityElement>();

		public QuantityElement this[string key] => _elements[key];

		private static Regex _parseRegex = new Regex(@"^(?:(?<QuantityElement>[^;]+)(?:; )?)+$", RegexOptions.Compiled);

		/// <summary>
		/// Initialises a new instance of the <see cref="Quantity"/> class.
		/// </summary>
		protected Quantity()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Quantity"/> class.
		/// </summary>
		/// <param name="unitDecimal">The value and unit of the new quantity.</param>
		/// <param name="key">A key uniquely identifying the initial quantity element.</param>
		/// <param name="dataType">The data type of the initial quantity element.</param>
		/// <param name="uncertainty">The uncertainty of the initial quantity element.</param>
		internal Quantity(UnitDecimal unitDecimal, string key = "", ExtensibleEnum<QuantityDataType>? dataType = null,
			Uncertainty uncertainty = null)
		{
			// Create a single quantity element to hold the given values
			_elements[key] = new QuantityElement(unitDecimal, key, dataType ?? QuantityDataType.Decimal, uncertainty);
		}

		/// <summary>
		/// Constructs a new quantity from a collection of quantity elements.
		/// </summary>
		/// <param name="elements">The quantity elements from which to construct the new quantity.</param>
		/// <returns>A new quantity constructed from the given collection of quantity elements.</returns>
		public static Quantity FromElements(IEnumerable<QuantityElement> elements)
		{
			elements = ArgumentValidation.AssertNotNull(elements, nameof(elements));
			if (elements.Count() == 1)
			{
				return SingleQuantity.FromElement(elements.Single());
			}

			return new Quantity() { _elements = elements.Copy().ToDictionary(e => e.Key) };
		}

		public bool ContainsKey(string key)
		{
			return _elements.ContainsKey(key);
		}

		public Quantity And(SingleQuantity quantity)
		{
			quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
			var newQuantity = Copy();
			newQuantity._elements.Add(quantity.Key, quantity.Element.Copy());
			return newQuantity;
		}

		public Quantity Copy()
		{
			var copy = new Quantity();
			Elements.ForEach(e => copy._elements.Add(e.Key, e.Copy()));
			return copy;
		}

		public static implicit operator Quantity(UnitDecimal unitDecimal)
		{
			return new DecimalQuantity(unitDecimal);
		}

		public static implicit operator Quantity(decimal number)
		{
			return new DecimalQuantity(number);
		}

		public static Quantity operator +(Quantity quantity)
		{
			return QuantityMath<Quantity>.UnaryPlus(quantity);
		}

		public static Quantity operator -(Quantity quantity)
		{
			return QuantityMath<Quantity>.UnaryMinus(quantity);
		}

		public static Quantity operator +(Quantity left, Quantity right)
		{
			return QuantityMath<Quantity>.Add(left, right);
		}

		public static Quantity operator -(Quantity left, Quantity right)
		{
			return QuantityMath<Quantity>.Subtract(left, right);
		}

		public static Quantity operator *(Quantity left, Quantity right)
		{
			return QuantityMath<Quantity>.Multiply(left, right);
		}

		public static Quantity operator *(Quantity quantity, decimal constant)
		{
			return QuantityMath<Quantity>.Multiply(quantity, constant);
		}

		public static Quantity operator *(decimal constant, Quantity quantity)
		{
			return QuantityMath<Quantity>.Multiply(constant, quantity);
		}

		public static Quantity operator /(Quantity left, Quantity right)
		{
			return QuantityMath<Quantity>.Divide(left, right);
		}

		public static Quantity operator /(Quantity quantity, decimal constant)
		{
			return QuantityMath<Quantity>.Divide(quantity, constant);
		}

		public static Quantity operator /(decimal constant, Quantity quantity)
		{
			return QuantityMath<Quantity>.Divide(constant, quantity);
		}

		public override string ToString()
		{
			return string.Join("; ", Elements);
		}

		public SerialisedQuantity Serialise()
		{
			var serialisedValue = new SerialisedQuantity();
			Elements.ForEach(element => serialisedValue.Elements.Add(element.Serialise()));
			return serialisedValue;
		}

		public static Quantity Deserialise(SerialisedQuantity serialisedQuantity)
		{
			serialisedQuantity = ArgumentValidation.AssertNotNull(serialisedQuantity, nameof(serialisedQuantity));
			if (serialisedQuantity.Elements.Count == 0)
			{
				return null;
			}

			return FromElements(serialisedQuantity.Elements.Select(element => QuantityElement.Deserialise(element)));
		}

		/// <summary>
		/// Gets a hash code for the quantity.
		/// </summary>
		/// <returns>A hash code for the quantity.</returns>
		public override int GetHashCode()
		{
			return Elements.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var quantity = obj as Quantity;
			if (quantity == null)
			{
				return false;
			}

			// Not equal if differnet number of elements
			if (quantity.Elements.Count() != Elements.Count())
			{
				return false;
			}

			// Compare the two
			return Equals(quantity);
		}

        /// <summary>
        /// Compares with another <see cref="Quantity"/> for equality.
        /// </summary>
        /// <param name="quantity">The quantity with which to compare.</param>
        /// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
        public bool Equals(Quantity quantity)
		{
			if (quantity == null)
			{
				return false;
			}

			// Compare the two
			foreach (var element in quantity.Elements)
			{
				if (!_elements.TryGetValue(element.Key, out var otherElement))
				{
					return false;
				}

				if (element != otherElement)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection of quantity elements.
		/// </summary>
		/// <returns>An enumerator that iterates through the collection of quantity elements.</returns>
		public IEnumerator<QuantityElement> GetEnumerator()
		{
			return _elements.Values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection of quantity elements.
		/// </summary>
		/// <returns>An enumerator that iterates through the collection of quantity elements.</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return _elements.Values.GetEnumerator();
		}

		/// <summary>
		/// Compares two <see cref="Quantity"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(Quantity left, Quantity right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.Equals(right);
		}

		/// <summary>
		/// Compares two <see cref="Quantity"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(Quantity left, Quantity right)
		{
			return !(left == right);
		}
	}
}
