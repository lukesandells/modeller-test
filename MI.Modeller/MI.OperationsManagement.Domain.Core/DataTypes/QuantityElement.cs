﻿using System;
using MI.Framework;
using MI.Framework.Units;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
    /// <summary>
    /// Represents an element of a <see cref="Quantity"/> object.
    /// </summary>
    public class QuantityElement : ICopyable<QuantityElement>
    {
		/// <summary>
		/// The data type of the quantity element.
		/// </summary>
		public ExtensibleEnum<QuantityDataType> DataType => _dataType;
		private ExtensibleEnum<QuantityDataType> _dataType;

		/// <summary>
		/// A key uniquely identifying the quantity element.
		/// </summary>
		public string Key => _key;
		private string _key = string.Empty;

		/// <summary>
		/// Indicates whether the quantity element has a key.
		/// </summary>
		public bool HasKey => !string.IsNullOrEmpty(Key);

		/// <summary>
		/// The decimal value of the quantity element.
		/// </summary>
		public decimal DecimalValue => UnitDecimal.DecimalValue;

		/// <summary>
		/// The unit of the quantity element.
		/// </summary>
		public Unit Unit => UnitDecimal.Unit;

		/// <summary>
		/// The <see cref="UnitDecimal"/> underpinning the quantity element.
		/// </summary>
		internal UnitDecimal UnitDecimal => _unitDecimal;
		private UnitDecimal _unitDecimal;

		/// <summary>
		/// The measurement uncertainty of the quantity element.
		/// </summary>
		public Uncertainty Uncertainty => _uncertainty;
		private Uncertainty _uncertainty;

		/// <summary>
		/// Initialises a new instance of the <see cref="QuantityElement"/> class.
		/// </summary>
		protected QuantityElement()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="QuantityElement"/> class.
		/// </summary>
		/// <param name="unitDecimal">The <see cref="UnitDecimal"/> to underpin the new quantity.</param>
		/// <param name="key">A key uniquely identifying the quantity element.</param>
		/// <param name="dataType">The data type of the new quantity.</param>
		/// <param name="uncertainty">The measurement uncertainty of the new quantity element.</param>
		internal QuantityElement(UnitDecimal unitDecimal, string key = "", ExtensibleEnum<QuantityDataType>? dataType = null,
			Uncertainty uncertainty = null)
        {
			// Validate the key
			key = ArgumentValidation.AssertNotNull(key, nameof(key));
			if (key != string.Empty)
			{
				key = ArgumentValidation.AssertValidIdentifier(key, nameof(key));
			}

			// Check the given data type is supported
			_dataType = dataType ?? QuantityDataType.Decimal;
			if (!((QuantityDataType)_dataType).IsSupported())
			{
				throw new ArgumentException(string.Format(ExceptionMessage.UnsupportedQuantityDataType, dataType), nameof(dataType));
			}

			// Validate the object if a validator is declared
			var validator = ((QuantityDataType)_dataType).GetValidator();
			if (!(validator?.IsValid(unitDecimal.DecimalValue) ?? true))
			{
				throw new ArgumentOutOfRangeException(ExceptionMessage.UnitDecimalValueOutOfRange);
			}

			_unitDecimal = unitDecimal;
			_key = key;
			_uncertainty = uncertainty;
        }

		/// <summary>
		/// Returns a copy of the quantity element.
		/// </summary>
		/// <returns>A copy of the quantity element.</returns>
		public QuantityElement Copy()
		{
			return new QuantityElement(UnitDecimal, Key, DataType, Uncertainty?.Copy());
		}

		/// <summary>
		/// Returns a string representation of the quantity element.
		/// </summary>
		/// <returns>A string representation of the quantity element.</returns>
		public override string ToString()
		{
			var keyString = Key == string.Empty ? Key : string.Format("{0}: ", Key);
			var quantityString = string.Format("{0} ({1})", UnitDecimal.ToString(), DataType.ToString());
			var uncertaintyString = Uncertainty == null ? string.Empty 
				: string.Format(" with uncertainty {0}", Uncertainty.ToString());
			return keyString + quantityString + uncertaintyString;
		}

		/// <summary>
		/// Serialises a quantity element.
		/// </summary>
		/// <returns>The serialised quantity element.</returns>
		internal SerialisedQuantity.Element Serialise()
		{
			return new SerialisedQuantity.Element()
			{
				Value = DecimalValue.ToString("G29"),
				DataType = DataType.ToString(),
				UnitCode = Unit.Code,
				Key = Key,
				Uncertainty = Uncertainty?.Serialise()
			};
		}

		/// <summary>
		/// Deserialises a quantity element.
		/// </summary>
		/// <param name="serialisedElement">The serialised quantity element to deserialise.</param>
		/// <returns>The deserialised quantity element.</returns>
		internal static QuantityElement Deserialise(SerialisedQuantity.Element serialisedElement)
		{
			serialisedElement = ArgumentValidation.AssertNotNull(serialisedElement, nameof(serialisedElement));

			// Attempt to parse the data type
			if (!Enum.TryParse<QuantityDataType>(serialisedElement.DataType, out var dataType))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.UnrecognisedQuantityDataType, serialisedElement.DataType), nameof(serialisedElement));
			}

			// Check data type is supported
			if (!dataType.IsSupported())
			{
				throw new ArgumentException(string.Format(ExceptionMessage.UnsupportedQuantityDataType, serialisedElement.DataType), nameof(serialisedElement));
			}

			return new QuantityElement()
			{
				_key = serialisedElement.Key,
				_dataType = dataType,
				_unitDecimal = UnitDecimal.Parse(serialisedElement.Value, serialisedElement.UnitCode),
				_uncertainty = serialisedElement.Uncertainty == null ? null : Uncertainty.Deserialise(serialisedElement.Uncertainty)
			};
		}

		/// <summary>
		/// Gets a hash code for the quantity element.
		/// </summary>
		/// <returns>A hash code for the quantity element.</returns>
		public override int GetHashCode()
		{
			return (UnitDecimal, Key, DataType, Uncertainty).GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var quantityElement = obj as QuantityElement;
			if (quantityElement == null)
			{
				return false;
			}

			// Compare the two
			return quantityElement.UnitDecimal == UnitDecimal
				&& quantityElement.Key == Key
				&& quantityElement.DataType == DataType
				&& quantityElement.Uncertainty == Uncertainty;
		}

		/// <summary>
		/// Compares with another <see cref="QuantityElement"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(QuantityElement quantityElement)
		{
			if (quantityElement == null)
			{
				return false;
			}

			// Compare the two
			return quantityElement.UnitDecimal == UnitDecimal
				&& quantityElement.Key == Key
				&& quantityElement.DataType == DataType
				&& quantityElement.Uncertainty == Uncertainty;
		}

		/// <summary>
		/// Compares two <see cref="QuantityElement"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(QuantityElement left, QuantityElement right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.UnitDecimal == right.UnitDecimal
				&& left.Key == right.Key
				&& left.DataType == right.DataType
				&& left.Uncertainty == right.Uncertainty;
		}

		/// <summary>
		/// Compares two <see cref="QuantityElement"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(QuantityElement left, QuantityElement right)
		{
			return !(left == right);
		}
	}
}