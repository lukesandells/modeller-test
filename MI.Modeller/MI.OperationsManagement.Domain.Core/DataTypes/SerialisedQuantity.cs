﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// A serialised quantity.
	/// </summary>
	public class SerialisedQuantity
	{
		/// <summary>
		/// The serialised quantity elements.
		/// </summary>
		public ISet<Element> Elements { get; } = new HashSet<Element>();

		/// <summary>
		/// A serialised quantity element.
		/// </summary>
		public class Element
		{
			/// <summary>
			/// A string representation of the numeric value.
			/// </summary>
			public string Value { get; set; }

			/// <summary>
			/// A string representation of the quantity data type.
			/// </summary>
			public string DataType { get; set; }

			/// <summary>
			/// A key that uniquely identifies a <see cref="QuantityElement"/>.
			/// </summary>
			public string Key { get; set; }

			/// <summary>
			/// A unit code, in accordance with the current unit library (see <see cref="UnitLibrary.Current"/>).
			/// </summary>
			public string UnitCode { get; set; }

			/// <summary>
			/// The elements of an uncertainty, represented as strings.
			/// </summary>
			public SerialisedUncertainty Uncertainty { get; set; }
		}

		/// <summary>
		/// Returns a JSON representation of the serialised quantity.
		/// </summary>
		/// <returns>A JSON representation of the serialised quantity.</returns>
		public override string ToString()
		{
			return JsonConvert.SerializeObject(this);
		}

		/// <summary>
		/// Parses a JSON reprsentation of a serialised quantity.
		/// </summary>
		/// <param name="s">The JSON representation of the serialised quantity to parse.</param>
		/// <returns>The result of parsing the given string.</returns>
		public static SerialisedQuantity Parse(string s)
		{
			return JsonConvert.DeserializeObject<SerialisedQuantity>(s);
		}

		/// <summary>
		/// Deserialises the serialised quantity.
		/// </summary>
		/// <returns>The deserialised quantity.</returns>
		public Quantity Deserialise()
		{
			return Quantity.Deserialise(this);
		}
	}
}
