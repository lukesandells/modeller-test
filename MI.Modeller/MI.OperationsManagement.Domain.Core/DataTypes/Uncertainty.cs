﻿using MI.Framework;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents a measurement uncertainty.
	/// </summary>
	public abstract class Uncertainty : ICopyable<Uncertainty>
	{
		/// <summary>
		/// The type of measurement uncertainty.
		/// </summary>
		public abstract UncertaintyType UncertaintyType { get; }

		/// <summary>
		/// Returns a copy of the uncertainty.
		/// </summary>
		/// <returns>A copy of the uncertainty.</returns>
		public abstract Uncertainty Copy();

		/// <summary>
		/// Returns a measurement uncertainty represented using an <see cref="ExpandedUncertainty"/>.
		/// </summary>
		/// <param name="coverageFactor">The number of standard deviations covered by the expanded uncertainty.</param>
		/// <param name="confidenceLevel">The probability that the true value of the measurand falls within the measurement value,
		/// plus or minus the expanded uncertainty (which is the coverage factor multiplied by the standard uncertainty).</param>
		/// <param name="standardUncertainty">The standard uncertainty, which is the margin of error constituting a single standard deviation.
		/// Provide either the <paramref name="standardUncertainty"/> or <paramref name="expandedUncertainty"/>, but not both.</param>
		/// <param name="expandedUncertainty">The expanded uncertainty, which is range of values for a measurand about a measurement for 
		/// which the probability that the true value of the measurand is within that range is the <paramref name="confidenceLevel"/>.
		/// Provide either the <paramref name="standardUncertainty"/> or <paramref name="expandedUncertainty"/>, but not both.</param>
		/// <returns>A measurement uncertainty represented using an <see cref="ExpandedUncertainty"/></returns>
		public static ExpandedUncertainty ExpandedUncertainty(float coverageFactor, float confidenceLevel,
			float? standardUncertainty = null, float? expandedUncertainty = null)
		{
			return new DataTypes.ExpandedUncertainty(coverageFactor, confidenceLevel, standardUncertainty, expandedUncertainty);
		}

		/// <summary>
		/// Returns a measurement uncertainty represented using an <see cref="AccuracyAndPrecision"/> class.
		/// </summary>
		/// <param name="accuracy">The accuracy (systemic bias between the a measurement value and the real value of the measurand).</param>
		/// <param name="precision">The precision (standard deviation between a measurement value and the real value of the measurand).</param>
		/// <returns>A measurement uncertainty represented using an <see cref="AccuracyAndPrecision"/> class</returns>
		public static AccuracyAndPrecision AccuracyAndPrecision(float accuracy, float precision)
		{
			return new DataTypes.AccuracyAndPrecision(accuracy, precision);
		}

		/// <summary>
		/// Deserialises a serialised uncertainty.
		/// </summary>
		/// <param name="serialisedUncertainty">The serialised uncertainty to deserialise.</param>
		/// <returns>The deserialised uncertainty.</returns>
		public static Uncertainty Deserialise(SerialisedUncertainty serialisedUncertainty)
		{
			serialisedUncertainty = ArgumentValidation.AssertNotNull(serialisedUncertainty, nameof(serialisedUncertainty));
			if (serialisedUncertainty.UncertaintyType == UncertaintyType.AccuracyAndPrecision)
			{
				return new AccuracyAndPrecision(float.Parse(serialisedUncertainty.Accuracy), float.Parse(serialisedUncertainty.Precision));
			}
			else
			{
				float? standardUncertainty = null;
				float? expandedUncertainty = null;
				if (!string.IsNullOrEmpty(serialisedUncertainty.StandardUncertainty))
				{
					standardUncertainty = float.Parse(serialisedUncertainty.StandardUncertainty);
				}
				else
				{
					expandedUncertainty = float.Parse(serialisedUncertainty.ExpandedUncertainty);
				}

				return new ExpandedUncertainty(float.Parse(serialisedUncertainty.CoverageFactor), 
					float.Parse(serialisedUncertainty.ConfidenceLevel), standardUncertainty, expandedUncertainty);
			}
		}

		/// <summary>
		/// Serialises the uncertainty.
		/// </summary>
		/// <returns>The serialised uncertainty.</returns>
		public SerialisedUncertainty Serialise()
		{
			if (this is AccuracyAndPrecision accuracyAndPrecision)
			{
				return new SerialisedUncertainty()
				{
					UncertaintyType = UncertaintyType,
					Accuracy = accuracyAndPrecision.Accuracy.ToString(),
					Precision = accuracyAndPrecision.Precision.ToString()
				};
			}
			else
			{
				var expandedUncertainty = (ExpandedUncertainty)this;
				return new SerialisedUncertainty()
				{
					UncertaintyType = UncertaintyType,
					StandardUncertainty = expandedUncertainty.StandardUncertainty.ToString(),
					ExpandedUncertainty = expandedUncertainty.Value.ToString(),
					CoverageFactor = expandedUncertainty.CoverageFactor.ToString(),
					ConfidenceLevel = expandedUncertainty.ConfidenceLevel.ToString()
				};
			}
		}

		/// <summary>
		/// Gets a hash code for the uncertainty.
		/// </summary>
		/// <returns>A hash code for the uncertainty.</returns>
		/// <remarks>Implementation found in derived classes.  Overridden here to satisfy the compiler.</remarks>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		/// <remarks>Implementation found in derived classes.  Overridden here to satisfy the compiler.</remarks>
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		/// <summary>
		/// Compares with another <see cref="Uncertainty"/> uncertainty for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns>True if the objects are equal, otherwise false.</returns>
		public bool Equals(Uncertainty uncertainty)
		{
			return Equals((object)uncertainty);
		}

		/// <summary>
		/// Compares two <see cref="Uncertainty"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(Uncertainty left, Uncertainty right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.Equals((object)right);
		}

		/// <summary>
		/// Compares two <see cref="Uncertainty"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(Uncertainty left, Uncertainty right)
		{
			return !(left == right);
		}
	}
}
