﻿using MI.Framework.Units;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents a measure of something.  This may be determined through physical measurement or other means.  A measure must not be
	/// confused with a quantity (see remarks).
	/// </summary>
	/// <remarks>
	/// A measure represents the measure of something, while a quantity represents the amount of something.  Both may be determined through
	/// measurement, but each have different semantics from the other.  For example, a temperature may be a measure, but not a quantity.
	/// </remarks>
	public struct Measure
	{
		private UnitDecimal _unitDecimal;

		public decimal DecimalValue => _unitDecimal.DecimalValue;
		public Unit Unit => _unitDecimal.Unit;

		public Uncertainty Uncertainty => _uncertainty;
		private Uncertainty _uncertainty;

		internal Measure(UnitDecimal unitDecimal) : this(unitDecimal, null)
		{
		}

		internal Measure(UnitDecimal unitDecimal, Uncertainty uncertainty)
		{
			_unitDecimal = unitDecimal;
			_uncertainty = uncertainty;
		}

		/// <summary>
		/// Returns a new measure with the given <see cref="Uncertainty"/>.
		/// </summary>
		/// <param name="uncertainty">The uncertainty to apply to the new measure.</param>
		/// <returns>A new measure with the given <see cref="Uncertainty"/>.</returns>
		public Measure WithUncertainty(Uncertainty uncertainty)
		{
			return new Measure(_unitDecimal, uncertainty);
		}

		public static bool TryParse(string valueString, string unitCode, out Measure result)
		{
			if (!UnitDecimal.TryParse(valueString, unitCode, out var unitDecimal))
			{
				result = 0;
				return false;
			}

			result = new Measure(unitDecimal);
			return true;
		}

		/// <summary>
		/// Implicitly casts a <see cref="Measure"/> to a <see cref="Measure"/>.
		/// </summary>
		/// <param name="unitDecimal">The unit decimal to cast.</param>
		public static implicit operator Measure(UnitDecimal unitDecimal)
		{
			return new Measure(unitDecimal);
		}

		/// <summary>
		/// Implicitly casts a <see cref="Decimal"/> number to a <see cref="Measure"/>.
		/// </summary>
		/// <param name="number">The decimal number to cast.</param>
		public static implicit operator Measure(decimal number)
		{
			return new Measure(number);
		}

		/// <summary>
		/// Gets a hash code for the measure.
		/// </summary>
		/// <returns>A hash code for the measure.</returns>
		public override int GetHashCode()
		{
			return _unitDecimal.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return _unitDecimal.Equals(obj);
		}

		/// <summary>
		/// Compares with another <see cref="Measure"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(Measure measure)
		{
			return _unitDecimal.Equals(measure._unitDecimal);
		}

		/// <summary>
		/// Compares two <see cref="Measure"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(Measure left, Measure right)
		{
			return left._unitDecimal == right._unitDecimal;
		}

		/// <summary>
		/// Compares two <see cref="Measure"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(Measure left, Measure right)
		{
			return !(left == right);
		}

		public override string ToString()
		{
			return _unitDecimal.ToString();
		}
	}
}
