﻿using System;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// A measurement uncertainty specified as an accuracy (systemic bias) and precision (standard deviation between a measurement value and
	/// the real value of the measurand).
	/// </summary>
	public class AccuracyAndPrecision : Uncertainty
	{
		/// <summary>
		/// The type of measurement uncertainty.
		/// </summary>
		public override UncertaintyType UncertaintyType => UncertaintyType.AccuracyAndPrecision;

		/// <summary>
		/// The accuracy (systemic bias between the a measurement value and the real value of the measurand).
		/// </summary>
		public float Accuracy => _accuracy;
		private float _accuracy;

		/// <summary>
		/// The precision (standard deviation between a measurement value and the real value of the measurand).
		/// </summary>
		public float Precision => _precision;
		private float _precision;

		/// <summary>
		/// Initialises a new instance of the <see cref="AccuracyAndPrecision"/> class.
		/// </summary>
		/// <param name="accuracy">The accuracy (systemic bias between the a measurement value and the real value of the measurand).</param>
		/// <param name="precision">The precision (standard deviation between a measurement value and the real value of the measurand).</param>
		internal AccuracyAndPrecision(float accuracy, float precision)
		{
			if (precision < 0 || precision > 1)
			{
				throw new ArgumentException(ExceptionMessage.PrecisionMustBeBetweenZeroAndOne);

			}
			_accuracy = accuracy;
			_precision = precision;
		}

		/// <summary>
		/// Returns a copy of the accuracy and precision.
		/// </summary>
		/// <returns>A copy of the accuracy and precision.</returns>
		public override Uncertainty Copy()
		{
			return new AccuracyAndPrecision(Accuracy, Precision);
		}

		/// <summary>
		/// Returns a string representation of the uncertainty.
		/// </summary>
		/// <returns>A string representation of the uncertainty.</returns>
		public override string ToString()
		{
			return string.Format("{0} (Bias: {0})", Precision, Accuracy);
		}

		/// <summary>
		/// Gets a hash code for the uncertainty.
		/// </summary>
		/// <returns>A hash code for the uncertainty.</returns>
		public override int GetHashCode()
		{
			return (Accuracy, Precision).GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns>True if the objects are equal, otherwise false.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var uncertainty = obj as AccuracyAndPrecision;
			if (uncertainty == null)
			{
				return false;
			}

			// Compare the two
			return uncertainty.Accuracy == Accuracy && uncertainty.Precision == Precision;
		}

		/// <summary>
		/// Compares with another <see cref="AccuracyAndPrecision"/> uncertainty for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns>True if the objects are equal, otherwise false.</returns>
		public bool Equals(AccuracyAndPrecision uncertainty)
		{
			if (uncertainty == null)
			{
				return false;
			}

			// Compare the two
			return uncertainty.Accuracy == Accuracy && uncertainty.Precision == Precision;
		}

		/// <summary>
		/// Compares two <see cref="AccuracyAndPrecision"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(AccuracyAndPrecision left, AccuracyAndPrecision right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.Accuracy == right.Accuracy && left.Precision == right.Precision;
		}

		/// <summary>
		/// Compares two <see cref="AccuracyAndPrecision"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(AccuracyAndPrecision left, AccuracyAndPrecision right)
		{
			return !(left == right);
		}
	}
}
