﻿using System;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Converts an object to its string representation in the format required by ISA-95.
	/// </summary>
	public abstract class Validator
	{
		/// <summary>
		/// Validates the given object against the constraints required by a data type.
		/// </summary>
		/// <param name="source">The object validate.</param>
		/// <returns><c>true</c> if the given object is valid; otherwise <c>false</c>.</returns>
		public abstract bool IsValid(object source);

		/// <summary>
		/// Validates that a given string is a valid identifier.
		/// </summary>
		public class Identifier : Validator
		{
			/// <summary>
			/// Validates the given string against the constraints required by a data type.
			/// </summary>
			/// <param name="source">The object validate.  Expects a <see cref="String"/>.</param>
			/// <returns><c>true</c> if the given object is valid; otherwise <c>false</c>.</returns>
			public override bool IsValid(object source)
			{
				try
				{
					ArgumentValidation.AssertValidIdentifier((string)source, nameof(source));
				}
				catch (ArgumentException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as an <see cref="SByte"/>.
		/// </summary
		public class SByte : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid positive <see cref="SByte"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToSByte(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as an <see cref="Byte"/>.
		/// </summary
		public class Byte : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid positive <see cref="Byte"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToByte(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as an <see cref="BigInteger"/>.
		/// </summary
		public class BigInteger : Validator
		{
			/// <summary>
			/// Validates an object as a positive <see cref="BigInteger"/>.
			/// </summary
			public class Positive : Validator
			{
				/// <summary>
				/// Returns whether the given object is a valid positive <see cref="BigInteger"/>.
				/// </summary>
				/// <param name="source">The object to validate.</param>
				/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
				public override bool IsValid(object source)
				{
					return BigInteger.IsValid(source, true, false, false);
				}
			}

			/// <summary>
			/// Validates an object as a negative <see cref="BigInteger"/>.
			/// </summary
			public class Negative : Validator
			{
				/// <summary>
				/// Returns whether the given object is a valid negative <see cref="BigInteger"/>.
				/// </summary>
				/// <param name="source">The object to validate.</param>
				/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
				public override bool IsValid(object source)
				{
					return BigInteger.IsValid(source, false, true, false);
				}
			}

			/// <summary>
			/// Validates an object as a non-positive <see cref="BigInteger"/>.
			/// </summary
			public class NonPositive : Validator
			{
				/// <summary>
				/// Returns whether the given object is a valid non-positive <see cref="BigInteger"/>.
				/// </summary>
				/// <param name="source">The object to validate.</param>
				/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
				public override bool IsValid(object source)
				{
					return BigInteger.IsValid(source, false, true, true);
				}
			}

			/// <summary>
			/// Validates an object as a non-negative <see cref="BigInteger"/>.
			/// </summary
			public class NonNegative : Validator
			{
				/// <summary>
				/// Returns whether the given object is a valid non-negative <see cref="BigInteger"/>.
				/// </summary>
				/// <param name="source">The object to validate.</param>
				/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
				public override bool IsValid(object source)
				{
					return BigInteger.IsValid(source, true, false, true);
				}
			}

			/// <summary>
			/// Returns whether the given object is a valid <see cref="BigInteger"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				return IsValid(source, true, true, true);
			}

			/// <summary>
			/// Returns whether the given object is a valid <see cref="BigInteger"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <param name="permitPositive">Indicates whether positive numbers are permitted.</param>
			/// <param name="permitNegative">Indicates whether negative numbers are permitted.</param>
			/// <param name="permitZero">Indicates whether a zero value is permitted.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			private static bool IsValid(object source, bool permitPositive, bool permitNegative, bool permitZero)
			{
				source = ArgumentValidation.AssertNotNull(source, nameof(source));
				if (!System.Numerics.BigInteger.TryParse(source.ToString(), out var bigInteger))
				{
					return false;
				}

				if (bigInteger > 0 && !permitPositive || bigInteger < 0 && !permitNegative || bigInteger == 0 && !permitZero)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as an <see cref="Int32"/>.
		/// </summary>
		public class Int32 : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="Int32"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToInt32(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="UInt32"/>.
		/// </summary>
		public class UInt32 : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="UInt32"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToUInt32(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="Int64"/>.
		/// </summary>
		public class Int64 : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="Int64"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToInt64(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="UInt64"/>.
		/// </summary>
		public class UInt64 : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="UInt64"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToUInt64(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="Int16"/>.
		/// </summary>
		public class Int16 : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="Int16"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToInt16(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="UInt16"/>.
		/// </summary>
		public class UInt16 : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="UInt16"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToUInt16(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="Single"/>.
		/// </summary>
		public class Single : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="Single"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToSingle(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}

		/// <summary>
		/// Validates an object as a <see cref="Double"/>.
		/// </summary>
		public class Double : Validator
		{
			/// <summary>
			/// Returns whether the given object is a valid <see cref="Double"/>.
			/// </summary>
			/// <param name="source">The object to validate.</param>
			/// <returns><c>true</c> if the object is valid; otherwise <c>false</c>."/></returns>
			public override bool IsValid(object source)
			{
				try
				{
					Convert.ToDouble(source);
				}
				catch (OverflowException)
				{
					return false;
				}

				return true;
			}
		}
	}
}
