﻿using System;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents measurement uncertainty as an expanded uncertainty.
	/// </summary>
	public class ExpandedUncertainty : Uncertainty
	{ 
		/// <summary>
		/// The type of measurement uncertainty.
		/// </summary>
		public override UncertaintyType UncertaintyType => UncertaintyType.ExpandedUncertainty;

		/// <summary>
		/// The value of the expanded uncertainty.  See remarks for more information.
		/// </summary>
		/// <remarks>
		/// The expanded uncertainty determines the range of values for a measurand about a measurement for which the probability that
		/// the true value of the measurand is within that range is the <see cref="ConfidenceLevel"/>.
		/// </remarks>
		public float Value => StandardUncertainty * CoverageFactor;

		/// <summary>
		/// The number of standard deviations covered by this expanded uncertainty.
		/// </summary>
		public float CoverageFactor => _coverageFactor;
		private float _coverageFactor;

		/// <summary>
		/// Margin of error constituting a single standard deviation.  Equivalent to the <see cref="AccuracyAndPrecision.Precision"/> property
		/// of the <see cref="AccuracyAndPrecision"/> class.
		/// </summary>
		public float StandardUncertainty => _standardUncertainty;
		private float _standardUncertainty;

		/// <summary>
		/// The probability that the true value of the measurand falls within the measurement value, plus or minus the 
		/// expanded uncertainty <see cref="Value"/>.
		/// </summary>
		public float ConfidenceLevel => _confidenceLevel;
		private float _confidenceLevel;

		/// <summary>
		/// Initialises a new instance of the <see cref="ExpandedUncertainty"/> class.
		/// </summary>
		/// <param name="coverageFactor">The number of standard deviations covered by the expanded uncertainty.</param>
		/// <param name="confidenceLevel">The probability that the true value of the measurand falls within the measurement value,
		/// plus or minus the expanded uncertainty (which is the coverage factor multiplied by the standard uncertainty).</param>
		/// <param name="standardUncertainty">The standard uncertainty, which is the margin of error constituting a single standard deviation.
		/// Provide either the <paramref name="standardUncertainty"/> or <paramref name="expandedUncertainty"/>, but not both.</param>
		/// <param name="expandedUncertainty">The expanded uncertainty, which is range of values for a measurand about a measurement for 
		/// which the probability that the true value of the measurand is within that range is the <paramref name="confidenceLevel"/>.
		/// Provide either the <paramref name="standardUncertainty"/> or <paramref name="expandedUncertainty"/>, but not both.</param>
		internal ExpandedUncertainty(float coverageFactor, float confidenceLevel,
			float? standardUncertainty = null, float? expandedUncertainty = null)
		{
			if (standardUncertainty.HasValue == expandedUncertainty.HasValue)
			{ 
				throw new ArgumentException(ExceptionMessage.MustProvideEitherStandardOrExpandedUncertainty);
			}

			if (standardUncertainty.HasValue)
			{
				_standardUncertainty = standardUncertainty.Value;
			}
			else
			{
				_standardUncertainty = expandedUncertainty.Value / coverageFactor;
			}

			if (confidenceLevel < 0 || confidenceLevel > 1)
			{
				throw new ArgumentException(ExceptionMessage.ConfidenceLevelMustBeBetweenZeroAndOne);
			}

			_coverageFactor = coverageFactor;
			_confidenceLevel = confidenceLevel;
		}

		/// <summary>
		/// Returns a copy of the expanded uncertainty.
		/// </summary>
		/// <returns>A copy of the expanded uncertainty.</returns>
		public override Uncertainty Copy()
		{
			return new ExpandedUncertainty(CoverageFactor, ConfidenceLevel, StandardUncertainty);
		}

		/// <summary>
		/// Returns a string representation of the uncertainty.
		/// </summary>
		/// <returns>A string representation of the uncertainty.</returns>
		public override string ToString()
		{
			return string.Format("{0} (Standard Uncertainty: {1}, Coverage Factor: {2}, Confidence Level: {3})",
				Value, StandardUncertainty, CoverageFactor, ConfidenceLevel);
		}

		/// <summary>
		/// Gets a hash code for the uncertainty.
		/// </summary>
		/// <returns>A hash code for the uncertainty.</returns>
		public override int GetHashCode()
		{
			return (StandardUncertainty, CoverageFactor, ConfidenceLevel).GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var uncertainty = obj as ExpandedUncertainty;
			if (uncertainty == null)
			{
				return false;
			}

			// Compare the two
			return uncertainty.StandardUncertainty == StandardUncertainty 
				&& uncertainty.CoverageFactor == CoverageFactor 
				&& uncertainty.ConfidenceLevel == ConfidenceLevel;
		}

		/// <summary>
		/// Compares with another <see cref="ExpandedUncertainty"/> uncertainty for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(ExpandedUncertainty uncertainty)
		{
			if (uncertainty == null)
			{
				return false;
			}

			// Compare the two
			return uncertainty.StandardUncertainty == StandardUncertainty 
				&& uncertainty.CoverageFactor == CoverageFactor
				&& uncertainty.ConfidenceLevel == ConfidenceLevel;
		}

		/// <summary>
		/// Compares two <see cref="ExpandedUncertainty"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(ExpandedUncertainty left, ExpandedUncertainty right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.StandardUncertainty == right.StandardUncertainty
				&& left.CoverageFactor == right.CoverageFactor
				&& left.ConfidenceLevel == right.ConfidenceLevel;
		}

		/// <summary>
		/// Compares two <see cref="ExpandedUncertainty"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(ExpandedUncertainty left, ExpandedUncertainty right)
		{
			return !(left == right);
		}
	}
}