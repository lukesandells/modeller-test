﻿using System.Globalization;
using System.Threading;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents a text <see cref="String"/> with a corresponding <see cref="CultureInfo"/>.
	/// </summary>
	public class Text
	{
		/// <summary>
		/// The text string.
		/// </summary>
		private string _string;

		/// <summary>
		/// The culture information for the text.
		/// </summary>
		public CultureInfo CultureInfo { get => _cultureInfo; set => _cultureInfo=value; }
		private CultureInfo _cultureInfo;

		/// <summary>
		/// Initialises a new instance of the <see cref="Text"/> class.
		/// </summary>
		/// <param name="str">The text string.</param>
		/// <param name="cultureInfo">The culture information for the text.</param>
		public Text(string str, CultureInfo cultureInfo)
		{
			_string = str;
			_cultureInfo = cultureInfo;
		}

		/// <summary>
		/// Explicit cast operator from <see cref="Text"/> to <see cref="String"/>.
		/// </summary>
		/// <param name="text">The text to cast.</param>
		public static explicit operator string(Text text)
		{
			return text.ToString();
		}

		/// <summary>
		/// Implicit cast operator from <see cref="String"/> to <see cref="Text"/>.
		/// </summary>
		/// <param name="str">The string to cast.</param>
		public static implicit operator Text(string str)
		{
			return new Text(str, Thread.CurrentThread.CurrentCulture);
		}

		/// <summary>
		/// Overloaded == operator.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the left and right sides are equal; otherwise <c>false</c>.</returns>
		public static bool operator ==(Text left, Text right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Return true if the fields match
			return left._string == right._string && left._cultureInfo == right._cultureInfo;
		}

		/// <summary>
		/// Overloaded != operator.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the left and right sides are not equal; otherwise <c>false</c>.</returns>
		public static bool operator !=(Text left, Text right)
		{
			return !(left == right);
		}

		/// <summary>
		/// Determines whether the given object is equal to the text.
		/// </summary>
		/// <param name="obj">The object to evaluate.</param>
		/// <returns><c>true</c> if the given object is equal to this text; otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var text = obj as Text;
			if ((object)text == null)
			{
				return false;
			}

			// Return true if the fields match
			return text._string == _string && text._cultureInfo == _cultureInfo;
		}

		/// <summary>
		/// Gets a hash value for the text.
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return (_string, _cultureInfo).GetHashCode();
		}

		/// <summary>
		/// Returns the text string.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return _string;
		}
	}
}
