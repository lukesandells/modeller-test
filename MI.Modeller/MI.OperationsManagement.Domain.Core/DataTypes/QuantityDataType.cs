﻿using System.Numerics;
using MI.Framework;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Stipulates the data type of a <see cref="SingleQuantity"/> or <see cref="QuantityElement"/>.
	/// </summary>
	public enum QuantityDataType
	{
		/// <summary>
		/// Indicates a numeric value.  Derived from <see cref="Decimal"/>.
		/// </summary>
		[DataType(typeof(decimal))]
		Numeric,

		/// <summary>
		/// Indicates a signed byte value.
		/// </summary>
		[DataType(typeof(sbyte), Validator = typeof(Validator.SByte))]
		Byte,

		/// <summary>
		/// Indicates an unsigned byte value.
		/// </summary>
		[DataType(typeof(byte), Validator = typeof(Validator.Byte))]
		UnsignedByte,

		/// <summary>
		/// Indicates an unbounded integer value.
		/// </summary>
		[DataType(typeof(BigInteger), Validator = typeof(Validator.BigInteger))]
		Integer,

		/// <summary>
		/// Indicates an unbounded positive integer (with a value greater than zero).
		/// </summary>
		[DataType(typeof(BigInteger), Validator = typeof(Validator.BigInteger.Positive))]
		PositiveInteger,

		/// <summary>
		/// Indicates an unbounded negative integer (with a value less than zero).
		/// </summary>
		[DataType(typeof(BigInteger), Validator = typeof(Validator.BigInteger.Negative))]
		NegativeInteger,

		/// <summary>
		/// Indicates an unbounded non-negative integer (with a value of zero or greater).
		/// </summary>
		[DataType(typeof(BigInteger), Validator = typeof(Validator.BigInteger.NonNegative))]
		NonNegativeInteger,

		/// <summary>
		/// Indicates an unbounded non-positive integer (with a value of zero or less).
		/// </summary>
		[DataType(typeof(BigInteger), Validator = typeof(Validator.BigInteger.NonPositive))]
		NonPositiveInteger,

		/// <summary>
		/// Indicates a 32-bit integer.
		/// </summary>
		[DataType(typeof(int), Validator = typeof(Validator.Int32))]
		Int,

		/// <summary>
		/// Indicates an unsigned 32-bit integer.
		/// </summary>
		[DataType(typeof(uint), Validator = typeof(Validator.UInt32))]
		UnsignedInt,

		/// <summary>
		/// Indicates a 64-bit integer.
		/// </summary>
		[DataType(typeof(long), Validator = typeof(Validator.Int64))]
		Long,

		/// <summary>
		/// Inicates a 64-bit unsigned integer.
		/// </summary>
		[DataType(typeof(ulong), Validator = typeof(Validator.UInt64))]
		UnsignedLong,

		/// <summary>
		/// Indicates a 16-bit integer.
		/// </summary>
		[DataType(typeof(short), Validator = typeof(Validator.Int16))]
		Short,

		/// <summary>
		/// Indicates an unsigned 16-bit integer.
		/// </summary>
		[DataType(typeof(ushort), Validator = typeof(Validator.UInt16))]
		UnsignedShort,

		/// <summary>
		/// Indicates a decimal.
		/// </summary>
		[DataType(typeof(decimal))]
		Decimal,

		/// <summary>
		/// Indicates a single-precision floating point.
		/// </summary>
		[DataType(typeof(float), Validator = typeof(Validator.Single))]
		Float,

		/// <summary>
		/// Indicates a double-precision floating point.
		/// </summary>
		[DataType(typeof(double), Validator = typeof(Validator.Double))]
		Double,

		/// <summary>
		/// A data type defined outside this library.  Not supported.
		/// </summary>
		/// <remarks>
		/// In order to add support for this data type, a facility must be added to register additional data types with corresponding parsers.
		/// </remarks>
		[OtherValue]
		[DataType(Supported = false)]
		Other
	}
}
