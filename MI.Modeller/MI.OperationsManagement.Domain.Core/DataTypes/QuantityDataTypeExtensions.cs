﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Extension methods for the <see cref="QuantityDataType"/> enumeration.
	/// </summary>
	public static class QuantityDataTypeExtensions
	{
		/// <summary>
		/// Cache of <see cref="DataTypeAttribute"/> attributes against the <see cref="QuantityDataType"/> enumeration for improving performance.
		/// </summary>
		private static IDictionary<QuantityDataType, DataTypeAttribute> _dataTypeAttributes;

		/// <summary>
		/// Builds or gets the cache of <see cref="DataTypeAttribute"/> attributes.
		/// </summary>
		private static IDictionary<QuantityDataType, DataTypeAttribute> DataTypeAttributes
		{
			get
			{
				if (_dataTypeAttributes == null)
				{
					_dataTypeAttributes = new Dictionary<QuantityDataType, DataTypeAttribute>();
					var keyValuePairs = Enum.GetNames(typeof(QuantityDataType)).Select(name => new
					{
						Key = (QuantityDataType)Enum.Parse(typeof(QuantityDataType), name),
						Value = typeof(QuantityDataType).GetField(name).GetCustomAttribute<DataTypeAttribute>()
					});

					foreach (var keyValuePair in keyValuePairs)
					{
						_dataTypeAttributes[keyValuePair.Key] = keyValuePair.Value;
					}
				}

				return _dataTypeAttributes;
			}
		}

		/// <summary>
		/// Gets the <see cref="Validator"/> corresponding to the quantity data type.
		/// </summary>
		/// <param name="dataType">The quantity data type for which to find the corresponding validator.</param>
		/// <returns>The validator corresponding to the quantity data type.</returns>
		public static Validator GetValidator(this QuantityDataType dataType)
		{
			return DataTypeAttributes[dataType].Validator == null ? null : (Validator)Activator.CreateInstance(DataTypeAttributes[dataType].Validator);
		}

		/// <summary>
		/// Returns whether the quantity data type is supported by the system.
		/// </summary>
		/// <param name="dataType">The data type to evaluate.</param>
		/// <returns><c>true</c> if the data type is supported; otherwise <c>false</c>.</returns>
		public static bool IsSupported(this QuantityDataType dataType)
		{
			return DataTypeAttributes[dataType].Supported;
		}
	}
}
