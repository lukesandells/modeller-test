using System;
using System.Globalization;
using System.Numerics;
using MI.Framework;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
    /// <summary>
    /// Stipulates the data type of a <see cref="Value"/>.
    /// </summary>
    public enum ValueDataType
    {
		/// <summary>
		/// Indicates a monetary amount with a corresponding currency.  Not supported.
		/// </summary>
		/// <remarks>
		/// Not supported yet because an Amount class would need to be defined with a currency and conversions.
		/// </remarks>
		[DataType(Supported = false)]
		Amount,

		/// <summary>
		/// Indicates binary object.  Not supported.
		/// </summary>
		[DataType(Supported = false)]
		BinaryObject,

		/// <summary>
		/// Indicates a <see cref="String"/> that is used to represent an entry from a fixed set of enumerations.
		/// </summary>
		[DataType(typeof(string), Validator = typeof(Validator.Identifier))]
		Code,

		/// <summary>
		/// Indicates a value representing a particular point in time.  Always represented in UTC internally.
		/// Time zone conversions must occur at the point of receiving external data.
		/// </summary>
		[DataType(typeof(DateTime), Default = true)]
		DateTime,

		/// <summary>
		/// Indicates an identifier value.  Derived from <see cref="String"/>.
		/// </summary>
		[DataType(typeof(string), Validator = typeof(Validator.Identifier))]
		Identifier,

		/// <summary>
		/// Indicates an indicator value.  Equivalent to <see cref="Boolean"/>.
		/// </summary>
		[DataType(typeof(bool))]
		Indicator,

		/// <summary>
		/// Indicates a measurement value.
		/// </summary>
		/// <remarks>
		/// A <see cref="Measure"/> must not be confused with a <see cref="SingleQuantity"/>.  A quantity represents how much of something 
		/// there is, while a measure represents a measure of something.  For example, a temperature can be a measure, but not a quantity.
		/// </remarks>
		[DataType(typeof(Measure))]
		Measure,

		/// <summary>
		/// Indicates a numeric value.  Derived from <see cref="Decimal"/>.
		/// </summary>
		[DataType(typeof(decimal))]
		Numeric,

		/// <summary>
		/// Indicates a <see cref="DecimalQuantity"/> value with a <see cref="UnitOfMeasure"/>.  Derived from <see cref="Decimal"/>.
		/// </summary>
		[DataType(typeof(DecimalQuantity))]
		Quantity,

		/// <summary>
		/// Indicates a text value with a corresponding language identifier.
		/// </summary>
		/// <remarks>
		/// A <see cref="Text"/> class has been defined to provide support for this type.  However MI applications have not yet
		/// added support for localisation, and therefore the UI elements don't exist to provide support for this data type.
		/// It is therefore marked as unsupported until the UI element for selecting a language for user-entered text is provided.
		/// The user's configured language could be assumed, however then the difference between the text and string data types would
		/// not be clear to a user.
		/// </remarks>
		[DataType(Supported = false)]
		Text,

		/// <summary>
		/// Indicates a <see cref="String"/> value.
		/// </summary>
		[DataType(typeof(string), Default = true)]
		String,

        /// <summary>
        /// Indicates a signed byte value.
        /// </summary>
		[DataType(typeof(sbyte), RepresentativeType = typeof(decimal), Validator = typeof(Validator.SByte))]
        Byte,

		/// <summary>
		/// Indicates an unsigned byte value.
		/// </summary>
		[DataType(typeof(byte), RepresentativeType = typeof(decimal), Validator = typeof(Validator.Byte))]
		UnsignedByte,

		/// <summary>
		/// Indicates a binary value.
		/// </summary>
		[DataType(Supported = false)]
		Binary,

		/// <summary>
		/// Indicates an unbounded integer value.
		/// </summary>
		[DataType(typeof(BigInteger), Default = true, RepresentativeType = typeof(decimal), Validator = typeof(Validator.BigInteger))]
		Integer,

		/// <summary>
		/// Indicates an unbounded positive integer (with a value greater than zero).
		/// </summary>
		[DataType(typeof(BigInteger), RepresentativeType = typeof(decimal), Validator = typeof(Validator.BigInteger.Positive))]
		PositiveInteger,

		/// <summary>
		/// Indicates an unbounded negative integer (with a value less than zero).
		/// </summary>
		[DataType(typeof(BigInteger), RepresentativeType = typeof(decimal), Validator = typeof(Validator.BigInteger.Negative))]
		NegativeInteger,

		/// <summary>
		/// Indicates an unbounded non-negative integer (with a value of zero or greater).
		/// </summary>
		[DataType(typeof(BigInteger), RepresentativeType = typeof(decimal), Validator = typeof(Validator.BigInteger.NonNegative))]
		NonNegativeInteger,

		/// <summary>
		/// Indicates an unbounded non-positive integer (with a value of zero or less).
		/// </summary>
		[DataType(typeof(BigInteger), RepresentativeType = typeof(decimal), Validator = typeof(Validator.BigInteger.NonPositive))]
		NonPositiveInteger,

		/// <summary>
		/// Indicates a 32-bit integer.
		/// </summary>
		[DataType(typeof(int), RepresentativeType = typeof(decimal), Validator = typeof(Validator.Int32))]
		Int,

		/// <summary>
		/// Indicates an unsigned 32-bit integer.
		/// </summary>
		[DataType(typeof(uint), RepresentativeType = typeof(decimal), Validator = typeof(Validator.UInt32))]
		UnsignedInt,

		/// <summary>
		/// Indicates a 64-bit integer.
		/// </summary>
		[DataType(typeof(long), RepresentativeType = typeof(decimal), Validator = typeof(Validator.Int64))]
		Long,

		/// <summary>
		/// Inicates a 64-bit unsigned integer.
		/// </summary>
		[DataType(typeof(ulong), RepresentativeType = typeof(decimal), Validator = typeof(Validator.UInt64))]
		UnsignedLong,

		/// <summary>
		/// Indicates a 16-bit integer.
		/// </summary>
		[DataType(typeof(short), RepresentativeType = typeof(decimal), Validator = typeof(Validator.Int16))]
		Short,

		/// <summary>
		/// Indicates an unsigned 16-bit integer.
		/// </summary>
		[DataType(typeof(ushort), RepresentativeType = typeof(decimal), Validator = typeof(Validator.UInt16))]
		UnsignedShort,

		/// <summary>
		/// Indicates a decimal.
		/// </summary>
		[DataType(typeof(decimal), Default = true)]
		Decimal,

		/// <summary>
		/// Indicates a single-precision floating point.
		/// </summary>
		[DataType(typeof(float), RepresentativeType = typeof(double), Validator = typeof(Validator.Single))]
		Float,

		/// <summary>
		/// Indicates a double-precision floating point.
		/// </summary>
		[DataType(typeof(double), Validator = typeof(Validator.Double))]
		Double,

		/// <summary>
		/// Indicates a boolen.
		/// </summary>
		[DataType(typeof(bool), Default = true)]
		Boolean,

		/// <summary>
		/// Indicates a time of day.  Not supported.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for a time of day.
		/// </remarks>
		[DataType(Supported = false)]
		Time,

		/// <summary>
		/// Indicates a specific instant of time.  Equivalent to <see cref="DateTime"/>.
		/// </summary>
		[DataType(typeof(DateTime))]
		TimeInstant,

		/// <summary>
		/// Represents a specific period of time with a given start and end.  Not supported.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for a period of time defined between two specific dates.
		/// </remarks>
		[DataType(Supported = false)]
		TimePeriod,

		/// <summary>
		/// Represents a duration of time.
		/// </summary>
		[DataType(typeof(TimeSpan))]
		Duration,

		/// <summary>
		/// Represents a time period that starts at midnight of a specified day and lasts until midnight the following day.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		Date,

		/// <summary>
		/// Represents a time period that starts at midnight on the first day of the month and lasts until the midnight 
		/// that ends the last day of the month.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		Month,

		/// <summary>
		/// Represents a time period that starts at the midnight that starts the first day of the year and ends at the 
		/// midnight that ends the last day of the year
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		Year,

		/// <summary>
		/// Represents a time period that starts at the midnight that starts the first day of the century and ends at the 
		/// midnight that ends that last day of the century.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		Century,

		/// <summary>
		/// A day that recurs, specifically a day of the month such as the 5th of the month.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		RecurringDay,

		/// <summary>
		/// A date that recurs, specifically a day of the year such as the third of May.  Not supported.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		RecurringDate,

		/// <summary>
		/// A specific period of time that recurs with a specific frequency, starting from a specific point in time.  Not supported.
		/// </summary>
		/// <remarks>
		/// There is no native .NET type for this.
		/// </remarks>
		[DataType(Supported = false)]
		RecurringDuration,

		/// <summary>
		/// Indicates the name of an XML element or attribute.  Not supported.
		/// </summary>
		[DataType(Supported = false)]
		Name,

		/// <summary>
		/// An XML qualified name.  Not supported.
		/// </summary>
		[DataType(Supported = false)]
		QName,

		/// <summary>
		/// An XML non-colonised name.  Not supported.
		/// </summary>
		[DataType(Supported = false)]
		NCName,

		/// <summary>
		/// Indicates a URI type.
		/// </summary>
		[DataType(typeof(Uri))]
		UriReference,

		/// <summary>
		/// Indicates a language identifier, natively represented as a <see cref="CultureInfo"/> object.
		/// </summary>
		[DataType(typeof(CultureInfo))]
		Language,

		/// <summary>
		/// Indicates an ID type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		ID,

		/// <summary>
		/// Indicates an IDREF type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		IDREF,

		/// <summary>
		/// Indicates an IDREFS type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		IDREFS,

		/// <summary>
		/// Indicates an ENTITIES type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		ENTITY,

		/// <summary>
		/// Indicates an ENTITIES type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		ENTITIES,

		/// <summary>
		/// Indicates a NOTATION type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		NOTATION,

		/// <summary>
		/// Indicates an NMTOKEN type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		NMTOKEN,

		/// <summary>
		/// Indicates an NMTOKENS type, as defined in the XSD standard. Not supported.
		/// </summary>
		[DataType(Supported = false)]
		NMTOKENS,

		/// <summary>
		/// Indicates an enumeration of a specified type. Not supported.
		/// </summary>
		/// <remarks>
		/// In order to add support for this data type, a facility must be added to register additional enumeration data types.
		/// </remarks>
		[DataType(Supported = false)]
		Enumeration,

		/// <summary>
		/// Indicates a scalable vector graphic (SVG).  Not supported.
		/// </summary>
		[DataType(Supported = false)]
		SVG,

		/// <summary>
		/// A data type defined outside this library.  Not supported.
		/// </summary>
		/// <remarks>
		/// In order to add support for this data type, a facility must be added to register additional data types with corresponding parsers.
		/// </remarks>
		[OtherValue]
		[DataType(Supported = false)]
		Other
    }
}