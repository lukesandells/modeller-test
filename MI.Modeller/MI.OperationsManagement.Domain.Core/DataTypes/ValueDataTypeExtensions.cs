﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Extension methods for the <see cref="ValueDataType"/> enumeration.
	/// </summary>
	public static class ValueDataTypeExtensions
	{
		/// <summary>
		/// Builds or gets the cache of <see cref="DataTypeAttribute"/> attributes.
		/// </summary>
		private static IDictionary<ValueDataType, DataTypeAttribute> DataTypeAttributes
		{
			get
			{
				if (_dataTypeAttributes == null)
				{
					_dataTypeAttributes = new Dictionary<ValueDataType, DataTypeAttribute>();
					var keyValuePairs = Enum.GetNames(typeof(ValueDataType)).Select(name => new
					{
						Key = (ValueDataType)Enum.Parse(typeof(ValueDataType), name),
						Value = typeof(ValueDataType).GetField(name).GetCustomAttribute<DataTypeAttribute>()
					});

					foreach (var keyValuePair in keyValuePairs)
					{
						_dataTypeAttributes[keyValuePair.Key] = keyValuePair.Value;
					}
				}

				return _dataTypeAttributes;
			}
		}

		/// <summary>
		/// Cache of <see cref="DataTypeAttribute"/> attributes against the <see cref="ValueDataType"/> enumeration for improving performance.
		/// </summary>
		private static IDictionary<ValueDataType, DataTypeAttribute> _dataTypeAttributes;

		/// <summary>
		/// Returns the <see cref="Validator"/> corresponding to the value data type.
		/// </summary>
		/// <param name="dataType">The value data type for which to find the corresponding validator.</param>
		/// <returns>The validator corresponding to the value data type.</returns>
		public static Validator GetValidator(this ValueDataType dataType)
		{
			return DataTypeAttributes[dataType].Validator == null ? null : (Validator)Activator.CreateInstance(DataTypeAttributes[dataType].Validator);
		}

		/// <summary>
		/// Returns whether the value data type is supported by the system.
		/// </summary>
		/// <param name="dataType">The data type to evaluate.</param>
		/// <returns><c>true</c> if the data type is supported; otherwise <c>false</c>.</returns>
		public static bool IsSupported(this ValueDataType dataType)
		{
			return DataTypeAttributes[dataType].Supported;
		}

		/// <summary>
		/// Returns the representative type for the value data type.
		/// </summary>
		/// <param name="dataType">The value data type for which to get the representative type.</param>
		/// <returns>The representative type for the value data type.</returns>
		public static Type GetRepresentativeType(this ValueDataType dataType)
		{
			return DataTypeAttributes[dataType].RepresentativeType;
		}

		/// <summary>
		/// Returns the native type for the value data type.
		/// </summary>
		/// <param name="dataType">The value data type for which to get the native type.</param>
		/// <returns>The native type for the value data type.</returns>
		public static Type GetNativeType(this ValueDataType dataType)
		{
			return DataTypeAttributes[dataType].NativeType;
		}
	}
}
