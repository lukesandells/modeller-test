﻿namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	public enum UncertaintyType
	{
		ExpandedUncertainty,
		AccuracyAndPrecision
	}
}
