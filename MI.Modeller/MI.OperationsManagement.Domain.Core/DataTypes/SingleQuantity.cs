﻿using System;
using System.Linq;
using MI.Framework;
using MI.Framework.Units;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	public class SingleQuantity : Quantity
	{
		internal QuantityElement Element => Elements.Single();

		public string Key => Element.Key;

		public bool HasKey => Element.HasKey;

		public ExtensibleEnum<QuantityDataType> DataType => Element.DataType;

		public decimal DecimalValue => Element.DecimalValue;

		public Unit Unit => Element.Unit;

		protected internal UnitDecimal UnitDecimal => Element.UnitDecimal;

		public Uncertainty Uncertainty => Element.Uncertainty;

		public bool HasUncertainty => Uncertainty != null;

		protected SingleQuantity()
		{
		}

		internal SingleQuantity(UnitDecimal unitDecimal) 
			: base(unitDecimal)
		{
		}

		internal SingleQuantity(UnitDecimal unitDecimal, string key = "", ExtensibleEnum<QuantityDataType>? dataType = null, 
			Uncertainty uncertainty = null) 
			: base(unitDecimal, key, dataType ?? QuantityDataType.Decimal, uncertainty)
		{
		}

		/// <summary>
		/// Constructs a new single quantity from a quantity element.
		/// </summary>
		/// <param name="element">The quantity element from which to construct the new quantity.</param>
		/// <returns>A new single quantity constructed from the given quantity element.</returns>
		public static SingleQuantity FromElement(QuantityElement element)
		{
			element = ArgumentValidation.AssertNotNull(element, nameof(element));
			if (!element.HasKey && element.DataType == QuantityDataType.Decimal)
			{
				return DecimalQuantity.FromElement(element);
			}

			return new SingleQuantity(element.UnitDecimal, element.Key, element.DataType, element.Uncertainty);
		}
		
		public SingleQuantity WithKey(string key)
		{
			return new SingleQuantity(UnitDecimal, key, DataType, Uncertainty);
		}

		public SingleQuantity WithUncertainty(Uncertainty uncertainty)
		{
			return new SingleQuantity(UnitDecimal, Key, DataType, uncertainty);
		}

		public SingleQuantity WithDataType(QuantityDataType dataType)
		{
			return new SingleQuantity(UnitDecimal, Key, dataType, Uncertainty);
		}

		public SingleQuantity With(string key = "", QuantityDataType dataType = QuantityDataType.Decimal, 
			Uncertainty uncertainty = null)
		{
			return new SingleQuantity(UnitDecimal, key, dataType, uncertainty);
		}

		/// <summary>
		/// Implicitly casts a <see cref="UnitDecimal"/> to a <see cref="SingleQuantity"/>.
		/// </summary>
		/// <param name="unitDecimal">The unit decimal to cast.</param>
		public static implicit operator SingleQuantity(UnitDecimal unitDecimal)
		{
			return new DecimalQuantity(unitDecimal);
		}

		/// <summary>
		/// Implicitly casts a <see cref="Decimal"/> number to a <see cref="SingleQuantity"/>.
		/// </summary>
		/// <param name="number">The decimal number to cast.</param>
		public static implicit operator SingleQuantity(decimal number)
		{
			return new DecimalQuantity(number);
		}

		public static SingleQuantity operator +(SingleQuantity quantity)
		{
			return QuantityMath<SingleQuantity>.UnaryPlus(quantity);
		}

		public static SingleQuantity operator -(SingleQuantity quantity)
		{
			return QuantityMath<SingleQuantity>.UnaryMinus(quantity);
		}

		public static SingleQuantity operator +(SingleQuantity left, SingleQuantity right)
		{
			return QuantityMath<SingleQuantity>.Add(left, right);
		}

		public static SingleQuantity operator -(SingleQuantity left, SingleQuantity right)
		{
			return QuantityMath<SingleQuantity>.Subtract(left, right);
		}

		public static SingleQuantity operator *(SingleQuantity left, SingleQuantity right)
		{
			return QuantityMath<SingleQuantity>.Multiply(left, right);
		}

		public static SingleQuantity operator *(SingleQuantity quantity, decimal constant)
		{
			return QuantityMath<SingleQuantity>.Multiply(quantity, constant);
		}

		public static SingleQuantity operator *(decimal constant, SingleQuantity quantity)
		{
			return QuantityMath<SingleQuantity>.Multiply(constant, quantity);
		}

		public static SingleQuantity operator /(SingleQuantity left, SingleQuantity right)
		{
			return QuantityMath<SingleQuantity>.Divide(left, right);
		}

		public static SingleQuantity operator /(SingleQuantity quantity, decimal constant)
		{
			return QuantityMath<SingleQuantity>.Divide(quantity, constant);
		}

		public static SingleQuantity operator /(decimal constant, SingleQuantity quantity)
		{
			return QuantityMath<SingleQuantity>.Divide(constant, quantity);
		}

		/// <summary>
		/// Returns a string representation of the single quantity.
		/// </summary>
		/// <returns>A string representation of the single quantity.</returns>
		public override string ToString()
		{
			return Element.ToString();
		}

		/// <summary>
		/// Gets a hash code for the single quantity.
		/// </summary>
		/// <returns>A hash code for the single quantity.</returns>
		public override int GetHashCode()
		{
			return Element.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return Element.Equals(obj);
		}

		/// <summary>
		/// Compares with another <see cref="SingleQuantity"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(SingleQuantity quantity)
		{
			if (quantity == null)
			{
				return false;
			}

			return Element.Equals(quantity.Element);
		}

		/// <summary>
		/// Compares two <see cref="SingleQuantity"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(SingleQuantity left, SingleQuantity right)
		{
			return (Quantity)left == (Quantity)right;
		}

		/// <summary>
		/// Compares two <see cref="SingleQuantity"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(SingleQuantity left, SingleQuantity right)
		{
			return !(left == right);
		}
	}
}
