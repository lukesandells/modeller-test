﻿using System;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Applied to members of the <see cref="ValueDataType"/> enumeration to specify required information for each data type.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = false)]
	internal class DataTypeAttribute : Attribute
	{
		/// <summary>
		/// The native type that best represents the data type.
		/// </summary>
		public Type NativeType { get => _nativeType; set => _nativeType = value; }
		private Type _nativeType;

		/// <summary>
		/// Set this property to <c>true</c> when multiple data types have the same <see cref="NativeType"/> to identify which
		/// data type will represent the native type.
		/// </summary>
		public bool Default { get; set; }

		/// <summary>
		/// The type that will be used to represent values of the data type. Defaults to the <see cref="NativeType"/> if not specified.
		/// </summary>
		public Type RepresentativeType { get => _representativeType; set => _representativeType = value; }
		private Type _representativeType;

		/// <summary>
		/// The type of validator to use to validate instances of the <see cref="RepresentativeType"/>.
		/// </summary>
		/// <remarks>
		/// A validator can be specified to enforce constraints beyond those which are enforced by the <see cref="RepresentativeType"/>.
		/// </remarks>
		public Type Validator { get; set; }

		/// <summary>
		/// Indicates whether the data type is supported by this library.
		/// </summary>
		public bool Supported { get; set; } = true;

		/// <summary>
		/// Initialises a new instance of the <see cref="DataTypeAttribute"/> class.
		/// </summary>
		public DataTypeAttribute()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DataTypeAttribute"/> class.
		/// </summary>
		/// <param name="nativeType">The internal type used to represent the data type.</param>
		public DataTypeAttribute(Type nativeType)
		{
			_nativeType = ArgumentValidation.AssertNotNull(nativeType, nameof(nativeType));
			_representativeType = _nativeType;
		}
	}
}
