﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using MI.Framework;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Extension methods for the <see cref="Type"/> class.
	/// </summary>
	public static class TypeExtensions
	{
		/// <summary>
		/// Builds or gets the cache of value data types by native type.
		/// </summary>
		private static IDictionary<Type, ValueDataType> ValueDataTypesByNativeType
		{
			get
			{
				if (_valueDataTypesByNativeType == null)
				{
					var dataTypeAttributes = Enum.GetNames(typeof(ValueDataType))
						.Select(name => new
						{
							DataType = (ValueDataType)Enum.Parse(typeof(ValueDataType), name),
							Attribute = typeof(ValueDataType).GetField(name).GetCustomAttribute<DataTypeAttribute>()
						});

					var nativeTypeGroupings = dataTypeAttributes
						.Where(element => element.Attribute.Supported)
						.GroupBy(element => element.Attribute.NativeType);
					var invalidNativeTypeGroupings = nativeTypeGroupings
						.Where(g => g.Count() > 1 && g.Where(element => element.Attribute.Default).Count() != 1);

					if (invalidNativeTypeGroupings.Count() != 0)
					{
						throw new OperationsManagementException(string.Format(ExceptionMessage.MultipleDataTypesFoundForNativeType,
							invalidNativeTypeGroupings.First().Key));
					}

					_valueDataTypesByNativeType = new Dictionary<Type, ValueDataType>();
					nativeTypeGroupings.Select(g => g.Count() == 1 ? g.Single() : g.Where(element => element.Attribute.Default).Single())
						.ForEach(element => _valueDataTypesByNativeType.Add(element.Attribute.NativeType, element.DataType));
				}

				return _valueDataTypesByNativeType;
			}
		}

		/// <summary>
		/// Cache of value data types by native type, built from the <see cref="DataTypeAttribute"/> attributes against the <see cref="ValueDataType"/>
		/// enumeration for improving performance.
		/// </summary>
		private static IDictionary<Type, ValueDataType> _valueDataTypesByNativeType;

		/// <summary>
		/// Return the <see cref="ValueDataType"/> with a <see cref="DataTypeAttribute.NativeType"/> equal to the type.  If multiple value data types
		/// have the same native type, then the method returns the data type with a <see cref="DataTypeAttribute"/> with 
		/// <see cref="DataTypeAttribute.Default"/> = <c>true</c>.
		/// </summary>
		/// <param name="type">The type for which to find the corresponding value data type.</param>
		/// <param name="dataType">The value data type corresponding to the type.</param>
		/// <returns><c>true</c> if a corresponding data type was found; otherwise <c>false</c>.</returns>
		public static bool TryGetValueDataType(this Type type, out ValueDataType dataType)
		{
			return ValueDataTypesByNativeType.TryGetValue(type, out dataType);
		}
	}
}
