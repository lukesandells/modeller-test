﻿using Newtonsoft.Json;
using System.Collections.Generic;
using MI.Framework.Units;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// A serialised <see cref="Value"/>.
	/// </summary>
	public class SerialisedValue
	{
		/// <summary>
		/// The serialised value elements.
		/// </summary>
		public ISet<Element> Elements { get; } = new HashSet<Element>();

		/// <summary>
		/// A serialised <see cref="ValueElement"/>.
		/// </summary>
		public class Element
		{
			/// <summary>
			/// A string representation of the value data type.
			/// </summary>
			public string DataType { get; set; }

			/// <summary>
			/// A key that uniquely identifies the value element.
			/// </summary>
			public string Key { get; set; } = string.Empty;

			/// <summary>
			/// A string representation of the value.
			/// </summary>
			public string Value { get; set; }

			/// <summary>
			/// A unit code, in accordance with the current unit library (see <see cref="UnitLibrary.Current"/>).
			/// </summary>
			public string UnitCode { get; set; }

			/// <summary>
			/// A language code in accordance with ISO 639: 1988.
			/// </summary>
			public string LanguageCode { get; set; }

			/// <summary>
			/// A currency code in accordance with ISO 4217.
			/// </summary>
			public string CurrencyCode { get; set; }

			/// <summary>
			/// The elements of an uncertainty, represented as strings.
			/// </summary>
			public SerialisedUncertainty Uncertainty { get; set; }
		}

		/// <summary>
		/// Returns a JSON representation of the serialised value.
		/// </summary>
		/// <returns>A JSON representation of the serialised value.</returns>
		public override string ToString()
		{
			return JsonConvert.SerializeObject(this);
		}

		/// <summary>
		/// Parses a JSON reprsentation of a serialised value.
		/// </summary>
		/// <param name="s">The JSON representation of the serialised value to parse.</param>
		/// <returns>The result of parsing the given string.</returns>
		public static SerialisedValue Parse(string s)
		{
			return JsonConvert.DeserializeObject<SerialisedValue>(s);
		}

		/// <summary>
		/// Deserialises the serialised value.
		/// </summary>
		/// <returns>The deserialised value.</returns>
		public Value Deserialise()
		{
			return Value.Deserialise(this);
		}
	}
}
