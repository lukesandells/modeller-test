﻿using Newtonsoft.Json;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// A serialised uncertainty.
	/// </summary>
	public class SerialisedUncertainty
	{
		/// <summary>
		/// The type of uncertainty.
		/// </summary>
		public UncertaintyType UncertaintyType { get; set; }

		/// <summary>
		/// A string representation of the accuracy.
		/// </summary>
		public string Accuracy { get; set; }

		/// <summary>
		/// A string rperesentation of the precision.
		/// </summary>
		public string Precision { get; set; }

		/// <summary>
		/// A string representation of the standard uncertainty.
		/// </summary>
		public string StandardUncertainty { get; set; }

		/// <summary>
		/// A string representation of the expanded uncertainty.
		/// </summary>
		public string ExpandedUncertainty { get; set; }

		/// <summary>
		/// A string representation of the coverage factor.
		/// </summary>
		public string CoverageFactor { get; set; }

		/// <summary>
		/// A string representation of the confidence level.
		/// </summary>
		public string ConfidenceLevel { get; set; }

		/// <summary>
		/// Returns a JSON representation of the serialised uncertainty.
		/// </summary>
		/// <returns>A JSON representation of the serialised uncertainty.</returns>
		public override string ToString()
		{
			return JsonConvert.SerializeObject(this);
		}

		/// <summary>
		/// Parses a JSON reprsentation of a serialised uncertainty.
		/// </summary>
		/// <param name="s">The JSON representation of the serialised uncertainty to parse.</param>
		/// <returns>The result of parsing the given string.</returns>
		public static SerialisedUncertainty Parse(string s)
		{
			return JsonConvert.DeserializeObject<SerialisedUncertainty>(s);
		}

		/// <summary>
		/// Deserialises the serialised uncertainty.
		/// </summary>
		/// <returns>The deserialised uncertainty.</returns>
		public Uncertainty Deserialise()
		{
			return Uncertainty.Deserialise(this);
		}
	}
}
