﻿using System;
using System.Globalization;
using System.Xml;
using System.Linq;
using MI.Framework;
using MI.Framework.Units;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents an element of a <see cref="Value"/> object.
	/// </summary>
	public class ValueElement : ICopyable<ValueElement>
	{
		/// <summary>
		/// The data type of the value;
		/// </summary>
		public ExtensibleEnum<ValueDataType> DataType => _dataType;
		private ExtensibleEnum<ValueDataType> _dataType;

		/// <summary>
		/// A key uniquely identifying the value element.
		/// </summary>
		public string Key => _key;
		private string _key = string.Empty;

		/// <summary>
		/// The string value representing the value element when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// of <see cref="String"/> or <see cref="Uri"/>.
		/// </summary>
		private string _stringValue;

		/// <summary>
		/// The decimal value representing the value element when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// of <see cref="Decimal"/>.
		/// </summary>
		private decimal? _decimalValue;

		/// <summary>
		/// The decimal value representing the value element when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// of <see cref="Double"/>.
		/// </summary>
		private double? _doubleValue;

		/// <summary>
		/// The decimal value representing the value element when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// of <see cref="Boolean"/>.
		/// </summary>
		private bool? _booleanValue;

		/// <summary>
		/// The decimal value representing the value element when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// of <see cref="DateTime"/>.
		/// </summary>
		private DateTime? _dateTimeValue;

		/// <summary>
		/// The decimal value representing the value element when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// of <see cref="TimeSpan"/>.
		/// </summary>
		private TimeSpan? _timeSpanValue;

		/// <summary>
		/// The unit of measure for the value element for when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// with a unit of measure.
		/// </summary>
		private Unit _unit;

		/// <summary>
		/// The language information for the value element for when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// with language information.
		/// </summary>
		private CultureInfo _cultureInfo;

		/// <summary>
		/// The measurement uncertainty for the value element for when the <see cref="DataType"/> has a <see cref="DataTypeAttribute.RepresentativeType"/>
		/// with measurement uncertainty.
		/// </summary>
		private Uncertainty _uncertainty;

		/// <summary>
		/// Initialises a new instance of the <see cref="ValueElement"/> class.
		/// </summary>
		protected ValueElement()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ValueElement"/> class.
		/// </summary>
		/// <param name="value">An instance of a <see cref="DataTypeAttribute.RepresentativeType"/>.</param>
		internal ValueElement(object value) : this(value, string.Empty)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ValueElement"/> class.
		/// </summary>
		/// <param name="value">An instance of a <see cref="DataTypeAttribute.RepresentativeType"/>.</param>
		/// <param name="key">A key uniquely identifying the value element within a <see cref="Value"/>.</param>
		internal ValueElement(object value, string key)
		{
			// Figure out the value data type from the given object's type
			value = ArgumentValidation.AssertNotNull(value, nameof(value));
			if (!value.GetType().TryGetValueDataType(out var dataType))
			{
				throw new ArgumentOutOfRangeException(ExceptionMessage.ObjectTypeNotSupported);
			}

			if (!dataType.IsSupported())
			{
				throw new ArgumentException(ExceptionMessage.ObjectTypeNotSupported);
			}

			_dataType = dataType;
			_key = key;

			// Disassemble the object into its components
			var representativeType = dataType.GetRepresentativeType();
			if (representativeType == typeof(string))
			{
				value = _stringValue = (string)value;
			}
			else if (representativeType == typeof(decimal))
			{
				value = _decimalValue = Convert.ToDecimal(value);
			}
			else if (representativeType == typeof(double))
			{
				value = _doubleValue = Convert.ToDouble(value);
			}
			else if (representativeType == typeof(bool))
			{
				value = _booleanValue = Convert.ToBoolean(value);
			}
			else if (representativeType == typeof(DateTime))
			{
				value = _dateTimeValue = Convert.ToDateTime(value);
			}
			else if (representativeType == typeof(TimeSpan))
			{
				value = _timeSpanValue = (TimeSpan)value;
			}
			else if (representativeType == typeof(Uri))
			{
				_stringValue = value.ToString();
				value = new Uri(_stringValue);
			}
			else if (representativeType == typeof(DecimalQuantity))
			{
				var quantity = (DecimalQuantity)value;
				_decimalValue = quantity.DecimalValue;
				_unit = quantity.Unit;
				_uncertainty = quantity.Uncertainty;
				value = quantity;
			}
			else if (representativeType == typeof(Measure))
			{
				var measure = (Measure)value;
				_decimalValue = measure.DecimalValue;
				_unit = measure.Unit;
				_uncertainty = measure.Uncertainty;
				value = measure;
			}
			else if (representativeType == typeof(Text))
			{
				var text = (Text)value;
				_stringValue = text.ToString();
				_cultureInfo = text.CultureInfo;
				value = text;
			}
			else
			{
				throw new ArgumentOutOfRangeException(ExceptionMessage.ObjectTypeNotSupported);
			}

			// Validate the object if a validator is declared
			var validator = dataType.GetValidator();
			if (!(validator?.IsValid(value) ?? true))
			{
				throw new ArgumentOutOfRangeException(ExceptionMessage.ObjectValueOutOfRange);
			}
		}

		/// <summary>
		/// Returns an appropriate <see cref="Object"/> representing the value of the value element in accordance with the value element's data type.
		/// </summary>
		/// <returns>An object representing the value of the value element.</returns>
		public object ToObject()
		{
			var representativeType = ((ValueDataType)DataType).GetRepresentativeType();
			if (representativeType == typeof(string))
			{
				return _stringValue;
			}
			else if (representativeType == typeof(decimal))
			{
				return Convert.ChangeType(_decimalValue, ((ValueDataType)DataType).GetNativeType());
			}
			else if (representativeType == typeof(double))
			{
				return _doubleValue;
			}
			else if (representativeType == typeof(bool))
			{
				return _booleanValue;
			}
			else if (representativeType == typeof(DateTime))
			{
				return _dateTimeValue;
			}
			else if (representativeType == typeof(TimeSpan))
			{
				return _timeSpanValue;
			}
			else if (representativeType == typeof(Uri))
			{
				return new Uri(_stringValue);
			}
			else if (representativeType == typeof(DecimalQuantity))
			{
				return _decimalValue.Value.WithUnit(_unit).AsQuantity(_uncertainty);
			}
			else if (representativeType == typeof(Measure))
			{
				return _decimalValue.Value.WithUnit(_unit).AsMeasure(_uncertainty);
			}
			else if (representativeType == typeof(CultureInfo))
			{
				return _cultureInfo;
			}
			else if (representativeType == typeof(Text))
			{
				return new Text(_stringValue, _cultureInfo);
			}
			else
			{
				throw new InvalidOperationException(ExceptionMessage.ObjectTypeNotSupported);
			}
		}

		/// <summary>
		/// Returns a copy of the quantity element.
		/// </summary>
		/// <returns>A copy of the quantity element.</returns>
		public ValueElement Copy()
		{
			return new ValueElement
			{
				_dataType = _dataType,
				_booleanValue = _booleanValue,
				_cultureInfo = _cultureInfo,
				_dateTimeValue = _dateTimeValue,
				_decimalValue = _decimalValue,
				_doubleValue = _doubleValue,
				_key = _key,
				_stringValue = _stringValue,
				_timeSpanValue = _timeSpanValue,
				_uncertainty = _uncertainty,
				_unit = _unit
			};
		}

		/// <summary>
		/// Returns a string representation of the value element.
		/// </summary>
		/// <returns>A string representation of the value element.</returns>
		public override string ToString()
		{
			var keyString = Key == string.Empty ? Key : string.Format("{0}: ", Key);
			return keyString + string.Format("{0} ({1})", ToObject(), DataType);
		}

		/// <summary>
		/// Serialises the value element.
		/// </summary>
		/// <returns>The serialised value element.</returns>
		internal SerialisedValue.Element Serialise()
		{
			var serialisedElement = new SerialisedValue.Element();
			if (_stringValue != null)
			{
				serialisedElement.Value = _stringValue;
			}
			else if (_decimalValue.HasValue)
			{
				serialisedElement.Value = _decimalValue.Value.ToString("G29");
			}
			else if (_doubleValue.HasValue)
			{
				serialisedElement.Value = _doubleValue.Value.ToString();
			}
			else if (_dateTimeValue.HasValue)
			{
				serialisedElement.Value = _dateTimeValue.Value.ToString("o");
			}
			else if (_booleanValue.HasValue)
			{
				serialisedElement.Value = _booleanValue.Value.ToString();
			}
			else if (_timeSpanValue.HasValue)
			{
				serialisedElement.Value = XmlConvert.ToString(_timeSpanValue.Value);
			}

			serialisedElement.DataType = DataType.ToString();
			serialisedElement.UnitCode = _unit?.Code;
			serialisedElement.Key = Key;
			serialisedElement.Uncertainty = _uncertainty?.Serialise();
			serialisedElement.LanguageCode = _cultureInfo?.ThreeLetterISOLanguageName;

			return serialisedElement;
		}

		/// <summary>
		/// Deserialises a value element.
		/// </summary>
		/// <param name="serialisedElement">The serialised value element to deserialise.</param>
		/// <returns>The deserialised value element.</returns>
		internal static ValueElement Deserialise(SerialisedValue.Element serialisedElement)
		{
			// Attempt to parse the data type
			if (!Enum.TryParse<ValueDataType>(serialisedElement.DataType, out var dataType))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.UnrecognisedValueDataType, serialisedElement.DataType), nameof(serialisedElement));
			}

			// Check data type is supported
			if (!dataType.IsSupported())
			{
				throw new ArgumentException(string.Format(ExceptionMessage.UnsupportedValueDataType, serialisedElement.DataType), nameof(serialisedElement));
			}

			// Create the new value element to be populated
			var element = new ValueElement()
			{
				_key = serialisedElement.Key,
				_dataType = dataType
			};

			// Deserialise the uncertainty if one was provided
			if (serialisedElement.Uncertainty != null)
			{
				element._uncertainty = Uncertainty.Deserialise(serialisedElement.Uncertainty);
			}

			// Populate the value element in accordance with the representative type of the data type
			var representativeType = dataType.GetRepresentativeType();
			if (representativeType == typeof(string))
			{
				element._stringValue = serialisedElement.Value;
			}
			else if (representativeType == typeof(decimal))
			{
				if (!decimal.TryParse(serialisedElement.Value, out var decimalValue))
				{
					ThrowValueFormatException();
				}

				element._decimalValue = decimalValue;
			}
			else if (representativeType == typeof(double))
			{
				if (!double.TryParse(serialisedElement.Value, out var doubleValue))
				{
					ThrowValueFormatException();
				}

				element._doubleValue = doubleValue;
			}
			else if (representativeType == typeof(bool))
			{
				if (!bool.TryParse(serialisedElement.Value, out var booleanValue))
				{
					ThrowValueFormatException();
				}

				element._booleanValue = booleanValue;
			}
			else if (representativeType == typeof(DateTime))
			{
				if (!DateTime.TryParse(serialisedElement.Value, null, DateTimeStyles.RoundtripKind, out var dateTimeValue))
				{
					ThrowValueFormatException();
				}

				element._dateTimeValue = dateTimeValue;
			}
			else if (representativeType == typeof(TimeSpan))
			{
				TimeSpan timeSpan;
				try
				{
					timeSpan = XmlConvert.ToTimeSpan(serialisedElement.Value);
				}
				catch (FormatException)
				{
					ThrowValueFormatException();
				}

				element._timeSpanValue = timeSpan;
			}
			else if (representativeType == typeof(Uri))
			{
				if (!Uri.TryCreate(serialisedElement.Value, UriKind.Absolute, out var uri))
				{
					ThrowValueFormatException();
				}
			}
			else if (representativeType == typeof(DecimalQuantity) || representativeType == typeof(Measure))
			{
				if (!UnitDecimal.TryParse(serialisedElement.Value, serialisedElement.UnitCode, out var unitDecimal))
				{
					ThrowValueFormatException();
				}

				element._decimalValue = unitDecimal.DecimalValue;
				element._unit = unitDecimal.Unit;
			}
			else if (representativeType == typeof(CultureInfo) || representativeType == typeof(Text))
			{
				var languageCode = representativeType == typeof(CultureInfo) ? serialisedElement.Value : serialisedElement.LanguageCode;
				var cultureInfo = CultureInfo.GetCultures(CultureTypes.AllCultures).Where(c => c.ThreeLetterISOLanguageName == languageCode).FirstOrDefault();
				if (cultureInfo == null)
				{
					ThrowValueFormatException();
				}

				element._cultureInfo = cultureInfo;
				element._stringValue = serialisedElement.Value;
			}
			else
			{
				throw new InvalidOperationException(ExceptionMessage.ObjectTypeNotSupported);
			}

			return element;

			// Throws a format exception for the value
			void ThrowValueFormatException()
			{
				throw new FormatException(string.Format(ExceptionMessage.FormattingOfValueInconsistentWithDataType,
						serialisedElement.Value, serialisedElement.DataType));
			}
		}

		/// <summary>
		/// Gets a hash code for the quantity element.
		/// </summary>
		/// <returns>A hash code for the quantity element.</returns>
		public override int GetHashCode()
		{
			return (Key, ToObject()).GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var valueElement = obj as ValueElement;
			if (valueElement == null)
			{
				return false;
			}

			// Compare the two
			return valueElement.ToObject().Equals(ToObject()) && valueElement.Key == Key;
		}

		/// <summary>
		/// Compares with another <see cref="ValueElement"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(ValueElement valueElement)
		{
			if (valueElement == null)
			{
				return false;
			}

			// Compare the two
			return valueElement.ToObject().Equals(ToObject()) && valueElement.Key == Key;
		}

		/// <summary>
		/// Compares two <see cref="ValueElement"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(ValueElement left, ValueElement right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.ToObject().Equals(right.ToObject()) && left.Key == right.Key;
		}

		/// <summary>
		/// Compares two <see cref="ValueElement"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(ValueElement left, ValueElement right)
		{
			return !(left == right);
		}
	}
}
