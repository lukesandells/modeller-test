﻿using System;
using System.Globalization;
using System.Numerics;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Extension methods for converting objects of various types to <see cref="SingleValue"/> objects.
	/// </summary>
	public static class AsValueExtensions
	{
		/// <summary>
		/// Returns the quantity as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="quantity">The quantity to return as a new <see cref="Value"/>.</param>
		/// <returns>The given quantity as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Quantity quantity)
		{
			return new SingleValue(quantity);
		}

		/// <summary>
		/// Returns the quantity as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="quantity">The quantity to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given quantity within the new <see cref="Value"/>.</param>
		/// <returns>The given quantity as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Quantity quantity, string key)
		{
			return new SingleValue(quantity, key);
		}

		/// <summary>
		/// Returns the string as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="stringValue">The string to return as a new <see cref="Value"/>.</param>
		/// <returns>The given string as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this string stringValue)
		{
			return new SingleValue(stringValue);
		}

		/// <summary>
		/// Returns the string as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="stringValue">The string to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given string within the new <see cref="Value"/>.</param>
		/// <returns>The given string as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this string stringValue, string key)
		{
			return new SingleValue(stringValue, key);
		}

		/// <summary>
		/// Returns the boolean as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="boolean">The boolean to return as a new <see cref="Value"/>.</param>
		/// <returns>The given boolean as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this bool boolean)
		{
			return new SingleValue(boolean);
		}

		/// <summary>
		/// Returns the boolean as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="boolean">The boolean to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given boolean within the new <see cref="Value"/>.</param>
		/// <returns>The given boolean as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this bool boolean, string key)
		{
			return new SingleValue(boolean, key);
		}

		/// <summary>
		/// Returns the decimal as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The decimal to return as a new <see cref="Value"/>.</param>
		/// <returns>The given decimal as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this decimal number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the decimal as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The decimal to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given decimal within the new <see cref="Value"/>.</param>
		/// <returns>The given decimal as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this decimal number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the 32-bit signed integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 32-bit signed integer to return as a new <see cref="Value"/>.</param>
		/// <returns>The given 32-bit signed integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this int number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the 32-bit signed integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 32-bit signed integer to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given 32-bit signed integer within the new <see cref="Value"/>.</param>
		/// <returns>The given 32-bit signed integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this int number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the 32-bit unsigned integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 32-bit unsigned integer to return as a new <see cref="Value"/>.</param>
		/// <returns>The given 32-bit unsigned integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this uint number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the 32-bit unsigned integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 32-bit unsigned integer to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given 32-bit unsigned integer within the new <see cref="Value"/>.</param>
		/// <returns>The given 32-bit unsigned integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this uint number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the 16-bit signed integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 16-bit signed integer to return as a new <see cref="Value"/>.</param>
		/// <returns>The given 16-bit signed integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this short number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the 16-bit signed integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 16-bit signed integer to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given 16-bit signed integer within the new <see cref="Value"/>.</param>
		/// <returns>The given 16-bit signed integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this short number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the 16-bit unsigned integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 16-bit unsigned integer to return as a new <see cref="Value"/>.</param>
		/// <returns>The given 16-bit unsigned integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this ushort number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the 16-bit unsigned integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 16-bit unsigned integer to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given 16-bit unsigned integer within the new <see cref="Value"/>.</param>
		/// <returns>The given 16-bit unsigned integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this ushort number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the 64-bit signed integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 64-bit signed integer to return as a new <see cref="Value"/>.</param>
		/// <returns>The given 64-bit signed integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this long number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the 64-bit signed integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 64-bit signed integer to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given 64-bit signed integer within the new <see cref="Value"/>.</param>
		/// <returns>The given 64-bit signed integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this long number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the 64-bit unsigned integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 64-bit unsigned integer to return as a new <see cref="Value"/>.</param>
		/// <returns>The given 64-bit unsigned integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this ulong number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the 64-bit unsigned integer as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The 64-bit unsigned integer to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given 64-bit unsigned integer within the new <see cref="Value"/>.</param>
		/// <returns>The given 64-bit unsigned integer as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this ulong number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the byte (8-bit unsigned integer) as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The byte (8-bit unsigned integer) to return as a new <see cref="Value"/>.</param>
		/// <returns>The given byte (8-bit unsigned integer) as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this byte number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the byte (8-bit unsigned integer) as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The byte (8-bit unsigned integer) to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given byte (8-bit unsigned integer) within the new <see cref="Value"/>.</param>
		/// <returns>The given byte (8-bit unsigned integer) as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this byte number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the signed byte (8-bit integer) as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The signed byte (8-bit integer) to return as a new <see cref="Value"/>.</param>
		/// <returns>The given signed byte (8-bit integer) as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this sbyte number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the signed byte (8-bit integer) as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The signed byte (8-bit integer) to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given signed byte (8-bit integer) within the new <see cref="Value"/>.</param>
		/// <returns>The given signed byte (8-bit integer) as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this sbyte number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the <see cref="BigInteger"/> (unbounded integer) as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="BigInteger"/> (unbounded integer) to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="BigInteger"/> (unbounded integer) as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this BigInteger number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the <see cref="BigInteger"/> (unbounded integer) as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="BigInteger"/> (unbounded integer) to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="BigInteger"/> (unbounded integer) within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="BigInteger"/> (unbounded integer) as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this BigInteger number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the single-precision floating point as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The single-precision floating point to return as a new <see cref="Value"/>.</param>
		/// <returns>The given single-precision floating point as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this float number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the single-precision floating point as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The single-precision floating point to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given single-precision floating point within the new <see cref="Value"/>.</param>
		/// <returns>The given single-precision floating point as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this float number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the double-precision floating point as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The double-precision floating point to return as a new <see cref="Value"/>.</param>
		/// <returns>The given double-precision floating point as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this double number)
		{
			return new SingleValue(number);
		}

		/// <summary>
		/// Returns the double-precision floating point as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The double-precision floating point to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given double-precision floating point within the new <see cref="Value"/>.</param>
		/// <returns>The given double-precision floating point as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this double number, string key)
		{
			return new SingleValue(number, key);
		}

		/// <summary>
		/// Returns the <see cref="DateTime"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="DateTime"/> to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="DateTime"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this DateTime dateTime)
		{
			return new SingleValue(dateTime);
		}

		/// <summary>
		/// Returns the <see cref="DateTime"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="DateTime"/> to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="DateTime"/> within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="DateTime"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this DateTime dateTime, string key)
		{
			return new SingleValue(dateTime, key);
		}

		/// <summary>
		/// Returns the <see cref="TimeSpan"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="TimeSpan"/> to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="TimeSpan"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this TimeSpan timeSpan)
		{
			return new SingleValue(timeSpan);
		}

		/// <summary>
		/// Returns the <see cref="TimeSpan"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="TimeSpan"/> to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="TimeSpan"/> within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="TimeSpan"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this TimeSpan timeSpan, string key)
		{
			return new SingleValue(timeSpan, key);
		}

		/// <summary>
		/// Returns the <see cref="Measure"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="Measure"/> to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="Measure"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Measure measure)
		{
			return new SingleValue(measure);
		}

		/// <summary>
		/// Returns the <see cref="Measure"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="Measure"/> to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="Measure"/> within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="Measure"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Measure measure, string key)
		{
			return new SingleValue(measure, key);
		}

		/// <summary>
		/// Returns the <see cref="Text"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="Text"/> to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="Text"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Text text)
		{
			return new SingleValue(text);
		}

		/// <summary>
		/// Returns the <see cref="Text"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="Text"/> to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="Text"/> within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="Text"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Text text, string key)
		{
			return new SingleValue(text, key);
		}

		/// <summary>
		/// Returns the <see cref="CultureInfo"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="CultureInfo"/> to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="CultureInfo"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this CultureInfo cultureInfo)
		{
			return new SingleValue(cultureInfo);
		}

		/// <summary>
		/// Returns the <see cref="CultureInfo"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="CultureInfo"/> to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="CultureInfo"/> within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="CultureInfo"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this CultureInfo cultureInfo, string key)
		{
			return new SingleValue(cultureInfo, key);
		}

		/// <summary>
		/// Returns the <see cref="Uri"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="Uri"/> to return as a new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="Uri"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Uri uri)
		{
			return new SingleValue(uri);
		}

		/// <summary>
		/// Returns the <see cref="Uri"/> as a new <see cref="Value"/>.
		/// </summary>
		/// <param name="number">The <see cref="Uri"/> to return as a new <see cref="Value"/>.</param>
		/// <param name="key">Uniquely identifies the given <see cref="Uri"/> within the new <see cref="Value"/>.</param>
		/// <returns>The given <see cref="Uri"/> as a new <see cref="Value"/>.</returns>
		public static SingleValue AsValue(this Uri uri, string key)
		{
			return new SingleValue(uri, key);
		}
	}
}
