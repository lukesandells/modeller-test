﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Numerics;
using System.Text;
using MI.Framework;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Represents a value, comprising one or more elements (see <see cref="ValueElement"/>), each with a value of a given <see cref="ValueDataType"/>.
	/// </summary>
	public class Value : IEnumerable<ValueElement>, ICopyable<Value>
	{
		public IEnumerable<ValueElement> Elements => _elements.Values;

		private IDictionary<string, ValueElement> _elements = new Dictionary<string, ValueElement>();

		public ValueElement this[string key] => _elements[key];

		/// <summary>
		/// Initialises a new instance of the <see cref="Value"/> class.
		/// </summary>
		protected Value()
		{
		}

		internal Value(object value) : this(value, string.Empty)
		{
		}

		internal Value(object value, string key)
		{
			// Create a single quantity element to hold the given values
			_elements[key] = new ValueElement(value, key);
		}

		/// <summary>
		/// Constructs a new value from a collection of value elements.
		/// </summary>
		/// <param name="elements">The value elements with which to construct the new value.</param>
		/// <returns>A new value constructed from the given collection of value elements</returns>
		public static Value FromElements(IEnumerable<ValueElement> elements)
		{
			elements = ArgumentValidation.AssertNotNull(elements, nameof(elements));
			var value = new Value() { _elements = elements.Copy().ToDictionary(e => e.Key) };
			return value;
		}

		public bool ContainsKey(string key)
		{
			return _elements.ContainsKey(key);
		}

		public Value And(SingleValue value)
		{
			value = ArgumentValidation.AssertNotNull(value, nameof(value));
			var newValue = Copy();
			newValue._elements.Add(value.Key, value.Element.Copy());
			return newValue;
		}

		public Value Copy()
		{
			var copy = new Value();
			Elements.ForEach(e => copy._elements.Add(e.Key, e.Copy()));
			return copy;
		}

		public static implicit operator Value(DecimalQuantity quantity)
		{
			return new SingleValue(quantity);
		}

		public static explicit operator Value(Quantity quantity)
		{
			quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
			var singleQuantity = quantity as SingleQuantity;
			if (singleQuantity == null)
			{
				if (quantity.Elements.Count() > 1)
				{
					throw new InvalidOperationException(ExceptionMessage.CannotRepresentMultiElementQuantityAsValue);
				}

				singleQuantity = SingleQuantity.FromElement(quantity.Elements.Single());
			}

			if (singleQuantity.HasKey)
			{
				throw new InvalidOperationException(ExceptionMessage.CannotRepresentQuantityWithKeyAsValue);
			}

			if (singleQuantity.DataType != QuantityDataType.Decimal)
			{
				throw new InvalidOperationException(ExceptionMessage.CannotRepresentNonDecimalQuantityAsValue);
			}

			return new SingleValue(quantity as DecimalQuantity ?? new DecimalQuantity(singleQuantity.UnitDecimal, singleQuantity.Uncertainty));
		}

		public static implicit operator Value(string stringValue)
		{
			return new SingleValue(stringValue);
		}

		public static implicit operator Value(bool boolean)
		{
			return new SingleValue(boolean);
		}

		public static implicit operator Value(decimal number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(int number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(uint number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(float number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(double number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(short number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(ushort number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(long number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(ulong number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(byte number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(sbyte number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(BigInteger number)
		{
			return new SingleValue(number);
		}

		public static implicit operator Value(DateTime dateTime)
		{
			return new SingleValue(dateTime);
		}

		public static implicit operator Value(TimeSpan timeSpan)
		{
			return new SingleValue(timeSpan);
		}

		public static implicit operator Value(Measure measure)
		{
			return new SingleValue(measure);
		}

		public static implicit operator Value(Text text)
		{
			return new SingleValue(text);
		}

		public static implicit operator Value(CultureInfo cultureInfo)
		{
			return new SingleValue(cultureInfo);
		}

		public static implicit operator Value(Uri uri)
		{
			return new SingleValue(uri);
		}

		public override string ToString()
		{
			return string.Join("; ", Elements);
		}

		public SerialisedValue Serialise()
		{
			var serialisedValue = new SerialisedValue();
			Elements.ForEach(element => serialisedValue.Elements.Add(element.Serialise()));
			return serialisedValue;
		}

		public static Value Deserialise(SerialisedValue serialisedValue)
		{
			serialisedValue = ArgumentValidation.AssertNotNull(serialisedValue, nameof(serialisedValue));
			if (serialisedValue.Elements.Count == 0)
			{
				return null;
			}

			return FromElements(serialisedValue.Elements.Select(element => ValueElement.Deserialise(element)));
		}

		/// <summary>
		/// Gets a hash code for the quantity.
		/// </summary>
		/// <returns>A hash code for the quantity.</returns>
		public override int GetHashCode()
		{
			return Elements.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var value = obj as Value;
			if (value == null)
			{
				return false;
			}

			// Not equal if differnet number of elements
			if (value.Elements.Count() != Elements.Count())
			{
				return false;
			}

			// Compare the two
			return Equals(value);
		}

		/// <summary>
		/// Compares with another <see cref="Value"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(Value value)
		{
			if (value == null)
			{
				return false;
			}

			// Compare the two
			foreach (var element in value.Elements)
			{
				if (!_elements.TryGetValue(element.Key, out var otherElement))
				{
					return false;
				}

				if (element != otherElement)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection of vaule elements.
		/// </summary>
		/// <returns>An enumerator that iterates through the collection of value elements.</returns>
		public IEnumerator<ValueElement> GetEnumerator()
		{
			return _elements.Values.GetEnumerator();
		}

		/// <summary>
		/// Returns an enumerator that iterates through the collection of value elements.
		/// </summary>
		/// <returns>An enumerator that iterates through the collection of value elements.</returns>
		IEnumerator IEnumerable.GetEnumerator()
		{
			return _elements.Values.GetEnumerator();
		}

		/// <summary>
		/// Compares two <see cref="Value"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(Value left, Value right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// Due to the way that components are mapped the value may be non-null but have an empty elements list
			// This is equivilent to being null
			if ((object)left == null && !right.Elements.Any())
			{
				return true;
			}

			if ((object)right == null && !left.Elements.Any())
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two
			return left.Equals(right);
		}

		/// <summary>
		/// Compares two <see cref="Value"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(Value left, Value right)
		{
			return !(left == right);
		}
	}
}