﻿using System;
using MI.Framework.Units;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// A quantity with no key, with a <see cref="QuantityDataType"/> of <see cref="QuantityDataType.Decimal"/>.
	/// </summary>
	/// <remarks>
	/// A simple quantity is the only type of <see cref="Quantity"/> permitted to be represented as a <see cref="SingleValue"/> 
	/// or <see cref="ValueElement"/>.
	/// </remarks>
	public class DecimalQuantity : SingleQuantity
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="DecimalQuantity"/> class.
		/// </summary>
		protected DecimalQuantity()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DecimalQuantity"/> class.
		/// </summary>
		/// <param name="unitDecimal">The <see cref="UnitDecimal"/> to underpin the new quantity.</param>
		/// <param name="uncertainty">The measurement uncertainty of the new quantity.</param>
		internal DecimalQuantity(UnitDecimal unitDecimal, Uncertainty uncertainty = null) 
			: base(unitDecimal, dataType: QuantityDataType.Decimal, uncertainty: uncertainty)
		{
		}

		/// <summary>
		/// Constructs a new decimal quantity from a quantity element.
		/// </summary>
		/// <param name="element">The quantity element from which to construct the new quantity.</param>
		/// <returns>A new decimal quantity constructed from the given quantity element.</returns>
		new public static DecimalQuantity FromElement(QuantityElement element)
		{
			element = ArgumentValidation.AssertNotNull(element, nameof(element));
			if (!element.HasKey && element.DataType == QuantityDataType.Decimal)
			{
				return new DecimalQuantity(element.UnitDecimal, element.Uncertainty);
			}

			throw new ArgumentException(ExceptionMessage.CannotCreateDecimalQuantityFromElementWithKeyOrNonDecimalDataType);
		}

		/// <summary>
		/// Returns a new decimal quantity with the given <see cref="Uncertainty"/>.
		/// </summary>
		/// <param name="uncertainty">The uncertainty to apply to the new decimal quantity.</param>
		/// <returns>A new decimal quantity with the given <see cref="Uncertainty"/>.</returns>
		new public DecimalQuantity WithUncertainty(Uncertainty uncertainty)
		{
			return new DecimalQuantity(UnitDecimal, uncertainty);
		}

		/// <summary>
		/// Implicitly casts a <see cref="UnitDecimal"/> to a <see cref="DecimalQuantity"/>.
		/// </summary>
		/// <param name="unitDecimal">The unit decimal to cast.</param>
		public static implicit operator DecimalQuantity(UnitDecimal unitDecimal)
		{
			return new DecimalQuantity(unitDecimal);
		}

		/// <summary>
		/// Implicitly casts a <see cref="Decimal"/> number to a <see cref="DecimalQuantity"/>.
		/// </summary>
		/// <param name="number">The decimal number to cast.</param>
		public static implicit operator DecimalQuantity(decimal number)
		{
			return new DecimalQuantity(number);
		}

		public static DecimalQuantity operator +(DecimalQuantity quantity)
		{
			return QuantityMath<DecimalQuantity>.UnaryPlus(quantity);
		}

		public static DecimalQuantity operator -(DecimalQuantity quantity)
		{
			return QuantityMath<DecimalQuantity>.UnaryMinus(quantity);
		}

		public static DecimalQuantity operator +(DecimalQuantity left, DecimalQuantity right)
		{
			return QuantityMath<DecimalQuantity>.Add(left, right);
		}

		public static DecimalQuantity operator -(DecimalQuantity left, DecimalQuantity right)
		{
			return QuantityMath<DecimalQuantity>.Subtract(left, right);
		}

		public static DecimalQuantity operator *(DecimalQuantity left, DecimalQuantity right)
		{
			return QuantityMath<DecimalQuantity>.Multiply(left, right);
		}

		public static DecimalQuantity operator *(DecimalQuantity quantity, decimal constant)
		{
			return QuantityMath<DecimalQuantity>.Multiply(quantity, constant);
		}

		public static DecimalQuantity operator *(decimal constant, DecimalQuantity quantity)
		{
			return QuantityMath<DecimalQuantity>.Multiply(constant, quantity);
		}

		public static SingleQuantity operator /(DecimalQuantity left, DecimalQuantity right)
		{
			return QuantityMath<DecimalQuantity>.Divide(left, right);
		}

		public static DecimalQuantity operator /(DecimalQuantity quantity, decimal constant)
		{
			return QuantityMath<DecimalQuantity>.Divide(quantity, constant);
		}

		public static DecimalQuantity operator /(decimal constant, DecimalQuantity quantity)
		{
			return QuantityMath<DecimalQuantity>.Divide(constant, quantity);
		}

		/// <summary>
		/// Gets a hash code for the decimal quantity.
		/// </summary>
		/// <returns>A hash code for the decimal quantity.</returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		/// <summary>
		/// Compares with another <see cref="DecimalQuantity"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(DecimalQuantity quantity)
		{
			return base.Equals(quantity);
		}

		/// <summary>
		/// Compares two <see cref="DecimalQuantity"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(DecimalQuantity left, DecimalQuantity right)
		{
			return (Quantity)left == (Quantity)right;
		}

		/// <summary>
		/// Compares two <see cref="DecimalQuantity"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(DecimalQuantity left, DecimalQuantity right)
		{
			return !(left == right);
		}
	}
}
