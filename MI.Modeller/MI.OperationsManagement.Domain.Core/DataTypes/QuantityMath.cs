﻿using System;
using System.Linq;
using MI.Framework.Validation;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	/// <summary>
	/// Helper class for performing mathematics on quantities.
	/// </summary>
	/// <remarks>
	/// This class is needed to achieve reuse of the quantity math logic between different classes in the <see cref="Quantity"/> class hierarchy.
	/// </remarks>
	public static class QuantityMath<TQuantity> where TQuantity : Quantity
	{
		public static TQuantity UnaryPlus(TQuantity quantity)
		{
			return (TQuantity)quantity?.Copy();
		}

		public static TQuantity UnaryMinus(TQuantity quantity)
		{
			quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
			return (TQuantity)Quantity.FromElements(quantity.Select(element => 
				new QuantityElement(-element.UnitDecimal, element.Key, uncertainty: element.Uncertainty)));
		}

		public static TQuantity Add(TQuantity left, TQuantity right)
		{
			ValidateBinaryOperationQuantityOperands(left, right);
			return (TQuantity)Quantity.FromElements(left.Select(leftElement =>
				new QuantityElement(leftElement.UnitDecimal + right[leftElement.Key].UnitDecimal, leftElement.Key)));
		}

		public static TQuantity Subtract(TQuantity left, TQuantity right)
		{
			ValidateBinaryOperationQuantityOperands(left, right);
			return (TQuantity)Quantity.FromElements(left.Select(leftElement =>
				new QuantityElement(leftElement.UnitDecimal - right[leftElement.Key].UnitDecimal, leftElement.Key)));
		}

		public static TQuantity Multiply(TQuantity left, TQuantity right)
		{
			ValidateBinaryOperationQuantityOperands(left, right);
			return (TQuantity)Quantity.FromElements(left.Select(leftElement =>
				new QuantityElement(leftElement.UnitDecimal * right[leftElement.Key].UnitDecimal, leftElement.Key)));
		}

		public static TQuantity Multiply(TQuantity quantity, decimal constant)
		{
			quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
			return (TQuantity)Quantity.FromElements(quantity.Select(element =>
				new QuantityElement(-element.UnitDecimal * constant, element.Key, uncertainty: element.Uncertainty)));
		}

		public static TQuantity Multiply(decimal constant, TQuantity quantity)
		{
			return Multiply(quantity, constant);
		}

		public static TQuantity Divide(TQuantity left, TQuantity right)
		{
			ValidateBinaryOperationQuantityOperands(left, right);
			return (TQuantity)Quantity.FromElements(left.Select(leftElement =>
				new QuantityElement(leftElement.UnitDecimal / right[leftElement.Key].UnitDecimal, leftElement.Key)));
		}

		public static TQuantity Divide(TQuantity quantity, decimal constant)
		{
			quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
			quantity = ArgumentValidation.AssertNotNull(quantity, nameof(quantity));
			return (TQuantity)Quantity.FromElements(quantity.Select(element =>
				new QuantityElement(-element.UnitDecimal / constant, element.Key, uncertainty: element.Uncertainty)));
		}

		public static TQuantity Divide(decimal constant, TQuantity quantity)
		{
			return Divide(quantity, constant);
		}

		public static void ValidateBinaryOperationQuantityOperands(TQuantity left, TQuantity right)
		{
			left = ArgumentValidation.AssertNotNull(left, nameof(left));
			right = ArgumentValidation.AssertNotNull(right, nameof(right));

			if (left.Elements.Count() != right.Elements.Count() || left.Any(e => !right.ContainsKey(e.Key)))
			{
				throw new ArgumentException(ExceptionMessage.CannotPerformMathOperationOnQuantitiesWithDifferentElements);
			}
		}
	}
}
