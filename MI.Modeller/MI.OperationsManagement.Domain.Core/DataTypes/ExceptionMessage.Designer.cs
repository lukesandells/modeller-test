﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MI.OperationsManagement.Domain.Core.DataTypes {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ExceptionMessage {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ExceptionMessage() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MI.OperationsManagement.Domain.Core.DataTypes.ExceptionMessage", typeof(ExceptionMessage).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot create a decimal quantity from an element that has a key or a non-decimal data type..
        /// </summary>
        internal static string CannotCreateDecimalQuantityFromElementWithKeyOrNonDecimalDataType {
            get {
                return ResourceManager.GetString("CannotCreateDecimalQuantityFromElementWithKeyOrNonDecimalDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot add, subtract, multiply or divide two quantities that do not define the same number of elements with the same keys..
        /// </summary>
        internal static string CannotPerformMathOperationOnQuantitiesWithDifferentElements {
            get {
                return ResourceManager.GetString("CannotPerformMathOperationOnQuantitiesWithDifferentElements", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot represent a multi-element quantity as a value..
        /// </summary>
        internal static string CannotRepresentMultiElementQuantityAsValue {
            get {
                return ResourceManager.GetString("CannotRepresentMultiElementQuantityAsValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot represent a quantity with a data type other than decimal as a value..
        /// </summary>
        internal static string CannotRepresentNonDecimalQuantityAsValue {
            get {
                return ResourceManager.GetString("CannotRepresentNonDecimalQuantityAsValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cannot represent a quantity with a defined key as a value..
        /// </summary>
        internal static string CannotRepresentQuantityWithKeyAsValue {
            get {
                return ResourceManager.GetString("CannotRepresentQuantityWithKeyAsValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confidence level must be between zero and one, inclusive..
        /// </summary>
        internal static string ConfidenceLevelMustBeBetweenZeroAndOne {
            get {
                return ResourceManager.GetString("ConfidenceLevelMustBeBetweenZeroAndOne", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Formatting of value {0} inconsistent with data type {1}..
        /// </summary>
        internal static string FormattingOfValueInconsistentWithDataType {
            get {
                return ResourceManager.GetString("FormattingOfValueInconsistentWithDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Multiple data types were found for the native type of {0}.  Ensure where multiple data types are configured for a native type that only one data type has a DataTypeAttribute with Default = true..
        /// </summary>
        internal static string MultipleDataTypesFoundForNativeType {
            get {
                return ResourceManager.GetString("MultipleDataTypesFoundForNativeType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Either the standardUncertainty or expandedUncertainty parameters must be provided..
        /// </summary>
        internal static string MustProvideEitherStandardOrExpandedUncertainty {
            get {
                return ResourceManager.GetString("MustProvideEitherStandardOrExpandedUncertainty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The given object is of a type that is not supported..
        /// </summary>
        internal static string ObjectTypeNotSupported {
            get {
                return ResourceManager.GetString("ObjectTypeNotSupported", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The given object&apos;s value is out of range for the value data type..
        /// </summary>
        internal static string ObjectValueOutOfRange {
            get {
                return ResourceManager.GetString("ObjectValueOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Precision must be between zero and one, inclusive..
        /// </summary>
        internal static string PrecisionMustBeBetweenZeroAndOne {
            get {
                return ResourceManager.GetString("PrecisionMustBeBetweenZeroAndOne", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The given unit decimal&apos;s value is out of range for the quantity data type..
        /// </summary>
        internal static string UnitDecimalValueOutOfRange {
            get {
                return ResourceManager.GetString("UnitDecimalValueOutOfRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity data type {0} not recognised..
        /// </summary>
        internal static string UnrecognisedQuantityDataType {
            get {
                return ResourceManager.GetString("UnrecognisedQuantityDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value data type {0} not recognised..
        /// </summary>
        internal static string UnrecognisedValueDataType {
            get {
                return ResourceManager.GetString("UnrecognisedValueDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity data type {0} not supported..
        /// </summary>
        internal static string UnsupportedQuantityDataType {
            get {
                return ResourceManager.GetString("UnsupportedQuantityDataType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value data type {0} not supported..
        /// </summary>
        internal static string UnsupportedValueDataType {
            get {
                return ResourceManager.GetString("UnsupportedValueDataType", resourceCulture);
            }
        }
    }
}
