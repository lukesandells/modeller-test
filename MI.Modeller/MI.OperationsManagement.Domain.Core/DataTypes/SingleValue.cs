﻿using System.Linq;
using MI.Framework;

namespace MI.OperationsManagement.Domain.Core.DataTypes
{
	public class SingleValue : Value
	{
		internal ValueElement Element => Elements.Single();

		public string Key => Element.Key;

		public ExtensibleEnum<ValueDataType> DataType => Element.DataType;

		internal SingleValue(object value) : this(value, string.Empty)
		{
		}

		internal SingleValue(object value, string key) : base(value, key)
		{
		}

		public object ToObject()
		{
			return Element.ToObject();
		}

		public SingleValue WithKey(string key)
		{
			return new SingleValue(ToObject(), key);
		}

		/// <summary>
		/// Gets a hash code for the single value.
		/// </summary>
		/// <returns>A hash code for the single value.</returns>
		public override int GetHashCode()
		{
			return Element.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			return Element.Equals(obj);
		}

		/// <summary>
		/// Compares with another <see cref="SingleValue"/> for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public bool Equals(SingleValue value)
		{
			if (value == null)
			{
				return false;
			}

			return Element.Equals(value.Element);
		}

		/// <summary>
		/// Compares two <see cref="SingleValue"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(SingleValue left, SingleValue right)
		{
			return (Value)left == (Value)right;
		}

		/// <summary>
		/// Compares two <see cref="SingleValue"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(SingleValue left, SingleValue right)
		{
			return !(left == right);
		}
	}
}
