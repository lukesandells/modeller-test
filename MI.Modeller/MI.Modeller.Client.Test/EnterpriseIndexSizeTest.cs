﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MI.Modeller.Client.B2mmlMappers;
using MI.Modeller.Client.Repositories;
using MI.Modeller.Domain.Client;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace MI.Modeller.Client.Test
{
    [TestFixture]
    public class EnterpriseIndexSizeTest
    {
        public EnterpriseIndexEntry GenerateEntry(EnterpriseIndexEntry parent, string index, int depth)
        {
            var hierarchyScope = new HierarchyScope($"{parent.Scope.EquipmentId}.{index}", new EquipmentLevelType(EquipmentLevelTypeValue.Other), parent.Scope.EnterpriseHierarchyScope);
            var entry = new EnterpriseIndexEntry(hierarchyScope, parent);
            if (depth <= 5)
            {
                for (int i = 0; i < 5; i++)
                {
                    GenerateEntry(entry,
                        $"{i}",
                        depth + 1);
                }
            }
            for (int i = 0; i < 10; i++)
            {
                entry.ContainedIds.Add(new Tuple<string, string>(typeof(EquipmentClass).ToString(), $"EquipmentClass{i}"));
            }
            return entry;
        }

        [Test]
        public void TestTiming()
        {
            Process currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            Console.WriteLine($"Memory usage: {currentProcess.WorkingSet64 / 1048576}");
            var enterpriseScope = new HierarchyScope("ent", new EquipmentLevelType(EquipmentLevelTypeValue.Enterprise));
            var enterpriseEntry = new EnterpriseIndexEntry(enterpriseScope);
            for (int i = 0; i < 5; i++)
            {
                GenerateEntry(enterpriseEntry,
                    $"{i}",
                    1);
            }
            currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            Console.WriteLine($"Memory usage: {currentProcess.WorkingSet64/ 1048576}");
            SerializationHelper serialiser = new SerializationHelper();
            Stopwatch sw = new Stopwatch();
            sw.Start();
            EnterpriseIndexEntryToHierarchyScopeFileTypeMapper toMapper = new EnterpriseIndexEntryToHierarchyScopeFileTypeMapper();
            var typeToWrite = toMapper.Map(enterpriseEntry);
            sw.Stop();
            Console.WriteLine($"Time to serialise: {sw.Elapsed}");
            currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            Console.WriteLine($"Memory usage: {currentProcess.WorkingSet64/ 1048576}");
            sw.Reset();
            sw.Start();
            serialiser.WriteFile("D:\\Test\\TestEnterprise.xml", typeToWrite);
            sw.Stop();
            Console.WriteLine($"Time to write: {sw.Elapsed}");
            currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            Console.WriteLine($"Memory usage: {currentProcess.WorkingSet64/ 1048576}");
            sw.Reset();
            sw.Start();
            serialiser.ReadFile<HierarchyNodeFileType>("D:\\Test\\TestEnterprise.xml");
            sw.Stop();
            Console.WriteLine($"Time to open, read and deserialise: {sw.Elapsed}");
            currentProcess = System.Diagnostics.Process.GetCurrentProcess();
            Console.WriteLine($"Memory usage: {currentProcess.WorkingSet64/ 1048576}");
            Console.WriteLine("Done");
        }

    }
}
