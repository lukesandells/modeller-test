﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Client.ModellerEventArgs;
using Moq;
using NUnit.Framework;

namespace MI.Modeller.Client.Test.ModellerEventArgs
{
    [TestFixture]
    public class ModellerShapeSelectionEventArgsTests
    {
        [Test]
        public void Test_ModellerShapeSelectionEventArgs_CreatesValidInstance()
        {
            // Arrange
            var shapes = new HashSet<IModellerShape>();
            for (int i = 0; i < 10; i++)
            {
                 var shapeMock = new Mock<IModellerShape>().Object;
                shapes.Add(shapeMock);
            }
            var argument = "argumentString";

            // Act
            var args = new ModellerShapeSelectionEventArgs(argument,
                shapes);

            // Assert
            CollectionAssert.AreEquivalent(shapes, args.SelectedModellerShapes, "instance created with incorrect shapes collection");
            Assert.AreEqual(argument, args.Argument, "Instance was created with incorrect properties");
        }


        [Test]
        public void Test_ModellerShapeSelectionEventArgs_NullArgs_CreatesValidInstance()
        {
            // Arrange
            

            // Act
            var args = new ModellerShapeSelectionEventArgs(null, null);

            // Assert
            Assert.IsNotNull(args.SelectedModellerShapes, "instance created with null did not create an empty set of shapes");
            Assert.AreEqual(0, args.SelectedModellerShapes.Count(), "Instance was created with incorrect properties");
            Assert.IsNull(args.Argument, "Instance was created with incorrect properties");
        }
    }
}
