﻿using MI.Modeller.Client.ModellerEventArgs;
using Moq;
using NUnit.Framework;

namespace MI.Modeller.Client.Test.ModellerEventArgs
{
    [TestFixture]
    public class ModellerDocumentEventArgsTests
    {
        [Test]
        public void Test_ModellerDocumentEventArgs_ReturnsValidInstance()
        {
            // Arrange
            var documentMock = new Mock<IModellerDocument>().Object;

            // Act
            var args = new ModellerDocumentEventArgs(documentMock);

            // Assert
            Assert.AreEqual(documentMock, args.Document, "Instance was created with incorrect properties");


        }
    }
}
