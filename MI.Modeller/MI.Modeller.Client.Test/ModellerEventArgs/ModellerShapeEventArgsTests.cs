﻿using MI.Modeller.Client.ModellerEventArgs;
using Moq;
using NUnit.Framework;

namespace MI.Modeller.Client.Test.ModellerEventArgs
{
    [TestFixture]
    public class ModellerShapeEventArgsTests
    {
        [Test]
        public void Test_ModellerShapeEventArgs_CreatesValidInstance()
        {
            // Arrange
            var shapeMock = new Mock<IModellerShape>().Object;
            var argument = "argumentString";
	        var pageMock = new Mock<IModellerPage>().Object;
	        var documentMock = new Mock<IModellerDocument>().Object;

            // Act
	        var args = new ModellerShapeEventArgs{
				Argument = argument,
				ModellerShape = shapeMock,
				ModellerPage = pageMock,
				ModellerDocument = documentMock
			};

			// Assert
			Assert.AreEqual(shapeMock, args.ModellerShape, "Instance was created with incorrect properties");
			Assert.AreEqual(argument, args.Argument, "Instance was created with incorrect properties");
	        Assert.AreEqual(pageMock, args.ModellerPage, "Instance was created with incorrect properties");
	        Assert.AreEqual(documentMock, args.ModellerDocument, "Instance was created with incorrect properties");
		}
    }
}
