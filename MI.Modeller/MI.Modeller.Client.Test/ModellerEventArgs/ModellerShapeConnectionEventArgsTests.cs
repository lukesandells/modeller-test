﻿using MI.Modeller.Client.ModellerEventArgs;
using Moq;
using NUnit.Framework;

namespace MI.Modeller.Client.Test.ModellerEventArgs
{
    [TestFixture]
    public class ModellerShapeConnectionEventArgsTests
    {
        [Test]
        public void Test_ModellerShapeConnectionEventArgs_CreatesValidInstance()
        {
            // Arrange
            var connectorShapeMock = new Mock<IModellerConnector>().Object;
            var addedShapeMock = new Mock<IModellerShape>().Object;
            var removedShapeMock = new Mock<IModellerShape>().Object;


            // Act
	        var args = new ModellerShapeConnectionEventArgs(connectorShapeMock);

            // Assert
            Assert.AreEqual(connectorShapeMock, args.Connector, "Instance was created with incorrect properties");
		}
    }
}
