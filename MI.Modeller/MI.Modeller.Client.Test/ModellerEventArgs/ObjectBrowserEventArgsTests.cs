﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.ViewInterfaces;
using Moq;
using NUnit.Framework;

namespace MI.Modeller.Client.Test.ModellerEventArgs
{
	[TestFixture]
    public class ObjectBrowserEventArgsTests
    {
        [Test]
        public void Test_ObjectBrowserNodeEventArguments_CreatesValidInstance()
        {
            // Arrange
	        var node = new Mock<BrowserNode>().Object;

            // Act
            var args = new ObjectBrowserNodeEventArguments(node);

            // Assert
            Assert.AreEqual(node,
                args.BrowserNode,
                "Instance was created with incorrect properties");
        }
    }
}