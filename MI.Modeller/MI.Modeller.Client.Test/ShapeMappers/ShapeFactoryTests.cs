﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.Persistence;
using MI.Modeller.Client.MasterShapes;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using Moq;
using NUnit.Framework;

namespace MI.Modeller.Client.Test.ShapeMappers
{
    [TestFixture]
    public class ShapeFactoryTests: BaseModellerNodeTest
    {
        [Test]
        public void Test_GetDomainObjectFromShape_ReturnsInstance()
        {
			// Arrange
			// The occurrence
	        NodeType.AddMappingAssembly(typeof(ApplicationRootNodeMapping).Assembly);
			var factoryMock = new Mock<ShapeFactory>(MockBehavior.Strict);
			var model = new Model("Model");
	        var page = model.AddNew(NodeType.For(typeof(Page)));
			var hierarchyScopeNode = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent", new object[] { EquipmentLevel.Enterprise });
			var occurrence = ((Page) page).AddElementAsObject(hierarchyScopeNode as ModelElement);
			// Setting up the session
	        SetupSession();
			SessionMock.Setup(m => m.Query<ObjectOccurrence>())
		        .Returns(new List<ObjectOccurrence>(new[] { occurrence }).AsQueryable);
			// The shape
			var shapeMock = new Mock<IModellerShape>();
	        shapeMock.Setup(s => s.OccurrenceId)
		        .Returns(occurrence.Id);
			

			// Act
			var result = factoryMock.Object.GetOccurenceForShape(shapeMock.Object);

            // Assert
            Assert.IsNotNull(result,
                "Getting domain object from shape did not return a valid instance");
            Assert.AreEqual(occurrence,
                result,
                "Getting domain object from shape did not return the expected instance");
	        SessionMock.Verify(v => v.Close(), Times.Once);
	        TransactionMock.Verify(v => v.Commit(), Times.Once);

		}

        [Test]
        public void Test_GetShapeFromDomainObject()
        {
			// Arrange
			var shapeMock = new Mock<IModellerShape>();
			shapeMock.SetupAllProperties();
			var factoryMock = new Mock<ShapeFactory>(MockBehavior.Strict);
			factoryMock.Setup(s => s.CreateNewShapeOffPage(It.IsAny<ModelElement>(),
					It.IsAny<IModellerPresenter>()))
				.Returns(shapeMock.Object);
			//      var modelElement = new Mock<Occurrence>();
			//      modelElement.SetupGet(e => e.Id)
			//       .Returns(Guid.NewGuid);
			//      modelElement.SetupGet(e => e.)
			//       .Returns("Description");

			//      var rootNode = new RootNode(NodeTypeId.Root);
			//      ModellerNodeCache.Initialise(rootNode);
			//RegisterNodeMappings();
			//var node = rootNode.AddNew(NodeType.For(typeof(TestObject)), "Node1");

	        TestNodeMapping.SecondaryFolder<Model>(Guid.NewGuid(), "Models", true, false);
	        RegisterNodeMappings(new INodeMapping[] { new ModelNodeMapping(), new PageNodeMapping() });

	        var testNode = NodeType.For(typeof(TestObject)).NewDefinitionNode("Node1");
			var pageNode = NodeType.For(typeof(Page)).NewDefinitionNode("TestPage");
	        var occurrence = ((Page) pageNode.TargetObject).AddElementAsObject((ModelElement) testNode);
	        testNode.Description= "Description";

			// Act
			var result = factoryMock.Object.MapOccurrenceToShape(occurrence,
				shapeMock.Object);

			// Assert
			Assert.IsNotNull(result,
						 "Getting shape from domain object returned null instance");
			Assert.AreEqual("Node1",
				result.IdText,
				"Mapping shape from domain object did not correctly set the id text");
			Assert.AreEqual("Description",
			 result.DescriptionText,
			 "Mapping shape from domain object did not correctly set the description text");
		}

        [Test]
        public void Test_IsOfModellerShape_EquipmentClass_ReturnsTrue()
        {
            // Arrange
            var shapeMock = new Mock<IModellerShape>();
            shapeMock.Setup(s => s.MasterName)
                .Returns(MasterShape.EquipmentClassMasterShape.Name);
            var factoryMock = new Mock<ShapeFactory>(MockBehavior.Strict);

            // Act
            var result = factoryMock.Object.IsModellerShape(shapeMock.Object);

            // Assert
            Assert.IsTrue(result,
                "Shape which is of Modeller type does not return true");
        }

        [Test]
        public void Test_IsOfModellerShape_NonMaster_ReturnsFalse()
        {
            // Arrange
            var shapeMock = new Mock<IModellerShape>();
            shapeMock.Setup(s => s.MasterName)
                .Returns("I very much doubt we will ever have a master shape named this");
            var factoryMock = new Mock<ShapeFactory>(MockBehavior.Strict);

            // Act
            var result = factoryMock.Object.IsModellerShape(shapeMock.Object);

            // Assert
            Assert.IsFalse(result,
                "Shape which is not of Modeller type does not return false");
        }
    }
}