﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.ObjectBrowsing.Validation;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.Presenters.Interfaces;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Client.Test.Presenters
{
	[TestFixture]
	public class ObjectBrowserPresenterTests: BaseModellerNodeTest
	{
		[Test]
		public void Test_UpdateDisplayId_Valid_Passes()
		{
			// Arrange
			var rootNode = new RootNode(NodeTypeId.Root);
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object);
			var mockValidator = new Mock<IInputValidator>();
			string message = null;
			object validated = "New Id";
			mockValidator.Setup(v => v.ValidateInput(It.IsAny<NodeProperty>(), "New Id", out validated, out message))
				.Returns(false);
			TestNodeMapping.DisplayId(NodeTypeMember.Property.DisplayId, o => o.DisplayId, "Display ID").ValidateWith(mockValidator.Object);
			var rootNodeMapping = new ApplicationRootNodeMapping();
			rootNodeMapping.SecondaryFolder<TestObject>(Guid.NewGuid(), "Test Objects");
			RegisterNodeMappings(new INodeMapping[] { rootNodeMapping });
			var node = rootNode.AddNew(NodeType.For(typeof(TestObject)), "Node1");

			SetupSession();
			SessionMock.Setup(s => s.FindById<DefinitionNode>(node.Id)).Returns(node);

			// Act
			var result = presenter.UpdateDisplayId(node.Id, "New Id");

			// Assert
			mockValidator.Verify(v => v.ValidateInput(It.IsAny<NodeProperty>(), "New Id", out validated, out message), Times.Once, "Validator was not called");
			Assert.IsNull(result, "Validation result was not null as expected");
		}

		[Test]
		public void Test_UpdateDisplayId_Invalid_ReturnsMessage()
		{
			// Arrange
			var rootNode = new RootNode(NodeTypeId.Root);
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object);
			var mockValidator = new Mock<IInputValidator>();
			string message = "Test Message";
			object validated = "New Id";
			mockValidator.Setup(v => v.ValidateInput(It.IsAny<NodeProperty>(), "New Id", out validated, out message))
				.Returns(false);
			TestNodeMapping.DisplayId(NodeTypeMember.Property.DisplayId, o => o.DisplayId, "Display ID").ValidateWith(mockValidator.Object);
			var rootNodeMapping = new ApplicationRootNodeMapping();
			rootNodeMapping.SecondaryFolder<TestObject>(Guid.NewGuid(), "Test Objects");
			RegisterNodeMappings(new INodeMapping[] { rootNodeMapping });
			var node = rootNode.AddNew(NodeType.For(typeof(TestObject)), "Node1");

			SetupSession();
			SessionMock.Setup(s => s.FindById<DefinitionNode>(node.Id)).Returns(node);

			// Act
			var result = presenter.UpdateDisplayId(node.Id, "New Id");

			// Assert
			mockValidator.Verify(v => v.ValidateInput(It.IsAny<NodeProperty>(), "New Id", out validated, out message), Times.Once, "Validator was not called");
			Assert.AreEqual(message, result, "Expected message was not returned");
		}

		[Test]
		public void Test_NavigateBack_NoHistory()
		{
			// Arrange
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object);

			// Act
			presenter.NavigateBack();

			// Assert
			viewMock.Verify(v => v.SelectNodes(null, null));
		}

		[Test]
		public void Test_NavigateForward_NoHistory()
		{
			// Arrange
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object);

			// Act
			presenter.NavigateBack();

			// Assert
			viewMock.Verify(v => v.SelectNodes(null, null));
		}

		[Test]
		public void Test_NavigateBack_WithHistory()
		{
			// Arrange
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object);
			RegisterNodeMappings();

			var browserNode1 = NodeType.For(typeof(TestObject)).NewDefinitionNode("Node1");
			
			var args = new NodeSelectionChangedEventArguments(
				new HashSet<TargetFolderId> { new TargetFolderId(browserNode1.Id, browserNode1.PrimaryFolder.MemberId) }, 
				new HashSet<Guid> { browserNode1.Id });
			presenter.HandleSelectedNodeChanged(null,
				args);
			var browserNode2 = NodeType.For(typeof(TestObject)).NewDefinitionNode("Node2");
			args = new NodeSelectionChangedEventArguments(
				new HashSet<TargetFolderId> { new TargetFolderId(browserNode2.Id, browserNode2.PrimaryFolder.MemberId) },
				new HashSet<Guid> { browserNode2.Id });
			presenter.HandleSelectedNodeChanged(null,
				args);

			// Act
			presenter.NavigateBack();

			// Assert
			viewMock.Verify(v => v.SelectNodes(
				new HashSet<TargetFolderId> { new TargetFolderId(browserNode1.Id, browserNode1.PrimaryFolder.MemberId) },
				new [] { browserNode1.Id }));
		}

		[Test]
		public void Test_NavigateForward_WithHistory()
		{
			// Arrange
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object);
			RegisterNodeMappings();
			var browserNode1 = NodeType.For(typeof(TestObject)).NewDefinitionNode("Node1");

			var args = new NodeSelectionChangedEventArguments(
				new HashSet<TargetFolderId> { new TargetFolderId(browserNode1.Id, browserNode1.PrimaryFolder.MemberId) },
				new HashSet<Guid> { browserNode1.Id });
			presenter.HandleSelectedNodeChanged(null,
				args);
			var browserNode2 = NodeType.For(typeof(TestObject)).NewDefinitionNode("Node1");

			args = new NodeSelectionChangedEventArguments(
				new HashSet<TargetFolderId> { new TargetFolderId(browserNode2.Id, browserNode2.PrimaryFolder.MemberId) },
				new HashSet<Guid> { browserNode2.Id });
			presenter.HandleSelectedNodeChanged(null,
				args);
			presenter.NavigateBack();

			// Act
			presenter.NavigateForward();

			// Assert
			viewMock.Verify(v => v.SelectNodes(
				new HashSet<TargetFolderId> { new TargetFolderId(browserNode2.Id, browserNode2.PrimaryFolder.MemberId) },
				new[] { browserNode2.Id })); ;
		}

		[Test]
		public void Test_ExecuteOperations_Delete_Deletes()
		{
			// Arrange
			SetupSession();
			var viewMock = new Mock<IModellerTreeView>();
			var modellerPresenterMock = new Mock<IModellerPresenter>();
			var presenter = new ObjectBrowserPresenter(viewMock.Object, modellerPresenterMock.Object)
			{
				Prefetch = false
			};
			var rootNodeMapping = new ApplicationRootNodeMapping();
			rootNodeMapping.SecondaryFolder<TestObject>(Guid.NewGuid(), "Test Objects");
			RegisterNodeMappings(new INodeMapping[] { rootNodeMapping });
			var rootNode = new RootNode(NodeTypeId.Root);
			var browserNode1 = rootNode.AddNew(NodeType.For(typeof(TestObject)), "Node1");
			var browserNode2 = browserNode1.PrimaryFolder.AddNew(NodeType.For(typeof(TestObject)));
			base.SessionMock.Setup(s => s.Query<BrowserNode>()).Returns(new [] {browserNode1, browserNode2}.AsQueryable);
			viewMock.Setup(v => v.SelectedFolderIds).Returns(new[] { new TargetFolderId(browserNode1.Id, browserNode1.PrimaryFolder.MemberId),  });
			viewMock.Setup(v => v.SelectedNodeIds).Returns(new[] {browserNode1.Id});

			// Act
			presenter.ExecuteOperation(OperationType.Delete);

			// Assert
			CollectionAssert.AreEquivalent(new [] { browserNode1 , browserNode2 } , rootNode.RecycleBin.Nodes);
		}
		
	}
}
