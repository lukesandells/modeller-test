﻿using MI.Framework.ObjectBrowsing;
using MI.Modeller.Client.Forms;
using MI.Modeller.Client.MasterShapes;
using MI.Modeller.Client.ModellerEventArgs;
using MI.Modeller.Client.Presenters;
using MI.Modeller.Client.ShapeMappers;
using MI.Modeller.Client.UserControls;
using MI.Modeller.Client.ViewInterfaces;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Modeller.NodeMappings;

// ReSharper disable UnusedVariable

namespace MI.Modeller.Client.Test.Presenters
{
	[TestFixture]
	public class ModellerPresenterTests: BaseModellerNodeTest
	{
		[Test]
		public void GetModelForDocument_NotModellerDocument_ReturnsNull()
		{
			// Arrange
			var modellerMock = new Mock<IModeller>();
			var shapeFactoryMock = new Mock<IShapeFactory>();

			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object);

			var hierarcnyScope = new HierarchyScope("ent",
				EquipmentLevel.Enterprise);

			var documentMock = new Mock<IModellerDocument>();

			//Act
			var model = modellerPresenter.GetModelForDocument(documentMock.Object);

			//Assert
			Assert.IsNull(model);
		}

		[Test]
		public void Test_HandleDocumentCreated_CreatesForm()
		{
			// Arrange
			TestNodeMapping.SecondaryFolder<Model>(Guid.NewGuid(), "Models", true, false);
			RegisterNodeMappings(new INodeMapping[] { new ModelNodeMapping(), new PageNodeMapping(), new ApplicationRootNodeMapping(),  });

			var shapeFactoryMock = new Mock<IShapeFactory>();
			var modellerMock = new Mock<IModeller>();
			var shapeMapperFactoryMock = new Mock<IShapeFactory>();
			var modellerEventArgs = new ModellerShapeEventArgs();
			var treeviewFormMock = new Mock<ITreeViewForm>();
			treeviewFormMock.Setup(e => e.ShowDialog())
				.Returns(ModellerDialogResult.OK);
			var node = NodeType.For(typeof(TestObject)).NewDefinitionNode("Node1");
			treeviewFormMock.SetupGet(f => f.SelectedNodeId).Returns(node.Id);
			SetupSession();
			SessionMock.Setup(s => s.Query<RootNode>()).Returns(new [] {new RootNode(NodeTypeId.Root)}.AsQueryable);
			SessionMock.Setup(s => s.FindById<DefinitionNode>(node.Id)).Returns(node);
			var hierarchyScope = new HierarchyScope("InternalId",
				EquipmentLevel.Enterprise);

			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object,
				treeviewFormMock.Object);

			var documentMock = new Mock<IModellerDocument>();

			documentMock.SetupGet(e => e.Title)
				.Returns("Title");
			documentMock.Setup(s => s.IsModellerDocument)
				.Returns(true);

			var args = new ModellerDocumentEventArgs(documentMock.Object);

			// Act
			modellerMock.Raise(e => e.DocumentCreated += null,
				args);

			// Assert
			treeviewFormMock.Verify(e => e.ShowDialog(),
				Times.Once);
			documentMock.VerifySet(e => e.ModelInternalId = It.IsAny<Guid>(),
				Times.Once);
		}

		[Test]
		[Ignore("Not yet implemented")]
		public void Test_HandleDocumentCreated_makeSureThatTHeModelIsCreated_CreatesForm()
		{
			throw new NotImplementedException();
			// Arrange
		}

		[Test]
		public void Test_HandleShutDown_ClearsObjects()
		{
			// Arrange
			var shapeFactoryMock = new Mock<IShapeFactory>();
			var modellerMock = new Mock<IModeller>();
			var modellerEventArgs = new ModellerShapeEventArgs();

			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object);

			// Act / Assert
			Assert.DoesNotThrow(() => modellerMock.Raise(e => e.ShutDown += null,
				modellerEventArgs));
		}

		[Test]
		[Ignore("Not yet implemented")]
		public void Test_Model_CreatedForAllCases_CreatesForm()
		{
			throw new NotImplementedException();
		}

		[Test]
		[Ignore("Not yet implemented")]
		public void Test_OrphanShapesCheckedProperlyAndNotWhenLoading_CreatesForm()
		{
			throw new NotImplementedException();
		}

		[Test]
		public void Test_ShapeAdded_DragDropInProgress_IsIgnored()
		{
			// Arrange
			var shapeFactoryMock = new Mock<IShapeFactory>();
			var modellerMock = new Mock<IModeller>();
			var shapeMapperFactoryMock = new Mock<IShapeFactory>();

			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object);
			var modellerEventArgs = new ModellerShapeEventArgs
			{
				ModellerShape = new Mock<IModellerShape>().Object
			};
			modellerPresenter.DragInOperation = true;

			// Act
			modellerMock.Raise(e => e.ShapeAdded += null,
				modellerEventArgs);

			// Assert
			shapeMapperFactoryMock.Verify(e => e.GetDomainTypeFromShape(It.IsAny<IModellerShape>()),
				Times.Never);
		}

		[Test]
		[Ignore("Being reimplemented")]
		public void Test_ShapeAdded_ExistingWorkspaceShape_AddsOccurrenceOnly()
		{
			// Arrange
			TestNodeMapping.SecondaryFolder<Model>(Guid.NewGuid(), "Models", true, false);
			RegisterNodeMappings(new INodeMapping[] { new ModelNodeMapping(), new PageNodeMapping() });

			var shapeFactoryMock = new Mock<IShapeFactory>();
			var modellerMock = new Mock<IModeller>();
			var shapeMock = new Mock<IModellerShape>();
			shapeMock.SetupProperty(e => e.OccurrenceId, Guid.NewGuid());

			var modellerEventArgs = new ModellerShapeEventArgs
			{
				ModellerShape = shapeMock.Object
			};
			modellerEventArgs.ModellerShape = shapeMock.Object;


			var treeFormMock = new Mock<ITreeViewForm>();
			treeFormMock.Setup(e => e.ShowDialog())
				.Returns(ModellerDialogResult.OK);
			treeFormMock.SetupGet(e => e.SelectedNodeId)
				.Returns(NodeType.For(typeof(TestObject)).NewDefinitionNode("Node1").Id);

			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object,
				treeFormMock.Object);

			// Document
			var documentMock = new Mock<IModellerDocument>();
			// ReSharper disable once CollectionNeverUpdated.Local
			var shapes = new HashSet<IModellerShape>();

			documentMock.SetupAllProperties();
			var documentId = Guid.NewGuid();

			documentMock.SetupGet(e => e.Title)
				.Returns("DocumentTitle");
			documentMock.SetupGet(e => e.Shapes)
				.Returns(shapes);
			documentMock.SetupGet(e => e.ModelInternalId)
				.Returns(documentId);
			documentMock.SetupGet(s => s.FilePath)
				.Returns("C:\\Testpath");
			documentMock.SetupGet(s => s.ModelType)
				.Returns(ModelType.WorkDefinitionModel);
			documentMock.Setup(s => s.IsModellerDocument)
				.Returns(true);
			modellerMock.Setup(m => m.ActiveDocument).Returns(documentMock.Object);
			// create the document - this will attempt to popup
			var createEventArgs = new ModellerDocumentEventArgs(documentMock.Object);
			modellerMock.Raise(e => e.DocumentCreated += null,
				createEventArgs);

			shapeFactoryMock.Setup(e => e.IsModellerShape(shapeMock.Object))
				.Returns(true);

			// Act
			modellerMock.Raise(e => e.ShapeAdded += null,
				modellerEventArgs);

			// Assert
			shapeMock.VerifySet(shape => shape.OccurrenceId = It.IsAny<Guid?>());
		}

		[Test]
		[Ignore("Being reimplemented")]
		public void Test_ShapeAdded_NoHierarchyScope_ThrowsException()
		{
			// Arrange
			var shapeFactoryMock = new Mock<IShapeFactory>();
			var modellerMock = new Mock<IModeller>();
			var modellerEventArgs = new ModellerShapeEventArgs();
			var shapeMock = new Mock<IModellerShape>();
			modellerEventArgs.ModellerShape = shapeMock.Object;

			var hierarchyScope = new HierarchyScope("Test",
				EquipmentLevel.Enterprise);

			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object);

			shapeFactoryMock.Setup(e => e.IsModellerShape(shapeMock.Object))
				.Returns(true);

			// Act / Assert
			Assert.Throws<Exception>(() => modellerMock.Raise(e => e.ShapeAdded += null,
					modellerEventArgs),
				"New Shape Added and unable to determine its Hierarchy Scope did not result in an error as expected");
		}

		[Test]
		public void Test_ShapeAdded_NonModellerShape_IsIgnored()
		{
			// Arrange
			var shapeFactoryMock = new Mock<IShapeFactory>();
			var modellerMock = new Mock<IModeller>();
			var modellerEventArgs = new ModellerShapeEventArgs();

			// ReSharper disable once UnusedVariable
			var modellerPresenter = new ModellerPresenter(modellerMock.Object,
				shapeFactoryMock.Object);

			modellerEventArgs.ModellerShape = new Mock<IModellerShape>().Object;

			shapeFactoryMock.Setup(e => e.IsModellerShape(It.IsAny<IModellerShape>()))
				.Returns(false);

			// Act
			modellerMock.Raise(e => e.ShapeAdded += null,
				modellerEventArgs);

			// Assert
			shapeFactoryMock.Verify(e => e.IsModellerShape(It.IsAny<IModellerShape>()),
				Times.Once);
			shapeFactoryMock.Verify(e => e.GetDomainTypeFromShape(It.IsAny<IModellerShape>()),
				Times.Never);
		}
	}
}