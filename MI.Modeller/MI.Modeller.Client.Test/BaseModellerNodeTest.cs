﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Framework.Persistence;
using MI.Modeller.Domain;
using MI.OperationsManagement.Domain.MasterData.Design;
using Moq;
using NHibernate.Linq;
using NUnit.Framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Linq.Expressions;

namespace MI.Modeller.Client.Test
{


	/// <summary>
	/// Base modeller test to build further tests on
	/// </summary>
	public  class BaseModellerNodeTest
	{
		public static Guid DefinitionNodeGuid = Guid.Parse("44B6BCCC-0D89-45BB-B1EA-97182E767494");
		/// <summary>
		/// Tear down method to reset the node type mappings and modeller cache
		/// </summary>
		[TearDown]
		public void TearDown()
		{
			NodeType.ClearAllMappings();
			SessionMock = null;
			TransactionMock = null;
		}

		public void SetupSession()
		{
			SessionMock = new Mock<IStatefulPersistenceSession>();
			TransactionMock = new Mock<ITransaction>();
			SessionMock.Setup(s => s.Transaction)
				.Returns(TransactionMock.Object);
			var providerMock = new Mock<IPersistenceProvider>();
			providerMock.Setup(p => p.OpenSession(It.IsAny<bool>()))
				.Returns(SessionMock.Object);
			PersistenceContext.SetProvider(providerMock.Object);
		}

		public Mock<ITransaction> TransactionMock { get; private set; }

		public Mock<IStatefulPersistenceSession> SessionMock { get; private set; }

		/// <summary>
		/// Setups up the intitial node mapping
		/// </summary>
		[SetUp]
		public void SetUp()
		{
			TestNodeMapping = new DefinitionNodeMapping<TestObject, TestNode>();
			TestNodeMapping.Type(DefinitionNodeGuid, "DisplayName");
			TestNodeMapping.PrimaryFolder<TestObject>(Guid.NewGuid());
			TestNodeMapping.Description(NodeTypeMember.Property.Description, o => o.Description, "Description");

			TestHierarchyScopeNodeMapping = new DefinitionNodeMapping<HierarchyScope, ModelElement>();
			TestHierarchyScopeNodeMapping.Type(NodeTypeId.HierarchyScope, "DisplayName").InitialiseWith
				.Property(NodeTypeMember.Property.EquipmentLevel, target => target.EquipmentLevel, "Equipment Level"); ;
			TestHierarchyScopeNodeMapping.PrimaryFolder<HierarchyScope>(NodeTypeMember.Folder.HierarchyScopesAndEquipment);
			TestHierarchyScopeNodeMapping.SecondaryFolder<TestObject>(Guid.NewGuid(), "Test Objects", true, true);
			TestHierarchyScopeNodeMapping.DisplayId(NodeTypeMember.Property.DisplayId, o => o.ExternalId, "External Id");
		}

		/// <summary>
		/// Register the test node mapping and any additional required mappings
		/// </summary>
		/// <param name="additionalMappings"></param>
		public void RegisterNodeMappings(IEnumerable<INodeMapping> additionalMappings = null)
		{
			// Some unit tests will have specific needs for the test node's display ID so this needs to be deferred
			try
			{
				TestNodeMapping.DisplayId(NodeTypeMember.Property.DisplayId, o => o.DisplayId, "Display ID");
			}
			catch
			{
				// ignored
			}

			var mappings = additionalMappings?.Union(new INodeMapping[] { TestNodeMapping, TestHierarchyScopeNodeMapping }) ?? new INodeMapping[] { TestNodeMapping, TestHierarchyScopeNodeMapping };
			NodeType.Register(NodeMappingCompiler.Compile(mappings));
		}

		/// <summary>
		/// The test node mapping
		/// </summary>
		protected DefinitionNodeMapping<TestObject, TestNode> TestNodeMapping { get; private set; }
		protected DefinitionNodeMapping<HierarchyScope, ModelElement> TestHierarchyScopeNodeMapping { get; private set; }

		/// <summary>
		/// Class for a test definition node
		/// </summary>
		public class TestNode : ModelElement
		{
			public override Guid TypeId => DefinitionNodeGuid;
			private TestNode() { }
		}

		public class TestObject: DesignObject
		{
			private string _displayId = string.Empty;

			public string DisplayId
			{
				get => _displayId;
				set => _displayId = value;
			}

			private string _description = string.Empty;

			public string Description
			{
				get => _description;
				set => _description = value;
			}
		}

		public class TestQueryable<T> : IQueryable<T> where T : class
		{
			private IQueryable<T> _backingList = new List<T>().AsQueryable();

			private Mock<INhQueryProvider> _mockQuery = new Mock<INhQueryProvider>();

			public TestQueryable()
			{
				_mockQuery.Setup(q => q.CreateQuery<T>(It.IsAny<Expression>())).Callback(() => { new TestQueryable<T>(); });//Returns(this);
			}

			public Expression Expression => _backingList.Expression;
			public Type ElementType => _backingList.ElementType;
			public IQueryProvider Provider => _mockQuery.Object;
			public IEnumerator<T> GetEnumerator() => _backingList.GetEnumerator();

			IEnumerator IEnumerable.GetEnumerator() => _backingList.GetEnumerator();
		}

		public class TestSession : IStatefulPersistenceSession
		{
			public Mock<IStatefulPersistenceSession> BaseMock = new Mock<IStatefulPersistenceSession>();

			public void Dispose() => BaseMock.Object.Dispose();

			public ITransaction BeginTransaction(IsolationLevel isolationLevel) => BaseMock.Object.BeginTransaction(isolationLevel);

			public ITransaction Transaction => BaseMock.Object.Transaction;
			public DbConnection Connection => BaseMock.Object.Connection;
			public void Close() => BaseMock.Object.Close();

			public T FindById<T>(object id) where T : class => BaseMock.Object.FindById<T>(id);

			public T FindById<T>(object id, bool acquireLock) where T : class => BaseMock.Object.FindById<T>(id, acquireLock);

			public IList<T> ListAll<T>() where T : class => BaseMock.Object.ListAll<T>();

			public IQueryable<T> Query<T>() where T : class => BaseMock.Object.Query<T>();
			//{
			//	var mockQuery = new Mock<IQueryable<T>>();
			//	mockQuery.SetupGet(q => q.Provider).Returns(new Mock<INhQueryProvider>().Object);
			//	return mockQuery.Object;
			//}

			public IEnumerable<object> Entities => BaseMock.Object.Entities;
			public void Flush() => BaseMock.Object.Flush();

			public void Attach(object entity) => BaseMock.Object.Attach(entity);

			public void Refresh(object entity) => BaseMock.Object.Refresh(entity);

			public void Evict(object entity) => BaseMock.Object.Evict(entity);

			public void Clear() => BaseMock.Object.Clear();

			public void Initialise(object entity) => BaseMock.Object.Initialise(entity);

			public void Persist(object entity, PersistBehaviour behaviour = PersistBehaviour.QueryIfNotInferable) => BaseMock.Object.Persist(entity, behaviour);

			public void Save(object entity) => BaseMock.Object.Save(entity);

			public void Update(object entity) => BaseMock.Object.Update(entity);

			public T Merge<T>(T entity) where T : class => BaseMock.Object.Merge(entity);

			public IList<T> ListAll<T>(bool cacheable) where T : class => BaseMock.Object.ListAll<T>(cacheable);

			public void Delete(object entity) => BaseMock.Object.Delete(entity);
		}
	}

	
}
