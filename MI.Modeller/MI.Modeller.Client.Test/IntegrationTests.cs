﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.ObjectBrowsing.Validation;
using MI.Framework.Persistence;
using MI.Modeller.NodeMappings;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.Modeller.Client;
using MI.Modeller.Persistence;

namespace MI.Modeller.Domain.Client.Test
{
	[TestFixture]
	public class IntegrationTests
	{

		private const int C_NUMBER_OF_TEST_SITES = 3;
		private const int C_NUMBER_OF_TEST_AREAS = 5;
		private const int C_NUMBER_OF_TEST_WORK_CENTERS = 5;
		private const int C_NUMBER_OF_TEST_WORK_UNITS = 5;
		private const int C_NUMBER_OF_TEST_EQUIPMENT_CLASSES = 10;
		private const int C_NUMBER_OF_TEST_EQUIPMENT_PER_CLASS = 5;

		ModelElement _enterprise;

		[OneTimeSetUp]
		public void SetupHierarchy()
		{
			UnitLibrary.Current = new UneceRecommendation20Library();
			NodeType.AddMappingAssembly(typeof(ApplicationRootNodeMapping).Assembly);
			PropertyValueSerialiser.Add(quantity => quantity.Serialise().ToString(), s => SerialisedQuantity.Parse(s).Deserialise());
			PropertyValueSerialiser.Add(value => value.Serialise().ToString(), s => SerialisedValue.Parse(s).Deserialise());
			Database.Connect("test.db", true, false);
			NodeTransaction.Log = PersistentTransactionLog.Load();
		}

		[OneTimeTearDown]
		public void Cleanup()
		{
			NodeTransaction.Log.ClearFinalised();
			NodeType.ClearAllMappings();
		}

		[Test]
		[Order(1)]
		public void UnitsTest()
		{
			var distance = 12.WithUnit(Length.Kilometre);
			// double it
			Assert.AreEqual(24, (2 * distance).DecimalValue);
			Assert.AreEqual(Length.Kilometre, (2 * distance).Unit);

			var percentage = 35.WithUnit(Number.Percent);
			// double it
			Assert.AreEqual(70, (2 * percentage).DecimalValue);
			Assert.AreEqual(Length.Kilometre, (2 * distance).Unit);

			// apply it to the distance
			Assert.AreEqual(4.2, (distance * percentage).DecimalValue);
			Assert.AreEqual(Length.Kilometre, (distance * percentage).Unit);

			// distance by time
			Assert.AreEqual(12 / .25, (distance / (0.25).WithUnit(Time.Hour)).DecimalValue);
			Assert.AreEqual(Velocity.KilometrePerHour, (distance / (0.25).WithUnit(Time.Hour)).Unit);
			Assert.AreEqual(200f/15f, (distance / 15.WithUnit(Time.Minute)).DecimalValue);
			Assert.AreEqual(Velocity.MetrePerSecond, (distance / 15.WithUnit(Time.Minute)).Unit);
			Assert.AreEqual((0.25).WithUnit(Time.Hour), 15.WithUnit(Time.Minute));

			// weight
			var weight = 75.WithUnit(Mass.Kilogram);
			Assert.AreEqual(75, weight.DecimalValue);
			Assert.AreEqual(Mass.Kilogram, weight.Unit);

			// weight calculations
			UnitDecimal x = 5.WithUnit(Mass.Kilogram);
			UnitDecimal y = 10.WithUnit(Mass.Kilogram);
			Assert.AreEqual(2, (y/x).DecimalValue);
			Assert.AreEqual(Unit.BaseDimensionlessUnit, (y / x).Unit);
		}

		[Test]
		[Order(2)]
		public void BuildTestEnterpriseAndPersist()
		{
			NodeTransaction transaction;
			long totalNodes;
			var st = new Stopwatch();
			var results = new List<PerformanceResult>();
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				var rootNode = new RootNode(NodeTypeId.Root);
				rootNode.Save();
				
				st.Start();
				using (transaction = NodeTransaction.Begin())
				{
					using (new ValidatedNodeOperation())
					{
						_enterprise = BuildTestEnterprise(rootNode, "Enterprise.1");
					}

					// Need to save before completing the transaction because otherwise the transaction log entries will written first,
					// causing a foreign key violation in the database
					Console.WriteLine("Saving...");
					_enterprise.Save();
					transaction.Complete();
				}
				totalNodes = _enterprise.DescendantsOrderedByDepth.Union(new BrowserNode[] { _enterprise }).Count();
				Console.WriteLine("Creation took {0}ms per node", totalNodes / st.ElapsedMilliseconds);
				results.Add(new PerformanceResult
				{
					ActualResult = totalNodes / st.ElapsedMilliseconds,
					ExpectedResult = 15,
					ErrorMessage = "Initial creation took longer than expected.",
					ValidationType = ValidationType.LessThan
				});

				scope.Done();
			}

			Console.WriteLine("*********** TEST HIERARCHY CREATED *************");
			var site1 = _enterprise.PrimaryFolder.Nodes.Single(n => n.DisplayId == "Enterprise.1.Site.1");
			Console.WriteLine("Nodes in Enterprise 1: {0}", totalNodes);
			Console.WriteLine("Nodes in Site 1: {0}", site1.DescendantsOrderedByDepth.Union(new BrowserNode[] { site1 }).Count());

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				st.Restart();
				Console.WriteLine("Rolling back creation of test hierarchy.", transaction.LogEntries.Count());
				var rollbackTransaction = transaction.RollBackAndPersist();
				Console.WriteLine("Rollback took {0}ms per node", totalNodes / st.ElapsedMilliseconds);
				results.Add(new PerformanceResult
				{
					ActualResult = totalNodes/st.ElapsedMilliseconds,
					ExpectedResult = 15,
					ErrorMessage = "Rollback of initial creation took longer than expected.",
					ValidationType = ValidationType.LessThan
				});

				st.Restart();
				Console.WriteLine("Rolling back rollback transaction.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				Console.WriteLine("Rollback of rollback took {0}ms per node", totalNodes / st.ElapsedMilliseconds);
				results.Add(new PerformanceResult
				{
					ActualResult = totalNodes / st.ElapsedMilliseconds,
					ExpectedResult = 15,
					ErrorMessage = "Rollback took longer than expected.",
					ValidationType = ValidationType.LessThan
				});

				st.Restart();
				Console.WriteLine("Rolling back rollback transaction.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				Console.WriteLine("Rollback of rollback took {0}ms per node", totalNodes / st.ElapsedMilliseconds);
				results.Add(new PerformanceResult
				{
					ActualResult = totalNodes / st.ElapsedMilliseconds,
					ExpectedResult = 15,
					ErrorMessage = "Rollback 2 took longer than expected.",
					ValidationType = ValidationType.LessThan
				});

				st.Restart();
				Console.WriteLine("Rolling back rollback transaction.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				Console.WriteLine("Rollback of rollback took {0}ms per node", totalNodes / st.ElapsedMilliseconds);
				results.Add(new PerformanceResult
				{
					ActualResult = totalNodes / st.ElapsedMilliseconds,
					ExpectedResult = 15,
					ErrorMessage = "Rollback 3 took longer than expected.",
					ValidationType = ValidationType.LessThan
				});

				st.Restart();
				Console.WriteLine("Committing final rollback transaction and clearing the transaction log.");
				rollbackTransaction.Commit();
				Console.WriteLine("Commit took {0}ms per node", st.ElapsedMilliseconds);
				results.Add(new PerformanceResult
				{
					ActualResult = st.ElapsedMilliseconds,
					ExpectedResult = 15,
					ErrorMessage = "Commit took longer than expected.",
					ValidationType = ValidationType.LessThan
				});

				NodeTransaction.Log.ClearFinalised();

				Console.WriteLine("Saving...");
				scope.Done();
			}
			GC.Collect();
			VerifyResultSet(results);
		}

		[Test]
		[Order(3)]
		public static void DeleteEnterpriseAndPersist()
		{
			Console.WriteLine("*********** DELETE COMMENCED *************");
			var stopwatch = new Stopwatch();
			var results = new List<PerformanceResult>();
			stopwatch.Start();

			ModelElement enterprise;
			NodeTransaction transaction;
			int nodeCount;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				enterprise = scope.Query<ModelElement>().Where(element => element.DisplayId == "Enterprise.1").Single();
				nodeCount = enterprise.Descendants.Count() + 1;

				using (transaction = NodeTransaction.Begin())
				{
					using (new ValidatedNodeOperation())
					{
						enterprise.DeleteAndPersist(true);
					}

					transaction.Complete();
				}

				Console.WriteLine("Saving...");
				scope.Done();
			}

			Console.WriteLine("*********** DELETE COMPLETED *************");
			Console.WriteLine("Deleted {0} nodes in {1} seconds in memory at {2} nodes per second.", nodeCount,
				stopwatch.Elapsed.TotalSeconds, nodeCount / stopwatch.Elapsed.TotalSeconds);
			results.Add(new PerformanceResult
			{
				ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
				ExpectedResult = 200,
				ValidationType = ValidationType.GreaterThan,
				ErrorMessage = "Deleting nodes did not reach the desired nodes per second"
			});

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				stopwatch.Restart();
				Console.WriteLine("Rolling back delete.");
				var rollbackTransaction = transaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rollback of delete nodes did not reach the desired nodes per second"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of delete.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rollback 2 of delete nodes did not reach the desired nodes per second"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of rollback.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();

				Console.WriteLine("Committing final rollback transaction and clearing the transaction log.");
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rollback 3 of delete nodes did not reach the desired nodes per second"
				});
				stopwatch.Restart();
				rollbackTransaction.Commit();
				NodeTransaction.Log.ClearFinalised();

				Console.WriteLine("Saving...");
				scope.Done();
			}
			VerifyResultSet(results);
		}

		[Test]
		[Order(4)]
		public static void CopyEnterpriseAndPersist()
		{
			Console.WriteLine("*********** COPY COMMENCED *************");
			var stopwatch = new Stopwatch();
			var results = new List<PerformanceResult>();
			stopwatch.Start();

			ModelElement enterprise;
			NodeTransaction transaction;
			int nodeCount;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				enterprise = scope.Query<ModelElement>().Where(element => element.DisplayId == "Enterprise.1").Single();

				using (transaction = NodeTransaction.Begin())
				{
					using (new ValidatedNodeOperation())
					{
						enterprise.CopyAndPersistTo(enterprise.ParentNode, true);
					}

					transaction.Complete();
				}

				Console.WriteLine("Saving...");
				scope.Done();
			}

			Console.WriteLine("*********** COPY COMPLETED *************");
			nodeCount = enterprise.DescendantsOrderedByDepth.Count() + 1;
			Console.WriteLine("Copied {0} nodes in {1} seconds in memory at {2} nodes per second.", nodeCount,
				stopwatch.Elapsed.TotalSeconds, nodeCount / stopwatch.Elapsed.TotalSeconds);
			results.Add(new PerformanceResult
			{
				ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds, 
				ExpectedResult = 500,
				ValidationType = ValidationType.GreaterThan,
				ErrorMessage = "Copying nodes per second was slower than expected"
			});

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				stopwatch.Restart();
				Console.WriteLine("Rolling back copy.");
				var rollbackTransaction = transaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount/stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rockback nodes per second was slower than expected"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of copy.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rockback 2 of nodes per second was slower than expected"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of rollback.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rockback 3 of nodes per second was slower than expected"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of rollback.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 500,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Rockback 4 nodes per second was slower than expected"
				});
				stopwatch.Restart();

				Console.WriteLine("Committing final rollback transaction and clearing the transaction log.");
				rollbackTransaction.Commit();
				results.Add(new PerformanceResult
				{
					ActualResult = stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 1,
					ValidationType = ValidationType.LessThan,
					ErrorMessage = "Commit was slower than expected"
				});
				stopwatch.Restart();
				stopwatch.Reset();
				NodeTransaction.Log.ClearFinalised();

				Console.WriteLine("Saving...");
				scope.Done();
			}
			VerifyResultSet(results);
		}

		[Test]
		[Order(5)]
		public static void MoveSiteAndPersist()
		{
			Console.WriteLine("*********** MOVE COMMENCED *************");
			var stopwatch = new Stopwatch();
			var results = new List<PerformanceResult>();
			stopwatch.Start();

			ModelElement site;
			ModelElement enterprise;
			NodeTransaction transaction;
			int nodeCount;
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				enterprise = scope.Query<ModelElement>().Single(element => element.DisplayId == "Enterprise.1");
				site = (ModelElement)enterprise.PrimaryFolder.Nodes.First();
				var copy = scope.Query<ModelElement>().Single(element => element.DisplayId == "Enterprise.1 - Copy");

				using (transaction = NodeTransaction.Begin())
				{
					using (new ValidatedNodeOperation())
					{
						site.MoveAndPersistTo(copy, true);
					}

					transaction.Complete();
					Console.WriteLine("Saving...");
					scope.Done();
				}
			}

			stopwatch.Stop();
			nodeCount = site.DescendantsOrderedByDepth.Count() + 1;
			Console.WriteLine("*********** MOVE COMPLETED *************");
			Console.WriteLine("{0} nodes moved in {1} seconds at {2} nodes per second.", nodeCount,
				stopwatch.Elapsed.TotalSeconds, nodeCount / stopwatch.Elapsed.TotalSeconds);
			results.Add(new PerformanceResult
			{
				ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
				ExpectedResult = 300,
				ValidationType = ValidationType.GreaterThan,
				ErrorMessage = "Nodes per second moved is less than expected"
			});

			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				stopwatch.Restart();
				Console.WriteLine("Rolling back move.");
				var rollbackTransaction = transaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 300,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Nodes per second rockback is less than expected"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of move.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 300,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Nodes per second rockback 2 is less than expected"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of rollback.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 300,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Nodes per second rockback 3 is less than expected"
				});
				stopwatch.Restart();
				Console.WriteLine("Rolling back rollback of rollback.");
				rollbackTransaction = rollbackTransaction.RollBackAndPersist();
				results.Add(new PerformanceResult
				{
					ActualResult = nodeCount / stopwatch.Elapsed.TotalSeconds,
					ExpectedResult = 300,
					ValidationType = ValidationType.GreaterThan,
					ErrorMessage = "Nodes per second rockback 4 is less than expected"
				});

				Console.WriteLine("Committing final rollback transaction and clearing the transaction log.");
				rollbackTransaction.Commit();
				NodeTransaction.Log.ClearFinalised();

				Console.WriteLine("Saving...");
				scope.Done();
			}
			VerifyResultSet(results);
		}

		public static ModelElement BuildTestEnterprise(RootNode rootNode, string enterpriseDisplayId)
		{
			var enterpriseNode = (ModelElement)rootNode.AddNew(NodeType.For(typeof(HierarchyScope)), enterpriseDisplayId,
				new object[] { EquipmentLevel.Enterprise });

			for (var i = 0; i < C_NUMBER_OF_TEST_SITES; i++)
			{
				BuildTestSite(enterpriseNode, i);
			}

			return enterpriseNode;
		}

		public static void BuildTestSite(ModelElement enterpriseNode, int siteNumber)
		{
			var siteNode = (ModelElement)enterpriseNode.AddNew(NodeType.For(typeof(HierarchyScope)),
				enterpriseNode.DisplayId + ".Site." + (siteNumber + 1).ToString(), new object[] { EquipmentLevel.Site });

			// Create equipment classes
			for (var i = 0; i < C_NUMBER_OF_TEST_EQUIPMENT_CLASSES; i++)
			{
				var equipmentClass = siteNode.AddNew(NodeType.For(typeof(EquipmentClass)), "WorkCentre.Class." + (i + 1).ToString(),
					new object[] { EquipmentLevel.WorkCenter });
			}

			// Create areas
			for (var i = 0; i < C_NUMBER_OF_TEST_AREAS; i++)
			{
				BuildTestArea(siteNode, i);
			}
		}

		public static void AddEquipment(ModelElement node, EquipmentLevel level)
		{
			for (var i = 0; i < C_NUMBER_OF_TEST_EQUIPMENT_PER_CLASS; i++)
			{
				var equipmentNode = (ModelElement)node.AddNew(NodeType.For(typeof(Equipment)), node.DisplayId + ".Equipment." + (i + 1).ToString(),
					new object[] { level });
				var property = ((Equipment)equipmentNode.DesignObject).AddProperty("Property.1");
				property.AddSubProperty("Property.1.1", new Random().Next(500).WithUnit(Mass.Kilogram).AsQuantity());
				property.AddSubProperty("Property.1.2", "String value");
				AddToRandomEquipmentClass(equipmentNode, level);
			}
		}

		public static void AddToRandomEquipmentClass(ModelElement node, EquipmentLevel level)
		{
			// Find all visible equipment classes
			var equipmentClasses = NodePath(node).OfType<DefinitionNode>()
				.SelectMany(n => n.NonEmptyFolders.Where(f => f.MemberId == NodeTypeMember.Folder.EquipmentClasses))
				.SelectMany(f => f.Nodes.OfType<ModelElement>())
				.Where(element => ((EquipmentClass)element.DesignObject).EquipmentLevel == level)
				.ToList();

			if (equipmentClasses.Count > 0)
			{
				var randomEquipmentClass = equipmentClasses[new Random().Next(equipmentClasses.Count)];
				node.AddTo(randomEquipmentClass);
			}
		}

		public static void BuildTestArea(ModelElement siteNode, int areaNumber)
		{
			var areaNode = (ModelElement)siteNode.AddNew(NodeType.For(typeof(HierarchyScope)), siteNode.DisplayId + ".Area." + (areaNumber + 1).ToString(),
				new object[] { EquipmentLevel.Area });

			/// Add equipment
			AddEquipment(areaNode, EquipmentLevel.Area);

			// Create work centres
			for (var i = 0; i < C_NUMBER_OF_TEST_WORK_CENTERS; i++)
			{
				BuildTestWorkCentre(areaNode, i);
			}
		}

		public static void BuildTestWorkCentre(ModelElement areaNode, int workCentreNumber)
		{
			var workCentreNode = (ModelElement)areaNode.AddNew(NodeType.For(typeof(HierarchyScope)),
				areaNode.DisplayId + ".WorkCentre." + (workCentreNumber + 1).ToString(),
				new object[] { EquipmentLevel.WorkCenter });

			// Add to a random equipment class
			AddToRandomEquipmentClass(workCentreNode, EquipmentLevel.WorkCenter);

			// Add equipment
			AddEquipment(workCentreNode, EquipmentLevel.WorkCenter);

			// Create work units
			for (var i = 0; i < C_NUMBER_OF_TEST_WORK_UNITS; i++)
			{
				BuildTestWorkUnit(workCentreNode, i);
			}
		}

		public static void BuildTestWorkUnit(ModelElement workCentreNode, int workUnitNumber)
		{
			workCentreNode.AddNew(NodeType.For(typeof(HierarchyScope)),
				workCentreNode.DisplayId + ".WorkUnit." + (workUnitNumber + 1).ToString(),
				new object[] { EquipmentLevel.WorkUnit });
		}

		public static IList<BrowserNode> NodePath(ModelElement node)
		{
			var nodePath = new List<BrowserNode>();
			BrowserNode currentNode = node;
			while (currentNode != null)
			{
				nodePath.Add(currentNode);
				currentNode = currentNode.ParentNode;
			}

			return nodePath;
		}

		private static void VerifyResultSet(IEnumerable<PerformanceResult> validationResults)
		{
			foreach (var result in validationResults)
				switch (result.ValidationType)
				{
					// Validate that result is <validation type> than expected result>
					case ValidationType.GreaterThan:
						if (result.ActualResult < result.ExpectedResult)
						{
							Assert.Inconclusive(result.ErrorMessage);
						}
						break;
					case ValidationType.LessThan:
						if (result.ActualResult > result.ExpectedResult)
						{
							Assert.Inconclusive(result.ErrorMessage);
						}
						break;
				}
		}

		struct PerformanceResult
		{
			public double ActualResult;
			public double ExpectedResult;
			public string ErrorMessage;
			// Validate that result is <validation type> than expected result>
			public ValidationType ValidationType;
		}

		enum ValidationType
		{
			GreaterThan,
			LessThan
		}
	}
}
