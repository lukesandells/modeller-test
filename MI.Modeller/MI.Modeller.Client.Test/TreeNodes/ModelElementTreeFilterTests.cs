﻿using MI.Framework.ObjectBrowsing;
using MI.Framework.ObjectBrowsing.Mapping;
using MI.Modeller.Client.TreeNodes;
using MI.Modeller.NodeMappings.Resources;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;
using NUnit.Framework.Internal;
using System;
using System.Linq;
using MI.Framework;
using MI.Framework.ObjectBrowsing.Persistence;
using MI.Modeller.Domain;
using MI.Modeller.NodeMappings;

namespace MI.Modeller.Client.Test.TreeNodes
{
	[TestFixture]
	class ModelElementTreeFilterTests:BaseModellerNodeTest
	{
		[Test]
		public void Test_Construcor_NullNodeType_ThrowsException()
		{
			// Act/Assert
			Assert.Throws<ArgumentNullException>(() => new ModelElementTreeFilter(null), "Null node type does not throw exception as expected");
		}

		[Test]
		public void Test_IsSelectable_NoHierarchyScope_ReturnsAllOfType()
		{
			// Arrange
			TestHierarchyScopeNodeMapping.SecondaryFolder<Equipment>(NodeTypeMember.Folder.Equipment, "Equipment", true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});
			RegisterNodeMappings(new INodeMapping[] { new EquipmentClassNodeMapping(), new EquipmentNodeMapping() });
			var ent1 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent1", new object[] { EquipmentLevel.Enterprise });
			var eq1 = ent1.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq1", new object[] { EquipmentLevel.Enterprise });
			var ent2 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent2", new object[] { EquipmentLevel.Enterprise });
			var eq2 = ent2.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq2", new object[] { EquipmentLevel.Enterprise });

			SetupSession();
			SessionMock.Setup(m => m.Query<DesignObject>())
				.Returns(new[]
				{
					(DesignObject) ent1.TargetObject,
					(DesignObject) ent2.TargetObject,
					(DesignObject) eq1.TargetObject,
					(DesignObject) eq2.TargetObject
				}.AsQueryable());
			SessionMock.Setup(m => m.Query<ModelElement>())
				.Returns(new[]
				{
					(ModelElement) ent1,
					(ModelElement) ent2,
					(ModelElement) eq1,
					(ModelElement) eq2
				}.AsQueryable());
			var filter = new ModelElementTreeFilter(NodeType.For(typeof(Equipment)));

			// Act
			var selectableNodes = filter.SelectableNodes();

			// Assert
			CollectionAssert.AreEquivalent(new[] {
					eq1,
					eq2},
				selectableNodes.ToList(),
				"Selectable nodes not as expected");
		}

		[Test]
		public void Test_IsSelectable_HierarchyScope_ReturnsOnlyVisible()
		{
			// Arrange
			TestHierarchyScopeNodeMapping.SecondaryFolder<Equipment>(NodeTypeMember.Folder.Equipment, "Equipment", true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});
			RegisterNodeMappings(new INodeMapping[] { new EquipmentClassNodeMapping(), new EquipmentNodeMapping() });
			var ent1 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent1", new object[] { EquipmentLevel.Enterprise });
			var eq1 = ent1.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq1", new object[] { EquipmentLevel.Enterprise });
			var ent2 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent2", new object[] { EquipmentLevel.Enterprise });
			var eq2 = ent2.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq2", new object[] { EquipmentLevel.Enterprise });

			SetupSession();
			SessionMock.Setup(m => m.Query<HierarchyScopedObject>())
				.Returns(new[]
				{
					(HierarchyScopedObject) eq1.TargetObject,
					(HierarchyScopedObject) eq2.TargetObject
				}.AsQueryable());
			SessionMock.Setup(m => m.Query<ModelElement>())
				.Returns(new[]
				{
					(ModelElement) ent1,
					(ModelElement) ent2,
					(ModelElement) eq1,
					(ModelElement) eq2
				}.AsQueryable());
			var filter = new ModelElementTreeFilter(NodeType.For(typeof(Equipment)), ent1.TargetObject as HierarchyScope);

			// Act
			var selectableNodes = filter.SelectableNodes();

			// Assert
			CollectionAssert.AreEquivalent(new[] {
					eq1},
				selectableNodes.ToList(),
				"Selectable nodes not as expected");
		}

		[Test]
		public void Test_IsVisible_NoHierarchyScope_AllVisible()
		{
			// Arrange
			TestHierarchyScopeNodeMapping.SecondaryFolder<Equipment>(NodeTypeMember.Folder.Equipment, "Equipment", true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});
			RegisterNodeMappings(new INodeMapping[] { new EquipmentClassNodeMapping(), new EquipmentNodeMapping() });
			var ent1 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent1", new object[] { EquipmentLevel.Enterprise });
			var eq1 = ent1.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq1", new object[] { EquipmentLevel.Enterprise });
			var ent2 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent2", new object[] { EquipmentLevel.Enterprise });
			var eq2 = ent2.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq2", new object[] { EquipmentLevel.Enterprise });

			SetupSession();
			SessionMock.Setup(m => m.Query<DesignObject>())
				.Returns(new[]
				{
					(DesignObject) ent1.TargetObject,
					(DesignObject) ent2.TargetObject,
					(DesignObject) eq1.TargetObject,
					(DesignObject) eq2.TargetObject
				}.AsQueryable());
			SessionMock.Setup(m => m.Query<ModelElement>())
				.Returns(new[]
				{
					(ModelElement) ent1,
					(ModelElement) ent2,
					(ModelElement) eq1,
					(ModelElement) eq2
				}.AsQueryable());
			var filter = new ModelElementTreeFilter(NodeType.For(typeof(Equipment)));

			// Act/Assert
			Assert.IsTrue(filter.IsVisible(ent1), "Node not visible as expected");
			Assert.IsTrue(filter.IsVisible(eq1), "Node not visible as expected");
			Assert.IsTrue(filter.IsVisible(ent2), "Node not visible as expected");
			Assert.IsTrue(filter.IsVisible(eq2), "Node not visible as expected");
		}

		[Test]
		public void Test_IsSelectable_HierarchyScope_OnlyWitinScopeVisible()
		{
			// Arrange
			TestHierarchyScopeNodeMapping.SecondaryFolder<Equipment>(NodeTypeMember.Folder.Equipment, "Equipment", true, false, permittedType =>
			{
				permittedType.InverseProperty(NodeTypeMember.Property.HierarchyScope, true);
				permittedType.CanAddNewIf((parent, @params) => parent.EquipmentLevel.PermitsChild(
					TypeConverter.Convert<ExtensibleEnum<EquipmentLevel>>(@params[0]), PermittedFor.Equipment));
			});
			RegisterNodeMappings(new INodeMapping[] { new EquipmentClassNodeMapping(), new EquipmentNodeMapping() });
			var ent1 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent1", new object[] { EquipmentLevel.Enterprise });
			var eq1 = ent1.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq1", new object[] { EquipmentLevel.Enterprise });
			var ent2 = NodeType.For(typeof(HierarchyScope)).NewDefinitionNode("Ent2", new object[] { EquipmentLevel.Enterprise });
			var eq2 = ent2.AddNewAndPersist(NodeType.For(typeof(Equipment)), "Eq2", new object[] { EquipmentLevel.Enterprise });

			SetupSession();
			SessionMock.Setup(m => m.Query<HierarchyScopedObject>())
				.Returns(new[]
				{
					(HierarchyScopedObject) eq1.TargetObject,
					(HierarchyScopedObject) eq2.TargetObject
				}.AsQueryable());
			SessionMock.Setup(m => m.Query<ModelElement>())
				.Returns(new[]
				{
					(ModelElement) ent1,
					(ModelElement) ent2,
					(ModelElement) eq1,
					(ModelElement) eq2
				}.AsQueryable());
			var filter = new ModelElementTreeFilter(NodeType.For(typeof(Equipment)), ent1.TargetObject as HierarchyScope);

			// Act/Assert
			Assert.IsTrue(filter.IsVisible(ent1), "Node not visible as expected");
			Assert.IsTrue(filter.IsVisible(eq1), "Node not visible as expected");
			Assert.IsFalse(filter.IsVisible(ent2), "Node visible not as expected");
			Assert.IsFalse(filter.IsVisible(eq2), "Node visible not as expected");
		}
	}
}
