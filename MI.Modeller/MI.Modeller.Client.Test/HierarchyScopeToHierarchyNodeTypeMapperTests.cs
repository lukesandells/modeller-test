﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using MI.Modeller.Client.B2mmlMappers;
//using MI.Modeller.Client.Test.B2mmlMapper;
//using MI.Modeller.Client.Test.B2mmlMapper.DomainDataGenerator;
//using MI.Modeller.Domain.Client;
//using NUnit.Framework.Internal;
//using NUnit.Framework;

//namespace MI.Modeller.Client.Test
//{
//    [TestFixture]
//    public class HierarchyScopeToHierarchyNodeTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the hierarchy scope.
//        /// </summary>
//        /// <param name="hierarchyScope">The hierarchy scope.</param>
//        /// <param name="b2mmlHierarchyNodeType">Type of the b2 MML hierarchy scope.</param>
//        public static void CheckHierarchyNodeType(HierarchyScope hierarchyScope,
//            HierarchyNodeType b2mmlHierarchyNodeType)
//        {
//            Assert.IsNotNull(b2mmlHierarchyNodeType,
//                "HierarchyScopeToHierarchyNodeFileTypeMapper returned a null instance given valid input");

//            CheckEquipmentId(hierarchyScope,
//                b2mmlHierarchyNodeType);
//            CheckEquipmentLevel(hierarchyScope,
//                b2mmlHierarchyNodeType);
//            if (hierarchyScope.Parent == null)
//            {
//                Assert.IsNull(b2mmlHierarchyNodeType.ParentHierarchyScope,
//                    string.Format(invalidPropertyMessage,
//                        "HierarchyScope(Parent)"));
//            }
//            else
//            {
//                CheckHierarchyNodeType(hierarchyScope.Parent,
//                    b2mmlHierarchyNodeType.ParentHierarchyScope);
//            }
//        }

//        /// <summary>
//        ///     Checks the equipment level.
//        /// </summary>
//        /// <param name="hierarchyScope">The hierarchy scope.</param>
//        /// <param name="b2mmlHierarchyNodeType">Type of the b2 MML hierarchy scope.</param>
//        private static void CheckEquipmentLevel(HierarchyScope hierarchyScope,
//            HierarchyNodeType b2mmlHierarchyNodeType)
//        {
//            // level may not be null - mandatory
//            Assert.IsNotNull(b2mmlHierarchyNodeType.EquipmentLevel,
//                string.Format(invalidPropertyMessage,
//                    "EquipmentLevel"));
//            EquipmentLevelTypeToEquipmentElementLevelTypeMapperTests.CheckEquipmentElementLevelType(
//                hierarchyScope.EquipmentLevel,
//                b2mmlHierarchyNodeType.EquipmentLevel);
//        }

//        /// <summary>
//        ///     Checks the equipment identifier.
//        /// </summary>
//        /// <param name="hierarchyScope">The hierarchy scope.</param>
//        /// <param name="b2mmlHierarchyNodeType">Type of the b2 MML hierarchy scope.</param>
//        private static void CheckEquipmentId(HierarchyScope hierarchyScope,
//            HierarchyNodeType b2mmlHierarchyNodeType)
//        {
//            // InternalId may not be null either
//            Assert.IsNotNull(b2mmlHierarchyNodeType.EquipmentID,
//                string.Format(invalidPropertyMessage,
//                    "EquipmentID"));
//            Assert.AreEqual(hierarchyScope.EquipmentId,
//                b2mmlHierarchyNodeType.EquipmentID.Value,
//                string.Format(invalidPropertyMessage,
//                    "EquipmentID"));
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var hierarchyScopeToB2mmlMapper = new HierarchyScopeToHierarchyNodeFileTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlHierarchyNodeType = hierarchyScopeToB2mmlMapper.Map(default(HierarchyScope));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "HierarchyScopeToHierarchyNodeFileTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="hierarchyScope">The hierarchy scope.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(HierarchyScopeDataGenerator),
//                 nameof(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes))] HierarchyScope hierarchyScope)
//        {
//            // Arrange
//            var hierarchyScopeToB2mmlMapper = new HierarchyScopeToHierarchyNodeFileTypeMapper();

//            // Act
//            var b2mmlHierarchyNodeType = hierarchyScopeToB2mmlMapper.Map(hierarchyScope);

//            // Assert
//            CheckHierarchyNodeType(hierarchyScope,
//                b2mmlHierarchyNodeType);
//        }

//        /// <summary>
//        ///     Tests to b2 MML returns valid instance.
//        /// </summary>
//        [Test]
//        public void Test_ToB2mml_ReturnsValidInstance()
//        {
//            //Arrange
//            var child = new HierarchyScope("MI.Perth",
//                EquipmentLevel.Area);

//            //parent
//            // ReSharper disable once UnusedVariable
//            var hierarchyScope = new HierarchyScope("MI",
//                new EquipmentLevelType(EquipmentLevelTypeValue.Other,
//                    "otherValue"),
//                new HashSet<HierarchyScope>
//                {
//                    child
//                });
//            var mapper = new HierarchyScopeToHierarchyNodeFileTypeMapper();

//            //Act
//            var type = mapper.Map(child);

//            //Assert
//            Assert.IsNotNull(type,
//                "Mapping of HierachyNodeType instance resulted in a null object reference");
//            Assert.AreEqual("MI.Perth",
//                type.EquipmentID.Value,
//                "Mapping of HierachyNodeType instance resulted in an incorrect EquipmentID");
//            Assert.AreEqual(EquipmentLevelTypeValue.Area.ToString(),
//                type.EquipmentLevel.Value,
//                "Mapping of HierachyNodeType instance resulted in an incorrect Equipment Level");

//        }
//    }
//}

