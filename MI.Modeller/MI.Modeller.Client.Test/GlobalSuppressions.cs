[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Globalization", "CA1303:Do not pass literals as localized parameters", MessageId = "NUnit.Framework.Assert.IsNotNull(System.Object,System.String,System.Object[])", Scope = "member", Target = "MI.Modeller.Client.Test.B2mmlMapper.AssemblyRelationshipTypeFromAssemblyRelationshipTypeMapperTests.#Test_Map_InvalidInputs_ThrowsException(AssemblyRelationshipType)")]
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

