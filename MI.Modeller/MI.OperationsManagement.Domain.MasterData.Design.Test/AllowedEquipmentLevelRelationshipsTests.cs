﻿using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    [TestFixture]
    public class EquipmentLevelExtensionsTests
    {
        [Test]
        public void Test_Enterprise_PermittedParents()
        {
			CollectionAssert.IsEmpty(EquipmentLevel.Enterprise.PermittedParents(PermittedFor.HierarchyScope),
				"Enterprise should have no permitted parents for hierarchy scope.");

			var permittedParents = EquipmentLevel.Enterprise.PermittedParents(PermittedFor.Equipment);
			Assert.IsTrue(permittedParents.Count() == 1 && permittedParents.Contains(EquipmentLevel.Enterprise),
				"Enterprise should permit only Enterprise parent for equipment.");
        }

		[Test]
		public void Test_Enterprise_PermittedChildren()
		{
			var permittedChildren = EquipmentLevel.Enterprise.PermittedChildren(PermittedFor.HierarchyScope);
            Assert.AreEqual(12, permittedChildren.Count(), "Enterprise should have 12 permitted children for hierarchy scope");

            Assert.IsTrue(permittedChildren.All(c => c.IsPermittedForHierarchyScopes()),
				"Enterprise should have 12 permitted children for hierarchy scope, all of which are permitted for hierarchy scopes.");

			permittedChildren = EquipmentLevel.Enterprise.PermittedChildren(PermittedFor.Equipment);
			Assert.IsTrue(permittedChildren.Count() == 15, "Enterprise should have 15 permitted children for hierarchy scope.");
		}

		[Test]
		public void Test_Other_PermittedParents()
		{
			var permittedParents = EquipmentLevel.Other.PermittedParents(PermittedFor.HierarchyScope);
			Assert.IsTrue(permittedParents.Count() == 13, "Other should have 13 permitted parents for hierarchy scope.");

			permittedParents = EquipmentLevel.Other.PermittedParents(PermittedFor.Equipment);
			Assert.IsTrue(permittedParents.Count() == 15, "Other should have 15 permitted parents for equipment.");
		}

		[Test]
		public void Test_Other_PermittedChildren()
		{
			var permittedChildren = EquipmentLevel.Other.PermittedChildren(PermittedFor.Equipment);
			Assert.IsTrue(permittedChildren.Count() == 1 && permittedChildren.Contains(EquipmentLevel.Other),
				"Other should permit only Other child for equipment.");

			permittedChildren = EquipmentLevel.Other.PermittedChildren(PermittedFor.HierarchyScope);
			Assert.IsTrue(permittedChildren.Count() == 1 && permittedChildren.Contains(EquipmentLevel.Other),
				"Other should permit only Other child for hierarchy scope.");
		}

		[Test]
		public void Test_EquipmentModule_ImmediateParents()
		{
			var permittedParents = EquipmentLevel.EquipmentModule.ImmediateParents();
			var workUnits = new EquipmentLevel[] { EquipmentLevel.WorkUnit, EquipmentLevel.WorkCell, EquipmentLevel.Unit, EquipmentLevel.StorageUnit };
			Assert.IsTrue(permittedParents.Count() == workUnits.Length && permittedParents.All(p => workUnits.Contains(p)),
				"Equipment modules should have all work unit equipment levels as immediate parents.");
		}

		[Test]
		public void Test_WorkCenter_PermittedChildren()
		{
			var permittedChildren = EquipmentLevel.WorkCenter.PermittedChildren(PermittedFor.HierarchyScope);
			var workUnitsAndOther = new EquipmentLevel[] { EquipmentLevel.WorkUnit, EquipmentLevel.WorkCell, EquipmentLevel.Unit, EquipmentLevel.StorageUnit, EquipmentLevel.Other };
			
			Assert.IsTrue(permittedChildren.Count() == workUnitsAndOther.Length && permittedChildren.All(c => workUnitsAndOther.Contains(c)),
				"Permitted children for work center for hierarchy scope should contain all work unit equipment levels and other.");

			var workCenters = new EquipmentLevel[] { EquipmentLevel.WorkCenter, EquipmentLevel.ProductionLine, EquipmentLevel.ProcessCell, EquipmentLevel.ProductionUnit, EquipmentLevel.StorageZone };
			var processControlLevels = new EquipmentLevel[] { EquipmentLevel.EquipmentModule, EquipmentLevel.ControlModule };
			var allLevelsBelowArea = workUnitsAndOther.Union(workCenters.Union(processControlLevels));

			permittedChildren = EquipmentLevel.WorkCenter.PermittedChildren(PermittedFor.Equipment);
			Assert.IsTrue(permittedChildren.Count() == allLevelsBelowArea.Count() && permittedChildren.All(c => allLevelsBelowArea.Contains(c)),
				"Permitted children for work center for equipment should contain all equipment levels below Area.");
		}

		[Test]
		public void Test_StorageZone_PermittedChildren()
		{
			var permittedChildren = EquipmentLevel.StorageZone.PermittedChildren(PermittedFor.HierarchyScope);
			var storageUnitAndOther = new EquipmentLevel[] { EquipmentLevel.StorageUnit, EquipmentLevel.Other };
			Assert.IsTrue(permittedChildren.Count() == storageUnitAndOther.Count() && permittedChildren.All(c => storageUnitAndOther.Contains(c)),
				"Permitted children for storage zone for hierarchy scope should contain Storage Unit and Other.");
		}
    }
}