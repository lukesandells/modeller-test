﻿using NUnit.Framework;
using System;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

// ReSharper disable UnusedVariable

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class HierarchyScopeTests.
    /// </summary>
    [TestFixture]
    internal class HierarchyScopeTests
    {
        [Test]
        public void Test_Constructor_DoubleEnterprise_ThrowsException()
        {
            // arrange
            var enterpriseHierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var newHierarchyScope = new HierarchyScope("InternalId",
                EquipmentLevel.Enterprise);

            // Act Assert
            Assert.Throws<InvalidOperationException> (delegate { enterpriseHierarchyScope.AddChild(newHierarchyScope); },
                "Creating an Enterprise HierarchyScope with a parent Enterprise HierarchyScope did not throw an exception as expected");
        }

        [Test]
        public void Test_Constructor_ReturnsValidInstance()
        {
            // Act
            var hierarchyScope = new HierarchyScope("HierarchyScope", EquipmentLevel.Site)
            {
                Description = "Description"
            };

            // Assert
            Assert.IsNotNull(hierarchyScope, "New Hierarchy Scope returned null instance");
            Assert.IsNotNull(hierarchyScope.DefiningEquipment, "New Hierarchy Scope did not create a defining Equipment");
            Assert.AreEqual("HierarchyScope", hierarchyScope.DefiningEquipment.ExternalId, "New Hierarchy Scope's defining equipment did not have the expected external id");
            Assert.AreEqual(EquipmentLevel.Site, (EquipmentLevel) hierarchyScope.DefiningEquipment.EquipmentLevel, "New Hierarchy Scope's defining equipment did not have the expected equipment level");
            Assert.AreEqual("Description", hierarchyScope.Description, "New Hierarchy Scope did not have the expected Description");
            Assert.AreEqual("HierarchyScope", hierarchyScope.ExternalId, "New Hierarchy Scope did not have the defining ExternalId");
            Assert.AreEqual(EquipmentLevel.Site, (EquipmentLevel)hierarchyScope.EquipmentLevel, "New Hierarchy Scope did not have the expected Equipment Level");
            Assert.AreEqual(hierarchyScope, hierarchyScope.DefiningEquipment.HierarchyScope, "New Hierarchy Scope did not set the defining equipment's Hierarhy Scope as expected");
        }
    }
}