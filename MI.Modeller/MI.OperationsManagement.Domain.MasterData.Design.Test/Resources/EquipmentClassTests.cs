﻿using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class EquipmentClassTests.
    /// </summary>
    [TestFixture]
    public class EquipmentClassTests
    {
        /// <summary>
        ///     Tests the constructor with valid values returns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "EquipmentClass.ID.001";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);
            var equipmentLevel = EquipmentLevel.Area;

            // Act
            var newEquipment = new EquipmentClass(hierarchyScope,
                id,
                equipmentLevel)
            { 
               Description =  description,
                EquipmentLevel = equipmentLevel
            };

            // Assert
            Assert.IsNotNull(newEquipment,
                "Instance Creation of Equipment object resulted in a null object reference");
            Assert.AreEqual(id,
                newEquipment.ExternalId,
                "Equipment Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(description,
                newEquipment.Description,
                "Equipment Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newEquipment.HierarchyScope,
                "Equipment Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(equipmentLevel,
                (EquipmentLevel) newEquipment.EquipmentLevel,
                "Equipment Creation resulted in an object with the incorrect EquipmentLevel");
        }

        [Test]
        public void Test_AddEquipment_UpdatesInstanceCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var newEquipmentClass = new EquipmentClass(hierarchyScope,
                "EquipmentClass.ID",
                EquipmentLevel.Site);
            var equipment = new Equipment(hierarchyScope, "Eq", EquipmentLevel.Site);


            //Act
            newEquipmentClass.AddMember(equipment);

            // Assert
            Assert.AreEqual(1,
                newEquipmentClass.Members.Count(),
                "Adding Equipment did not Update the Equipment Class as expected");
            Assert.IsTrue(newEquipmentClass.Members.Contains(equipment),
                "Adding Equipment did not add the equipment to the class");
            Assert.IsTrue(equipment.EquipmentClasses.Contains(newEquipmentClass),
                "Adding Equipment did not add the class to the equipment");
        }

        [Test]
        public void Test_RemoveEquipment_UpdatesInstanceCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var newEquipmentClass = new EquipmentClass(hierarchyScope,
                "EquipmentClass.ID",
                EquipmentLevel.Site);
            var equipment = new Equipment(hierarchyScope, "Eq", EquipmentLevel.Site);
            newEquipmentClass.AddMember(equipment);

            //Act
            
            newEquipmentClass.RemoveMember(equipment);

            // Assert
            Assert.AreEqual(0,
                newEquipmentClass.Members.Count(),
                "Removing Equipment did not Update the Equipment Class as expected");
            Assert.IsFalse(newEquipmentClass.Members.Contains(equipment),
                "Removing Equipment did not remove the equipment from the class");
            Assert.IsFalse(equipment.EquipmentClasses.Contains(newEquipmentClass),
                "Removing Equipment did not remove the class from the equipment");
        }

	    [Test]
	    public void Test_PromoteToHierarchyScope_Correct()
	    {
			// Arrange
			var enterprise = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var definingEquipment = new Equipment(enterprise, "HierarchyScope", EquipmentLevel.Site)
			{
				Description = "Description"
			};

			// Act
		    var hierarchyScope = definingEquipment.PromoteToHierarchyScope();

			// Assert
			Assert.IsNotNull(hierarchyScope, "New Hierarchy Scope returned null instance");
			Assert.AreEqual(definingEquipment, hierarchyScope.DefiningEquipment, "New Hierarchy Scope did not have the expected defining equipment");
			Assert.AreEqual("Description", hierarchyScope.Description, "New Hierarchy Scope did not have the expected Description");
			Assert.AreEqual("HierarchyScope", hierarchyScope.ExternalId, "New Hierarchy Scope did not have the defining ExternalId");
			Assert.AreEqual(EquipmentLevel.Site, (EquipmentLevel)hierarchyScope.EquipmentLevel, "New Hierarchy Scope did not have the expected Equipment Level");
			Assert.AreEqual(enterprise, hierarchyScope.Parent, "New Hierarchy Scope did not have the expected parent");
			Assert.AreEqual(hierarchyScope, definingEquipment.HierarchyScope, "New Hierarchy Scope did not set the defining equipment's Hierarhy Scope as expected");
		}
	}
}