﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class EquipmentTests.
    /// </summary>
    [TestFixture]
    public class EquipmentTests
    {/// <summary>
        ///     Tests the constructor with valid values returns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "Equipment.ID.001";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);
            var equipmentLevel = EquipmentLevel.Area;

            // Act
            var newEquipment = new Equipment(hierarchyScope,
                id,
                equipmentLevel)
            {
                Description = description
            };

            // Assert
            Assert.IsNotNull(newEquipment,
                "Instance Creation of Equipment object resulted in a null object reference");
            Assert.AreEqual(id,
                newEquipment.ExternalId,
                "Equipment Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(description,
                newEquipment.Description,
                "Equipment Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newEquipment.HierarchyScope,
                "Equipment Creation resulted in an object with the incorrect HierarchyScope");
            Assert.IsFalse(newEquipment.EquipmentLevel.HasOtherValue);
            Assert.AreEqual(equipmentLevel,
                (EquipmentLevel) newEquipment.EquipmentLevel,
                "Equipment Creation resulted in an object with the incorrect EquipmentLevel");
            Assert.IsFalse(newEquipment.DefinesHierarchyScope,
                "New equipment should not define a Hierarchy Scope");
        }
    }
}