﻿using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class MaterialClassTests.
    /// </summary>
    [TestFixture]
    public class MaterialClassTests
    {
        [Test]
        public void Test_AddMaterialDefinition_UpdatesInstanceCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var newMaterialClass = new MaterialClass(hierarchyScope,
                "MaterialClass.ID");
            var definition = new MaterialDefinition(hierarchyScope, "def");

            // Act 
            newMaterialClass.AddMember(definition);

            // Assert
            Assert.AreEqual(1, newMaterialClass.Members.Count(), "Adding material definition did not add to collection");
            Assert.IsTrue(newMaterialClass.Members.Contains(definition), "Adding new material definition did not add definition to class");
            Assert.IsTrue(definition.MaterialClasses.Contains(newMaterialClass), "Adding new material definition did not add class to definition");
        }

        /// <summary>
        ///     Tests the constructor with valid values returns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "MaterialClass.ID.001";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);
            var assemblyType = AssemblyType.Physical;
            var assemblyRelationship = AssemblyRelationship.Permanent;

            // Act
            var newMaterialClass = new MaterialClass(hierarchyScope,
                id)
            {
                Description = description,
                AssemblyType = assemblyType,
                AssemblyRelationship = assemblyRelationship
            };

            // Assert
            Assert.IsNotNull(newMaterialClass,
                "Instance Creation of MaterialClass object resulted in a null object reference");
            Assert.AreEqual(id,
                newMaterialClass.ExternalId,
                "MaterialClass Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(description,
                newMaterialClass.Description,
                "MaterialClass Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newMaterialClass.HierarchyScope,
                "MaterialClass Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(assemblyType,
                (AssemblyType) newMaterialClass.AssemblyType,
                "MaterialClass Creation resulted in an object with the incorrect AssemblyType");
            Assert.AreEqual(assemblyRelationship,
                (AssemblyRelationship) newMaterialClass.AssemblyRelationship,
                "MaterialClass Creation resulted in an object with the incorrect AssemblyRelationship");
        }

        [Test]
        public void Test_RemoveMaterialDefinition_UpdatesInstanceCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var newMaterialClass = new MaterialClass(hierarchyScope,
                "MaterialClass.ID");
            var definition = new MaterialDefinition(hierarchyScope, "def");
            newMaterialClass.AddMember(definition);

            // Act
            newMaterialClass.RemoveMember(definition);

            // Assert
            // Assert
            Assert.AreEqual(0, newMaterialClass.Members.Count(), "Removing material definition did not remove from collection");
            Assert.IsFalse(newMaterialClass.Members.Contains(definition), "Removing new material definition did not remove definition to class");
            Assert.IsFalse(definition.MaterialClasses.Contains(newMaterialClass), "Removing new material definition did not remove class to definition");
        }
    }
}