﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class PhysicalAssetClassTests.
    /// </summary>
    [TestFixture]
    public class PhysicalAssetClassTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "PhysicalAssetClass.ID.001";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);

            // Act
            var newPhysicalAsset = new PhysicalAssetClass(hierarchyScope,
                id)
            {
                Description = description,
            };

            // Assert
            Assert.IsNotNull(newPhysicalAsset,
                "Instance Creation of PhysicalAsset object resulted in a null object reference");
            Assert.AreEqual(id,
                newPhysicalAsset.ExternalId,
                "PhysicalAsset Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(description,
                newPhysicalAsset.Description,
                "PhysicalAsset Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newPhysicalAsset.HierarchyScope,
                "PhysicalAsset Creation resulted in an object with the incorrect HierarchyScope");

            Assert.IsNotNull(newPhysicalAsset.Manufacturers,
                "Manufacturers");
        }

        [Test]
        public void Test_AddManufacter_Adds()
        {
            // Arrange 
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);
            var physicalAsset = new PhysicalAssetClass(hierarchyScope, "Phyiscal Asset");

            // Act
            physicalAsset.AddManufacturer("Manufacturer");

            // Assert
            CollectionAssert.AreEquivalent(new[] { "Manufacturer" }, physicalAsset.Manufacturers, "Adding manufacturer did not add as expected");
        }

        [Test]
        public void Test_RemoveManufacter_Removes()
        {
            // Arrange 
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);
            var physicalAsset = new PhysicalAssetClass(hierarchyScope, "Phyiscal Asset");
            physicalAsset.AddManufacturer("Manufacturer");

            // Act
            physicalAsset.RemoveManufacturer("Manufacturer");

            // Assert
            CollectionAssert.IsEmpty(physicalAsset.Manufacturers, "Removing manufacturer did not remove as expected");
        }
    }
}