﻿using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class MaterialDefinitionTests.
    /// </summary>
    [TestFixture]
    public class MaterialDefinitionTests
    {
        [Test]
        public void Test_AddMaterialClass_UpdatesInstanceCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var newMaterialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition.ID");
            var materialClass = new MaterialClass(hierarchyScope, "Class");

            // Act 
            newMaterialDefinition.AddToClass(materialClass);

            // Assert
            Assert.AreEqual(1, newMaterialDefinition.MaterialClasses.Count(), "Adding class to definition did not add to definition");
            Assert.IsTrue(newMaterialDefinition.MaterialClasses.Contains(materialClass), "Adding class to definition did not add to definition");
            Assert.IsTrue(materialClass.Members.Contains(newMaterialDefinition), "Adding class to definition did not add to class");
        }

        [Test]
        public void Test_RemoveMaterialClass_UpdatesInstanceCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var newMaterialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition.ID");
            var materialClass = new MaterialClass(hierarchyScope, "Class");

            // Act 
            newMaterialDefinition.RemoveFromMaterialClass(materialClass);

            // Assert
            Assert.AreEqual(0, newMaterialDefinition.MaterialClasses.Count(), "Removing class to definition did not remove from definition");
            Assert.IsFalse(newMaterialDefinition.MaterialClasses.Contains(materialClass), "Removing class to definition did not remove from definition");
            Assert.IsFalse(materialClass.Members.Contains(newMaterialDefinition), "Removing class to definition did not remove from class");
        }

        /// <summary>
        ///     Tests the constructor with valid values returns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "MaterialDefinition.ID.001";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);
            var assemblyType = AssemblyType.Physical;
            var assemblyRelationship = AssemblyRelationship.Permanent;

            // Act
            var newMaterialDefinition = new MaterialDefinition(hierarchyScope,
                id)
            {
                Description = description,
                AssemblyRelationship = assemblyRelationship,
                AssemblyType = assemblyType
            };

            // Assert
            Assert.IsNotNull(newMaterialDefinition,
                "Instance Creation of MaterialDefinition object resulted in a null object reference");
            Assert.AreEqual(id,
                newMaterialDefinition.ExternalId,
                "MaterialDefinition Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(description,
                newMaterialDefinition.Description,
                "MaterialDefinition Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newMaterialDefinition.HierarchyScope,
                "MaterialDefinition Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(assemblyType,
                (AssemblyType) newMaterialDefinition.AssemblyType,
                "MaterialDefinition Creation resulted in an object with the incorrect AssemblyType");
            Assert.AreEqual(assemblyRelationship,
                (AssemblyRelationship) newMaterialDefinition.AssemblyRelationship,
                "MaterialDefinition Creation resulted in an object with the incorrect AssemblyRelationship");
        }
    }
}