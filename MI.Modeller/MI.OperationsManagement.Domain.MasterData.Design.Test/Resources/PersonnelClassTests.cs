﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class PersonnelClassTests.
    /// </summary>
    [TestFixture]
    public class PersonnelClassTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string id = "PersonnelClass.ID.001";
            const string description = "Description";
            var hierarchyScope = new HierarchyScope("Hscope.ID",
                EquipmentLevel.Enterprise);

            // Act
            var newPersonnel = new PersonnelClass(hierarchyScope,
                id)
            {
                Description = description
            };

            // Assert
            Assert.IsNotNull(newPersonnel,
                "Instance Creation of Personnel object resulted in a null object reference");
            Assert.AreEqual(id,
                newPersonnel.ExternalId,
                "Personnel Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(description,
                newPersonnel.Description,
                "Personnel Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newPersonnel.HierarchyScope,
                "Personnel Creation resulted in an object with the incorrect HierarchyScope");
        }
    }
}