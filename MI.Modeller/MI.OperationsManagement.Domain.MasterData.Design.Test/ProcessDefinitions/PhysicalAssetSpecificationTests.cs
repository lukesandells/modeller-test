﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
	/// <summary>
	///     Class PhysicalAssetSpecificationTests.
	/// </summary>
	[TestFixture]
    public class PhysicalAssetSpecificationTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var physicalAssetClass = new PhysicalAssetClass(hierarchyScope,
                "ClassId");
            const string description = "Description";
            const string physicalAssetUse = "physicalAssetUse";
            var geospatialDesignation = new SpatialDefinition("SRID",
                "SRIDA",
                "format",
                "Value");

            // Act
            var newSpecification = new PhysicalAssetSpecification<WorkMaster>(hierarchyScope,
                "SpecId")
            {
                PhysicalAssetClass = physicalAssetClass,
                Description = description,
                PhysicalAssetUse = physicalAssetUse,
                SpatialDefinition = geospatialDesignation,
                Quantity = 250.WithUnit(Mass.Tonne)
            };

            // Assert
            Assert.IsNotNull(newSpecification,
                "Instance Creation of PhysicalAssetSpecification  object resulted in a null object reference");
            Assert.AreEqual("SpecId", newSpecification.ExternalId,
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect ExternalId");
            Assert.AreEqual(physicalAssetClass,
                newSpecification.PhysicalAssetClass,
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect PhysicalAssetClassId");
            Assert.AreEqual(physicalAssetClass,
                newSpecification.PhysicalAssetClass,
                "PhysicalAssetSpecification resulted in an incorrect PhysicalAssetClass ");
            Assert.AreEqual(physicalAssetUse,
                newSpecification.PhysicalAssetUse,
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect PhysicalAssetUse");
            Assert.AreEqual(description,
                newSpecification.Description,
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(geospatialDesignation,
                newSpecification.SpatialDefinition,
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect Spatial definition");
            Assert.AreEqual(hierarchyScope,
                newSpecification.HierarchyScope,
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual((Quantity) 250.WithUnit(Mass.Tonne), 
                newSpecification.Quantity, 
                "PhysicalAssetSpecification Creation resulted in an object with the incorrect quantity");
        }
    }
}