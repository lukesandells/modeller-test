﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using NUnit.Framework;
using System;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class MaterialSpecificationTests.
    /// </summary>
    [TestFixture]
    public class MaterialSpecificationTests
    {

        [Test]
        public void Test_Constructor_ReturnsValidInstance()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var materialClass = new MaterialClass(hierarchyScope, "MaterialClassId");
            var materialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinitionId");
            var spatialDefinition = new SpatialDefinition();

            // Act
            var newSpecification = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec")
            {
                AssemblyRelationship = AssemblyRelationship.Permanent,
                AssemblyType = AssemblyType.Physical,
                Description = "Description",
                MaterialClass = materialClass,
                MaterialDefinition = materialDefinition,
                MaterialUse = MaterialUse.Produced,
                Quantity = 250.WithUnit(Mass.Tonne),
                SpatialDefinition = spatialDefinition
            };

            // Assert
            Assert.IsNotNull(newSpecification, "Creating a new Material Specification did not return a valid instance");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Hierarchy Scope");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct External Id");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Assembly Relationship");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Assembly Type");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Description");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Material Class");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Material Definition");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Material Use");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Quantity");
            Assert.AreEqual(hierarchyScope, newSpecification.HierarchyScope, "Creating a new Material Specification did not set the correct Spatial Definition");
        }

        [Test]
        public void Test_AddAssemblyElement_NullProcessDefinition_OK()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var workMaster = new WorkMaster(hierarchyScope, "WorkMaster");
            var newSpecification1 = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec1");
            workMaster.AddMaterialSpecification(newSpecification1);
            var newSpecification2 = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec2");

            // Act
            newSpecification1.AddAssemblyElement(newSpecification2);

            // Assert
            Assert.AreEqual(1, newSpecification1.AssemblyElements.Count(), "Adding assembly element did not update element count");
            Assert.IsTrue(newSpecification1.AssemblyElements.Contains(newSpecification2), "Adding assembly element did not add to list of element");
            Assert.AreEqual(1, newSpecification2.ParentAssemblies.Count(), "Adding assembly did not add to list of element's parent assemblies");
            Assert.IsTrue(newSpecification2.ParentAssemblies.Contains(newSpecification1), "Adding assembly did not add to list of element's parent assemblies");
	        Assert.AreEqual(workMaster, newSpecification2.ProcessDefinition, "Adding assembly did not update the process definition");
            CollectionAssert.AreEquivalent(new [] { newSpecification1 }, workMaster.MaterialSpecifications, 
                "Adding element did not add it to process' list of material specifications");
        }

        [Test]
        public void Test_AddAssemblyElement_DifferentProcessTree_ThrowsException()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var workMaster = new WorkMaster(hierarchyScope, "WorkMaster");
            var newSpecification1 = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec1");
            workMaster.AddMaterialSpecification(newSpecification1);
            var workMaster2 = new WorkMaster(hierarchyScope, "WorkMaster2");
            var newSpecification2 = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec2");
            workMaster2.AddMaterialSpecification(newSpecification2);

            // Act/Assert
            Assert.Throws<ArgumentException>(() => newSpecification1.AddAssemblyElement(newSpecification2), "Adding specification in different process tree did not throw exception as expected");
        }
    }
}