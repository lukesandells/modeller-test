﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
	/// <summary>
	///     Class OperationsMaterialBillItemTests.
	/// </summary>
	[TestFixture]
    public class OperationsMaterialBillItemTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            const string id = "OperationsMaterialBillItem.ID";
            const string description = "OperationsMaterialBillItem Description";
            var materialClass = new MaterialClass(hierarchyScope, "MaterialClass");
            var materialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition");
            const string useType = "useType.ID";
            var assemblyType = AssemblyType.Logical;
            var assemblyRelationshipType = AssemblyRelationship.Permanent;
            var operationsMaterialBill = new OperationsMaterialBill(hierarchyScope, "OperationsMaterialBill");
            Quantity quantities = 250.WithUnit(Mass.Tonne);

            // Act
            var newOperationsMaterialBillItem = new OperationsMaterialBillItem(hierarchyScope,id)
            {
                Description = description,
                MaterialClass = materialClass,
                MaterialDefinition = materialDefinition,
                UseType = useType,
                AssemblyType = assemblyType,
                AssemblyRelationshipType = assemblyRelationshipType,
                OperationsMaterialBill = operationsMaterialBill,
                Quantity = quantities
            };

            // Assert
            Assert.IsNotNull(newOperationsMaterialBillItem,
                "Instance Creation of OperationsMaterialBillItem object resulted in a null object reference");
            Assert.AreEqual(id,
                newOperationsMaterialBillItem.ExternalId,
                "OperationsMaterialBillItem Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(hierarchyScope,
                newOperationsMaterialBillItem.HierarchyScope,
                "OperationsMaterialBillItem Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(materialClass,
                newOperationsMaterialBillItem.MaterialClass,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect MaterialClassID");
            Assert.AreEqual(materialDefinition,
                newOperationsMaterialBillItem.MaterialDefinition,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect MaterialDefinitionID");
            Assert.AreEqual(useType,
                newOperationsMaterialBillItem.UseType,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect UseType");
            Assert.AreEqual(assemblyType,
                (AssemblyType) newOperationsMaterialBillItem.AssemblyType,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect AssemblyType");
            Assert.AreEqual(assemblyRelationshipType,
                (AssemblyRelationship) newOperationsMaterialBillItem.AssemblyRelationshipType,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect AssemblyRelationshipType");
            Assert.AreEqual(quantities,
                newOperationsMaterialBillItem.Quantity,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect Quantities");
            Assert.AreEqual(operationsMaterialBill,
                newOperationsMaterialBillItem.OperationsMaterialBill,
                "OperationsMaterialBillItem creation resulted in an object with the incorrect OperationsMaterialBill");
        }

        [Test]
        public void Test_AddMaterialSpecification_Success()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            const string id = "OperationsMaterialBillItem.ID";
            var newOperationsMaterialBillItem = new OperationsMaterialBillItem(hierarchyScope, id);
            var materialSpecification = new MaterialSpecification<OperationsSegment>(hierarchyScope, "MaterialSpecfication");

            // Act
            newOperationsMaterialBillItem.AddMaterialSpecification(materialSpecification);

            // Assert
            CollectionAssert.AreEquivalent(new[] { materialSpecification }, newOperationsMaterialBillItem.MaterialSpecifications,
                "Adding Material Specification to Operations Material Bill Item did not add to list");
        }

        [Test]
        public void Test_RemoveMaterialSpecification_Success()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            const string id = "OperationsMaterialBillItem.ID";
            var newOperationsMaterialBillItem = new OperationsMaterialBillItem(hierarchyScope, id);
            var materialSpecification = new MaterialSpecification<OperationsSegment>(hierarchyScope, "MaterialSpecfication");
            newOperationsMaterialBillItem.AddMaterialSpecification(materialSpecification);

            // Act
            newOperationsMaterialBillItem.RemoveMaterialSpecification(materialSpecification);

            // Assert
            CollectionAssert.IsEmpty(newOperationsMaterialBillItem.MaterialSpecifications,
                "Removing Material Specification from Operations Material Bill Item did not remove from list");
        }
    }
}