﻿using NUnit.Framework;
using System;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class OperationsSegmentTests.
    /// </summary>
    [TestFixture]
    public class OperationsSegmentTests
    {
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string newId = "Operations.Definition.ID";
            const string newDescription = "Description String";
            var newOperationsType = ProcessType.Inventory;
            var newHierarchyScope = new HierarchyScope("Equipment.ID.001",
                EquipmentLevel.Enterprise);
            var newDuration = new TimeSpan(1,
                11,
                25,
                33);
            var operationsDefinition = new OperationsDefinition(newHierarchyScope,
                "OpsDef");

            // Act
            var newOperationsSegment = new OperationsSegment(newHierarchyScope, newId)
            {
                Description = newDescription,
                ProcessType = newOperationsType,
                Duration = newDuration
            };
            operationsDefinition.AddOperationsSegment(newOperationsSegment);

            // Assert
            Assert.IsNotNull(newOperationsSegment,
                "Instance Creation of OperationsSegment object resulted in a null object reference");
            Assert.AreEqual(newId,
                newOperationsSegment.ExternalId,
                "OperationsSegment Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(newDescription,
                newOperationsSegment.Description,
                "OperationsSegment Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(newOperationsType,
                newOperationsSegment.ProcessType,
                "OperationsSegment Creation resulted in an object with the incorrect ProcessType");
            Assert.AreEqual(newHierarchyScope,
                newOperationsSegment.HierarchyScope,
                "OperationsSegment Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(newDuration,
                newOperationsSegment.Duration,
                "OperationsSegment Creation resulted in an object with the incorrect Duration");
        }
    }
}