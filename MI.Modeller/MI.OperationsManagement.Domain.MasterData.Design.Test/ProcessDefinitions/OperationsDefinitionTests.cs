﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
	/// <summary>
	///     Class OperationsDefinitionTests.
	/// </summary>
	[TestFixture]
	public class OperationsDefinitionTests
	{
		[Test]
		public void Test_Constructor_WithValidValues_ReturnsValidInstance()
		{
			// Arrange
			const string id = "OperationsDefinition.ID.001";
			const string description = "OperationsDefinition Description";
			var hierarchyScope = new HierarchyScope("Equipment.ID", EquipmentLevel.Enterprise);
			var operationsType = ProcessType.Inventory;
			var billOfMaterials = new OperationsMaterialBill(hierarchyScope, "BOM ID");
			const string workDefinitionId = "Work Definition ID";
			const string billOfResourcesId = "Bill Of Resources ID";

			// Act

			var newOperationsDefinition = new OperationsDefinition(hierarchyScope, id)
			{
				Description = description,
				ProcessType = operationsType,
				BillOfResourcesId = billOfResourcesId,
				WorkDefinitionId = workDefinitionId
			};

			// Assert
			Assert.IsNotNull(newOperationsDefinition, "Instance Creation of OperationsDefinition object resulted in a null object reference");
			Assert.AreEqual(id, newOperationsDefinition.ExternalId, "OperationsDefinition Creation resulted in an object with the incorrect ID");
			Assert.AreEqual(description, newOperationsDefinition.Description, "OperationsDefinition Creation resulted in an object with the incorrect Description");
			Assert.AreEqual(hierarchyScope, newOperationsDefinition.HierarchyScope, "OperationsDefinition Creation resulted in an object with the incorrect HierarchyScope");
			Assert.AreEqual(operationsType.ToString(), newOperationsDefinition.ProcessType.ToString(), "OperationsDefinition Creation resulted in an object with the incorrect ProcessType");
			Assert.AreEqual(workDefinitionId, newOperationsDefinition.WorkDefinitionId, "OperationsDefinition Creation resulted in an object with the incorrect WorkDefinitionID");
			Assert.AreEqual(billOfResourcesId, newOperationsDefinition.BillOfResourcesId, "OperationsDefinition Creation resulted in an object with the incorrect BillOfResourcesID");
		}

		[Test]
		public void Test_AddOperationsSegment_Adds()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Equipment.ID", EquipmentLevel.Enterprise);
			var operationsDefinition = new OperationsDefinition(hierarchyScope, "OperationsDefinitionId");
			var operationsSegment = new OperationsSegment(hierarchyScope, "OperationsSegment");

			// Act
			operationsDefinition.AddOperationsSegment(operationsSegment);

			// Assert
			CollectionAssert.AreEqual(new[] { operationsSegment }, operationsDefinition.OperationsSegments, "Adding Operations Segment did not add as expected");
		}

		[Test]
		public void Test_Remove_OperationsSegment_Removes()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Equipment.ID", EquipmentLevel.Enterprise);
			var operationsDefinition = new OperationsDefinition(hierarchyScope, "OperationsDefinitionId");
			var operationsSegment = new OperationsSegment(hierarchyScope, "OperationsSegment");
			operationsDefinition.AddOperationsSegment(operationsSegment);

			// Act
			operationsDefinition.RemoveOperationsSegment(operationsSegment);

			// Assert
			CollectionAssert.IsEmpty(operationsDefinition.OperationsSegments, "Removing Operations Segment did not remove as expected");
		}

		[Test]
		public void Test_AddOperationsMaterialBill_Adds()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Equipment.ID", EquipmentLevel.Enterprise);
			var operationsDefinition = new OperationsDefinition(hierarchyScope, "OperationsDefinitionId");
			var operationsMaterialBill = new OperationsMaterialBill(hierarchyScope, "OperationsMaterialBill");

			// Act
			operationsDefinition.AddOperationsMaterialBill(operationsMaterialBill);

			// Assert
			CollectionAssert.AreEqual(new[] { operationsMaterialBill }, operationsDefinition.OperationsMaterialBills, "Adding Operations Segment did not add as expected");
		}

		[Test]
		public void Test_Remove_OperationsMaterialBill_Removes()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Equipment.ID", EquipmentLevel.Enterprise);
			var operationsDefinition = new OperationsDefinition(hierarchyScope, "OperationsDefinitionId");
			var operationsMaterialBill = new OperationsMaterialBill(hierarchyScope, "OperationsMaterialBill");
			operationsDefinition.AddOperationsMaterialBill(operationsMaterialBill);

			// Act
			operationsDefinition.RemoveOperationsMaterialBill(operationsMaterialBill);

			// Assert
			CollectionAssert.IsEmpty(operationsDefinition.OperationsMaterialBills, "Removing Operations Segment did not remove as expected");
		}
	}
}