﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
	/// <summary>
	///     Class PersonnelSpecificationTests.
	/// </summary>
	[TestFixture]
    public class PersonnelSpecificationTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var personnelClass = new PersonnelClass(hierarchyScope,
                "PeronnelClassId");
            const string description = "Description";
            const string personnelUse = "personnelUse";
            var geospatialDesignation = new SpatialDefinition("SRID",
                "SRIDA",
                "format",
                "Value");
            Quantity quantity = 250.WithUnit(Mass.Tonne);

            // Act
            var newSpecification = new PersonnelSpecification<WorkMaster>(hierarchyScope,
                "specId")
            {
                PersonnelClass = personnelClass,
                Description = description,
                PersonnelUse = personnelUse,
                SpatialDefinition = geospatialDesignation,
                Quantity = quantity
            };

            // Assert
            Assert.IsNotNull(newSpecification,
                "Instance Creation of PersonnelSpecification  object resulted in a null object reference");
            Assert.AreEqual("specId", newSpecification.ExternalId,
                "PersonnelSpecification Creation resulted in an object with the incorrect PersonId");
            Assert.AreEqual(personnelClass,
                newSpecification.PersonnelClass,
                "PersonnelSpecification Creation resulted in an object with the incorrect PersonnelClassId");
            Assert.AreEqual(personnelClass,
                newSpecification.PersonnelClass,
                "PersonnelSpecification creation resulted in an incorrect personnel class");
            Assert.AreEqual(personnelUse,
                newSpecification.PersonnelUse,
                "PersonnelSpecification Creation resulted in an object with the incorrect PersonnelUse");
            Assert.AreEqual(description,
                newSpecification.Description,
                "PersonnelSpecification Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(geospatialDesignation,
                newSpecification.SpatialDefinition,
                "PersonnelSpecification Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(hierarchyScope,
                newSpecification.HierarchyScope,
                "PersonnelSpecification Creation resulted in an object with the incorrect HierarchyScope");

            Assert.AreEqual(quantity,
                newSpecification.Quantity,
                "PersonnelSpecification Creation resulted in an object with the incorrect Quantity");
        }
    }
}