﻿using NUnit.Framework;
using System;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class ProcessSegmentTests.
    /// </summary>
    [TestFixture]
    public class ProcessSegmentTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string newId = "Operations.Definition.ID";
            const string newDescription = "Description String";
            var newOperationsType = ProcessType.Inventory;
            var newHierarchyScope = new HierarchyScope("Equipment.ID.001", EquipmentLevel.Enterprise);
            var newDuration = new TimeSpan(1, 11, 25, 33);

            // Act
            var newProcessSegment = new ProcessSegment(newHierarchyScope, newId)
            {
                Description = newDescription,
                ProcessType = newOperationsType,
                Duration = newDuration,
            };

            // Assert
            Assert.IsNotNull(newProcessSegment, "Instance Creation of ProcessSegment object resulted in a null object reference");
            Assert.AreEqual(newId, newProcessSegment.ExternalId, "ProcessSegment Creation resulted in an object with the incorrect ID");
            Assert.AreEqual(newDescription, newProcessSegment.Description, "ProcessSegment Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(newOperationsType, (ProcessType) newProcessSegment.ProcessType, "ProcessSegment Creation resulted in an object with the incorrect ProcessType");
            Assert.AreEqual(newHierarchyScope, newProcessSegment.HierarchyScope, "ProcessSegment Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(newDuration, newProcessSegment.Duration, "ProcessSegment Creation resulted in an object with the incorrect Duration");
        }

        [Test]
        public void Test_AddSegmentDependency()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Equipment.ID.001", EquipmentLevel.Enterprise);
            var newProcessSegment = new ProcessSegment(hierarchyScope, "ProcessSegment");
            var segmentDependency = new SegmentDependency<ProcessSegment>("DependencyType");

            // Act
            newProcessSegment.AddSegmentDependency(segmentDependency);

            // Assert
            CollectionAssert.AreEquivalent(new[] { segmentDependency }, newProcessSegment.SegmentDependencies, "Adding segment dependency did not add to list of dependencies");
        }

        [Test]
        public void Test_RemoveSegmentDependency()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Equipment.ID.001", EquipmentLevel.Enterprise);
            var newProcessSegment = new ProcessSegment(hierarchyScope, "ProcessSegment");
            var segmentDependency = new SegmentDependency<ProcessSegment>("DependencyType");
            newProcessSegment.AddSegmentDependency(segmentDependency);

            // Act
            newProcessSegment.RemoveSegmentDependency(segmentDependency);

            // Assert
            CollectionAssert.IsEmpty(newProcessSegment.SegmentDependencies, "Removing segment dependency did not remove from list of dependencies");
        }
    }
}