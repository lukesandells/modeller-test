﻿//using System;
//using System.Collections.Generic;
//using NUnit.Framework;

//namespace MI.Modeller.Domain.Client.Test
//{
//    /// <summary>
//    ///     Class OperationsMaterialBillTests.
//    /// </summary>
//    [TestFixture]
//    public class OperationsMaterialBillTests
//    {
//        /// <summary>
//        ///     Builds the operations material bill items collection.
//        /// </summary>
//        /// <returns>IEnumerable&lt;ISet&lt;OperationsMaterialBillItem&gt;&gt;.</returns>
//        public static IEnumerable<ISet<OperationsMaterialBillItem>> BuildOperationsMaterialBillItemsCollection()
//        {
//            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
//            ISet<OperationsMaterialBillItem> operationsMaterialBillItems = new HashSet<OperationsMaterialBillItem>();
//            for (var i = 0; i < 5; i++)
//            {
//                operationsMaterialBillItems.Add(new OperationsMaterialBillItem($"Op.Bill.Item.ID.{i}", hierarchyScope));
//            }
//            yield return operationsMaterialBillItems;
//        }
      
//        /// <summary>
//        ///     Tests the constructor identifier only returns valid instance.
//        /// </summary>
//        [Test]
//        public void Test_Constructor_IDOnly_ReturnsValidInstance()
//        {
//            // Arrange
//            var newOperationsMaterialBillId = "OperationsMaterialBill.ID.001";

//            // Act
//            var newOperationsMaterialBill = new OperationsMaterialBill(newOperationsMaterialBillId);

//            // Assert
//            Assert.IsNotNull(newOperationsMaterialBill,
//                "Instance Creation of OperationsMaterialBill object resulted in a null object reference");
//            Assert.AreEqual(newOperationsMaterialBillId,
//                newOperationsMaterialBill.ExternalId,
//                "OperationsMaterialBill Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(default(string),
//                newOperationsMaterialBill.Description,
//                "OperationsMaterialBill Creation resulted in an object with the incorrect Description");
//            Assert.AreEqual(default(HierarchyScope),
//                newOperationsMaterialBill.HierarchyScope,
//                "OperationsMaterialBill Creation resulted in an object with the incorrect HierarchyScope");
//            Assert.AreEqual(0,
//                newOperationsMaterialBill.OperationsMaterialBillItems.Count,
//                "OperationsMaterialBill creation without any OperationsMaterialBillItems resulted in an object with OperationsMaterialBillItems");
//        }

      
//        /// <summary>
//        ///     Tests the constructor with valid values returns valid instance.
//        /// </summary>
//        /// <param name="operationsMaterialBillItems">The operations material bill items.</param>
//        [Test]
//        public void Test_Constructor_WithValidValues_ReturnsValidInstance(
//            [ValueSource(nameof(BuildOperationsMaterialBillItemsCollection))] ISet<OperationsMaterialBillItem>
//                operationsMaterialBillItems)

//        {
//            // Arrange
//            const string id = "OperationsMaterialBill.ID.001";
//            const string description = "OperationsMaterialBill Description";
//            var hierarchyScope = new HierarchyScope("Equipment.ID",
//                EquipmentLevel.Enterprise);

//            // Act
//            var newOperationsMaterialBill = new OperationsMaterialBill(id,
//                description,
//                hierarchyScope,
//                operationsMaterialBillItems);

//            // Assert
//            Assert.IsNotNull(newOperationsMaterialBill,
//                "Instance Creation of OperationsMaterialBill object resulted in a null object reference");
//            Assert.AreEqual(id,
//                newOperationsMaterialBill.ExternalId,
//                "OperationsMaterialBill Creation resulted in an object with the incorrect ID");
//            Assert.AreEqual(description,
//                newOperationsMaterialBill.Description,
//                "OperationsMaterialBill Creation resulted in an object with the incorrect Description");
//            Assert.AreEqual(hierarchyScope.EquipmentId,
//                newOperationsMaterialBill.HierarchyScope.EquipmentId,
//                "OperationsMaterialBill Creation resulted in an object with the incorrect HierarchyScope");

//            Helpers.CompareTwoCollections(operationsMaterialBillItems,
//                newOperationsMaterialBill.OperationsMaterialBillItems,
//                "OperationsMaterialBillItem");
//        }

//        [Test]
//        public void Test_RemoveReferenceTo()
//        {
//            // Arrange
//            var hierarchyScope = new HierarchyScope("Ent",
//                EquipmentLevel.Enterprise);
//            var bill = new OperationsMaterialBill(hierarchyScope,
//                "ExtId");
//            var model = new Model(hierarchyScope, "Model", ModelType.OperationsDefinitionModel);

//            // Assert
//            Assert.Throws<NotImplementedException>(() => { bill.RemoveReferenceTo(model); });
//        }
//    }
//}