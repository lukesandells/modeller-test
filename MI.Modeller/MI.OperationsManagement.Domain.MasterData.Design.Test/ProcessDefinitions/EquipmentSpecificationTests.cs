﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
	/// <summary>
	///     Class EquipmentSpecificationTests.
	/// </summary>
	[TestFixture]
    public class EquipmentSpecificationTests
    {
        [Test]
        public void Test_Constructor_WithValidValues_ReturnsValidInstance()

        {
            // Arrange
            const string specificationId = "EquipmentSpecification";
            var hierarchyScope = new HierarchyScope("Ent",
                EquipmentLevel.Enterprise);
            var equipment = new Equipment(hierarchyScope,
                "EquipmentId",
                EquipmentLevel.Area);
            var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site);
            const string description = "Description";
            const string equipmentUse = "equipmentUse";
            var geospatialDesignation = new SpatialDefinition("SRID",
                "SRIDA",
                "format",
                "Value");
             Quantity quantity = 250.WithUnit(Mass.Tonne);

            // Act
            var newSpecification = new EquipmentSpecification<WorkMaster>(hierarchyScope,
                specificationId)
            {
                Equipment = equipment,
                EquipmentClass = equipmentClass,
                Description = description,
                EquipmentUse = equipmentUse,
                Quantity = quantity,
                SpatialDefinition = geospatialDesignation
            };

            // Assert
            Assert.IsNotNull(newSpecification,
                "Instance Creation of EquipmentSpecification  object resulted in a null object reference");
            Assert.AreEqual(equipmentClass,
                newSpecification.EquipmentClass);
            Assert.AreEqual(equipment,
                newSpecification.Equipment);
            Assert.AreEqual(equipmentUse,
                newSpecification.EquipmentUse,
                "EquipmentSpecification Creation resulted in an object with the incorrect EquipmentUse");
            Assert.AreEqual(description,
                newSpecification.Description,
                "EquipmentSpecification Creation resulted in an object with the incorrect Description");
            Assert.AreEqual(geospatialDesignation.Srid,
                newSpecification.SpatialDefinition.Srid,
                "EquipmentSpecification Creation resulted in an object with the incorrect SpatialLocation");
            Assert.AreEqual(hierarchyScope,
                newSpecification.HierarchyScope,
                "EquipmentSpecification Creation resulted in an object with the incorrect HierarchyScope");
            Assert.AreEqual(quantity, 
                newSpecification.Quantity,
                "EquipmentSpecification Creation resulted in an object with the incorrect Quantity");
        }
    }
}