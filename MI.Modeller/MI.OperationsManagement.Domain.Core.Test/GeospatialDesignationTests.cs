﻿using MI.OperationsManagement.Domain.Core;
using NUnit.Framework;

namespace MI.Modeller.Domain.Client.Test
{
    /// <summary>
    ///     Class GeospatialDesignationTests.
    /// </summary>
    [TestFixture]
    public class GeospatialDesignationTests
    {
        /// <summary>
        ///     Tests the constructor retruns valid instance.
        /// </summary>
        [Test]
        public void Test_Constructor_RetrunsValidInstance()
        {
            //Arrange

            //Act
            var geospatialDesignation = new SpatialDefinition("srid",
                "authority",
                "format",
                "value");

            //Assert
            Assert.AreEqual("srid",
                geospatialDesignation.Srid,
                $"Instance Creation of SpatialLocation object resulted in an object with an srid string ({geospatialDesignation.Srid}).");
            Assert.AreEqual("authority",
                geospatialDesignation.SridAuthority,
                $"Instance Creation of SpatialLocation object resulted in an object with an srid authority string ({geospatialDesignation.SridAuthority}).");
            Assert.AreEqual("format",
                geospatialDesignation.Format,
                $"Instance Creation of SpatialLocation object resulted in an object with an format string ({geospatialDesignation.Format}).");
            Assert.AreEqual("value",
                geospatialDesignation.Value,
                $"Instance Creation of SpatialLocation object resulted in an object with an value string ({geospatialDesignation.Value}).");
        }
    }
}