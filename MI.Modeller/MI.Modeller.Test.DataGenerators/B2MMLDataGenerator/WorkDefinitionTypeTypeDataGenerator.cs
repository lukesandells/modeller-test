﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkDefinitionTypeTypeDataGenerator.
    /// </summary>
    public static class WorkDefinitionTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid work definition type types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkDefinitionTypeType&gt;.</returns>
        public static IEnumerable<WorkDefinitionTypeType> GenerateInvalidWorkDefinitionTypeTypes()
        {
            // null no allowed
            yield return default(WorkDefinitionTypeType);
            // type must value a type 
            yield return new WorkDefinitionTypeType();
            // invalid values
            yield return new WorkDefinitionTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid work definition type types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkDefinitionTypeType&gt;.</returns>
        public static IEnumerable<WorkDefinitionTypeType> GenerateValidWorkDefinitionTypeTypes()
        {
            return from WorkDefinitionType value in Enum.GetValues(typeof(WorkDefinitionType))
                select new WorkDefinitionTypeType
                {
                    Value = value.ToString(),
                    OtherValue = value == WorkDefinitionType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}