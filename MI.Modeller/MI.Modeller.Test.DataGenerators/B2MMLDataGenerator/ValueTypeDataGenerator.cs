﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ValueTypeDataGenerator.
    ///     Used for data generation for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class ValueTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid value types.
        /// </summary>
        /// <returns>IEnumerable&lt;ValueType&gt;.</returns>
        public static IEnumerable<ValueType> GenerateInvalidValueTypes()
        {
            // return null
            yield return default(ValueType);

            //invalid units o measure
            foreach (var valueType in UnitOfMeasureTypeDataGenerator.GenerateInvalidUnitOfMeasureTypes()
                .Where(t => t != null)
                .Select(unitOfMeasure => new ValueType
                {
                    UnitOfMeasure = unitOfMeasure
                }))
            {
                yield return valueType;
            }

            foreach (var valueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ValueType
                {
                    DataType = dataType
                }))
            {
                yield return valueType;
            }

            foreach (var valueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ValueType
                {
                    Accuracy = dataType
                }))
            {
                yield return valueType;
            }

            foreach (var valueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ValueType
                {
                    CoverageFactor = dataType
                }))
            {
                yield return valueType;
            }

            foreach (var valueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ValueType
                {
                    ExpandedUncertainty = dataType
                }))
            {
                yield return valueType;
            }

            foreach (var valueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ValueType
                {
                    LevelOfConfidence = dataType
                }))
            {
                yield return valueType;
            }

            foreach (var valueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ValueType
                {
                    Precision = dataType
                }))
            {
                yield return valueType;
            }

            // Invalid Accuracy
            yield return new ValueType
            {
                Accuracy = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid CoverageFactor
            yield return new ValueType
            {
                CoverageFactor = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid ExpandedUncertainty
            yield return new ValueType
            {
                ExpandedUncertainty = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid LevelOfConfidence
            yield return new ValueType
            {
                LevelOfConfidence = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid Precision
            yield return new ValueType
            {
                Precision = new DataTypeType()
                {
                    Value = "A"
                }
            };
        }

        /// <summary>
        ///     Generates the valid value type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ValueType[]&gt;.</returns>
        public static IEnumerable<ValueType[]> GenerateValidValueTypeArrays()
        {
            // null, empty, 1, and 2 records
            yield return null;
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ValueType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ValueType
                    {
                        Key = new IdentifierType
                        {
                            Value = $"ValueType.Key.{j}"
                        }
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid value types.
        /// </summary>
        /// <returns>IEnumerable&lt;ValueType&gt;.</returns>
        public static IEnumerable<ValueType> GenerateValidValueTypes()
        {
            // generate valid value with 
            // no attributes
            // attributes with values

            // all values populated - precision/accuracy
            yield return new ValueType
            {
                Key = new IdentifierType
                {
                    Value = "Value.Key.Value"
                },
                UnitOfMeasure = UnitOfMeasureTypeDataGenerator.GenerateValidUnitOfMeasureTypes()
                    .First(),
                DataType = DataTypeTypeDataGenerator.GenerateValidDataTypeTypes()
                    .First(),
                ValueString = new ValueStringType
                {
                    Value = "Value.ValueString.Value"
                },
                Accuracy = new DataTypeType()
                {
                    Value = "90"
                },
                Precision = new DataTypeType()
                {
                    Value = "95"
                }
            };

            // all values populated - confidence/coverage/expanded
            yield return new ValueType
            {
                Key = new IdentifierType
                {
                    Value = "Value.Key.Value"
                },
                UnitOfMeasure = UnitOfMeasureTypeDataGenerator.GenerateValidUnitOfMeasureTypes()
                    .First(),
                DataType = DataTypeTypeDataGenerator.GenerateValidDataTypeTypes()
                    .First(),
                ValueString = new ValueStringType
                {
                    Value = "Value.ValueString.Value"
                },
                LevelOfConfidence = new DataTypeType()
                {
                    Value = "95"
                },
                CoverageFactor = new DataTypeType()
                {
                    Value = "80"
                },
                ExpandedUncertainty = new DataTypeType()
                {
                    Value = "12"
                }
            };

            // attributes created but not populated (If this is allowed)
            yield return new ValueType
            {
                Key = new IdentifierType(),
                ValueString = new ValueStringType()
            };
            // basic object
            yield return new ValueType();
        }

        #endregion
    }
}