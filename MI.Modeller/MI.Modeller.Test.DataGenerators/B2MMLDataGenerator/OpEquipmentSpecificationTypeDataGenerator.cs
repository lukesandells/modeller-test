﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpEquipmentSpecificationTypeDataGenerator.
    ///     Generate data for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class OpEquipmentSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op equipment specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpEquipmentSpecificationType&gt;.</returns>
        public static IEnumerable<OpEquipmentSpecificationType> GenerateInvalidOpEquipmentSpecificationTypes()
        {
            EquipmentIDType[] equipmentId =
            {
                new EquipmentIDType
                {
                    Value = "InternalId"
                }
            };

            // null ref
            yield return default(OpEquipmentSpecificationType);

            // invalid Quantitities
            foreach (var invalidOpEquipmentSpecificationType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(t => t != null)
                    .Select(quantity => new OpEquipmentSpecificationType
                    {
                        EquipmentID = equipmentId,
                        Quantity = new[]
                        {
                            quantity
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))

            {
                yield return invalidOpEquipmentSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidOpEquipmentSpecificationType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new OpEquipmentSpecificationType
                    {
                        EquipmentID = equipmentId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidOpEquipmentSpecificationType;
            }

            // invalid properties
            foreach (var invalidOpEquipmentSpecificationType in
                OpEquipmentSpecificationPropertyTypeDataGenerator.GenerateInvalidOpEquipmentSpecificationPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new OpEquipmentSpecificationType
                    {
                        EquipmentID = equipmentId,
                        EquipmentSpecificationProperty = new[]
                        {
                            property
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidOpEquipmentSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid op equipment specification type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpEquipmentSpecificationType[]&gt;.</returns>
        public static IEnumerable<OpEquipmentSpecificationType[]> GenerateValidOpEquipmentSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpEquipmentSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpEquipmentSpecificationType
                    {
                        EquipmentClassID = GenerateValidEquipmentClassIdTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        EquipmentID = null,
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        EquipmentUse = new EquipmentUseType
                        {
                            Value = "EquipmentUse"
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        SpatialLocation = 
                            GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                                .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        EquipmentSpecificationProperty =
                            OpEquipmentSpecificationPropertyTypeDataGenerator
                                .GenerateValidOpEquipmentSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op equipment specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpEquipmentSpecificationType&gt;.</returns>
        public static IEnumerable<OpEquipmentSpecificationType> GenerateValidOpEquipmentSpecificationTypes()
        {
            var equipmentUse = new EquipmentUseType
            {
                Value = "EquipmentUse"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpEquipmentSpecificationType
                {
                    EquipmentClassID = null,
                    EquipmentID = GenerateValidEquipmentIdTypeArrays()
                        .FirstOrDefault(p => p?.Length == 1),
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    EquipmentUse = equipmentUse,
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    SpatialLocation = 
                        GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                            .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    EquipmentSpecificationProperty =
                        OpEquipmentSpecificationPropertyTypeDataGenerator
                            .GenerateValidOpEquipmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new OpEquipmentSpecificationType
            {
                EquipmentID = GenerateValidEquipmentIdTypeArrays()
                    .FirstOrDefault(p => p?.Length == 1),
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
            };

            yield return new OpEquipmentSpecificationType
            {
                EquipmentClassID = GenerateValidEquipmentClassIdTypeArrays()
                    .FirstOrDefault(p => p?.Length == 1),
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
            };
        }

        /// <summary>
        ///     Generates the valid equipment class identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;EquipmentClassIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentClassIDType[]> GenerateValidEquipmentClassIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            yield return null;
            for (var i = 0; i < 3; i++)
            {
                var vals = new EquipmentClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentClassIDType
                    {
                        Value = $"ClassID.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid equipment identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;EquipmentIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentIDType[]> GenerateValidEquipmentIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new EquipmentIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentIDType
                    {
                        Value = $"EquipmentID.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}