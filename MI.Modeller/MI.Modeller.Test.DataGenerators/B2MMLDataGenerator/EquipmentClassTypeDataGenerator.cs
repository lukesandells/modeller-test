﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentClassTypeDataGenerator.
    /// </summary>
    public static class EquipmentClassTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment class types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassType&gt;.</returns>
        public static IEnumerable<EquipmentClassType> GenerateInvalidEquipmentClassTypes()
        {
            var equipmentId = new IdentifierType
            {
                Value = "Equipment.InternalId"
            };

            // null ref
            yield return default(EquipmentClassType);

            // no ID
            yield return new EquipmentClassType();

            // invalid hierarchy scopes
            foreach (var invalidEquipmentClassType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new EquipmentClassType
                    {
                        ID = equipmentId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidEquipmentClassType;
            }

            // invalid equipment Level
            foreach (var invalidEquipmentClassType in
                EquipmentElementLevelTypeDataGenerator.GenerateInvalidEquipmentElementLevelTypes()
                    .Where(q => q != null)
                    .Select(equipmentLevel => new EquipmentClassType
                    {
                        ID = equipmentId,
                        EquipmentLevel = equipmentLevel
                    }))
            {
                yield return invalidEquipmentClassType;
            }

            // invalid properties
            foreach (var invalidEquipmentClassType in
                EquipmentClassPropertyTypeDataGenerator.GenerateInvalidEquipmentClassPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new EquipmentClassType
                    {
                        ID = equipmentId,
                        EquipmentClassProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidEquipmentClassType;
            }
        }

        /// <summary>
        ///     Generates the valid equipment class type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassType[]&gt;.</returns>
        public static IEnumerable<EquipmentClassType[]> GenerateValidEquipmentClassTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentClassType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentClassType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Equipment.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        EquipmentLevel =
                            EquipmentElementLevelTypeDataGenerator.GenerateValidEquipmentElementLevelTypes()
                                .FirstOrDefault(h => h != null),
                        EquipmentClassProperty =
                            EquipmentClassPropertyTypeDataGenerator.GenerateValidEquipmentClassPropertyTypeArrays()
                                .FirstOrDefault(m => m.Length == 2),
                        EquipmentID = GenerateValidEquipmentIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        EquipmentCapabilityTestSpecificationID = GenerateValidEquipmentTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid equipment class types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassType&gt;.</returns>
        public static IEnumerable<EquipmentClassType> GenerateValidEquipmentClassTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentClassType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Equipment.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    EquipmentLevel = EquipmentElementLevelTypeDataGenerator.GenerateValidEquipmentElementLevelTypes()
                        .FirstOrDefault(h => h != null),
                    EquipmentClassProperty =
                        EquipmentClassPropertyTypeDataGenerator.GenerateValidEquipmentClassPropertyTypeArrays()
                            .FirstOrDefault(m => m.Length == i),
                    EquipmentID = GenerateValidEquipmentIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    EquipmentCapabilityTestSpecificationID = GenerateValidEquipmentTestSpecificationIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    ContainedEquipmentClass = GenerateValidEquipmentClassTypeArrays().FirstOrDefault(e => e.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new EquipmentClassType
            {
                ID = new IdentifierType
                {
                    Value = "Equipment.ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .FirstOrDefault(h => h != null)
            };
        }

        /// <summary>
        ///     Generates the valid equipment identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentIDType[]> GenerateValidEquipmentIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new EquipmentIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentIDType
                    {
                        Value = $"EquipmentIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid equipment test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentCapabilityTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentCapabilityTestSpecificationIDType[]>
            GenerateValidEquipmentTestSpecificationIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new EquipmentCapabilityTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentCapabilityTestSpecificationIDType
                    {
                        Value = $"EquipmentCapabilityTestSpecificationIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}