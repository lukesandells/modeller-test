﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class MaterialClassPropertyTypeDataGenerator.
    /// </summary>
    public static class MaterialClassPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid material property types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassPropertyType&gt;.</returns>
        public static IEnumerable<MaterialClassPropertyType> GenerateInvalidMaterialClassPropertyTypes()
        {
            // null ref
            yield return default(MaterialClassPropertyType);

            // No InternalId
            yield return new MaterialClassPropertyType();

            // invalid Values
            foreach (var invalidMaterialClassPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new MaterialClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "MaterialClassProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidMaterialClassPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid material property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassPropertyType[]&gt;.</returns>
        public static IEnumerable<MaterialClassPropertyType[]> GenerateValidMaterialClassPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialClassPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"MaterialClassProperty.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        MaterialTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid material property types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassPropertyType&gt;.</returns>
        public static IEnumerable<MaterialClassPropertyType> GenerateValidMaterialClassPropertyTypes()
        {
            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new MaterialClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"MaterialClassProperty.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    MaterialTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    MaterialClassProperty = GenerateValidMaterialClassPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new MaterialClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"MaterialClassProperty.InternalId.{i}"
                    }
                };
            }
        }

        /// <summary>
        ///     Generates the valid test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<MaterialTestSpecificationIDType[]> GenerateValidTestSpecificationIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialTestSpecificationIDType
                    {
                        Value = $"Test Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}