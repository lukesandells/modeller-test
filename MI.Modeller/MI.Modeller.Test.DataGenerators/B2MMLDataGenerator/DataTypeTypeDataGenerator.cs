﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class DataTypeTypeDataGenerator.
    ///     Generates DataTypeDtpe objects for testing
    /// </summary>
    public static class DataTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid data type types.
        /// </summary>
        /// <returns>IEnumerable&lt;DataTypeType&gt;.</returns>
        public static IEnumerable<DataTypeType> GenerateInvalidDataTypeTypes()
        {
            // default not allowed
            yield return default(DataTypeType);
            // datatype must have a type
            yield return new DataTypeType();
            // invalid type
            yield return new DataTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid data type types.
        /// </summary>
        /// <returns>IEnumerable&lt;DataTypeType&gt;.</returns>
        public static IEnumerable<DataTypeType> GenerateValidDataTypeTypes()
        {
            return from DataType value in Enum.GetValues(typeof(DataType))
                select new DataTypeType
                {
                    Value = value.ToString(),
                    OtherValue = value == DataType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}