﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class QuantityValueTypeDataGenerator.
    ///     Generate Data for Testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class QuantityValueTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid quantity value types.
        /// </summary>
        /// <returns>IEnumerable&lt;QuantityValueType&gt;.</returns>
        public static IEnumerable<QuantityValueType> GenerateInvalidQuantityValueTypes()
        {
            // return null
            yield return default(QuantityValueType);

            // invalid units o measure
            foreach (var quantityValueType in UnitOfMeasureTypeDataGenerator.GenerateInvalidUnitOfMeasureTypes()
                .Where(t => t != null)
                .Select(unitOfMeasure => new QuantityValueType
                {
                    UnitOfMeasure = unitOfMeasure
                }))
            {
                yield return quantityValueType;
            }

            foreach (var quantityValueType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new QuantityValueType
                {
                    DataType = dataType
                }))
            {
                yield return quantityValueType;
            }

            // Invalid Accuracy
            yield return new QuantityValueType
            {
                Accuracy = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid CoverageFactor
            yield return new QuantityValueType
            {
                CoverageFactor = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid ExpandedUncertainty
            yield return new QuantityValueType
            {
                ExpandedUncertainty = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid LevelOfConfidence
            yield return new QuantityValueType
            {
                LevelOfConfidence = new DataTypeType()
                {
                    Value = "A"
                }
            };

            // Invalid Precision
            yield return new QuantityValueType
            {
                Precision = new DataTypeType()
                {
                    Value = "A"
                }
            };
        }

        /// <summary>
        ///     Generates the valid quantity value type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;QuantityValueType[]&gt;.</returns>
        public static IEnumerable<QuantityValueType[]> GenerateValidQuantityValueTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new QuantityValueType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new QuantityValueType
                    {
                        Key = new IdentifierType
                        {
                            Value = $"QuantityValueType.Key.{j}"
                        }
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        /// <summary>
        ///     Generates the valid quantity value types.
        /// </summary>
        /// <returns>IEnumerable&lt;QuantityValueType&gt;.</returns>
        public static IEnumerable<QuantityValueType> GenerateValidQuantityValueTypes()
        {
            // generate valid value with 
            // no attributes
            // attributes with values

            // all values populated - precision/accuracy
            yield return new QuantityValueType
            {
                Key = new IdentifierType
                {
                    Value = "Quantity.Key.Value"
                },
                UnitOfMeasure = UnitOfMeasureTypeDataGenerator.GenerateValidUnitOfMeasureTypes()
                    .First(),
                DataType = DataTypeTypeDataGenerator.GenerateValidDataTypeTypes()
                    .First(),
                QuantityString = new QuantityStringType
                {
                    Value = "Quantity.QuantityString.Value"
                },
                Accuracy = new DataTypeType()
                {
                    Value = "90"
                },
                Precision = new DataTypeType()
                {
                    Value = "95"
                }
            };

            // all values populated - confidence/coverage/expanded
            yield return new QuantityValueType
            {
                Key = new IdentifierType
                {
                    Value = "Quantity.Key.Value"
                },
                UnitOfMeasure = UnitOfMeasureTypeDataGenerator.GenerateValidUnitOfMeasureTypes()
                    .First(),
                DataType = DataTypeTypeDataGenerator.GenerateValidDataTypeTypes()
                    .First(),
                QuantityString = new QuantityStringType
                {
                    Value = "Quantity.QuantityString.Value"
                },
                LevelOfConfidence = new DataTypeType()
                {
                    Value = "95"
                },
                CoverageFactor = new DataTypeType()
                {
                    Value = "80"
                },
                ExpandedUncertainty = new DataTypeType()
                {
                    Value = "12"
                }
            };

            // attributes created but npt populated (If this is allowed)
            yield return new QuantityValueType
            {
                Key = new IdentifierType(),
                QuantityString = new QuantityStringType()
            };
            // basic object
            yield return new QuantityValueType();
        }

        #endregion
    }
}