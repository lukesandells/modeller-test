﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class HierarchyScopeTypeDataGenerator.
    ///     Data Generator for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    public static class HierarchyScopeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid hierarchy scope types.
        /// </summary>
        /// <returns>IEnumerable&lt;HierarchyScopeType&gt;.</returns>
        public static IEnumerable<HierarchyScopeType> GenerateInvalidHierarchyScopeTypes()
        {
            // null ref
            yield return default(HierarchyScopeType);
            // no ID
            yield return new HierarchyScopeType();
            // invalid 
            foreach (var hierarchyScopeType in
                EquipmentElementLevelTypeDataGenerator.GenerateInvalidEquipmentElementLevelTypes()
                    .Select(invalidEquipmentLevel => new HierarchyScopeType
                    {
                        EquipmentID = new EquipmentIDType
                        {
                            Value = "InternalId.Of.Invalid.Object"
                        },
                        EquipmentLevel = invalidEquipmentLevel
                    }))
            {
                yield return hierarchyScopeType;
            }
        }

        /// <summary>
        ///     Generates the valid hierarchy scope types.
        /// </summary>
        /// <returns>IEnumerable&lt;HierarchyScopeType&gt;.</returns>
        public static IEnumerable<HierarchyScopeType> GenerateValidHierarchyScopeTypes()
        {
         
          // Site in Area
            yield return new HierarchyScopeType
            {
                EquipmentID = new EquipmentIDType
                {
                    Value = "Ent.ID.1"
                },
                EquipmentLevel = new EquipmentElementLevelType
                {
                    Value = "Enterprise"
                }
            };


            //// Site
            //yield return new HierarchyScopeType
            //{
            //    EquipmentID = new EquipmentIDType
            //    {
            //        Value = "Site.ID.1"
            //    },
            //    EquipmentLevel = new EquipmentElementLevelType
            //    {
            //        Value = "Site"
            //    },
            // };
            
        }

        #endregion
    }
}