﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PhysicalAssetClassTypeDataGenerator.
    /// </summary>
    public static class PhysicalAssetClassTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid physicalAsset class types.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClassType&gt;.</returns>
        public static IEnumerable<PhysicalAssetClassType> GenerateInvalidPhysicalAssetClassTypes()
        {
            var physicalAssetId = new IdentifierType
            {
                Value = "PhysicalAsset.InternalId"
            };

            // null ref
            yield return default(PhysicalAssetClassType);

            // no ID
            yield return new PhysicalAssetClassType();

            // invalid hierarchy scopes
            foreach (var invalidPhysicalAssetClassType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new PhysicalAssetClassType
                    {
                        ID = physicalAssetId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidPhysicalAssetClassType;
            }


            // invalid properties
            foreach (var invalidPhysicalAssetClassType in
                PhysicalAssetClassPropertyTypeDataGenerator.GenerateInvalidPhysicalAssetClassPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new PhysicalAssetClassType
                    {
                        ID = physicalAssetId,
                        PhysicalAssetClassProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidPhysicalAssetClassType;
            }
        }

        /// <summary>
        ///     Generates the valid physicalAsset class type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClassType[]&gt;.</returns>
        public static IEnumerable<PhysicalAssetClassType[]> GenerateValidPhysicalAssetClassTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PhysicalAssetClassType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetClassType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PhysicalAsset.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),

                        PhysicalAssetClassProperty =
                            PhysicalAssetClassPropertyTypeDataGenerator.GenerateValidPhysicalAssetClassPropertyTypeArrays()
                                .FirstOrDefault(m => m.Length == 2),
                        Manufacturer = GenerateValidManufacturerIdArrays().FirstOrDefault(d => d.Length == 2),
                        PhysicalAssetID = GenerateValidAssetIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        PhysicalAssetCapabilityTestSpecificationID = GenerateValidPhysicalAssetCapabilityTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid physicalAsset class types.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClassType&gt;.</returns>
        public static IEnumerable<PhysicalAssetClassType> GenerateValidPhysicalAssetClassTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PhysicalAssetClassType
                {
                    ID = new IdentifierType
                    {
                        Value = $"PhysicalAsset.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    PhysicalAssetClassProperty =
                        PhysicalAssetClassPropertyTypeDataGenerator.GenerateValidPhysicalAssetClassPropertyTypeArrays()
                            .FirstOrDefault(m => m.Length == i),
                    Manufacturer = GenerateValidManufacturerIdArrays().FirstOrDefault(d => d.Length == i),
                    PhysicalAssetID = GenerateValidAssetIdTypeArrays()
                            .FirstOrDefault(c => c.Length == i),
                    PhysicalAssetCapabilityTestSpecificationID = GenerateValidPhysicalAssetCapabilityTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new PhysicalAssetClassType
            {
                ID = new IdentifierType
                {
                    Value = "PhysicalAsset.ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .FirstOrDefault(h => h != null)
            };
        }

        /// <summary>
        ///     Generates the valid physicalAsset identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetIDType[]&gt;.</returns>
        private static IEnumerable<PhysicalAssetIDType[]> GenerateValidAssetIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new PhysicalAssetIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetIDType
                    {
                        Value = $"AssetID.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid physicalAsset identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetIDType[]&gt;.</returns>
        private static IEnumerable<NameType[]> GenerateValidManufacturerIdArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new NameType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new NameType
                    {
                        Value = $"Manufacturer.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }


        /// <summary>
        ///     Generates the valid physicalAsset test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetCapabilityTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<PhysicalAssetCapabilityTestSpecificationIDType[]>
            GenerateValidPhysicalAssetCapabilityTestSpecificationIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new PhysicalAssetCapabilityTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetCapabilityTestSpecificationIDType
                    {
                        Value = $"PhysicalAssetCapabilityTestSpecificationIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}