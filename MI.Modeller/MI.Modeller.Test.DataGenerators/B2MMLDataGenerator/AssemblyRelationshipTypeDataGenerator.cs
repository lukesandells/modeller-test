﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class AssemblyRelationshipTypeDataGenerator.
    /// </summary>
    public static class AssemblyRelationshipTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid assembly relationship types.
        /// </summary>
        /// <returns>IEnumerable&lt;AssemblyRelationshipType&gt;.</returns>
        public static IEnumerable<AssemblyRelationshipType> GenerateInvalidAssemblyRelationshipTypes()
        {
            // default not allowed
            yield return default(AssemblyRelationshipType);
            // datatype must have a type
            yield return new AssemblyRelationshipType();
            // invalid type
            yield return new AssemblyRelationshipType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid assembly relationship types.
        /// </summary>
        /// <returns>IEnumerable&lt;AssemblyRelationshipType&gt;.</returns>
        public static IEnumerable<AssemblyRelationshipType> GenerateValidAssemblyRelationshipTypes()
        {
            return from Domain.Client.AssemblyRelationshipType value in Enum.GetValues(
                    typeof(Domain.Client.AssemblyRelationshipType))
                select new AssemblyRelationshipType
                {
                    Value = value.ToString(),
                    OtherValue = value == Domain.Client.AssemblyRelationshipType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}