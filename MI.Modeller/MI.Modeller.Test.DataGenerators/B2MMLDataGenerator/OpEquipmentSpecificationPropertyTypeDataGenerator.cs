﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpEquipmentSpecificationPropertyTypeDataGenerator.
    /// </summary>
    public static class OpEquipmentSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op equipment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpEquipmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpEquipmentSpecificationPropertyType>
            GenerateInvalidOpEquipmentSpecificationPropertyTypes()
        {
            // null ref
            yield return default(OpEquipmentSpecificationPropertyType);

            // No InternalId
            yield return new OpEquipmentSpecificationPropertyType();

            // invalid Values
            foreach (
                var invalidOpEquipmentSpecificationPropertyType in ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new OpEquipmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "EquipmentSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidOpEquipmentSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidOpEquipmentSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != default(QuantityValueType))
                    .Select(quantity => new OpEquipmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "EquipmentSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidOpEquipmentSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid op equipment specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OpEquipmentSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<OpEquipmentSpecificationPropertyType[]>
            GenerateValidOpEquipmentSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpEquipmentSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpEquipmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"EquipmentSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op equipment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpEquipmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpEquipmentSpecificationPropertyType>
            GenerateValidOpEquipmentSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "EquipmentSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpEquipmentSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    EquipmentSpecificationProperty = GenerateValidOpEquipmentSpecificationPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic 
            yield return new OpEquipmentSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}