﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OperationsTypeTypeDataGenerator.
    ///     Generates OperationsTypeDtpe objects for testing
    /// </summary>
    public static class OperationsTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid operations type types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsTypeType&gt;.</returns>
        public static IEnumerable<OperationsTypeType> GenerateInvalidOperationsTypeTypes()
        {
            // default not allowed
            yield return default(OperationsTypeType);
            // operations type must have a type
            yield return new OperationsTypeType();
            // invalid type
            yield return new OperationsTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid operations type types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsTypeType&gt;.</returns>
        public static IEnumerable<OperationsTypeType> GenerateValidOperationsTypeTypes()
        {
            return from OperationsType value in Enum.GetValues(typeof(OperationsType))
                select new OperationsTypeType
                {
                    Value = value.ToString(),
                    OtherValue = value == OperationsType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}