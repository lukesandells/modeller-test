﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class RelationshipFormTypeDataGenerator.
    /// </summary>
    public static class RelationshipFormTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid relationship form types.
        /// </summary>
        /// <returns>IEnumerable&lt;RelationshipFormType&gt;.</returns>
        public static IEnumerable<RelationshipFormType> GenerateInvalidRelationshipFormTypes()
        {
            // default not allowed
            yield return default(RelationshipFormType);
            // datatype must have a type
            yield return new RelationshipFormType();
            // invalid type
            yield return new RelationshipFormType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid relationship form types.
        /// </summary>
        /// <returns>IEnumerable&lt;RelationshipFormType&gt;.</returns>
        public static IEnumerable<RelationshipFormType> GenerateValidRelationshipFormTypes()
        {
            return
                from ResourceRelationshipFormType value in
                Enum.GetValues(typeof(ResourceRelationshipFormType))
                select new RelationshipFormType
                {
                    Value = value.ToString(),
                    OtherValue = value == ResourceRelationshipFormType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}