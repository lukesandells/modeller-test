﻿// ***********************************************************************
//  Solution    : MI.Modeller
//  FileName    : OperationsDefinitionTypeDataGenerator.cs 
//  Created     : 2016/09/29
//  Modified    : 2016/09/29
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OperationsDefinitionTypeDataGenerator.
    /// </summary>
    public static class OperationsDefinitionTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid operations definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsDefinitionType&gt;.</returns>
        public static IEnumerable<OperationsDefinitionType> GenerateInvalidOperationsDefinitionTypes()
        {
            // null ref
            yield return default(OperationsDefinitionType);

            // null id
            yield return new OperationsDefinitionType();

            // invalid HierarchyScopes
            foreach (var invalidOperationsDefinitionType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(i => i != null)
                    .Select(hierarchyScope => new OperationsDefinitionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsDefinition.InternalId"
                        },
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidOperationsDefinitionType;
            }

            // invalid OperationsTypeType
            foreach (var invalidOperationsDefinitionType in
                OperationsTypeTypeDataGenerator.GenerateInvalidOperationsTypeTypes()
                    .Where(i => i != null)
                    .Select(operationsTypeType => new OperationsDefinitionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsDefinition.InternalId"
                        },
                        OperationsType = operationsTypeType,
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidOperationsDefinitionType;
            }

            // invalid Operations material Bill Items
            foreach (var invalidOperationsDefinitionType in
                OperationsMaterialBillTypeDataGenerator.GenerateInvalidOperationsMaterialBillTypes()
                    .Where(i => i != null)
                    .Select(operationsMaterialBillType => new OperationsDefinitionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsDefinition.InternalId"
                        },
                        OperationsMaterialBill = new[]
                        {
                            operationsMaterialBillType
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidOperationsDefinitionType;
            }

            // invalid Op Segs
            foreach (var generateInvalidOperationsSegmentTypes in
                OperationsSegmentTypeDataGenerator.GenerateInvalidOperationsSegmentTypes()
                    .Where(i => i != null)
                    .Select(val => new OperationsDefinitionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsDefinition.InternalId"
                        },
                        OperationsSegment = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return generateInvalidOperationsSegmentTypes;
            }
        }

        /// <summary>
        ///     Generates the valid operations definition type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsDefinitionType[]&gt;.</returns>
        public static IEnumerable<OperationsDefinitionType[]> GenerateValidOperationsDefinitionTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OperationsDefinitionType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OperationsDefinitionType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"OperationsDefinition.InternalId.{i}.{j}"
                        },
                        Version = new VersionType
                        {
                            Value = "Version"
                        },
                        Description = new[]
                        {
                            new DescriptionType
                            {
                                Value = $"Description{i}.{j}"
                            }
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        OperationsType = OperationsTypeTypeDataGenerator.GenerateValidOperationsTypeTypes()
                            .FirstOrDefault(),
                        PublishedDate = new PublishedDateType
                        {
                            Value = DateTime.Today
                        },
                        BillOfMaterialsID = new BillOfMaterialsIDType
                        {
                            Value = "Bill Of Materials ID"
                        },
                        WorkDefinitionID = new IdentifierType
                        {
                            Value = "Work Definition ID"
                        },
                        BillOfResourcesID = new BillOfResourcesIDType
                        {
                            Value = "Bill Of resources ID"
                        },
                        OperationsMaterialBill =
                            OperationsMaterialBillTypeDataGenerator.GenerateValidOperationsMaterialBillTypeArrays()
                                .FirstOrDefault(p => p.Length == 2),
                        OperationsSegment =
                            OperationsSegmentTypeDataGenerator.GenerateValidOperationsSegmentTypeArrays()
                                .FirstOrDefault(p => p.Length == 2)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid operations definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsDefinitionType&gt;.</returns>
        public static IEnumerable<OperationsDefinitionType> GenerateValidOperationsDefinitionTypes()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OperationsDefinitionType
                {
                    ID = new IdentifierType
                    {
                        Value = $"OperationsDefinition.InternalId.{i}"
                    },
                    Version = new VersionType
                    {
                        Value = "Version"
                    },
                    Description = new[]
                    {
                        new DescriptionType
                        {
                            Value = $"Description{i}"
                        }
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(),
                    OperationsType = OperationsTypeTypeDataGenerator.GenerateValidOperationsTypeTypes()
                        .FirstOrDefault(),
                    PublishedDate = new PublishedDateType
                    {
                        Value = DateTime.Today
                    },
                    BillOfMaterialsID = new BillOfMaterialsIDType
                    {
                        Value = "Bill Of Materials ID"
                    },
                    WorkDefinitionID = new IdentifierType
                    {
                        Value = "Work Definition ID"
                    },
                    BillOfResourcesID = new BillOfResourcesIDType
                    {
                        Value = "Bill Of resources ID"
                    },
                    OperationsMaterialBill =
                        OperationsMaterialBillTypeDataGenerator.GenerateValidOperationsMaterialBillTypeArrays()
                            .FirstOrDefault(p => p.Length == i),
                    OperationsSegment = OperationsSegmentTypeDataGenerator.GenerateValidOperationsSegmentTypeArrays()
                        .FirstOrDefault(p => p.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new OperationsDefinitionType
            {
                ID = new IdentifierType
                {
                    Value = "OperationsDefinition.InternalId"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        #endregion
    }
}