﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PersonnelClassTypeDataGenerator.
    /// </summary>
    public static class PersonnelClassTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid personnel class types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClassType&gt;.</returns>
        public static IEnumerable<PersonnelClassType> GenerateInvalidPersonnelClassTypes()
        {
            var personnelId = new IdentifierType
            {
                Value = "Personnel.InternalId"
            };

            // null ref
            yield return default(PersonnelClassType);

            // no ID
            yield return new PersonnelClassType();

            // invalid hierarchy scopes
            foreach (var invalidPersonnelClassType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new PersonnelClassType
                    {
                        ID = personnelId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidPersonnelClassType;
            }

          
            // invalid properties
            foreach (var invalidPersonnelClassType in
                PersonnelClassPropertyTypeDataGenerator.GenerateInvalidPersonnelClassPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new PersonnelClassType
                    {
                        ID = personnelId,
                        PersonnelClassProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidPersonnelClassType;
            }
        }

        /// <summary>
        ///     Generates the valid personnel class type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClassType[]&gt;.</returns>
        public static IEnumerable<PersonnelClassType[]> GenerateValidPersonnelClassTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PersonnelClassType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonnelClassType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Personnel.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                       
                        PersonnelClassProperty =
                            PersonnelClassPropertyTypeDataGenerator.GenerateValidPersonnelClassPropertyTypeArrays()
                                .FirstOrDefault(m => m.Length == 2),
                        PersonID = GenerateValidPersonIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        QualificationTestSpecificationID = GenerateValidQualificationTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid personnel class types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClassType&gt;.</returns>
        public static IEnumerable<PersonnelClassType> GenerateValidPersonnelClassTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PersonnelClassType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Personnel.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                   PersonnelClassProperty =
                        PersonnelClassPropertyTypeDataGenerator.GenerateValidPersonnelClassPropertyTypeArrays()
                            .FirstOrDefault(m => m.Length == i),
                    PersonID = GenerateValidPersonIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    QualificationTestSpecificationID = GenerateValidQualificationTestSpecificationIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new PersonnelClassType
            {
                ID = new IdentifierType
                {
                    Value = "Personnel.ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .FirstOrDefault(h => h != null)
            };
        }

        /// <summary>
        ///     Generates the valid personnel identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelIDType[]&gt;.</returns>
        private static IEnumerable<PersonIDType[]> GenerateValidPersonIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new PersonIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonIDType
                    {
                        Value = $"PersonIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid personnel test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelCapabilityTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<QualificationTestSpecificationIDType[]>
            GenerateValidQualificationTestSpecificationIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new QualificationTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new QualificationTestSpecificationIDType
                    {
                        Value = $"QualificationTestSpecificationIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}