﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ResourcePropertyTypeDataGenerator.
    /// </summary>
    public static class ResourcePropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid resource property types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourcePropertyType&gt;.</returns>
        public static IEnumerable<ResourcePropertyType> GenerateInvalidResourcePropertyTypes()
        {
            // null ref
            yield return default(ResourcePropertyType);
            // no ID
            yield return new ResourcePropertyType();
            // invalid Values
            foreach (var invalidResourcePropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new ResourcePropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PersonnelSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidResourcePropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid resource property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourcePropertyType[]&gt;.</returns>
        public static IEnumerable<ResourcePropertyType[]> GenerateValidResourcePropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ResourcePropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ResourcePropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PersonnelSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid resource property types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourcePropertyType&gt;.</returns>
        public static IEnumerable<ResourcePropertyType> GenerateValidResourcePropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "ResourceProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourcePropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new ResourcePropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}