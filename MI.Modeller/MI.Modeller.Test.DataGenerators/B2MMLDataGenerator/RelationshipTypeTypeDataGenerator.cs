﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class RelationshipTypeTypeDataGenerator.
    /// </summary>
    public static class RelationshipTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid relationship type types.
        /// </summary>
        /// <returns>IEnumerable&lt;RelationshipTypeType&gt;.</returns>
        public static IEnumerable<RelationshipTypeType> GenerateInvalidRelationshipTypeTypes()
        {
            // default not allowed
            yield return default(RelationshipTypeType);
            // datatype must have a type
            yield return new RelationshipTypeType();
            // invalid type
            yield return new RelationshipTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid relationship type types.
        /// </summary>
        /// <returns>IEnumerable&lt;RelationshipTypeType&gt;.</returns>
        public static IEnumerable<RelationshipTypeType> GenerateValidRelationshipTypeTypes()
        {
            return from ResourceRelationshipType value in Enum.GetValues(typeof(ResourceRelationshipType))
                select new RelationshipTypeType
                {
                    Value = value.ToString(),
                    OtherValue = value == ResourceRelationshipType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}