﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpMaterialSpecificationPropertyTypeDataGenerator.
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    public static class OpMaterialSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op material specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpMaterialSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpMaterialSpecificationPropertyType>
            GenerateInvalidOpMaterialSpecificationPropertyTypes()
        {
            // null ref
            yield return default(OpMaterialSpecificationPropertyType);

            // No InternalId
            yield return new OpMaterialSpecificationPropertyType();

            // invalid Values
            foreach (var invalidOpMaterialSpecificationPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new OpMaterialSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "MaterialSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidOpMaterialSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidOpMaterialSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != null)
                    .Select(quantity => new OpMaterialSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "MaterialSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidOpMaterialSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid op material specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OpMaterialSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<OpMaterialSpecificationPropertyType[]>
            GenerateValidOpMaterialSpecificationPropertyTypeArrays()
        {
            // null, 
            yield return null;
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpMaterialSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpMaterialSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"MaterialSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op material specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpMaterialSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpMaterialSpecificationPropertyType> GenerateValidOpMaterialSpecificationPropertyTypes
            ()
        {
            var id = new IdentifierType
            {
                Value = "MaterialSpecificationProperty.ID"
            };

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpMaterialSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    MaterialSpecificationProperty = GenerateValidOpMaterialSpecificationPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new OpMaterialSpecificationPropertyType
                {
                    ID = id
                };
            }
        }

        #endregion
    }
}