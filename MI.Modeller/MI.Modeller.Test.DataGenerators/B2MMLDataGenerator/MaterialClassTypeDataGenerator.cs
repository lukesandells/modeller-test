﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class MaterialClassTypeDataGenerator.
    /// </summary>
    public static class MaterialClassTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid material definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassType&gt;.</returns>
        public static IEnumerable<MaterialClassType> GenerateInvalidMaterialClassTypes()
        {
            var materialClassId = new IdentifierType
            {
                Value = "materialClassId"
            };

            // null ref
            yield return default(MaterialClassType);

            // no ID
            yield return new MaterialClassType();

            // invalid hierarchy scopes
            foreach (var invalidMaterialClassType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new MaterialClassType
                    {
                        ID = materialClassId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidMaterialClassType;
            }

            // invalid properties
            foreach (var invalidMaterialClassType in
                MaterialClassPropertyTypeDataGenerator.GenerateInvalidMaterialClassPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new MaterialClassType
                    {
                        ID = materialClassId,
                        MaterialClassProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidMaterialClassType;
            }
        }

        /// <summary>
        ///     Generates the valid material definition type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassType[]&gt;.</returns>
        public static IEnumerable<MaterialClassType[]> GenerateValidMaterialClassTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialClassType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialClassType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Mat.Def.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        MaterialClassProperty =
                            MaterialClassPropertyTypeDataGenerator.GenerateValidMaterialClassPropertyTypeArrays()
                                .FirstOrDefault(m => m.Length == 2),
                        MaterialDefinitionID = GenerateValidMaterialDefinitionIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        MaterialTestSpecificationID = GenerateValidMaterialTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        AssemblyClassID = GenerateValidMaterialClassIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        AssemblyType = AssemblyTypeTypeDataGenerator.GenerateValidAssemblyTypeTypes()
                            .FirstOrDefault(a => a != null),
                        AssemblyRelationship =
                            AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                                .FirstOrDefault(a => a != null)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassType&gt;.</returns>
        public static IEnumerable<MaterialClassType> GenerateValidMaterialClassTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new MaterialClassType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Mat.Def.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(h => h != null),
                    MaterialClassProperty =
                        MaterialClassPropertyTypeDataGenerator.GenerateValidMaterialClassPropertyTypeArrays()
                            .FirstOrDefault(m => m.Length == i),
                    MaterialDefinitionID = GenerateValidMaterialDefinitionIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    MaterialTestSpecificationID = GenerateValidMaterialTestSpecificationIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    AssemblyClassID = GenerateValidMaterialClassIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    AssemblyType = AssemblyTypeTypeDataGenerator.GenerateValidAssemblyTypeTypes()
                        .FirstOrDefault(a => a != null),
                    AssemblyRelationship =
                        AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                            .FirstOrDefault(a => a != null)
                };
            }

            // basic with only mandatory fields
            yield return new MaterialClassType
            {
                ID = new IdentifierType
                {
                    Value = "ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(h => h != null),
            };
        }

        /// <summary>
        ///     Generates the valid material definition identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassIDType[]&gt;.</returns>
        private static IEnumerable<MaterialClassIDType[]> GenerateValidMaterialClassIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialClassIDType
                    {
                        Value = $"MaterialClassIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material class identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassIDType[]&gt;.</returns>
        private static IEnumerable<MaterialDefinitionIDType[]> GenerateValidMaterialDefinitionIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialDefinitionIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialDefinitionIDType
                    {
                        Value = $"MaterialDefinitionIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<MaterialTestSpecificationIDType[]> GenerateValidMaterialTestSpecificationIdTypeArrays
            ()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialTestSpecificationIDType
                    {
                        Value = $"MaterialTestSpecificationIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}