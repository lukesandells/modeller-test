﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PersonnelClassPropertyTypeDataGenerator.
    /// </summary>
    public static class PersonnelClassPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid personnel property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClassPropertyType&gt;.</returns>
        public static IEnumerable<PersonnelClassPropertyType> GenerateInvalidPersonnelClassPropertyTypes()
        {
            // null ref
            yield return default(PersonnelClassPropertyType);

            // No InternalId
            yield return new PersonnelClassPropertyType();

            // invalid Values
            foreach (var invalidPersonnelClassPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new PersonnelClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PersonnelClassProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidPersonnelClassPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid personnel property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClassPropertyType[]&gt;.</returns>
        public static IEnumerable<PersonnelClassPropertyType[]> GenerateValidPersonnelClassPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PersonnelClassPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonnelClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PersonnelClassProperty.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        
                        QualificationTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid personnel property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClassPropertyType&gt;.</returns>
        public static IEnumerable<PersonnelClassPropertyType> GenerateValidPersonnelClassPropertyTypes()
        {
            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PersonnelClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"PersonnelClassProperty.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    QualificationTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    PersonnelClassProperty = GenerateValidPersonnelClassPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new PersonnelClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"PersonnelClassProperty.InternalId.{i}"
                    }
                };
            }
        }

        /// <summary>
        ///     Generates the valid test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;QualificationTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<QualificationTestSpecificationIDType[]>
            GenerateValidTestSpecificationIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new QualificationTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new QualificationTestSpecificationIDType
                    {
                        Value = $"Test Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}