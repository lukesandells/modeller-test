﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PhysicalAssetSegmentSpecificationTypeDataGenerator.
    ///     Generate data for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class PhysicalAssetSegmentSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op physical asset specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PhysicalAssetSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<PhysicalAssetSegmentSpecificationType>
            GenerateInvalidPhysicalAssetSegmentSpecificationTypes()
        {
            var physicalAssetId = new PhysicalAssetIDType
            {
                Value = "InternalId"
            };

            // null ref
            yield return default(PhysicalAssetSegmentSpecificationType);

            // invalid Quantitities
            foreach (var invalidPhysicalAssetSegmentSpecificationType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(t => t != null)
                    .Select(quantity => new PhysicalAssetSegmentSpecificationType
                    {
                        PhysicalAssetID = physicalAssetId,
                        Quantity = new[]
                        {
                            quantity
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))

            {
                yield return invalidPhysicalAssetSegmentSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidPhysicalAssetSegmentSpecificationType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new PhysicalAssetSegmentSpecificationType
                    {
                        PhysicalAssetID = physicalAssetId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidPhysicalAssetSegmentSpecificationType;
            }

            // invalid properties
            foreach (var invalidPhysicalAssetSegmentSpecificationType in
                PhysicalAssetSegmentSpecificationPropertyTypeDataGenerator
                    .GenerateInvalidPhysicalAssetSegmentSpecificationPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new PhysicalAssetSegmentSpecificationType
                    {
                        PhysicalAssetID = physicalAssetId,
                        PhysicalAssetSegmentSpecificationProperty = new[]
                        {
                            property
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidPhysicalAssetSegmentSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid op physical asset specification type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PhysicalAssetSegmentSpecificationType[]&gt;.</returns>
        public static IEnumerable<PhysicalAssetSegmentSpecificationType[]>
            GenerateValidPhysicalAssetSegmentSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PhysicalAssetSegmentSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetSegmentSpecificationType
                    {
                        PhysicalAssetClassID = null,
                        PhysicalAssetID = new PhysicalAssetIDType
                        {
                            Value = $"ID.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        PhysicalAssetUse = new PhysicalAssetUseType
                        {
                            Value = "PhysicalAssetUse"
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        GeospatialDesignation =
                            GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                                .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        PhysicalAssetSegmentSpecificationProperty =
                            PhysicalAssetSegmentSpecificationPropertyTypeDataGenerator
                                .GenerateValidPhysicalAssetSegmentSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op physical asset specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PhysicalAssetSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<PhysicalAssetSegmentSpecificationType>
            GenerateValidPhysicalAssetSegmentSpecificationTypes()
        {
            var physicalAssetUse = new PhysicalAssetUseType
            {
                Value = "PhysicalAssetUse"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PhysicalAssetSegmentSpecificationType
                {
                    PhysicalAssetClassID = null,
                    PhysicalAssetID = new PhysicalAssetIDType
                    {
                        Value = $"ID.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    PhysicalAssetUse = physicalAssetUse,
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    GeospatialDesignation =
                        GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                            .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    PhysicalAssetSegmentSpecificationProperty =
                        PhysicalAssetSegmentSpecificationPropertyTypeDataGenerator
                            .GenerateValidPhysicalAssetSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new PhysicalAssetSegmentSpecificationType
            {
                PhysicalAssetClassID = null,
                PhysicalAssetID = new PhysicalAssetIDType
                {
                    Value = "AssetID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            yield return new PhysicalAssetSegmentSpecificationType
            {
                PhysicalAssetClassID = new PhysicalAssetClassIDType
                {
                    Value = "ClassID"
                },
                PhysicalAssetID = null,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        #endregion
    }
}