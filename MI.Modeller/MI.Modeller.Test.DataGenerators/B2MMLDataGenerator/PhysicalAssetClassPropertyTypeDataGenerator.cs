﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PhysicalAssetClassPropertyTypeDataGenerator.
    /// </summary>
    public static class PhysicalAssetClassPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid physicalAsset property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClassPropertyType&gt;.</returns>
        public static IEnumerable<PhysicalAssetClassPropertyType> GenerateInvalidPhysicalAssetClassPropertyTypes()
        {
            // null ref
            yield return default(PhysicalAssetClassPropertyType);

            // No InternalId
            yield return new PhysicalAssetClassPropertyType();

            // invalid Values
            foreach (var invalidPhysicalAssetClassPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new PhysicalAssetClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PhysicalAssetClassProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidPhysicalAssetClassPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid physicalAsset property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClassPropertyType[]&gt;.</returns>
        public static IEnumerable<PhysicalAssetClassPropertyType[]> GenerateValidPhysicalAssetClassPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PhysicalAssetClassPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PhysicalAssetClassProperty.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1), 

                        PhysicalAssetCapabilityTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid physicalAsset property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClassPropertyType&gt;.</returns>
        public static IEnumerable<PhysicalAssetClassPropertyType> GenerateValidPhysicalAssetClassPropertyTypes()
        {
            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PhysicalAssetClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"PhysicalAssetClassProperty.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    PhysicalAssetCapabilityTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    PhysicalAssetClassProperty = GenerateValidPhysicalAssetClassPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new PhysicalAssetClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"PhysicalAssetClassProperty.InternalId.{i}"
                    }
                };
            }
        }

        /// <summary>
        ///     Generates the valid test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;QualificationTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<PhysicalAssetCapabilityTestSpecificationIDType[]>
            GenerateValidTestSpecificationIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PhysicalAssetCapabilityTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetCapabilityTestSpecificationIDType
                    {
                        Value = $"Test Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}