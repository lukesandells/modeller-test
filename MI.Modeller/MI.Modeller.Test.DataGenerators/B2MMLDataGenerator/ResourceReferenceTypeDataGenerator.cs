﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ResourceReferenceTypeDataGenerator.
    /// </summary>
    public static class ResourceReferenceTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid resource reference types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReferenceType&gt;.</returns>
        public static IEnumerable<ResourceReferenceType> GenerateInvalidResourceReferenceTypes()
        {
            // null ref
            yield return default(ResourceReferenceType);

            // invalid Values
            foreach (var invalidResourceReferenceType in
                ResourcePropertyTypeDataGenerator.GenerateInvalidResourcePropertyTypes()
                    .Where(o => o != default(ResourcePropertyType))
                    .Select(property => new ResourceReferenceType
                    {
                        ID = new IdentifierType
                        {
                            Value = "ResourceReference.InternalId"
                        },
                        ResourceProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidResourceReferenceType;
            }
        }

        /// <summary>
        ///     Generates the valid resource reference type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReferenceType[]&gt;.</returns>
        public static IEnumerable<ResourceReferenceType[]> GenerateValidResourceReferenceTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ResourceReferenceType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ResourceReferenceType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"ResourceReference.InternalId.{j}"
                        },
                        ResourceID = new ResourceIDType
                        {
                            Value = "ResourceId"
                        },
                        ResourceProperty = ResourcePropertyTypeDataGenerator.GenerateValidResourcePropertyTypeArrays()
                            .FirstOrDefault(p => p.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid resource reference types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReferenceType&gt;.</returns>
        public static IEnumerable<ResourceReferenceType> GenerateValidResourceReferenceTypes()
        {
            var id = new IdentifierType
            {
                Value = "ResourceReference.ID"
            };
            var resourceId = new ResourceIDType
            {
                Value = "ResourceId"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourceReferenceType
                {
                    ID = id,
                    ResourceID = resourceId,
                    ResourceProperty = ResourcePropertyTypeDataGenerator.GenerateValidResourcePropertyTypeArrays()
                        .FirstOrDefault(p => p.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new ResourceReferenceType();
        }

        #endregion
    }
}