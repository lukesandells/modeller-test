﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpPhysicalAssetSpecificationPropertyTypeDataGenerator.
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class OpPhysicalAssetSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op physicalAsset specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpPhysicalAssetSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpPhysicalAssetSpecificationPropertyType>
            GenerateInvalidOpPhysicalAssetSpecificationPropertyTypes()
        {
            // null ref
            yield return default(OpPhysicalAssetSpecificationPropertyType);

            // invalid Values
            foreach (var invalidOpPhysicalAssetSpecificationPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new OpPhysicalAssetSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PhysicalAssetSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidOpPhysicalAssetSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidOpPhysicalAssetSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != default(QuantityValueType))
                    .Select(quantity => new OpPhysicalAssetSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PhysicalAssetSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidOpPhysicalAssetSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid op physicalAsset specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OpPhysicalAssetSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<OpPhysicalAssetSpecificationPropertyType[]>
            GenerateValidOpPhysicalAssetSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpPhysicalAssetSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpPhysicalAssetSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PhysicalAssetSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op physicalAsset specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpPhysicalAssetSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpPhysicalAssetSpecificationPropertyType>
            GenerateValidOpPhysicalAssetSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "PhysicalAssetSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpPhysicalAssetSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    PhysicalAssetSpecificationProperty = GenerateValidOpPhysicalAssetSpecificationPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new OpPhysicalAssetSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}