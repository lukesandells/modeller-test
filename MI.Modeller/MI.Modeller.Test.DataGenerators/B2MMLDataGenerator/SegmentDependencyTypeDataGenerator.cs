﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class to generate SegmentDependencyType data
    /// </summary>
    public class SegmentDependencyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid SegmentDependencyType.
        /// </summary>
        /// <returns>IEnumerable&lt;SegmentDependencyType&gt;.</returns>
        public static IEnumerable<SegmentDependencyType> GenerateInvalidSegmentDependencyTypes()
        {
            var segmentDependecyId = new IdentifierType
            {
                Value = "InternalId"
            };

            // null no allowed
            yield return default(SegmentDependencyType);
            //invalid dependancy types
            foreach (var invalidSegementDependencyType in
                DependencyTypeDataGenerator.GenerateInvalidDependencyTypes()
                    .Where(t => t != null)
                    .Select(t => new SegmentDependencyType
                    {
                        ID = segmentDependecyId,
                        Dependency = t
                    }))
            {
                yield return invalidSegementDependencyType;
            }
            //invalid timing factors (values)
            foreach (var invalidSegementDependencyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(v => v != null)
                    .Select(v => new SegmentDependencyType
                    {
                        ID = segmentDependecyId,
                        TimingFactor = new[]
                        {
                            v
                        }
                    }))
            {
                yield return invalidSegementDependencyType;
            }

            // no segmentID or process Segment ID
            yield return new SegmentDependencyType
            {
                ID = segmentDependecyId
            };

            // segment ID AND process Segment ID populated
            yield return new SegmentDependencyType
            {
                ID = segmentDependecyId,
                Items = GenerateInvalidItems()
            };
        }

        public static IEnumerable<SegmentDependencyType[]> GenerateValidSegmentDependencyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new SegmentDependencyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new SegmentDependencyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == i),
                        TimingFactor = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == i),
                        Dependency = DependencyTypeDataGenerator.GenerateValidDependencyTypes()
                            .First(),
                        Items = GenerateValidItem(i)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid SegmentDependencyType.
        /// </summary>
        /// <returns>IEnumerable&lt;SegmentDependencyType&gt;.</returns>
        public static IEnumerable<SegmentDependencyType> GenerateValidSegmentDependencyTypes()
        {
            var segmentDependecyId = new IdentifierType
            {
                Value = "InternalId"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3; i >= 0; i--)
            {
                yield return new SegmentDependencyType
                {
                    ID = segmentDependecyId,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    TimingFactor = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    Dependency = DependencyTypeDataGenerator.GenerateValidDependencyTypes()
                        .First(),
                    Items = GenerateValidItem(i)
                };
            }
        }

        private static IdentifierType[] GenerateInvalidItems()
        {
            return new[]
            {
                new IdentifierType
                {
                    Value = "identifierId"
                },
                new ProcessSegmentIDType
                {
                    Value = "processSegmentId"
                }
            };
        }

        private static IdentifierType[] GenerateValidItem(int i)
        {
            // Three options, ProcessSegment, Identifier type
            switch (i)
            {
                case 0:
                case 1:
                    return new[]
                    {
                        new IdentifierType
                        {
                            Value = "identifierId"
                        }
                    };
                case 2:
                case 3:
                    return new IdentifierType[]
                    {
                        new ProcessSegmentIDType
                        {
                            Value = "processSegmentId"
                        }
                    };

                default:
                    return null;
            }
        }

        #endregion
    }
}