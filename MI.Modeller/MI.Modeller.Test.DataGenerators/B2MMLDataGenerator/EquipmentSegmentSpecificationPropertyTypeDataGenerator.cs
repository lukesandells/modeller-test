﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentSegmentSpecificationPropertyTypeDataGenerator.
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class EquipmentSegmentSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op equipment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<EquipmentSegmentSpecificationPropertyType>
            GenerateInvalidEquipmentSegmentSpecificationPropertyTypes()
        {
            // null ref
            yield return default(EquipmentSegmentSpecificationPropertyType);

            // invalid Values
            foreach (var invalidEquipmentSegmentSpecificationPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new EquipmentSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "EquipmentSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidEquipmentSegmentSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidEquipmentSegmentSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != default(QuantityValueType))
                    .Select(quantity => new EquipmentSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "EquipmentSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidEquipmentSegmentSpecificationPropertyType;
            }

            // class and definition population
        }

        /// <summary>
        ///     Generates the valid op equipment specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentSegmentSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<EquipmentSegmentSpecificationPropertyType[]>
            GenerateValidEquipmentSegmentSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentSegmentSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"EquipmentSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op equipment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<EquipmentSegmentSpecificationPropertyType>
            GenerateValidEquipmentSegmentSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "EquipmentSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentSegmentSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    EquipmentSegmentSpecificationProperty =
                        GenerateValidEquipmentSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic 
            yield return new EquipmentSegmentSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}