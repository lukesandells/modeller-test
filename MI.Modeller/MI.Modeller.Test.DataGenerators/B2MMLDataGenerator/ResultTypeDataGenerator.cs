﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ResultTypeDataGenerator.
    /// </summary>
    public static class ResultTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid result types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResultType&gt;.</returns>
        public static IEnumerable<ResultType> GenerateInvalidResultTypes()
        {
            // return null
            yield return default(ResultType);

            // invalid units o measure
            foreach (var resultType in UnitOfMeasureTypeDataGenerator.GenerateInvalidUnitOfMeasureTypes()
                .Where(t => t != null)
                .Select(unitOfMeasure => new ResultType
                {
                    UnitOfMeasure = unitOfMeasure
                }))
            {
                yield return resultType;
            }

            foreach (var resultType in DataTypeTypeDataGenerator.GenerateInvalidDataTypeTypes()
                .Where(t => t != null)
                .Select(dataType => new ResultType
                {
                    DataType = dataType
                }))
            {
                yield return resultType;
            }
        }

        /// <summary>
        ///     Generates the valid result type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ResultType[]&gt;.</returns>
        public static IEnumerable<ResultType[]> GenerateValidResultTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ResultType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ResultType
                    {
                        Key = new IdentifierType
                        {
                            Value = $"ResultType.Key.{j}"
                        }
                    };
                }
                yield return vals;
            }

            yield return null;
        }

        /// <summary>
        ///     Generates the valid result types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResultType&gt;.</returns>
        public static IEnumerable<ResultType> GenerateValidResultTypes()
        {
            // all values populated
            yield return new ResultType
            {
                Key = new IdentifierType
                {
                    Value = "Result.Key.Value"
                },
                UnitOfMeasure = UnitOfMeasureTypeDataGenerator.GenerateValidUnitOfMeasureTypes()
                    .First(),
                DataType = DataTypeTypeDataGenerator.GenerateValidDataTypeTypes()
                    .First(),
                ValueString = new ValueStringType
                {
                    Value = "Result.ValueString.Value"
                }
            };

            // attributes created but not populated (If this is allowed)
            yield return new ResultType
            {
                Key = new IdentifierType(),
                ValueString = new ValueStringType()
            };
            // basic object
            yield return new ResultType();
        }

        #endregion
    }
}