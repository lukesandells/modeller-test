﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpPersonnelSpecificationTypeDataGenerator.
    ///     Generate data for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class OpPersonnelSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op personnel specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpPersonnelSpecificationType&gt;.</returns>
        public static IEnumerable<OpPersonnelSpecificationType> GenerateInvalidOpPersonnelSpecificationTypes()
        {
            PersonIDType[] personId =
            {
                new PersonIDType
                {
                    Value = "InternalId"
                }
            };

            // null ref
            yield return default(OpPersonnelSpecificationType);

            // class and definition
            yield return new OpPersonnelSpecificationType
            {
                PersonnelClassID = GenerateValidPersonnelClassIdTypeArrays()
                    .FirstOrDefault(p => p?.Length == 2),
                PersonID = personId,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            // invalid Quantitities
            foreach (var invalidOpPersonnelSpecificationType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(t => t != null)
                    .Select(quantity => new OpPersonnelSpecificationType
                    {
                        PersonID = personId,
                        Quantity = new[]
                        {
                            quantity
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))

            {
                yield return invalidOpPersonnelSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidOpPersonnelSpecificationType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new OpPersonnelSpecificationType
                    {
                        PersonID = personId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidOpPersonnelSpecificationType;
            }

            // invalid properties
            foreach (var invalidOpPersonnelSpecificationType in
                OpPersonnelSpecificationPropertyTypeDataGenerator.GenerateInvalidOpPersonnelSpecificationPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new OpPersonnelSpecificationType
                    {
                        PersonID = personId,
                        PersonnelSpecificationProperty = new[]
                        {
                            property
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidOpPersonnelSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid op personnel specification type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpPersonnelSpecificationType[]&gt;.</returns>
        public static IEnumerable<OpPersonnelSpecificationType[]> GenerateValidOpPersonnelSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpPersonnelSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpPersonnelSpecificationType
                    {
                        PersonnelClassID = GenerateValidPersonnelClassIdTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        PersonID = null,
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        PersonnelUse = new PersonnelUseType
                        {
                            Value = "PersonnelUse"
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        SpatialLocation = 
                            GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                                .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        PersonnelSpecificationProperty =
                            OpPersonnelSpecificationPropertyTypeDataGenerator
                                .GenerateValidOpPersonnelSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op personnel specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpPersonnelSpecificationType&gt;.</returns>
        public static IEnumerable<OpPersonnelSpecificationType> GenerateValidOpPersonnelSpecificationTypes()
        {
            PersonIDType[] personId =
            {
                new PersonIDType
                {
                    Value = "InternalId"
                }
            };

            var personnelUse = new PersonnelUseType
            {
                Value = "PersonnelUse"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpPersonnelSpecificationType
                {
                    PersonnelClassID = null,
                    PersonID = GenerateValidPersonIdTypeArrays()
                        .FirstOrDefault(p => p?.Length == 2),
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    PersonnelUse = personnelUse,
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    SpatialLocation = 
                        GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                            .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    PersonnelSpecificationProperty =
                        OpPersonnelSpecificationPropertyTypeDataGenerator
                            .GenerateValidOpPersonnelSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new OpPersonnelSpecificationType
            {
                PersonID = personId,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            // basic with only mandatory fields
            yield return new OpPersonnelSpecificationType
            {
                PersonnelClassID = GenerateValidPersonnelClassIdTypeArrays()
                    .FirstOrDefault(p => p?.Length == 2),
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        /// <summary>
        ///     Generates the valid person identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PersonIDType[]&gt;.</returns>
        private static IEnumerable<PersonIDType[]> GenerateValidPersonIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new PersonIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonIDType
                    {
                        Value = $"PersonID.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid personnel class identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PersonnelClassIDType[]&gt;.</returns>
        private static IEnumerable<PersonnelClassIDType[]> GenerateValidPersonnelClassIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            yield return null;
            for (var i = 0; i < 3; i++)
            {
                var vals = new PersonnelClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonnelClassIDType
                    {
                        Value = $"ClassID.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}