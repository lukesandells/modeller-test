﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ResourceReferenceTypeTypeDataGenerator.
    /// </summary>
    public static class ResourceReferenceTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid resource reference type types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReferenceTypeType&gt;.</returns>
        public static IEnumerable<ResourceReferenceTypeType> GenerateInvalidResourceReferenceTypeTypes()
        {
            // default not allowed
            yield return default(ResourceReferenceTypeType);
            // datatype must have a type
            yield return new ResourceReferenceTypeType();
            // invalid type
            yield return new ResourceReferenceTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid resource reference type types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReferenceTypeType&gt;.</returns>
        public static IEnumerable<ResourceReferenceTypeType> GenerateValidResourceReferenceTypeTypes()
        {
            return from Domain.Client.ResourceReferenceType value in Enum.GetValues(typeof(Domain.Client.ResourceReferenceType))
                select new ResourceReferenceTypeType
				{
                    Value = value.ToString(),
                    OtherValue = value == Domain.Client.ResourceReferenceType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}