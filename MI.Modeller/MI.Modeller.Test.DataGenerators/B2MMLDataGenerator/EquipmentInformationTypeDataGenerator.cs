﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentInformationTypeDataGenerator.
    /// </summary>
    public static class EquipmentInformationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment information types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentInformationType&gt;.</returns>
        public static IEnumerable<EquipmentInformationType> GenerateValidEquipmentInformationTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentInformationType
                {
                    ID = new IdentifierType
                    {
                        Value = $"EquipmentInformationType.InternalId.{i}"
                    },
                    PublishedDate = new PublishedDateType
                    {
                        Value = DateTime.Now
                    },
                    Equipment = EquipmentTypeDataGenerator.GenerateValidEquipmentTypeArrays()
                        .FirstOrDefault(e => e.Length == i),
                    EquipmentClass = EquipmentClassTypeDataGenerator.GenerateValidEquipmentClassTypeArrays()
                        .FirstOrDefault(e => e.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new EquipmentInformationType
            {
                ID = new IdentifierType
                {
                    Value = "Equipment.ID"
                }
            };
        }

        #endregion
    }
}