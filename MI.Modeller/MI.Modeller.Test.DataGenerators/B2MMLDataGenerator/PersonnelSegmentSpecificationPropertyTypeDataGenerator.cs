﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PersonnelSegmentSpecificationPropertyTypeDataGenerator.
    /// </summary>
    public static class PersonnelSegmentSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid personnel segment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<PersonnelSegmentSpecificationPropertyType>
            GenerateInvalidPersonnelSegmentSpecificationPropertyTypes()
        {
            // null ref
            yield return default(PersonnelSegmentSpecificationPropertyType);

            // null id
            yield return new PersonnelSegmentSpecificationPropertyType();
            // invalid Values
            foreach (var invalidPersonnelSegmentSpecificationPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new PersonnelSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PersonnelSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidPersonnelSegmentSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidPersonnelSegmentSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != default(QuantityValueType))
                    .Select(quantity => new PersonnelSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PersonnelSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidPersonnelSegmentSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid personnel segment specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelSegmentSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<PersonnelSegmentSpecificationPropertyType[]>
            GenerateValidPersonnelSegmentSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PersonnelSegmentSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonnelSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PersonnelSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid personnel segment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<PersonnelSegmentSpecificationPropertyType>
            GenerateValidPersonnelSegmentSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "PersonnelSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PersonnelSegmentSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    PersonnelSegmentSpecificationProperty =
                        GenerateValidPersonnelSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new PersonnelSegmentSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}