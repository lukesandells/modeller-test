﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationConnectionTypeDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationConnectionTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid workflow specification connection types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationConnectionType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationConnectionType>
            GenerateInvalidWorkflowSpecificationConnectionTypes()
        {
            // null ref
            yield return default(WorkflowSpecificationConnectionType);
            // no ID
            yield return new WorkflowSpecificationConnectionType();
            // invalid Properties
            foreach (var invalidWorkflowSpecificationConnectionType in
                WorkflowSpecificationPropertyTypeDataGenerator.GenerateInvalidWorkflowSpecificationPropertyTypes()
                    .Where(value => value != default(WorkflowSpecificationPropertyType))
                    .Select(value => new WorkflowSpecificationConnectionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkflowSpecificationConnection.InternalId"
                        },
                        Property = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidWorkflowSpecificationConnectionType;
            }
        }

        /// <summary>
        ///     Generates the valid workflow specification connection type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationConnectionType[]&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationConnectionType[]>
            GenerateValidWorkflowSpecificationConnectionTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new WorkflowSpecificationConnectionType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new WorkflowSpecificationConnectionType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"WorkflowSpecificationConnection.InternalId.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        ConnectionType = new IdentifierType
                        {
                            Value = "PropertyType"
                        },
                        FromNodeID = GenerateNodeIdArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        ToNodeID = GenerateNodeIdArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        Property =
                            WorkflowSpecificationPropertyTypeDataGenerator
                                .GenerateValidWorkflowSpecificationPropertyTypeArrays()
                                .FirstOrDefault(e => e.Length == 2)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid workflow specification connection types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationConnectionType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationConnectionType> GenerateValidWorkflowSpecificationConnectionTypes
            ()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecificationConnectionType
                {
                    ID = new IdentifierType
                    {
                        Value = $"WorkflowSpecificationConnection.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    ConnectionType = new IdentifierType
                    {
                        Value = "PropertyType"
                    },
                    FromNodeID = GenerateNodeIdArrays()
                        .FirstOrDefault(d => d.Length == i),
                    ToNodeID = GenerateNodeIdArrays()
                        .FirstOrDefault(d => d.Length == i),
                    Property =
                        WorkflowSpecificationPropertyTypeDataGenerator
                            .GenerateValidWorkflowSpecificationPropertyTypeArrays()
                            .FirstOrDefault(e => e.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new WorkflowSpecificationConnectionType
            {
                ID = new IdentifierType
                {
                    Value = "WorkflowSpecificationConnection.InternalId"
                }
            };
        }

        /// <summary>
        ///     Generates the node identifier arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;DescriptionType[]&gt;.</returns>
        private static IEnumerable<DescriptionType[]> GenerateNodeIdArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new DescriptionType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new DescriptionType
                    {
                        Value = $"Node.InternalId.{j}"
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        #endregion
    }
}