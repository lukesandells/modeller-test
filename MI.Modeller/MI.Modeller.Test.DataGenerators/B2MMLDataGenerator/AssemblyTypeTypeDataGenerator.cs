﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class AssemblyTypeTypeDataGenerator.
    ///     Generates AssemblyTypeDtpe objects for testing
    /// </summary>
    public static class AssemblyTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid data type types.
        /// </summary>
        /// <returns>IEnumerable&lt;AssemblyTypeType&gt;.</returns>
        public static IEnumerable<AssemblyTypeType> GenerateInvalidAssemblyTypeTypes()
        {
            // default not allowed
            yield return default(AssemblyTypeType);
            // datatype must have a type
            yield return new AssemblyTypeType();
            // invalid type
            yield return new AssemblyTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid data type types.
        /// </summary>
        /// <returns>IEnumerable&lt;AssemblyTypeType&gt;.</returns>
        public static IEnumerable<AssemblyTypeType> GenerateValidAssemblyTypeTypes()
        {
            return from AssemblyType value in Enum.GetValues(typeof(AssemblyType))
                select new AssemblyTypeType
                {
                    Value = value.ToString(),
                    OtherValue = value == AssemblyType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}