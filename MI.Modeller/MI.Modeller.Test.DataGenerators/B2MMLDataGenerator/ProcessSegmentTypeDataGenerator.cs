﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ProcessSegmentTypeDataGenerator.
    /// </summary>
    public static class ProcessSegmentTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid process segment types.
        /// </summary>
        /// <returns>IEnumerable&lt;ProcessSegmentType&gt;.</returns>
        public static IEnumerable<ProcessSegmentType> GenerateInvalidProcessSegmentTypes()
        {
            // null ref
            yield return default(ProcessSegmentType);

            // null id
            yield return new ProcessSegmentType();

            // invalid OperationsTypType
            foreach (
                var invalidOperationsTypeType in OperationsTypeTypeDataGenerator.GenerateInvalidOperationsTypeTypes()
                    .Where(i => i != null)
                    .Select(operationsTypeType => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        OperationsType = operationsTypeType,
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidOperationsTypeType;
            }

            // invalid HierarchyScopes
            foreach (
                var invalidProcessSegmentType in HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(i => i != null)
                    .Select(hierarchyScope => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidProcessSegmentType;
            }

            // invalid Parameters
            foreach (var invalidProcessSegmentType in ParameterTypeDataGenerator.GenerateInvalidParameterTypes()
                .Where(i => i != null)
                .Select(parameter => new ProcessSegmentType
                {
                    ID = new IdentifierType
                    {
                        Value = "OperationsSegment.InternalId"
                    },
                    Parameter = new[]
                    {
                        parameter
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                }))
            {
                yield return invalidProcessSegmentType;
            }

            // invalid Personnel Specifications
            foreach (var invalidProcessSegmentType in
                PersonnelSegmentSpecificationTypeDataGenerator.GenerateInvalidPersonnelSegmentSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        PersonnelSegmentSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidProcessSegmentType;
            }

            // invalid Equipment Specifications
            foreach (var invalidProcessSegmentType in
                EquipmentSegmentSpecificationTypeDataGenerator.GenerateInvalidEquipmentSegmentSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        EquipmentSegmentSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidProcessSegmentType;
            }

            // invalid PhysicalAsset Specifications
            foreach (var invalidProcessSegmentType in
                PhysicalAssetSegmentSpecificationTypeDataGenerator.GenerateInvalidPhysicalAssetSegmentSpecificationTypes
                    ()
                    .Where(i => i != null)
                    .Select(val => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        PhysicalAssetSegmentSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidProcessSegmentType;
            }

            // invalid Material Specifications
            foreach (var invalidProcessSegmentType in
                MaterialSegmentSpecificationTypeDataGenerator.GenerateInvalidMaterialSegmentSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        MaterialSegmentSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidProcessSegmentType;
            }

            // invalid Segment Dependency
            foreach (var invalidProcessSegmentType in
                SegmentDependencyTypeDataGenerator.GenerateInvalidSegmentDependencyTypes()
                    .Where(i => i != null)
                    .Select(val => new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        SegmentDependency = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidProcessSegmentType;
            }
        }

        /// <summary>
        ///     Generates the valid process segment type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ProcessSegmentType[]&gt;.</returns>
        public static IEnumerable<ProcessSegmentType[]> GenerateValidProcessSegmentTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ProcessSegmentType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ProcessSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"OperationsSegment.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        OperationsType = OperationsTypeTypeDataGenerator.GenerateValidOperationsTypeTypes()
                            .FirstOrDefault(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        PublishedDate = new PublishedDateType
                        {
                            Value = DateTime.Today
                        },
                        Duration = new TimeSpan(1,
                            11,
                            53,
                            25).ToString(),
                        PersonnelSegmentSpecification =
                            PersonnelSegmentSpecificationTypeDataGenerator
                                .GenerateValidPersonnelSegmentSpecificationTypeArrays()
                                .FirstOrDefault(p => p.Length == 2),
                        EquipmentSegmentSpecification =
                            EquipmentSegmentSpecificationTypeDataGenerator
                                .GenerateValidEquipmentSegmentSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        PhysicalAssetSegmentSpecification =
                            PhysicalAssetSegmentSpecificationTypeDataGenerator
                                .GenerateValidPhysicalAssetSegmentSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        MaterialSegmentSpecification =
                            MaterialSegmentSpecificationTypeDataGenerator
                                .GenerateValidMaterialSegmentSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        Parameter = ParameterTypeDataGenerator.GenerateValidParameterTypeArrays()
                            .FirstOrDefault(p => p.Length == 2),
                        SegmentDependency =
                            SegmentDependencyTypeDataGenerator.GenerateValidSegmentDependencyTypeArrays()
                                .FirstOrDefault(e => e.Length == 2)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid process segment types.
        /// </summary>
        /// <returns>IEnumerable&lt;ProcessSegmentType&gt;.</returns>
        public static IEnumerable<ProcessSegmentType> GenerateValidProcessSegmentTypes()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ProcessSegmentType
                {
                    ID = new IdentifierType
                    {
                        Value = $"OperationsSegment.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == 2),
                    OperationsType = OperationsTypeTypeDataGenerator.GenerateValidOperationsTypeTypes()
                        .FirstOrDefault(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    PublishedDate = new PublishedDateType
                    {
                        Value = DateTime.Today
                    },
                    Duration = new TimeSpan(1,
                        11,
                        53,
                        25).ToString(),
                    PersonnelSegmentSpecification =
                        PersonnelSegmentSpecificationTypeDataGenerator
                            .GenerateValidPersonnelSegmentSpecificationTypeArrays()
                            .FirstOrDefault(p => p.Length == i),
                    EquipmentSegmentSpecification =
                        EquipmentSegmentSpecificationTypeDataGenerator
                            .GenerateValidEquipmentSegmentSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    PhysicalAssetSegmentSpecification =
                        PhysicalAssetSegmentSpecificationTypeDataGenerator
                            .GenerateValidPhysicalAssetSegmentSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == 2),
                    MaterialSegmentSpecification =
                        MaterialSegmentSpecificationTypeDataGenerator
                            .GenerateValidMaterialSegmentSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    Parameter = ParameterTypeDataGenerator.GenerateValidParameterTypeArrays()
                        .FirstOrDefault(p => p.Length == i),
                    SegmentDependency = SegmentDependencyTypeDataGenerator.GenerateValidSegmentDependencyTypeArrays()
                        .FirstOrDefault(e => e.Length == i),
                    ProcessSegment = GenerateValidProcessSegmentTypeArrays()
                        .FirstOrDefault(e => e.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new ProcessSegmentType
            {
                ID = new IdentifierType
                {
                    Value = "OperationsSegment.InternalId"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        #endregion
    }
}