﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentTypeDataGenerator.
    /// </summary>
    public static class EquipmentTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentType&gt;.</returns>
        public static IEnumerable<EquipmentType> GenerateInvalidEquipmentTypes()
        {
            var equipmentId = new IdentifierType
            {
                Value = "Equipment.InternalId"
            };

            // null ref
            yield return default(EquipmentType);

            // no ID
            yield return new EquipmentType();

            // invalid hierarchy scopes
            foreach (var invalidEquipmentType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new EquipmentType
                    {
                        ID = equipmentId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidEquipmentType;
            }

            // invalid equipment Level
            foreach (var invalidEquipmentType in
                EquipmentElementLevelTypeDataGenerator.GenerateInvalidEquipmentElementLevelTypes()
                    .Where(q => q != null)
                    .Select(equipmentLevel => new EquipmentType
                    {
                        ID = equipmentId,
                        EquipmentLevel = equipmentLevel
                    }))
            {
                yield return invalidEquipmentType;
            }

            // invalid properties
            foreach (var invalidEquipmentType in
                EquipmentPropertyTypeDataGenerator.GenerateInvalidEquipmentPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new EquipmentType
                    {
                        ID = equipmentId,
                        EquipmentProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidEquipmentType;
            }
        }

        /// <summary>
        ///     Generates the valid equipment type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentType[]&gt;.</returns>
        public static IEnumerable<EquipmentType[]> GenerateValidEquipmentTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Equipment.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        EquipmentLevel =
                            EquipmentElementLevelTypeDataGenerator.GenerateValidEquipmentElementLevelTypes()
                                .FirstOrDefault(l => l != null),
                        EquipmentProperty =
                            EquipmentPropertyTypeDataGenerator.GenerateValidEquipmentPropertyTypeArrays()
                                .FirstOrDefault(m => m.Length == 2),
                        EquipmentClassID = GenerateValidEquipmentClassIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        EquipmentCapabilityTestSpecificationID = GenerateValidEquipmentTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid equipment types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentType&gt;.</returns>
        public static IEnumerable<EquipmentType> GenerateValidEquipmentTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Equipment.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    EquipmentLevel = EquipmentElementLevelTypeDataGenerator.GenerateValidEquipmentElementLevelTypes()
                        .FirstOrDefault(h => h != null),
                    EquipmentProperty = EquipmentPropertyTypeDataGenerator.GenerateValidEquipmentPropertyTypeArrays()
                        .FirstOrDefault(m => m.Length == i),
                    EquipmentClassID = GenerateValidEquipmentClassIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    EquipmentCapabilityTestSpecificationID = GenerateValidEquipmentTestSpecificationIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    Equipment = GenerateValidEquipmentTypeArrays()
                        .FirstOrDefault(e => e.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new EquipmentType
            {
                ID = new IdentifierType
                {
                    Value = "Equipment.ID.Main"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .FirstOrDefault(h => h != null)
            };
        }

        /// <summary>
        ///     Generates the valid equipment class identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentClassIDType[]> GenerateValidEquipmentClassIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new EquipmentClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentClassIDType
                    {
                        Value = $"EquipmentClassIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        
        /// <summary>
        ///     Generates the valid equipment test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentCapabilityTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentCapabilityTestSpecificationIDType[]>
            GenerateValidEquipmentTestSpecificationIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new EquipmentCapabilityTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentCapabilityTestSpecificationIDType
                    {
                        Value = $"EquipmentCapabilityTestSpecificationIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}