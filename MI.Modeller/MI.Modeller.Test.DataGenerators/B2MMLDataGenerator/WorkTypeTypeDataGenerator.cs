using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkTypeDataGenerator.
    ///     Generate Data for Test cases
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    public static class WorkTypeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment level types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkTypeType&gt;.</returns>
        public static IEnumerable<WorkTypeType> GenerateInvalidWorkTypeTypes()
        {
            // null no allowed
            yield return default(WorkTypeType);
            // type must value a type 
            yield return new WorkTypeType();
            // invalid values
            yield return new WorkTypeType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid equipment level types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkTypeType&gt;.</returns>
        public static IEnumerable<WorkTypeType> GenerateValidWorkTypeTypes()
        {
            return from WorkType value in Enum.GetValues(typeof(WorkType))
                select new WorkTypeType
                {
                    Value = value.ToString(),
                    OtherValue = value == WorkType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}