﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationPropertyTypeDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid workflow specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationPropertyType> GenerateInvalidWorkflowSpecificationPropertyTypes()
        {
            // null ref
            yield return default(WorkflowSpecificationPropertyType);

            // null id
            yield return new WorkflowSpecificationPropertyType();

            // invalid Values
            foreach (var invalidWorkflowSpecificationPropertyType in ValueTypeDataGenerator.GenerateInvalidValueTypes()
                .Where(value => value != default(ValueType))
                .Select(value => new WorkflowSpecificationPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = "WorkflowSpecificationProperty.InternalId"
                    },
                    Value = new[]
                    {
                        value
                    }
                }))
            {
                yield return invalidWorkflowSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid workflow specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationPropertyType[]>
            GenerateValidWorkflowSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new WorkflowSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new WorkflowSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"WorkflowSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid workflow specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationPropertyType> GenerateValidWorkflowSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "WorkflowSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Property = GenerateValidWorkflowSpecificationPropertyTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                };
            }

            // basic mandatory fields only
            yield return new WorkflowSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}