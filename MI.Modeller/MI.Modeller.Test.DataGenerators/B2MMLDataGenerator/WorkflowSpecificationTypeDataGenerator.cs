﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationTypeDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid workflow specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationType> GenerateInvalidWorkflowSpecificationTypes()
        {
            // null ref
            yield return default(WorkflowSpecificationType);
            // no ID
            yield return new WorkflowSpecificationType();
            // invalid Nodes
            foreach (var invalidWorkflowSpecificationType in
                WorkflowSpecificationNodeTypeDataGenerator.GenerateInvalidWorkflowSpecificationNodeTypes()
                    .Where(value => value != null)
                    .Select(node => new WorkflowSpecificationType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkflowSpecification.InternalId"
                        },
                        Node = new[]
                        {
                            node
                        }
                    }))
            {
                yield return invalidWorkflowSpecificationType;
            }

            //invalid connections
            foreach (var invalidWorkflowSpecificationType in
                WorkflowSpecificationConnectionTypeDataGenerator.GenerateInvalidWorkflowSpecificationConnectionTypes()
                    .Where(value => value != null)
                    .Select(connection => new WorkflowSpecificationType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkflowSpecification.InternalId"
                        },
                        Connection = new[]
                        {
                            connection
                        }
                    }))
            {
                yield return invalidWorkflowSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid workflow specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationType[]&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationType[]> GenerateValidWorkflowSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new WorkflowSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new WorkflowSpecificationType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"WorkflowSpecification.InternalId.{i}.{j}"
                        },
                        Version = new VersionType
                        {
                            Value = $"Version.{i}.{j}"
                        },
                        Connection =
                            WorkflowSpecificationConnectionTypeDataGenerator
                                .GenerateValidWorkflowSpecificationConnectionTypeArrays()
                                .FirstOrDefault(c => c.Length == 2),
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        Node =
                            WorkflowSpecificationNodeTypeDataGenerator.GenerateValidWorkflowSpecificationNodeTypeArrays()
                                .FirstOrDefault(n => n.Length == 2)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid workflow specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationType> GenerateValidWorkflowSpecificationTypes()
        {
            var id = new IdentifierType
            {
                Value = "WorkflowSpecification.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecificationType
                {
                    ID = new IdentifierType
                    {
                        Value = $"WorkflowSpecification.InternalId.{i}"
                    },
                    Version = new VersionType
                    {
                        Value = $"Version.{i}"
                    },
                    Connection =
                        WorkflowSpecificationConnectionTypeDataGenerator
                            .GenerateValidWorkflowSpecificationConnectionTypeArrays()
                            .FirstOrDefault(c => c.Length == i),
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    Node = WorkflowSpecificationNodeTypeDataGenerator.GenerateValidWorkflowSpecificationNodeTypeArrays()
                        .FirstOrDefault(n => n.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new WorkflowSpecificationType
            {
                ID = id
            };
        }

        #endregion
    }
}