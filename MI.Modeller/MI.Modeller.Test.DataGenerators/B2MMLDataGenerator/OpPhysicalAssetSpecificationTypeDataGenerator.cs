﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpPhysicalAssetSpecificationTypeDataGenerator.
    ///     Generate data for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class OpPhysicalAssetSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op physical asset specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpPhysicalAssetSpecificationType&gt;.</returns>
        public static IEnumerable<OpPhysicalAssetSpecificationType> GenerateInvalidOpPhysicalAssetSpecificationTypes()
        {
            PhysicalAssetIDType[] physicalAssetId =
            {
                new PhysicalAssetIDType
                {
                    Value = "InternalId"
                }
            };

            // null ref
            yield return default(OpPhysicalAssetSpecificationType);

            // ID and claSS
            yield return new OpPhysicalAssetSpecificationType
            {
                PhysicalAssetClassID = GenerateValidPhysicalAssetClassIdTypeArrays()
                    .FirstOrDefault(p => p?.Length == 2),
                PhysicalAssetID = new[]
                {
                    new PhysicalAssetIDType
                    {
                        Value = "AssetId"
                    }
                    
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            // invalid Quantitities
            foreach (var invalidOpPhysicalAssetSpecificationType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(t => t != null)
                    .Select(quantity => new OpPhysicalAssetSpecificationType
                    {
                        PhysicalAssetID = physicalAssetId,
                        Quantity = new[]
                        {
                            quantity
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))

            {
                yield return invalidOpPhysicalAssetSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidOpPhysicalAssetSpecificationType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new OpPhysicalAssetSpecificationType
                    {
                        PhysicalAssetID = physicalAssetId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidOpPhysicalAssetSpecificationType;
            }

            // invalid properties
            foreach (var invalidOpPhysicalAssetSpecificationType in
                OpPhysicalAssetSpecificationPropertyTypeDataGenerator
                    .GenerateInvalidOpPhysicalAssetSpecificationPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new OpPhysicalAssetSpecificationType
                    {
                        PhysicalAssetID = physicalAssetId,
                        PhysicalAssetSpecificationProperty = new[]
                        {
                            property
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidOpPhysicalAssetSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid op physical asset specification type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpPhysicalAssetSpecificationType[]&gt;.</returns>
        public static IEnumerable<OpPhysicalAssetSpecificationType[]>
            GenerateValidOpPhysicalAssetSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpPhysicalAssetSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpPhysicalAssetSpecificationType
                    {
                        PhysicalAssetClassID = GenerateValidPhysicalAssetClassIdTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        PhysicalAssetID = null,
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        PhysicalAssetUse = new PhysicalAssetUseType
                        {
                            Value = "PhysicalAssetUse"
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        SpatialLocation = 
                            GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                                .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        PhysicalAssetSpecificationProperty =
                            OpPhysicalAssetSpecificationPropertyTypeDataGenerator
                                .GenerateValidOpPhysicalAssetSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op physical asset specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpPhysicalAssetSpecificationType&gt;.</returns>
        public static IEnumerable<OpPhysicalAssetSpecificationType> GenerateValidOpPhysicalAssetSpecificationTypes()
        {
            PhysicalAssetIDType[] physicalAssetId =
            {
                new PhysicalAssetIDType
                {
                    Value = "InternalId"
                }
            };

            var physicalAssetUse = new PhysicalAssetUseType
            {
                Value = "PhysicalAssetUse"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpPhysicalAssetSpecificationType
                {
                    PhysicalAssetClassID = null,
                    PhysicalAssetID = GenerateValidPhysicalAssetIdTypeArrays()
                        .FirstOrDefault(p => p?.Length == 1),
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    PhysicalAssetUse = physicalAssetUse,
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    SpatialLocation = 
                        GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                            .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    PhysicalAssetSpecificationProperty =
                        OpPhysicalAssetSpecificationPropertyTypeDataGenerator
                            .GenerateValidOpPhysicalAssetSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new OpPhysicalAssetSpecificationType
            {
                PhysicalAssetID = physicalAssetId,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            yield return new OpPhysicalAssetSpecificationType
            {
                PhysicalAssetClassID = GenerateValidPhysicalAssetClassIdTypeArrays()
                    .FirstOrDefault(p => p?.Length == 2),
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        /// <summary>
        ///     Generates the valid physical asset class identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PhysicalAssetClassIDType[]&gt;.</returns>
        private static IEnumerable<PhysicalAssetClassIDType[]> GenerateValidPhysicalAssetClassIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            yield return null;
            for (var i = 0; i < 3; i++)
            {
                var vals = new PhysicalAssetClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetClassIDType
                    {
                        Value = $"ClassID.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid physical asset identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;PhysicalAssetIDType[]&gt;.</returns>
        private static IEnumerable<PhysicalAssetIDType[]> GenerateValidPhysicalAssetIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new PhysicalAssetIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetIDType
                    {
                        Value = $"PhysicalAssetID.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}