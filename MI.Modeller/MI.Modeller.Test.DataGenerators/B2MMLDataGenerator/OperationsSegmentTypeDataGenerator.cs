﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OperationsSegmentTypeTypeDataGenerator.
    /// </summary>
    public static class OperationsSegmentTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid work master types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsSegmentType&gt;.</returns>
        public static IEnumerable<OperationsSegmentType> GenerateInvalidOperationsSegmentTypes()
        {
            // null ref
            yield return default(OperationsSegmentType);

            // null id
            yield return new OperationsSegmentType();



            // invalid OperationsTypType
            foreach (
                var invalidOperationsTypeType in OperationsTypeTypeDataGenerator.GenerateInvalidOperationsTypeTypes()
                    .Where(i => i != null)
                    .Select(operationsTypeType => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        OperationsType = operationsTypeType,
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                    }))
            {
                yield return invalidOperationsTypeType;
            }

            // invalid HierarchyScopes
            foreach (
                var invalidOperationsSegmentType in HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(i => i != null)
                    .Select(hierarchyScope => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidOperationsSegmentType;
            }

            // invalid Parameters
            foreach (var invalidOperationsSegmentType in ParameterTypeDataGenerator.GenerateInvalidParameterTypes()
                .Where(i => i != null)
                .Select(parameter => new OperationsSegmentType
                {
                    ID = new IdentifierType
                    {
                        Value = "OperationsSegment.InternalId"
                    },
                    Parameter = new[]
                    {
                        parameter
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                }))
            {
                yield return invalidOperationsSegmentType;
            }

            // invalid Personnel Specifications
            foreach (var invalidOperationsSegmentType in
                OpPersonnelSpecificationTypeDataGenerator.GenerateInvalidOpPersonnelSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        PersonnelSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                    }))
            {
                yield return invalidOperationsSegmentType;
            }

            // invalid Equipment Specifications
            foreach (var invalidOperationsSegmentType in
                OpEquipmentSpecificationTypeDataGenerator.GenerateInvalidOpEquipmentSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        EquipmentSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                    }))
            {
                yield return invalidOperationsSegmentType;
            }

            // invalid PhysicalAsset Specifications
            foreach (var invalidOperationsSegmentType in
                OpPhysicalAssetSpecificationTypeDataGenerator.GenerateInvalidOpPhysicalAssetSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        PhysicalAssetSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                    }))
            {
                yield return invalidOperationsSegmentType;
            }

            // invalid Material Specifications
            foreach (var invalidOperationsSegmentType in
                OpMaterialSpecificationTypeDataGenerator.GenerateInvalidOpMaterialSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        MaterialSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                    }))
            {
                yield return invalidOperationsSegmentType;
            }

            // invalid Segment Dependency
            foreach (var invalidOperationsSegmentType in
                SegmentDependencyTypeDataGenerator.GenerateInvalidSegmentDependencyTypes()
                    .Where(i => i != null)
                    .Select(val => new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsSegment.InternalId"
                        },
                        SegmentDependency = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null)
                    }))
            {
                yield return invalidOperationsSegmentType;
            }
        }

        /// <summary>
        ///     Generates the valid work master type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsSegmentType[]&gt;.</returns>
        public static IEnumerable<OperationsSegmentType[]> GenerateValidOperationsSegmentTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OperationsSegmentType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OperationsSegmentType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"OperationsSegment.InternalId.{i}.{j}"
                        },
                        Description = new DescriptionType
                        {
                            Value = $"Description{i}.{j}"
                        },
                        OperationsType = OperationsTypeTypeDataGenerator.GenerateValidOperationsTypeTypes()
                            .FirstOrDefault(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        Duration = new TimeSpan(1,
                            11,
                            53,
                            25).ToString(),
                        ProcessSegmentID = GenerateValidProcessSegmentIdType(),
                        Parameter = ParameterTypeDataGenerator.GenerateValidParameterTypeArrays()
                            .FirstOrDefault(p => p.Length == 2),
                        PersonnelSpecification =
                            OpPersonnelSpecificationTypeDataGenerator.GenerateValidOpPersonnelSpecificationTypeArrays()
                                .FirstOrDefault(p => p.Length == 2),
                        EquipmentSpecification =
                            OpEquipmentSpecificationTypeDataGenerator.GenerateValidOpEquipmentSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        PhysicalAssetSpecification =
                            OpPhysicalAssetSpecificationTypeDataGenerator
                                .GenerateValidOpPhysicalAssetSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        MaterialSpecification =
                            OpMaterialSpecificationTypeDataGenerator.GenerateValidOpMaterialSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        SegmentDependency =
                            SegmentDependencyTypeDataGenerator.GenerateValidSegmentDependencyTypeArrays()
                                .FirstOrDefault(e => e.Length == 2)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op personnel specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsSegmentType&gt;.</returns>
        public static IEnumerable<OperationsSegmentType> GenerateValidOperationsSegmentTypes()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OperationsSegmentType
                {
                    ID = new IdentifierType
                    {
                        Value = $"OperationsSegment.InternalId.{i}"
                    },
                    Description = new DescriptionType
                    {
                        Value = $"Description{i}"
                    },
                    OperationsType = OperationsTypeTypeDataGenerator.GenerateValidOperationsTypeTypes()
                        .FirstOrDefault(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    Duration = new TimeSpan(1,
                        11,
                        53,
                        25).ToString(),
                    ProcessSegmentID = GenerateValidProcessSegmentIdType(),
                    Parameter = ParameterTypeDataGenerator.GenerateValidParameterTypeArrays()
                        .FirstOrDefault(p => p.Length == i),
                    PersonnelSpecification =
                        OpPersonnelSpecificationTypeDataGenerator.GenerateValidOpPersonnelSpecificationTypeArrays()
                            .FirstOrDefault(p => p.Length == i),
                    EquipmentSpecification =
                        OpEquipmentSpecificationTypeDataGenerator.GenerateValidOpEquipmentSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    PhysicalAssetSpecification =
                        OpPhysicalAssetSpecificationTypeDataGenerator
                            .GenerateValidOpPhysicalAssetSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    MaterialSpecification =
                        OpMaterialSpecificationTypeDataGenerator.GenerateValidOpMaterialSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    SegmentDependency = SegmentDependencyTypeDataGenerator.GenerateValidSegmentDependencyTypeArrays()
                        .FirstOrDefault(e => e.Length == 2),
                    OperationsSegment = GenerateValidOperationsSegmentTypeArrays()
                        .FirstOrDefault(e => e.Length == i)
                };
            }

         
        }

        /// <summary>
        ///     Generates the type of the valid process segment identifier.
        /// </summary>
        /// <returns>ProcessSegmentIDType[].</returns>
        private static ProcessSegmentIDType[] GenerateValidProcessSegmentIdType()
        {
            return new[]
            {
                new ProcessSegmentIDType
                {
                    Value = "processSegmentId"
                }
            };
        }

        #endregion
    }
}