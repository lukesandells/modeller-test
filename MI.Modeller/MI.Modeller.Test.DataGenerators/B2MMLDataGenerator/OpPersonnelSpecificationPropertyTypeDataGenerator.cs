﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpPersonnelSpecificationPropertyTypeDataGenerator.
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class OpPersonnelSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op personnel specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpPersonnelSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpPersonnelSpecificationPropertyType>
            GenerateInvalidOpPersonnelSpecificationPropertyTypes()
        {
            // null ref
            yield return default(OpPersonnelSpecificationPropertyType);

            // null id
            yield return new OpPersonnelSpecificationPropertyType();
            // invalid Values
            foreach (
                var invalidOpPersonnelSpecificationPropertyType in ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new OpPersonnelSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PersonnelSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidOpPersonnelSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidOpPersonnelSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != default(QuantityValueType))
                    .Select(quantity => new OpPersonnelSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PersonnelSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidOpPersonnelSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid op personnel specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OpPersonnelSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<OpPersonnelSpecificationPropertyType[]>
            GenerateValidOpPersonnelSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpPersonnelSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpPersonnelSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PersonnelSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op personnel specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;OpPersonnelSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<OpPersonnelSpecificationPropertyType>
            GenerateValidOpPersonnelSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "PersonnelSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpPersonnelSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    PersonnelSpecificationProperty = GenerateValidOpPersonnelSpecificationPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new OpPersonnelSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}