﻿using System.Collections.Generic;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class DescriptionTypeDataGenerator.
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    internal static class DescriptionTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid description type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;DescriptionType[]&gt;.</returns>
        public static IEnumerable<DescriptionType[]> GenerateValidDescriptionTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new DescriptionType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new DescriptionType
                    {
                        Value = $"Description{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}