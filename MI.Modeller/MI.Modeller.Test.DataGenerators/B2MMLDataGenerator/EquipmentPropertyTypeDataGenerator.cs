﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentPropertyTypeDataGenerator.
    /// </summary>
    public static class EquipmentPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment property types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentPropertyType&gt;.</returns>
        public static IEnumerable<EquipmentPropertyType> GenerateInvalidEquipmentPropertyTypes()
        {
            // null ref
            yield return default(EquipmentPropertyType);

            // No InternalId
            yield return new EquipmentPropertyType();

            // invalid Values
            foreach (var invalidEquipmentPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new EquipmentPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "EquipmentProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidEquipmentPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid equipment property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentPropertyType[]&gt;.</returns>
        public static IEnumerable<EquipmentPropertyType[]> GenerateValidEquipmentPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"EquipmentProperty.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        EquipmentCapabilityTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        TestResult = TestResultTypeDataGenerator.GenerateValidTestResultTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid equipment property types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentPropertyType&gt;.</returns>
        public static IEnumerable<EquipmentPropertyType> GenerateValidEquipmentPropertyTypes()
        {
            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"EquipmentProperty.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    EquipmentCapabilityTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    TestResult = TestResultTypeDataGenerator.GenerateValidTestResultTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    EquipmentProperty = GenerateValidEquipmentPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new EquipmentPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"EquipmentProperty.InternalId.{i}"
                    }
                };
            }
        }

        /// <summary>
        ///     Generates the valid test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentCapabilityTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentCapabilityTestSpecificationIDType[]>
            GenerateValidTestSpecificationIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentCapabilityTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentCapabilityTestSpecificationIDType
                    {
                        Value = $"Test Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}