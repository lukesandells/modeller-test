﻿using System.Collections.Generic;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class GeoSpatialDesignationTypeDataGenerator.
    ///     Data Generator for tests
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    public static class GeoSpatialDesignationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid geospatial designation types.
        ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
        ///     records
        /// </summary>
        /// <returns>IEnumerable&lt;GeospatialDesignationType&gt;.</returns>
        public static IEnumerable<GeospatialDesignationType> GenerateInvalidGeospatialDesignationTypes()
        {
            yield return default(GeospatialDesignationType);
        }

        /// <summary>
        ///     Generates the valid geospatial designation types.
        ///     null properties (where allowe)
        ///     empty properties
        ///     populated properties
        /// </summary>
        /// <returns>IEnumerable&lt;GeospatialDesignationType&gt;.</returns>
        public static IEnumerable<GeospatialDesignationType> GenerateValidGeospatialDesignationTypes()
        {
            // with populated properties
            yield return new GeospatialDesignationType
            {
                SRID = new IdentifierType
                {
                    Value = "SRID.Value"
                },
                SRIDAuthority = new IdentifierType
                {
                    Value = "sridAuthority.Value"
                },
                Format = new DescriptionType
                {
                    Value = "format.Value"
                },
                Value = new ValueStringType
                {
                    Value = "value.Value"
                }
            };
            // with empty properties
            yield return new GeospatialDesignationType
            {
                SRID = new IdentifierType(),
                SRIDAuthority = new IdentifierType(),
                Format = new DescriptionType(),
                Value = new ValueStringType()
            };

            // send with null properties
            yield return new GeospatialDesignationType();
        }

        #endregion
    }
}