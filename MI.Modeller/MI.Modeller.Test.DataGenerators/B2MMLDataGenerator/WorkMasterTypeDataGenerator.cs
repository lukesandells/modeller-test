﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkMasterTypeTypeDataGenerator.
    /// </summary>
    public static class WorkMasterTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid work master types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkMasterType&gt;.</returns>
        public static IEnumerable<WorkMasterType> GenerateInvalidWorkMasterTypes()
        {
            // null ref
            yield return default(WorkMasterType);

            // null id
            yield return new WorkMasterType();

            // invalid HierarchyScopes
            foreach (var invalidWorkMasterType in HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                .Where(i => i != null)
                .Select(hierarchyScope => new WorkMasterType
                {
                    ID = new IdentifierType
                    {
                        Value = "WorkMaster.InternalId"
                    },
                    HierarchyScope = hierarchyScope
                }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid WorkTypeTypes
            foreach (var invalidWorkMasterType in WorkTypeTypeDataGenerator.GenerateInvalidWorkTypeTypes()
                .Where(i => i != null)
                .Select(workType => new WorkMasterType
                {
                    ID = new IdentifierType
                    {
                        Value = "WorkMaster.InternalId"
                    },
                    WorkType = workType,
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid WorkDefinitionTypeTypes
            foreach (var invalidWorkMasterType in
                WorkDefinitionTypeTypeDataGenerator.GenerateInvalidWorkDefinitionTypeTypes()
                    .Where(i => i != null)
                    .Select(workDefinitionType => new WorkMasterType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkMaster.InternalId"
                        },
                        WorkDefinitionType = workDefinitionType,
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid Parameters
            foreach (var invalidWorkMasterType in ParameterTypeDataGenerator.GenerateInvalidParameterTypes()
                .Where(i => i != null)
                .Select(parameter => new WorkMasterType
                {
                    ID = new IdentifierType
                    {
                        Value = "WorkMaster.InternalId"
                    },
                    Parameter = new[]
                    {
                        parameter
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid Personnel Specifications
            foreach (var invalidWorkMasterType in
                OpPersonnelSpecificationTypeDataGenerator.GenerateInvalidOpPersonnelSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new WorkMasterType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkMaster.InternalId"
                        },
                        PersonnelSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid Equipment Specifications
            foreach (var invalidWorkMasterType in
                OpEquipmentSpecificationTypeDataGenerator.GenerateInvalidOpEquipmentSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new WorkMasterType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkMaster.InternalId"
                        },
                        EquipmentSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid PhysicalAsset Specifications
            foreach (var invalidWorkMasterType in
                OpPhysicalAssetSpecificationTypeDataGenerator.GenerateInvalidOpPhysicalAssetSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new WorkMasterType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkMaster.InternalId"
                        },
                        PhysicalAssetSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidWorkMasterType;
            }

            // invalid Material Specifications
            foreach (var invalidWorkMasterType in
                OpMaterialSpecificationTypeDataGenerator.GenerateInvalidOpMaterialSpecificationTypes()
                    .Where(i => i != null)
                    .Select(val => new WorkMasterType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkMaster.InternalId"
                        },
                        MaterialSpecification = new[]
                        {
                            val
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidWorkMasterType;
            }
        }

        /// <summary>
        ///     Generates the valid work master type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkMasterType[]&gt;.</returns>
        public static IEnumerable<WorkMasterType[]> GenerateValidWorkMasterTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new WorkMasterType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new WorkMasterType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"WorkMaster.InternalId.{i}.{j}"
                        },
                        Version = new VersionType
                        {
                            Value = $"Version.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        WorkType = WorkTypeTypeDataGenerator.GenerateValidWorkTypeTypes()
                            .FirstOrDefault(),
                        WorkDefinitionType = WorkDefinitionTypeTypeDataGenerator.GenerateValidWorkDefinitionTypeTypes()
                            .FirstOrDefault(),
                        Duration = new TimeSpan(2,
                            12,
                            35,
                            55).ToString(),
                        PublishedDate = new PublishedDateType
                        {
                            Value = DateTime.Today
                        },
                        OperationsDefinitionID = new OperationsDefinitionIDType
                        {
                            Value = $"OperationsDefinitionID.{i}.{j}"
                        },
                        OperationsSegmentID = new OperationsSegmentIDType
                        {
                            Value = $"OperationsSegmentID.{i}.{j}"
                        },
                        Parameter = ParameterTypeDataGenerator.GenerateValidParameterTypeArrays()
                            .FirstOrDefault(p => p.Length == 2),
                        PersonnelSpecification =
                            OpPersonnelSpecificationTypeDataGenerator.GenerateValidOpPersonnelSpecificationTypeArrays()
                                .FirstOrDefault(p => p.Length == 2),
                        EquipmentSpecification =
                            OpEquipmentSpecificationTypeDataGenerator.GenerateValidOpEquipmentSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        PhysicalAssetSpecification =
                            OpPhysicalAssetSpecificationTypeDataGenerator
                                .GenerateValidOpPhysicalAssetSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        MaterialSpecification =
                            OpMaterialSpecificationTypeDataGenerator.GenerateValidOpMaterialSpecificationTypeArrays()
                                .FirstOrDefault(e => e.Length == 2),
                        WorkflowSpecificationID = GenerateValidWorkflowSpecificationIdTypeArrays()
                            .FirstOrDefault(w => w.Length == 2)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op personnel specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkMasterType&gt;.</returns>
        public static IEnumerable<WorkMasterType> GenerateValidWorkMasterTypes()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkMasterType
                {
                    ID = new IdentifierType
                    {
                        Value = $"WorkMaster.InternalId.{i}"
                    },
                    Version = new VersionType
                    {
                        Value = $"Version.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    WorkType = WorkTypeTypeDataGenerator.GenerateValidWorkTypeTypes()
                        .FirstOrDefault(),
                    WorkDefinitionType = WorkDefinitionTypeTypeDataGenerator.GenerateValidWorkDefinitionTypeTypes()
                        .FirstOrDefault(),
                    Duration = new TimeSpan(3,
                        3,
                        3,
                        3).ToString(),
                    PublishedDate = new PublishedDateType
                    {
                        Value = DateTime.Today
                    },
                    OperationsDefinitionID = new OperationsDefinitionIDType
                    {
                        Value = $"OperationsDefinitionID.{i}"
                    },
                    OperationsSegmentID = new OperationsSegmentIDType
                    {
                        Value = $"OperationsSegmentID.{i}"
                    },
                    Parameter = ParameterTypeDataGenerator.GenerateValidParameterTypeArrays()
                        .FirstOrDefault(p => p.Length == i),
                    PersonnelSpecification =
                        OpPersonnelSpecificationTypeDataGenerator.GenerateValidOpPersonnelSpecificationTypeArrays()
                            .FirstOrDefault(p => p.Length == i),
                    EquipmentSpecification =
                        OpEquipmentSpecificationTypeDataGenerator.GenerateValidOpEquipmentSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    PhysicalAssetSpecification =
                        OpPhysicalAssetSpecificationTypeDataGenerator
                            .GenerateValidOpPhysicalAssetSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    MaterialSpecification =
                        OpMaterialSpecificationTypeDataGenerator.GenerateValidOpMaterialSpecificationTypeArrays()
                            .FirstOrDefault(e => e.Length == i),
                    WorkflowSpecificationID = GenerateValidWorkflowSpecificationIdTypeArrays()
                        .FirstOrDefault(w => w.Length == 0),
                    WorkMaster = GenerateValidWorkMasterTypeArrays()
                        .FirstOrDefault(w => w.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new WorkMasterType
            {
                ID = new IdentifierType
                {
                    Value = "WorkMaster.InternalId"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        /// <summary>
        ///     Generates the valid equipment class identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassIDType[]&gt;.</returns>
        private static IEnumerable<IdentifierType[]> GenerateValidWorkflowSpecificationIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new IdentifierType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new IdentifierType
                    {
                        Value = $"WorkflowSpecification.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}