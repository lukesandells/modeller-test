﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PersonnelSegmentSpecificationTypeDataGenerator.
    /// </summary>
    public static class PersonnelSegmentSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid personnel segment specification types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<PersonnelSegmentSpecificationType> GenerateInvalidPersonnelSegmentSpecificationTypes()
        {
            var personId = new PersonIDType
            {
                Value = "InternalId"
            };

            // null ref
            yield return default(PersonnelSegmentSpecificationType);

            // invalid Quantitities
            foreach (var invalidPersonnelSegmentSpecificationType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(t => t != null)
                    .Select(quantity => new PersonnelSegmentSpecificationType
                    {
                        PersonID = personId,
                        Quantity = new[]
                        {
                            quantity
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()

                    }))

            {
                yield return invalidPersonnelSegmentSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidPersonnelSegmentSpecificationType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new PersonnelSegmentSpecificationType
                    {
                        PersonID = personId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidPersonnelSegmentSpecificationType;
            }

            // invalid properties
            foreach (var invalidPersonnelSegmentSpecificationType in
                PersonnelSegmentSpecificationPropertyTypeDataGenerator
                    .GenerateInvalidPersonnelSegmentSpecificationPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new PersonnelSegmentSpecificationType
                    {
                        PersonID = personId,
                        PersonnelSegmentSpecificationProperty = new[]
                        {
                            property
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))
            {
                yield return invalidPersonnelSegmentSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid personnel segment specification type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelSegmentSpecificationType[]&gt;.</returns>
        public static IEnumerable<PersonnelSegmentSpecificationType[]>
            GenerateValidPersonnelSegmentSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PersonnelSegmentSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PersonnelSegmentSpecificationType
                    {
                        PersonnelClassID = null,
                        PersonID = new PersonIDType
                        {
                            Value = $"PersonID.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        PersonnelUse = new PersonnelUseType
                        {
                            Value = "PersonnelUse"
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        PersonnelSegmentSpecificationProperty =
                            PersonnelSegmentSpecificationPropertyTypeDataGenerator
                                .GenerateValidPersonnelSegmentSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid personnel segment specification types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<PersonnelSegmentSpecificationType> GenerateValidPersonnelSegmentSpecificationTypes()
        {
            var personnelUse = new PersonnelUseType
            {
                Value = "PersonnelUse"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PersonnelSegmentSpecificationType
                {
                    PersonnelClassID = null,
                    PersonID = new PersonIDType
                    {
                        Value = $"PersonID.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    PersonnelUse = personnelUse,
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    PersonnelSegmentSpecificationProperty =
                        PersonnelSegmentSpecificationPropertyTypeDataGenerator
                            .GenerateValidPersonnelSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new PersonnelSegmentSpecificationType
            {
                PersonnelClassID = new PersonnelClassIDType
                {
                    Value = "ClassID"
                },
                PersonID = null,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            yield return new PersonnelSegmentSpecificationType
            {
                PersonnelClassID = null,
                PersonID = new PersonIDType
                {
                    Value = "PersonID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };
        }

        #endregion
    }
}