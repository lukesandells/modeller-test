﻿using System.Collections.Generic;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class UnitOfMeasureTypeDataGenerator.
    ///     Used for generating test Data
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class UnitOfMeasureTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid unit of measure types.
        /// </summary>
        /// <returns>IEnumerable&lt;UnitOfMeasureType&gt;.</returns>
        public static IEnumerable<UnitOfMeasureType> GenerateInvalidUnitOfMeasureTypes()
        {
            yield return default(UnitOfMeasureType);

            yield return new UnitOfMeasureType
            {
                Value = null
            };
        }

        /// <summary>
        ///     Generates the valid unit of measure types.
        /// </summary>
        /// <returns>IEnumerable&lt;UnitOfMeasureType&gt;.</returns>
        public static IEnumerable<UnitOfMeasureType> GenerateValidUnitOfMeasureTypes()
        {
            var symbol = "Kg";

            yield return new UnitOfMeasureType
            {
                Value = symbol
            };
        }

        #endregion
    }
}