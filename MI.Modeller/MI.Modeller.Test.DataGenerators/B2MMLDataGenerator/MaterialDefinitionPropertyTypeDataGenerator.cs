﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class MaterialDefinitionPropertyTypeDataGenerator.
    /// </summary>
    public static class MaterialDefinitionPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid material definition property types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionPropertyType&gt;.</returns>
        public static IEnumerable<MaterialDefinitionPropertyType> GenerateInvalidMaterialDefinitionPropertyTypes()
        {
            // null ref
            yield return default(MaterialDefinitionPropertyType);

            // No InternalId
            yield return new MaterialDefinitionPropertyType();

            // invalid Values
            foreach (var invalidMaterialDefinitionPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new MaterialDefinitionPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "MaterialDefinitionProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidMaterialDefinitionPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid material definition property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionPropertyType[]&gt;.</returns>
        public static IEnumerable<MaterialDefinitionPropertyType[]> GenerateValidMaterialDefinitionPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialDefinitionPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialDefinitionPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"MaterialDefinitionProperty.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        MaterialTestSpecificationID = GenerateValidMaterialTestSpecificationIdTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid material definition property types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionPropertyType&gt;.</returns>
        public static IEnumerable<MaterialDefinitionPropertyType> GenerateValidMaterialDefinitionPropertyTypes()
        {
            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new MaterialDefinitionPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"MaterialDefinitionProperty.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    MaterialTestSpecificationID = GenerateValidMaterialTestSpecificationIdTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    MaterialDefinitionProperty = GenerateValidMaterialDefinitionPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new MaterialDefinitionPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"MaterialDefinitionProperty.InternalId.{i}"
                    }
                };
            }
        }

        /// <summary>
        ///     Generates the valid material test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<MaterialTestSpecificationIDType[]> GenerateValidMaterialTestSpecificationIdTypeArrays
            ()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialTestSpecificationIDType
                    {
                        Value = $"Test Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}