﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class MaterialSegmentSpecificationTypeDataGenerator.
    /// </summary>
    public static class MaterialSegmentSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid material segment specification types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<MaterialSegmentSpecificationType> GenerateInvalidMaterialSegmentSpecificationTypes()
        {
            var materialClassId = new MaterialClassIDType
            {
                Value = "InternalId"
            };

            var materialDefinitionId = new MaterialDefinitionIDType
            {
                Value = "InternalId"
            };

            // null ref
            yield return default(MaterialSegmentSpecificationType);

            var materialSpecficationId = new IdentifierType
            {
                Value = "materialSpecificationId"
            };

            // class and definition specified
            yield return new MaterialSegmentSpecificationType
            {
                MaterialClassID = materialClassId,
                MaterialDefinitionID = materialDefinitionId,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .First()
            };

            // invalid Quantitities

            foreach (var invalidMaterialSegmentSpecificationType in QuantityValueTypeDataGenerator
                .GenerateInvalidQuantityValueTypes()
                .Where(t => t != null)
                .Select(quantity => new MaterialSegmentSpecificationType
                {
                    ID = materialSpecficationId,
                    MaterialClassID = materialClassId,
                    Quantity = new[]
                    {
                        quantity
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
                }))

            {
                yield return invalidMaterialSegmentSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidMaterialSegmentSpecificationType in HierarchyScopeTypeDataGenerator
                .GenerateInvalidHierarchyScopeTypes()
                .Where(q => q != null)
                .Select(hierarchyScope => new MaterialSegmentSpecificationType
                {
                    ID = materialSpecficationId,
                    MaterialClassID = materialClassId,
                    HierarchyScope = hierarchyScope
                }))
            {
                yield return invalidMaterialSegmentSpecificationType;
            }

            // invalid properties
            foreach (var invalidMaterialSegmentSpecificationType in MaterialSegmentSpecificationPropertyTypeDataGenerator
                .GenerateInvalidMaterialSegmentSpecificationPropertyTypes()
                .Where(q => q != null)
                .Select(property => new MaterialSegmentSpecificationType
                {
                    ID = materialSpecficationId,
                    MaterialClassID = materialClassId,
                    MaterialSegmentSpecificationProperty = new[]
                    {
                        property
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
                }))
            {
                yield return invalidMaterialSegmentSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid material segment specification type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialSegmentSpecificationType[]&gt;.</returns>
        public static IEnumerable<MaterialSegmentSpecificationType[]> GenerateValidMaterialSegmentSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialSegmentSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialSegmentSpecificationType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Specification.ID.{i}.{j}"
                        },
                        MaterialClassID = new MaterialClassIDType
                        {
                            Value = $"ClassID.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        MaterialUse = new MaterialUseType
                        {
                            Value = Domain.Client.MaterialUseType.Produced.ToString()
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        GeospatialDesignation = GeoSpatialDesignationTypeDataGenerator
                            .GenerateValidGeospatialDesignationTypes()
                            .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        MaterialSegmentSpecificationProperty = MaterialSegmentSpecificationPropertyTypeDataGenerator
                            .GenerateValidMaterialSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material segment specification types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<MaterialSegmentSpecificationType> GenerateValidMaterialSegmentSpecificationTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new MaterialSegmentSpecificationType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Specification.ID.{i}"
                    },
                    MaterialDefinitionID = new MaterialDefinitionIDType
                    {
                        Value = $"Definition ID.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    MaterialUse = new MaterialUseType
                    {
                        Value = Domain.Client.MaterialUseType.Produced.ToString()
                    },
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    GeospatialDesignation = GeoSpatialDesignationTypeDataGenerator
                        .GenerateValidGeospatialDesignationTypes()
                        .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    MaterialSegmentSpecificationProperty = MaterialSegmentSpecificationPropertyTypeDataGenerator
                        .GenerateValidMaterialSegmentSpecificationPropertyTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    AssemblyType = new AssemblyTypeType
                    {
                        Value = AssemblyType.Other.ToString()
                    },
                    AssemblyRelationship = new AssemblyRelationshipType
                    {
                        Value = Domain.Client.AssemblyRelationshipType.Permanent.ToString()
                    },
                    AssemblySpecificationID = GenerateValidAssemblySpecificationIdArrays()
                        .FirstOrDefault(c => c.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new MaterialSegmentSpecificationType
            {
                ID = new IdentifierType
                {
                    Value = "Specification.ID"
                },
                MaterialDefinitionID = new MaterialDefinitionIDType
                {
                    Value = "Definition ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .First()
            };

            // basic with only mandatory fields
            yield return new MaterialSegmentSpecificationType
            {
                ID = new IdentifierType
                {
                    Value = "Specification.ID"
                },
                MaterialClassID = new MaterialClassIDType
                {
                    Value = "ClassID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .First()
            };
        }

        /// <summary>
        ///     Generates the valid assembly specification identifier arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;IdentifierType[]&gt;.</returns>
        private static IEnumerable<IdentifierType[]> GenerateValidAssemblySpecificationIdArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new IdentifierType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new IdentifierType
                    {
                        Value = $"Assembly Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}