﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ResourceRelationshipNetworkTypeDataGenerator.
    /// </summary>
    public static class ResourceRelationshipNetworkTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid resource relationship network types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceRelationshipNetworkType&gt;.</returns>
        public static IEnumerable<ResourceRelationshipNetworkType> GenerateInvalidResourceRelationshipNetworkTypes()
        {
            //private ResourceNetworkConnectionType[] resourceNetworkConnectionField;

            // null ref
            yield return default(ResourceRelationshipNetworkType);

            // invalid Values
            foreach (var invalidResourceRelationshipNetworkType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(o => o != default(HierarchyScopeType))
                    .Select(var => new ResourceRelationshipNetworkType
                    {
                        ID = new IdentifierType
                        {
                            Value = "ResourceRelationshipNetwork.InternalId"
                        },
                        HierarchyScope = var
                    }))
            {
                yield return invalidResourceRelationshipNetworkType;
            }

            // invalid relationshipTypeField
            foreach (var invalidResourceRelationshipNetworkType in
                RelationshipTypeTypeDataGenerator.GenerateInvalidRelationshipTypeTypes()
                    .Where(o => o != default(RelationshipTypeType))
                    .Select(var => new ResourceRelationshipNetworkType
                    {
                        ID = new IdentifierType
                        {
                            Value = "ResourceRelationshipNetwork.InternalId"
                        },
                        RelationshipType = var
                    }))
            {
                yield return invalidResourceRelationshipNetworkType;
            }

            // invalid relationshipFormField
            foreach (var invalidResourceRelationshipNetworkType in
                RelationshipFormTypeDataGenerator.GenerateInvalidRelationshipFormTypes()
                    .Where(o => o != default(RelationshipFormType))
                    .Select(var => new ResourceRelationshipNetworkType
                    {
                        ID = new IdentifierType
                        {
                            Value = "ResourceRelationshipNetwork.InternalId"
                        },
                        RelationshipForm = var
                    }))
            {
                yield return invalidResourceRelationshipNetworkType;
            }

            // invalid resourceNetworkConnectionField
            foreach (var invalidResourceRelationshipNetworkType in
                ResourceNetworkConnectionTypeDataGenerator.GenerateInvalidResourceNetworkConnectionTypes()
                    .Where(o => o != default(ResourceNetworkConnectionType))
                    .Select(var => new ResourceRelationshipNetworkType
                    {
                        ID = new IdentifierType
                        {
                            Value = "ResourceRelationshipNetwork.InternalId"
                        },
                        ResourceNetworkConnection = new[]
                        {
                            var
                        }
                    }))
            {
                yield return invalidResourceRelationshipNetworkType;
            }
        }

        /// <summary>
        ///     Generates the valid resource relationship network type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceRelationshipNetworkType[]&gt;.</returns>
        public static IEnumerable<ResourceRelationshipNetworkType[]> GenerateValidResourceRelationshipNetworkTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ResourceRelationshipNetworkType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ResourceRelationshipNetworkType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"ResourceRelationshipNetwork.InternalId.{j}"
                        },
                        Description = new[]
                        {
                            new DescriptionType
                            {
                                Value = "Description"
                            }
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        RelationshipType = RelationshipTypeTypeDataGenerator.GenerateValidRelationshipTypeTypes()
                            .FirstOrDefault(r => r != null),
                        RelationshipForm = RelationshipFormTypeDataGenerator.GenerateValidRelationshipFormTypes()
                            .FirstOrDefault(r => r != null),
                        PublishedDate = new PublishedDateType
                        {
                            Value = DateTime.Today
                        },
                        ResourceNetworkConnection =
                            ResourceNetworkConnectionTypeDataGenerator.GenerateValidResourceNetworkConnectionTypeArrays()
                                .FirstOrDefault(c => c.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid resource relationship network types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceRelationshipNetworkType&gt;.</returns>
        public static IEnumerable<ResourceRelationshipNetworkType> GenerateValidResourceRelationshipNetworkTypes()
        {
            var id = new IdentifierType
            {
                Value = "ResourceRelationshipNetwork.ID"
            };
            var description = new[]
            {
                new DescriptionType
                {
                    Value = "Description"
                }
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourceRelationshipNetworkType
                {
                    ID = id,
                    Description = description,
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    RelationshipType = RelationshipTypeTypeDataGenerator.GenerateValidRelationshipTypeTypes()
                        .FirstOrDefault(r => r != null),
                    RelationshipForm = RelationshipFormTypeDataGenerator.GenerateValidRelationshipFormTypes()
                        .FirstOrDefault(r => r != null),
                    PublishedDate = new PublishedDateType
                    {
                        Value = DateTime.Today
                    },
                    ResourceNetworkConnection =
                        ResourceNetworkConnectionTypeDataGenerator.GenerateValidResourceNetworkConnectionTypeArrays()
                            .FirstOrDefault(c => c.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new ResourceRelationshipNetworkType();
        }

        #endregion
    }
}