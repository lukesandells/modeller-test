﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PersonnelInformationTypeDataGenerator.
    /// </summary>
    public static class PersonnelInformationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment information types.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelInformationType&gt;.</returns>
        public static IEnumerable<PersonnelInformationType> GenerateValidPersonnelInformationTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PersonnelInformationType
                {
                    ID = new IdentifierType
                    {
                        Value = $"PersonnelInformationType.InternalId.{i}"
                    },
                    PublishedDate = new PublishedDateType
                    {
                        Value = DateTime.Now
                    },
                    PersonnelClass = PersonnelClassTypeDataGenerator.GenerateValidPersonnelClassTypeArrays()
                        .FirstOrDefault(e => e.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new PersonnelInformationType
            {
                ID = new IdentifierType
                {
                    Value = "Personnel.ID"
                }
            };
        }

        #endregion
    }
}