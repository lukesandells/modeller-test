﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class MaterialDefinitionTypeDataGenerator.
    /// </summary>
    public static class MaterialDefinitionTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid material definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionType&gt;.</returns>
        public static IEnumerable<MaterialDefinitionType> GenerateInvalidMaterialDefinitionTypes()
        {
            var materialDefinitionId = new IdentifierType
            {
                Value = "Mat.Def.InternalId"
            };

            // null ref
            yield return default(MaterialDefinitionType);

            // no ID
            yield return new MaterialDefinitionType();

            // invalid hierarchy scopes
            foreach (var invalidMaterialDefinitionType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new MaterialDefinitionType
                    {
                        ID = materialDefinitionId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidMaterialDefinitionType;
            }

            // invalid properties
            foreach (var invalidMaterialDefinitionType in
                MaterialDefinitionPropertyTypeDataGenerator.GenerateInvalidMaterialDefinitionPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new MaterialDefinitionType
                    {
                        ID = materialDefinitionId,
                        MaterialDefinitionProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidMaterialDefinitionType;
            }
        }

        /// <summary>
        ///     Generates the valid material definition type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionType[]&gt;.</returns>
        public static IEnumerable<MaterialDefinitionType[]> GenerateValidMaterialDefinitionTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialDefinitionType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialDefinitionType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Mat.Def.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        MaterialDefinitionProperty =
                            MaterialDefinitionPropertyTypeDataGenerator
                                .GenerateValidMaterialDefinitionPropertyTypeArrays()
                                .FirstOrDefault(m => m.Length == 2),
                        MaterialClassID = GenerateValidMaterialClassIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        MaterialLotID = GenerateValidMaterialLotIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        MaterialTestSpecificationID = GenerateValidMaterialTestSpecificationIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        AssemblylDefinitionID = GenerateValidMaterialDefinitionIdTypeArrays()
                            .FirstOrDefault(c => c.Length == 2),
                        AssemblyType = AssemblyTypeTypeDataGenerator.GenerateValidAssemblyTypeTypes()
                            .FirstOrDefault(a => a != null),
                        AssemblyRelationship =
                            AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                                .FirstOrDefault(a => a != null)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionType&gt;.</returns>
        public static IEnumerable<MaterialDefinitionType> GenerateValidMaterialDefinitionTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new MaterialDefinitionType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Mat.Def.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    MaterialDefinitionProperty =
                        MaterialDefinitionPropertyTypeDataGenerator.GenerateValidMaterialDefinitionPropertyTypeArrays()
                            .FirstOrDefault(m => m.Length == i),
                    MaterialClassID = GenerateValidMaterialClassIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    MaterialLotID = GenerateValidMaterialLotIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    MaterialTestSpecificationID = GenerateValidMaterialTestSpecificationIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    AssemblylDefinitionID = GenerateValidMaterialDefinitionIdTypeArrays()
                        .FirstOrDefault(c => c.Length == i),
                    AssemblyType = AssemblyTypeTypeDataGenerator.GenerateValidAssemblyTypeTypes()
                        .FirstOrDefault(a => a != null),
                    AssemblyRelationship =
                        AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                            .FirstOrDefault(a => a != null)
                };
            }

            // basic with only mandatory fields
            yield return new MaterialDefinitionType
            {
                ID = new IdentifierType
                {
                    Value = "Specification.ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(h => h != null),
            };
        }

        /// <summary>
        ///     Generates the valid material class identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassIDType[]&gt;.</returns>
        private static IEnumerable<MaterialClassIDType[]> GenerateValidMaterialClassIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialClassIDType
                    {
                        Value = $"MaterialClassIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material definition identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionIDType[]&gt;.</returns>
        private static IEnumerable<MaterialDefinitionIDType[]> GenerateValidMaterialDefinitionIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialDefinitionIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialDefinitionIDType
                    {
                        Value = $"MaterialDefinitionIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material lot identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialLotIDType[]&gt;.</returns>
        private static IEnumerable<MaterialLotIDType[]> GenerateValidMaterialLotIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialLotIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialLotIDType
                    {
                        Value = $"MaterialLotIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<MaterialTestSpecificationIDType[]> GenerateValidMaterialTestSpecificationIdTypeArrays
            ()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialTestSpecificationIDType
                    {
                        Value = $"MaterialTestSpecificationIDType.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}