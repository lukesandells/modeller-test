using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OperationsMaterialBillItemTypeDataGenerator.
    /// </summary>
    public static class OperationsMaterialBillItemTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid operations material bill item types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBillItemType&gt;.</returns>
        public static IEnumerable<OperationsMaterialBillItemType> GenerateInvalidOperationsMaterialBillItemTypes()
        {
            // null ref
            yield return default(OperationsMaterialBillItemType);

            // null id
            yield return new OperationsMaterialBillItemType();

            // invalid Assembly Type
            foreach (var invalidOperationsMaterialBillItemType in
                AssemblyTypeTypeDataGenerator.GenerateInvalidAssemblyTypeTypes()
                    .Where(o => o != default(AssemblyTypeType))
                    .Select(v => new OperationsMaterialBillItemType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsMaterialBillItemType.InternalId"
                        },
                        AssemblyType = v
                    }))
            {
                yield return invalidOperationsMaterialBillItemType;
            }

            // invalid AssemblyRelationshipType
            foreach (var invalidOperationsMaterialBillItemType in
                AssemblyRelationshipTypeDataGenerator.GenerateInvalidAssemblyRelationshipTypes()
                    .Where(o => o != default(AssemblyRelationshipType))
                    .Select(v => new OperationsMaterialBillItemType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsMaterialBillItemType.InternalId"
                        },
                        AssemblyRelationship = v
                    }))
            {
                yield return invalidOperationsMaterialBillItemType;
            }

            //invalid quantities
            foreach (var invalidOperationsMaterialBillItemType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(o => o != default(QuantityValueType))
                    .Select(quantity => new OperationsMaterialBillItemType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsMaterialBillItemType.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidOperationsMaterialBillItemType;
            }
        }

        /// <summary>
        ///     Generates the valid operations material bill item type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBillItemType[]&gt;.</returns>
        public static IEnumerable<OperationsMaterialBillItemType[]> GenerateValidOperationsMaterialBillItemTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OperationsMaterialBillItemType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OperationsMaterialBillItemType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"OperationsMaterialBillItemType.InternalId.{j}"
                        }
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid operations material bill item types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBillItemType&gt;.</returns>
        public static IEnumerable<OperationsMaterialBillItemType> GenerateValidOperationsMaterialBillItemTypes()
        {
            var id = new IdentifierType
            {
                Value = "OperationsMaterialBillItemType.InternalId"
            };
            var description = new DescriptionType
            {
                Value = "Description"
            };

            var useType = new MaterialUseType
            {
                Value = "Produced"
            };

            var assemblyType = new AssemblyTypeType
            {
                Value = "Logical"
            };
            var assemblyRelationship = new AssemblyRelationshipType
            {
                Value = "Permanent"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OperationsMaterialBillItemType
                {
                    ID = id,
                    Description = description,
                    MaterialClassID = GenerateValidMaterialClassIdArrays()
                        .FirstOrDefault(v => v.Length == i),
                    MaterialDefinitionID = GenerateValidMaterialDefinitionIdArrays()
                        .FirstOrDefault(v => v.Length == i),
                    UseType = useType,
                    AssemblyBillOfMaterialItem = GenerateValidOperationsMaterialBillItemTypeArrays()
                        .FirstOrDefault(v => v.Length == i),
                    AssemblyType = assemblyType,
                    AssemblyRelationship = assemblyRelationship,
                    MaterialSpecificationID = GenerateValidMaterialSpecificationIdArrays()
                        .FirstOrDefault(v => v.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(v => v.Length == i)
                };
            }

            yield return new OperationsMaterialBillItemType
            {
                ID = id
            };
        }

        /// <summary>
        ///     Generates the valid material class identifier arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClassIDType[]&gt;.</returns>
        private static IEnumerable<MaterialClassIDType[]> GenerateValidMaterialClassIdArrays()
        {
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialClassIDType
                    {
                        Value = $"Material ClassID.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material definition identifier arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinitionIDType[]&gt;.</returns>
        private static IEnumerable<MaterialDefinitionIDType[]> GenerateValidMaterialDefinitionIdArrays()
        {
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialDefinitionIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialDefinitionIDType
                    {
                        Value = $"MaterialDefinitionID.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material specification identifier arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;IdentifierType[]&gt;.</returns>
        private static IEnumerable<IdentifierType[]> GenerateValidMaterialSpecificationIdArrays()
        {
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new IdentifierType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new IdentifierType
                    {
                        Value = $"MaterialSpecificationID.{i}.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}