﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentLevelTypeDataGenerator.
    ///     Generate Data for Test cases
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    public static class EquipmentElementLevelTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment level types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentElementLevelType&gt;.</returns>
        public static IEnumerable<EquipmentElementLevelType> GenerateInvalidEquipmentElementLevelTypes()
        {
            // null no allowed
            yield return default(EquipmentElementLevelType);
            // type must value a type 
            yield return new EquipmentElementLevelType();
            // invalid values
            yield return new EquipmentElementLevelType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid equipment level types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentElementLevelType&gt;.</returns>
        public static IEnumerable<EquipmentElementLevelType> GenerateValidEquipmentElementLevelTypes()
        {
            return from EquipmentLevel value in Enum.GetValues(typeof(EquipmentLevel))
                where !EquipmentLevel.Enterprise.Equals(value)
                select new EquipmentElementLevelType
                {
                    Value = value.ToString(),
                    OtherValue = value == EquipmentLevel.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}