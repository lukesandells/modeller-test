﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationNodeTypeDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationNodeTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid workflow specification node types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationNodeType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationNodeType> GenerateInvalidWorkflowSpecificationNodeTypes()
        {
            // null ref
            yield return default(WorkflowSpecificationNodeType);
            // no ID
            yield return new WorkflowSpecificationNodeType();
            // Dont generate these as it results in a circular reference
            //// invalid Specifications
            //foreach (
            //    var invalidWorkflowSpecificationNodeType in
            //    WorkflowSpecificationTypeDataGenerator.GenerateInvalidWorkflowSpecificationTypes()
            //        .Where(value => value != null)
            //        .Select(specification => new WorkflowSpecificationNodeType
            //        {
            //            ID = new IdentifierType
            //            {
            //                Value = "WorkflowSpecificationNode.InternalId"
            //            },
            //            WorkflowSpecification = specification
            //        }))
            //{
            //    yield return invalidWorkflowSpecificationNodeType;
            //}

            // invalid Properties
            foreach (var invalidWorkflowSpecificationNodeType in
                WorkflowSpecificationPropertyTypeDataGenerator.GenerateInvalidWorkflowSpecificationPropertyTypes()
                    .Where(value => value != null)
                    .Select(property => new WorkflowSpecificationNodeType
                    {
                        ID = new IdentifierType
                        {
                            Value = "WorkflowSpecificationNode.InternalId"
                        },
                        Property = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidWorkflowSpecificationNodeType;
            }
        }

        /// <summary>
        ///     Generates the valid workflow specification node type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationNodeType[]&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationNodeType[]> GenerateValidWorkflowSpecificationNodeTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new WorkflowSpecificationNodeType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new WorkflowSpecificationNodeType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"WorkflowSpecificationNode.InternalId.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        NodeType = new IdentifierType
                        {
                            Value = "NodeType"
                        },
                        Property =
                            WorkflowSpecificationPropertyTypeDataGenerator
                                .GenerateValidWorkflowSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p.Length == 2),
                        WorkflowSpecification = new WorkflowSpecificationType
                        {
                            ID = new IdentifierType
                            {
                                Value = "ID"
                            }
                        }
                        // cannot generate as it results in circulare referdnce
                        //WorkflowSpecificationTypeDataGenerator.GenerateValidWorkflowSpecificationTypes()
                        //    .FirstOrDefault(s => s != null)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid workflow specification node types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationNodeType&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationNodeType> GenerateValidWorkflowSpecificationNodeTypes()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecificationNodeType
                {
                    ID = new IdentifierType
                    {
                        Value = $"WorkflowSpecificationNode.ID.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    NodeType = new IdentifierType
                    {
                        Value = $"NodeType.{i}"
                    },
                    Property =
                        WorkflowSpecificationPropertyTypeDataGenerator
                            .GenerateValidWorkflowSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p.Length == i),
                    WorkflowSpecification = new WorkflowSpecificationType
                    {
                        ID = new IdentifierType
                        {
                            Value = "ID"
                        }
                    }
                };
            }

            // basic mandatory fields only
            yield return new WorkflowSpecificationNodeType
            {
                ID = new IdentifierType
                {
                    Value = "WorkflowSpecificationNode.ID"
                }
            };
        }

        #endregion
    }
}