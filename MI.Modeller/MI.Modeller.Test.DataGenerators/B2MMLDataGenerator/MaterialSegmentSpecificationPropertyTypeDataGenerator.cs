﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class MaterialSegmentSpecificationPropertyTypeDataGenerator.
    /// </summary>
    public static class MaterialSegmentSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid material segment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<MaterialSegmentSpecificationPropertyType>
            GenerateInvalidMaterialSegmentSpecificationPropertyTypes()
        {
            // null ref
            yield return default(MaterialSegmentSpecificationPropertyType);

            // null id
            yield return new MaterialSegmentSpecificationPropertyType();

            // invalid Values
            foreach (var invalidMaterialSegmentSpecificationPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new MaterialSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "MaterialSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidMaterialSegmentSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidMaterialSegmentSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != null)
                    .Select(quantity => new MaterialSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "MaterialSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidMaterialSegmentSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid material segment specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialSegmentSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<MaterialSegmentSpecificationPropertyType[]>
            GenerateValidMaterialSegmentSpecificationPropertyTypeArrays()
        {
            // null, 
            yield return null;
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new MaterialSegmentSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"MaterialSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material segment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<MaterialSegmentSpecificationPropertyType>
            GenerateValidMaterialSegmentSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "MaterialSpecificationProperty.ID"
            };

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new MaterialSegmentSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    MaterialSegmentSpecificationProperty = GenerateValidMaterialSegmentSpecificationPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new MaterialSegmentSpecificationPropertyType
                {
                    ID = id
                };
            }
        }

        #endregion
    }
}