﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class PhysicalAssetSegmentSpecificationPropertyTypeDataGenerator.
    /// </summary>
    public static class PhysicalAssetSegmentSpecificationPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid physical asset segment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<PhysicalAssetSegmentSpecificationPropertyType>
            GenerateInvalidPhysicalAssetSegmentSpecificationPropertyTypes()
        {
            // null ref
            yield return default(PhysicalAssetSegmentSpecificationPropertyType);

            // invalid Values
            foreach (var invalidPhysicalAssetSegmentSpecificationPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new PhysicalAssetSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PhysicalAssetSpecificationProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidPhysicalAssetSegmentSpecificationPropertyType;
            }

            // invalid quantities
            foreach (var invalidPhysicalAssetSegmentSpecificationPropertyType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(quantity => quantity != default(QuantityValueType))
                    .Select(quantity => new PhysicalAssetSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "PhysicalAssetSpecificationProperty.InternalId"
                        },
                        Quantity = new[]
                        {
                            quantity
                        }
                    }))
            {
                yield return invalidPhysicalAssetSegmentSpecificationPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid physical asset segment specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetSegmentSpecificationPropertyType[]&gt;.</returns>
        public static IEnumerable<PhysicalAssetSegmentSpecificationPropertyType[]>
            GenerateValidPhysicalAssetSegmentSpecificationPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new PhysicalAssetSegmentSpecificationPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new PhysicalAssetSegmentSpecificationPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PhysicalAssetSpecificationProperty.InternalId.{j}"
                        },
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid physical asset segment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetSegmentSpecificationPropertyType&gt;.</returns>
        public static IEnumerable<PhysicalAssetSegmentSpecificationPropertyType>
            GenerateValidPhysicalAssetSegmentSpecificationPropertyTypes()
        {
            var id = new IdentifierType
            {
                Value = "PhysicalAssetSpecificationProperty.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new PhysicalAssetSegmentSpecificationPropertyType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(q => q?.Length == i),
                    PhysicalAssetSegmentSpecificationProperty =
                        GenerateValidPhysicalAssetSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new PhysicalAssetSegmentSpecificationPropertyType
            {
                ID = id
            };
        }

        #endregion
    }
}