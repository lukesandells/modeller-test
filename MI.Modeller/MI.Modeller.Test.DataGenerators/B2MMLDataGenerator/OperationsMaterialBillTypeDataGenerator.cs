﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OperationsMaterialBillTypeDataGenerator.
    /// </summary>
    public static class OperationsMaterialBillTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid operations material bill types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBillType&gt;.</returns>
        public static IEnumerable<OperationsMaterialBillType> GenerateInvalidOperationsMaterialBillTypes()
        {
            // null ref
            yield return default(OperationsMaterialBillType);
            // no ID
            yield return new OperationsMaterialBillType();
            // invalid Hierarchy Scope
            foreach (var invalidOperationsMaterialBillType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(o => o != default(HierarchyScopeType))
                    .Select(hierarchyScope => new OperationsMaterialBillType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsMaterialBillType.InternalId"
                        },
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidOperationsMaterialBillType;
            }

            // Invalid Items
            foreach (var invalidOperationsMaterialBillType in
                OperationsMaterialBillItemTypeDataGenerator.GenerateInvalidOperationsMaterialBillItemTypes()
                    .Where(o => o != default(OperationsMaterialBillItemType))
                    .Select(item => new OperationsMaterialBillType
                    {
                        ID = new IdentifierType
                        {
                            Value = "OperationsMaterialBillType.InternalId"
                        },
                        OperationsMaterialBillItem = new[]
                        {
                            item
                        }
                    }))
            {
                yield return invalidOperationsMaterialBillType;
            }
        }

        public static IEnumerable<OperationsMaterialBillType[]> GenerateValidOperationsMaterialBillTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OperationsMaterialBillType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OperationsMaterialBillType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"OperationsMaterialBillType.InternalId.{i}.{j}"
                        },
                        Description = new DescriptionType
                        {
                            Value = $"Description.{i}.{j}"
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .FirstOrDefault(h => h != null),
                        OperationsMaterialBillItem =
                            OperationsMaterialBillItemTypeDataGenerator
                                .GenerateValidOperationsMaterialBillItemTypeArrays()
                                .FirstOrDefault(h => h.Length == i)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid operations material bill types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBillType&gt;.</returns>
        public static IEnumerable<OperationsMaterialBillType> GenerateValidOperationsMaterialBillTypes()
        {
            var id = new IdentifierType
            {
                Value = "OperationsMaterialBillType.InternalId"
            };
            var description = new DescriptionType
            {
                Value = "Description"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OperationsMaterialBillType
                {
                    ID = id,
                    Description = description,
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .FirstOrDefault(h => h != null),
                    OperationsMaterialBillItem =
                        OperationsMaterialBillItemTypeDataGenerator.GenerateValidOperationsMaterialBillItemTypeArrays()
                            .FirstOrDefault(h => h.Length == i)
                };
            }

            // basic - InternalId Only
            yield return new OperationsMaterialBillType
            {
                ID = id,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
            };
        }

        #endregion
    }
}