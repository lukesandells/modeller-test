﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class TestResultTypeDataGenerator.
    /// </summary>
    internal static class TestResultTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid test result types.
        /// </summary>
        /// <returns>IEnumerable&lt;TestResultType&gt;.</returns>
        public static IEnumerable<TestResultType> GenerateInvalidTestResultTypes()
        {
            // null ref
            yield return default(TestResultType);

            // null id
            yield return new TestResultType();

            // invalid Results
            foreach (var invalidTestResultType in ResultTypeDataGenerator.GenerateInvalidResultTypes()
                .Where(i => i != null)
                .Select(resultType => new TestResultType
                {
                    ID = new IdentifierType
                    {
                        Value = "TestResult.InternalId"
                    },
                    Result = new[]
                    {
                        resultType
                    }
                }))
            {
                yield return invalidTestResultType;
            }
        }

        /// <summary>
        ///     Generates the valid test result type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;TestResultType[]&gt;.</returns>
        public static IEnumerable<TestResultType[]> GenerateValidTestResultTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new TestResultType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new TestResultType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"TestResult.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        TestDateTime = new TestDateTimeType
                        {
                            Value = DateTime.Today
                        },
                        Result = ResultTypeDataGenerator.GenerateValidResultTypeArrays()
                            .FirstOrDefault(d => d.Length == 2),
                        ExpirationTime = new ExpirationTimeType
                        {
                            Value = DateTime.Today
                        }
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid op personnel specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;TestResultType&gt;.</returns>
        public static IEnumerable<TestResultType> GenerateValidTestResultTypes()
        {
            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new TestResultType
                {
                    ID = new IdentifierType
                    {
                        Value = $"TestResult.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    TestDateTime = new TestDateTimeType
                    {
                        Value = DateTime.Today
                    },
                    Result = ResultTypeDataGenerator.GenerateValidResultTypeArrays()
                        .FirstOrDefault(d => d.Length == i),
                    ExpirationTime = new ExpirationTimeType
                    {
                        Value = DateTime.Today
                    }
                };
            }

            // basic mandatory fields only
            yield return new TestResultType
            {
                ID = new IdentifierType
                {
                    Value = "TestResult.InternalId"
                }
            };
        }

        #endregion
    }
}