﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ParameterTypeDataGenerator.
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class ParameterTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op equipment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;ParameterType&gt;.</returns>
        public static IEnumerable<ParameterType> GenerateInvalidParameterTypes()
        {
            // null ref
            yield return default(ParameterType);

            // invalid Values
            foreach (var invalidParameterType in ValueTypeDataGenerator.GenerateInvalidValueTypes()
                .Where(value => value != default(ValueType))
                .Select(value => new ParameterType
                {
                    ID = new IdentifierType
                    {
                        Value = "Parameter.InternalId"
                    },
                    Value = new[]
                    {
                        value
                    }
                }))
            {
                yield return invalidParameterType;
            }
        }

        /// <summary>
        ///     Generates the valid op equipment specification property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ParameterType[]&gt;.</returns>
        public static IEnumerable<ParameterType[]> GenerateValidParameterTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ParameterType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ParameterType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Parameter.InternalId.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op equipment specification property types.
        /// </summary>
        /// <returns>IEnumerable&lt;ParameterType&gt;.</returns>
        public static IEnumerable<ParameterType> GenerateValidParameterTypes()
        {
            var id = new IdentifierType
            {
                Value = "Parameter.ID"
            };

            // all fields created but empty

            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ParameterType
                {
                    ID = id,
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    Parameter = GenerateValidParameterTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new ParameterType
            {
                ID = id
            };
        }

        #endregion
    }
}