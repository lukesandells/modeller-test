﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class DependencyTypeDataGenerator.
    ///     Generate Data for Test cases
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    ///     records
    /// </summary>
    public static class DependencyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid dependency types.
        /// </summary>
        /// <returns>IEnumerable&lt;DependencyType&gt;.</returns>
        public static IEnumerable<DependencyType> GenerateInvalidDependencyTypes()
        {
            // null no allowed
            yield return default(DependencyType);
            // type must value a type 
            yield return new DependencyType();
            // invalid values
            yield return new DependencyType
            {
                Value = "InvalidValue"
            };
        }

        /// <summary>
        ///     Generates the valid dependency types.
        /// </summary>
        /// <returns>IEnumerable&lt;DependencyType&gt;.</returns>
        public static IEnumerable<DependencyType> GenerateValidDependencyTypes()
        {
            return from Domain.Client.DependencyType value in Enum.GetValues(typeof(Domain.Client.DependencyType))
                select new DependencyType
				{
                    Value = value.ToString(),
                    OtherValue = value == Domain.Client.DependencyType.Other
                        ? "OtherValue"
                        : default(string)
                };
        }

        #endregion
    }
}