﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentSegmentSpecificationTypeDataGenerator.
    /// </summary>
    public static class EquipmentSegmentSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment segment specification types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<EquipmentSegmentSpecificationType> GenerateInvalidEquipmentSegmentSpecificationTypes()
        {
            var equipmentId = new EquipmentIDType
            {
                Value = "EquipmentId"
            };

            // null ref
            yield return default(EquipmentSegmentSpecificationType);

            // class and definition
            yield return new EquipmentSegmentSpecificationType
            {
                EquipmentID = equipmentId,
                EquipmentClassID = new EquipmentClassIDType
                {
                    Value = "EquipmentClassId"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
            };

            // invalid Quantitities
            foreach (var invalidEquipmentSegmentSpecificationType in
                QuantityValueTypeDataGenerator.GenerateInvalidQuantityValueTypes()
                    .Where(t => t != null)
                    .Select(quantity => new EquipmentSegmentSpecificationType
                    {
                        EquipmentID = equipmentId,
                        Quantity = new[]
                        {
                            quantity
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First()
                    }))

            {
                yield return invalidEquipmentSegmentSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidEquipmentSegmentSpecificationType in
                HierarchyScopeTypeDataGenerator.GenerateInvalidHierarchyScopeTypes()
                    .Where(q => q != null)
                    .Select(hierarchyScope => new EquipmentSegmentSpecificationType
                    {
                        EquipmentID = equipmentId,
                        HierarchyScope = hierarchyScope
                    }))
            {
                yield return invalidEquipmentSegmentSpecificationType;
            }

            // invalid properties
            foreach (var invalidEquipmentSegmentSpecificationType in
                EquipmentSegmentSpecificationPropertyTypeDataGenerator
                    .GenerateInvalidEquipmentSegmentSpecificationPropertyTypes()
                    .Where(q => q != null)
                    .Select(property => new EquipmentSegmentSpecificationType
                    {
                        EquipmentID = equipmentId,
                        EquipmentSegmentSpecificationProperty = new[]
                        {
                            property
                        },
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                    }))
            {
                yield return invalidEquipmentSegmentSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid equipment segment specification type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentSegmentSpecificationType[]&gt;.</returns>
        public static IEnumerable<EquipmentSegmentSpecificationType[]>
            GenerateValidEquipmentSegmentSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentSegmentSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentSegmentSpecificationType
                    {
                        EquipmentClassID = new EquipmentClassIDType
                        {
                            Value = $"InternalId{i}.{j}"
                        },
                        EquipmentID = null,
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        EquipmentUse = new EquipmentUseType
                        {
                            Value = "EquipmentUse"
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        GeospatialDesignation =
                            GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                                .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        EquipmentSegmentSpecificationProperty =
                            EquipmentSegmentSpecificationPropertyTypeDataGenerator
                                .GenerateValidEquipmentSegmentSpecificationPropertyTypeArrays()
                                .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid equipment segment specification types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentSegmentSpecificationType&gt;.</returns>
        public static IEnumerable<EquipmentSegmentSpecificationType> GenerateValidEquipmentSegmentSpecificationTypes()
        {
            var equipmentUse = new EquipmentUseType
            {
                Value = "EquipmentUse"
            };

            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentSegmentSpecificationType
                {
                    EquipmentClassID = null,
                    EquipmentID = new EquipmentIDType
                    {
                        Value = $"InternalId{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    EquipmentUse = equipmentUse,
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    GeospatialDesignation =
                        GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                            .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    EquipmentSegmentSpecificationProperty =
                        EquipmentSegmentSpecificationPropertyTypeDataGenerator
                            .GenerateValidEquipmentSegmentSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == i)
                };
            }
        }

        #endregion
    }
}