﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class OpMaterialSpecificationTypeDataGenerator.
    ///     Generate data for testing
    ///     Methods will yield the most comprehensive objects first, so all data populated and collections filled with multiple
    /// </summary>
    public static class OpMaterialSpecificationTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid op material specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpMaterialSpecificationType&gt;.</returns>
        public static IEnumerable<OpMaterialSpecificationType> GenerateInvalidOpMaterialSpecificationTypes()
        {
            MaterialClassIDType[] materialClassId =
            {
                new MaterialClassIDType
                {
                    Value = "EquipmentClassId"
                }
            };

            MaterialDefinitionIDType[] materialDefinitionId =
            {
                new MaterialDefinitionIDType
                {
                    Value = "DefId"
                }
            };

            var materialSpecficationId = new IdentifierType
            {
                Value = "materialSpecificationId"
            };

            // null ref
            yield return default(OpMaterialSpecificationType);

            // Class and definition specificed
            yield return new OpMaterialSpecificationType
            {
                MaterialClassID = materialClassId,
                MaterialDefinitionID = materialDefinitionId,
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .First()
            };

            // invalid Quantitities
            foreach (var invalidOpMaterialSpecificationType in QuantityValueTypeDataGenerator
                .GenerateInvalidQuantityValueTypes()
                .Where(t => t != null)
                .Select(quantity => new OpMaterialSpecificationType
                {
                    ID = materialSpecficationId,
                    MaterialClassID = materialClassId,
                    Quantity = new[]
                    {
                        quantity
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
                }))

            {
                yield return invalidOpMaterialSpecificationType;
            }

            // invalid hierarchy scopes
            foreach (var invalidOpMaterialSpecificationType in HierarchyScopeTypeDataGenerator
                .GenerateInvalidHierarchyScopeTypes()
                .Where(q => q != null)
                .Select(hierarchyScope => new OpMaterialSpecificationType
                {
                    ID = materialSpecficationId,
                    MaterialClassID = materialClassId,
                    HierarchyScope = hierarchyScope
                }))
            {
                yield return invalidOpMaterialSpecificationType;
            }

            // invalid properties
            foreach (var invalidOpMaterialSpecificationType in OpMaterialSpecificationPropertyTypeDataGenerator
                .GenerateInvalidOpMaterialSpecificationPropertyTypes()
                .Where(q => q != null)
                .Select(property => new OpMaterialSpecificationType
                {
                    ID = materialSpecficationId,
                    MaterialClassID = materialClassId,
                    MaterialSpecificationProperty = new[]
                    {
                        property
                    },
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First()
                }))
            {
                yield return invalidOpMaterialSpecificationType;
            }
        }

        /// <summary>
        ///     Generates the valid op material specification type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpMaterialSpecificationType[]&gt;.</returns>
        public static IEnumerable<OpMaterialSpecificationType[]> GenerateValidOpMaterialSpecificationTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new OpMaterialSpecificationType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new OpMaterialSpecificationType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"Specification.ID.{i}.{j}"
                        },
                        MaterialClassID = GenerateValidMaterialClassIdTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        MaterialUse = new MaterialUseType
                        {
                            Value = Domain.Client.MaterialUseType.Produced.ToString()
                        },
                        Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2),
                        SpatialLocation = GeoSpatialDesignationTypeDataGenerator
                            .GenerateValidGeospatialDesignationTypes()
                            .First(),
                        HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                            .First(),
                        MaterialSpecificationProperty = OpMaterialSpecificationPropertyTypeDataGenerator
                            .GenerateValidOpMaterialSpecificationPropertyTypeArrays()
                            .FirstOrDefault(p => p?.Length == 2)
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid op material specification types.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;OpMaterialSpecificationType&gt;.</returns>
        public static IEnumerable<OpMaterialSpecificationType> GenerateValidOpMaterialSpecificationTypes()
        {
            // All fields populated and combinations of collections
            // empty
            // one
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OpMaterialSpecificationType
                {
                    ID = new IdentifierType
                    {
                        Value = $"Specification.ID.{i}"
                    },
                    MaterialDefinitionID = GenerateValidMaterialDefinitionIdTypeArrays()
                        .FirstOrDefault(p => p?.Length == 1),
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    MaterialUse = new MaterialUseType
                    {
                        Value = Domain.Client.MaterialUseType.Produced.ToString()
                    },
                    Quantity = QuantityValueTypeDataGenerator.GenerateValidQuantityValueTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    SpatialLocation = GeoSpatialDesignationTypeDataGenerator.GenerateValidGeospatialDesignationTypes()
                        .First(),
                    HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                        .First(),
                    MaterialSpecificationProperty = OpMaterialSpecificationPropertyTypeDataGenerator
                        .GenerateValidOpMaterialSpecificationPropertyTypeArrays()
                        .FirstOrDefault(p => p?.Length == i),
                    AssemblyType = new AssemblyTypeType
                    {
                        Value = AssemblyType.Other.ToString()
                    },
                    AssemblyRelationship = new AssemblyRelationshipType
                    {
                        Value = Domain.Client.AssemblyRelationshipType.Permanent.ToString()
                    },
                    AssemblySpecification = GenerateValidOpMaterialSpecificationTypeArrays()
                        .FirstOrDefault(c => c.Length == i)
                };
            }

            // basic with only mandatory fields
            yield return new OpMaterialSpecificationType
            {
                ID = new IdentifierType
                {
                    Value = "Specification.ID"
                },
                HierarchyScope = HierarchyScopeTypeDataGenerator.GenerateValidHierarchyScopeTypes()
                    .First()
            };
        }

        /// <summary>
        ///     Generates the valid material class identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MaterialClassIDType[]&gt;.</returns>
        private static IEnumerable<MaterialClassIDType[]> GenerateValidMaterialClassIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            yield return null;
            for (var i = 0; i < 3; i++)
            {
                var vals = new MaterialClassIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialClassIDType
                    {
                        Value = $"MaterialClassID.{j}"
                    };
                }
                yield return vals;
            }
        }

        /// <summary>
        ///     Generates the valid material definition identifier type arrays.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MaterialDefinitionIDType[]&gt;.</returns>
        private static IEnumerable<MaterialDefinitionIDType[]> GenerateValidMaterialDefinitionIdTypeArrays()
        {
            // 1, and 2 records
            for (var i = 1; i < 3; i++)
            {
                var vals = new MaterialDefinitionIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new MaterialDefinitionIDType
                    {
                        Value = $"MaterialDefinitionID.{j}"
                    };
                }
                yield return vals;
            }
        }

        #endregion
    }
}