﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class EquipmentClassPropertyTypeDataGenerator.
    /// </summary>
    public static class EquipmentClassPropertyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the invalid equipment property types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassPropertyType&gt;.</returns>
        public static IEnumerable<EquipmentClassPropertyType> GenerateInvalidEquipmentClassPropertyTypes()
        {
            // null ref
            yield return default(EquipmentClassPropertyType);

            // No InternalId
            yield return new EquipmentClassPropertyType();

            // invalid Values
            foreach (var invalidEquipmentClassPropertyType in
                ValueTypeDataGenerator.GenerateInvalidValueTypes()
                    .Where(value => value != default(ValueType))
                    .Select(value => new EquipmentClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = "EquipmentClassProperty.InternalId"
                        },
                        Value = new[]
                        {
                            value
                        }
                    }))
            {
                yield return invalidEquipmentClassPropertyType;
            }
        }

        /// <summary>
        ///     Generates the valid equipment property type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassPropertyType[]&gt;.</returns>
        public static IEnumerable<EquipmentClassPropertyType[]> GenerateValidEquipmentClassPropertyTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentClassPropertyType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentClassPropertyType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"EquipmentClassProperty.InternalId.{i}.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        EquipmentCapabilityTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                            .FirstOrDefault(d => d?.Length == 1),
                        Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                            .FirstOrDefault(v => v?.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid equipment property types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClassPropertyType&gt;.</returns>
        public static IEnumerable<EquipmentClassPropertyType> GenerateValidEquipmentClassPropertyTypes()
        {
            // all values filled, with valid permutations of collections as:
            // empty
            // single
            // multiple
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new EquipmentClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"EquipmentClassProperty.InternalId.{i}"
                    },
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    Value = ValueTypeDataGenerator.GenerateValidValueTypeArrays()
                        .FirstOrDefault(v => v?.Length == i),
                    EquipmentCapabilityTestSpecificationID = GenerateValidTestSpecificationIdTypeArrays()
                        .FirstOrDefault(d => d?.Length == i),
                    EquipmentClassProperty = GenerateValidEquipmentClassPropertyTypeArrays()
                        .FirstOrDefault(o => o?.Length == i)
                };

                // basic mandatory fields only
                yield return new EquipmentClassPropertyType
                {
                    ID = new IdentifierType
                    {
                        Value = $"EquipmentClassProperty.InternalId.{i}"
                    }
                };
            }
        }

        /// <summary>
        ///     Generates the valid test specification identifier type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentCapabilityTestSpecificationIDType[]&gt;.</returns>
        private static IEnumerable<EquipmentCapabilityTestSpecificationIDType[]>
            GenerateValidTestSpecificationIdTypeArrays()
        {
            // null, empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new EquipmentCapabilityTestSpecificationIDType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new EquipmentCapabilityTestSpecificationIDType
                    {
                        Value = $"Test Specification ID.{i}.{j}"
                    };
                }
                yield return vals;
            }
            yield return null;
        }

        #endregion
    }
}