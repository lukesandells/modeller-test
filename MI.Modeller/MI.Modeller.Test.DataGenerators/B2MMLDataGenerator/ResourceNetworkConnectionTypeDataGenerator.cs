﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Modeller.Test.DataGenerators.B2MMLDataGenerator
{
    /// <summary>
    ///     Class ResourceNetworkConnectionTypeDataGenerator.
    /// </summary>
    public static class ResourceNetworkConnectionTypeDataGenerator

    {
        #region Members

        /// <summary>
        ///     Generates the invalid resource network connection types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceNetworkConnectionType&gt;.</returns>
        public static IEnumerable<ResourceNetworkConnectionType> GenerateInvalidResourceNetworkConnectionTypes()
        {
            // null ref
            yield return default(ResourceNetworkConnectionType);

            // invalid fromResourceReferenceField
            foreach (var invalidConnectionType in
                ResourceReferenceTypeDataGenerator.GenerateInvalidResourceReferenceTypes()
                    .Where(value => value != default(ResourceReferenceType))
                    .Select(fromReferenceType => new ResourceNetworkConnectionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "From.Reference.InternalId"
                        },
                        FromResourceReference = fromReferenceType
                    }))
            {
                yield return invalidConnectionType;
            }

            // invalid toResourceReferenceField
            foreach (var invalidConnectionType in
                ResourceReferenceTypeDataGenerator.GenerateInvalidResourceReferenceTypes()
                    .Where(value => value != default(ResourceReferenceType))
                    .Select(fromReferenceType => new ResourceNetworkConnectionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "From.Reference.InternalId"
                        },
                        ToResourceReference = fromReferenceType
                    }))
            {
                yield return invalidConnectionType;
            }

            // invalid property
            foreach (var invalidConnectionType in
                ResourcePropertyTypeDataGenerator.GenerateInvalidResourcePropertyTypes()
                    .Where(value => value != default(ResourcePropertyType))
                    .Select(property => new ResourceNetworkConnectionType
                    {
                        ID = new IdentifierType
                        {
                            Value = "From.Reference.InternalId"
                        },
                        ConnectionProperty = new[]
                        {
                            property
                        }
                    }))
            {
                yield return invalidConnectionType;
            }
        }

        /// <summary>
        ///     Generates the valid resource network connection type arrays.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceNetworkConnectionType[]&gt;.</returns>
        public static IEnumerable<ResourceNetworkConnectionType[]> GenerateValidResourceNetworkConnectionTypeArrays()
        {
            //empty, 1, and 2 records
            for (var i = 3 - 1; i >= 0; i--)
            {
                var vals = new ResourceNetworkConnectionType[i];
                for (var j = 0; j < vals.Length; j++)
                {
                    vals[j] = new ResourceNetworkConnectionType
                    {
                        ID = new IdentifierType
                        {
                            Value = $"PersonnelSpecificationProperty.InternalId.{j}"
                        },
                        Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                            .FirstOrDefault(q => q?.Length == 1),
                        ResourceNetworkConnectionID = new ResourceNetworkConnectionIDType
                        {
                            Value = "Connection.ID"
                        },
                        FromResourceReference = ResourceReferenceTypeDataGenerator.GenerateValidResourceReferenceTypes()
                            .FirstOrDefault(p => p != null),
                        ToResourceReference = ResourceReferenceTypeDataGenerator.GenerateValidResourceReferenceTypes()
                            .FirstOrDefault(p => p != null),
                        ConnectionProperty = ResourcePropertyTypeDataGenerator.GenerateValidResourcePropertyTypeArrays()
                            .FirstOrDefault(p => p.Length == 1)
                    };
                }
                yield return vals;
            }

            // null, 
            yield return null;
        }

        /// <summary>
        ///     Generates the valid resource network connection types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceNetworkConnectionType&gt;.</returns>
        public static IEnumerable<ResourceNetworkConnectionType> GenerateValidResourceNetworkConnectionTypes()
        {
            var id = new IdentifierType
            {
                Value = "NetworkConnection.ID"
            };
            var connectionId = new ResourceNetworkConnectionIDType
            {
                Value = "Connection.InternalId"
            };

            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourceNetworkConnectionType
                {
                    ID = id,
                    ResourceNetworkConnectionID = connectionId,
                    FromResourceReference = ResourceReferenceTypeDataGenerator.GenerateValidResourceReferenceTypes()
                        .FirstOrDefault(e => e != null),
                    ToResourceReference = ResourceReferenceTypeDataGenerator.GenerateValidResourceReferenceTypes()
                        .FirstOrDefault(e => e != null),
                    ConnectionProperty = ResourcePropertyTypeDataGenerator.GenerateValidResourcePropertyTypeArrays()
                        .FirstOrDefault(p => p.Length == i),
                    Description = DescriptionTypeDataGenerator.GenerateValidDescriptionTypeArrays()
                        .FirstOrDefault(d => d?.Length == i)
                };
            }

            // basic mandatory fields only
            yield return new ResourceNetworkConnectionType();
        }

        #endregion
    }
}