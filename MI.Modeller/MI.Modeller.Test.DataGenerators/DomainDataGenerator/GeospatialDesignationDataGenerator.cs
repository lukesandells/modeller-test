﻿using System.Collections.Generic;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class GeospatialDesignationDataGenerator.
    ///     Generate valid SpatialLocation objects
    /// </summary>
    public static class GeospatialDesignationDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid geospatial designations.
        /// </summary>
        /// <returns>IEnumerable&lt;SpatialLocation&gt;.</returns>
        public static IEnumerable<SpatialDefinition> GenerateValidGeospatialDesignations()
        {
            // an object with all values populated
            yield return new SpatialDefinition("Srid",
                "sridAuthority",
                "Format",
                "Value");

            // return an object with minimal data completed
            yield return new SpatialDefinition();
        }

        #endregion
    }
}