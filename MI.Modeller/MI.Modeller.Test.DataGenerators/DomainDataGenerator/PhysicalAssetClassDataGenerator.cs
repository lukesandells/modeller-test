﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class PhysicalAssetClassDataGenerator.
    /// </summary>
    public static class PhysicalAssetClassDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid physicalAsset class collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;PhysicalAssetClass&gt;&gt;.</returns>
        public static IEnumerable<ISet<PhysicalAssetClass>> GenerateValidPhysicalAssetClassCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<PhysicalAssetClass> definitions = new HashSet<PhysicalAssetClass>();
                for (var j = 0; j < i; j++)
                {
                    var newPhysicalAssetClass =
                        new PhysicalAssetClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                                .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                            $"ID.{i}.{j}")
                        {
                            Description = $"Description.{i}.{j}"
                        };
                    definitions.Add(newPhysicalAssetClass);
                    foreach (var manufacturer in GenerateValidIdCollections("Manufacturer.")
                        .First(e => e.Count == 2))
                    {
                        newPhysicalAssetClass.Manufacturers.Add(manufacturer);
                    }
                    foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .First(m => m.Count == 2))
                    {
                        newPhysicalAssetClass.PhysicalAssetClassProperties.Add(property);
                    }
                }
                yield return definitions;
            }
        }

        /// <summary>
        ///     Generates the valid physicalAsset classes.
        /// </summary>
        /// <returns>IEnumerable&lt;PhysicalAssetClass&gt;.</returns>
        public static IEnumerable<PhysicalAssetClass> GenerateValidPhysicalAssetClasses()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newPhysicalAssetClass =
                    new PhysicalAssetClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                        $"ID.{i}")
                    {
                        Description = $"Description.{i}"
                    };
                foreach (var manufacturer in GenerateValidIdCollections("Manufacturer.")
                    .First(e => e.Count == 2))
                {
                    newPhysicalAssetClass.Manufacturers.Add(manufacturer);
                }
                foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                    .First(m => m.Count == i))
                {
                    newPhysicalAssetClass.PhysicalAssetClassProperties.Add(property);
                }
                yield return newPhysicalAssetClass;
            }
            // basic instance with only key fields specified
            yield return new PhysicalAssetClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .First(e => e.EquipmentLevel == EquipmentLevel.Site),
                "physicalAssetClassID");
        }

        /// <summary>
        ///     Generates the valid identifier collections.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        private static IEnumerable<ISet<string>> GenerateValidIdCollections(string prefix)
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> ids = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    ids.Add($"{prefix}.InternalId.{i}.{j}");
                }
                yield return ids;
            }
        }

        #endregion
    }
}