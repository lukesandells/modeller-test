﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    public static class DistributedPropertyDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid distributed properties.
        ///     Most comprehensive object first, so externally firstOrDefault returns this one
        /// </summary>
        /// <returns>IEnumerable&lt;DistributedProperty&gt;.</returns>
        public static IEnumerable<DistributedProperty> GenerateValidDistributedProperties()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newProperty = new DistributedProperty($"DistributedProperty.InternalId.{i}",
                    $"DistributedProperty.Description.{i}",
                    PropertyValueDataGenerator.GenerateValidPropertyValueCollections()
                        .FirstOrDefault(v => v.Count == i));
                var childCollection = GenerateValidDistributedPropertyCollections()
                    .FirstOrDefault(v => v.Count == i);
                if (childCollection != null)
                    foreach (var child in childCollection)
                    {
                        newProperty.AddChild(child);
                    }
                yield return newProperty;
            }

            // basic object with only mandatory fields populated
            yield return new DistributedProperty("DistributedProperty.InternalId");
        }

        /// <summary>
        ///     Generates the valid distributed property collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;DistributedProperty&gt;&gt;.</returns>
        public static IEnumerable<IList<DistributedProperty>>
            GenerateValidDistributedPropertyCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                IList<DistributedProperty> properties = new List<DistributedProperty>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    properties.Add(new DistributedProperty($"Property.InternalId.{i}.{j}",
                        $"Description.{i}.{j}",
                        PropertyValueDataGenerator.GenerateValidPropertyValueCollections()
                            .FirstOrDefault(c => c.Count == 2)));
                }
                yield return properties;
            }
        }

        #endregion
    }
}