﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class MaterialClassDataGenerator.
    /// </summary>
    public static class MaterialClassDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid material class collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;MaterialClass&gt;&gt;.</returns>
        public static IEnumerable<ISet<MaterialClass>> GenerateValidMaterialClassCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<MaterialClass> classs = new HashSet<MaterialClass>();
                for (var j = 0; j < i; j++)
                {
                    var newClass = new MaterialClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(),
                        $"ID.{i}.{j}")
                    {
                        Description = $"Description.{i}.{j}",
                        AssemblyType = AssemblyTypeDataGenerator.GenerateValidAssemblyTypes()
                            .FirstOrDefault(),
                        AssemblyRelationship =
                            AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                                .FirstOrDefault()
                    };
                    foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .First(m => m.Count == 2))
                    {
                        newClass.MaterialClassProperties.Add(property);
                    }
                    classs.Add(newClass);
                }
                yield return classs;
            }
        }

        /// <summary>
        ///     Generates the valid material classs.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialClass&gt;.</returns>
        public static IEnumerable<MaterialClass> GenerateValidMaterialClasss()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newClass = new MaterialClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    $"ID.{i}")
                {
                    Description = $"Description.{i}",
                    AssemblyType = AssemblyTypeDataGenerator.GenerateValidAssemblyTypes()
                        .FirstOrDefault(),
                    AssemblyRelationship =
                        AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                            .FirstOrDefault()
                };
                var propertyCollection = BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                    .FirstOrDefault(m => m.Count == i);
                if (propertyCollection != null)
                {
                    foreach (var property in propertyCollection)
                    {
                        newClass.MaterialClassProperties.Add(property);
                    }
                }
                var parents = GenerateValidMaterialClassCollections()
                    .FirstOrDefault(e => e.Count == i - 1);
                if (parents != null)
                {
                    foreach (var parent in parents)
                    {
                        parent.AddChild(newClass);
                    }
                }
                var children = GenerateValidMaterialClassCollections()
                    .FirstOrDefault(e => e.Count == i + 1);
                if (children != null)
                {
                    foreach (var child in children)
                    {
                        newClass.AddChild(child);
                    }
                }
                foreach (var materialDefinition in
                    MaterialDefinitionDataGenerator.GenerateValidMaterialDefinitionCollections()
                        .First(d => d.Count == 2))
                {
                    newClass.AddMaterialDefinition(materialDefinition);
                }
                yield return newClass;
            }
            // basic instance with only key fields specified
            yield return new MaterialClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(),
                "materialClassID");
        }

        /// <summary>
        ///     Generates the valid identifier collections.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        private static IEnumerable<ISet<string>> GenerateValidIdCollections(string prefix)
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> ids = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    ids.Add($"{prefix}.InternalId.{i}.{j}");
                }
                yield return ids;
            }
        }

        #endregion
    }
}