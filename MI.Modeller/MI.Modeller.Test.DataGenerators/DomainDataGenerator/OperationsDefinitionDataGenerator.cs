﻿// ***********************************************************************
//  Solution    : MI.Modeller
//  FileName    : OperationsDefinitionDataGenerator.cs 
//  Created     : 2016/09/29
//  Modified    : 2016/09/29
// ***********************************************************************

using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class OperationsDefinitionDataGenerator.
    /// </summary>
    public static class OperationsDefinitionDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid operations segment collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsDefinition&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsDefinition>> GenerateValidOperationsDefinitionCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<OperationsDefinition> items = new HashSet<OperationsDefinition>();
                for (var j = 0; j < i; j++)
                {
                    var opsDef = new OperationsDefinition($"OperationsDefinition.InternalId.{i}.{j}",
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(h => h != null),
                        "Version1.0",
                        $"Description.{i}.{j}",
                        OperationsTypeDataGenerator.GenerateValidOperationsTypes()
                            .FirstOrDefault(h => h != null),
                        DateTime.Now,
                        "BOM ID",
                        "Work Definition ID",
                        "Bill Of resources ID",
                        OperationsMaterialBillDataGenerator.GenerateValidOperationsMaterialBillCollections()
                            .FirstOrDefault(s => s.Count == 2));
                    // this will generally be used to make children so dont add children
                    items.Add(opsDef);
                    foreach (var opsSeg in OperationsSegmentDataGenerator.GenerateValidOperationsSegmentCollections()
                            .First(s => s.Count == 2))
                    {
                        opsDef.AddOperationsSegment(opsSeg);
                    }
                }
                yield return items;
            }
        }

        /// <summary>
        ///     Generates the valid operations segments.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsDefinition&gt;.</returns>
        public static IEnumerable<OperationsDefinition> GenerateValidOperationsDefinitions()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var opsDef =new OperationsDefinition($"OperationsDefinition.InternalId.{i}",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    "Version1.0",
                    $"Description.{i}",
                    OperationsTypeDataGenerator.GenerateValidOperationsTypes()
                        .FirstOrDefault(h => h != null),
                    DateTime.Now,
                    "BOM ID",
                    "Work Definition ID",
                    "Bill Of resources ID",
                    OperationsMaterialBillDataGenerator.GenerateValidOperationsMaterialBillCollections()
                        .FirstOrDefault(s => s.Count == i));
                foreach (var opsSeg in OperationsSegmentDataGenerator.GenerateValidOperationsSegmentCollections()
                    .First(s => s.Count == i))
                {
                    opsDef.AddOperationsSegment(opsSeg);
                }
                yield return opsDef;
            }

            // basic object with only mandatory fields populated
            yield return new OperationsDefinition("InternalId",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null));
        }

        #endregion
    }
}