﻿using System.Collections.Generic;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class HierarchyScopeDataGenerator.
    /// </summary>
    public static class HierarchyScopeDataGenerator
    {
        #region Static Fields and Constants

        private static readonly ISet<HierarchyScope> _hierarchyScopes = new HashSet<HierarchyScope>();

        #endregion

        #region Constructors

        static HierarchyScopeDataGenerator()
        {
            var enterprise1 = new HierarchyScope("Enterprise.Id1",
                 EquipmentLevel.Enterprise);
            var site1 = new HierarchyScope("Site.InternalId.1",
                EquipmentLevel.Site,
                enterprise1);
            // ReSharper disable once ObjectCreationAsStatement
            new HierarchyScope("Area.InternalId.1",
                EquipmentLevel.Area,
                site1);
            var site2 = new HierarchyScope("Site.InternalId.2",
                EquipmentLevel.Site,
                enterprise1);
            // ReSharper disable once ObjectCreationAsStatement
            new HierarchyScope("Area.InternalId.2",
                EquipmentLevel.Area,
                site2);
            _hierarchyScopes.Add(enterprise1);

            var enterprise2 = new HierarchyScope("Enterprise.Id2",
                EquipmentLevel.Enterprise);
            var site3 = new HierarchyScope("Site.InternalId.3",
                EquipmentLevel.Site,
                enterprise2);
            // ReSharper disable once ObjectCreationAsStatement
            new HierarchyScope("Area.InternalId.3",
                EquipmentLevel.Area,
                site3);
            _hierarchyScopes.Add(site3);

            var enterprise3 = new HierarchyScope("Enterprise.Id3",
                EquipmentLevel.Enterprise);
            var site4 = new HierarchyScope("Site.InternalId.4",
                EquipmentLevel.Site,
                enterprise3);
            _hierarchyScopes.Add(site4);
        }

        #endregion

        #region Members

        /// <summary>
        ///     Generates the valid hierarchy scopes.
        /// </summary>
        /// <returns>IEnumerable&lt;HierarchyScope&gt;.</returns>
        public static IEnumerable<HierarchyScope> GenerateValidHierarchyScopes()
        {
            //// dont get too many else the number of test cases gets too high
            return _hierarchyScopes;
        }

        #endregion
    }
}