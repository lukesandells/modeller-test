﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class EquipmentDataGenerator.
    /// </summary>
    public static class EquipmentDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;Equipment&gt;&gt;.</returns>
        public static IEnumerable<ISet<Equipment>> GenerateValidEquipmentCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<Equipment> definitions = new HashSet<Equipment>();
                for (var j = 0; j < i; j++)
                {
                    var newEquipment = new Equipment(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                        $"ID.{i}.{j}",
                        EquipmentLevelTypeDataGenerator.GenerateValidEquipmentLevelTypes()
                            .FirstOrDefault(e => e == EquipmentLevel.Area))
                    {
                        Description = $"Description.{i}.{j}"
                    };
                    definitions.Add(newEquipment);
                    foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .First(m => m.Count == 2))
                    {
                        newEquipment.EquipmentProperties.Add(property);
                    }
                    foreach (var equipmentClass in EquipmentClassDataGenerator.GenerateValidEquipmentClassCollections()
                        .First(m => m.Count == 2))
                    {
                        newEquipment.AddEquipmentClass(equipmentClass);
                    }
                }
                yield return definitions;
            }
        }

        /// <summary>
        ///     Generates the valid equipments.
        /// </summary>
        /// <returns>IEnumerable&lt;Equipment&gt;.</returns>
        public static IEnumerable<Equipment> GenerateValidEquipments()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newEquipment = new Equipment(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                    $"ID.{i}",
                    EquipmentLevelTypeDataGenerator.GenerateValidEquipmentLevelTypes()
                        .FirstOrDefault(e => e == EquipmentLevel.Area))
                {
                    Description = $"Description.{i}"
                };
                foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                    .First(m => m.Count == i))
                {
                    newEquipment.EquipmentProperties.Add(property);
                }
                foreach (var equipmentClass in EquipmentClassDataGenerator.GenerateValidEquipmentClassCollections()
                    .First(e => e.Count == i))
                {
                    newEquipment.AddEquipmentClass(equipmentClass);
                }
                var children = GenerateValidEquipmentCollections()
                    .FirstOrDefault(e => e.Count == i);
                if (children != null)
                {
                    foreach (var child in children)
                    {
                        newEquipment.AddChild(child);
                    }
                }
                yield return newEquipment;
            }
            // basic instance with only key fields specified
            yield return new Equipment(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                "equipmentID",
                EquipmentLevelTypeDataGenerator.GenerateValidEquipmentLevelTypes()
                    .FirstOrDefault(e => e == EquipmentLevel.Area));
        }

        #endregion
    }
}