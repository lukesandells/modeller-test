﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class WorkDefinitionTypeDataGenerator.
    /// </summary>
    public static class WorkDefinitionTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid work definition types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkDefinitionType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<WorkDefinitionType>> GenerateValidWorkDefinitionTypes() => from WorkDefinitionType value in Enum
                .GetValues(typeof(WorkDefinitionType))
            select ExtensibleEnum<WorkDefinitionType>.Parse(value == WorkDefinitionType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}