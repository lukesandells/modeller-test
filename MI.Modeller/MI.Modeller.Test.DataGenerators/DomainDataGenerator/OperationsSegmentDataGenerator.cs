﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class OperationsSegmentDataGenerator.
    /// </summary>
    public static class OperationsSegmentDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid operations segment collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsSegment&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsSegment>> GenerateValidOperationsSegmentCollections()
        {
            var opsDef = new OperationsDefinition(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(h => h != null), "OpsDef");
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<OperationsSegment> items = new HashSet<OperationsSegment>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    var opsSeg = new OperationsSegment($"OperationsSegment.InternalId.{i}.{j}",
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(h => h != null),
                        $"Description.{i}.{j}",
                        OperationsTypeDataGenerator.GenerateValidOperationsTypes()
                            .FirstOrDefault(h => h != null),
                        new TimeSpan(2,
                            5,
                            4,
                            33),
                        GenerateValidProcessSegmentIdCollections()
                            .FirstOrDefault(s => s.Count == 2),
                        BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .FirstOrDefault(p => p.Count == 2),
                        SegmentDependencyDataGenerator.GenerateValidSegmentDependencyCollections()
                            .FirstOrDefault(s => s.Count == 2));
                    items.Add(opsSeg);
                    opsDef.AddOperationsSegment(opsSeg);
                    foreach (
                        var materialSpec in
                        MaterialSpecificationDataGenerator.GenerateValidMaterialSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                     opsSeg.AddMaterialSpecification(materialSpec);   
                    }
                    foreach (
                        var personnelSpec in
                        PersonnelSpecificationDataGenerator.GenerateValidPersonnelSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        opsSeg.AddPersonnelSpecification(personnelSpec);
                    }
                    foreach (
                        var equipmentSpec in
                        EquipmentSpecificationDataGenerator.GenerateValidEquipmentSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        opsSeg.AddEquipmentSpecification(equipmentSpec);
                    }
                    foreach (
                        var assetSpec in
                        PhysicalAssetSpecificationDataGenerator.GenerateValidPhysicalAssetSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        opsSeg.AddPhysicalAssetSpecification(assetSpec);
                    }
                }
                yield return items;
            }
        }

        /// <summary>
        ///     Generates the valid operations segments.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsSegment&gt;.</returns>
        public static IEnumerable<OperationsSegment> GenerateValidOperationsSegments()
        {
            var opsDef = new OperationsDefinition(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(h => h != null), "OpsDef");
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var opsSegment = new OperationsSegment($"OperationsSegment.InternalId.{i}",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    $"Description.{i}",
                    OperationsTypeDataGenerator.GenerateValidOperationsTypes()
                        .FirstOrDefault(h => h != null),
                    new TimeSpan(2,
                        5,
                        4,
                        33),
                    GenerateValidProcessSegmentIdCollections()
                        .FirstOrDefault(s => s.Count == i),
                    BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault(p => p.Count == i),
                    SegmentDependencyDataGenerator.GenerateValidSegmentDependencyCollections()
                        .FirstOrDefault(s => s.Count == 2));
                opsDef.AddOperationsSegment(opsSegment);
                foreach (
                        var materialSpec in
                        MaterialSpecificationDataGenerator.GenerateValidMaterialSpecificationCollections()
                            .First(s => s.Count == 2))
                {
                    opsSegment.AddMaterialSpecification(materialSpec);
                }
                foreach (
                    var personnelSpec in
                    PersonnelSpecificationDataGenerator.GenerateValidPersonnelSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    opsSegment.AddPersonnelSpecification(personnelSpec);
                }
                foreach (
                    var equipmentSpec in
                    EquipmentSpecificationDataGenerator.GenerateValidEquipmentSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    opsSegment.AddEquipmentSpecification(equipmentSpec);
                }
                foreach (
                    var assetSpec in
                    PhysicalAssetSpecificationDataGenerator.GenerateValidPhysicalAssetSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    opsSegment.AddPhysicalAssetSpecification(assetSpec);
                }
                var children = GenerateValidOperationsSegmentCollections()
                    .FirstOrDefault(s => s.Count == i);
                if (children != null)
                    foreach (var child in children)
                {
                    opsSegment.AddChild(child);
                }
            }

            // basic object with only mandatory fields populated
            yield return new OperationsSegment("InternalId",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null));
        }

        /// <summary>
        ///     Generates the valid process segment identifier collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        private static IEnumerable<ISet<string>> GenerateValidProcessSegmentIdCollections()
        {
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> vals = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    vals.Add($"processSegmentID{i}.{j}");
                }
                yield return vals;
            }
        }

        #endregion
    }
}