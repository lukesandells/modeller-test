﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ResourceRelationshipTypeDataGenerator.
    /// </summary>
    public static class ResourceRelationshipTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid resource reference types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceRelationshipType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<ResourceRelationshipType>> GenerateValidResourceRelationshipTypes() => from ResourceRelationshipType value in Enum
                .GetValues(typeof(ResourceRelationshipType))
            select ExtensibleEnum<ResourceRelationshipType>.Parse(value == ResourceRelationshipType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}