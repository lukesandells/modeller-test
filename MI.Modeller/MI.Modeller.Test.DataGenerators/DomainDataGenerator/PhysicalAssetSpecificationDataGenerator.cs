﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class PhysicalAssetSpecificationDataGenerator.
    ///     Generate valid PhysicalAssetSpecification items for testing
    ///     Most comprehensive object first, so externally firstOrDefault returns this one
    /// </summary>
    public static class PhysicalAssetSpecificationDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid physical asset specification collections.
        /// </summary>
        /// <returns>
        ///     System.Collections.Generic.IEnumerable&lt;System.Collections.Generic.ISet&lt;
        ///     MI.Modeller.Domain.Client.PhysicalAssetSpecification&gt;&gt;.
        /// </returns>
        public static IEnumerable<ISet<PhysicalAssetSpecification>> GenerateValidPhysicalAssetSpecificationCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<PhysicalAssetSpecification> specifications = new HashSet<PhysicalAssetSpecification>();
                for (var j = 0; j < i; j++)
                {
                    var newSpec =
                        new PhysicalAssetSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                                .FirstOrDefault(),
                            $"MasterID.{i}.{j}")
                        {
                            PhysicalAssetClass = PhysicalAssetClassDataGenerator.GenerateValidPhysicalAssetClasses()
                                .FirstOrDefault(),
                            Description = $"Description {i}.{j}",
                            Use = $"Use.{i}.{j}",
                            SpatialLocation = GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                                .FirstOrDefault()
                        };
                    specifications.Add(newSpec);
                    foreach (var quantity in QuantityDataGenerator.GenerateValidQuantityCollections()
                        .First(q => q.Count == 2))
                    {
                        newSpec.Quantities.Add(quantity);
                    }
                    foreach (
                        var property in DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                            .First(p => p.Count == 2))
                    {
                        newSpec.SpecificationProperties.Add(property);
                    }
                }
                yield return specifications;
            }
        }

        /// <summary>
        ///     Generates the valid physical asset specifications.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MI.Modeller.Domain.Client.PhysicalAssetSpecification&gt;.</returns>
        public static IEnumerable<PhysicalAssetSpecification> GenerateValidPhysicalAssetSpecifications()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newSpec = new PhysicalAssetSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    "MasterId")
                {
                    PhysicalAssetClass = PhysicalAssetClassDataGenerator.GenerateValidPhysicalAssetClasses()
                        .FirstOrDefault(),
                    Description = "Description",
                    Use = "physicalAssetUse",
                    SpatialLocation = GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                        .FirstOrDefault()
                };
                var quantities = QuantityDataGenerator.GenerateValidQuantityCollections()
                    .FirstOrDefault(q => q.Count == i);
                if (quantities != null)
                {
                    foreach (var quantity in quantities)
                    {
                        newSpec.Quantities.Add(quantity);
                    }
                }
                var properties = DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                    .FirstOrDefault(p => p.Count == i);
                if (properties != null)
                {
                    foreach (var property in properties)
                    {
                        newSpec.SpecificationProperties.Add(property);
                    }
                }
                yield return newSpec;
            }
            // basic instance with only key fields specified
            yield return new PhysicalAssetSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(),
                "MasterId")
            {
                PhysicalAssetClass = PhysicalAssetClassDataGenerator.GenerateValidPhysicalAssetClasses()
                    .FirstOrDefault()
            };
        }

        #endregion
    }
}