﻿using System.Collections.Generic;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    public static class UnitOfMeasureDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid unit of measures.
        /// </summary>
        /// <returns>IEnumerable&lt;UnitOfMeasure&gt;.</returns>
        public static IEnumerable<UnitOfMeasure> GenerateValidUnitOfMeasures()
        {
            yield return new UnitOfMeasure("To");
        }

        #endregion
    }
}