﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    public static class WorkMasterDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid personnel specification properties.
        ///     Most comprehensive object first, so externally firstOrDefault returns this one
        /// </summary>
        /// <returns>IEnumerable&lt;WorkMaster&gt;.</returns>
        public static IEnumerable<WorkMaster> GenerateValidWorkMasters()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var children = new HashSet<WorkMaster>
                {
                    new WorkMaster($"Child.InternalId {i}",
                        hierarchyScope: HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault())
                };
                var workMaster = new WorkMaster($"WorkMaster.InternalId.{i}",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    $"Version.{i}",
                    WorkTypeDataGenerator.GenerateValidWorkTypes()
                        .FirstOrDefault(),
                    WorkDefinitionTypeDataGenerator.GenerateValidWorkDefinitionTypes()
                        .FirstOrDefault(),
                    new TimeSpan(1,
                        23,
                        45,
                        55),
                    DateTime.Today,
                    $"OperationsDefinitionId.{i}",
                    $"operationsSegmentId.{i}",
                    $"Description {i}",
                    BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault(p => p.Count == i));
                foreach (
                        var materialSpec in
                        MaterialSpecificationDataGenerator.GenerateValidMaterialSpecificationCollections()
                            .First(s => s.Count == 2))
                {
                    workMaster.AddMaterialSpecification(materialSpec);
                }
                foreach (
                    var personnelSpec in
                    PersonnelSpecificationDataGenerator.GenerateValidPersonnelSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    workMaster.AddPersonnelSpecification(personnelSpec);
                }
                foreach (
                    var equipmentSpec in
                    EquipmentSpecificationDataGenerator.GenerateValidEquipmentSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    workMaster.AddEquipmentSpecification(equipmentSpec);
                }
                foreach (
                    var assetSpec in
                    PhysicalAssetSpecificationDataGenerator.GenerateValidPhysicalAssetSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    workMaster.AddPhysicalAssetSpecification(assetSpec);
                }
                foreach (
                    var workflowSpec in
                    WorkflowSpecificationDataGenerator.GenerateValidWorkflowSpecificationCollections()
                        .First(w => w.Count == i))
                {
                    workMaster.AddWorkflowSpecification(workflowSpec);
                }
                foreach (var child in children)
                {
                    workMaster.AddChild(child);
                }
                yield return workMaster;
            }

            // basic object with only mandatory fields populated
            yield return new WorkMaster("WorkMaster.InternalId",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null));
        }

        /// <summary>
        ///     Generates the valid identifier collections.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        private static IEnumerable<ISet<string>> GenerateValidIdCollections(string prefix)
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> ids = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    ids.Add($"{prefix}.InternalId.{i}.{j}");
                }
                yield return ids;
            }
        }

        #endregion
    }
}