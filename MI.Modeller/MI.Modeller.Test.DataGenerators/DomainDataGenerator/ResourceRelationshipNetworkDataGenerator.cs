﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ResourceRelationshipNetworkDataGenerator.
    /// </summary>
    public static class ResourceRelationshipNetworkDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid resource relationship networks.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceRelationshipNetwork&gt;.</returns>
        public static IEnumerable<ResourceRelationshipNetwork> GenerateValidResourceRelationshipNetworks()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourceRelationshipNetwork("ResourceRelationshipNetwork.InternalId",
                    "Description",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    ResourceRelationshipTypeDataGenerator.GenerateValidResourceRelationshipTypes()
                        .FirstOrDefault(r => r != null),
                    ResourceRelationshipFormTypeDataGenerator.GenerateValidResourceRelationshipFormTypes()
                        .FirstOrDefault(r => r != null),
                    DateTime.Today,
                    ResourceNetworkConnectionDataGenerator.GenerateValidResourceNetworkConnectionCollections()
                        .FirstOrDefault(n => n.Count == 2));
            }

            // basic object with only mandatory fields populated
            yield return new ResourceRelationshipNetwork();
        }

        #endregion
    }
}