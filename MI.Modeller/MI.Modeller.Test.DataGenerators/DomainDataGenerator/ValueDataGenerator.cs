﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;
using MI.Modeller.Domain.Client.DataTypes;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ValueDataGenerator.
    ///     Most comprehensive object first, so externally firstOrDefault returns this one
    /// </summary>
    public static class ValueDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid value collections.
        ///     Most comprehensive object first, so externally firstOrDefault returns this one
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;Value&gt;&gt;.</returns>
        public static IEnumerable<IList<Value>> GenerateValidValueCollections()
        {
            // dont get too many else the number of test cases gets too high
            for (var i = 3; i >= 0; i--)
            {
                IList<Value> values = new List<Value>();
                for (var j = 0; j < i; j++)
                {
                    values.Add(new Value($"Value.Key.{j}"));
                }
                yield return values;
            }
        }

        /// <summary>
        ///     Generates the valid values.
        /// </summary>
        /// <returns>IEnumerable&lt;Value&gt;.</returns>
        public static IEnumerable<Value> GenerateValidValues()
        {
            // all values populated
            foreach (var uncertainty in UncertaintyDataGenerator.GenerateValidUncertainties())
            {
                yield return new Value("Key",
                    "ValueString",
                    DataTypeDataGenerator.GenerateValidDataTypes()
                        .First(),
                    UnitOfMeasureDataGenerator.GenerateValidUnitOfMeasures()
                        .First(),
                    uncertainty);
            }

            yield return new Value();
        }

        #endregion
    }
}