﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class PropertyValueDataGenerator.
    ///     Most comprehensive object first, so externally firstOrDefault returns this one
    /// </summary>
    public class PropertyValueDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid values.
        /// </summary>
        /// <returns>IEnumerable&lt;Value&gt;.</returns>
        public static IEnumerable<PropertyValue> GenerateValidPropertyValues()
        {
            // Just value
            yield return new PropertyValue(ValueDataGenerator.GenerateValidValueCollections()
                    .First(v => v.Count == 1),
                new List<Quantity>());
            // Just quantity
            yield return new PropertyValue(new List<Value>(),
                QuantityDataGenerator.GenerateValidQuantityCollections()
                    .First(v => v.Count == 1));
            // all values populated
            yield return new PropertyValue(ValueDataGenerator.GenerateValidValueCollections()
                    .First(v => v.Count == 1),
                QuantityDataGenerator.GenerateValidQuantityCollections()
                    .First(v => v.Count == 1));

            yield return new PropertyValue(new List<Value>(),
                new List<Quantity>());
        }

        /// <summary>
        ///     Generates the valid value collections.
        ///     Most comprehensive object first, so externally firstOrDefault returns this one
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;Value&gt;&gt;.</returns>
        public static IEnumerable<IList<PropertyValue>> GenerateValidPropertyValueCollections()
        {
            // Empty
            yield return new List<PropertyValue>();
            // Just values
            for (var i = 3; i >= 0; i--)
            {
                IList<PropertyValue> propertyValues = new List<PropertyValue>();
                for (var j = 0; j < i; j++)
                {
                    propertyValues.Add(new PropertyValue(ValueDataGenerator.GenerateValidValueCollections()
                            .First(v => v.Count == i),
                        new List<Quantity>()));
                }
                yield return propertyValues;
            }
            // Just quantities
            for (var i = 3; i >= 0; i--)
            {
                IList<PropertyValue> propertyValues = new List<PropertyValue>();
                for (var j = 0; j < i; j++)
                {
                    propertyValues.Add(new PropertyValue(new List<Value>(),
                        QuantityDataGenerator.GenerateValidQuantityCollections()
                            .First(q => q.Count == i)));
                }
                yield return propertyValues;
            }
            //both
            // dont get too many else the number of test cases gets too high
            for (var i = 3; i >= 0; i--)
            {
                IList<PropertyValue> propertyValues = new List<PropertyValue>();
                for (var j = 0; j < i; j++)
                {
                    propertyValues.Add(new PropertyValue(ValueDataGenerator.GenerateValidValueCollections()
                            .First(v => v.Count == i),
                        QuantityDataGenerator.GenerateValidQuantityCollections()
                            .First(q => q.Count == i)));
                }
                yield return propertyValues;
            }
        }

        #endregion
    }
}