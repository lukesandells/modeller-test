﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class RootModelObjectDataGenerator.
    /// </summary>
    public class RootModelObjectDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid objects.
        /// </summary>
        /// <returns>IEnumerable&lt;ModelObject&gt;.</returns>
        public static IEnumerable<RootModelObject> GenerateValidRootModelObjects()
        {
            yield return EquipmentDataGenerator.GenerateValidEquipments()
                .FirstOrDefault();
            yield return EquipmentClassDataGenerator.GenerateValidEquipmentClasses()
                .FirstOrDefault();
            yield return HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                .FirstOrDefault();
            yield return MaterialDefinitionDataGenerator.GenerateValidMaterialDefinitions()
                .FirstOrDefault();
            yield return MaterialClassDataGenerator.GenerateValidMaterialClasss()
                .FirstOrDefault();
            yield return OperationsDefinitionDataGenerator.GenerateValidOperationsDefinitions()
                .FirstOrDefault();
            yield return ProcessSegmentDataGenerator.GenerateValidProcessSegments()
                .FirstOrDefault();
            yield return WorkMasterDataGenerator.GenerateValidWorkMasters()
                .FirstOrDefault();
        }

        #endregion
    }
}