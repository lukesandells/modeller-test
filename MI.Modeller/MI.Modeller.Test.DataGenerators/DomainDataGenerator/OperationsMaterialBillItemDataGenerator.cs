﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class OperationsMaterialBillItemDataGenerator.
    /// </summary>
    public static class OperationsMaterialBillItemDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid operations material bill item collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsMaterialBillItem&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsMaterialBillItem>>
            GenerateValidOperationsMaterialBillItemCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<OperationsMaterialBillItem> resourceReferences = new HashSet<OperationsMaterialBillItem>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    resourceReferences.Add(new OperationsMaterialBillItem($"OperationsMaterialBillItem.ID{i}.{j}",
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes().First(),
                        "Description",
                        "MaterialClassID",
                        "MaterialDefinitionID",
                        "Use Type",
                        assemblyType: AssemblyTypeDataGenerator.GenerateValidAssemblyTypes()
                            .FirstOrDefault(v => v != null),
                        assemblyRelationshipType:
                        AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                            .FirstOrDefault(v => v != null),
                        materialSpecificationIDs: GenerateValidMaterialSpecificationIdCollections()
                            .FirstOrDefault(v => v != null),
                        quantities: QuantityDataGenerator.GenerateValidQuantityCollections()
                            .FirstOrDefault(v => v != null)));
                }
                yield return resourceReferences;
            }
        }

        /// <summary>
        ///     Generates the valid operations material bill items.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBillItem&gt;.</returns>
        public static IEnumerable<OperationsMaterialBillItem> GenerateValidOperationsMaterialBillItems()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var opsBill =  new OperationsMaterialBillItem($"OperationsMaterialBillItem.ID{i}",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes().First(),
                    "Description",
                    "MaterialClassID",
                    "MaterialDefinitionID",
                    "Use Type",
                    null,
                    AssemblyTypeDataGenerator.GenerateValidAssemblyTypes()
                        .FirstOrDefault(v => v != null),
                    AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                        .FirstOrDefault(v => v != null),
                    GenerateValidMaterialSpecificationIdCollections()
                        .FirstOrDefault(v => v.Count == i),
                    QuantityDataGenerator.GenerateValidQuantityCollections()
                        .FirstOrDefault(v => v.Count == i));
                var operationsMaterialBillItems = GenerateValidOperationsMaterialBillItemCollections()
                    .FirstOrDefault(v => v.Count == i);
                if (operationsMaterialBillItems == null)
                {
                    yield return opsBill;
                }
                if (operationsMaterialBillItems != null)
                    foreach (var child in operationsMaterialBillItems)
                    {
                        opsBill.AddChild(child);
                    }
                yield return opsBill;
            }


            // basic object with only mandatory fields populated
            yield return new OperationsMaterialBillItem("OperationsMaterialBillItem.InternalId", HierarchyScopeDataGenerator.GenerateValidHierarchyScopes().First());
        }

        private static IEnumerable<ISet<string>> GenerateValidMaterialSpecificationIdCollections()
        {
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> vals = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    vals.Add($"MaterialSpecificationID1.ID{i}.{j}");
                }
                yield return vals;
            }
        }

        #endregion
    }
}