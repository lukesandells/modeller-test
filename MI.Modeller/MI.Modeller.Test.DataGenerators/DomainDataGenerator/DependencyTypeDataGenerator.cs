﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class DependencyTypeDataGenerator.
    ///     Returns DependencyTypeValue objects for all valid values
    /// </summary>
    public static class DependencyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid dependency types.
        /// </summary>
        /// <returns>IEnumerable&lt;DependencyType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<Domain.Client.DependencyType>> GenerateValidDependencyTypes() => from Domain.Client.DependencyType value in Enum
                .GetValues(typeof(Domain.Client.DependencyType))
            select ExtensibleEnum<Domain.Client.DependencyType>.Parse(value == Domain.Client.DependencyType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}