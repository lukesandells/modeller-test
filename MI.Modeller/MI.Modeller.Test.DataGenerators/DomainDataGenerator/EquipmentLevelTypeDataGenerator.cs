﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class EquipmentLevelTypeDataGenerator.
    ///     Returns EquipmentLevelTypeValue objects for all valid values
    /// </summary>
    public static class EquipmentLevelTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment level types.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentLevelType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<EquipmentLevel>> GenerateValidEquipmentLevelTypes() => from EquipmentLevel value in Enum
                .GetValues(typeof(EquipmentLevel))
            where value != EquipmentLevel.Enterprise
            select ExtensibleEnum<EquipmentLevel>.Parse(value == EquipmentLevel.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}