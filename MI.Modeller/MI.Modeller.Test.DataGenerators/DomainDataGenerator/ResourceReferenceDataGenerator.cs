﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ResourceReferenceDataGenerator.
    /// </summary>
    public static class ResourceReferenceDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid resource reference collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;ResourceReference&gt;&gt;.</returns>
        public static IEnumerable<ISet<ResourceReference>> GenerateValidResourceReferenceCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<ResourceReference> resourceReferences = new HashSet<ResourceReference>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    resourceReferences.Add(new ResourceReference($"InternalId.{i}.{j}",
                        $"ResourceId.InternalId.{i}.{j}",
                        Domain.Client.ResourceReferenceType.Equipment,
						BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .FirstOrDefault((IList<BasicProperty> v) => v.Count == 2)));
                }
                yield return resourceReferences;
            }
        }

        /// <summary>
        ///     Generates the valid resource references.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReference&gt;.</returns>
        public static IEnumerable<ResourceReference> GenerateValidResourceReferences()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourceReference("ResourceReference.InternalId",
                    "ResourceReference.ResourceId",
                   Domain.Client.ResourceReferenceType.Equipment,
					BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault((IList<BasicProperty> v) => v.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new ResourceReference();
        }

        #endregion
    }
}