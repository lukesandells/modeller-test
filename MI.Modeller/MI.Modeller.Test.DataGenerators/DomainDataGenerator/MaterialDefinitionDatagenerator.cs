﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class MaterialDefinitionDataGenerator.
    /// </summary>
    public static class MaterialDefinitionDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid material definition collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;MaterialDefinition&gt;&gt;.</returns>
        public static IEnumerable<ISet<MaterialDefinition>> GenerateValidMaterialDefinitionCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<MaterialDefinition> definitions = new HashSet<MaterialDefinition>();
                for (var j = 0; j < i; j++)
                {
                    var definition = new MaterialDefinition(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(),
                        $"ID.{i}.{j}")
                    {
                        Description = $"Description.{i}.{j}",
                        AssemblyType = AssemblyTypeDataGenerator.GenerateValidAssemblyTypes()
                            .FirstOrDefault(),
                        AssemblyRelationship =
                            AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                                .FirstOrDefault()
                    };
                    foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .First(m => m.Count == 2))
                    {
                        definition.MaterialDefinitionProperties.Add(property);
                    }

                    definitions.Add(definition);
                }
                yield return definitions;
            }
        }

        /// <summary>
        ///     Generates the valid material definitions.
        /// </summary>
        /// <returns>IEnumerable&lt;MaterialDefinition&gt;.</returns>
        public static IEnumerable<MaterialDefinition> GenerateValidMaterialDefinitions()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var materialDefinition =
                    new MaterialDefinition(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(),
                        $"ID.{i}")
                    {
                        Description = $"Description.{i}",
                        AssemblyType = AssemblyTypeDataGenerator.GenerateValidAssemblyTypes()
                            .FirstOrDefault(),
                        AssemblyRelationship =
                            AssemblyRelationshipTypeDataGenerator.GenerateValidAssemblyRelationshipTypes()
                                .FirstOrDefault()
                    };
                foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                    .First(m => m.Count == 2))
                {
                    materialDefinition.MaterialDefinitionProperties.Add(property);
                }
                var materialClasses = MaterialClassDataGenerator.GenerateValidMaterialClassCollections()
                    .FirstOrDefault(e => e.Count == i);
                if (materialClasses != null)
                {
                    foreach (var materialClass in materialClasses)
                    {
                        materialDefinition.AddMaterialClass(materialClass);
                    }
                }
                var parents = GenerateValidMaterialDefinitionCollections()
                    .FirstOrDefault(e => e.Count == i - 1);
                if (parents != null)
                {
                    foreach (var parent in parents)
                    {
                        parent.AddChild(materialDefinition);
                    }
                }
                var children = GenerateValidMaterialDefinitionCollections()
                    .FirstOrDefault(e => e.Count == i + 1);
                if (children != null)
                {
                    foreach (var child in children)
                    {
                        materialDefinition.AddChild(child);
                    }
                }
                yield return materialDefinition;
            }
            // basic instance with only key fields specified
            yield return new MaterialDefinition(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(),
                "materialDefinitionID");
        }

        /// <summary>
        ///     Generates the valid identifier collections.
        /// </summary>
        /// <param name="prefix">The prefix.</param>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        private static IEnumerable<ISet<string>> GenerateValidIdCollections(string prefix)
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> ids = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    ids.Add($"{prefix}.InternalId.{i}.{j}");
                }
                yield return ids;
            }
        }

        #endregion
    }
}