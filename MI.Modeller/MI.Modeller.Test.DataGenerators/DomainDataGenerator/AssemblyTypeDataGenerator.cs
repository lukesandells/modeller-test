﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class AssemblyTypeDataGenerator.
    /// </summary>
    public static class AssemblyTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid assembly types.
        /// </summary>
        /// <returns>IEnumerable&lt;AssemblyType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<AssemblyType>> GenerateValidAssemblyTypes() => from AssemblyType value in Enum
                .GetValues(typeof(AssemblyType))
            select ExtensibleEnum<AssemblyType>.Parse(value == AssemblyType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}