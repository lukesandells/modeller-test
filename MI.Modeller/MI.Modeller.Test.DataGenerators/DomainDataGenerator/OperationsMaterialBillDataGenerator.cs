﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class OperationsMaterialBillDataGenerator.
    /// </summary>
    public static class OperationsMaterialBillDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid operations material bill collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;OperationsMaterialBill&gt;&gt;.</returns>
        public static IEnumerable<ISet<OperationsMaterialBill>> GenerateValidOperationsMaterialBillCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<OperationsMaterialBill> items = new HashSet<OperationsMaterialBill>();
                for (var j = 0; j < i; j++)
                {
                    items.Add(new OperationsMaterialBill($"OperationsMaterialBill.ID{i}.{j}",
                        "Description",
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(v => v != null),
                        OperationsMaterialBillItemDataGenerator.GenerateValidOperationsMaterialBillItemCollections()
                            .FirstOrDefault(e => e.Count == 2)));
                }
                yield return items;
            }
        }

        /// <summary>
        ///     Generates the valid operations material bills.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsMaterialBill&gt;.</returns>
        public static IEnumerable<OperationsMaterialBill> GenerateValidOperationsMaterialBills()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new OperationsMaterialBill($"OperationsMaterialBill.ID{i}",
                    "Description",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(v => v != null),
                    OperationsMaterialBillItemDataGenerator.GenerateValidOperationsMaterialBillItemCollections()
                        .FirstOrDefault(e => e.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new OperationsMaterialBill("OperationsMaterialBill.InternalId");
        }

        #endregion
    }
}