﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class QuantityDataGenerator.
    /// </summary>
    public static class QuantityDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid quantities.
        ///     Most comprehensive object first, so externally firstOrDefault returns this one
        /// </summary>
        /// <returns>IEnumerable&lt;Quantity&gt;.</returns>
        public static IEnumerable<Quantity> GenerateValidQuantities()
        {
            // dont get too many else the number of test cases gets too high
            // All objects populated
            foreach (var uncertainty in UncertaintyDataGenerator.GenerateValidUncertainties())
            {
                yield return new Quantity("Key",
                    "QuantityString",
                    DataTypeDataGenerator.GenerateValidDataTypes()
                        .First(),
                    UnitOfMeasureDataGenerator.GenerateValidUnitOfMeasures()
                        .First(),
                    uncertainty);
            }

            // basic object
            yield return new Quantity();
        }

        /// <summary>
        ///     Generates the valid quantity collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;Quantity&gt;&gt;.</returns>
        public static IEnumerable<IList<Quantity>> GenerateValidQuantityCollections()
        {
            // dont get too many else the number of test cases gets too high
            for (var i = 3 - 1; i >= 0; i--)
            {
                IList<Quantity> quantities = new List<Quantity>();
                for (var j = 0; j < i; j++)
                {
                    quantities.Add(new Quantity($"Quantity.Key.{j}"));
                }
                yield return quantities;
            }
        }

        #endregion
    }
}