﻿using System.Collections.Generic;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    public class UncertaintyDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid uncertainties.
        /// </summary>
        /// <returns>IEnumerable&lt;Uncertainty&gt;.</returns>
        public static IEnumerable<Uncertainty> GenerateValidUncertainties()
        {
            yield return new Uncertainty(95,
                80,
                75,
                null,
                null);
            yield return new Uncertainty(null,
                80,
                75,
                null,
                null);
            yield return new Uncertainty(95,
                null,
                75,
                null,
                null);
            yield return new Uncertainty(95,
                80,
                null,
                null,
                null);
            yield return new Uncertainty(null,
                null,
                null,
                95,
                90);
            yield return new Uncertainty(null,
                null,
                null,
                null,
                90);
            yield return new Uncertainty(null,
                null,
                null,
                95,
                null);

            yield return new Uncertainty(null,
                null,
                null,
                null,
                null);
        }

        #endregion
    }
}