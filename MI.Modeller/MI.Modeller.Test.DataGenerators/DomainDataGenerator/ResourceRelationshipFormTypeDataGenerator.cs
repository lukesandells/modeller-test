﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ResourceRelationshipFormTypeDataGenerator.
    /// </summary>
    public static class ResourceRelationshipFormTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid resource reference types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceRelationshipFormType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<ResourceRelationshipFormType>> GenerateValidResourceRelationshipFormTypes() => from ResourceRelationshipFormType value in Enum
                .GetValues(typeof(ResourceRelationshipFormType))
            select ExtensibleEnum<ResourceRelationshipFormType>.Parse(value == ResourceRelationshipFormType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}