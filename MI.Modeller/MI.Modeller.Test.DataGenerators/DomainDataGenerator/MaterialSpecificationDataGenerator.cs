﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

// ReSharper disable ObjectCreationAsStatement

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class MaterialSpecificationDataGenerator.
    ///     Generate valid MaterialSpecification items for testing
    ///     Most comprehensive object first, so externally firstOrDefault returns this one
    /// </summary>
    public static class MaterialSpecificationDataGenerator
    {
        #region Static Fields and Constants

        private static readonly ISet<ISet<MaterialSpecification>> _materialSpecificationCollections =
            new HashSet<ISet<MaterialSpecification>>();

        private static readonly ISet<MaterialSpecification> _materialSpecifications =
            new HashSet<MaterialSpecification>();

        #endregion

        #region Constructors

        static MaterialSpecificationDataGenerator()
        {
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<MaterialSpecification> specifications = new HashSet<MaterialSpecification>();
                for (var j = 0; j < i; j++)
                {
                    var newSpec = new MaterialSpecification(null,
                        $"MasterID.{i}.{j}",
                        null,
                        MaterialDefinitionDataGenerator.GenerateValidMaterialDefinitions()
                            .FirstOrDefault(),
                        Domain.Client.MaterialUseType.Produced,
                        GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                            .FirstOrDefault(),
                        MaterialAssemblyType.Physical,
                        Domain.Client.AssemblyRelationshipType.Permanent,
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(),
                        "Description",
                        QuantityDataGenerator.GenerateValidQuantityCollections()
                            .FirstOrDefault(q => q.Count == i),
                        DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                            .FirstOrDefault(p => p.Count == i));
                    for (var k = 0; k < i; k++)
                    {
                        var child = new MaterialSpecification("WorkMasterID",
                            null,
                            MaterialClassDataGenerator.GenerateValidMaterialClasss()
                                .FirstOrDefault(),
                            hierarchyScope: HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                                .FirstOrDefault());
                        newSpec.AddChild(child);
                    }
                    specifications.Add(newSpec);
                }
                _materialSpecificationCollections.Add(specifications);
            }

            for (var i = 3 - 1; i >= 0; i--)
            {
                var newSpec = new MaterialSpecification("MasterId." + i,
                    null,
                    null,
                    MaterialDefinitionDataGenerator.GenerateValidMaterialDefinitions()
                        .FirstOrDefault(),
                    Domain.Client.MaterialUseType.Produced,
                    GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                        .FirstOrDefault(),
                    MaterialAssemblyType.Physical,
                    Domain.Client.AssemblyRelationshipType.Permanent,
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    "Description",
                    QuantityDataGenerator.GenerateValidQuantityCollections()
                        .FirstOrDefault(q => q.Count == i),
                    DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                        .FirstOrDefault(p => p.Count == i));
                _materialSpecifications.Add(newSpec);

                var child = new MaterialSpecification("WorkMasterID",
                    null,
                    MaterialClassDataGenerator.GenerateValidMaterialClasss()
                        .FirstOrDefault(),
                    hierarchyScope: HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault());
                newSpec.AddChild(child);

                newSpec = new MaterialSpecification(null,
                    "MasterId." + i,
                    MaterialClassDataGenerator.GenerateValidMaterialClasss()
                        .FirstOrDefault(),
                    null,
                    Domain.Client.MaterialUseType.Produced,
                    GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                        .FirstOrDefault(),
                    MaterialAssemblyType.Physical,
                    Domain.Client.AssemblyRelationshipType.Permanent,
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    "Description",
                    QuantityDataGenerator.GenerateValidQuantityCollections()
                        .FirstOrDefault(q => q.Count == i),
                    DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                        .FirstOrDefault(p => p.Count == i));
                _materialSpecifications.Add(newSpec);
                child = new MaterialSpecification("WorkMasterID",
                    null,
                    MaterialClassDataGenerator.GenerateValidMaterialClasss()
                        .FirstOrDefault());
                newSpec.AddChild(child);

                newSpec = new MaterialSpecification(null,
                    "MasterId",
                    MaterialClassDataGenerator.GenerateValidMaterialClasss()
                        .FirstOrDefault(),
                    null,
                    Domain.Client.MaterialUseType.Produced,
                    GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                        .FirstOrDefault(),
                    MaterialAssemblyType.Physical,
                    Domain.Client.AssemblyRelationshipType.Permanent,
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    "Description",
                    QuantityDataGenerator.GenerateValidQuantityCollections()
                        .FirstOrDefault(q => q.Count == i),
                    DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                        .FirstOrDefault(p => p.Count == i));
                _materialSpecifications.Add(newSpec);
                new MaterialSpecification("WorkMasterID",
                    null,
                    MaterialClassDataGenerator.GenerateValidMaterialClasss()
                        .FirstOrDefault());
                newSpec.AddChild(child);
            }
            // basic instance with only key fields specified
            _materialSpecifications.Add(new MaterialSpecification("materialSpecificationID",
                "MaterialClassId"));
        }

        #endregion

        #region Members

        /// <summary>
        ///     Generates the valid material specification collections.
        /// </summary>
        /// <returns>
        ///     System.Collections.Generic.IEnumerable&lt;System.Collections.Generic.ISet&lt;
        ///     MI.Modeller.Domain.Client.MaterialSpecification&gt;&gt;.
        /// </returns>
        public static IEnumerable<ISet<MaterialSpecification>> GenerateValidMaterialSpecificationCollections()
        {
            return _materialSpecificationCollections;
        }

        /// <summary>
        ///     Generates the valid material specifications.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MI.Modeller.Domain.Client.MaterialSpecification&gt;.</returns>
        public static IEnumerable<MaterialSpecification> GenerateValidMaterialSpecifications()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            return _materialSpecifications;
        }

        #endregion
    }
}