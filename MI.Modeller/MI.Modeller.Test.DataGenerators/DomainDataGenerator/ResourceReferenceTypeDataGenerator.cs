﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ResourceReferenceTypeDataGenerator.
    ///     Generates all valid ResourceReferenceType objects
    /// </summary>
    public static class ResourceReferenceTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid data types.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceReferenceType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<Domain.Client.ResourceReferenceType>> GenerateValidResourceReferenceTypes() => from Domain.Client.ResourceReferenceType value in Enum
                .GetValues(typeof(Domain.Client.ResourceReferenceType))
            select ExtensibleEnum<Domain.Client.ResourceReferenceType>.Parse(
                value == Domain.Client.ResourceReferenceType.Other
                    ? "OtherValue"
                    : value.ToString());

        #endregion
    }
}