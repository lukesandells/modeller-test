﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationNodeDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationNodeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid workflow specification node collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecificationNode&gt;&gt;.</returns>
        public static IEnumerable<ISet<WorkflowSpecificationNode>>
            GenerateValidWorkflowSpecificationNodeCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<WorkflowSpecificationNode> nodes = new HashSet<WorkflowSpecificationNode>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    nodes.Add(new WorkflowSpecificationNode($"WorkflowSpecificationNode.InternalId.{i}.{j}",
                        $"WorkflowSpecificationNode.NodeType.{i}.{j}",
                        GenerateWorkflowSpecification()
                            .FirstOrDefault(w => w != null),
                        $"WorkflowSpecificationNode.Description.{i}.{j}",
                        BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .FirstOrDefault(p => p.Count == 2)));
                }
                yield return nodes;
            }
        }

        /// <summary>
        ///     Generates the valid workflow specification nodes.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationNode&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationNode> GenerateValidWorkflowSpecificationNodes()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecificationNode($"WorkflowSpecificationNode.InternalId.{i}",
                    $"WorkflowSpecificationNode.NodeType.{i}",
                    GenerateWorkflowSpecification()
                        .FirstOrDefault(w => w != null),
                    $"WorkflowSpecificationNode.Description.{i}",
                    BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault(p => p.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new WorkflowSpecificationNode("WorkflowSpecificationNode.InternalId");
        }

        private static IEnumerable<WorkflowSpecification> GenerateWorkflowSpecification()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecification($"WorkflowSpecification.InternalId.{i}",
                    $"WorkflowSpecification.Version.{i}",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    $"WorkflowSpecification.Description.{i}",
                    null,
                    WorkflowSpecificationConnectionDataGenerator.GenerateValidWorkflowSpecificationConnectionCollections
                        ()
                        .FirstOrDefault(c => c.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new WorkflowSpecification("WorkflowSpecification.InternalId");
        }

        #endregion
    }
}