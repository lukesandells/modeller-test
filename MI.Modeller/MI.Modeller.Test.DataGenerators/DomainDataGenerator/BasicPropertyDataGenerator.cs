﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class BasicPropertyDataGenerator.
    /// </summary>
    public static class BasicPropertyDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid basic properties.
        /// </summary>
        /// <returns>IEnumerable&lt;BasicProperty&gt;.</returns>
        public static IEnumerable<BasicProperty> GenerateValidBasicProperties()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newProperty = new BasicProperty($"BasicProperty.InternalId.{i}",
                    "BasicProperty.Description",
                    ValueDataGenerator.GenerateValidValueCollections()
                        .FirstOrDefault(v => v.Count == i),
                    $"Property Type.{i}");
                var childCollection = GenerateValidBasicPropertyCollections()
                    .FirstOrDefault(v => v.Count == i);
                if (childCollection != null)
                {
                    foreach (var child in childCollection)
                    {
                        newProperty.AddChild(child);
                    }
                }
                yield return newProperty;
            }

            // basic object with only mandatory fields populated
            yield return new BasicProperty("BasicProperty.InternalId");
        }

        /// <summary>
        ///     Generates the valid basic property collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;BasicProperty&gt;&gt;.</returns>
        public static IEnumerable<IList<BasicProperty>> GenerateValidBasicPropertyCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                IList<BasicProperty> properties = new List<BasicProperty>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    properties.Add(new BasicProperty($"Property.InternalId.{i}.{j}",
                        $"Description.{i}.{j}",
                        ValueDataGenerator.GenerateValidValueCollections()
                            .FirstOrDefault(c => c.Count == 2)));
                }
                yield return properties;
            }
        }

        #endregion
    }
}