﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ResourceNetworkConnectionDataGenerator.
    /// </summary>
    public static class ResourceNetworkConnectionDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid resource network connection collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;ResourceNetworkConnection&gt;&gt;.</returns>
        public static IEnumerable<ISet<ResourceNetworkConnection>>
            GenerateValidResourceNetworkConnectionCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<ResourceNetworkConnection> connections = new HashSet<ResourceNetworkConnection>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    connections.Add(new ResourceNetworkConnection($"InternalId.{i}.{j}",
                        $"ResourceNetworkConnection.Description.{i}.{j}",
                        $"ResourceNetworkConnection.InternalId.{i}.{j}",
                        ResourceReferenceDataGenerator.GenerateValidResourceReferences()
                            .FirstOrDefault(r => r != null),
                        ResourceReferenceDataGenerator.GenerateValidResourceReferences()
                            .FirstOrDefault(r => r != null),
                        BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .FirstOrDefault(v => v.Count == 2)));
                }
                yield return connections;
            }
        }

        /// <summary>
        ///     Generates the valid resource network connections.
        /// </summary>
        /// <returns>IEnumerable&lt;ResourceNetworkConnection&gt;.</returns>
        public static IEnumerable<ResourceNetworkConnection> GenerateValidResourceNetworkConnections()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new ResourceNetworkConnection("InternalId",
                    "ResourceNetworkConnection.Description",
                    "ResourceNetworkConnection.InternalId",
                    ResourceReferenceDataGenerator.GenerateValidResourceReferences()
                        .FirstOrDefault(r => r != null),
                    ResourceReferenceDataGenerator.GenerateValidResourceReferences()
                        .FirstOrDefault(r => r != null),
                    BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault(v => v.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new ResourceNetworkConnection();
        }

        #endregion
    }
}