﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class ProcessSegmentDataGenerator.
    /// </summary>
    public static class ProcessSegmentDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid operations segment collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;ProcessSegment&gt;&gt;.</returns>
        public static IEnumerable<ISet<ProcessSegment>> GenerateValidProcessSegmentCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<ProcessSegment> items = new HashSet<ProcessSegment>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    var processSegment = new ProcessSegment($"ProcessSegment.InternalId.{i}.{j}",
                        $"Description.{i}.{j}",
                        OperationsTypeDataGenerator.GenerateValidOperationsTypes()
                            .FirstOrDefault(h => h != null),
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(h => h != null),
                        DateTime.Today,
                        new TimeSpan(2,
                            5,
                            4,
                            33),
                        BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .FirstOrDefault(p => p.Count == 2),
                        SegmentDependencyDataGenerator.GenerateValidSegmentDependencyCollections()
                            .FirstOrDefault(s => s.Count == 2));
                    items.Add(processSegment);
                    foreach (
                        var materialSpec in
                        MaterialSpecificationDataGenerator.GenerateValidMaterialSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        processSegment.AddMaterialSpecification(materialSpec);
                    }
                    foreach (
                        var personnelSpec in
                        PersonnelSpecificationDataGenerator.GenerateValidPersonnelSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        processSegment.AddPersonnelSpecification(personnelSpec);
                    }
                    foreach (
                        var equipmentSpec in
                        EquipmentSpecificationDataGenerator.GenerateValidEquipmentSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        processSegment.AddEquipmentSpecification(equipmentSpec);
                    }
                    foreach (
                        var assetSpec in
                        PhysicalAssetSpecificationDataGenerator.GenerateValidPhysicalAssetSpecificationCollections()
                            .First(s => s.Count == 2))
                    {
                        processSegment.AddPhysicalAssetSpecification(assetSpec);
                    }
                }
                yield return items;
            }
        }

        /// <summary>
        ///     Generates the valid operations segments.
        /// </summary>
        /// <returns>IEnumerable&lt;ProcessSegment&gt;.</returns>
        public static IEnumerable<ProcessSegment> GenerateValidProcessSegments()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var processSegment =  new ProcessSegment($"ProcessSegment.InternalId.{i}",
                    $"Description.{i}",
                    OperationsTypeDataGenerator.GenerateValidOperationsTypes()
                        .FirstOrDefault(h => h != null),
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    DateTime.Today,
                    new TimeSpan(2,
                        5,
                        4,
                        33),
                    BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault(p => p.Count == i),
                    SegmentDependencyDataGenerator.GenerateValidSegmentDependencyCollections()
                        .FirstOrDefault(s => s.Count == 2));
                foreach (
                                        var materialSpec in
                                        MaterialSpecificationDataGenerator.GenerateValidMaterialSpecificationCollections()
                                            .First(s => s.Count == 2))
                {
                    processSegment.AddMaterialSpecification(materialSpec);
                }
                foreach (
                    var personnelSpec in
                    PersonnelSpecificationDataGenerator.GenerateValidPersonnelSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    processSegment.AddPersonnelSpecification(personnelSpec);
                }
                foreach (
                    var equipmentSpec in
                    EquipmentSpecificationDataGenerator.GenerateValidEquipmentSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    processSegment.AddEquipmentSpecification(equipmentSpec);
                }
                foreach (
                    var assetSpec in
                    PhysicalAssetSpecificationDataGenerator.GenerateValidPhysicalAssetSpecificationCollections()
                        .First(s => s.Count == 2))
                {
                    processSegment.AddPhysicalAssetSpecification(assetSpec);
                }
                var children = GenerateValidProcessSegmentCollections()
                    .FirstOrDefault(s => s.Count == i);
                if (children != null)
                    foreach (var child in children)
                    {
                        processSegment.AddChild(child);
                    }
                yield return processSegment;
            }

            // basic object with only mandatory fields populated
            yield return new ProcessSegment("InternalId");
        }

        #endregion
    }
}