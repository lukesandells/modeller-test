﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class OperationsTypeDataGenerator.
    ///     Generates all valid OperationsType objects
    /// </summary>
    public static class OperationsTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid operations types.
        /// </summary>
        /// <returns>IEnumerable&lt;OperationsType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<OperationsType>> GenerateValidOperationsTypes() => from OperationsType value in Enum
                .GetValues(typeof(OperationsType))
            select ExtensibleEnum<OperationsType>.Parse(value == OperationsType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}