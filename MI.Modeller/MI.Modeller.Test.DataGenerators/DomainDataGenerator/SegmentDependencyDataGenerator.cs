﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class SegmentDependencyDataGenerator.
    /// </summary>
    public static class SegmentDependencyDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid segment dependencies.
        /// </summary>
        /// <returns>IEnumerable&lt;SegmentDependency&gt;.</returns>
        public static IEnumerable<SegmentDependency> GenerateValidSegmentDependencies()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                // segment ID and no processSegmentId
                yield return new SegmentDependency($"SegmentDependency.InternalId.Segment{i}",
                    DependencyTypeDataGenerator.GenerateValidDependencyTypes()
                        .First(),
                    null,
                    "SegmentId",
                    "SegmentDependency.Description",
                    ValueDataGenerator.GenerateValidValueCollections()
                        .FirstOrDefault(v => v.Count == i));

                yield return new SegmentDependency($"SegmentDependency.InternalId.ProcessId.{i}",
                    DependencyTypeDataGenerator.GenerateValidDependencyTypes()
                        .First(),
                    "ProcessSegmentID",
                    null,
                    "SegmentDependency.Description",
                    ValueDataGenerator.GenerateValidValueCollections()
                        .FirstOrDefault(v => v.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new SegmentDependency("SegmentDependency.InternalId",
                DependencyTypeDataGenerator.GenerateValidDependencyTypes()
                    .First(),
                "ProcessSegmentId");
        }

        /// <summary>
        ///     Generates the valid segment dependency collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;SegmentDependency&gt;&gt;.</returns>
        public static IEnumerable<ISet<SegmentDependency>> GenerateValidSegmentDependencyCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<SegmentDependency> items = new HashSet<SegmentDependency>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    items.Add(new SegmentDependency($"SegmentDependency.InternalId.{i}.{j}",
                        DependencyTypeDataGenerator.GenerateValidDependencyTypes()
                            .First(),
                        "processSegmentID",
                        null,
                        "SegmentDependency.Description",
                        ValueDataGenerator.GenerateValidValueCollections()
                            .FirstOrDefault(v => v.Count <= i)));
                }
                yield return items;
            }
        }

        #endregion
    }
}