﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class EquipmentClassDataGenerator.
    /// </summary>
    public static class EquipmentClassDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment class collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;EquipmentClass&gt;&gt;.</returns>
        public static IEnumerable<ISet<EquipmentClass>> GenerateValidEquipmentClassCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<EquipmentClass> definitions = new HashSet<EquipmentClass>();
                for (var j = 0; j < i; j++)
                {
                    var newDefinition = new EquipmentClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                        $"ID.{i}.{j}")
                    {
                        Description = $"Description.{i}.{j}",
                        EquipmentLevel = EquipmentLevelTypeDataGenerator.GenerateValidEquipmentLevelTypes()
                            .FirstOrDefault(e => e == EquipmentLevel.Area)
                    };
                    definitions.Add(newDefinition);
                        foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .First(m => m.Count == 2))
                        {
                            newDefinition.EquipmentClassProperties.Add(property);
                        }
                }
                yield return definitions;
            }
        }

        /// <summary>
        ///     Generates the valid equipment classes.
        /// </summary>
        /// <returns>IEnumerable&lt;EquipmentClass&gt;.</returns>
        public static IEnumerable<EquipmentClass> GenerateValidEquipmentClasses()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newClass = new EquipmentClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                        $"ID.{i}")
                    {
                        Description = $"Description.{i}",
                    EquipmentLevel = EquipmentLevelTypeDataGenerator.GenerateValidEquipmentLevelTypes()
                        .FirstOrDefault(e => e == EquipmentLevel.Area)
                };
                foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .First(m => m.Count == i))
                {
                    newClass.EquipmentClassProperties.Add(property);
                }
                var children = GenerateValidEquipmentClassCollections()
                    .FirstOrDefault(e => e.Count == i);
                if (children != null)
                    foreach (var child in children)
                    {
                        newClass.AddChild(child);
                    }
                yield return newClass;
            }
            // basic instance with only key fields specified
            yield return new EquipmentClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                "equipmentClassID");
        }

        #endregion
    }
}