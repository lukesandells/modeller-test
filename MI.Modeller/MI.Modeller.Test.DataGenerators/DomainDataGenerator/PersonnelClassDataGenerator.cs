﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class PersonnelClassDataGenerator.
    /// </summary>
    public static class PersonnelClassDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid personnel class collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;PersonnelClass&gt;&gt;.</returns>
        public static IEnumerable<ISet<PersonnelClass>> GenerateValidPersonnelClassCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<PersonnelClass> definitions = new HashSet<PersonnelClass>();
                for (var j = 0; j < i; j++)
                {
                    var newClass = new PersonnelClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                        $"ID.{i}.{j}")
                    {
                        Description = $"Description.{i}.{j}"
                    };
                        foreach (var property in BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .First(m => m.Count == 2))
                    {
                        newClass.PersonnelClassProperties.Add(property);
                    }
                    definitions.Add(newClass);
                }
                yield return definitions;
            }
        }

        /// <summary>
        ///     Generates the valid personnel classes.
        /// </summary>
        /// <returns>IEnumerable&lt;PersonnelClass&gt;.</returns>
        public static IEnumerable<PersonnelClass> GenerateValidPersonnelClasses()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newClass =  new PersonnelClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                    $"ID.{i}")
                {
                    Description = $"Description.{i}"
                };
                var properties = BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                    .FirstOrDefault(m => m.Count == i);
                if (properties != null)
                {
                    foreach (var property in properties)
                    {
                        newClass.PersonnelClassProperties.Add(property);
                    }
                }
                yield return newClass;
            }
            // basic instance with only key fields specified
            yield return new PersonnelClass(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(e => e.EquipmentLevel == EquipmentLevel.Site),
                "personnelClassID");
        }

        #endregion
    }
}