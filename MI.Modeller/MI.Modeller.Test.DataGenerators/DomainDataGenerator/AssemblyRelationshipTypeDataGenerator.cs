﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class AssemblyRelationshipTypeDataGenerator.
    /// </summary>
    public static class AssemblyRelationshipTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid assembly types.
        /// </summary>
        /// <returns>IEnumerable&lt;AssemblyRelationshipType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<Domain.Client.AssemblyRelationship>> GenerateValidAssemblyRelationshipTypes() => from Domain.Client.AssemblyRelationship value in Enum
                .GetValues(typeof(Domain.Client.AssemblyRelationship))
            select ExtensibleEnum<Domain.Client.AssemblyRelationship>.Parse(
                value == Domain.Client.AssemblyRelationship.Other
                    ? "OtherValue"
                    : value.ToString());

        #endregion
    }
}