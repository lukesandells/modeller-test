﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class EquipmentSpecificationDataGenerator.
    ///     Generate valid EquipmentSpecification items for testing
    ///     Most comprehensive object first, so externally firstOrDefault returns this one
    /// </summary>
    public static class EquipmentSpecificationDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment specification collections.
        /// </summary>
        /// <returns>
        ///     System.Collections.Generic.IEnumerable&lt;System.Collections.Generic.ISet&lt;
        ///     MI.Modeller.Domain.Client.EquipmentSpecification&gt;&gt;.
        /// </returns>
        public static IEnumerable<ISet<EquipmentSpecification>> GenerateValidEquipmentSpecificationCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<EquipmentSpecification> specifications = new HashSet<EquipmentSpecification>();
                for (var j = 0; j < i; j++)
                {
                    var newSpec = new EquipmentSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(),
                        $"MasterID.{i}.{j}")
                    {
                        Equipment = EquipmentDataGenerator.GenerateValidEquipments()
                            .FirstOrDefault(),
                        Description = $"Description {i}.{j}",
                        Use = $"Use.{i}.{j}",
                        SpatialLocation = GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                            .FirstOrDefault()
                    };
                    specifications.Add(newSpec);
                    foreach (var quantity in QuantityDataGenerator.GenerateValidQuantityCollections()
                        .First(q => q.Count == 2))
                    {
                        newSpec.Quantities.Add(quantity);
                    }
                    foreach (
                        var property in DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                            .First(p => p.Count == 2))
                    {
                        newSpec.SpecificationProperties.Add(property);
                    }
                }
                yield return specifications;
            }
        }

        /// <summary>
        ///     Generates the valid equipment specifications.
        /// </summary>
        /// <returns>System.Collections.Generic.IEnumerable&lt;MI.Modeller.Domain.Client.EquipmentSpecification&gt;.</returns>
        public static IEnumerable<EquipmentSpecification> GenerateValidEquipmentSpecifications()
        {
            // all attributes specified with boundaries for collections:
            // empty 
            // 1
            // 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                var newSpec = new EquipmentSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    "MasterId")
                {
                    Description = "Description",
                    Use = "equipmentUse",
                    SpatialLocation = GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                        .FirstOrDefault(),
                    EquipmentClass = EquipmentClassDataGenerator.GenerateValidEquipmentClasses()
                        .FirstOrDefault()
                };
                var quantities = QuantityDataGenerator.GenerateValidQuantityCollections()
                    .FirstOrDefault(q => q.Count == i);
                if (quantities != null)
                {
                    foreach (var quantity in quantities)
                    {
                        newSpec.Quantities.Add(quantity);
                    }
                }
                var properties = DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                    .FirstOrDefault(p => p.Count == i);
                if (properties != null)
                {
                    foreach (var property in properties)
                    {
                        newSpec.SpecificationProperties.Add(property);
                    }
                }
                yield return newSpec;

                newSpec = new EquipmentSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(),
                    "MasterId")
                {
                    Equipment = EquipmentDataGenerator.GenerateValidEquipments()
                        .FirstOrDefault(),
                    Description = "Description",
                    Use = "equipmentUse",
                    SpatialLocation = GeospatialDesignationDataGenerator.GenerateValidGeospatialDesignations()
                        .FirstOrDefault()
                };
                yield return newSpec;
                quantities = QuantityDataGenerator.GenerateValidQuantityCollections()
                    .FirstOrDefault(q => q.Count == i);
                if (quantities != null)
                {
                    foreach (var quantity in quantities)
                    {
                        newSpec.Quantities.Add(quantity);
                    }
                }
                properties = DistributedPropertyDataGenerator.GenerateValidDistributedPropertyCollections()
                    .FirstOrDefault(p => p.Count == i);
                if (properties != null)
                {
                    foreach (var property in properties)
                    {
                        newSpec.SpecificationProperties.Add(property);
                    }
                }
            }
            // basic instance with only key fields specified
            yield return new EquipmentSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(),
                "MasterId")
            {
                Equipment = EquipmentDataGenerator.GenerateValidEquipments()
                    .FirstOrDefault()
            };
            yield return new EquipmentSpecification(HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                    .FirstOrDefault(),
                "MasterId")
            {
                EquipmentClass = EquipmentClassDataGenerator.GenerateValidEquipmentClasses()
                    .FirstOrDefault()
            };
        }

        #endregion
    }
}