﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class DataTypeDataGenerator.
    ///     Generates all valid DataType objects
    /// </summary>
    public static class DataTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid data types.
        /// </summary>
        /// <returns>IEnumerable&lt;DataType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<DataType>> GenerateValidDataTypes() => from DataType value in Enum
                .GetValues(typeof(DataType))
            select ExtensibleEnum<DataType>.Parse(value == DataType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}