﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid workflow specification.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecification&gt;.</returns>
        public static IEnumerable<WorkflowSpecification> GenerateValidWorkflowSpecification()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecification($"WorkflowSpecification.InternalId.{i}",
                    $"WorkflowSpecification.Version.{i}",
                    HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                        .FirstOrDefault(h => h != null),
                    $"WorkflowSpecification.Description.{i}",
                    WorkflowSpecificationNodeDataGenerator.GenerateValidWorkflowSpecificationNodeCollections()
                        .FirstOrDefault(n => n.Count == i),
                    WorkflowSpecificationConnectionDataGenerator.GenerateValidWorkflowSpecificationConnectionCollections
                        ()
                        .FirstOrDefault(c => c.Count == i));
            }

            // basic object with only mandatory fields populated
            yield return new WorkflowSpecification("WorkflowSpecification.InternalId");
        }

        /// <summary>
        ///     Generates the valid workflow specification collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecification&gt;&gt;.</returns>
        public static IEnumerable<ISet<WorkflowSpecification>> GenerateValidWorkflowSpecificationCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<WorkflowSpecification> specifications = new HashSet<WorkflowSpecification>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    specifications.Add(new WorkflowSpecification($"WorkflowSpecification.InternalId.{i}.{j}",
                        $"WorkflowSpecification.Version.{i}.{j}",
                        HierarchyScopeDataGenerator.GenerateValidHierarchyScopes()
                            .FirstOrDefault(h => h != null),
                        $"WorkflowSpecification.Description.{i}.{j}",
                        WorkflowSpecificationNodeDataGenerator.GenerateValidWorkflowSpecificationNodeCollections()
                            .FirstOrDefault(n => n.Count == 2),
                        WorkflowSpecificationConnectionDataGenerator
                            .GenerateValidWorkflowSpecificationConnectionCollections()
                            .FirstOrDefault(c => c.Count == 2)));
                }
                yield return specifications;
            }
        }

        #endregion
    }
}