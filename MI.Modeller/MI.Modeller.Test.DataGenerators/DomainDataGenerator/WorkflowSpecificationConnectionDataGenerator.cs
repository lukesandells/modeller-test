﻿using System.Collections.Generic;
using System.Linq;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class WorkflowSpecificationConnectionDataGenerator.
    /// </summary>
    public static class WorkflowSpecificationConnectionDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid workflow specification connection collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;WorkflowSpecificationConnection&gt;&gt;.</returns>
        public static IEnumerable<ISet<WorkflowSpecificationConnection>>
            GenerateValidWorkflowSpecificationConnectionCollections()
        {
            // empty, 1, 2
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<WorkflowSpecificationConnection> connections = new HashSet<WorkflowSpecificationConnection>();
                for (var j = 0; j < i; j++)
                {
                    // this will generally be used to make children so dont add children
                    connections.Add(new WorkflowSpecificationConnection($"WorkflowSpecificationConnection.InternalId.{i}.{j}",
                        $"ConnectionType.{i}.{j}",
                        $"WorkflowSpecificationConnection.Description.{i}.{j}",
                        GenerateNodeIdCollections()
                            .FirstOrDefault(n => n.Count == 2),
                        GenerateNodeIdCollections()
                            .FirstOrDefault(n => n.Count == 2),
                        BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                            .FirstOrDefault(n => n.Count == 2)));
                }
                yield return connections;
            }
        }

        /// <summary>
        ///     Generates the valid workflow specification connections.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkflowSpecificationConnection&gt;.</returns>
        public static IEnumerable<WorkflowSpecificationConnection> GenerateValidWorkflowSpecificationConnections()
        {
            // all attributes filled, and collections with boundaries
            // empty, 1, and 2 items
            for (var i = 3 - 1; i >= 0; i--)
            {
                yield return new WorkflowSpecificationConnection($"WorkflowSpecificationConnection.InternalId.{i}",
                    $"ConnectionType.{i}",
                    $"WorkflowSpecificationConnection.Description.{i}",
                    GenerateNodeIdCollections()
                        .FirstOrDefault(n => n.Count == i),
                    GenerateNodeIdCollections()
                        .FirstOrDefault(n => n.Count == i),
                    BasicPropertyDataGenerator.GenerateValidBasicPropertyCollections()
                        .FirstOrDefault(n => n.Count == i));
            }
            // basic object with only mandatory fields populated
            yield return new WorkflowSpecificationConnection("WorkflowSpecificationConnection.InternalId");
        }

        /// <summary>
        ///     Generates the node identifier collections.
        /// </summary>
        /// <returns>IEnumerable&lt;ISet&lt;System.String&gt;&gt;.</returns>
        private static IEnumerable<ISet<string>> GenerateNodeIdCollections()
        {
            // empty, 1, 2,3
            for (var i = 3 - 1; i >= 0; i--)
            {
                ISet<string> properties = new HashSet<string>();
                for (var j = 0; j < i; j++)
                {
                    properties.Add($"Node.InternalId.{i}.{j}");
                }
                yield return properties;
            }
        }

        #endregion
    }
}