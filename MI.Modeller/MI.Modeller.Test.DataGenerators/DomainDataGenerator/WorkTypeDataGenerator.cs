﻿using System;
using System.Collections.Generic;
using System.Linq;
using MI.Framework;
using MI.Modeller.Domain.Client;

namespace MI.Modeller.Test.DataGenerators.DomainDataGenerator
{
    /// <summary>
    ///     Class WorkTypeDataGenerator.
    ///     Returns WorkTypeValue objects for all valid values
    /// </summary>
    public static class WorkTypeDataGenerator
    {
        #region Members

        /// <summary>
        ///     Generates the valid equipment level types.
        /// </summary>
        /// <returns>IEnumerable&lt;WorkType&gt;.</returns>
        public static IEnumerable<ExtensibleEnum<WorkType>> GenerateValidWorkTypes() => from WorkType value in Enum
                .GetValues(typeof(WorkType))
            select ExtensibleEnum<WorkType>.Parse(value == WorkType.Other
                ? "OtherValue"
                : value.ToString());

        #endregion
    }
}