﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorreectly()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var physicalAssetClass = new PhysicalAssetClass(hierarchyScope, "PhysicalAssetClass");
			var physicalAssetSpecification = new PhysicalAssetSpecification<WorkMaster>(hierarchyScope, "PhysicalAssetSpec")
			{
				Description = "Description",
				PhysicalAssetClass = physicalAssetClass,
				PhysicalAssetUse = "Use",
				SpatialDefinition = new SpatialDefinition("SRID"),
				Quantity = 1.WithUnit(Mass.Tonne)
			};
			var correspondingProperty = physicalAssetClass.AddProperty("P1", 2.AsValue());
			physicalAssetSpecification.AddProperty(correspondingProperty, 3.AsValue());
			var mapper = new PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapper<WorkMaster>();

			// Act
			var result = mapper.Map(physicalAssetSpecification, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
			Assert.AreEqual("PhysicalAssetSpec", result.ID.Value, "Mapper returned result with invalid id");
			Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid descrpition");
			Assert.AreEqual("PhysicalAssetClass", result.PhysicalAssetClassID.Value, "Mapper returned result with invalid class id");
			Assert.AreEqual("Use", result.PhysicalAssetUse.Value, "Mapper returned result with invalid use");
			Assert.AreEqual("SRID", result.GeospatialDesignation.SRID.Value, "Mapper returned result with invalid spatial definition");
			Assert.AreEqual("1", result.Quantity.SingleOrDefault()?.QuantityString.Value, "Mapper returned result with invalid quantity");
			Assert.AreEqual("P1", result.PhysicalAssetSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
			Assert.AreEqual("3", result.PhysicalAssetSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper returned result with invalid property value");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorreectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var physicalAssetSpecification = new PhysicalAssetSpecification<WorkMaster>(hierarchyScope, "PhysicalAssetSpec");
		    var mapper = new PhysicalAssetSpecificationToPhysicalAssetSegmentSpecificationTypeMapper<WorkMaster>();

		    // Act
		    var result = mapper.Map(physicalAssetSpecification, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("PhysicalAssetSpec", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid descrpition");
		    Assert.IsNull(result.PhysicalAssetClassID, "Mapper returned result with invalid class id");
		    Assert.IsNull(result.PhysicalAssetUse, "Mapper returned result with invalid use");
		    Assert.IsNull(result.GeospatialDesignation, "Mapper returned result with invalid spatial definition");
		    Assert.IsNull(result.Quantity, "Mapper returned result with invalid quantity");
		    Assert.IsNull(result.PhysicalAssetSegmentSpecificationProperty, "Mapper returned result with invalid property");
	    }
	}
}