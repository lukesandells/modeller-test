using NUnit.Framework;
using System;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class OperationsSegmentToB2mmlOperationsSegmentTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class OperationsSegmentToOperationsSegmentTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var operationsSegment = new OperationsSegment(hierarchyScope, "OpsSeg")
			{
				Description = "Description",
				Duration = new TimeSpan(1,0,0), // 1 hour
				ProcessType = ProcessType.Maintenance
			};
		    operationsSegment.AddProperty("P1");
			operationsSegment.AddChild(new OperationsSegment(hierarchyScope, "Child"));
		    operationsSegment.AddEquipmentSpecification(hierarchyScope, "EquipmentSpec");
		    operationsSegment.AddMaterialSpecification(hierarchyScope, "MaterialSpec");
		    operationsSegment.AddPersonnelSpecification(hierarchyScope, "PersonnelSpec");
		    operationsSegment.AddPhysicalAssetSpecification(hierarchyScope, "PhysicalAssetSpec");
			operationsSegment.AddProcessSegment(new ProcessSegment(hierarchyScope, "ProcessSegment"));
			operationsSegment.AddSegmentDependency(new SegmentDependency<OperationsSegment>("SegmentDependency"));
		    var mapper = new OperationsSegmentToOperationsSegmentTypeMapper();

			// Act
		    var result = mapper.Map(operationsSegment, false);

			// Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("OpsSeg", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("01:00:00", result.Duration, "Mapper returned result with invalid duration");
		    Assert.AreEqual("Maintenance", result.OperationsType.Value, "Mapper returned result with invalid process type");
		    Assert.AreEqual("ProcessSegment", result.ProcessSegmentID.SingleOrDefault()?.Value, "Mapper returned result with invalid process segment");
		    Assert.AreEqual("SegmentDependency", result.SegmentDependency.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid segment dependency");
		    Assert.AreEqual("P1", result.Parameter.SingleOrDefault()?.ID.Value,
				"Mapper returned result with invalid property");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var operationsSegment = new OperationsSegment(hierarchyScope, "OpsSeg");
		    var mapper = new OperationsSegmentToOperationsSegmentTypeMapper();

		    // Act
		    var result = mapper.Map(operationsSegment, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("OpsSeg", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.Duration, "Mapper returned result with invalid duration");
		    Assert.IsNull(result.OperationsType, "Mapper returned result with invalid process type");
		    Assert.IsNull(result.OperationsSegment, "Mapper returned result with invalid child");
		    Assert.IsNull(result.EquipmentSpecification, "Mapper returned result with invalid equipment specification");
		    Assert.IsNull(result.MaterialSpecification, "Mapper returned result with invalid material specification");
		    Assert.IsNull(result.PersonnelSpecification, "Mapper returned result with invalid personnel specification");
		    Assert.IsNull(result.PhysicalAssetSpecification, "Mapper returned result with invalid physical asset specification");
		    Assert.IsNull(result.ProcessSegmentID, "Mapper returned result with invalid process segment");
		    Assert.IsNull(result.SegmentDependency, "Mapper returned result with invalid segment dependency");
		    Assert.IsNull(result.Parameter,
			    "Mapper returned result with invalid property");
	    }
	}
}