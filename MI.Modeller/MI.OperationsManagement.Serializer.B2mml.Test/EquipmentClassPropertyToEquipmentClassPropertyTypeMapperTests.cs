using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentClassPropertyToEquipmentClassPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentClassPropertyToEquipmentClassPropertyTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new EquipmentClassPropertyToEquipmentClassPropertyTypeMapper();
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site);
		    var property = equipmentClass.AddProperty("P1");
		    equipmentClass.AddProperty("Child", 2.AsValue(), property);

			// Act
		    var result = mapper.Map(property);

			// Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
		    Assert.AreEqual(1, result.EquipmentClassProperty.Length, "Mapper mapped incorrect number of children");
		    Assert.AreEqual("Child", result.EquipmentClassProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
		    Assert.AreEqual("2", result.EquipmentClassProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value, "Mapper mapped incorrect child");
		}
    }
}