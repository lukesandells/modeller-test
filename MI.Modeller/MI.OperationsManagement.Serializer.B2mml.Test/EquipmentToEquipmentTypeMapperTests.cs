using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentToEquipmentTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentToEquipmentTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
			// Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var equipment = new Equipment(hierarchyScope, "Equipment", EquipmentLevel.Site)
		    {
			    Description = "Description"
		    };
		    equipment.AddProperty("P1");
			equipment.AddChild(new Equipment(hierarchyScope, "Child", EquipmentLevel.Area));
			var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site);
			equipmentClass.AddMember(equipment);
		    var mapper = new EquipmentToEquipmentTypeMapper();

			// Act
		    var result = mapper.Map(equipment, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Equipment", result.ID.Value, "Mapper returned result with invalid ID");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("P1", result.EquipmentProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		    Assert.AreEqual("EquipmentClass", result.EquipmentClassID.SingleOrDefault()?.Value, "Mapper returned result with invalid equipment class");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var equipment = new Equipment(hierarchyScope, "Equipment", EquipmentLevel.Site);
		    var mapper = new EquipmentToEquipmentTypeMapper();

		    // Act
		    var result = mapper.Map(equipment, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Equipment", result.ID.Value, "Mapper returned result with invalid ID");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.EquipmentProperty, "Mapper returned result with invalid property");
		    Assert.IsNull(result.Equipment, "Mapper returned result with invalid child equipment");
		    Assert.IsNull(result.EquipmentClassID, "Mapper returned result with invalid equipment class");
	    }
	}
}