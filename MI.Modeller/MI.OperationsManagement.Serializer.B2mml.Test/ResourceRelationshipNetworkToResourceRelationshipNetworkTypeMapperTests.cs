﻿//using System;
//using System.Linq;
//using Castle.Core.Internal;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapperTests.
//    /// </summary>
//    public class ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapperTests
//    {
//        #region Static Fields and Constants

//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        #endregion

//        #region Members

//        /// <summary>
//        ///     Checks the type of the resource relationship network.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="b2mmlResourceRelationshipNetworkType">Type of the b2 MML resource relationship network.</param>
//        public static void CheckResourceRelationshipNetworkType(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType b2mmlResourceRelationshipNetworkType)
//        {
//            Assert.IsNotNull(b2mmlResourceRelationshipNetworkType,
//                "ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapper returned a null instance given valid input");

//            CheckId(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);
//            CheckDescription(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);
//            CheckHierarchyScope(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);
//            CheckRelationshipType(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);
//            CheckRelationshipForm(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);
//            CheckPublishDate(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);

//            CheckConnections(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);

//            //ResourceRelationshipType collectionRelationshipType = default(ResourceRelationshipType),
//            //ResourceRelationshipFormType relationshipForm = default(ResourceRelationshipFormType),
//            //DateTime publishedDate = default(DateTime),
//            //ISet< ResourceNetworkConnection > resourceNetworkConnections = null)
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var resourceRelationshipNetworkToB2mmlMapper =
//                new ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlType = resourceRelationshipNetworkToB2mmlMapper.Map(default(ResourceRelationshipNetwork));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(ResourceRelationshipNetworkDataGenerator),
//                 nameof(ResourceRelationshipNetworkDataGenerator.GenerateValidResourceRelationshipNetworks))] ResourceRelationshipNetwork resourceRelationshipNetwork)
//        {
//            // Arrange
//            var resourceRelationshipNetworkToB2mmlMapper =
//                new ResourceRelationshipNetworkToResourceRelationshipNetworkTypeMapper();

//            // Act
//            var b2mmlResourceRelationshipNetworkType =
//                resourceRelationshipNetworkToB2mmlMapper.Map(resourceRelationshipNetwork);

//            // Assert
//            CheckResourceRelationshipNetworkType(resourceRelationshipNetwork,
//                b2mmlResourceRelationshipNetworkType);
//        }

//        /// <summary>
//        ///     Checks the connections.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="resourceRelationshipNetworkType">Type of the resource relationship network.</param>
//        private static void CheckConnections(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType resourceRelationshipNetworkType)
//        {
//            // collections are never null in domain - always empty
//            if (resourceRelationshipNetwork.ResourceNetworkConnections.Count == 0)
//            {
//                Assert.IsNull(resourceRelationshipNetworkType.ResourceNetworkConnection,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceNetworkConnection"));
//            }
//            else
//            {
//                foreach (var connection in resourceRelationshipNetwork.ResourceNetworkConnections)
//                {
//                    var b2mmlConnection =
//                        resourceRelationshipNetworkType.ResourceNetworkConnection.FirstOrDefault(
//                            o => o.ID?.Value == connection.Id);

//                    Assert.IsNotNull(b2mmlConnection,
//                        string.Format(invalidPropertyMessage,
//                            "ResourceNetworkConnection"));
//                    ResourceNetworkConnectionToResourceNetworkConnectionTypeMapperTests
//                        .CheckResourceNetworkConnectionType(connection,
//                            b2mmlConnection);
//                }

//                Assert.AreEqual(resourceRelationshipNetwork.ResourceNetworkConnections.Count,
//                    resourceRelationshipNetworkType.ResourceNetworkConnection.Length,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceNetworkConnection"));
//            }
//        }

//        /// <summary>
//        ///     Checks the description.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="resourceRelationshipNetworkType">Type of the resource relationship network.</param>
//        private static void CheckDescription(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType resourceRelationshipNetworkType)
//        {
//            if (resourceRelationshipNetwork.Description == default(string))
//            {
//                Assert.IsNull(resourceRelationshipNetworkType.Description,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//            else
//            {
//                Assert.IsFalse(resourceRelationshipNetworkType.Description.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(1,
//                    resourceRelationshipNetworkType.Description.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(resourceRelationshipNetwork.Description,
//                    resourceRelationshipNetworkType.Description[0].Value,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//        }

//        /// <summary>
//        ///     Checks the hierarchy scope.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="b2mmlResourceRelationshipNetworkType">Type of the b2 MML resource relationship network.</param>
//        private static void CheckHierarchyScope(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType b2mmlResourceRelationshipNetworkType)
//        {
//            if (resourceRelationshipNetwork.HierarchyScope == null)
//            {
//                Assert.IsNull(b2mmlResourceRelationshipNetworkType.HierarchyScope,
//                    string.Format(invalidPropertyMessage,
//                        "HierarchyScope"));
//            }
//            else
//            {
//                Assert.IsNotNull(b2mmlResourceRelationshipNetworkType.HierarchyScope,
//                    string.Format(invalidPropertyMessage,
//                        "HierarchyScope"));
//                HierarchyScopeToHierarchyScopeTypeMapperTests.CheckHierarchyScopeType(
//                    resourceRelationshipNetwork.HierarchyScope,
//                    b2mmlResourceRelationshipNetworkType.HierarchyScope);
//            }
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="b2mmlResourceRelationshipNetworkType">Type of the b2 MML resource relationship network.</param>
//        private static void CheckId(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType b2mmlResourceRelationshipNetworkType)
//        {
//            if (resourceRelationshipNetwork.Id == null)
//            {
//                Assert.IsNull(b2mmlResourceRelationshipNetworkType.ID,
//                    string.Format(invalidPropertyMessage,
//                        "ID"));
//            }
//            else
//            {
//                Assert.IsNotNull(b2mmlResourceRelationshipNetworkType.ID,
//                    string.Format(invalidPropertyMessage,
//                        "ID"));
//                Assert.AreEqual(resourceRelationshipNetwork.Id,
//                    b2mmlResourceRelationshipNetworkType.ID.Value,
//                    string.Format(invalidPropertyMessage,
//                        "ID"));
//            }
//        }

//        /// <summary>
//        ///     Checks the publish date.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="b2mmlResourceRelationshipNetworkType">Type of the b2 MML resource relationship network.</param>
//        private static void CheckPublishDate(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType b2mmlResourceRelationshipNetworkType)
//        {
//            if (resourceRelationshipNetwork.PublishedDate == default(DateTime))
//            {
//                Assert.IsNull(b2mmlResourceRelationshipNetworkType.PublishedDate,
//                    string.Format(invalidPropertyMessage,
//                        "PublishedDate"));
//            }
//            else
//            {
//                Assert.AreEqual(resourceRelationshipNetwork.PublishedDate,
//                    b2mmlResourceRelationshipNetworkType.PublishedDate.Value,
//                    string.Format(invalidPropertyMessage,
//                        "PublishedDate"));
//            }
//        }

//        /// <summary>
//        ///     Checks the relationship form.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="b2mmlResourceRelationshipNetworkType">Type of the b2 MML resource relationship network.</param>
//        private static void CheckRelationshipForm(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType b2mmlResourceRelationshipNetworkType)
//        {
//            if (resourceRelationshipNetwork.RelationshipForm == null)
//            {
//                Assert.IsNull(b2mmlResourceRelationshipNetworkType.RelationshipForm,
//                    string.Format(invalidPropertyMessage,
//                        "RelationshipForm"));
//            }
//            else
//            {
//                ResourceRelationshipFormTypeFromRelationshipFormTypeMapperTests.CheckResourceRelationshipFormType(
//                    b2mmlResourceRelationshipNetworkType.RelationshipForm,
//                    resourceRelationshipNetwork.RelationshipForm);
//            }
//        }

//        /// <summary>
//        ///     Checks the type of the relationship.
//        /// </summary>
//        /// <param name="resourceRelationshipNetwork">The resource relationship network.</param>
//        /// <param name="b2mmlResourceRelationshipNetworkType">Type of the b2 MML resource relationship network.</param>
//        private static void CheckRelationshipType(ResourceRelationshipNetwork resourceRelationshipNetwork,
//            ResourceRelationshipNetworkType b2mmlResourceRelationshipNetworkType)
//        {
//            if (resourceRelationshipNetwork.RelationshipType == null)
//            {
//                Assert.IsNull(b2mmlResourceRelationshipNetworkType.RelationshipType,
//                    string.Format(invalidPropertyMessage,
//                        "CollectionRelationshipType"));
//            }
//            else
//            {
//                ResourceRelationshipTypeFromRelationshipTypeTypeMapperTests.CheckResourceRelationshipTypeType(
//                    b2mmlResourceRelationshipNetworkType.RelationshipType,
//                    resourceRelationshipNetwork.RelationshipType);
//            }
//        }

//        #endregion
//    }
//}