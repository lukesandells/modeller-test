using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class MaterialDefinitionPropertyToMaterialDefinitionPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class MaterialDefinitionPropertyToMaterialDefinitionPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new MaterialDefinitionPropertyToMaterialDefinitionPropertyTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var materialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition");
			var property = materialDefinition.AddProperty("P1");
			materialDefinition.AddProperty("Child", 2.AsValue(), property);

			// Act
			var result = mapper.Map(property);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.MaterialDefinitionProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("2", result.MaterialDefinitionProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}