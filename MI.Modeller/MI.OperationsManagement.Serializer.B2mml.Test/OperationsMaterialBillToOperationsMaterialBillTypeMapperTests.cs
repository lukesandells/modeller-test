﻿using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
    /// <summary>
    ///     Class OperationsMaterialBillToOperationsMaterialBillTypeMapperTests.
    /// </summary>
    [TestFixture]
    public class OperationsMaterialBillToOperationsMaterialBillTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
			// Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var bill = new OperationsMaterialBill(hierarchyScope,"OpsMaterialBill")
			{
				Description = "Description"
			};
			bill.AddOperationsMaterialBillItem(new OperationsMaterialBillItem(hierarchyScope, "Item"));
		    var mapper = new OperationsMaterialBillToOperationsMaterialBillTypeMapper();

			// Act
		    var result = mapper.Map(bill, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("OpsMaterialBill", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.Value, "Mapper returned result with invalid description");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var bill = new OperationsMaterialBill(hierarchyScope, "OpsMaterialBill");
		    var mapper = new OperationsMaterialBillToOperationsMaterialBillTypeMapper();

		    // Act
		    var result = mapper.Map(bill, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
			Assert.AreEqual("OpsMaterialBill", result.ID.Value, "Mapper returned result with invalid id");
			Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.OperationsMaterialBillItem, "Mapper returned result with invalid item");
	    }
	}
}