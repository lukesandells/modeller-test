﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentSpecificationPropertyToOpEquipmentSpecificationPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentSpecificationPropertyToOpEquipmentSpecificationPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new EquipmentSpecificationPropertyToOpEquipmentSpecificationPropertyTypeMapper<WorkMaster>();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site);
			var parentProperty = equipmentClass.AddProperty("P1");
			var correspondingProperty = equipmentClass.AddProperty("Child", 2.AsValue(), parentProperty);
			var equipmentSpecification = new EquipmentSpecification<WorkMaster>(hierarchyScope, "EquipmentSpecification");
			equipmentSpecification.AddProperty(correspondingProperty, 3.AsValue());

			// Act
			var result = mapper.Map(equipmentSpecification.Properties.Single());

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.EquipmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("3", result.EquipmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}