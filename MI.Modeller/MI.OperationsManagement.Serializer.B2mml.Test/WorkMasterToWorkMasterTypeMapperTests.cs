﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class WorkMasterToWorkMasterTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class WorkMasterToWorkMasterTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new WorkMasterToWorkMasterTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var workMaster = new WorkMaster(hierarchyScope, "WorkMaster")
			{
				Description = "Description",
				OperationsSegment = new OperationsSegment(hierarchyScope, "OperationsSegment"),
				Duration = new TimeSpan(1,0,0), // one hour
				ProcessSegment = new ProcessSegment(hierarchyScope, "ProcessSegment"),
				ProcessType = ProcessType.Inventory,
				Version = "1.0"
			};
		    workMaster.AddProperty("P1", 2.AsValue());
		    workMaster.AddEquipmentSpecification(hierarchyScope, "EquipmentSpec");
		    workMaster.AddMaterialSpecification(hierarchyScope, "MaterialSpec");
		    workMaster.AddPersonnelSpecification(hierarchyScope, "PersonnelSpec");
		    workMaster.AddPhysicalAssetSpecification(hierarchyScope, "PhysicalAssetSpec");
			workMaster.AddChild(new WorkMaster(hierarchyScope, "Child"));

		    // Act
		    var result = mapper.Map(workMaster, false);

		    // Assert
			Assert.IsNotNull(result, "Mapper returned a null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapped work master does not have expected hierarchy Scope");
		    Assert.AreEqual("WorkMaster", result.ID.Value, "Mapped work master does not have expected ID");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapped work master does not have expected Description");
		    Assert.AreEqual("OperationsSegment", result.OperationsSegmentID.Value, "Mapped work master does not have expected operations segment id");
		    Assert.AreEqual("01:00:00", result.Duration, "Mapped work master does not have expected duration");
		    Assert.AreEqual("ProcessSegment", result.ProcessSegmentID.SingleOrDefault()?.Value, "Mapped work master does not have expected process segment id");
		    Assert.AreEqual("Inventory", result.WorkType.Value, "Mapped work master does not have expected work type");
		    Assert.AreEqual("1.0", result.Version.Value, "Mapped work master does not have expected version");
		    Assert.AreEqual("P1", result.Parameter.SingleOrDefault()?.ID.Value, "Expected result did not have the expected paramter");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new WorkMasterToWorkMasterTypeMapper();
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var workMaster = new WorkMaster(hierarchyScope, "WorkMaster");

		    // Act
		    var result = mapper.Map(workMaster, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned a null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapped work master does not have expected hierarchy Scope");
		    Assert.AreEqual("WorkMaster", result.ID.Value, "Mapped work master does not have expected ID");
		    Assert.IsNull(result.Description, "Mapped work master does not have expected Description");
		    Assert.IsNull(result.OperationsSegmentID, "Mapped work master does not have expected operations segment id");
		    Assert.IsNull(result.Duration, "Mapped work master does not have expected duration");
		    Assert.IsNull(result.ProcessSegmentID, "Mapped work master does not have expected process segment id");
		    Assert.IsNull(result.WorkType, "Mapped work master does not have expected work type");
		    Assert.IsNull(result.Version, "Mapped work master does not have expected version");
		    Assert.IsNull(result.Parameter, "Expected result did not have the expected number of parameters");
		    Assert.IsNull(result.EquipmentSpecification, "Expected result did not have the expected equipment specifications");
		    Assert.IsNull(result.MaterialSpecification, "Expected result did not have the expected material specifications");
		    Assert.IsNull(result.PersonnelSpecification, "Expected result did not have the expected personnel specifications");
		    Assert.IsNull(result.PhysicalAssetSpecification, "Expected result did not have the physical asset specifications");
		    Assert.IsNull(result.WorkMaster, "Expected result did not have the assembly specifications");
	    }
	}
}