using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PhysicalAssetClassPropertyToPhysicalAssetClassPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PhysicalAssetClassPropertyToPhysicalAssetClassPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new PhysicalAssetClassPropertyToPhysicalAssetClassPropertyTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var physicalAssetClass = new PhysicalAssetClass(hierarchyScope, "PhysicalAssetClass");
			var property = physicalAssetClass.AddProperty("P1");
			physicalAssetClass.AddProperty("Child", 2, property);

			// Act
			var result = mapper.Map(property);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.PhysicalAssetClassProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("2", result.PhysicalAssetClassProperty.SingleOrDefault()?.Value?.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}