using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PersonnelSpecificationPropertyToPersonnelSegmentSpecificationPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PersonnelSpecificationPropertyToPersonnelSegmentSpecificationPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new PersonnelSpecificationPropertyToPersonnelSegmentSpecificationPropertyTypeMapper<WorkMaster>();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var personnelClass = new PersonnelClass(hierarchyScope, "PersonnelClass");
			var parentProperty = personnelClass.AddProperty("P1");
			var correspondingProperty = personnelClass.AddProperty("Child", 2.AsValue(), parentProperty);
			var personnelSpecification = new PersonnelSpecification<WorkMaster>(hierarchyScope, "PersonnelSpecification");
			personnelSpecification.AddProperty(correspondingProperty, 3.AsValue());

			// Act
			var result = mapper.Map(personnelSpecification.Properties.Single());

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.PersonnelSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("3", result.PersonnelSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child value");
		}
	}
}