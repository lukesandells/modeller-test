﻿using NUnit.Framework;
using System.Linq;
using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class OperationsMaterialBillItemToOperationsMaterialBillItemTypeMapperTests.
	/// </summary>
	[TestFixture]
    public class OperationsMaterialBillItemToOperationsMaterialBillItemTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var billItem = new OperationsMaterialBillItem(hierarchyScope, "Item")
			{
				AssemblyRelationshipType = AssemblyRelationship.Transient,
				AssemblyType = AssemblyType.Logical,
				Description = "Description",
				MaterialClass = new MaterialClass(hierarchyScope, "MaterialClass"),
				MaterialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition"),
				Quantity = 1.WithUnit(Mass.Tonne),
				UseType = "UseType"
			};
			billItem.AddChild(new OperationsMaterialBillItem(hierarchyScope, "Child"));
		    var mapper = new OperationsMaterialBillItemToOperationsMaterialBillItemTypeMapper();

		    // Act
		    var result = mapper.Map(billItem);

		    // Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Item", result.ID.Value, "Mapper returned result with incorrect id");
		    Assert.AreEqual("Transient", result.AssemblyRelationship.Value, "Mapper returned result with incorrect assembly relationship");
		    Assert.AreEqual("Logical", result.AssemblyType.Value, "Mapper returned result with incorrect assembly type");
		    Assert.AreEqual("Description", result.Description.Value, "Mapper returned result with incorrect description");
		    Assert.AreEqual("MaterialClass", result.MaterialClassID.SingleOrDefault()?.Value, "Mapper returned result with incorrect material class");
		    Assert.AreEqual("MaterialDefinition", result.MaterialDefinitionID.SingleOrDefault()?.Value, "Mapper returned result with incorrect material definition");
		    Assert.AreEqual("1", result.Quantity.SingleOrDefault()?.QuantityString.Value, "Mapper returned result with incorrect quantity");
		    Assert.AreEqual("UseType", result.UseType.Value, "Mapper returned result with incorrect use type");
		    Assert.AreEqual("Child", result.AssemblyBillOfMaterialItem.Single()?.ID.Value, "Mapper returned result with incorrect child");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var billItem = new OperationsMaterialBillItem(hierarchyScope, "Item");
		    var mapper = new OperationsMaterialBillItemToOperationsMaterialBillItemTypeMapper();

		    // Act
		    var result = mapper.Map(billItem);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Item", result.ID.Value, "Mapper returned result with incorrect id");
		    Assert.IsNull(result.AssemblyRelationship, "Mapper returned result with incorrect assembly relationship");
		    Assert.IsNull(result.AssemblyType, "Mapper returned result with incorrect assembly type");
		    Assert.IsNull(result.Description, "Mapper returned result with incorrect description");
		    Assert.IsNull(result.MaterialClassID, "Mapper returned result with incorrect material class");
		    Assert.IsNull(result.MaterialDefinitionID, "Mapper returned result with incorrect material definition");
		    Assert.IsNull(result.Quantity, "Mapper returned result with incorrect quantity");
		    Assert.IsNull(result.UseType, "Mapper returned result with incorrect use type");
		    Assert.IsNull(result.AssemblyBillOfMaterialItem, "Mapper returned result with incorrect child");
	    }
	}
}