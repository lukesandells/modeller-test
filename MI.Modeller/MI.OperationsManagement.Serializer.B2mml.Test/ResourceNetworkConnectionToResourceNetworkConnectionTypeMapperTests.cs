﻿//using System;
//using System.Linq;
//using Castle.Core.Internal;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class ResourceNetworkConnectionToResourceNetworkConnectionTypeMapperTests.
//    /// </summary>
//    public class ResourceNetworkConnectionToResourceNetworkConnectionTypeMapperTests
//    {
//        #region Static Fields and Constants

//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        #endregion

//        #region Members

//        /// <summary>
//        ///     Checks the type of the resource network connection.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        /// <param name="b2mmlResourceNetworkConnectionType">Type of the b2 MML resource network connection.</param>
//        public static void CheckResourceNetworkConnectionType(ResourceNetworkConnection resourceNetworkConnection,
//            ResourceNetworkConnectionType b2mmlResourceNetworkConnectionType)
//        {
//            Assert.IsNotNull(b2mmlResourceNetworkConnectionType,
//                "ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper returned a null instance given valid input");

//            CheckId(resourceNetworkConnection,
//                b2mmlResourceNetworkConnectionType);
//            CheckDescription(resourceNetworkConnection,
//                b2mmlResourceNetworkConnectionType);
//            CheckResourceNetworkConnectionId(resourceNetworkConnection,
//                b2mmlResourceNetworkConnectionType);
//            CheckResourceReference(resourceNetworkConnection.FromResourceReference,
//                b2mmlResourceNetworkConnectionType.FromResourceReference);
//            CheckResourceReference(resourceNetworkConnection.ToResourceReference,
//                b2mmlResourceNetworkConnectionType.ToResourceReference);
//            CheckProperties(resourceNetworkConnection,
//                b2mmlResourceNetworkConnectionType);
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var resourceNetworkConnectionToB2mmlMapper =
//                new ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlResourcePropertyType =
//                    resourceNetworkConnectionToB2mmlMapper.Map(default(ResourceNetworkConnection));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(ResourceNetworkConnectionDataGenerator),
//                 nameof(ResourceNetworkConnectionDataGenerator.GenerateValidResourceNetworkConnections))] ResourceNetworkConnection resourceNetworkConnection)
//        {
//            // Arrange
//            var resourceNetworkConnectionToB2mmlMapper =
//                new ResourceNetworkConnectionToResourceNetworkConnectionTypeMapper();

//            // Act
//            var b2mmlResourceNetworkConnectionType =
//                resourceNetworkConnectionToB2mmlMapper.Map(resourceNetworkConnection);

//            // Assert
//            CheckResourceNetworkConnectionType(resourceNetworkConnection,
//                b2mmlResourceNetworkConnectionType);
//        }

//        /// <summary>
//        ///     Checks the description.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        /// <param name="resourceNetworkConnectionType">Type of the resource network connection.</param>
//        private static void CheckDescription(ResourceNetworkConnection resourceNetworkConnection,
//            ResourceNetworkConnectionType resourceNetworkConnectionType)
//        {
//            var description = "Description";
//            if (resourceNetworkConnection.Description == default(string))
//            {
//                Assert.IsNull(resourceNetworkConnectionType.Description,
//                    string.Format(invalidPropertyMessage,
//                        description));
//            }
//            else
//            {
//                Assert.IsFalse(resourceNetworkConnectionType.Description.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        description));
//                Assert.AreEqual(1,
//                    resourceNetworkConnectionType.Description.Length,
//                    string.Format(invalidPropertyMessage,
//                        description));
//                Assert.AreEqual(resourceNetworkConnection.Description,
//                    resourceNetworkConnectionType.Description[0].Value,
//                    string.Format(invalidPropertyMessage,
//                        description));
//            }
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        /// <param name="b2mmlResourceNetworkConnectionType">Type of the b2 MML resource network connection.</param>
//        private static void CheckId(ResourceNetworkConnection resourceNetworkConnection,
//            ResourceNetworkConnectionType b2mmlResourceNetworkConnectionType)
//        {
//            Assert.IsNotNull(b2mmlResourceNetworkConnectionType.ID,
//                string.Format(invalidPropertyMessage,
//                    "ID"));
//            Assert.AreEqual(resourceNetworkConnection.Id,
//                b2mmlResourceNetworkConnectionType.ID.Value,
//                string.Format(invalidPropertyMessage,
//                    "ID"));
//        }

//        /// <summary>
//        ///     Checks the properties.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        /// <param name="resourceNetworkConnectionType">Type of the resource network connection.</param>
//        private static void CheckProperties(ResourceNetworkConnection resourceNetworkConnection,
//            ResourceNetworkConnectionType resourceNetworkConnectionType)
//        {
//            // collections are never null in domain - always empty
//            var _connectionproperty = "ConnectionProperty";

//            if (resourceNetworkConnection.ConnectionProperties.Count == 0)
//            {
//                Assert.IsNull(resourceNetworkConnectionType.ConnectionProperty,
//                    string.Format(invalidPropertyMessage,
//                        _connectionproperty));
//            }
//            else
//            {
//                foreach (var connectionProperty in resourceNetworkConnection.ConnectionProperties)
//                {
//                    var b2mmlProperty =
//                        resourceNetworkConnectionType.ConnectionProperty.FirstOrDefault(
//                            o => o.ID?.Value == connectionProperty.PropertyId);

//                    Assert.IsNotNull(b2mmlProperty,
//                        string.Format(invalidPropertyMessage,
//                            _connectionproperty));
//                    ResourcePropertyToResourcePropertyTypeMapperTests.CheckResourcePropertyType(connectionProperty,
//                        b2mmlProperty);
//                }

//                Assert.AreEqual(resourceNetworkConnection.ConnectionProperties.Count,
//                    resourceNetworkConnectionType.ConnectionProperty.Length,
//                    string.Format(invalidPropertyMessage,
//                        _connectionproperty));
//            }
//        }

//        /// <summary>
//        ///     Checks the resource network connection identifier.
//        /// </summary>
//        /// <param name="resourceNetworkConnection">The resource network connection.</param>
//        /// <param name="b2mmlResourceNetworkConnectionType">Type of the b2 MML resource network connection.</param>
//        private static void CheckResourceNetworkConnectionId(ResourceNetworkConnection resourceNetworkConnection,
//            ResourceNetworkConnectionType b2mmlResourceNetworkConnectionType)
//        {
//            var resourcenetworkconnectionid = "ResourceNetworkConnectionID";
//            if (resourceNetworkConnection.ResourceNetworkConnectionId == null)
//            {
//                Assert.IsNull(b2mmlResourceNetworkConnectionType.ResourceNetworkConnectionID,
//                    string.Format(invalidPropertyMessage,
//                        resourcenetworkconnectionid));
//            }
//            else
//            {
//                Assert.IsNotNull(b2mmlResourceNetworkConnectionType.ResourceNetworkConnectionID,
//                    string.Format(invalidPropertyMessage,
//                        resourcenetworkconnectionid));
//                Assert.AreEqual(resourceNetworkConnection.ResourceNetworkConnectionId,
//                    b2mmlResourceNetworkConnectionType.ResourceNetworkConnectionID.Value,
//                    string.Format(invalidPropertyMessage,
//                        resourcenetworkconnectionid));
//            }
//        }

//        /// <summary>
//        ///     Checks the resource reference.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        /// <param name="b2mmlResourceReferenceType">Type of the b2 MML resource reference.</param>
//        private static void CheckResourceReference(ResourceReference resourceReference,
//            ResourceReferenceType b2mmlResourceReferenceType)
//        {
//            // collections are never null in domain - always empty
//            if (resourceReference == null)
//            {
//                Assert.IsNull(b2mmlResourceReferenceType,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceReference"));
//            }
//            else
//            {
//                ResourceReferenceToResourceReferenceTypeMapperTests.CheckResourceReferenceType(resourceReference,
//                    b2mmlResourceReferenceType);
//            }
//        }

//        #endregion
//    }
//}