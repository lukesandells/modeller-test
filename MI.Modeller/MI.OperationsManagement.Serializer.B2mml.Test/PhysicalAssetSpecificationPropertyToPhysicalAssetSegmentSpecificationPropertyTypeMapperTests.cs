﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PhysicalAssetSpecificationPropertyToB2mmlPhysicalAssetSegmentSpecificationPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PhysicalAssetSpecificationPropertyToPhysicalAssetSegmentSpecificationPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new PhysicalAssetSpecificationPropertyToPhysicalAssetSegmentSpecificationPropertyTypeMapper<WorkMaster>();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var physicalAssetClass = new PhysicalAssetClass(hierarchyScope, "PhysicalAssetClass");
			var parentProperty = physicalAssetClass.AddProperty("P1");
			var correspondingProperty = physicalAssetClass.AddProperty("Child", 2.AsValue(), parentProperty);
			var physicalAssetSpecification = new PhysicalAssetSpecification<WorkMaster>(hierarchyScope, "PhysicalAssetSpecification");
			physicalAssetSpecification.AddProperty(correspondingProperty, 3.AsValue());

			// Act
			var result = mapper.Map(physicalAssetSpecification.Properties.Single(), false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.PhysicalAssetSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("3", result.PhysicalAssetSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child value");
		}
	}
}