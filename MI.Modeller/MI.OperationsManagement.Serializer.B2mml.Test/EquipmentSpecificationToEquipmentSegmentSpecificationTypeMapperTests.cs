using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentSpecificationToB2mmlEquipmentSegmentSpecificationTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentSpecificationToEquipmentSegmentSpecificationTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var equipment = new Equipment(hierarchyScope, "Equipment", EquipmentLevel.Site);
			var equipmentSpecification = new EquipmentSpecification<WorkMaster>(hierarchyScope, "EquipmentSpec")
			{
				Description = "Description",
				EquipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site),
				Equipment = equipment,
				EquipmentUse = "Use",
				SpatialDefinition = new SpatialDefinition("SRID"),
				Quantity = 1.WithUnit(Mass.Tonne)
			};
			var correspondingProperty = equipment.AddProperty("P1", 2.AsValue());
			equipmentSpecification.AddProperty(correspondingProperty, 3.AsValue());
			var mapper = new EquipmentSpecificationToEquipmentSegmentSpecificationTypeMapper<WorkMaster>();

			// Act
			var result = mapper.Map(equipmentSpecification);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid Hierarchy Scope");
			Assert.AreEqual("EquipmentSpec", result.ID.Value, "Mapper returned result with invalid ID");
			Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
			Assert.AreEqual("EquipmentClass", result.EquipmentClassID.Value, "Mapper returned result with invalid equipment class id");
			Assert.AreEqual("Equipment", result.EquipmentID.Value, "Mapper returned result with invalid equipment id");
			Assert.AreEqual("Use", result.EquipmentUse.Value, "Mapper returned result with invalid equipment use");
			Assert.AreEqual("SRID", result.GeospatialDesignation.SRID.Value, "Mapper returned result with invalid geospatial designation");
			Assert.AreEqual("1", result.Quantity.SingleOrDefault()?.QuantityString.Value, "Mapper returned result with invalid quantity");
			Assert.AreEqual("P1", result.EquipmentSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var equipmentSpecification = new EquipmentSpecification<WorkMaster>(hierarchyScope, "EquipmentSpec");
		    var mapper = new EquipmentSpecificationToEquipmentSegmentSpecificationTypeMapper<WorkMaster>();

		    // Act
		    var result = mapper.Map(equipmentSpecification);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid Hierarchy Scope");
		    Assert.AreEqual("EquipmentSpec", result.ID.Value, "Mapper returned result with invalid ID");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.EquipmentClassID, "Mapper returned result with invalid equipment class id");
		    Assert.IsNull(result.EquipmentID, "Mapper returned result with invalid equipment id");
		    Assert.IsNull(result.EquipmentUse, "Mapper returned result with invalid equipment use");
		    Assert.IsNull(result.GeospatialDesignation, "Mapper returned result with invalid geospatial designation");
		    Assert.IsNull(result.Quantity, "Mapper returned result with invalid quantity");
		    Assert.IsNull(result.EquipmentSegmentSpecificationProperty, "Mapper returned result with invalid property");
	    }
	}
}