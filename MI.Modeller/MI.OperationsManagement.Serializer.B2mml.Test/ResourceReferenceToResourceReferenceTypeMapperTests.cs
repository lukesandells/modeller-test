﻿//using System;
//using System.Linq;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class ResourceReferenceToResourceReferenceTypeMapperTests.
//    /// </summary>
//    public class ResourceReferenceToResourceReferenceTypeMapperTests
//    {
//        #region Static Fields and Constants

//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        #endregion

//        #region Members

//        /// <summary>
//        ///     Checks the type of the resource reference.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        /// <param name="b2mmlResourceReferenceType">Type of the b2 MML resource reference.</param>
//        public static void CheckResourceReferenceType(ResourceReference resourceReference,
//            ResourceReferenceType b2mmlResourceReferenceType)
//        {
//            Assert.IsNotNull(b2mmlResourceReferenceType,
//                "ResourceReferenceToResourceReferenceTypeMapper returned a null instance given valid input");

//            CheckId(resourceReference,
//                b2mmlResourceReferenceType);
//            CheckResourceId(resourceReference,
//                b2mmlResourceReferenceType);
//            CheckResourceType(resourceReference,
//                b2mmlResourceReferenceType);
//            CheckProperties(resourceReference,
//                b2mmlResourceReferenceType);
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var resourceReferenceToB2mmlMapper = new ResourceReferenceToResourceReferenceTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlType = resourceReferenceToB2mmlMapper.Map(default(ResourceReference));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "ResourceReferenceToResourceReferenceTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(ResourceReferenceDataGenerator),
//                 nameof(ResourceReferenceDataGenerator.GenerateValidResourceReferences))] ResourceReference
//                resourceReference)
//        {
//            // Arrange
//            var resourceReferenceToB2mmlMapper = new ResourceReferenceToResourceReferenceTypeMapper();

//            // Act
//            var b2mmlResourceReferenceType = resourceReferenceToB2mmlMapper.Map(resourceReference);

//            // Assert
//            CheckResourceReferenceType(resourceReference,
//                b2mmlResourceReferenceType);
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        /// <param name="b2mmlResourceReferenceType">Type of the b2 MML resource reference.</param>
//        private static void CheckId(ResourceReference resourceReference,
//            ResourceReferenceType b2mmlResourceReferenceType)
//        {
//            if (resourceReference.Id == null)
//            {
//                Assert.IsNull(b2mmlResourceReferenceType.ID,
//                    string.Format(invalidPropertyMessage,
//                        "ID"));
//            }
//            else
//            {
//                Assert.IsNotNull(b2mmlResourceReferenceType.ID,
//                    string.Format(invalidPropertyMessage,
//                        "ID"));
//                Assert.AreEqual(resourceReference.Id,
//                    b2mmlResourceReferenceType.ID.Value,
//                    string.Format(invalidPropertyMessage,
//                        "ID"));
//            }
//        }

//        /// <summary>
//        ///     Checks the properties.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        /// <param name="resourceReferenceType">Type of the resource reference.</param>
//        private static void CheckProperties(ResourceReference resourceReference,
//            ResourceReferenceType resourceReferenceType)
//        {
//            // collections are never null in domain - always empty
//            if (resourceReference.ResourceProperties.Count == 0)
//            {
//                Assert.IsNull(resourceReferenceType.ResourceProperty,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceProperty"));
//            }
//            else
//            {
//                foreach (var resourceProperty in resourceReference.ResourceProperties)
//                {
//                    var b2mmlProperty =
//                        resourceReferenceType.ResourceProperty.FirstOrDefault(o => o.ID?.Value == resourceProperty.PropertyId);

//                    Assert.IsNotNull(b2mmlProperty,
//                        string.Format(invalidPropertyMessage,
//                            "ResourceProperties"));
//                    ResourcePropertyToResourcePropertyTypeMapperTests.CheckResourcePropertyType(resourceProperty,
//                        b2mmlProperty);
//                }

//                Assert.AreEqual(resourceReference.ResourceProperties.Count,
//                    resourceReferenceType.ResourceProperty.Length,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceProperties"));
//            }
//        }

//        /// <summary>
//        ///     Checks the resource identifier.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        /// <param name="b2mmlResourceReferenceType">Type of the b2 MML resource reference.</param>
//        private static void CheckResourceId(ResourceReference resourceReference,
//            ResourceReferenceType b2mmlResourceReferenceType)
//        {
//            if (resourceReference.ResourceId == null)
//            {
//                Assert.IsNull(b2mmlResourceReferenceType.ResourceID,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceID"));
//            }
//            else
//            {
//                Assert.IsNotNull(b2mmlResourceReferenceType.ResourceID,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceID"));
//                Assert.AreEqual(resourceReference.ResourceId,
//                    b2mmlResourceReferenceType.ResourceID.Value,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceID"));
//            }
//        }

//        /// <summary>
//        ///     Checks the type of the resource.
//        /// </summary>
//        /// <param name="resourceReference">The resource reference.</param>
//        /// <param name="b2mmlResourceReferenceType">Type of the b2 MML resource reference.</param>
//        private static void CheckResourceType(ResourceReference resourceReference,
//            ResourceReferenceType b2mmlResourceReferenceType)
//        {
//            if (resourceReference.ResourceType == null)
//            {
//                Assert.IsNull(b2mmlResourceReferenceType.ResourceType,
//                    string.Format(invalidPropertyMessage,
//                        "ResourceType"));
//            }
//            else
//            {
//                ResourceReferenceTypeFromResourceReferenceTypeTypeMapperTests.CheckResourceTypeType(
//                    b2mmlResourceReferenceType.ResourceType,
//                    resourceReference.ResourceType);
//            }
//        }

//        #endregion
//    }
//}