﻿using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class SpatialDefinitionToGeospatialDesignationTypeMapperTests.
	/// </summary>
	internal class SpatialDefinitionToGeospatialDesignationTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var mapper = new SpatialDefinitionToGeospatialDesignationTypeMapper();
			var spatialDefinition = new SpatialDefinition("SRID", "Auth", "Format", "Value");

			// Act
			var result = mapper.Map(spatialDefinition, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("SRID", result.SRID.Value, "Mapper returned result with invalid SRID");
		    Assert.AreEqual("Auth", result.SRIDAuthority.Value, "Mapper returned result with invalid Authority");
		    Assert.AreEqual("Format", result.Format.Value, "Mapper returned result with invalid Format");
		    Assert.AreEqual("Value", result.Value.Value, "Mapper returned result with invalid Value");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new SpatialDefinitionToGeospatialDesignationTypeMapper();
		    var spatialDefinition = new SpatialDefinition();

		    // Act
		    var result = mapper.Map(spatialDefinition, false);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.IsNull(result.SRID, "Mapper returned result with invalid SRID");
		    Assert.IsNull(result.SRIDAuthority, "Mapper returned result with invalid Authority");
		    Assert.IsNull(result.Format, "Mapper returned result with invalid Format");
		    Assert.IsNull(result.Value, "Mapper returned result with invalid Value");
	    }
	}
}