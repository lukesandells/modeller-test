﻿using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class SegmentDependencyToSegmentDependencyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class SegmentDependencyToSegmentDependencyTypeMapperTests
    {
		[Test]
		public void Test_MapWorkMaster_MapsCorrectly()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var workMaster = new WorkMaster(hierarchyScope, "WorkMasterId");
			var mapper = new SegmentDependencyToSegmentDependencyTypeMapper<WorkMaster>();
			var segmentDependency = new SegmentDependency<WorkMaster>("dependencyId")
			{
				DependencyType = OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType.PossibleParallel,
				Description = "Description",
				Process = workMaster
			};
			segmentDependency.TimingFactor = 1;

			// Act

			var result = mapper.Map(segmentDependency, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned invalid result");
			Assert.AreEqual("Description", result.Description[0].Value, "Result has unexpected description");
			Assert.AreEqual("PossibleParallel", result.Dependency.Value, "Result has unexpected dependency type");
			Assert.AreEqual("dependencyId", result.ID.Value, "Result has unexpected ID");
			Assert.AreEqual(1, result.Items.Length, "Result has unexpected number of items");
			Assert.AreEqual("WorkMasterId", result.Items[0].Value, "Result has unexpected item");
			Assert.AreEqual(typeof(IdentifierType), result.Items[0].GetType(), "Result has unexpected type of identifier");
			Assert.AreEqual(1, result.TimingFactor.Length, "Result has unexpected number of timing factors");
			Assert.AreEqual("1", result.TimingFactor[0].ValueString.Value, "Result has unexpected timing factor");
		}

	    [Test]
	    public void Test_MapProcessSegment_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var processSegment = new ProcessSegment(hierarchyScope, "ProcessSegmentId");
		    var mapper = new SegmentDependencyToSegmentDependencyTypeMapper<ProcessSegment>();
		    var segmentDependency = new SegmentDependency<ProcessSegment>("dependencyId")
		    {
			    DependencyType = OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType.PossibleParallel,
			    Description = "Description",
			    Process = processSegment
		    };
		    segmentDependency.TimingFactor = 1;

		    // Act

		    var result = mapper.Map(segmentDependency, false);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned invalid result");
		    Assert.AreEqual("Description", result.Description[0].Value, "Result has unexpected description");
		    Assert.AreEqual("PossibleParallel", result.Dependency.Value, "Result has unexpected dependency type");
		    Assert.AreEqual("dependencyId", result.ID.Value, "Result has unexpected ID");
		    Assert.AreEqual(1, result.Items.Length, "Result has unexpected number of items");
		    Assert.AreEqual("ProcessSegmentId", result.Items[0].Value, "Result has unexpected item");
		    Assert.AreEqual(typeof(ProcessSegmentIDType), result.Items[0].GetType(), "Result has unexpected type of identifier");
		    Assert.AreEqual(1, result.TimingFactor.Length, "Result has unexpected number of timing factors");
		    Assert.AreEqual("1", result.TimingFactor[0].ValueString.Value, "Result has unexpected timing factor");
	    }

	    [Test]
	    public void Test_MapOperationsSegment_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var operationsSegment = new OperationsSegment(hierarchyScope, "OperationsSegmentId");
		    var mapper = new SegmentDependencyToSegmentDependencyTypeMapper<OperationsSegment>();
		    var segmentDependency = new SegmentDependency<OperationsSegment>("dependencyId")
		    {
			    DependencyType = OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType.PossibleParallel,
			    Description = "Description",
			    Process = operationsSegment
		    };
		    segmentDependency.TimingFactor = 1;

		    // Act

		    var result = mapper.Map(segmentDependency, false);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned invalid result");
		    Assert.AreEqual("Description", result.Description[0].Value, "Result has unexpected description");
		    Assert.AreEqual("PossibleParallel", result.Dependency.Value, "Result has unexpected dependency type");
		    Assert.AreEqual("dependencyId", result.ID.Value, "Result has unexpected ID");
		    Assert.AreEqual(1, result.Items.Length, "Result has unexpected number of items");
		    Assert.AreEqual("OperationsSegmentId", result.Items[0].Value, "Result has unexpected item");
		    Assert.AreEqual(typeof(IdentifierType), result.Items[0].GetType(), "Result has unexpected type of identifier");
		    Assert.AreEqual(1, result.TimingFactor.Length, "Result has unexpected number of timing factors");
		    Assert.AreEqual("1", result.TimingFactor[0].ValueString.Value, "Result has unexpected timing factor");
	    }

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new SegmentDependencyToSegmentDependencyTypeMapper<OperationsSegment>();
		    var segmentDependency = new SegmentDependency<OperationsSegment>("dependencyId");

		    // Act

		    var result = mapper.Map(segmentDependency, false);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned invalid result");
		    Assert.IsNull(result.Description, "Result has unexpected description");
		    Assert.IsNull(result.Dependency, "Result has unexpected dependency type");
		    Assert.AreEqual("dependencyId", result.ID.Value, "Result has unexpected ID");
		    Assert.IsNull(result.Items, "Result has unexpected number of items");
	    }
	}
}