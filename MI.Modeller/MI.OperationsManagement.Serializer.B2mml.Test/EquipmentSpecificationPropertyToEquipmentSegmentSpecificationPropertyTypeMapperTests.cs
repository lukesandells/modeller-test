using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentSpecificationPropertyToB2mmlEquipmentSegmentSpecificationPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentSpecificationPropertyToEquipmentSegmentSpecificationPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new EquipmentSpecificationPropertyToEquipmentSegmentSpecificationPropertyTypeMapper<WorkMaster>();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site);
			var correspondingProperty = equipmentClass.AddProperty("P1");
			var correspondingSubPropery = equipmentClass.AddProperty("Child", 2.AsValue(), correspondingProperty);
			var equipmentSpecification = new EquipmentSpecification<WorkMaster>(hierarchyScope, "EquipmentSpecification");
			equipmentSpecification.AddProperty(correspondingSubPropery, 4.AsValue());

			// Act
			var result = mapper.Map(equipmentSpecification.Properties.Single());

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.EquipmentSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("4", result.EquipmentSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}