﻿//using System;
//using System.Linq;
//using Castle.Core.Internal;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationToWorkflowSpecificationTypeMapperTests.
//    /// </summary>
//    [TestFixture]
//    internal class WorkflowSpecificationToWorkflowSpecificationTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the workflow specification.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        public static void CheckWorkflowSpecificationType(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            Assert.IsNotNull(b2mmlWorkflowSpecificationType,
//                "WorkflowSpecificationToWorkflowSpecificationTypeMapper returned a null instance for given valid input");

//            CheckId(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//            CheckVersion(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//            CheckDescription(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//            CheckNode(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//            CheckConnection(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//            CheckHierarchyScope(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        private static void CheckId(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            // InternalId may not be null either
//            Assert.IsNotNull(b2mmlWorkflowSpecificationType.ID,
//                string.Format(invalidPropertyMessage,
//                    "InternalId"));
//            Assert.AreEqual(workflowSpecification.Id,
//                b2mmlWorkflowSpecificationType.ID.Value,
//                string.Format(invalidPropertyMessage,
//                    "ID"));
//        }

//        /// <summary>
//        ///     Checks the description.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        private static void CheckDescription(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            if (workflowSpecification.Description == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationType.Description,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationType.Description.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(1,
//                    b2mmlWorkflowSpecificationType.Description.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(workflowSpecification.Description,
//                    b2mmlWorkflowSpecificationType.Description[0].Value,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//        }

//        /// <summary>
//        ///     Checks the hierarchy scope.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        private static void CheckHierarchyScope(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            if (workflowSpecification.HierarchyScope == default(HierarchyScope))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationType.HierarchyScope,
//                    string.Format(invalidPropertyMessage,
//                        "HierarchyScope"));
//            }
//            else
//            {
//                HierarchyScopeToHierarchyScopeTypeMapperTests.CheckHierarchyScopeType(
//                    workflowSpecification.HierarchyScope,
//                    b2mmlWorkflowSpecificationType.HierarchyScope);
//            }
//        }

//        /// <summary>
//        ///     Checks the version.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        private static void CheckVersion(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            if (workflowSpecification.Version == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationType.Version,
//                    string.Format(invalidPropertyMessage,
//                        "Version"));
//            }
//            else
//            {
//                Assert.AreEqual(workflowSpecification.Version,
//                    b2mmlWorkflowSpecificationType.Version.Value,
//                    string.Format(invalidPropertyMessage,
//                        "Version"));
//            }
//        }

//        /// <summary>
//        ///     Checks the node.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        private static void CheckNode(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            // collections are never null in domain - always empty
//            if (workflowSpecification.Nodes.Count == 0)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationType.Node,
//                    string.Format(invalidPropertyMessage,
//                        "Node"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationType.Node.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Node"));
//                Assert.AreEqual(workflowSpecification.Nodes.Count,
//                    b2mmlWorkflowSpecificationType.Node.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Node"));
//                foreach (var node in workflowSpecification.Nodes)
//                {
//                    var b2mmlNode = b2mmlWorkflowSpecificationType.Node.FirstOrDefault(c => c.ID?.Value == node.Id);
//                    Assert.IsNotNull(b2mmlNode,
//                        string.Format(invalidPropertyMessage,
//                            "Node"));
//                    WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapperTests
//                        .CheckWorkflowSpecificationNodeType(node,
//                            b2mmlNode);
//                }
//            }
//        }

//        /// <summary>
//        ///     Checks the connection.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        /// <param name="b2mmlWorkflowSpecificationType">Type of the b2 MML workflow specification.</param>
//        private static void CheckConnection(WorkflowSpecification workflowSpecification,
//            WorkflowSpecificationType b2mmlWorkflowSpecificationType)
//        {
//            // collections are never null in domain - always empty
//            if (workflowSpecification.Connections.Count == 0)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationType.Connection,
//                    string.Format(invalidPropertyMessage,
//                        "Connection"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationType.Connection.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Connection"));
//                Assert.AreEqual(workflowSpecification.Connections.Count,
//                    b2mmlWorkflowSpecificationType.Connection.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Connection"));
//                foreach (var node in workflowSpecification.Connections)
//                {
//                    var b2mmlConnection =
//                        b2mmlWorkflowSpecificationType.Connection.FirstOrDefault(c => c.ID?.Value == node.Id);
//                    Assert.IsNotNull(b2mmlConnection,
//                        string.Format(invalidPropertyMessage,
//                            "Connection"));
//                    WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapperTests
//                        .CheckWorkflowSpecificationConnectionType(node,
//                            b2mmlConnection);
//                }
//            }
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var workflowSpecificationToB2mmlMapper = new WorkflowSpecificationToWorkflowSpecificationTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlWorkflowSpecificationType =
//                    workflowSpecificationToB2mmlMapper.Map(default(WorkflowSpecification));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "WorkflowSpecificationToWorkflowSpecificationTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="workflowSpecification">The workflow specification.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(WorkflowSpecificationDataGenerator),
//                 nameof(WorkflowSpecificationDataGenerator.GenerateValidWorkflowSpecification))] WorkflowSpecification
//                workflowSpecification)
//        {
//            // Arrange
//            var workflowSpecificationToB2mmlMapper = new WorkflowSpecificationToWorkflowSpecificationTypeMapper();

//            // Act
//            var b2mmlWorkflowSpecificationType = workflowSpecificationToB2mmlMapper.Map(workflowSpecification);

//            // Assert
//            CheckWorkflowSpecificationType(workflowSpecification,
//                b2mmlWorkflowSpecificationType);
//        }
//    }
//}