﻿using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class QuantityElementToQuantityValueTypeMapperTests.
	/// </summary>
	internal class QuantityElementToQuantityValueTypeMapperTests
    {
	    [Test]
	    public void Test_ExpandedUncertainty_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new QuantityElementToQuantityValueTypeMapper();
		    var value = 15.WithUnit(Mass.Tonne)
			    .AsQuantity("key")
			    .WithUncertainty(Uncertainty.ExpandedUncertainty(1, 0.5f, null, 4))
			    .WithDataType(QuantityDataType.Byte);

		    // Act
		    var result = mapper.Map(value.Serialise().Elements.SingleOrDefault(), false);

		    // Assert
		    Assert.IsNotNull(result);
		    Assert.AreEqual("15", result.QuantityString.Value, "Result did not have value as expected");
		    Assert.AreEqual("key", result.Key.Value, "Result did not have key as expected");
		    Assert.AreEqual("TNE", result.UnitOfMeasure.Value, "Result did not have unit of measure as expected");
		    Assert.AreEqual("1", result.CoverageFactor.Value, "Result did not have coverage factor as expected");
		    Assert.AreEqual("0.5", result.LevelOfConfidence.Value, "Result did not have level of confidence as expected");
		    Assert.AreEqual("4", result.ExpandedUncertainty.Value, "Result did not have expanded uncertainty as expected");
			Assert.AreEqual("Byte", result.DataType.Value, "Result did not have data type as expected");
		    Assert.IsNull(result.Accuracy, "Result had unexpected accuracy");
		    Assert.IsNull(result.Precision, "Result had unexpected precision");
	    }

	    [Test]
	    public void Test_AccuracyAndPrecision_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new QuantityElementToQuantityValueTypeMapper();
		    var value = 15.WithUnit(Mass.Tonne).
			    AsQuantity("key")
			    .WithUncertainty(Uncertainty.AccuracyAndPrecision(1, 0.5f))
			    .WithDataType(QuantityDataType.Byte);

		    // Act
		    var result = mapper.Map(value.Serialise().Elements.SingleOrDefault(), false);

		    // Assert
		    Assert.IsNotNull(result);
		    Assert.AreEqual("15", result.QuantityString.Value, "Result did not have value as expected");
		    Assert.AreEqual("key", result.Key.Value, "Result did not have key as expected");
		    Assert.AreEqual("TNE", result.UnitOfMeasure.Value, "Result did not have unit of measure as expected");
		    Assert.AreEqual("1", result.Accuracy.Value, "Result did not have accuracy as expected");
		    Assert.AreEqual("0.5", result.Precision.Value, "Result did not have precision as expected");
		    Assert.AreEqual("Byte", result.DataType.Value, "Result did not have data type as expected");
		    Assert.IsNull(result.ExpandedUncertainty, "Result had unexpected expanded uncertainty");
		    Assert.IsNull(result.CoverageFactor, "Result had unexpected coverage factor");
		    Assert.IsNull(result.LevelOfConfidence, "Result had unexpected level of confidence");
	    }
	}
}