﻿using System;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class ProcessSegmentToProcessSegmentTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class ProcessSegmentToProcessSegmentTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var mapper = new ProcessSegmentToProcessSegmentTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var processSegment = new ProcessSegment(hierarchyScope, "ProcessSegmentId")
			{
				Description = "Description",
				Duration = new TimeSpan(1,0,0), // 1 hour
				ProcessType = ProcessType.Maintenance,
				Version = "1.0"
			};
		    processSegment.AddProperty("P1");
			processSegment.AddChild(new ProcessSegment(hierarchyScope, "Child"));
		    processSegment.AddEquipmentSpecification(new EquipmentSpecification<ProcessSegment>(hierarchyScope, "EquipmentSpec"));
		    processSegment.AddMaterialSpecification(new MaterialSpecification<ProcessSegment>(hierarchyScope, "MaterialSpec"));
		    processSegment.AddPersonnelSpecification(new PersonnelSpecification<ProcessSegment>(hierarchyScope, "PersonnelSpec"));
		    processSegment.AddPhysicalAssetSpecification(new PhysicalAssetSpecification<ProcessSegment>(hierarchyScope, "PhysicalAssetSpec"));
			processSegment.AddSegmentDependency(new SegmentDependency<ProcessSegment>("SegmentDependency"));

			// Act
		    var result = mapper.Map(processSegment, false);
			
			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Description", result.Description[0].Value, "Result returned invalid description");
		    Assert.AreEqual("01:00:00", result.Duration, "Result returned invalid duration");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Result returned invalid hierarchy scope");
		    Assert.AreEqual("ProcessSegmentId", result.ID.Value, "Result returned invalid id");
		    Assert.AreEqual("Maintenance", result.OperationsType.Value, "Result returned invalid ");
		    Assert.AreEqual("P1", result.Parameter.SingleOrDefault()?.ID.Value, "Result did not have the expected parameter");
		    Assert.AreEqual("SegmentDependency", result.SegmentDependency.SingleOrDefault()?.ID.Value, "Result did not have the expected SegmentDependency");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new ProcessSegmentToProcessSegmentTypeMapper();
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var processSegment = new ProcessSegment(hierarchyScope, "ProcessSegmentId");

		    // Act
		    var result = mapper.Map(processSegment, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.IsNull(result.Description, "Result returned invalid description");
		    Assert.IsNull(result.Duration, "Result returned invalid duration");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Result returned invalid hierarchy scope");
		    Assert.AreEqual("ProcessSegmentId", result.ID.Value, "Result returned invalid id");
			Assert.IsNull(result.OperationsType, "Result returned invalid ");
			Assert.IsNull(result.Parameter, "Result did not have the expected parameter");
		    Assert.IsNull(result.EquipmentSegmentSpecification, "Result did not have the expected EquipmentSegmentSpecification");
		    Assert.IsNull(result.MaterialSegmentSpecification, "Result did not have the expected MaterialSegmentSpecification");
		    Assert.IsNull(result.PhysicalAssetSegmentSpecification, "Result did not have the expected PhysicalAssetSegmentSpecification");
		    Assert.IsNull(result.PersonnelSegmentSpecification, "Result did not have the expected PersonnelSegmentSpecification");
		    Assert.IsNull(result.SegmentDependency, "Result did not have the expected SegmentDependency");
		    Assert.IsNull(result.ProcessSegment, "Result did not have the expected children ProcessSegment");
	    }
	}
}