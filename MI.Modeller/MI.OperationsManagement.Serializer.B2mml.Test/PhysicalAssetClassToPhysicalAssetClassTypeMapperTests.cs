using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PhysicalAssetClassToPhysicalAssetClassTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PhysicalAssetClassToPhysicalAssetClassTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var physicalAssetClass = new PhysicalAssetClass(hierarchyScope, "PhysicalAssetClass")
			{
				Description = "Description"
			};
		    physicalAssetClass.AddProperty("P1");
			physicalAssetClass.AddManufacturer("Manufacturer");
			physicalAssetClass.AddSubclass(new PhysicalAssetClass(hierarchyScope, "Subclass"));
			var mapper = new PhysicalAssetClassToPhysicalAssetClassTypeMapper();

			// Act
		    var result = mapper.Map(physicalAssetClass, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("PhysicalAssetClass", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("P1", result.PhysicalAssetClassProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		    Assert.AreEqual("Manufacturer", result.Manufacturer.SingleOrDefault()?.Value, "Mapper returned result with invalid manufacturer");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var physicalAssetClass = new PhysicalAssetClass(hierarchyScope, "PhysicalAssetClass");
		    var mapper = new PhysicalAssetClassToPhysicalAssetClassTypeMapper();

		    // Act
		    var result = mapper.Map(physicalAssetClass, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("PhysicalAssetClass", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.PhysicalAssetClassProperty, "Mapper returned result with invalid property");
		    Assert.IsNull(result.Manufacturer, "Mapper returned result with invalid manufacturer");
		    Assert.IsNull(result.ContainedPhysicalAssetClass, "Mapper returned result with invalid subclass");
	    }
	}
}