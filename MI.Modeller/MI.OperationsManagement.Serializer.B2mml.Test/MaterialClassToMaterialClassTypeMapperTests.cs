using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class MaterialClassToMaterialClassTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class MaterialClassToMaterialClassTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var materialClass = new MaterialClass(hierarchyScope, "MaterialClass")
			{
				Description = "Description",
				AssemblyRelationship = AssemblyRelationship.Permanent,
				AssemblyType = AssemblyType.Logical
			};
		    materialClass.AddProperty("P1");
			materialClass.AddMember(new MaterialDefinition(hierarchyScope, "MaterialDefinition"));
			materialClass.AddSubclass(new MaterialClass(hierarchyScope, "Subclass"));
			var mapper = new MaterialClassToMaterialClassTypeMapper();

			// Act
		    var result = mapper.Map(materialClass, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("MaterialClass", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid descirption");
		    Assert.AreEqual("Permanent", result.AssemblyRelationship.Value, "Mapper returned result with invalid assembly relationship");
		    Assert.AreEqual("Logical", result.AssemblyType.Value, "Mapper returned result with invalid assembly type");
		    Assert.AreEqual("P1", result.MaterialClassProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		    Assert.AreEqual("MaterialDefinition", result.MaterialDefinitionID.SingleOrDefault()?.Value, "Mapper returned result with invalid material definition");
		    Assert.AreEqual("Subclass", result.AssemblyClassID.SingleOrDefault()?.Value, "Mapper returned result with invalid subclass");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var materialClass = new MaterialClass(hierarchyScope, "MaterialClass");
		    var mapper = new MaterialClassToMaterialClassTypeMapper();

		    // Act
		    var result = mapper.Map(materialClass, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("MaterialClass", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid descirption");
		    Assert.IsNull(result.AssemblyRelationship, "Mapper returned result with invalid assembly relationship");
		    Assert.IsNull(result.AssemblyType, "Mapper returned result with invalid assembly type");
		    Assert.IsNull(result.MaterialClassProperty, "Mapper returned result with invalid property");
		    Assert.IsNull(result.MaterialDefinitionID, "Mapper returned result with invalid material definition");
		    Assert.IsNull(result.AssemblyClassID, "Mapper returned result with invalid subclass");
	    }
	}
}