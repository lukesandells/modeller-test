using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class MaterialDefinitionToMaterialDefinitionTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class MaterialDefinitionToMaterialDefinitionTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var materialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition")
			{
				Description = "Description",
				AssemblyRelationship = AssemblyRelationship.Permanent,
				AssemblyType = AssemblyType.Physical
			};
		    materialDefinition.AddProperty("P1");
			materialDefinition.AddAssemblyElement(new MaterialDefinition(hierarchyScope, "AssemblyMaterial"));
			materialDefinition.AddToClass(new MaterialClass(hierarchyScope, "MaterialClass"));
			var mapper = new MaterialDefinitionToMaterialDefinitionTypeMapper();

			// Act
		    var result = mapper.Map(materialDefinition, false);

			// Assert
		    Assert.IsNotNull(result);
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("MaterialDefinition", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("Permanent", result.AssemblyRelationship.Value, "Mapper returned result with invalid assembly relationship");
		    Assert.AreEqual("Physical", result.AssemblyType.Value, "Mapper returned result with invalid assembly type");
		    Assert.AreEqual("P1", result.MaterialDefinitionProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		    Assert.AreEqual("AssemblyMaterial", result.AssemblylDefinitionID.SingleOrDefault()?.Value, "Mapper returned result with invalid assembly material");
		    Assert.AreEqual("MaterialClass", result.MaterialClassID.SingleOrDefault()?.Value, "Mapper returned result with invalid material class");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var materialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition");
		    var mapper = new MaterialDefinitionToMaterialDefinitionTypeMapper();

		    // Act
		    var result = mapper.Map(materialDefinition, true);

		    // Assert
		    Assert.IsNotNull(result);
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("MaterialDefinition", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.AssemblyRelationship, "Mapper returned result with invalid assembly relationship");
		    Assert.IsNull(result.AssemblyType, "Mapper returned result with invalid assembly type");
		    Assert.IsNull(result.MaterialDefinitionProperty, "Mapper returned result with invalid property");
		    Assert.IsNull(result.AssemblylDefinitionID, "Mapper returned result with invalid assembly material");
		    Assert.IsNull(result.MaterialClassID, "Mapper returned result with invalid material class");
	    }
	}
}