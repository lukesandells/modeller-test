using MI.OperationsManagement.Domain.Core.DataTypes;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class BasicPropertyToResourcePropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    public class BasicPropertyToResourcePropertyTypeMapperTests
    {
        [Test]
        public void Test_Map_MapsCorrectly()
        {
            // Arrange
            var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
            var equipmentClass = new EquipmentClass(hierarchyScope, "Equipment", EquipmentLevel.Site);
            var parentProperty = equipmentClass.AddProperty("P1");
            equipmentClass.AddProperty("P2", 2.AsValue(), parentProperty);
            var mapper = new BasicPropertyToResourcePropertyTypeMapper<EquipmentClass>();

            // Act
            var result = mapper.Map(parentProperty);

            // Assert
            Assert.IsNotNull(result, "Mapper returned null result");
            Assert.AreEqual("P1", result.ID.Value, "Mapper returned result with invalid ID");
            Assert.AreEqual("P2", result.ResourceProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid child ID");
            Assert.AreEqual("2", result.ResourceProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value, "Mapper returned result with invalid child value string");
        }
    }
}