using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentClassToEquipmentClassTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentClassToEquipmentClassTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site)
			{
				Description = "Description"
			};
			equipmentClass.AddProperty("P1");
			equipmentClass.AddMember(new Equipment(hierarchyScope, "Equipment", EquipmentLevel.Site));
			equipmentClass.AddSubclass(new EquipmentClass(hierarchyScope, "Subclass", EquipmentLevel.Site));
			var mapper = new EquipmentClassToEquipmentClassTypeMapper();

			// Act
			var result = mapper.Map(equipmentClass, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned invalid hierarchy scope");
			Assert.AreEqual("EquipmentClass", result.ID.Value, "Mapper returned invalid id");
			Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned invalid description");
			Assert.AreEqual("P1", result.EquipmentClassProperty.SingleOrDefault()?.ID.Value, "Mapper returned invalid property");
			Assert.AreEqual("Equipment", result.EquipmentID.SingleOrDefault()?.Value, "Mapper returned invalid member ID");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var equipmentClass = new EquipmentClass(hierarchyScope, "EquipmentClass", EquipmentLevel.Site);
		    var mapper = new EquipmentClassToEquipmentClassTypeMapper();

		    // Act
		    var result = mapper.Map(equipmentClass, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned invalid hierarchy scope");
		    Assert.AreEqual("EquipmentClass", result.ID.Value, "Mapper returned invalid id");
		    Assert.IsNull(result.Description, "Mapper returned invalid description");
		    Assert.IsNull(result.EquipmentClassProperty, "Mapper returned invalid property");
		    Assert.IsNull(result.EquipmentID, "Mapper returned invalid member ID");
		    Assert.IsNull(result.ContainedEquipmentClass, "Mapper returned invalid subclass");
	    }
	}
}