using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var personnelClass = new PersonnelClass(hierarchyScope, "PersonnelClass");
			var personnelSpec = new PersonnelSpecification<WorkMaster>(hierarchyScope, "PersonnelSpec")
			{
				Description = "Description",
				PersonnelClass = personnelClass,
				PersonnelUse = "Use",
				Quantity = 1.WithUnit(Mass.Tonne),
				SpatialDefinition = new SpatialDefinition("SRID")
			};
		    var property = personnelClass.AddProperty("P1", 2.AsValue());
		    personnelSpec.AddProperty(property, 3.AsValue());
		    var mapper = new PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapper<WorkMaster>();

			// Act
		    var result = mapper.Map(personnelSpec);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("PersonnelSpec", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("PersonnelClass", result.PersonnelClassID.Value, "Mapper returned result with invalid personnel class");
		    Assert.AreEqual("Use", result.PersonnelUse.Value, "Mapper returned result with invalid personnel use");
		    Assert.AreEqual("1", result.Quantity.SingleOrDefault()?.QuantityString.Value, "Mapper returned result with invalid quantity");
		    Assert.AreEqual("P1", result.PersonnelSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		    Assert.AreEqual("3", result.PersonnelSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper returned result with invalid property value");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var personnelSpec = new PersonnelSpecification<WorkMaster>(hierarchyScope, "PersonnelSpec");
		    var mapper = new PersonnelSpecificationToPersonnelSegmentSpecificationTypeMapper<WorkMaster>();

		    // Act
		    var result = mapper.Map(personnelSpec);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("PersonnelSpec", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.PersonnelClassID, "Mapper returned result with invalid personnel class");
		    Assert.IsNull(result.PersonnelUse, "Mapper returned result with invalid personnel use");
		    Assert.IsNull(result.Quantity, "Mapper returned result with invalid quantity");
		    Assert.IsNull(result.PersonnelSegmentSpecificationProperty, "Mapper returned result with invalid property");
	    }
	}
}