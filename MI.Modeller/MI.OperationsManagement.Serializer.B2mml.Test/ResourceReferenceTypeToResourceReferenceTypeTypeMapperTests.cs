﻿//using System;
//using MI.Framework;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class ResourceReferenceTypeTypeToB2mmlMapperTests.
//    /// </summary>
//    [TestFixture]
//    internal class ResourceReferenceTypeToResourceReferenceTypeTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the resource reference type.
//        /// </summary>
//        /// <param name="resourceReferenceType">Type of the resource reference.</param>
//        /// <param name="b2mmlResourceReferenceTypeType">Type of the b2 MML resource reference type.</param>
//        public static void CheckResourceReferenceTypeType(
//            ExtensibleEnum<Domain.Client.ResourceReferenceType>? resourceReferenceType,
//            ResourceReferenceTypeType b2mmlResourceReferenceTypeType)
//        {
//            Assert.IsNotNull(b2mmlResourceReferenceTypeType,
//                "ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper returned a null instance for Domain.Client.ResourceReferenceType given valid input");
//            Assert.AreEqual(((Domain.Client.ResourceReferenceType) resourceReferenceType).ToString(),
//                b2mmlResourceReferenceTypeType.Value,
//                string.Format(invalidPropertyMessage,
//                    "Value"));
//            Assert.AreEqual(resourceReferenceType?.HasOtherValue ?? false
//                    ? resourceReferenceType.ToString()
//                    : null,
//                b2mmlResourceReferenceTypeType.OtherValue,
//                string.Format(invalidPropertyMessage,
//                    "OtherValue"));
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var resourceReferenceTypeToB2mmlMapper =
//                new ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlResourceReferenceTypeType =
//                    resourceReferenceTypeToB2mmlMapper.Map(default(Domain.Client.ResourceReferenceType));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="resourceReferenceType">Type of the resource reference.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance([ValueSource(typeof(ResourceReferenceTypeDataGenerator),
//                nameof(ResourceReferenceTypeDataGenerator.GenerateValidResourceReferenceTypes))]
//            Domain.Client.ResourceReferenceType resourceReferenceType)
//        {
//            // Arrange
//            var resourceReferenceTypeToB2mmlMapper =
//                new ResourceReferenceTypeToResourceReferenceTypeTypeToB2MmlMapper();

//            // Act
//            var b2mmlResourceReferenceTypeType = resourceReferenceTypeToB2mmlMapper.Map(resourceReferenceType);

//            // Assert
//            CheckResourceReferenceTypeType(resourceReferenceType,
//                b2mmlResourceReferenceTypeType);
//        }
//    }
//}