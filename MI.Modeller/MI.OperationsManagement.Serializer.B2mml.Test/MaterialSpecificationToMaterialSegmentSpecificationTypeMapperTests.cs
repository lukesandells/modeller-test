using MI.Framework.Units;
using MI.Framework.Units.UneceRecommendation20;
using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class MaterialSpecificationToMaterialSegmentSpecificationTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class MaterialSpecificationToMaterialSegmentSpecificationTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var materialClass = new MaterialClass(hierarchyScope, "MaterialClass");
			var materialSpecification = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec")
			{
				Description = "Description",
				MaterialClass = materialClass,
				AssemblyRelationship = AssemblyRelationship.Transient,
				AssemblyType = AssemblyType.Logical,
				MaterialDefinition = new MaterialDefinition(hierarchyScope, "MaterialDefinition"),
				MaterialUse = MaterialUse.ByProductProduced,
				Quantity = 1.WithUnit(Mass.Tonne),
				SpatialDefinition = new SpatialDefinition("SRID")
			};
			materialSpecification.AddAssemblyElement(new MaterialSpecification<WorkMaster>(hierarchyScope, "AssemblyElement"));
		    var property = materialClass.AddProperty("P1", 2.AsValue());
		    materialSpecification.AddProperty(property, 3.AsValue());
			var mapper = new MaterialSpecificationToMaterialSegmentSpecificationTypeMapper<WorkMaster>();

			// Act
		    var result = mapper.Map(materialSpecification, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("MaterialSpec", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("Transient", result.AssemblyRelationship.Value, "Mapper returned result with invalid assembly relationship");
		    Assert.AreEqual("Logical", result.AssemblyType.Value, "Mapper returned result with invalid assembly type");
		    Assert.AreEqual("MaterialDefinition", result.MaterialDefinitionID.Value, "Mapper returned result with invalid material definition");
		    Assert.AreEqual("MaterialClass", result.MaterialClassID.Value, "Mapper returned result with invalid material class");
		    Assert.AreEqual("ByProductProduced", result.MaterialUse.Value, "Mapper returned result with invalid material use");
		    Assert.AreEqual("1", result.Quantity.SingleOrDefault()?.QuantityString.Value, "Mapper returned result with invalid quantity");
		    Assert.AreEqual("SRID", result.GeospatialDesignation.SRID.Value, "Mapper returned result with invalid spatial definition");
		    Assert.AreEqual("AssemblyElement", result.AssemblySpecificationID.SingleOrDefault()?.Value, "Mapper returned result with invalid assembly element");
		    Assert.AreEqual("P1", result.MaterialSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		    Assert.AreEqual("3", result.MaterialSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper returned result with invalid property value");
		}

	    [Test]
	    public void Test_BasicMap_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var materialSpecification = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpec");
		    var mapper = new MaterialSpecificationToMaterialSegmentSpecificationTypeMapper<WorkMaster>();

		    // Act
		    var result = mapper.Map(materialSpecification, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("MaterialSpec", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
			Assert.IsNull(result.AssemblyRelationship, "Mapper returned result with invalid assembly relationship");
		    Assert.IsNull(result.AssemblyType, "Mapper returned result with invalid assembly type");
		    Assert.IsNull(result.MaterialDefinitionID, "Mapper returned result with invalid material definition");
		    Assert.IsNull(result.MaterialClassID, "Mapper returned result with invalid material class");
		    Assert.IsNull(result.MaterialUse, "Mapper returned result with invalid material use");
		    Assert.IsNull(result.Quantity, "Mapper returned result with invalid quantity");
		    Assert.IsNull(result.GeospatialDesignation, "Mapper returned result with invalid spatial definition");
		    Assert.IsNull(result.AssemblySpecificationID, "Mapper returned result with invalid assembly element");
		    Assert.IsNull(result.MaterialSegmentSpecificationProperty, "Mapper returned result with invalid property");
	    }
	}
}