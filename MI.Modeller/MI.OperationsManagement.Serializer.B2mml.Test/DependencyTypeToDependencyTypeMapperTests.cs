using MI.Framework;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	[TestFixture]
    internal class DependencyTypeToDependencyTypeMapperTests
    {
		[Test]
	    public void Test_MapFromEnumerator_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new DependencyTypeToDependencyTypeMapper();
		    var dependencyType = OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType.AtStart;

		    // Act
		    var result = mapper.Map(dependencyType);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("AtStart", result.Value, "Mapper did not return expected value");
		    Assert.IsNull(result.OtherValue, "Mapper returned other value when none expected");
	    }

	    [Test]
	    public void Test_MapOtherValue_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new DependencyTypeToDependencyTypeMapper();
		    var dependencyType = ExtensibleEnum<OperationsManagement.Domain.MasterData.Design.ProcessDefinitions.DependencyType>.Parse("OtherValue");

		    // Act
		    var result = mapper.Map(dependencyType);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Other", result.Value, "Mapper did not return Other as value");
		    Assert.AreEqual("OtherValue", result.OtherValue, "Mapper did not return other value as expected");
	    }
	}
}