﻿using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class ProcessTypeToOperationsTypeTypeMapperTests.
	/// </summary>
	[TestFixture]
	internal class ProcessTypeToOperationsTypeTypeMapperTests
	{
		[Test]
		public void Test_MapFromEnumerator_MapsCorrectly()
		{
			// Arrange
			var mapper = new ProcessTypeToOperationsTypeTypeMapper();
			var processType = ProcessType.Maintenance;

			// Act
			var result = mapper.Map(processType);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("Maintenance", result.Value, "Mapper did not return expected value");
			Assert.IsNull(result.OtherValue, "Mapper returned other value when none expected");
		}

		[Test]
		public void Test_MapOtherValue_MapsCorrectly()
		{
			// Arrange
			var mapper = new ProcessTypeToOperationsTypeTypeMapper();
			var processType = ExtensibleEnum<ProcessType>.Parse("OtherValue");

			// Act
			var result = mapper.Map(processType);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("Other", result.Value, "Mapper did not return Other as value");
			Assert.AreEqual("OtherValue", result.OtherValue, "Mapper did not return other value as expected");
		}
	}
}