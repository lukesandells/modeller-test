﻿//using System;
//using System.Linq;
//using Castle.Core.Internal;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapperTests.
//    /// </summary>
//    [TestFixture]
//    internal class WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the workflow specification connection.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        public static void CheckWorkflowSpecificationConnectionType(
//            WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            Assert.IsNotNull(b2mmlWorkflowSpecificationConnectionType,
//                "WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper returned a null instance for given valid input");

//            CheckId(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//            CheckDescription(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//            CheckConnectionType(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//            CheckFromNodeId(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//            CheckToNodeId(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//            CheckProperty(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        private static void CheckId(WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            // InternalId may not be null either
//            Assert.IsNotNull(b2mmlWorkflowSpecificationConnectionType.ID,
//                string.Format(invalidPropertyMessage,
//                    "InternalId"));
//            Assert.AreEqual(workflowSpecificationConnection.Id,
//                b2mmlWorkflowSpecificationConnectionType.ID.Value,
//                string.Format(invalidPropertyMessage,
//                    "ID"));
//        }

//        /// <summary>
//        ///     Checks the description.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        private static void CheckDescription(WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            if (workflowSpecificationConnection.Description == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationConnectionType.Description,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationConnectionType.Description.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(1,
//                    b2mmlWorkflowSpecificationConnectionType.Description.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(workflowSpecificationConnection.Description,
//                    b2mmlWorkflowSpecificationConnectionType.Description[0].Value,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//        }

//        /// <summary>
//        ///     Checks the type of the connection.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        private static void CheckConnectionType(WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            if (workflowSpecificationConnection.ConnectionType == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationConnectionType.ConnectionType,
//                    string.Format(invalidPropertyMessage,
//                        "ConnectionType"));
//            }
//            else
//            {
//                Assert.AreEqual(workflowSpecificationConnection.ConnectionType,
//                    b2mmlWorkflowSpecificationConnectionType.ConnectionType.Value,
//                    string.Format(invalidPropertyMessage,
//                        "ConnectionType"));
//            }
//        }

//        /// <summary>
//        ///     Checks from node identifier.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        private static void CheckFromNodeId(WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            // collections are never null in domain - always empty
//            if (workflowSpecificationConnection.FromNodeIDs.Count == 0)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationConnectionType.FromNodeID,
//                    string.Format(invalidPropertyMessage,
//                        "FromNodeID"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationConnectionType.FromNodeID.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "FromNodeID"));
//                Assert.AreEqual(workflowSpecificationConnection.FromNodeIDs.Count,
//                    b2mmlWorkflowSpecificationConnectionType.FromNodeID.Length,
//                    string.Format(invalidPropertyMessage,
//                        "FromNodeID"));
//                foreach (var node in workflowSpecificationConnection.FromNodeIDs)
//                {
//                    Assert.IsNotNull(
//                        b2mmlWorkflowSpecificationConnectionType.FromNodeID.FirstOrDefault(n => n?.Value == node),
//                        string.Format(invalidPropertyMessage,
//                            "FromNodeIDs"));
//                }
//            }
//        }

//        /// <summary>
//        ///     Checks to node identifier.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        private static void CheckToNodeId(WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            // collections are never null in domain - always empty
//            if (workflowSpecificationConnection.ToNodeIDs.Count == 0)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationConnectionType.ToNodeID,
//                    string.Format(invalidPropertyMessage,
//                        "ToNodeID"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationConnectionType.ToNodeID.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "ToNodeID"));
//                Assert.AreEqual(workflowSpecificationConnection.ToNodeIDs.Count,
//                    b2mmlWorkflowSpecificationConnectionType.ToNodeID.Length,
//                    string.Format(invalidPropertyMessage,
//                        "ToNodeID"));
//                foreach (var node in workflowSpecificationConnection.ToNodeIDs)
//                {
//                    Assert.IsNotNull(
//                        b2mmlWorkflowSpecificationConnectionType.ToNodeID.FirstOrDefault(n => n?.Value == node),
//                        string.Format(invalidPropertyMessage,
//                            "ToNodeIDs"));
//                }
//            }
//        }

//        /// <summary>
//        ///     Checks the property.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        /// <param name="b2mmlWorkflowSpecificationConnectionType">Type of the b2 MML workflow specification connection.</param>
//        private static void CheckProperty(WorkflowSpecificationConnection workflowSpecificationConnection,
//            WorkflowSpecificationConnectionType b2mmlWorkflowSpecificationConnectionType)
//        {
//            // collections are never null in domain - always empty
//            if (workflowSpecificationConnection.Properties.Count == 0)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationConnectionType.Property,
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationConnectionType.Property.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//                Assert.AreEqual(workflowSpecificationConnection.Properties.Count,
//                    b2mmlWorkflowSpecificationConnectionType.Property.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//                foreach (var property in workflowSpecificationConnection.Properties)
//                {
//                    var b2mmlProperty =
//                        b2mmlWorkflowSpecificationConnectionType.Property.FirstOrDefault(c => c.ID?.Value == property.PropertyId);
//                    Assert.IsNotNull(b2mmlProperty,
//                        string.Format(invalidPropertyMessage,
//                            "Property"));
//                    WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapperTests
//                        .CheckWorkflowSpecificationPropertyType(property,
//                            b2mmlProperty);
//                }
//            }
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var workflowSpecificationConnectionToB2mmlMapper =
//                new WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlWorkflowSpecificationConnectionType =
//                    workflowSpecificationConnectionToB2mmlMapper.Map(default(WorkflowSpecificationConnection));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="workflowSpecificationConnection">The workflow specification connection.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(WorkflowSpecificationConnectionDataGenerator),
//                 nameof(WorkflowSpecificationConnectionDataGenerator.GenerateValidWorkflowSpecificationConnections))] WorkflowSpecificationConnection workflowSpecificationConnection)
//        {
//            // Arrange
//            var workflowSpecificationConnectionToB2mmlMapper =
//                new WorkflowSpecificationConnectionToWorkflowSpecificationConnectionTypeMapper();

//            // Act
//            var b2mmlWorkflowSpecificationConnectionType =
//                workflowSpecificationConnectionToB2mmlMapper.Map(workflowSpecificationConnection);

//            // Assert
//            CheckWorkflowSpecificationConnectionType(workflowSpecificationConnection,
//                b2mmlWorkflowSpecificationConnectionType);
//        }
//    }
//}