using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class EquipmentPropertyToEquipmentPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class EquipmentPropertyToEquipmentPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new EquipmentPropertyToEquipmentPropertyTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var equipment = new Equipment(hierarchyScope, "Equipment", EquipmentLevel.Site);
			var property = equipment.AddProperty("P1");
			equipment.AddProperty("Child", 2.AsValue(), property);

			// Act
			var result = mapper.Map(property);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.EquipmentProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("2", result.EquipmentProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}