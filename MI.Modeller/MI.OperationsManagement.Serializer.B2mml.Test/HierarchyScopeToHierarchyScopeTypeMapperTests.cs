﻿using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class HierarchyScopeToHierarchyScopeTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class HierarchyScopeToHierarchyScopeTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var mapper = new HierarchyScopeToHierarchyScopeTypeMapper();

			// Act
		    var result = mapper.Map(hierarchyScope);

			// Assert
			Assert.IsNotNull(result, "Mapper returned invalid result");
		    Assert.AreEqual("Ent", result.EquipmentID.Value, "Mapper returned result with invalid equipment id");
		    Assert.AreEqual("Enterprise", result.EquipmentLevel.Value, "Mapper returned result with invalid equipment level");
		}
    }
}