using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PersonnelClassToPersonnelClassTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PersonnelClassToPersonnelClassTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var personnelClass = new PersonnelClass(hierarchyScope, "PersonnelClass")
		    {
			    Description = "Description"
		    };
		    personnelClass.AddProperty("P1");
			var mapper = new PersonnelClassToPersonnelClassTypeMapper();

			// Act
		    var result = mapper.Map(personnelClass, false);

			// Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarcy scope");
		    Assert.AreEqual("PersonnelClass", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("P1", result.PersonnelClassProperty.SingleOrDefault()?.ID.Value, "Mapper returned result with invalid property");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var personnelClass = new PersonnelClass(hierarchyScope, "PersonnelClass");
		    var mapper = new PersonnelClassToPersonnelClassTypeMapper();

		    // Act
		    var result = mapper.Map(personnelClass, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarcy scope");
		    Assert.AreEqual("PersonnelClass", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.PersonnelClassProperty, "Mapper returned result with invalid property");
	    }
	}
}