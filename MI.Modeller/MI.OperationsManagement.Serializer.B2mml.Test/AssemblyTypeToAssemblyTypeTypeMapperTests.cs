using MI.Framework;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class AssemblyTypeToB2mmlMapperTests.
	/// </summary>
	[TestFixture]
    internal class AssemblyTypeToAssemblyTypeTypeMapperTests
    {
		[Test]
	    public void Test_MapFromEnumerator_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new AssemblyTypeToAssemblyTypeTypeMapper();
		    var assemblyType = AssemblyType.Logical;

		    // Act
		    var result = mapper.Map(assemblyType);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Logical", result.Value, "Mapper did not return expected value");
		    Assert.IsNull(result.OtherValue, "Mapper returned other value when none expected");
	    }

	    [Test]
	    public void Test_MapOtherValue_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new AssemblyTypeToAssemblyTypeTypeMapper();
		    var assemblyType = ExtensibleEnum<AssemblyType>.Parse("OtherValue");

		    // Act
		    var result = mapper.Map(assemblyType);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Other", result.Value, "Mapper did not return Other as value");
		    Assert.AreEqual("OtherValue", result.OtherValue, "Mapper did not return other value as expected");
	    }
	}
}