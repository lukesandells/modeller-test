﻿//using System;
//using System.Linq;
//using Castle.Core.Internal;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapperTests.
//    /// </summary>
//    [TestFixture]
//    internal class WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the workflow specification property.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        /// <param name="b2mmlWorkflowSpecificationPropertyType">Type of the b2 MML workflow specification property.</param>
//        public static void CheckWorkflowSpecificationPropertyType(
//            BasicProperty workflowSpecificationProperty,
//            WorkflowSpecificationPropertyType b2mmlWorkflowSpecificationPropertyType)
//        {
//            Assert.IsNotNull(b2mmlWorkflowSpecificationPropertyType,
//                "WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper returned a null instance for given valid input");

//            CheckId(workflowSpecificationProperty,
//                b2mmlWorkflowSpecificationPropertyType);
//            CheckDescription(workflowSpecificationProperty,
//                b2mmlWorkflowSpecificationPropertyType);
//            CheckValues(workflowSpecificationProperty,
//                b2mmlWorkflowSpecificationPropertyType);
//            CheckChildren(workflowSpecificationProperty,
//                b2mmlWorkflowSpecificationPropertyType);
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        /// <param name="b2mmlWorkflowSpecificationPropertyType">Type of the b2 MML workflow specification property.</param>
//        private static void CheckId(BasicProperty workflowSpecificationProperty,
//            WorkflowSpecificationPropertyType b2mmlWorkflowSpecificationPropertyType)
//        {
//            // InternalId may not be null either
//            Assert.IsNotNull(b2mmlWorkflowSpecificationPropertyType.ID,
//                string.Format(invalidPropertyMessage,
//                    "InternalId"));
//            Assert.AreEqual(workflowSpecificationProperty.PropertyId,
//                b2mmlWorkflowSpecificationPropertyType.ID.Value,
//                string.Format(invalidPropertyMessage,
//                    "ID"));
//        }

//        /// <summary>
//        ///     Checks the description.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        /// <param name="b2mmlWorkflowSpecificationPropertyType">Type of the b2 MML workflow specification property.</param>
//        private static void CheckDescription(BasicProperty workflowSpecificationProperty,
//            WorkflowSpecificationPropertyType b2mmlWorkflowSpecificationPropertyType)
//        {
//            if (workflowSpecificationProperty.Description == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationPropertyType.Description,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationPropertyType.Description.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(1,
//                    b2mmlWorkflowSpecificationPropertyType.Description.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(workflowSpecificationProperty.Description,
//                    b2mmlWorkflowSpecificationPropertyType.Description[0].Value,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//        }

//        /// <summary>
//        ///     Checks the values.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        /// <param name="b2mmlWorkflowSpecificationPropertyType">Type of the b2 MML workflow specification property.</param>
//        private static void CheckValues(BasicProperty workflowSpecificationProperty,
//            WorkflowSpecificationPropertyType b2mmlWorkflowSpecificationPropertyType)
//        {
//            // collections are never null in domain - always empty
//            if (!workflowSpecificationProperty.Values.Any())
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationPropertyType.Value,
//                    string.Format(invalidPropertyMessage,
//                        "Value"));
//            }
//            else
//            {
//                // cannpt check each value as the values are not keyed and have no unique mamdatory attributes
//                Assert.IsFalse(b2mmlWorkflowSpecificationPropertyType.Value.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Value"));
//                Assert.AreEqual(workflowSpecificationProperty.Values.Count(),
//                    b2mmlWorkflowSpecificationPropertyType.Value.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Value"));
//            }
//        }

//        /// <summary>
//        ///     Checks the children.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        /// <param name="b2mmlWorkflowSpecificationPropertyType">Type of the b2 MML workflow specification property.</param>
//        private static void CheckChildren(BasicProperty workflowSpecificationProperty,
//            WorkflowSpecificationPropertyType b2mmlWorkflowSpecificationPropertyType)
//        {
//            // collections are never null in domain - always empty
//            if (!workflowSpecificationProperty.Children.Any())
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationPropertyType.Property,
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationPropertyType.Property.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//                Assert.AreEqual(workflowSpecificationProperty.Children.Count(),
//                    b2mmlWorkflowSpecificationPropertyType.Property.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//                foreach (var child in workflowSpecificationProperty.Children)
//                {
//                    var b2mmlChild =
//                        b2mmlWorkflowSpecificationPropertyType.Property.FirstOrDefault(c => c.ID?.Value == child.PropertyId);
//                    Assert.IsNotNull(b2mmlChild,
//                        string.Format(invalidPropertyMessage,
//                            "Property"));
//                    CheckWorkflowSpecificationPropertyType(child,
//                        b2mmlChild);
//                }
//            }
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var workflowSpecificationPropertyToB2mmlMapper =
//                new WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlWorkflowSpecificationPropertyType =
//                    workflowSpecificationPropertyToB2mmlMapper.Map(default(BasicProperty));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="workflowSpecificationProperty">The workflow specification property.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(BasicPropertyDataGenerator),
//                 nameof(BasicPropertyDataGenerator.GenerateValidBasicProperties))] BasicProperty workflowSpecificationProperty)
//        {
//            // Arrange
//            var workflowSpecificationPropertyToB2mmlMapper =
//                new WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapper();

//            // Act
//            var b2mmlWorkflowSpecificationPropertyType =
//                workflowSpecificationPropertyToB2mmlMapper.Map(workflowSpecificationProperty);

//            // Assert
//            CheckWorkflowSpecificationPropertyType(workflowSpecificationProperty,
//                b2mmlWorkflowSpecificationPropertyType);
//        }
//    }
//}