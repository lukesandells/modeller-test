using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class MaterialClassPropertyToMaterialClassPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class MaterialClassPropertyToMaterialClassPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new MaterialClassPropertyToMaterialClassPropertyTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var materialClass = new MaterialClass(hierarchyScope, "MaterialClass");
			var property = materialClass.AddProperty("P1");
			materialClass.AddProperty("Child", 2.AsValue(), property);

			// Act
			var result = mapper.Map(property);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.MaterialClassProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("2", result.MaterialClassProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}