﻿//using System;
//using MI.Framework;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class ResourceRelationshipFormTypeToRelationshipFormTypeMapperTests.
//    /// </summary>
//    [TestFixture]
//    internal class ResourceRelationshipFormTypeToRelationshipFormTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the relationship form.
//        /// </summary>
//        /// <param name="resourceRelationshipFormType">Type of the resource relationship form.</param>
//        /// <param name="b2mmlRelationshipFormType">Type of the b2 MML relationship form.</param>
//        public static void CheckRelationshipFormType(
//            ExtensibleEnum<ResourceRelationshipFormType>? resourceRelationshipFormType,
//            RelationshipFormType b2mmlRelationshipFormType)
//        {
//            Assert.IsNotNull(b2mmlRelationshipFormType,
//                "ResourceRelationshipFormTypeToRelationshipFormTypeMapper returned a null instance for ResourceRelationshipFormType given valid input");
//            Assert.AreEqual(((ResourceRelationshipFormType) resourceRelationshipFormType).ToString(),
//                b2mmlRelationshipFormType.Value,
//                string.Format(invalidPropertyMessage,
//                    "Value"));
//            Assert.AreEqual(resourceRelationshipFormType?.HasOtherValue ?? false
//                    ? resourceRelationshipFormType.ToString()
//                    : null,
//                b2mmlRelationshipFormType.OtherValue,
//                string.Format(invalidPropertyMessage,
//                    "OtherValue"));
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var resourceRelationshipFormTypeToB2mmlMapper =
//                new ResourceRelationshipFormTypeToRelationshipFormTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlRelationshipFormType =
//                    resourceRelationshipFormTypeToB2mmlMapper.Map(default(ResourceRelationshipFormType));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "ResourceRelationshipFormTypeToRelationshipFormTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="resourceRelationshipFormType">Type of the resource relationship form.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance([ValueSource(
//                typeof(ResourceRelationshipFormTypeDataGenerator),
//                nameof(ResourceRelationshipFormTypeDataGenerator.GenerateValidResourceRelationshipFormTypes))]
//            ResourceRelationshipFormType resourceRelationshipFormType)
//        {
//            // Arrange
//            var resourceRelationshipFormTypeToB2mmlMapper =
//                new ResourceRelationshipFormTypeToRelationshipFormTypeMapper();

//            // Act
//            var b2mmlRelationshipFormType = resourceRelationshipFormTypeToB2mmlMapper.Map(resourceRelationshipFormType);

//            // Assert
//            CheckRelationshipFormType(resourceRelationshipFormType,
//                b2mmlRelationshipFormType);
//        }
//    }
//}