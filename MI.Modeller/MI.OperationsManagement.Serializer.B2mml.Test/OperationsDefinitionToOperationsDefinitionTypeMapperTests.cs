using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class OperationsDefinitionToOperationsDefinitionTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class OperationsDefinitionToOperationsDefinitionTypeMapperTests
    {
	    [Test]
	    public void Test_Map_MapsCorrectly()
	    {
		    // Arrange
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var operationsDefinition = new OperationsDefinition(hierarchyScope, "OpsDef")
			{
				Description = "Description",
				BillOfResourcesId = "BillOfResources",
				ProcessType = ProcessType.Maintenance
			};
			operationsDefinition.AddOperationsSegment(new OperationsSegment(hierarchyScope, "OpsSegment"));
			operationsDefinition.AddOperationsMaterialBill(new OperationsMaterialBill(hierarchyScope, "OperationsMaterialBill"));
		    var mapper = new OperationsDefinitionToOperationsDefinitionTypeMapper();

			// Act
		    var result = mapper.Map(operationsDefinition, false);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("OpsDef", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.AreEqual("Description", result.Description.SingleOrDefault()?.Value, "Mapper returned result with invalid description");
		    Assert.AreEqual("BillOfResources", result.BillOfResourcesID.Value, "Mapper returned result with invalid bill of resources");
		    Assert.AreEqual("Maintenance", result.OperationsType.Value, "Mapper returned result with invalid operations type");
		}

	    [Test]
	    public void Test_MapBasic_MapsCorrectly()
	    {
		    // Arrange
		    var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
		    var operationsDefinition = new OperationsDefinition(hierarchyScope, "OpsDef");
		    var mapper = new OperationsDefinitionToOperationsDefinitionTypeMapper();

		    // Act
		    var result = mapper.Map(operationsDefinition, true);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Ent", result.HierarchyScope.EquipmentID.Value, "Mapper returned result with invalid hierarchy scope");
		    Assert.AreEqual("OpsDef", result.ID.Value, "Mapper returned result with invalid id");
		    Assert.IsNull(result.Description, "Mapper returned result with invalid description");
		    Assert.IsNull(result.OperationsMaterialBill, "Mapper returned result with invalid bill of materials");
		    Assert.IsNull(result.BillOfResourcesID, "Mapper returned result with invalid bill of resources");
		    Assert.IsNull(result.OperationsType, "Mapper returned result with invalid operations type");
		    Assert.IsNull(result.OperationsSegment, "Mapper returned result with invalid operations segment");
	    }
	}
}