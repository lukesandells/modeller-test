using MI.OperationsManagement.Domain.Core.DataTypes;
using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class MaterialSpecificationPropertyToMaterialSegmentSpecificationPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class MaterialSpecificationPropertyToMaterialSegmentSpecificationPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new MaterialSpecificationPropertyToMaterialSegmentSpecificationPropertyTypeMapper<WorkMaster>();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var materialClass = new MaterialClass(hierarchyScope, "MaterialClass");
			var parentProperty = materialClass.AddProperty("P1");
			var correspondingProperty = materialClass.AddProperty("Child", 2.AsValue(), parentProperty);
			var materialSpecification = new MaterialSpecification<WorkMaster>(hierarchyScope, "MaterialSpecification");
			materialSpecification.AddProperty(correspondingProperty, 3.AsValue());

			// Act
			var result = mapper.Map(materialSpecification.Properties.Single());

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual("Child", result.MaterialSegmentSpecificationProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("3", result.MaterialSegmentSpecificationProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}