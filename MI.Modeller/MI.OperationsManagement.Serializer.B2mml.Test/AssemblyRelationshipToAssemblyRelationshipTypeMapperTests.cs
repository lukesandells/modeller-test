﻿using MI.Framework;
using MI.OperationsManagement.Domain.Core;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>sss
	///     Class AssemblyRelationshipToAssemblyRelationshipTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class AssemblyRelationshipToAssemblyRelationshipTypeMapperTests
    {
		[Test]
		public void Test_MapFromEnumerator_MapsCorrectly()
		{
			// Arrange
			var mapper = new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper();
			var assemblyRelationship = AssemblyRelationship.Transient;

			// Act
			var result = mapper.Map(assemblyRelationship);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("Transient", result.Value, "Mapper did not return expected value");
			Assert.IsNull(result.OtherValue, "Mapper returned other value when none expected");
		}

	    [Test]
	    public void Test_MapOtherValue_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new AssemblyRelationshipTypeToAssemblyRelationshipTypeMapper();
		    var assemblyRelationship = ExtensibleEnum<AssemblyRelationship>.Parse("OtherValue");

		    // Act
		    var result = mapper.Map(assemblyRelationship);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Other", result.Value, "Mapper did not return Other as value");
		    Assert.AreEqual("OtherValue", result.OtherValue, "Mapper did not return other value as expected");
	    }
	}
}