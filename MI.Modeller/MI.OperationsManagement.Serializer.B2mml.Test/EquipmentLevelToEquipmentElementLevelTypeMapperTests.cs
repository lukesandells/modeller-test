﻿using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	[TestFixture]
    internal class EquipmentLevelToEquipmentElementLevelTypeMapperTests
    {
		[Test]
	    public void Test_MapFromEnumerator_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new EquipmentLevelToEquipmentElementLevelTypeMapper();
		    var equipmentLevel = EquipmentLevel.Area;

		    // Act
		    var result = mapper.Map(equipmentLevel);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Area", result.Value, "Mapper did not return expected value");
		    Assert.IsNull(result.OtherValue, "Mapper returned other value when none expected");
	    }

	    [Test]
	    public void Test_MapOtherValue_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new EquipmentLevelToEquipmentElementLevelTypeMapper();
		    var equipmentLevel = ExtensibleEnum<EquipmentLevel>.Parse("OtherValue");

		    // Act
		    var result = mapper.Map(equipmentLevel);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Other", result.Value, "Mapper did not return Other as value");
		    Assert.AreEqual("OtherValue", result.OtherValue, "Mapper did not return other value as expected");
	    }
	}
}