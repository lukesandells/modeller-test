﻿//using System;
//using System.Linq;
//using Castle.Core.Internal;
//using MI.Modeller.Domain.Client;
//using MI.Modeller.Test.DataGenerators.DomainDataGenerator;
//using NUnit.Framework;

//namespace MI.OperationsManagement.Serializer.B2mml.Test
//{
//    /// <summary>
//    ///     Class WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapperTests.
//    /// </summary>
//    [TestFixture]
//    internal class WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapperTests
//    {
//        /// <summary>
//        ///     The invalid property message
//        /// </summary>
//        private static readonly string invalidPropertyMessage = "Mapping resulted in an invalid value for {0} property";

//        /// <summary>
//        ///     Checks the type of the workflow specification node.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <param name="b2mmlWorkflowSpecificationNodeType">Type of the b2 MML workflow specification node.</param>
//        public static void CheckWorkflowSpecificationNodeType(WorkflowSpecificationNode workflowSpecificationNode,
//            WorkflowSpecificationNodeType b2mmlWorkflowSpecificationNodeType)
//        {
//            Assert.IsNotNull(b2mmlWorkflowSpecificationNodeType,
//                "WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper returned a null instance for given valid input");

//            CheckId(workflowSpecificationNode,
//                b2mmlWorkflowSpecificationNodeType);
//            CheckDescription(workflowSpecificationNode,
//                b2mmlWorkflowSpecificationNodeType);
//            CheckNodeType(workflowSpecificationNode,
//                b2mmlWorkflowSpecificationNodeType);
//            CheckWorkflowSpecification(workflowSpecificationNode,
//                b2mmlWorkflowSpecificationNodeType);
//            CheckProperties(workflowSpecificationNode,
//                b2mmlWorkflowSpecificationNodeType);
//        }

//        /// <summary>
//        ///     Checks the identifier.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <param name="b2mmlWorkflowSpecificationNodeType">Type of the b2 MML workflow specification node.</param>
//        private static void CheckId(WorkflowSpecificationNode workflowSpecificationNode,
//            WorkflowSpecificationNodeType b2mmlWorkflowSpecificationNodeType)
//        {
//            // InternalId may not be null either
//            Assert.IsNotNull(b2mmlWorkflowSpecificationNodeType.ID,
//                string.Format(invalidPropertyMessage,
//                    "InternalId"));
//            Assert.AreEqual(workflowSpecificationNode.Id,
//                b2mmlWorkflowSpecificationNodeType.ID.Value,
//                string.Format(invalidPropertyMessage,
//                    "ID"));
//        }

//        /// <summary>
//        ///     Checks the description.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <param name="b2mmlWorkflowSpecificationNodeType">Type of the b2 MML workflow specification node.</param>
//        private static void CheckDescription(WorkflowSpecificationNode workflowSpecificationNode,
//            WorkflowSpecificationNodeType b2mmlWorkflowSpecificationNodeType)
//        {
//            if (workflowSpecificationNode.Description == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationNodeType.Description,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationNodeType.Description.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(1,
//                    b2mmlWorkflowSpecificationNodeType.Description.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//                Assert.AreEqual(workflowSpecificationNode.Description,
//                    b2mmlWorkflowSpecificationNodeType.Description[0].Value,
//                    string.Format(invalidPropertyMessage,
//                        "Description"));
//            }
//        }

//        /// <summary>
//        ///     Checks the type of the node.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <param name="b2mmlWorkflowSpecificationNodeType">Type of the b2 MML workflow specification node.</param>
//        private static void CheckNodeType(WorkflowSpecificationNode workflowSpecificationNode,
//            WorkflowSpecificationNodeType b2mmlWorkflowSpecificationNodeType)
//        {
//            if (workflowSpecificationNode.NodeType == default(string))
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationNodeType.NodeType,
//                    string.Format(invalidPropertyMessage,
//                        "NodeType"));
//            }
//            else
//            {
//                Assert.AreEqual(workflowSpecificationNode.NodeType,
//                    b2mmlWorkflowSpecificationNodeType.NodeType.Value,
//                    string.Format(invalidPropertyMessage,
//                        "NodeType"));
//            }
//        }

//        /// <summary>
//        ///     Checks the workflow specification.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <param name="b2mmlWorkflowSpecificationNodeType">Type of the b2 MML workflow specification node.</param>
//        private static void CheckWorkflowSpecification(WorkflowSpecificationNode workflowSpecificationNode,
//            WorkflowSpecificationNodeType b2mmlWorkflowSpecificationNodeType)
//        {
//            if (workflowSpecificationNode.WorkflowSpecification == null)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationNodeType.WorkflowSpecification,
//                    string.Format(invalidPropertyMessage,
//                        "WorkflowSpecification"));
//            }
//            else
//            {
//                WorkflowSpecificationToWorkflowSpecificationTypeMapperTests.CheckWorkflowSpecificationType(
//                    workflowSpecificationNode.WorkflowSpecification,
//                    b2mmlWorkflowSpecificationNodeType.WorkflowSpecification);
//            }
//        }

//        /// <summary>
//        ///     Checks the properties.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        /// <param name="b2mmlWorkflowSpecificationNodeType">Type of the b2 MML workflow specification node.</param>
//        private static void CheckProperties(WorkflowSpecificationNode workflowSpecificationNode,
//            WorkflowSpecificationNodeType b2mmlWorkflowSpecificationNodeType)
//        {
//            // collections are never null in domain - always empty
//            if (workflowSpecificationNode.Properties.Count == 0)
//            {
//                Assert.IsNull(b2mmlWorkflowSpecificationNodeType.Property,
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//            }
//            else
//            {
//                Assert.IsFalse(b2mmlWorkflowSpecificationNodeType.Property.IsNullOrEmpty(),
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//                Assert.AreEqual(workflowSpecificationNode.Properties.Count,
//                    b2mmlWorkflowSpecificationNodeType.Property.Length,
//                    string.Format(invalidPropertyMessage,
//                        "Property"));
//                foreach (var property in workflowSpecificationNode.Properties)
//                {
//                    var b2mmlProperty =
//                        b2mmlWorkflowSpecificationNodeType.Property.FirstOrDefault(c => c.ID?.Value == property.PropertyId);
//                    Assert.IsNotNull(b2mmlProperty,
//                        string.Format(invalidPropertyMessage,
//                            "Property"));
//                    WorkflowSpecificationPropertyToWorkflowSpecificationPropertyTypeMapperTests
//                        .CheckWorkflowSpecificationPropertyType(property,
//                            b2mmlProperty);
//                }
//            }
//        }

//        /// <summary>
//        ///     Tests the map null reference throws exception.
//        /// </summary>
//        [Test]
//        public void Test_Map_NullReference_ThrowsException()
//        {
//            // Arrange
//            var workflowSpecificationNodeToB2mmlMapper =
//                new WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper();
//            Exception mappingException = null;

//            // Act
//            try
//            {
//                // ReSharper disable once UnusedVariable
//                var b2mmlWorkflowSpecificationNodeType =
//                    workflowSpecificationNodeToB2mmlMapper.Map(default(WorkflowSpecificationNode));
//            }
//            catch (Exception e)
//            {
//                mappingException = e;
//            }

//            // Assert
//            Assert.IsNotNull(mappingException,
//                "WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper did not throw an exception with Null input reference");
//        }

//        /// <summary>
//        ///     Tests the map valid inputs returns valid instance.
//        /// </summary>
//        /// <param name="workflowSpecificationNode">The workflow specification node.</param>
//        [Test]
//        public void Test_Map_ValidInputs_ReturnsValidInstance(
//            [ValueSource(typeof(WorkflowSpecificationNodeDataGenerator),
//                 nameof(WorkflowSpecificationNodeDataGenerator.GenerateValidWorkflowSpecificationNodes))] WorkflowSpecificationNode workflowSpecificationNode)
//        {
//            // Arrange
//            var workflowSpecificationNodeToB2mmlMapper =
//                new WorkflowSpecificationNodeToWorkflowSpecificationNodeTypeMapper();

//            // Act
//            var b2mmlWorkflowSpecificationNodeType =
//                workflowSpecificationNodeToB2mmlMapper.Map(workflowSpecificationNode);

//            // Assert
//            CheckWorkflowSpecificationNodeType(workflowSpecificationNode,
//                b2mmlWorkflowSpecificationNodeType);
//        }
//    }
//}