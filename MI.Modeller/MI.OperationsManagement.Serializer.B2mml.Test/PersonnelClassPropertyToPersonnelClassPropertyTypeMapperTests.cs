using NUnit.Framework;
using System.Linq;
using MI.OperationsManagement.Domain.MasterData.Design;
using MI.OperationsManagement.Domain.MasterData.Design.Resources;
using MI.OperationsManagement.Serializer.B2mml.Mappers;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class PersonnelClassPropertyToPersonnelClassPropertyTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class PersonnelClassPropertyToPersonnelClassPropertyTypeMapperTests
    {
		[Test]
		public void Test_Map_MapsCorrectly()
		{
			// Arrange
			var mapper = new PersonnelClassPropertyToPersonnelClassPropertyTypeMapper();
			var hierarchyScope = new HierarchyScope("Ent", EquipmentLevel.Enterprise);
			var personnelClass = new PersonnelClass(hierarchyScope, "PersonnelClass");
			var property = personnelClass.AddProperty("P1");
			personnelClass.AddProperty("Child", 2, property);

			// Act
			var result = mapper.Map(property);

			// Assert
			Assert.IsNotNull(result, "Mapper returned null result");
			Assert.AreEqual("P1", result.ID.Value, "Mapper mapped incorrect ID");
			Assert.AreEqual(1, result.PersonnelClassProperty.Length, "Mapper mapped incorrect number of children");
			Assert.AreEqual("Child", result.PersonnelClassProperty.SingleOrDefault()?.ID.Value, "Mapper mapped incorrect child");
			Assert.AreEqual("2", result.PersonnelClassProperty.SingleOrDefault()?.Value.SingleOrDefault()?.ValueString.Value,
				"Mapper mapped incorrect child");
		}
	}
}