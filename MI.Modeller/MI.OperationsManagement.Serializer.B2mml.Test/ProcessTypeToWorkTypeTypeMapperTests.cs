﻿using MI.Framework;
using MI.OperationsManagement.Domain.MasterData.Design.ProcessDefinitions;
using MI.OperationsManagement.Serializer.B2mml.Mappers;
using NUnit.Framework;

namespace MI.OperationsManagement.Serializer.B2mml.Test
{
	/// <summary>
	///     Class ProcessTypeToWorkTypeTypeMapperTests.
	/// </summary>
	[TestFixture]
    internal class ProcessTypeToWorkTypeTypeMapperTests
    {
		[Test]
	    public void Test_MapFromEnumerator_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new ProcessTypeToWorkTypeTypeMapper();
		    var processType = ProcessType.Inventory;

		    // Act
		    var result = mapper.Map(processType, false);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Inventory", result.Value, "Mapper did not return expected value");
		    Assert.IsNull(result.OtherValue, "Mapper returned other value when none expected");
	    }

	    [Test]
	    public void Test_MapOtherValue_MapsCorrectly()
	    {
		    // Arrange
		    var mapper = new ProcessTypeToWorkTypeTypeMapper();
		    var processType = ExtensibleEnum<ProcessType>.Parse("OtherValue");

		    // Act
		    var result = mapper.Map(processType, false);

		    // Assert
		    Assert.IsNotNull(result, "Mapper returned null result");
		    Assert.AreEqual("Other", result.Value, "Mapper did not return Other as value");
		    Assert.AreEqual("OtherValue", result.OtherValue, "Mapper did not return other value as expected");
	    }
	}
}