﻿namespace MI.Framework.Persistence
{
	/// <summary>
	/// Governs the behaviour of the <see cref="PersistenceScope.Persist(object, PersistBehaviour)"/> method.
	/// </summary>
	public enum PersistBehaviour
	{
		/// <summary>
		/// Query the database to determine whether the entity to be persistent is persistent or transient if
		/// not inferrable from the object state.
		/// </summary>
		QueryIfNotInferable,

		/// <summary>
		/// Assume the entity to be persisted is transient if not inferrable from the object state.
		/// </summary>
		SaveIfNotInferable,
	}
}
