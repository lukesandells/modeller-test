using System.Collections.Generic;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Represents a stateful persistence session.
	/// </summary>
	public interface IStatefulPersistenceSession : IPersistenceSession
	{
		/// <summary>
		/// The entities held in the session cache.
		/// </summary>
		IEnumerable<object> Entities { get; }

		/// <summary>
		/// Flushes unsaved entities to persistent storage.
		/// </summary>
		void Flush();

		/// <summary>
		/// Attaches an entity to this session.
		/// </summary>
		/// <param name="entity">The entity to attach.</param>
		void Attach( object entity );

		/// <summary>
		/// Re-read the state of the given instance from the underlying database.
		/// </summary>
		/// <param name="entity">The persistent entity to refresh.</param>
		void Refresh(object entity);

		/// <summary>
		/// Evicts the given entity from this session.
		/// </summary>
		/// <param name="entity">The entity to evict.</param>
		void Evict( object entity );

		/// <summary>
		/// Clears the session.
		/// </summary>
		void Clear();

		/// <summary>
		/// Intialises a lazy loaded entity.
		/// </summary>
		/// <param name="entity">The entity to initialise.</param>
		void Initialise( object entity );

		/// <summary>
		/// Persists a transient or persistent entity to the database.
		/// </summary>
		/// <param name="entity">The entity to persist.</param>
		/// <param name="behaviour">The required behaviour of the persist operation.</param>
		void Persist(object entity, PersistBehaviour behaviour = PersistBehaviour.QueryIfNotInferable);

		/// <summary>
		/// Saves a transient entity to the database.
		/// </summary>
		/// <param name="entity">The entity to save.</param>
		void Save(object entity);

		/// <summary>
		/// Updates a persistent entity in the database.
		/// </summary>
		/// <param name="entity">The entity to update.</param>
		void Update(object entity);

		/// <summary>
		/// Merges an entity into the database.
		/// </summary>
		/// <param name="entity">The transient or persistent entity to be merged into the database.</param>
		/// <returns>The newly merged entity.</returns>
		/// <remarks>
		/// If the given entity is not resident in the session it is loaded from the database before being merged
		/// with the given <paramref name="entity"/>.
		/// </remarks>
		T Merge<T>(T entity) where T : class;

		/// <summary>
		/// Retrieves all entities of the type designated in the type parameter.
		/// </summary>
		/// <param name="cacheable">Indicates whether the results should be cached in the second level cache.</param>
		/// <returns>A list of all entities.</returns>
		IList<T> ListAll<T>(bool cacheable) where T : class;

		/// <summary>
		/// Deletes a persistent entity.
		/// </summary>
		/// <param name="entity">The persistent entity to be deleted.</param>
		void Delete(object entity);
	}
}
