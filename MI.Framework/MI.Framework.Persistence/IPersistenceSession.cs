﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Represents a persistence session.
	/// </summary>
	public interface IPersistenceSession : IDisposable
	{
		/// <summary>
		/// Begins a transaction on the session.
		/// </summary>
		/// <param name="isolationLevel">The tansaction isolation level.</param>
		/// <returns>The new transaction.</returns>
		ITransaction BeginTransaction(IsolationLevel isolationLevel);

		/// <summary>
		/// Gets the current transaction.
		/// </summary>
		ITransaction Transaction { get; }

		/// <summary>
		/// Gets the current database connection.
		/// </summary>
		DbConnection Connection { get; }

		/// <summary>
		/// Closes this session.
		/// </summary>
		void Close();

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		T FindById<T>(object id) where T : class;

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		T FindById<T>(object id, bool acquireLock) where T : class;

		/// <summary>
		/// Retrieves all entities of the type designated in the type parameter.
		/// </summary>
		/// <returns>A list of all entities.</returns>
		IList<T> ListAll<T>() where T : class;

		/// <summary>
		/// Provides a queryable interface for objects of the given type.
		/// </summary>
		/// <typeparam name="T">The type of entity to find.</typeparam>
		/// <returns>A queryable interface objects of the given type.</returns>
		IQueryable<T> Query<T>() where T : class;
	}
}
