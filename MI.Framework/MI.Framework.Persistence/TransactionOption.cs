namespace MI.Framework.Persistence
{
	/// <summary>
	/// Specifies the transaction mode.
	/// </summary>
	public enum TransactionOption
	{
		/// <summary>
		/// No database transaction will be created.
		/// </summary>
		None = 0,

		/// <summary>
		/// Database transactions are supported.  The persistence session will be enlisted
		/// in a transaction if one already exists.  Otherwise, the session will operate without
		/// a database transaction.
		/// </summary>
		Supported = 1,

		/// <summary>
		/// A database transaction is required.  If one already exists, the persistence session
		/// will be enlisted in this transaction.  If not, a new transaction will be created.
		/// </summary>
		Required = 2,

		/// <summary>
		/// A new database transaction is required.  Regardless of whether a database transaction is
		/// already in progress on the currently executing thread, the persistence session will execute
		/// within the context of a new database transaction.
		/// </summary>
		RequiresNew = 3
	}
}
