using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Linq;

using MI.Framework.Logging;
using MI.Framework.Contexts;
using MI.Framework.Validation;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Demarks a segment of executing code at runtime with certain persistence-related characteristics.
	/// </summary>
	public sealed class PersistenceScope : Scope
	{
		/// <summary>
		/// Log for code instrumentation
		/// </summary>
		private static readonly ILog _log = LogManager.GetLogger( MethodBase.GetCurrentMethod().DeclaringType );

		/// <summary>
		/// Creates a new transactional persistence context for managing persistence session and transactions with 
		/// "read committed" transaction isolation.
		/// </summary>
		public PersistenceScope() 
			: this( TransactionOption.Required, IsolationLevel.ReadCommitted )
		{
		}

		/// <summary>
		/// Creates a new persistence context for managing persistence session and transactions with "read committed" 
		/// transaction isolation.
		/// </summary>
		/// <param name="txOption">Determines whether a transaction is required for this transaction context.  See 
		/// <see cref="TransactionOption"/> for more information.</param>
		public PersistenceScope( TransactionOption txOption ) 
			: this( txOption, txOption == TransactionOption.Required 
			|| txOption == TransactionOption.RequiresNew ? IsolationLevel.ReadCommitted : IsolationLevel.Unspecified )
		{
		}

		/// <summary>
		/// Creates a new persistence context for managing persistence session and transactions.
		/// </summary>
		/// <param name="txOption">Determines whether a transaction is required for this transaction context.  See 
		/// <see cref="TransactionOption"/> for more information.</param>
		/// <param name="txIsolationLevel">Specifieds the level of transaction isolation for the persistence context.
		/// See <see cref="System.Data.IsolationLevel"/> for more information.</param>
		public PersistenceScope( TransactionOption txOption, IsolationLevel txIsolationLevel ) 
			: base( new PersistenceContextFactory( txOption, txIsolationLevel, false ) )
		{
			// Log operation commencement for debug purposes
			if( _log.IsDebugEnabled )
			{
				_log.DebugFormat( "Opening persistence scope {{{0}}}: TxMode: {1}, TxIsolationLevel: {2}", 
					Context.ContextId.ToString(), txOption.ToString(), txIsolationLevel.ToString() );
			}
		}

		/// <summary>
		/// Gets the current <see cref="PersistenceScope" /> object representing the validation context demarked by
		/// this <see cref="Scope" /> instance.
		/// </summary>
		private new PersistenceContext Context
		{
			get{ return ( PersistenceContext )base.Context; }
		}

		/// <summary>
		/// Gets the current persistence session
		/// </summary>
		public IStatefulPersistenceSession Session
		{
			get{ return (IStatefulPersistenceSession)Context.Session; }
		}

		/// <summary>
		/// Attaches the specified object instance to the persistence session.
		/// </summary>
		/// <param name="instance">The object to be attached to the persistence session.</param>
		public void Attach( object instance )
		{
			Session.Attach( instance );
		}

		/// <summary>
		/// Re-read the state of the given instance from the underlying database.
		/// </summary>
		/// <param name="entity">The persistent entity to refresh.</param>
		public void Refresh(object entity)
		{
			Session.Refresh(entity);
		}

		/// <summary>
		/// Evicts the given entity from the persistence session.
		/// </summary>
		/// <param name="entity">The entity to evict.</param>
		public void Evict( object entity )
		{
			Session.Evict( entity );
		}

		/// <summary>
		/// Clears the session.
		/// </summary>
		public void Clear()
		{
			Session.Clear();
		}

		/// <summary>
		/// Initialises a lazy-loaded object.
		/// </summary>
		/// <param name="entity"></param>
		/// <remarks>
		/// Use this method to ensure an object has been initialised.  This guarantees that the object has been loaded.
		/// </remarks>
		public void Initialise( object entity )
		{
			Session.Initialise(entity);
		}

		/// <summary>
		/// Initialises a lazy-loaded field of a persistent object.
		/// </summary>
		/// <param name="entity">The persistent object containing the lazy-loaded field to be initialised.</param>
		/// <param name="fieldName">The name of the private field of the persistent object.</param>
		public void Initialise( object entity, string fieldName )
		{
			Session.Initialise( GetFieldValue( entity, fieldName ) );
		}

		/// <summary>
		/// Persists an entity to the database.
		/// </summary>
		/// <remarks>
		/// </remarks>
		/// <typeparam name="T">The type of entity to persist.</typeparam>
		/// <param name="entity">The entity to persist.</param>
		/// <param name="behaviour">The required behaviour of the persist operation.</param>
		public void Persist(object entity, PersistBehaviour behaviour = PersistBehaviour.QueryIfNotInferable)
		{
			Session.Persist(entity, behaviour);
		}

		/// <summary>
		/// Saves a transient entity to the database.
		/// </summary>
		/// <param name="entity">The entity to save.</param>
		public void Save(object entity)
		{
			Session.Save(entity);
		}

		/// <summary>
		/// Updates a persistent entity in the database.
		/// </summary>
		/// <param name="entity">The entity to update.</param>
		public void Update(object entity)
		{
			Session.Update(entity);
		}

		/// <summary>
		/// Merges an entity into the database.
		/// </summary>
		/// <param name="entity">The transient or persistent entity to be merged into the database.</param>
		/// <returns>The newly merged entity.</returns>
		/// <remarks>
		/// If the given entity is not resident in the session it is loaded from the database before being merged
		/// with the given <paramref name="entity"/>.
		/// </remarks>
		public T Merge<T>(T entity) where T : class
		{
			return Session.Merge(entity);
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id) where T : class
		{
			return Session.FindById<T>(id);
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id, bool acquireLock) where T : class
		{
			return Session.FindById<T>(id, acquireLock);
		}

		/// <summary>
		/// Retrieves all entities of the type designated in the type parameter.
		/// </summary>
		/// <param name="cacheable">Indicates whether the results should be cached in the second level cache.</param>
		/// <returns>A list of all entities.</returns>
		public IList<T> ListAll<T>(bool cacheable) where T : class
		{
			return Session.ListAll<T>(cacheable);
		}

		/// <summary>
		/// Retrieves all entities of the type designated in the type parameter.
		/// </summary>
		/// <returns>A list of all entities.</returns>
		public IList<T> ListAll<T>() where T : class
		{
			return Session.ListAll<T>();
		}

		/// <summary>
		/// Deletes a persistent entity.
		/// </summary>
		/// <param name="entity">The persistent entity to be deleted.</param>
		public void Delete(object entity)
		{
			Session.Delete(entity);
		}

		/// <summary>
		/// Provides a queryable interface for objects of the given type.
		/// </summary>
		/// <typeparam name="T">The type of entity to find.</typeparam>
		/// <returns>A queryable interface objects of the given type.</returns>
		public IQueryable<T> Query<T>() where T : class
		{
			return Session.Query<T>();
		}

		/// <summary>
		/// Flushes unsaved entities to persistent storage.
		/// </summary>
		public void Flush()
		{
			Session.Flush();
		}
		
		/// <summary>
		/// Gets the value of a field (private or otherwise) for an object.
		/// </summary>
		/// <param name="target">The object from which to read the field value.</param>
		/// <param name="fieldName">The name of the field to read.</param>
		/// <returns>The value of the field with the given name on the designated object.</returns>
		private static object GetFieldValue( object target, string fieldName )
		{
			// Ensure arguments not null
			target = ArgumentValidation.AssertNotNull( target, "target" );
			fieldName = ArgumentValidation.AssertNotNull( fieldName, "fieldName" );

			// Get the field value
			return target.GetType().GetField( fieldName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic ).GetValue( target );
		}

		/// <summary>
		/// Indicates whether the currently executing transaction is consistent.
		/// </summary>
		/// <remarks>
		/// If <see cref="IsConsistent"/> is <c>True</c> at the time the <see cref="PersistenceScope" /> instance 
		/// is Disposed, then the underlying database transaction is committed.  Otherwise it is rolled back.
		/// </remarks>
		public bool IsConsistent
		{
			get{ return Context.IsConsistent; }
			set{ Context.IsConsistent = value; }
		}

		/// <summary>
		/// A <see cref="System.Boolean" /> indicating whether the <see cref="PersistenceScope" /> is currently
		/// executing within a database transaction.
		/// </summary>
		public bool IsInTransaction
		{
			get{ return Context.IsInTransaction; }
		}

		/// <summary>
		/// An <see cref="IsolationLevel" /> enumeration, indicating the current level of database transaction isolation.
		/// </summary>
		public IsolationLevel TxIsolationLevel
		{
			get{ return Context.TxIsolationLevel; }
		}

		/// <summary>
		/// A <see cref="TransactionOption" /> enumeration indicating the transaction mode of the current 
		/// <see cref="PersistenceScope" /> instance.
		/// </summary>
		public TransactionOption TxMode
		{
			get{ return Context.TxMode; }
		}

		/// <summary>
		/// Enlists an ADO.NET <see cref="DbCommand" /> in the current persistence scope.  The <see cref="DbCommand"/> 
		/// inherits the existing database connection and any existing transaction on that connection.
		/// </summary>
		/// <param name="command">An ADO.NET Command implementing the <see cref="DbCommand"/> interface to be 
		/// enrolled in the connection and transaction of the <see cref="PersistenceScope"/>.</param>
		public void Enlist( DbCommand command )
		{
			Context.Enlist( command );
		}

		/// <summary>
		/// Indicates the work related to this persistence scope is complete.
		/// </summary>
		public void Done()
		{
			IsConsistent = true;
		}

		/// <summary>
		/// Traps the Dispose event in order to log it
		/// </summary>
		/// <param name="disposing">A <see cref="System.Boolean" /> indicating that the method was invoked by
		/// a client calling the <see cref="Dispose"/> method rather than the garbage collector.</param>
		protected override void Dispose(bool disposing)
		{
			try
			{
				// Log the termination of the operation for debug purposes
				if( _log.IsDebugEnabled )
				{
					_log.DebugFormat( "Closing persistence scope {{{0}}}.", Context.ContextId.ToString() );
				}
			}
			finally
			{
				// Base method - actually does the clean up
				base.Dispose( disposing );
			}
		}
	}
}
