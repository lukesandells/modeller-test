using System.Data.Common;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Represents a transaction.
	/// </summary>
	public interface ITransaction
	{
		/// <summary>
		/// Commits the transaction.
		/// </summary>
		void Commit();
		
		/// <summary>
		/// Enlists the given <see cref="DbCommand"/> in this transaction.
		/// </summary>
		/// <param name="command"></param>
		void Enlist( DbCommand command );
		
		/// <summary>
		/// Forces the underlying transaction to roll back.
		/// </summary>
		void Rollback();
	}
}
