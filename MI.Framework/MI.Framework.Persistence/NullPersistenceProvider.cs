using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Represents a null persistence provider.
	/// </summary>
	/// <remarks>
	/// This persistence provider implements the necessary persitence provider interface, but provides no
	/// persistence functionality.
	/// </remarks>
	internal class NullPersistenceProvider : IPersistenceProvider
	{
		/// <summary>
		/// Represents a null transaction.
		/// </summary>
		private class NullTransaction : ITransaction
		{
			/// <summary>
			/// Commits the transaction.
			/// </summary>
			public void Commit()
			{
			}

			/// <summary>
			/// Enlists the given <see cref="DbCommand"/> in this transaction.
			/// </summary>
			/// <param name="command"></param>
			public void Enlist( DbCommand command )
			{
			}

			/// <summary>
			/// Forces the underlying transaction to roll back.
			/// </summary>
			public void Rollback()
			{
			}
		}

		/// <summary>
		/// Represents a null persistence session.
		/// </summary>
		private class NullPersistenceSession : IStatefulPersistenceSession, IStatelessPersistenceSession
		{
			/// <summary>
			/// The currently active transaction.
			/// </summary>
			private ITransaction _transaction;

			/// <summary>
			/// The entities held in the session cache.
			/// </summary>
			public IEnumerable<object> Entities => new object[] { };

			/// <summary>
			/// Begins a transaction on the session.
			/// </summary>
			/// <param name="isolationLevel">The tansaction isolation level.</param>
			/// <returns>The new transaction.</returns>
			public ITransaction BeginTransaction(IsolationLevel isolationLevel)
			{
				_transaction = new NullTransaction();
				return _transaction;
			}

			/// <summary>
			/// Gets the current transaction.
			/// </summary>
			public ITransaction Transaction
			{
				get { return _transaction; }
			}

			/// <summary>
			/// Gets the current database connection.
			/// </summary>
			public DbConnection Connection
			{
				get { return null; }
			}

			/// <summary>
			/// Closes this session.
			/// </summary>
			public void Close()
			{
			}

			/// <summary>
			/// Flushes unsaved entities to persistent storage.
			/// </summary>
			public void Flush()
			{
			}

			/// <summary>
			/// Attaches an entity to this session.
			/// </summary>
			/// <param name="entity">The entity to attach.</param>
			public void Attach(object entity)
			{
			}

			/// <summary>
			/// Stipulates whether the given entity's state is finalised and consistent with the databsae such that if it is finalised,
			/// the entity's state is not evaluated on <see cref="Flush"/> or <see cref="Scope.Dispose"/> to determine whether updates 
			/// are required to the database.
			/// </summary>
			/// <param name="entity">The entity that is to be marked as finalised or unfinalised.</param>
			/// <param name="finalised">Determines whether the given entity is finalised.</param>
			public void SetFinalised(object entity, bool isFinalised)
			{
			}

			/// <summary>
			/// Evicts the given entity from this session.
			/// </summary>
			/// <param name="entity">The entity to evict.</param>
			public void Evict(object entity)
			{
			}

			/// <summary>
			/// Intialises an lazy loaded entity.
			/// </summary>
			/// <param name="entity">The entity to initialise.</param>
			public void Initialise(object entity)
			{
			}

			/// <summary>
			/// Persists an entity to the database.
			/// </summary>
			/// <remarks>
			/// </remarks>
			/// <typeparam name="T">The type of entity to persist.</typeparam>
			/// <param name="entity">The entity to persist.</param>
			/// <param name="behaviour">The required behaviour of the persist operation.</param>
			public void Persist(object entity, PersistBehaviour behaviour = PersistBehaviour.QueryIfNotInferable)
			{
			}

			/// <summary>
			/// Saves a transient entity to the database.
			/// </summary>
			/// <param name="entity">The entity to save.</param>
			public void Save(object entity)
			{
			}

			/// <summary>
			/// Updates a persistent entity in the database.
			/// </summary>
			/// <param name="entity">The entity to update.</param>
			public void Update(object entity)
			{
			}

			/// <summary>
			/// Finds the entity with the given identifier.
			/// </summary>
			/// <param name="id">The database identifier of the entity to be retrieved.</param>
			/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
			public T FindById<T>(object id) where T : class
			{
				return null;
			}

			/// <summary>
			/// Finds the entity with the given identifier.
			/// </summary>
			/// <param name="id">The database identifier of the entity to be retrieved.</param>
			/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
			/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
			public T FindById<T>(object id, bool acquireLock) where T : class
			{
				return null;
			}

			/// <summary>
			/// Retrieves all entities of the type designated in the type parameter.
			/// </summary>
			/// <param name="cacheable">Indicates whether the results should be cached in the second level cache.</param>
			/// <returns>A list of all entities.</returns>
			public IList<T> ListAll<T>(bool cacheable) where T : class
			{
				return new List<T>();
			}

			/// <summary>
			/// Retrieves all entities of the type designated in the type parameter.
			/// </summary>
			/// <returns>A list of all entities.</returns>
			public IList<T> ListAll<T>() where T : class
			{
				return new List<T>();
			}

			/// <summary>
			/// Deletes a persistent entity from the persistence scope.
			/// </summary>
			/// <param name="entity">The persistent entity to be deleted.</param>
			public void Delete<T>(T entity) where T : class
			{
			}

			/// <summary>
			/// Queries the persistence scope.
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <returns></returns>
			public IQueryable<T> Query<T>() where T : class
			{
				return new List<T>().AsQueryable<T>();
			}

			/// <summary>
			/// Disposes the persistence session.
			/// </summary>
			public void Dispose()
			{
			}

			/// <summary>
			/// Clears the session.
			/// </summary>
			public void Clear()
			{ 
			}

			/// <summary>
			/// Merges an entity into the database.
			/// </summary>
			/// <param name="entity">The transient or persistent entity to be merged into the database.</param>
			/// <returns>The newly merged entity.</returns>
			/// <remarks>
			/// If the given entity is not resident in the session it is loaded from the database before being merged
			/// with the given <paramref name="entity"/>.
			/// </remarks>
			public T Merge<T>(T entity) where T : class
			{
				return entity;
			}

			/// <summary>
			/// Inserts an entity.
			/// </summary>
			/// <param name="entity">A new transient instance.</param>
			/// <returns>The identifier of the instance.</returns>
			public object Insert(object entity)
			{
				return new object();
			}

			/// <summary>
			/// Delete an entity.
			/// </summary>
			/// <param name="entity">A detached entity instance.</param>
			public void Delete(object entity)
			{
			}

			/// <summary>
			/// Re-read the state of the given instance from the underlying database.
			/// </summary>
			/// <param name="entity">The persistent entity to refresh.</param>
			public void Refresh(object entity)
			{
			}

			/// <summary>
			/// Refresh the entity instance state from the database.
			/// </summary>
			/// <param name="entity">The entity to be refreshed.</param>
			/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
			public void Refresh(object entity, bool acquireLock)
			{
			}
		}

		/// <summary>
		/// Opens a new persistence session.
		/// </summary>
		/// <param name="stateless">Stipulates whether to open a stateless session.</param>
		/// <returns>The new persistence session.</returns>
		public IPersistenceSession OpenSession( bool stateless )
		{
			return new NullPersistenceSession();
		}
	}
}
