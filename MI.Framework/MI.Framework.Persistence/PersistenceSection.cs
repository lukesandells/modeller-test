using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace Fortron.Framework.Persistence.Configuration
{
	/// <summary>
	/// Represents the persistence configuration section for the Fortron framework.
	/// </summary>
	/// <remarks>
	/// This class is implemented using the programmatic model rather than the declarative model because
	/// for some reason (probably a bug in .NET 2.0), you cannot make child elements REQUIRED using the 
	/// declarative model.  Since the <c><provider /></c> child element is required, the declarative model
	/// could not be used.
	/// </remarks>
	public class PersistenceSection : ConfigurationSection
	{
		/// <summary>
		/// The persistence configuration section name.
		/// </summary>
		public const string DefaultName = "fortron.framework.persistence";

		/// <summary>
		/// The configuration section properties.
		/// </summary>
		private static ConfigurationPropertyCollection _properties;

		/// <summary>
		/// Gets the configuration section properties.
		/// </summary>
		protected override ConfigurationPropertyCollection Properties
		{
			get { return _properties; }
		}

		/// <summary>
		/// The provider configuration.
		/// </summary>
		private static ConfigurationProperty _providerProperty;

		/// <summary>
		/// Gets the provider configuration.
		/// </summary>
		public ProviderElement Provider
		{
			get { return ( ProviderElement )this[ "provider" ]; }
		}

		/// <summary>
		/// Initialises the <see cref="PersistenceSection"/> class.
		/// </summary>
		static PersistenceSection()
		{
			// Create the configuration properties
			_providerProperty = new ConfigurationProperty( "provider", typeof( ProviderElement ), null, ConfigurationPropertyOptions.IsRequired );
			
			// Add the properties to the collection
			_properties = new ConfigurationPropertyCollection();
			_properties.Add( _providerProperty );
		}
	}
}
