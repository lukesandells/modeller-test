using System;
using System.Configuration;
using System.Collections.Specialized;

namespace MI.Framework.Persistence.Configuration
{
	/// <summary>
	/// Configuration element for a persistence provider.
	/// </summary>
	public class ProviderElement : ConfigurationElement
	{
		/// <summary>
		/// The fully qualified type name of the persistence provider.
		/// </summary>
		[ConfigurationProperty( "type", IsRequired = true )]
		public string TypeName
		{
			get { return ( string )this[ "type" ]; }
		}

		/// <summary>
		/// Gets the <see cref="Type"/> instance corresponding to the value of the <see cref="TypeName"/> property.
		/// </summary>
		public Type Type
		{
			get
			{
				// Attempt to load the type
				Type type = Type.GetType( this.TypeName, true, false );

				// Ensure the type is correct
				SubclassTypeValidator validator = new SubclassTypeValidator( typeof( IPersistenceProvider ) );
				validator.Validate( type );

				// Return the type
				return type;
			}
		}

		/// <summary>
		/// The additional provider parameters.
		/// </summary>
		private NameValueCollection _parameters = new NameValueCollection();

		/// <summary>
		/// Gets the additionally configured provider parameters.
		/// </summary>
		public NameValueCollection Parameters
		{
			get { return _parameters; }
		}

		/// <summary>
		/// Processes unrecognised attributes on the provider configuration element.
		/// </summary>
		/// <param name="name">The parameter name.</param>
		/// <param name="value">The parameter value.</param>
		/// <returns><c>true</c> if the parameter was successfully processed; otherwise <c>false</c>.</returns>
		protected override bool OnDeserializeUnrecognizedAttribute( string name, string value )
		{
			// Add the unrecognised attribute as a parameter
			_parameters.Add( name, value );

			// Handled okay
			return true;
		}
	}
}
