using System;
using System.Data;
using System.Data.SqlClient;

namespace Fortron.Framework.Persistence
{
	/// <summary>
	/// Factory helper class for creating instances of the <see cref="SqlParameter"/> class.
	/// </summary>
	public static class SqlParameterFactory
	{
		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class by inferring the type from that
		/// of the <paramref name="value"/> parameter.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="value">The parameter value.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and value.</returns>
		public static SqlParameter CreateParameter( string parameterName, object value )
		{
			return new SqlParameter( parameterName, value == null ? ( object )DBNull.Value : value );
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class by inferring the type from that
		/// of the <paramref name="value"/> parameter.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="value">The parameter value.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and value.</returns>
		public static SqlParameter CreateParameter( string parameterName, object value, SqlDbType dbType )
		{
			SqlParameter parameter = new SqlParameter( parameterName, dbType );
			parameter.Value = value == null ? ( object )DBNull.Value : value;
			return parameter;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class as a specific type.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and type.</returns>
		public static SqlParameter CreateParameter( string parameterName, SqlDbType dbType )
		{
			return new SqlParameter( parameterName, dbType );
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class as a specific type with a given size.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <param name="size">The length of the parameter.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and type.</returns>
		public static SqlParameter CreateParameter( string parameterName, SqlDbType dbType, int size )
		{
			return new SqlParameter( parameterName, dbType, size );
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class as a specific type with a given precision and scale.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <param name="precision">The precision of the parameter.</param>
		/// <param name="scale">The scale of the parameter.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and type.</returns>
		public static SqlParameter CreateParameter( string parameterName, SqlDbType dbType, byte precision, byte scale )
		{
			SqlParameter parameter = new SqlParameter( parameterName, dbType );
			parameter.Scale = scale;
			parameter.Precision = precision;

			return parameter;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class by inferring the type from that
		/// of the <paramref name="value"/> parameter, also specifying a parameter direction.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="value">The parameter value.</param>
		/// <param name="direction">The parameter direction.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name, value and direction.</returns>
		public static SqlParameter CreateParameter( string parameterName, object value, ParameterDirection direction )
		{
			SqlParameter parameter = new SqlParameter( parameterName, value );
			parameter.Direction = direction;
			return parameter;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class as a specific type and direction.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <param name="direction">The parameter direction.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name, type and direction.</returns>
		public static SqlParameter CreateParameter( string parameterName, SqlDbType dbType, ParameterDirection direction )
		{
			SqlParameter parameter = new SqlParameter( parameterName, dbType );
			parameter.Direction = direction;
			return parameter;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class as a specific type with a given size and direction.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <param name="size">The length of the parameter.</param>
		/// <param name="direction">The parameter direction.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and type.</returns>
		public static SqlParameter CreateParameter( string parameterName, SqlDbType dbType, int size, ParameterDirection direction )
		{
			SqlParameter parameter = new SqlParameter( parameterName, dbType, size );
			parameter.Direction = direction;
			return parameter;
		}

		/// <summary>
		/// Creates a new instance of the <see cref="SqlParameter"/> class as a specific type with a given precision, scale and direction.
		/// </summary>
		/// <param name="parameterName">The name of the SQL parameter.</param>
		/// <param name="dbType">The type to assign the new SQL parameter.</param>
		/// <param name="precision">The precision of the parameter.</param>
		/// <param name="scale">The scale of the parameter.</param>
		/// <param name="direction">The parameter direction.</param>
		/// <returns>A new <see cref="SqlParameter"/> object with the given name and type.</returns>
		public static SqlParameter CreateParameter( string parameterName, SqlDbType dbType, byte precision, byte scale, ParameterDirection direction )
		{
			SqlParameter parameter = new SqlParameter( parameterName, dbType );
			parameter.Scale = scale;
			parameter.Precision = precision;
			parameter.Direction = direction;

			return parameter;
		}
	}
}
