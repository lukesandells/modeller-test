using System;
using System.Data;
using System.Reflection;
using System.Configuration;
using System.Data.Common;

using MI.Framework.Logging;
using MI.Framework.Validation;
using MI.Framework.Contexts;
using MI.Framework.Persistence.Configuration;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// The PersistenceContext class acts as a piece of middleware between the application and the persistence library.
	/// </summary>
	/// <remarks>
	/// The PersistenceContext class provides an automated session/transaction enrolment mechanism whereby
	/// a common thread local persistence session can be accessed from within any container.  If the requested context 
	/// transaction attributes are compatible with the current persistence context, then the new context is simply
	/// enrolled in the existing context.  If not, a new context is created housing a new persistence session (and 
	/// transaction if so requested).
	/// </remarks>
	public sealed class PersistenceContext : Context, IDisposable
	{
		/// <summary>
		/// Log for code instrumentation
		/// </summary>
		private static readonly ILog _log = LogManager.GetLogger( MethodBase.GetCurrentMethod().DeclaringType );

		/// <summary>
		/// The synchronisation root.
		/// </summary>
		private static object _syncRoot = new object();

		/// <summary>
		/// Gets the synchronisation root.
		/// </summary>
		public static object SyncRoot
		{
			get { return _syncRoot; }
		}

		/// <summary>
		/// The configured persistence provider.
		/// </summary>
		private static IPersistenceProvider _provider;

		/// <summary>
		/// Gets the configured persistence provider.
		/// </summary>
		private static IPersistenceProvider Provider
		{
			get
			{
				lock(SyncRoot)
				{
					if( _provider == null )
					{
						// Get the persistence provider
						PersistenceSection configuration = ( PersistenceSection )ConfigurationManager.GetSection( PersistenceSection.DefaultName );
						if( configuration == null || !configuration.ElementInformation.IsPresent )
						{
							_provider = new NullPersistenceProvider();
						}
						else
						{
							_provider = ( IPersistenceProvider )Activator.CreateInstance( configuration.Provider.Type, configuration.Provider.Parameters );
						}
					}
				}

				return _provider;
			}
		}

		/// <summary>
		/// Sets the persistence provider.
		/// </summary>
		/// <param name="provider">The persistence provider to set.</param>
		public static void SetProvider(IPersistenceProvider provider)
		{
			_provider = ArgumentValidation.AssertNotNull(provider, nameof(provider));
		}

		/// <summary>
		/// The persistence session this persistence context represents.
		/// </summary>
		private IPersistenceSession _session;

		/// <summary>
		/// The root transaction context for this persistence context.
		/// </summary>
		/// <remarks>
		/// The root transaction context is the persistence context in the stack where a new transaction was spawned.
		/// </remarks>
		private PersistenceContext _transactionRootContext;

		/// <summary>
		/// The root session context for this persistence context.
		/// </summary>
		/// <remarks>
		/// The root persistence context is the persistence context in the stack where a new session was created.
		/// </remarks>
		private PersistenceContext _sessionRootContext;

		/// <summary>
		/// Determines the transaction mode selected for the persistence context.
		/// </summary>
		private TransactionOption _txOption;

		/// <summary>
		/// Indicates whether the persistence context is stateless.
		/// </summary>
		private bool _stateless;

		/// <summary>
		/// Indicates whether this persistent context's transactional state (if one exists) is 
		/// consistent - i.e. okay to commit at any time. We default to not consistent - that way, 
		/// if an exception occurs before the consistency state is updated, the transaction will 
		/// automatically roll back when the context is disposed.
		/// </summary>
		private bool _isConsistent;

		/// <summary>
		/// Used only in root contexts.  Indicates whether the transaction is doomed to roll back because one of the
		/// child contexts was disposed while its consistent flag was false.  This is used to prevent unnecessary work
		/// against the database because the transaction is guaranteed to be rolled back when the root context is disposed.
		/// </summary>
		private bool _isDoomed;

		/// <summary>
		/// Specifies the transaction isolation level for this persistence context.
		/// </summary>
		private IsolationLevel _txIsolationLevel = IsolationLevel.Unspecified;

		/// <summary>
		/// Creates a new persistence context for managing persistence session and transactions.
		/// </summary>
		/// <param name="txOption">Determines whether a transaction is required for this transaction context.  See 
		/// <see cref="TransactionOption"/> for more information.</param>
		/// <param name="txIsolationLevel">Specifieds the level of transaction isolation for the persistence context.
		/// See <see cref="System.Data.IsolationLevel"/> for more information.</param>
		/// <param name="currentContext">The currently executing <see cref="PersistenceContext"/> if one
		/// is present in the call context.  Otherwise, <c>Null</c> (<c>Nothing</c> in Visual Basic).</param>
		/// <param name="stateless">Stipulates whether to create a stateless session.</param>
		internal PersistenceContext( TransactionOption txOption, IsolationLevel txIsolationLevel, PersistenceContext currentContext, bool stateless )
		{
			bool isCompatible;
			_stateless = stateless;

			// Validate arguments
			if( txOption == TransactionOption.Required || txOption == TransactionOption.RequiresNew )
			{
				// Transactional scope requested - cannot have unspecified transaction isolation level
				if( txIsolationLevel == IsolationLevel.Unspecified )
				{
					throw new ArgumentException(ExceptionMessage.IncompatibleTxIsolationLevel, nameof(txIsolationLevel));
				}
			}
			else
			{
				// No transaction, therefore should be NO specified transaction isolation level
				if( txIsolationLevel != IsolationLevel.Unspecified )
				{
					throw new ArgumentException(ExceptionMessage.InvalidTxIsolationLevel, nameof(txIsolationLevel));
				}
			}
			
			try
			{
				// Do we have a currently executing context?
				if( currentContext == null )
				{
					// No, so we are definitely NOT compatible
					isCompatible = false;
				}
				else if( currentContext._stateless != stateless )
				{
					// Parent context has different statefulness as currently executing context, so definition NOT compatible
					isCompatible = false;
				}
				else
				{
					// Test compatibility between parent context and requested level of transaction support
					if( txOption == TransactionOption.Supported )
					{
						isCompatible = true;
					}
					else if( txOption == TransactionOption.RequiresNew )
					{
						isCompatible = false;
					}
					else
					{
						// Are we currently operating inside a transaction?
						if( currentContext.IsInTransaction )
						{
							// We're in a transaction - so unless one was specifically disallowed, we're compatible
							isCompatible = ( txOption != TransactionOption.None );
						}
						else
						{
							// We're not in a transaction - so unless one was requested, we're compatible
							isCompatible = ( txOption == TransactionOption.None );
						}
					}
				}

				// Check if the requested transaction attributes are compatible with those of the current persistence context
				if( isCompatible )
				{
					if( _log.IsDebugEnabled )
					{
						_log.DebugFormat( "Context {{{0}}} Enrolling in compatible root persistence context {{{1}}}.", 
							base.ContextId, currentContext._transactionRootContext.ContextId );
					}

					// Is the currently executing context in which we are about to enroll, already rolled back?
					if( currentContext._isDoomed )
					{
						throw new DoomedTransactionEnrolmentException();
					}
					
					// Check to see if the transaction isolation level is compatible
					if( ( int )txIsolationLevel > ( int )currentContext._transactionRootContext._txIsolationLevel )
					{
						throw new ArgumentException(ExceptionMessage.IncompatibleTxIsolationLevel, nameof(txIsolationLevel));
					}
					
					// Adopt the current context's session
					_session = currentContext._session;
					_transactionRootContext = currentContext._transactionRootContext;
					_sessionRootContext = currentContext._sessionRootContext;
					_txOption = _transactionRootContext._txOption;
					_txIsolationLevel = _transactionRootContext._txIsolationLevel;
				}
				else
				{
					if( _log.IsDebugEnabled )
					{
						_log.DebugFormat( "Creating new root persistence context {{{0}}}.", base.ContextId );
					}

					// If no current context, then this is the session root context
					if (currentContext == null)
					{
						_sessionRootContext = this;
						_session = Provider.OpenSession(stateless);
					}
					else
					{
						_session = currentContext._session;
					}

					// Set the transaction mode and isolation level
					_txOption = txOption;
					_txIsolationLevel = txIsolationLevel;

					// Do we need a transaction?
					if (IsInTransaction)
					{
						// We are our own root context
						_transactionRootContext = this;

						// Create a new transaction with the provided isolation level
						_session.BeginTransaction( txIsolationLevel );
					}
				}
			}
			catch( Exception ex )
			{
				// Make a note of the exception
				_log.Error( "Error creating new persistence context.", ex);

				// If we are a root context, then we have some cleaning up to do
				if (this == _transactionRootContext)
				{
					if( _session != null )
					{
						if( _session.Transaction != null )
						{
							_session.Transaction.Rollback();
						}
					}
				}

				if (this == _sessionRootContext)
				{
					_session.Close();
					_session.Dispose();
				}

				// Re-throw the exception
				throw;
			}
		}

		/// <summary>
		/// The current persistence context.
		/// </summary>
		public static PersistenceContext Current => GetCurrent<PersistenceContext>();

		/// <summary>
		/// The persistence session associated with the persistence context.
		/// </summary>
		public IPersistenceSession Session
		{
			get { return _session; }
		}

		/// <summary>
		/// Gets the persistence session associated with the persistence context, specifically as either a <see cref="IStatefulPersistenceSession"/>
		/// of <see cref="IStatelessPersistenceSession"/>.
		/// </summary>
		/// <typeparam name="TSession">The type of persistence session to get.</typeparam>
		/// <returns>The persistence session associated with the persistence context, specifically as the given type of session.</returns>
		public TSession SessionOfType<TSession>() where TSession : IPersistenceSession
		{
			return (TSession)_session;
		}

		/// <summary>
		/// State of consistency of the transactional context.  The flag indicates whether at any given time the system
		/// is okay to commit the transaction to the database.
		/// </summary>
		public bool IsConsistent
		{
			get{ return _isConsistent; }
			set{ _isConsistent = value; }
		}

		/// <summary>
		/// Indicates whether the persistence context is transactional.
		/// </summary>
		public bool IsInTransaction
		{
			get{ return _txOption == TransactionOption.Required || _txOption == TransactionOption.RequiresNew; }
		}

		/// <summary>
		/// The enforced transaction isolation level for the current <see cref="PersistenceContext" /> instance.
		/// </summary>
		public IsolationLevel TxIsolationLevel
		{
			get{ return _txIsolationLevel; }
		}

		/// <summary>
		/// The <see cref="TransactionOption" /> requested for the current <see cref="PersistenceContext" /> instance.
		/// </summary>
		public TransactionOption TxMode
		{
			get{ return _txOption; }
		}

		/// <summary>
		/// Enlists an <see cref="DbCommand" /> in the currently executing transaction if the current persistence context 
		/// is transactional.  Otherwise, this method takes no action.
		/// </summary>
		/// <param name="command"></param>
		public void Enlist( DbCommand command )
		{
			// Validate argument
			command = ArgumentValidation.AssertNotNull( command, "command" );

			// Does this command have a connection?
			if( command.Connection == null )
			{
				// No, then give it one
				command.Connection = _session.Connection;
			}
			
			// Are we currently executing in a transactional context?
			if( this.IsInTransaction )
			{
				// Yes, then enlist the specified command object in the current transaction
				_session.Transaction.Enlist( command );
			}
		}

		#region IDisposable Members

		/// <summary>
		/// Cleans up the persistence context.
		/// </summary>
		/// <remarks>
		/// If the context is transactional, it is a root context and the <see cref="IsConsistent" /> property
		/// is true, then the transaction is committed.  If the <see cref="IsConsistent" /> flag is false, then the transaction is
		/// rolled back.  If the context is not a root context and the <see cref="IsConsistent" /> flag is false, then the root context
		/// is marked as "doomed" as there is no way the transaction will ever be permitted to commit.  If the context is non-transactional
		/// and it is a root context, the in-memory state of the persistence context is synchronised with the database, but this 
		/// operation is performed outside the context of a database transaction.
		/// </remarks>
		public void Dispose()
		{
			if( _log.IsDebugEnabled )
			{
				_log.DebugFormat( "Cleaning up persistence context: {{{0}}}.", base.ContextId );
			}

			try
			{
				// Is the context in a transaction?
				if( this.IsInTransaction )
				{
					// Are we the root context?
					if( this == _transactionRootContext )
					{
						// Yes, is the transaction consistent?
						if( _isConsistent && !_isDoomed )
						{
							// Yes, then commit the transaction
							_session.Transaction.Commit();
						}
						else
						{
							// Transaction is not consistent - so roll it back
							_session.Transaction.Rollback();
						}
					}
					else
					{
						// We are a child context.  Are we consistent?
						if( !_isConsistent )
						{
							// We are not consistent and we are finished, so the root transaction is now doomed to roll back.
							_transactionRootContext._isDoomed = true;
						}
					}
				}
				else
				{
					// Not in a transaction.  Are we the root context and consistent?
					if( this == _sessionRootContext && this._isConsistent )
					{
						// Yes, then we need to flush the session
						(_session as IStatefulPersistenceSession)?.Flush();
					}
				}
			}
			finally
			{
				// Are we the root context?
				if( this == _sessionRootContext )
				{
					// Yep, then we need to dispose of the session
					_session.Close();
					_session.Dispose();
				}
			}
		}

		#endregion
	}
}
