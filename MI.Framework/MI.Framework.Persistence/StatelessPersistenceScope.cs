﻿using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Linq;

using MI.Framework.Logging;
using MI.Framework.Contexts;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Demarks a segment of executing code at runtime with certain persistence-related characteristics, where the scope does not
	/// maintain a stateful session.
	/// </summary>
	public sealed class StatelessPersistenceScope : Scope
	{
		/// <summary>
		/// Log for code instrumentation
		/// </summary>
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		/// <summary>
		/// Creates a new transactional persistence context for managing persistence session and transactions with 
		/// "read committed" transaction isolation.
		/// </summary>
		public StatelessPersistenceScope()
			: this(TransactionOption.Required, IsolationLevel.ReadCommitted)
		{
		}

		/// <summary>
		/// Creates a new persistence context for managing persistence session and transactions with "read committed" 
		/// transaction isolation.
		/// </summary>
		/// <param name="txOption">Determines whether a transaction is required for this transaction context.  See 
		/// <see cref="TransactionOption"/> for more information.</param>
		public StatelessPersistenceScope(TransactionOption txOption)
			: this(txOption, txOption == TransactionOption.Required
			|| txOption == TransactionOption.RequiresNew ? IsolationLevel.ReadCommitted : IsolationLevel.Unspecified)
		{
		}

		/// <summary>
		/// Creates a new persistence context for managing persistence session and transactions.
		/// </summary>
		/// <param name="txOption">Determines whether a transaction is required for this transaction context.  See 
		/// <see cref="TransactionOption"/> for more information.</param>
		/// <param name="txIsolationLevel">Specifieds the level of transaction isolation for the persistence context.
		/// See <see cref="System.Data.IsolationLevel"/> for more information.</param>
		public StatelessPersistenceScope(TransactionOption txOption, IsolationLevel txIsolationLevel)
			: base(new PersistenceContextFactory(txOption, txIsolationLevel, true))
		{
			// Log operation commencement for debug purposes
			if (_log.IsDebugEnabled)
			{
				_log.DebugFormat("Opening persistence scope {{{0}}}: TxMode: {1}, TxIsolationLevel: {2}",
					Context.ContextId.ToString(), txOption.ToString(), txIsolationLevel.ToString());
			}
		}

		/// <summary>
		/// Gets the current <see cref="PersistenceScope" /> object representing the validation context demarked by
		/// this <see cref="Scope" /> instance.
		/// </summary>
		private new PersistenceContext Context
		{
			get { return (PersistenceContext)base.Context; }
		}

		/// <summary>
		/// Gets the current persistence session.
		/// </summary>
		public IStatelessPersistenceSession Session
		{
			get { return (IStatelessPersistenceSession)Context.Session; }
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id) where T : class
		{
			return this.Session.FindById<T>(id);
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id, bool acquireLock) where T : class
		{
			return this.Session.FindById<T>(id, acquireLock);
		}

		/// <summary>
		/// Inserts an entity.
		/// </summary>
		/// <param name="entity">A new transient instance.</param>
		/// <returns>The identifier of the instance.</returns>
		public object Insert(object entity)
		{
			return Session.Insert(entity);
		}

		/// <summary>
		/// Update an entity.
		/// </summary>
		/// <param name="entity">A detached entity instance.</param>
		public void Update(object entity)
		{
			Session.Update(entity);
		}

		/// <summary>
		/// Delete an entity.
		/// </summary>
		/// <param name="entity">A detached entity instance.</param>
		public void Delete(object entity)
		{
			Session.Delete(entity);
		}

		/// <summary>
		/// Refresh the entity instance state from the database.
		/// </summary>
		/// <param name="entity">The entity to be refreshed.</param>
		public void Refresh(object entity)
		{
			Session.Refresh(entity);
		}

		/// <summary>
		/// Refresh the entity instance state from the database.
		/// </summary>
		/// <param name="entity">The entity to be refreshed.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		public void Refresh(object entity, bool acquireLock)
		{
			Session.Refresh(entity, acquireLock);
		}

		/// <summary>
		/// Retrieves all entities of the type designated in the type parameter.
		/// </summary>
		/// <returns>A list of all entities.</returns>
		public IList<T> ListAll<T>() where T : class
		{
			return this.Session.ListAll<T>();
		}

		/// <summary>
		/// Provides a queryable interface for objects of the given type.
		/// </summary>
		/// <typeparam name="T">The type of entity to find.</typeparam>
		/// <returns>A queryable interface objects of the given type.</returns>
		public IQueryable<T> Query<T>() where T : class
		{
			return this.Session.Query<T>();
		}

		/// <summary>
		/// Indicates whether the currently executing transaction is consistent.
		/// </summary>
		/// <remarks>
		/// If <see cref="IsConsistent"/> is <c>True</c> at the time the <see cref="PersistenceScope" /> instance 
		/// is Disposed, then the underlying database transaction is committed.  Otherwise it is rolled back.
		/// </remarks>
		public bool IsConsistent
		{
			get { return Context.IsConsistent; }
			set { Context.IsConsistent = value; }
		}

		/// <summary>
		/// A <see cref="System.Boolean" /> indicating whether the <see cref="PersistenceScope" /> is currently
		/// executing within a database transaction.
		/// </summary>
		public bool IsInTransaction
		{
			get { return Context.IsInTransaction; }
		}

		/// <summary>
		/// An <see cref="IsolationLevel" /> enumeration, indicating the current level of database transaction isolation.
		/// </summary>
		public IsolationLevel TxIsolationLevel
		{
			get { return Context.TxIsolationLevel; }
		}

		/// <summary>
		/// A <see cref="TransactionOption" /> enumeration indicating the transaction mode of the current 
		/// <see cref="PersistenceScope" /> instance.
		/// </summary>
		public TransactionOption TxMode
		{
			get { return Context.TxMode; }
		}

		/// <summary>
		/// Enlists an ADO.NET <see cref="DbCommand" /> in the current persistence scope.  The <see cref="DbCommand"/> 
		/// inherits the existing database connection and any existing transaction on that connection.
		/// </summary>
		/// <param name="command">An ADO.NET Command implementing the <see cref="DbCommand"/> interface to be 
		/// enrolled in the connection and transaction of the <see cref="PersistenceScope"/>.</param>
		public void Enlist(DbCommand command)
		{
			Context.Enlist(command);
		}

		/// <summary>
		/// Indicates the work related to this persistence scope is complete.
		/// </summary>
		public void Done()
		{
			this.IsConsistent = true;
		}

		/// <summary>
		/// Traps the Dispose event in order to log it
		/// </summary>
		/// <param name="disposing">A <see cref="System.Boolean" /> indicating that the method was invoked by
		/// a client calling the <see cref="Dispose"/> method rather than the garbage collector.</param>
		protected override void Dispose(bool disposing)
		{
			try
			{
				// Log the termination of the operation for debug purposes
				if (_log.IsDebugEnabled)
				{
					_log.DebugFormat("Closing persistence scope {{{0}}}.", Context.ContextId.ToString());
				}
			}
			finally
			{
				// Base method - actually does the clean up
				base.Dispose(disposing);
			}
		}
	}
}
