using System;
using System.Runtime.Serialization;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// The exception that is thrown when a new <see cref="PersistenceScope" /> is enrolled in an existing 
	/// transactional <see cref="PersistenceScope" /> where the underlying transaction is marked for rollback.
	/// </summary>
	/// <remarks>
	/// This exception prevents additional persistence-related work being performed as part of a database
	/// transaction which is doomed to be rolled back.  Such work would be wasteful.  A <see cref="PersistenceScope" />
	/// is doomed to roll back if a child <see cref="PersistenceScope" /> is disposed while its 
	/// <see cref="PersistenceScope.IsConsistent" /> property is <c>False</c>.
	/// </remarks>
	[ Serializable() ]
	public class DoomedTransactionEnrolmentException : FrameworkException
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="DoomedTransactionEnrolmentException" /> class with a 
		/// relevant system supplied exception message.
		/// </summary>
		public DoomedTransactionEnrolmentException()
			: base(ExceptionMessage.DefaultDoomedTxEnrolment )
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DoomedTransactionEnrolmentException" /> class with a 
		/// specified error message.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		public DoomedTransactionEnrolmentException( string message ) 
			: base( message )
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DoomedTransactionEnrolmentException" /> class with a specified error message
		/// and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception.</param>
		public DoomedTransactionEnrolmentException( string message, System.Exception innerException ) 
			: base( message, innerException )
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DoomedTransactionEnrolmentException" /> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the 
		/// exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about 
		/// the source or destination.</param>
		protected DoomedTransactionEnrolmentException( SerializationInfo info, StreamingContext context ) 
			: base( info, context )
		{
		}
	}
}
