using System.Data;

using MI.Framework.Contexts;

namespace MI.Framework.Persistence
{
	/// <summary>
	/// Used by the <see cref="Scope" /> class to create instances of the 
	/// <see cref="PersistenceContext" /> class when <see cref="Scope" /> 
	/// instances are created.  Based on the Abstract Factory design pattern.
	/// </summary>
	public class PersistenceContextFactory : IContextFactory
	{
		/// <summary>
		/// The requested level of transaction support for <see cref="PersistenceContext" /> instances 
		/// created by the factory.
		/// </summary>
		private TransactionOption _txOption;

		/// <summary>
		/// The transaction isolation level of the <see cref="PersistenceContext" /> instances created
		/// by the factory.
		/// </summary>
		private IsolationLevel _txIsolationLevel;

		/// <summary>
		/// Stipulates whether the factory will create stateless persistence contexts.
		/// </summary>
		private bool _stateless;

		/// <summary>
		/// Initialises a new instance of the <see cref="PersistenceContextFactory" /> class.
		/// </summary>
		/// <param name="txOption">Specifies the transactional requirements of the <see cref="PersistenceContext" /> 
		/// instances created by the factory.</param>
		/// <param name="txIsolationLevel">Specifies the transaction isolation level of <see cref="PersistenceContext" />
		/// instances created by the factory.</param>
		/// <param name="stateless">Stipulates whether the factory will create stateless persistence contexts.</param>
		internal PersistenceContextFactory( TransactionOption txOption, IsolationLevel txIsolationLevel, bool stateless )
		{
			_txOption = txOption;
			_txIsolationLevel = txIsolationLevel;
			_stateless = stateless;
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="PersistenceContext" /> class.
		/// </summary>
		/// <param name="currentContext">The current context if one exists, otherwise Null.</param>
		/// <returns>A new <see cref="PersistenceContext"/> instance, enrolled in the current context
		/// if present.</returns>
		public Context CreateContext( Context currentContext )
		{
			return new PersistenceContext( _txOption, _txIsolationLevel, currentContext as PersistenceContext, _stateless );
		}

		/// <summary>
		/// Gets the <see cref="System.Type"/> indicating the type of <see cref="Context" /> object
		/// produced by this context factory.
		/// </summary>
		public System.Type ContextType
		{
			get{ return typeof( PersistenceContext ); }
		}
	}
}
