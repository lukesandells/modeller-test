namespace MI.Framework.Persistence
{
	/// <summary>
	/// Interface for a persistence provider.
	/// </summary>
	public interface IPersistenceProvider
	{
		/// <summary>
		/// Opens a new persistence session.
		/// </summary>
		/// <param name="stateless">Stipulates whether to open a stateless session.</param>
		/// <returns>The new persistence session.</returns>
		IPersistenceSession OpenSession( bool stateless );
	}
}
