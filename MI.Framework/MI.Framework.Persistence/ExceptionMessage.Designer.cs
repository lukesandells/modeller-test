﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MI.Framework.Persistence {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class ExceptionMessage {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ExceptionMessage() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("MI.Framework.Persistence.ExceptionMessage", typeof(ExceptionMessage).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Attempt to enroll in a persistence context that has already rolled back or is doomed to fail..
        /// </summary>
        internal static string DefaultDoomedTxEnrolment {
            get {
                return ResourceManager.GetString("DefaultDoomedTxEnrolment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Attempt to create a persistence context with a transaction isolation level higher than the currently executing context..
        /// </summary>
        internal static string IncompatibleTxIsolationLevel {
            get {
                return ResourceManager.GetString("IncompatibleTxIsolationLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The requested transaction isolation level parameter does not match the requested transaction mode parameter..
        /// </summary>
        internal static string InvalidTxIsolationLevel {
            get {
                return ResourceManager.GetString("InvalidTxIsolationLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Persistence session has not been correctly configured..
        /// </summary>
        internal static string PersistenceSessionNotConfigured {
            get {
                return ResourceManager.GetString("PersistenceSessionNotConfigured", resourceCulture);
            }
        }
    }
}
