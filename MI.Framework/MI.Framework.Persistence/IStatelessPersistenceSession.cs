﻿namespace MI.Framework.Persistence
{
	/// <summary>
	/// Represents a stateless persistence session.
	/// </summary>
	public interface IStatelessPersistenceSession : IPersistenceSession
	{
		/// <summary>
		/// Inserts an entity.
		/// </summary>
		/// <param name="entity">A new transient instance.</param>
		/// <returns>The identifier of the instance.</returns>
		object Insert(object entity);

		/// <summary>
		/// Update an entity.
		/// </summary>
		/// <param name="entity">A detached entity instance.</param>
		void Update(object entity);

		/// <summary>
		/// Delete an entity.
		/// </summary>
		/// <param name="entity">A detached entity instance.</param>
		void Delete(object entity);

		/// <summary>
		/// Refresh the entity instance state from the database.
		/// </summary>
		/// <param name="entity">The entity to be refreshed.</param>
		void Refresh(object entity);

		/// <summary>
		/// Refresh the entity instance state from the database.
		/// </summary>
		/// <param name="entity">The entity to be refreshed.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		void Refresh(object entity, bool acquireLock);
	}
}
