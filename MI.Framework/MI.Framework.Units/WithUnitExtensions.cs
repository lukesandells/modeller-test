﻿namespace MI.Framework.Units
{
	/// <summary>
	/// Extension methods for creating unit decimals from numeric literals.
	/// </summary>
	public static class WithUnitExtensions
	{
		/// <summary>
		/// Returns a <see cref="UnitDecimal"/>, combining the given number with a given <see cref="Unit"/>.
		/// </summary>
		/// <param name="number">The number with which to create the dimensioned decimal.</param>
		/// <param name="unit">The unit with which to create the dimensioned decimal.</param>
		/// <returns>The given number as a unit decimal with the given unit.</returns>
		public static UnitDecimal WithUnit(this int number, Unit unit)
		{
			return new UnitDecimal(number, unit);
		}

		/// <summary>
		/// Returns a <see cref="UnitDecimal"/>, combining the given number with a given <see cref="Unit"/>.
		/// </summary>
		/// <param name="number">The number with which to create the dimensioned decimal.</param>
		/// <param name="unit">The unit with which to create the dimensioned decimal.</param>
		/// <returns>The given number as a unit decimal with the given unit.</returns>
		public static UnitDecimal WithUnit(this decimal number, Unit unit)
		{
			return new UnitDecimal(number, unit);
		}

		/// <summary>
		/// Returns a <see cref="UnitDecimal"/>, combining the given number with a given <see cref="Unit"/>.
		/// </summary>
		/// <param name="number">The number with which to create the dimensioned decimal.</param>
		/// <param name="unit">The unit with which to create the dimensioned decimal.</param>
		/// <returns>The given number as a unit decimal with the given unit.</returns>
		public static UnitDecimal WithUnit(this double number, Unit unit)
		{
			return new UnitDecimal((decimal)number, unit);
		}
	}
}
