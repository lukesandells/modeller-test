﻿namespace MI.Framework.Units
{
	/// <summary>
	/// Applied to classes that define physical properties as static properties, identified 
	/// with the <see cref="UnitDefinitionAttribute"/>.
	/// </summary>
	public interface IPhysicalPropertyDefinition
	{
		/// <summary>
		/// The physical property defined by the property definition.
		/// </summary>
		PhysicalProperty DefinedProperty { get; }
	}
}
