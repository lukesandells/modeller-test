﻿using System;
using MI.Framework.Validation;

namespace MI.Framework.Units
{
    public class Unit
    {
		public static Unit BaseDimensionlessUnit { get; } = new Unit();

		public IPhysicalProperty PhysicalProperty => _physicalProperty;
		private IPhysicalProperty _physicalProperty;

		public string Symbol => _symbol;
		private string _symbol;

		public string Name => _name;
		private string _name;

		public string Code => _code;
		private string _code;

		public UnitDefinition Definition => _definition;
		private UnitDefinition _definition;

		internal decimal ScaleFactor => _scaleFactor;
		private decimal _scaleFactor;

		internal decimal Offset => _offset;
		private decimal _offset;

		public bool IsBaseUnit => _isBaseUnit;
		private bool _isBaseUnit;

		public bool IsNormative => _isNormative;
		private bool _isNormative;

		public bool IsDimensionless => PhysicalProperty == BaseDimensionlessUnit.PhysicalProperty;

		public bool IsBaseDimensionless => this == BaseDimensionlessUnit;

		private Unit()
		{
			_symbol = string.Empty;
			_name = UnitName.Number;
			_code = string.Empty;
			_scaleFactor = 1;
			_isBaseUnit = true;
			_isNormative = true;
			_definition = UnitDefinition.Multiply(this, 1);
			_physicalProperty = UnitLibrary.NumberProperty;
		}

		private Unit(IPhysicalProperty property, string symbol, string name, string code, bool isNormative)
		{
			_physicalProperty = ArgumentValidation.AssertNotNull(property, nameof(property));
			_symbol = ArgumentValidation.AssertValidIdentifier(symbol, nameof(symbol));
			_name = ArgumentValidation.AssertNotNullOrEmpty(name, nameof(name));
			_code = ArgumentValidation.AssertValidIdentifier(code, nameof(code));
			_isNormative = isNormative;
			_scaleFactor = 1;

			_definition = UnitDefinition.Multiply(this, 1);
		}

		public static Unit NewBaseUnit(IPhysicalProperty property, string symbol, string name, string code)
		{
			var unit = new Unit(property, symbol, name, code, true)
			{
				_isBaseUnit = true
			};

			return unit;
		}

		public static Unit operator /(Unit left, Unit right)
		{
			left = ArgumentValidation.AssertNotNull(left, nameof(left));
			right = ArgumentValidation.AssertNotNull(right, nameof(right));

			// Check if the units cancel each other out
			if (left == right)
			{
				return BaseDimensionlessUnit;
			}

			// If denominator is dimensionless, then the new unit is that of the numerator
			if (right.IsDimensionless)
			{
				return left;
			}
			
			// Locate the new unit by deriving the normative unit definition of the new unit
			var normativeUnitDefinition = UnitDefinition.Divide(left.PhysicalProperty.NormativeUnit, right.PhysicalProperty.NormativeUnit);
			if (!UnitLibrary.Current.UnitsByDefinition.TryGetValue(normativeUnitDefinition, out var unit))
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.NoUnitDefinedForDivision, left, right));
			}

			// Now see if there is a unit defined in the new unit's physical property with the required scale factor and offset
			if (unit.PhysicalProperty.UnitsByScaleFactorAndOffset.TryGetValue((right.ScaleFactor / left.ScaleFactor, 
				right.Offset / left.ScaleFactor), out var scaledUnit))
			{
				return scaledUnit;
			}
			
			return unit;
		}

		public static Unit operator *(Unit left, Unit right)
		{
			left = ArgumentValidation.AssertNotNull(left, nameof(left));
			right = ArgumentValidation.AssertNotNull(right, nameof(right));

			// If left or right is dimensionless, then the new unit is that of whichever of left and right is dimensioned
			if (left.IsDimensionless)
			{
				return right;
			}
			else if (right.IsDimensionless)
			{
				return left;
			}
			
			// Locate the new unit by deriving the normative unit definition of the new unit
			var normativeUnitDefinition = UnitDefinition.Multiply(left.PhysicalProperty.NormativeUnit, right.PhysicalProperty.NormativeUnit);
			if (!UnitLibrary.Current.UnitsByDefinition.TryGetValue(normativeUnitDefinition, out var unit))
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.NoUnitDefinedForMultiplication, left, right));
			}

			// Now see if there is a unit defined in the new unit's physical property with the required scale factor and offset
			if (unit.PhysicalProperty.UnitsByScaleFactorAndOffset.TryGetValue((right.ScaleFactor * left.ScaleFactor, 
				right.Offset * left.ScaleFactor), out var scaledUnit))
			{
				return scaledUnit;
			}
			
			return unit;
		}

		public static Unit operator +(Unit unit, decimal constant)
		{
			unit = ArgumentValidation.AssertNotNull(unit, nameof(unit));

			// Get the new definition and locate the new unit in the unit library
			var unitDefinition = UnitDefinition.Add(unit, constant);
			if (!UnitLibrary.Current.UnitsByDefinition.TryGetValue(unitDefinition, out unit))
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.NoUnitDefinedForAdditionOfConstant, unit, constant));
			}

			return unit;
		}

		public static Unit operator -(Unit unit, decimal constant)
		{
			unit = ArgumentValidation.AssertNotNull(unit, nameof(unit));

			// Get the new definition and locate the new unit in the unit library
			var unitDefinition = UnitDefinition.Add(unit, -constant);
			if (!UnitLibrary.Current.UnitsByDefinition.TryGetValue(unitDefinition, out unit))
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.NoUnitDefinedForSubtractionOfConstant, unit, constant));
			}

			return unit;
		}

		public Unit DividedBy(decimal constant, string symbol, string name, string code)
		{
			return MultipliedBy(1 / constant, symbol, name, code);
		}

		public Unit MultipliedBy(decimal constant, string symbol, string name, string code)
		{
			var unit = new Unit(_physicalProperty, symbol, name, code, false)
			{
				_definition = UnitDefinition.Multiply(this, constant)
			};

			unit._scaleFactor *= constant;
			unit._offset *= constant;
			
			return unit;
		}

		public Unit DividedBy(Unit unitDivisor, IPhysicalProperty property, string symbol, string name, string code)
		{
			unitDivisor = ArgumentValidation.AssertNotNull(unitDivisor, nameof(unitDivisor));
			var unit = new Unit(property, symbol, name, code, true)
			{
				_definition = UnitDefinition.Divide(this, unitDivisor)
			};

			unit._scaleFactor /= unitDivisor._scaleFactor;
			unit._offset /= unitDivisor._scaleFactor;
			//unit._scaleFactor = _scaleFactor;

			return unit;
		}

		public Unit MultipliedBy(Unit unitMultiplier, PhysicalProperty property, string symbol, string name, string code)
		{
			unitMultiplier = ArgumentValidation.AssertNotNull(unitMultiplier, nameof(unitMultiplier));
			var unit = new Unit(property, symbol, name, code, true)
			{
				_definition = UnitDefinition.Multiply(this, unitMultiplier)
			};

			unit._scaleFactor *= unitMultiplier._scaleFactor;
			unit._offset *= unitMultiplier._scaleFactor;
			//unit._scaleFactor = _scaleFactor;

			return unit;
		}

		public Unit Plus(decimal constant, string symbol, string name, string code)
		{
			var unit = new Unit(_physicalProperty, symbol, name, code, false)
			{
				_definition = UnitDefinition.Add(this, constant),
				_scaleFactor = _scaleFactor
			};

			unit._offset += constant;

			return unit;
		}

		public Unit Minus(decimal constant, string symbol, string name, string code)
		{
			return Plus(-constant, symbol, name, code);
		}

		public override string ToString()
		{
			return string.Format(@"{0} ({1})", Name, PhysicalProperty.Name);
		}
	}
}
