﻿using System.Collections.Generic;

namespace MI.Framework.Units
{
	public interface IUnitLibrary
	{
		IEnumerable<IPhysicalProperty> PhysicalProperties { get; }

		IReadOnlyDictionary<string, Unit> UnitsBySymbol { get; }

		IReadOnlyDictionary<UnitDefinition, Unit> UnitsByDefinition { get; }

		IReadOnlyDictionary<string, Unit> UnitsByCode { get; }
	}
}
