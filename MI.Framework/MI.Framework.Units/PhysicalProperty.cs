﻿using System;
using System.Collections.Generic;

using MI.Framework.Validation;

namespace MI.Framework.Units
{
	public class PhysicalProperty : IPhysicalProperty
	{
		public string Name => _name;
		private string _name;

		public Unit NormativeUnit => _normativeUnit;
		private Unit _normativeUnit;

		public IEnumerable<Unit> Units => _units;
		private ISet<Unit> _units = new HashSet<Unit>();

		public IReadOnlyDictionary<(decimal, decimal), Unit> UnitsByScaleFactorAndOffset => _unitsByScaleFactorAndOffset;
		private Dictionary<(decimal, decimal), Unit> _unitsByScaleFactorAndOffset = new Dictionary<(decimal, decimal), Unit>();

		public PhysicalProperty(string name)
		{
			_name = ArgumentValidation.AssertValidIdentifier(name, nameof(name));
		}

		public void AddUnit(Unit unit)
		{
			unit = ArgumentValidation.AssertNotNull(unit, nameof(unit));

			_units.Add(ArgumentValidation.AssertNotNull(unit, nameof(unit)));
			_unitsByScaleFactorAndOffset.Add((unit.ScaleFactor, unit.Offset), unit);

			if (unit.IsNormative)
			{
				if (unit.IsNormative && NormativeUnit != null)
				{
					throw new ArgumentException(ExceptionMessage.PropertyAlreadyHasNormativeUnit);
				}

				_normativeUnit = unit;
			}
		}

		public override string ToString()
		{
			return _name;
		}
	}
}
