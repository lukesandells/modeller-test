﻿using MI.Framework.Validation;

namespace MI.Framework.Units
{
	/// <summary>
	/// The definition of a unit of measure.
	/// </summary>
	public abstract class UnitDefinition
	{
		private class ConstantDefinition : UnitDefinition
		{
			public decimal Constant => _constant;
			private decimal _constant;

			public ConstantDefinition(decimal constant)
			{
				_constant = constant;
			}

			public override bool Equals(UnitDefinition unitDefinition)
			{
				// If parameter is null return false.
				if (unitDefinition == null)
				{
					return false;
				}

				// If parameter cannot be cast, return false.
				var constantDefinition = unitDefinition as ConstantDefinition;
				if ((object)constantDefinition == null)
				{
					return false;
				}

				return Constant == constantDefinition.Constant;
			}

			public override int GetHashCode()
			{
				return Constant.GetHashCode();
			}
		}

		private class UnitSymbolDefinition : UnitDefinition
		{
			public string Symbol => _symbol;
			private string _symbol;

			public UnitSymbolDefinition(string symbol)
			{
				_symbol = symbol;
			}

			public override bool Equals(UnitDefinition unitDefinition)
			{
				// If parameter is null return false.
				if (unitDefinition == null)
				{
					return false;
				}

				// If parameter cannot be cast, return false.
				var unitSymbolDefinition = unitDefinition as UnitSymbolDefinition;
				if ((object)unitSymbolDefinition == null)
				{
					return false;
				}

				return Symbol == unitSymbolDefinition.Symbol;
			}

			public override int GetHashCode()
			{
				return Symbol.GetHashCode();
			}
		}

		private class MultiplicationDefinition : UnitDefinition
		{
			public UnitSymbolDefinition UnitSymbol => _unitSymbol;
			private UnitSymbolDefinition _unitSymbol;

			public UnitDefinition Multiplier => _multiplier;
			private UnitDefinition _multiplier;

			public MultiplicationDefinition(UnitSymbolDefinition unitSymbol, UnitDefinition multiplier)
			{
				_unitSymbol = unitSymbol;
				_multiplier = multiplier;
			}

			public override string ToString()
			{
				var constantMultiplier = _multiplier as ConstantDefinition;
				if (constantMultiplier != null)
				{
					return string.Format("{0}{1}", constantMultiplier.Constant == 1 ? string.Empty : constantMultiplier.Constant.ToString(),
						_unitSymbol.Symbol);
				}
				else
				{
					return string.Format("{0}.{1}", _unitSymbol.Symbol, ((UnitSymbolDefinition)_multiplier).Symbol);
				}
			}

			public override bool Equals(UnitDefinition unitDefinition)
			{
				// If parameter is null return false.
				if (unitDefinition == null)
				{
					return false;
				}

				// If parameter cannot be cast, return false.
				var multiplicationDefinition = unitDefinition as MultiplicationDefinition;
				if ((object)multiplicationDefinition == null)
				{
					return false;
				}

				return UnitSymbol.Equals(multiplicationDefinition.UnitSymbol) && Multiplier.Equals(multiplicationDefinition.Multiplier)
					|| UnitSymbol.Equals(multiplicationDefinition.Multiplier) && Multiplier.Equals(multiplicationDefinition.UnitSymbol);
			}

			public override int GetHashCode()
			{
				return (UnitSymbol, Multiplier).GetHashCode();
			}
		}

		private class DivisionDefinition : UnitDefinition
		{
			public UnitSymbolDefinition UnitSymbol => _unitSymbol;
			private UnitSymbolDefinition _unitSymbol;

			public UnitDefinition Divisor => _divisor;
			private UnitDefinition _divisor;

			public DivisionDefinition(UnitSymbolDefinition unitSymbol, UnitDefinition divisor)
			{
				_unitSymbol = unitSymbol;
				_divisor = divisor;
			}

			public override string ToString()
			{
				var constantMultiplier = _divisor as ConstantDefinition;
				if (constantMultiplier != null)
				{
					return string.Format("{0}{1}", constantMultiplier.Constant == 1 ? string.Empty : constantMultiplier.Constant.ToString(),
						_unitSymbol.Symbol);
				}
				else
				{
					return string.Format("{0}/{1}", _unitSymbol.Symbol, ((UnitSymbolDefinition)_divisor).Symbol);
				}
			}

			public override bool Equals(UnitDefinition unitDefinition)
			{
				// If parameter is null return false.
				if (unitDefinition == null)
				{
					return false;
				}

				// If parameter cannot be cast, return false.
				var divisionDefinition = unitDefinition as DivisionDefinition;
				if ((object)divisionDefinition == null)
				{
					return false;
				}

				return UnitSymbol.Equals(divisionDefinition.UnitSymbol) && Divisor.Equals(divisionDefinition.Divisor);
			}

			public override int GetHashCode()
			{
				return (UnitSymbol, Divisor).GetHashCode();
			}
		}

		private class AdditionDefinition : UnitDefinition
		{
			public UnitSymbolDefinition UnitSymbol => _unitSymbol;
			private UnitSymbolDefinition _unitSymbol;

			public decimal Constant => _constant;
			private decimal _constant;

			public AdditionDefinition(UnitSymbolDefinition unitSymbol, decimal constant)
			{
				_unitSymbol = unitSymbol;
				_constant = constant;
			}

			public override bool Equals(UnitDefinition unitDefinition)
			{
				// If parameter is null return false.
				if (unitDefinition == null)
				{
					return false;
				}

				// If parameter cannot be cast, return false.
				var additionDefinition = unitDefinition as AdditionDefinition;
				if ((object)additionDefinition == null)
				{
					return false;
				}

				return UnitSymbol.Equals(additionDefinition.UnitSymbol) && Constant == additionDefinition.Constant;
			}

			public override int GetHashCode()
			{
				return (UnitSymbol, Constant).GetHashCode();
			}
		}

		public static UnitDefinition Multiply(Unit unit, decimal constant)
		{
			unit = ArgumentValidation.AssertNotNull(unit, nameof(unit));
			return new MultiplicationDefinition(new UnitSymbolDefinition(unit.Symbol), new ConstantDefinition(constant));
		}

		public static UnitDefinition Multiply(Unit leftUnit, Unit rightUnit)
		{
			leftUnit = ArgumentValidation.AssertNotNull(leftUnit, nameof(leftUnit));
			rightUnit = ArgumentValidation.AssertNotNull(rightUnit, nameof(rightUnit));

			if (leftUnit.IsBaseDimensionless)
			{
				return Multiply(rightUnit, 1);
			}
			else if (rightUnit.IsBaseDimensionless)
			{
				return Multiply(leftUnit, 1);
			}
			else
			{
				return new MultiplicationDefinition(new UnitSymbolDefinition(leftUnit.Symbol), new UnitSymbolDefinition(rightUnit.Symbol));
			}
		}

		public static UnitDefinition Divide(Unit leftUnit, Unit rightUnit)
		{
			leftUnit = ArgumentValidation.AssertNotNull(leftUnit, nameof(leftUnit));
			rightUnit = ArgumentValidation.AssertNotNull(rightUnit, nameof(rightUnit));

			if (rightUnit.IsBaseDimensionless)
			{
				return Multiply(leftUnit, 1);
			}
			else
			{
				return new DivisionDefinition(new UnitSymbolDefinition(leftUnit.Symbol), new UnitSymbolDefinition(rightUnit.Symbol));
			}
		}

		public static UnitDefinition Add(Unit unit, decimal constant)
		{
			unit = ArgumentValidation.AssertNotNull(unit, nameof(unit));
			return new AdditionDefinition(new UnitSymbolDefinition(unit.Symbol), constant);
		}

		/// <summary>
		/// Gets a hash code for the unit definition.
		/// </summary>
		/// <returns>A hash code.</returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			var unitDefinition = obj as UnitDefinition;
			if ((object)obj == null)
			{
				return false;
			}

			// Compare the unit definitions
			return Equals(unitDefinition);
		}

		/// <summary>
		/// Compares with another unit definition for equality.
		/// </summary>
		/// <param name="unitDefinition">The unit definition with which to compare.</param>
		/// <returns><c>true</c> if the unit definitions are equal, otherwise <c>false</c>.</returns>
		public abstract bool Equals(UnitDefinition unitDefinition);
		
		/// <summary>
		/// Compares two unit definitions to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(UnitDefinition left, UnitDefinition right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			return left.Equals(right);
		}

		/// <summary>
		/// Compares two unit definitions to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(UnitDefinition left, UnitDefinition right)
		{
			return !(left == right);
		}
	}
}
