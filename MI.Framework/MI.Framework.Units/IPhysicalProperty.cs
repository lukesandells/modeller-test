﻿using System.Collections.Generic;

namespace MI.Framework.Units
{
	public interface IPhysicalProperty
	{
		string Name { get; }
		
		Unit NormativeUnit { get; }
		
		IEnumerable<Unit> Units { get; }

		IReadOnlyDictionary<(decimal, decimal), Unit> UnitsByScaleFactorAndOffset { get; }
	}
}
