﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Reflection;

using MI.Framework.Validation;

namespace MI.Framework.Units
{
	public class UnitLibrary : IUnitLibrary
	{
		protected internal static PhysicalProperty NumberProperty = new PhysicalProperty(PropertyName.Number);

		public static IUnitLibrary Current
		{
			get
			{
				var currentUnitLibrary = CurrentThreadUnitLibrary ?? CurrentProcessUnitLibrary;
				if (currentUnitLibrary == null)
				{
					throw new InvalidOperationException(ExceptionMessage.NoCurrentUnitLibrarySet);
				}

				return currentUnitLibrary;
			}
			set
			{
				value = ArgumentValidation.AssertNotNull(value, nameof(value));
				CurrentThreadUnitLibrary = value;
				CurrentProcessUnitLibrary = value;
			}
		}

		public static IUnitLibrary CurrentProcessUnitLibrary { get; set; }
		
		public static IUnitLibrary CurrentThreadUnitLibrary { get => _currentThreadUnitLibrary.Value; set => _currentThreadUnitLibrary.Value = value; }
		private static ThreadLocal<IUnitLibrary> _currentThreadUnitLibrary = new ThreadLocal<IUnitLibrary>();

		public IEnumerable<IPhysicalProperty> PhysicalProperties => _physicalProperties;
		private ISet<IPhysicalProperty> _physicalProperties = new HashSet<IPhysicalProperty>();

		public IReadOnlyDictionary<string, Unit> UnitsBySymbol => _unitsBySymbol;
		private Dictionary<string, Unit> _unitsBySymbol = new Dictionary<string, Unit>();

		public IReadOnlyDictionary<UnitDefinition, Unit> UnitsByDefinition => _unitsByDefinition;
		private Dictionary<UnitDefinition, Unit> _unitsByDefinition = new Dictionary<UnitDefinition, Unit>();

		public IReadOnlyDictionary<string, Unit> UnitsByCode => _unitsByCode;
		private Dictionary<string, Unit> _unitsByCode = new Dictionary<string, Unit>();

		public UnitLibrary()
		{
			NumberProperty.AddUnit(Unit.BaseDimensionlessUnit);
			AddProperty(NumberProperty);
		}

		public virtual void AddProperty<TPropertyDefinition>() where TPropertyDefinition : IPhysicalPropertyDefinition, new()
		{
			// Get the physical property from the property definition
			var property = new TPropertyDefinition().DefinedProperty;

			// Get the declared units by reflection
			var bindingFlags = BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy;
			foreach (var propertyInfo in typeof(TPropertyDefinition).GetProperties(bindingFlags))
			{
				if (propertyInfo.PropertyType == typeof(Unit))
				{
					property.AddUnit((Unit)propertyInfo.GetValue(null));
				}
			}

			AddProperty(property);
		}

		public virtual void AddProperty(IPhysicalProperty property)
		{
			property = ArgumentValidation.AssertNotNull(property, nameof(property));
			if (property.NormativeUnit == null)
			{
				throw new ArgumentException(ExceptionMessage.PropertyHasNoNormativeUnit);
			}

			_physicalProperties.Add(property);
			property.Units.ForEach(p => _unitsBySymbol.Add(p.Symbol, p));
			property.Units.ForEach(p => _unitsByDefinition.Add(p.Definition, p));
			property.Units.ForEach(p => _unitsByCode.Add(p.Code, p));
		}

		protected virtual void Clear()
		{
			_physicalProperties.Clear();
			_unitsByCode.Clear();
			_unitsByDefinition.Clear();
			_unitsBySymbol.Clear();
		}

		public UnitLibrary CreateExtension()
		{
			return null;
		}

		public UnitLibrary CreateExtension(IEnumerable<Unit> selectedUnits)
		{
			return null;
		}
	}
}
