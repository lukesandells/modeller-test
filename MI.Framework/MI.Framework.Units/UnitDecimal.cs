﻿using System;
using System.Text.RegularExpressions;
using System.Globalization;

using MI.Framework.Validation;

namespace MI.Framework.Units
{
	/// <summary>
	/// Represents a <see cref="Decimal"/> with a corresponding <see cref="Unit"/>.
	/// </summary>
	public struct UnitDecimal
	{
		/// <summary>
		/// Regular expression for parsing unit decimals.
		/// </summary>
		private static Regex _parseRegex = new Regex(@"^(\d*(\.\d+)?([Ee][+-]\d+)?)(\w*)$", RegexOptions.Compiled);

		public decimal DecimalValue => _decimalValue;
		private decimal _decimalValue;

		public Unit Unit => _unit ?? Unit.BaseDimensionlessUnit;
		private Unit _unit;

		internal UnitDecimal(decimal decimalValue, Unit unit)
		{
			_decimalValue = decimalValue;
			_unit = unit;
		}

		public UnitDecimal In(Unit unit)
		{
			unit = ArgumentValidation.AssertNotNull(unit, nameof(unit));
			if (Unit.PhysicalProperty != unit.PhysicalProperty)
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.CannotConvertBetweenUnits, Unit, unit));
			}

			if (Unit == unit)
			{
				return this;
			}

			return new UnitDecimal(DecimalValue * Unit.ScaleFactor / unit.ScaleFactor, unit);
		}

		public static implicit operator UnitDecimal(decimal number)
		{
			return new UnitDecimal(number, Unit.BaseDimensionlessUnit);
		}

		public static implicit operator UnitDecimal(int number)
		{
			return new UnitDecimal(number, Unit.BaseDimensionlessUnit);
		}

		public static explicit operator UnitDecimal(double number)
		{
			return new UnitDecimal((decimal)number, Unit.BaseDimensionlessUnit);
		}

		public static UnitDecimal operator +(UnitDecimal unitDecimal)
		{
			return unitDecimal;
		}

		public static UnitDecimal operator -(UnitDecimal unitDecimal)
		{
			return new UnitDecimal(-unitDecimal.DecimalValue, unitDecimal.Unit);
		}

		public static UnitDecimal operator +(UnitDecimal left, UnitDecimal right)
		{
			if (left.Unit != right.Unit)
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.CannotConvertBetweenUnits, left.Unit, right.Unit));
			}

			if (left.Unit.ScaleFactor > right.Unit.ScaleFactor)
			{
				return new UnitDecimal(left.DecimalValue + right.In(left.Unit).DecimalValue, left.Unit);
			}
			else
			{
				return new UnitDecimal(left.In(right.Unit).DecimalValue + right.DecimalValue, right.Unit);
			}
		}

		public static UnitDecimal operator -(UnitDecimal left, UnitDecimal right)
		{
			if (left.Unit != right.Unit)
			{
				throw new ArgumentOutOfRangeException(string.Format(ExceptionMessage.CannotConvertBetweenUnits, left.Unit, right.Unit));
			}

			if (left.Unit.ScaleFactor > right.Unit.ScaleFactor)
			{
				return new UnitDecimal(left.DecimalValue - right.In(left.Unit).DecimalValue, left.Unit);
			}
			else
			{
				return new UnitDecimal(left.In(right.Unit).DecimalValue - right.DecimalValue, right.Unit);
			}
		}

		public static UnitDecimal operator *(UnitDecimal left, UnitDecimal right)
		{
			// Normalise the operands' units if necessary and multiply
			var unit = left.Unit * right.Unit;
			left = left.Unit.IsDimensionless && !left.Unit.IsNormative &&!right.Unit.IsDimensionless 
				|| unit.IsNormative && !left.Unit.IsNormative ?
				left.In(left.Unit.PhysicalProperty.NormativeUnit) : left;
			right = right.Unit.IsDimensionless && !right.Unit.IsNormative && !left.Unit.IsDimensionless 
				|| unit.IsNormative && !right.Unit.IsNormative ?
				right.In(right.Unit.PhysicalProperty.NormativeUnit) : right;

			return new UnitDecimal(left.DecimalValue * right.DecimalValue, unit);
		}

		public static UnitDecimal operator *(UnitDecimal unitDecimal, decimal constant)
		{
			return new UnitDecimal(unitDecimal.DecimalValue * constant, unitDecimal.Unit);
		}

		public static UnitDecimal operator *(decimal constant, UnitDecimal unitDecimal)
		{
			return unitDecimal * constant;
		}

		public static UnitDecimal operator /(UnitDecimal left, UnitDecimal right)
		{
			// Normalise the operands' units if necessary and divide
			var unit = left.Unit / right.Unit;
			left = left.Unit.IsDimensionless && !left.Unit.IsNormative && !right.Unit.IsDimensionless
				|| unit.IsNormative && !left.Unit.IsNormative ?
				left.In(left.Unit.PhysicalProperty.NormativeUnit) : left;
			right = right.Unit.IsDimensionless && !right.Unit.IsNormative && !left.Unit.IsDimensionless
				|| unit.IsNormative && !right.Unit.IsNormative ?
				right.In(right.Unit.PhysicalProperty.NormativeUnit) : right;

			return new UnitDecimal(left.DecimalValue / right.DecimalValue, unit);
		}

		public static UnitDecimal operator /(UnitDecimal unitDecimal, decimal constant)
		{
			return new UnitDecimal(unitDecimal.DecimalValue * constant, unitDecimal.Unit);
		}

		public static UnitDecimal operator /(decimal constant, UnitDecimal unitDecimal)
		{
			return unitDecimal / constant;
		}

		public static bool TryParse(string s, out UnitDecimal result)
		{
			var symbolMatch = _parseRegex.Match(s);
			if (symbolMatch.Success)
			{
				if (decimal.TryParse(symbolMatch.Groups[1].Value, NumberStyles.Any, CultureInfo.CurrentCulture, out var number))
				{
					if (UnitLibrary.Current.UnitsBySymbol.TryGetValue(symbolMatch.Groups[4].Value, out var unit))
					{
						result = new UnitDecimal(number, unit);
						return true;
					}
				}
			}

			result = 0;
			return false;
		}

		public static UnitDecimal Parse(string s)
		{
			if (!TryParse(s, out var result))
			{
				throw new FormatException();
			}

			return result;
		}

		public static bool TryParse(string decimalString, string unitCode, out UnitDecimal result)
		{
			result = 0;
			if (!UnitLibrary.Current.UnitsByCode.TryGetValue(unitCode, out var unit))
			{
				return false;
			}

			if (!decimal.TryParse(decimalString, out var decimalValue))
			{
				return false;
			}

			result = decimalValue.WithUnit(unit);
			return true;
		}

		public static UnitDecimal Parse(string decimalString, string unitCode)
		{
			if (!TryParse(decimalString, unitCode, out var result))
			{
				throw new FormatException();
			}

			return result;
		}

		/// <summary>
		/// Returns a string representing the <see cref="UnitDecimal"/>.
		/// </summary>
		/// <returns>A string representing the unit decimal.</returns>
		public override string ToString()
		{
			return DecimalValue.ToString("G29") + Unit.Symbol;
		}

		/// <summary>
		/// Returns a string representing the <see cref="UnitDecimal"/>, using the specified format.
		/// </summary>
		/// <param name="format">A standard or custom numeric format string.  See <see cref="Decimal.ToString(string)"/>.</param>
		/// <returns>A string representing the unit decimal.</returns>
		public string ToString(string format)
		{
			return DecimalValue.ToString(format) + Unit.Symbol;
		}

		/// <summary>
		/// Gets a hash code for the unit decimal.
		/// </summary>
		/// <returns>A hash code.</returns>
		public override int GetHashCode()
		{
			return (DecimalValue, Unit).GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns>True if the objects are equal, otherwise false.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			if (!(obj is UnitDecimal))
			{
				return false;
			}

			// Compare the two, with consideration for unit conversions
			var unitDecimal = (UnitDecimal)obj;
			if (unitDecimal.Unit == Unit)
			{
				return DecimalValue == unitDecimal.DecimalValue;
			}
			else if (unitDecimal.Unit.PhysicalProperty == Unit.PhysicalProperty)
			{
				return DecimalValue == unitDecimal.In(Unit).DecimalValue;
			}

			return false;
		}

		/// <summary>
		/// Compares two <see cref="UnitDecimal"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(UnitDecimal left, UnitDecimal right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Compare the two, with consideration for unit conversions
			if (left.Unit == right.Unit)
			{
				return left.DecimalValue == right.DecimalValue;
			}
			else if (left.Unit.PhysicalProperty == right.Unit.PhysicalProperty)
			{
				return left.DecimalValue == right.In(left.Unit).DecimalValue;
			}

			return false;
		}

		/// <summary>
		/// Compares two <see cref="UnitDecimal"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(UnitDecimal left, UnitDecimal right)
		{
			return !(left == right);
		}
	}
}
