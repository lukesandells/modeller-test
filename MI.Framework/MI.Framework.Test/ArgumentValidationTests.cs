﻿using System;
using MI.Framework.Validation;
using NUnit.Framework;
// ReSharper disable RedundantAssignment

namespace MI.Framework.Test
{
	[TestFixture]
	public class ArgumentValidationTests
	{
		[Test]
		public void AssertGreaterThanZero_GreaterThanZero_Passes()
		{
			// Arrange
			var parameter = 1;

			// Act
			parameter = ArgumentValidation.AssertGreaterThanZero(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertGreaterThanZero returned null");
			Assert.AreEqual(1,
				parameter,
				"AssertGreaterThanZero did not return the correct parameter");
		}

		[Test]
		public void AssertNoTimeOfDay_NoDay_Passes()
		{
			// Arrange
			var parameter = new DateTime(2016,
				10,
				11);

			// Act
			parameter = ArgumentValidation.AssertNoTimeOfDay(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertGreaterThanZero returned null");
			Assert.AreEqual(new DateTime(2016,
					10,
					11),
				parameter,
				"AssertGreaterThanZero did not return the correct parameter");
		}

		[Test]
		public void AssertNoTimeOfDay_ZeroDay_Passes()
		{
			// Arrange
			var parameter = new DateTime(2016,
				10,
				11,
				0,
				0,
				0);

			// Act
			parameter = ArgumentValidation.AssertNoTimeOfDay(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertGreaterThanZero returned null");
			Assert.AreEqual(new DateTime(2016,
					10,
					11,
					0,
					0,
					0),
				parameter,
				"AssertGreaterThanZero did not return the correct parameter");
		}

		[Test]
		public void Test_AssertNotNull_NotNull_Passes()
		{
			// Arrange
			var parameter = "parameter";

			// Act
			parameter = ArgumentValidation.AssertNotNull(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertNotNull returned null");
			Assert.AreEqual("parameter",
				parameter,
				"AssertNotNull did not return the correct parameter");
		}

		[Test]
		public void Test_AssertNotNull_Null_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			string parameter = null;

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				// ReSharper disable once RedundantAssignment
				parameter = ArgumentValidation.AssertNotNull(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertNotNull did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentNullException),
				ex.GetType(),
				"AssertNotNull did not throw an ArgumentNullException");
			Assert.AreEqual("parameter name",
				((ArgumentNullException)ex).ParamName,
				"AssertNotNull did not return the correct parameter name");
		}

		[Test]
		public void Test_AssertNotNullOrEmpty_Empty_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			var parameter = "";

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				parameter = ArgumentValidation.AssertNotNullOrEmpty(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertNotNullOrEmpty did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentException),
				ex.GetType(),
				"AssertNotNullOrEmpty did not throw an ArgumentException");
			Assert.AreEqual("parameter name",
				((ArgumentException)ex).ParamName,
				"AssertNotNullOrEmpty did not return the correct parameter name");
		}

		[Test]
		public void Test_AssertNotNullOrEmpty_NotNull_Passes()
		{
			// Arrange
			var parameter = "parameter";

			// Act
			parameter = ArgumentValidation.AssertNotNullOrEmpty(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertNotNullOrEmpty returned null");
			Assert.AreEqual("parameter",
				parameter,
				"AssertNotNullOrEmpty did not return the correct parameter");
		}

		[Test]
		public void Test_AssertNotNullOrEmpty_Null_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			string parameter = null;

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				parameter = ArgumentValidation.AssertNotNullOrEmpty(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertNotNullOrEmpty did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentException),
				ex.GetType(),
				"AssertNotNullOrEmpty did not throw an ArgumentException");
			Assert.AreEqual("parameter name",
				((ArgumentException)ex).ParamName,
				"AssertNotNullOrEmpty did not return the correct parameter name");
		}

		[Test]
		public void TestAssertGreaterThanEqualToZero_GreaterThanZero_Passes()
		{
			// Arrange
			var parameter = 1;

			// Act
			parameter = ArgumentValidation.AssertGreaterThanOrEqualToZero(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertGreaterThanEqualToZero returned null");
			Assert.AreEqual(1,
				parameter,
				"AssertGreaterThanEqualToZero did not return the correct parameter");
		}

		[Test]
		public void TestAssertGreaterThanEqualToZero_LessThanZero_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			var parameter = -1;

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				parameter = ArgumentValidation.AssertGreaterThanOrEqualToZero(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertGreaterThanEqualToZero did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentOutOfRangeException),
				ex.GetType(),
				"AssertGreaterThanEqualToZero did not throw an ArgumentException");
			Assert.AreEqual("parameter name",
				((ArgumentOutOfRangeException)ex).ParamName,
				"AssertGreaterThanEqualToZero did not return the correct parameter name");
		}

		[Test]
		public void TestAssertGreaterThanEqualToZero_Zero_Passes()
		{
			// Arrange
			var parameter = 0;

			// Act
			parameter = ArgumentValidation.AssertGreaterThanOrEqualToZero(parameter,
				"parameter name");

			// Assert
			Assert.IsNotNull(parameter,
				"AssertGreaterThanEqualToZero returned null");
			Assert.AreEqual(0,
				parameter,
				"AssertGreaterThanEqualToZero did not return the correct parameter");
		}

		[Test]
		public void TestAssertGreaterThanZero_LessThanZero_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			var parameter = -1;

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				parameter = ArgumentValidation.AssertGreaterThanZero(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertGreaterThanZero did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentOutOfRangeException),
				ex.GetType(),
				"AssertGreaterThanZero did not throw an ArgumentException");
			Assert.AreEqual("parameter name",
				((ArgumentOutOfRangeException)ex).ParamName,
				"AssertGreaterThanZero did not return the correct parameter name");
		}

		[Test]
		public void TestAssertGreaterThanZero_WithTime_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			var parameter = new DateTime(2016,
				10,
				11,
				16,
				56,
				11);

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				parameter = ArgumentValidation.AssertNoTimeOfDay(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertNoTimeOfDay did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentException),
				ex.GetType(),
				"AssertNoTimeOfDay did not throw an ArgumentException");
			Assert.AreEqual("parameter name",
				((ArgumentException)ex).ParamName,
				"AssertNoTimeOfDay did not return the correct parameter name");
		}

		[Test]
		public void TestAssertGreaterThanZero_Zero_ThrowsException()
		{
			// Arrange
			Exception ex = null;
			var parameter = 0;

			// Act
			try
			{
				// ReSharper disable once ExpressionIsAlwaysNull
				parameter = ArgumentValidation.AssertGreaterThanZero(parameter,
					"parameter name");
			}
			catch (Exception e)
			{
				ex = e;
			}

			// Assert
			Assert.IsNotNull(ex,
				"AssertGreaterThanZero did not throw error for null value");
			Assert.AreEqual(typeof(ArgumentOutOfRangeException),
				ex.GetType(),
				"AssertGreaterThanZero did not throw an ArgumentException");
			Assert.AreEqual("parameter name",
				((ArgumentOutOfRangeException)ex).ParamName,
				"AssertGreaterThanZero did not return the correct parameter name");
		}
	}
}