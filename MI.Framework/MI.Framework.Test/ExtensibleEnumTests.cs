﻿using System.Collections.Generic;
using NUnit.Framework;

namespace MI.Framework.Test
{
	[TestFixture]
	public class ExtensibleEnumTests
	{
		[Test]
		public void Test_Members()
		{
			// Act
			var members = ExtensibleEnum<TestEnum>.Members;

			// Assert
			Assert.AreEqual(3, members.Count, "Extenisble enum did not return correct set of members");
			CollectionAssert.AreEquivalent(new List<ExtensibleEnum<TestEnum>> { TestEnum.A, TestEnum.B, TestEnum.C }, members, "Extenisble enum did not return correct set of members");
		}

		[Test]
		public void Test_ImplicitConversion()
		{
			// Act
			ExtensibleEnum<TestEnum> extensibleEnum = TestEnum.A;

			// Assert
			Assert.IsNotNull(extensibleEnum);
		}

		[Test]
		public void Test_ExplicitConversion()
		{
			// Arrange
			ExtensibleEnum<TestEnum> extensibleEnum = TestEnum.A;

			// Act
			TestEnum testEnum = (TestEnum)extensibleEnum;

			// Assert
			Assert.IsNotNull(testEnum);
			Assert.AreEqual(TestEnum.A, testEnum);

		}

		[Test]
		public void Test_HasOtherValue()
		{

		}
	}

	internal enum TestEnum
	{
		A,
		B,
		C
	}

	internal enum TestOtherEnum
	{
		A,
		B,
		C,
		[OtherValue]
		Other
	}
}
