﻿using System;

namespace MI.Framework.Units.UneceRecommendation20
{
    public class UneceRecommendation20Library : UnitLibrary
    {
		internal static new PhysicalProperty NumberProperty { get; } = UnitLibrary.NumberProperty;

		private bool _isReadOnly;

		public UneceRecommendation20Library()
		{
			Clear();
			AddProperty<Number>();
			AddProperty<Mass>();
			AddProperty<Volume>();
			AddProperty<Length>();
			AddProperty<Time>();
			AddProperty<Velocity>();
			_isReadOnly = true;
		}

		public override void AddProperty(IPhysicalProperty property)
		{
			if (_isReadOnly)
			{
				throw new InvalidOperationException(ExceptionMessage.LibraryIsReadOnly);
			}

			base.AddProperty(property);
		}
	}
}
