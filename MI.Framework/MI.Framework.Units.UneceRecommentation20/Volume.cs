﻿namespace MI.Framework.Units.UneceRecommendation20
{
	public class Volume : IPhysicalPropertyDefinition
	{
		public static IPhysicalProperty Property { get; } = new PhysicalProperty(PropertyName.Volume);

		public static Unit CubicMetre { get; } = Unit.NewBaseUnit(Property, "m3", UnitName.CubicMetre, "MTQ");
		
		public PhysicalProperty DefinedProperty => (PhysicalProperty)Property;
	}
}
