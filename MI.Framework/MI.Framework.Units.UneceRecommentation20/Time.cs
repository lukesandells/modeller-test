﻿namespace MI.Framework.Units.UneceRecommendation20
{
	public class Time : IPhysicalPropertyDefinition
	{
		public static IPhysicalProperty Property { get; } = new PhysicalProperty(PropertyName.Time);

		public static Unit Second { get; } = Unit.NewBaseUnit(Property, "s", UnitName.Second, "SEC");
		public static Unit Minute { get; } = Second.MultipliedBy(60, "min", UnitName.Minute, "MIN");
		public static Unit Hour { get; } = Second.MultipliedBy(3600, "h", UnitName.Hour, "HUR");

		public PhysicalProperty DefinedProperty => (PhysicalProperty)Property;
	}
}
