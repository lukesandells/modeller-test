﻿namespace MI.Framework.Units.UneceRecommendation20
{
	public class Mass : IPhysicalPropertyDefinition
	{
		public static IPhysicalProperty Property { get; } = new PhysicalProperty(PropertyName.Mass);

		public static Unit Kilogram { get; } = Unit.NewBaseUnit(Property, "kg", UnitName.Kilogram, "KGM");
		public static Unit Tonne { get; } = Kilogram.MultipliedBy(1E3M, "t", UnitName.Tonne, "TNE");
		public static Unit Gram { get; } = Kilogram.DividedBy(1E3M, "g", UnitName.Gram, "GRM");
		public static Unit Milligram { get; } = Kilogram.DividedBy(1E6M, "mg", UnitName.Milligram, "MGM");

		public PhysicalProperty DefinedProperty => (PhysicalProperty)Property;
	}
}
