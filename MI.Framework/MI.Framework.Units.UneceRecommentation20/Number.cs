﻿namespace MI.Framework.Units.UneceRecommendation20
{
	public class Number : IPhysicalPropertyDefinition
	{
		public static IPhysicalProperty Property { get; } = UneceRecommendation20Library.NumberProperty;

		public static Unit Percent { get; } = Unit.BaseDimensionlessUnit.DividedBy(100, "%", UnitName.Percent, "PER");
		
		public PhysicalProperty DefinedProperty => (PhysicalProperty)Property;
	}
}
