﻿namespace MI.Framework.Units.UneceRecommendation20
{
	public class Length : IPhysicalPropertyDefinition
	{
		public static IPhysicalProperty Property { get; } = new PhysicalProperty(PropertyName.Length);

		public static Unit Metre { get; } = Unit.NewBaseUnit(Property, "m", UnitName.Metre, "MTR");
		public static Unit Kilometre { get; } = Metre.MultipliedBy(1E3M, "km", UnitName.Kilometre, "KMT");
		public static Unit Millimetre { get; } = Metre.DividedBy(1E3M, "mm", UnitName.Millimetre, "MMT");

		public PhysicalProperty DefinedProperty => (PhysicalProperty)Property;
	}
}
