﻿namespace MI.Framework.Units.UneceRecommendation20
{
	public class Velocity : IPhysicalPropertyDefinition
	{
		public static IPhysicalProperty Property { get; } = new PhysicalProperty(PropertyName.Velocity);

		public static Unit MetrePerSecond { get; } = Length.Metre.DividedBy(Time.Second, Property, "m/s", UnitName.MetrePerSecond, "MTS");
		public static Unit KilometrePerHour { get; } = MetrePerSecond.MultipliedBy(3.6M, "km/h", UnitName.KilometrePerHour, "KMH");

		public PhysicalProperty DefinedProperty => (PhysicalProperty)Property;
	}
}
