﻿using System.Collections.Generic;
using NHibernate.Mapping;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Specifies whether an index is standard, unique or clustered.
	/// </summary>
	public enum IndexType
	{
		Standard,
		Unique,
		Clustered
	}

	/// <summary>
	/// Defines an index.
	/// </summary>
	public class Index : SimpleAuxiliaryDatabaseObject
	{
		/// <summary>
		/// The set of declared indices.
		/// </summary>
		private static IDictionary<string, Index> _declaredIndices = new Dictionary<string, Index>();

		/// <summary>
		/// Gets the declared indices.
		/// </summary>
		public static IEnumerable<Index> DeclaredIndices { get => _declaredIndices.Values; }

		/// <summary>
		/// Declares a new index.
		/// </summary>
		/// <param name="indexName">The name of the index.</param>
		/// <param name="indexType">The type of the index.</param>
		/// <param name="tableName">The name of the table on which to create the index.</param>
		/// <param name="columns">The indexed columns.</param>
		public static void Declare(string indexName, IndexType indexType, string tableName, string[] columns)
		{
			_declaredIndices[indexName] = new Index(indexName, indexType, tableName, columns);
		}

		/// <summary>
		/// Declares a new index if no index with the given index name has yet been declared.
		/// </summary>
		/// <param name="indexName">The name of the index.</param>
		/// <param name="indexType">The type of the index.</param>
		/// <param name="tableName">The name of the table on which to create the index.</param>
		/// <param name="columns">The indexed columns.</param>
		public static void DeclareIfNotDeclared(string indexName, IndexType indexType, string tableName, string[] columns)
		{
			if (!IsDeclared(indexName))
			{
				_declaredIndices[indexName] = new Index(indexName, indexType, tableName, columns);
			}
		}

		/// <summary>
		/// Returns whether an index with the given name has been declared.
		/// </summary>
		/// <param name="indexName">The name of the index.</param>
		/// <returns><c>true</c> if an index with the given name has been declared; otherwise <c>false</c>.</returns>
		public static bool IsDeclared(string indexName)
		{
			return _declaredIndices.ContainsKey(indexName);
		}

		/// <summary>
		/// Creates SQL text for an index type.
		/// </summary>
		/// <param name="indexType">The type of index.</param>
		/// <returns>SQL text for the given index type.</returns>
		private static string IndexTypeString(IndexType indexType)
		{
			if (indexType == IndexType.Standard)
			{
				return string.Empty;
			}
			else
			{
				return indexType.ToString().ToLower();
			}
		}

		/// <summary>
		/// Creates a new index.
		/// </summary>
		/// <param name="indexName">The name of the index.</param>
		/// <param name="indexType">The type of the index.</param>
		/// <param name="tableName">The name of the table on which to create the index.</param>
		/// <param name="columns">The indexed columns.</param>
		public Index(string indexName, IndexType indexType, string tableName, string[] columns) : base(
			string.Format("create {0} index {1} ON {2} ({3})", IndexTypeString(indexType), indexName, tableName, string.Join(", ", columns)),
			string.Format("drop index if exists {0}", indexName))
		{
		}
	}
}
