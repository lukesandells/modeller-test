﻿namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Configuration base class for all NHibernate configurations.
	/// </summary>
	public abstract class NHibernateConfiguration : global::NHibernate.Cfg.Configuration
	{
		/// <summary>
		/// Adds any declared indices (see <see cref="Index.Declare(string, IndexType, string, string[])"/>) not already included in the configuration.
		/// </summary>
		public void AddDeclaredIndices()
		{
			// Add indices if not already added
			foreach (var index in Index.DeclaredIndices)
			{
				if (!auxiliaryDatabaseObjects.Contains(index))
				{
					AddAuxiliaryDatabaseObject(index);
				}
			}
		}
	}
}
