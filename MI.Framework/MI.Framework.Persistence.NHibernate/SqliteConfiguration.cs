﻿using System;
using System.Reflection;
using System.IO;
using System.Linq;

using NHibernate.Cfg;
using NHibernate.Connection;
using NHibernate.Dialect;
using NHibernate.Driver;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Mapping.ByCode.Conformist;

using MI.Framework.Validation;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Configuration for an SQLite database.
	/// </summary>
	public class SqliteConfiguration : NHibernateConfiguration
	{
		/// <summary>
		/// Database connection information.
		/// </summary>
		public class ConnectionString
		{
			/// <summary>
			/// Database path.
			/// </summary>
			public string DatabasePath { get; set; }

			/// <summary>
			/// Use connection pooling.
			/// </summary>
			public bool Pooling { get; set; }

			/// <summary>
			/// Maximum connection pool size.
			/// </summary>
			public int? MaxPoolSize { get; set; }

			/// <summary>
			/// Fail if the database file is missing.
			/// </summary>
			public bool FailIfMissing { get; set; }

			/// <summary>
			/// Use binary GUIDs.
			/// </summary>
			public bool BinaryGuid { get; set; }

			/// <summary>
			/// Create new database.
			/// </summary>
			public bool New { get; set; }

			/// <summary>
			/// Compressed database.
			/// </summary>
			public bool Compress { get; set; }

			/// <summary>
			/// SQLite database version.
			/// </summary>
			public int Version { get; set; } = 3;

			/// <summary>
			/// Stipulates whether to use UTF-16 encoding.
			/// </summary>
			public bool UseUtf16Encoding { get; set; } = false;

			/// <summary>
			/// The cache size.
			/// </summary>
			public int? CacheSize { get; set; }

			/// <summary>
			/// The page size.
			/// </summary>
			public int? PageSize { get; set; }

			/// <summary>
			/// Returns the string representation of the connection string.
			/// </summary>
			/// <returns>The string representation of the connection string</returns>
			public override string ToString()
			{
				var connectionString = string.Format("Data Source={0};Pooling={1};FailIfMissing={2};BinaryGUID={3};New={4};Compress={5};Version={6};UseUTF16Encoding={7};",
					DatabasePath, Pooling, FailIfMissing, BinaryGuid, New, Compress, Version, UseUtf16Encoding);
				if (CacheSize.HasValue)
				{
					connectionString = connectionString + string.Format("Cache Size={0};", CacheSize.Value);
				}
				if (PageSize.HasValue)
				{
					connectionString = connectionString + string.Format("Page Size={0};", PageSize.Value);
				}
				if (MaxPoolSize.HasValue)
				{
					connectionString = connectionString + string.Format("Max Pool Size={0};", MaxPoolSize.Value);
				}

				return connectionString;
			}
		}

		/// <summary>
		/// The database connection string.
		/// </summary>
		private ConnectionString _connectionString;

		/// <summary>
		/// The NHibernate mapper.
		/// </summary>
		private ModelMapper _mapper = new ModelMapper();

		/// <summary>
		/// Creates a new SQLite configuration.
		/// </summary>
		/// <param name="connectionString">Database connection information.</param>
		/// <param name="logSqlInConsole">Indicates whether to log SQL to the console.</param>
		public SqliteConfiguration(ConnectionString connectionString, bool logSqlInConsole)
		{
			_connectionString = ArgumentValidation.AssertNotNull(connectionString, nameof(connectionString));
			this.DataBaseIntegration(db =>
			{
				db.ConnectionProvider<DriverConnectionProvider>();
				db.Dialect<SQLiteDialect>();
				db.Driver<SQLite20Driver>();
				db.ConnectionString = _connectionString.ToString();
				db.LogSqlInConsole = logSqlInConsole;
			});
		}

		/// <summary>
		/// Adds the mappings in the given assembly to the configuration.
		/// </summary>
		/// <param name="mappingAssembly">Assembly containing the mappings.</param>
		public void AddMappingAssembly(Assembly mappingAssembly)
		{
			mappingAssembly = ArgumentValidation.AssertNotNull(mappingAssembly, nameof(mappingAssembly));

			// The OrderBy here is a workaround for an NHibernate bug where it isn't retrieving the mapping types in the order of
			// inheritance depth, thereby resuling in an error where mappings for subclasses extend classes that have mappings that 
			// aren't yet compiled.
			var mappingTypes = new[] { typeof(ClassMapping<>), typeof(SubclassMapping<>), typeof(UnionSubclassMapping<>), typeof(JoinedSubclassMapping<>) };
			_mapper.AddMappings(mappingAssembly.GetExportedTypes()
				.Where(type => !type.IsAbstract)
				.Where(type => typeof(IConformistHoldersProvider).IsAssignableFrom(type) && !type.IsGenericTypeDefinition)
				.OrderBy(type => GetFirstBaseTypeWithInterface(type, typeof(IConformistHoldersProvider)).GetGenericArguments()[0].GetInheritanceDepth()));
		}

		/// <summary>
		/// Gets the first base type of a type that has the given generic type definition.
		/// </summary>
		/// <param name="type">The type for which to find the base type.</param>
		/// <param name="interfaceType">The interface to find.</param>
		/// <returns>The first base type of a type that has the given generic type definition.</returns>
		private Type GetFirstBaseTypeWithInterface(Type type, Type interfaceType)
		{
			while (type.BaseType != null)
			{
				type = type.BaseType;
				if (interfaceType.IsAssignableFrom(type) && !interfaceType.IsAssignableFrom(type.BaseType))
				{
					return type;
				}
			}

			return null;
		}

		/// <summary>
		/// Compile the mappings in the added mapping assemblies (see <see cref="AddMappingAssembly(Assembly)"/>).
		/// </summary>
		public void CompileMappings()
		{
			AddDeserializedMapping(_mapper.CompileMappingForAllExplicitlyAddedEntities(), Assembly.GetExecutingAssembly().FullName);
		}

		/// <summary>
		/// Creates a new database file.
		/// </summary>
		/// <param name="replaceIfExists">Indicates whether to replace the database file if it exists.</param>
		/// <param name="logSqlInConsole">Indicates whether to log SQL to the console.</param>
		public void CreateDatabase(bool replaceIfExists, bool logSqlInConsole)
		{
			if (replaceIfExists && File.Exists(_connectionString.DatabasePath))
			{
				File.Delete(_connectionString.DatabasePath);
			}

			if (!File.Exists(_connectionString.DatabasePath))
			{
				// Add any declared indices to the configuration
				AddDeclaredIndices();

				// Export the schema
				new SchemaExport(this).Create(logSqlInConsole, true);
			}
		}
	}
}
