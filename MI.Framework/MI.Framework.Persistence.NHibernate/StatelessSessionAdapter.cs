﻿using System.Data;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;

using NHibernate;
using NHibernate.Linq;

using MI.Framework.Validation;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Adapter that mediates between the <see cref="IStatelessPersistenceSession"/> and <see cref="IStatelessSession"/> interfaces.
	/// </summary>
	public class StatelessSessionAdapter : IStatelessPersistenceSession
	{
		/// <summary>
		/// The inner NHibernate session.
		/// </summary>
		private IStatelessSession _innerSession;

		/// <summary>
		/// Gets the inner NHibernate session.
		/// </summary>
		public IStatelessSession InnerSession
		{
			get { return _innerSession; }
		}

		/// <summary>
		/// The current transaction.
		/// </summary>
		private ITransaction _transaction;

		/// <summary>
		/// Gets the current transaction.
		/// </summary>
		public ITransaction Transaction
		{
			get { return _transaction; }
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="StatelessSessionAdapter"/> class.
		/// </summary>
		/// <param name="innerSession">The inner NHibernate session.</param>
		public StatelessSessionAdapter(IStatelessSession innerSession)
		{
			_innerSession = ArgumentValidation.AssertNotNull(innerSession, "innerSession");
		}

		/// <summary>
		/// Begins a new transaction.
		/// </summary>
		/// <param name="isolationLevel">The transaction isolation level.</param>
		/// <returns>The new transaction.</returns>
		public ITransaction BeginTransaction(IsolationLevel isolationLevel)
		{
			_transaction = new TransactionAdapter(_innerSession.BeginTransaction(isolationLevel));
			return _transaction;
		}

		/// <summary>
		/// Gets the current database connection.
		/// </summary>
		public DbConnection Connection
		{
			get { return _innerSession.Connection; }
		}

		/// <summary>
		/// Closes the session.
		/// </summary>
		public void Close()
		{
			_innerSession.Close();
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id) where T : class
		{
			return _innerSession.Get<T>(id);
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id, bool acquireLock) where T : class
		{
			return _innerSession.Get<T>(id, LockMode.Upgrade);
		}

		/// <summary>
		/// Inserts an entity.
		/// </summary>
		/// <param name="entity">A new transient instance.</param>
		/// <returns>The identifier of the instance.</returns>
		public object Insert(object entity)
		{
			return _innerSession.Insert(entity);
		}

		/// <summary>
		/// Update an entity.
		/// </summary>
		/// <param name="entity">A detached entity instance.</param>
		public void Update(object entity)
		{
			_innerSession.Update(entity);
		}

		/// <summary>
		/// Delete an entity.
		/// </summary>
		/// <param name="entity">A detached entity instance.</param>
		public void Delete(object entity)
		{
			_innerSession.Delete(entity);
		}

		/// <summary>
		/// Refresh the entity instance state from the database.
		/// </summary>
		/// <param name="entity">The entity to be refreshed.</param>
		public void Refresh(object entity)
		{
			_innerSession.Refresh(entity);
		}

		/// <summary>
		/// Refresh the entity instance state from the database.
		/// </summary>
		/// <param name="entity">The entity to be refreshed.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		public void Refresh(object entity, bool acquireLock)
		{
			_innerSession.Refresh(entity, LockMode.Upgrade);
		}

		/// <summary>
		/// Retrieves all entities of the type designated for the repository from the database.
		/// </summary>
		/// <returns>A list of all entities.</returns>
		public IList<T> ListAll<T>() where T : class
		{
			return _innerSession.CreateCriteria(typeof(T)).List<T>();
		}

		/// <summary>
		/// Provides a queryable interface for objects of the given type.
		/// </summary>
		/// <typeparam name="T">The type of entity to find.</typeparam>
		/// <returns>A queryable interface objects of the given type.</returns>
		public IQueryable<T> Query<T>() where T : class
		{
			return _innerSession.Query<T>();
		}

		/// <summary>
		/// Disposes of this session.
		/// </summary>
		public void Dispose()
		{
			_innerSession.Dispose();
		}
	}
}
