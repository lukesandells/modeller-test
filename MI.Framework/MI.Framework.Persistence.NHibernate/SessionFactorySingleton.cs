using NHibernate;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Singleton class to house single global instance of NHibernate session factory.
	/// </summary>
	internal sealed class SessionFactorySingleton
	{
		private static readonly SessionFactorySingleton _self = new SessionFactorySingleton();
		private ISessionFactory _sessionFactory;

		/// <summary>
		/// Private constructor - gives us control over instantiation.
		/// </summary>
		private SessionFactorySingleton()
		{
		}
		
		/// <summary>
		/// The Singleton instance.
		/// </summary>
		public static SessionFactorySingleton Self
		{
			get 
			{ 
				// Check if the session factory has been initialised.  If not, throw a meaningful exception.
				if( _self == null )
				{
					throw new FrameworkException( "Attempt to retrieve session factory before it has been initialised." );
				}

				return _self; 
			}
		}

		/// <summary>
		/// Build the session factory singleton instance used to construct all NHibernate sessions.
		/// </summary>
		/// <param name="config">An NHibernate Configuration object from which the session factory will be built.</param>
		public static void BuildSessionFactory( global::NHibernate.Cfg.Configuration config )
		{
			_self._sessionFactory = config.BuildSessionFactory();
		}

		/// <summary>
		/// The encapsulated NHibernate factory.
		/// </summary>
		public ISessionFactory Factory
		{
			get { return _sessionFactory; }
		}
	}
}
