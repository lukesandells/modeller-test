using NHibernate;
using MI.Framework.Contexts;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Helper class to get access to the current NHibernate session.
	/// </summary>
	public static class NHibernateSession
	{
		/// <summary>
		/// Gets the current NHibernate session.
		/// </summary>
		public static ISession Current
		{
			get
			{
				// Get the current persistence context.
				PersistenceContext context = Context.GetCurrent<PersistenceContext>();

				// Found?
				if( context != null )
				{
					// Yep, then get the inner session from our session adapter
					StatefulSessionAdapter adapter = ( StatefulSessionAdapter )context.Session;
					return adapter.InnerSession;
				}

				// No context found
				return null;
			}
		}
	}
}
