﻿using System;
using System.Data;
using System.Data.Common;
using NHibernate;
using NHibernate.SqlTypes;
using NHibernate.UserTypes;

using MI.Framework.Validation;
using NHibernate.Engine;

namespace MI.Framework.Persistence.NHibernate.Types
{
	/// <summary>
	/// Represents the persistent type for <see cref="ExtensibleEnumType{TEnum}" />.
	/// </summary>
	public class ExtensibleEnumType<TEnum> : IUserType where TEnum : struct
	{
		/// <summary>
		/// The list of SQL types to which the <see cref="ExtensibleEnumType{TEnum}"/> type maps.
		/// </summary>
		private SqlType[] _sqlTypes = new SqlType[] { NHibernateUtil.String.SqlType };

		/// <summary>
		/// Tests two <see cref="ExtensibleEnumType{TEnum}" /> values for equality.
		/// </summary>
		/// <param name="x">The <see cref="ExtensibleEnumType{TEnum}"/> value on the left hand side of the equality operator.</param>
		/// <param name="y">The <see cref="ExtensibleEnumType{TEnum}"/> value on the right hand side of the equality operator.</param>
		/// <returns>A <see cref="Boolean"/> indicating whether the two <see cref="ExtensibleEnumType{TEnum}"/> values are equal.</returns>
		public new bool Equals(object x, object y)
		{
			if (ReferenceEquals(x, y))
			{
				return true;
			}

			return (ExtensibleEnum<TEnum>?)x == (ExtensibleEnum<TEnum>?)y;
		}

		/// <summary>
		/// Get a hashcode for the instance, consistent with persistence "equality".
		/// </summary>
		/// <param name="x">The object for which to get the hashcode.</param>
		/// <returns>The hashcode for the given object.</returns>
		public int GetHashCode(object x)
		{
			if (x != null)
			{
				return x.GetHashCode();
			}

			return 0;
		}

		/// <summary>
		/// Creates a deep copy of a <see cref="ExtensibleEnumType{TEnum}"/> value.
		/// </summary>
		/// <param name="value">The <see cref="ExtensibleEnumType{TEnum}"/> value to be copied.</param>
		/// <returns>A deep copy of the given <see cref="ExtensibleEnumType{TEnum}"/> value.</returns>
		public object DeepCopy(object value)
		{
			return (ExtensibleEnum<TEnum>?)value;
		}

		/// <summary>
		/// Gets a value indicating whether the <see cref="ExtensibleEnumType{TEnum}"/> type is mutable.
		/// </summary>
		/// <remarks>
		/// The <see cref="ExtensibleEnumType{TEnum}"/> type is not mutable so this method always returns <c>false</c>.
		/// </remarks>
		public bool IsMutable
		{
			get { return false; }
		}

		/// <summary>
		/// Performs a read of a <see cref="ExtensibleEnumType{TEnum}"/> value from an implementation of <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="rs">An implmentation of <see cref="IDataReader"/> containing the <see cref="ExtensibleEnumType{TEnum}"/> value to be read.</param>
		/// <param name="names">The array of field names to map to the <see cref="ExtensibleEnumType{TEnum}"/> value.</param>
		/// <param name="owner">The containing entity.</param>
		/// <returns>The <see cref="ExtensibleEnumType{TEnum}"/> value read from the <see cref="IDataReader"/> implementation.</returns>
		/// <param name="session">The session for which the operation is done. Allows access to
		/// <c>Factory.Dialect</c> and <c>Factory.ConnectionProvider.Driver</c> for adjusting to
		/// database or data provider capabilities.</param>
		public object NullSafeGet(DbDataReader rs, string[] names, ISessionImplementor session, object owner)
		{
			// Get the field value from the database
			var enumString = (string)NHibernateUtil.String.NullSafeGet(rs, names[0], session);
			
			// Is it null?
			if (enumString == null)
			{
				// Yep, then return the default value
				return default(ExtensibleEnum<TEnum>?);
			}
			else
			{
				// Not null, so get the string value
				return ExtensibleEnum<TEnum>.Parse(enumString);
			}
		}

		/// <summary>
		/// Performs a write of a <see cref="ExtensibleEnumType{TEnum}"/> value to the parameters of an implementation of <see cref="IDbCommand"/>.
		/// </summary>
		/// <param name="cmd">An implementation of <see cref="IDbCommand"/> representing a write command to be executed against the database.</param>
		/// <param name="value">The <see cref="ExtensibleEnumType{TEnum}"/> value to be written to the database.</param>
		/// <param name="index">The command parameter index.</param>
		/// <param name="session">The session for which the operation is done. Allows access to
		/// <c>Factory.Dialect</c> and <c>Factory.ConnectionProvider.Driver</c> for adjusting to
		/// database or data provider capabilities.</param>
		public void NullSafeSet(DbCommand cmd, object value, int index, ISessionImplementor session)
		{
			cmd = ArgumentValidation.AssertNotNull(cmd, "cmd");

			if (value == null)
			{
				((IDbDataParameter)cmd.Parameters[index]).Value = DBNull.Value;
			}
			else
			{
				((IDbDataParameter)cmd.Parameters[index]).Value = ((ExtensibleEnum<TEnum>)value).ToString();
			}
		}

		/// <summary>
		/// Gets the .NET type represented by this NHibernate type.
		/// </summary>
		public Type ReturnedType
		{
			get { return typeof(ExtensibleEnum<TEnum>?); }
		}

		/// <summary>
		/// Gets the SQL types to which the <see cref="ExtensibleEnumType{TEnum}"/> type maps.
		/// </summary>
		public SqlType[] SqlTypes
		{
			get { return _sqlTypes; }
		}

		/// <summary>
		/// Called when an entity is read from the second-level cache.
		/// </summary>
		/// <param name="cached">The cached object.</param>
		/// <param name="owner">The owning object.</param>
		/// <returns>A copy of the entity, reconstituted from the cache.</returns>
		public object Assemble(object cached, object owner)
		{
			return DeepCopy(cached);
		}

		/// <summary>
		/// Called when an entity is written to the second-level cache.
		/// </summary>
		/// <param name="value">The object to be cached.</param>
		/// <returns>A copy of the object to be stored in the cache.</returns>
		public object Disassemble(object value)
		{
			return DeepCopy(value);
		}

		/// <summary>
		/// During merge, replace the existing (target) value in the entity we are merging to with a new 
		/// (original) value from the detached entity we are merging. For immutable objects, or null 
		/// values, it is safe to simply return the first parameter. For mutable objects, it is safe to 
		/// return a copy of the first parameter. However, since composite user types often define component 
		/// values, it might make sense to recursively replace component values in the target object.
		/// </summary>
		/// <param name="original">The new value.</param>
		/// <param name="target">The existing value.</param>
		/// <param name="owner">The owning entity.</param>
		/// <returns>The replacement value.</returns>
		public object Replace(object original, object target, object owner)
		{
			// Immuatable type, so okay to return original
			return original;
		}
	}
}
