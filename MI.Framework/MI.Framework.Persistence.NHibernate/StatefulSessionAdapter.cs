using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;

using NHibernate;
using NHibernate.Linq;

using MI.Framework.Validation;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Adapter that mediates between the <see cref="Persistence.IPersistenceSession"/> and <see cref="ISession"/> interfaces.
	/// </summary>
	public class StatefulSessionAdapter : IStatefulPersistenceSession
	{
		/// <summary>
		/// The inner NHibernate session.
		/// </summary>
		private ISession _innerSession;

		/// <summary>
		/// Gets the inner NHibernate session.
		/// </summary>
		public ISession InnerSession
		{
			get { return _innerSession; }
		}

		/// <summary>
		/// The current transaction.
		/// </summary>
		private ITransaction _transaction;

		/// <summary>
		/// Gets the current transaction.
		/// </summary>
		public ITransaction Transaction
		{
			get { return _transaction; }
		}

		/// <summary>
		/// The entities held in the session cache.
		/// </summary>
		public IEnumerable<object> Entities => _innerSession.GetSessionImplementation().PersistenceContext.EntitiesByKey.Values;

		/// <summary>
		/// Initialises a new instance of the <see cref="StatefulSessionAdapter"/> class.
		/// </summary>
		/// <param name="innerSession">The inner NHibernate session.</param>
		public StatefulSessionAdapter( global::NHibernate.ISession innerSession )
		{
			_innerSession = ArgumentValidation.AssertNotNull( innerSession, "innerSession" );
		}

		/// <summary>
		/// Begins a new transaction.
		/// </summary>
		/// <param name="isolationLevel">The transaction isolation level.</param>
		/// <returns>The new transaction.</returns>
		public ITransaction BeginTransaction( IsolationLevel isolationLevel )
		{
			_transaction = new TransactionAdapter( _innerSession.BeginTransaction( isolationLevel ) );
			return _transaction;
		}

		/// <summary>
		/// Gets the current database connection.
		/// </summary>
		public DbConnection Connection
		{
			get { return _innerSession.Connection; }
		}

		/// <summary>
		/// Closes the session.
		/// </summary>
		public void Close()
		{
			_innerSession.Close();
		}

		/// <summary>
		/// Flushes the data cached by the session to persistent storage.
		/// </summary>
		public void Flush()
		{
			_innerSession.Flush();
		}

		/// <summary>
		/// Clears the session.
		/// </summary>
		public void Clear()
		{
			_innerSession.Clear();
		}

		/// <summary>
		/// Attaches an entity to the session.
		/// </summary>
		/// <param name="entity">The entity to attach.</param>
		public void Attach(object entity)
		{
			// Ensure entity not null and the entity is not already attached
			if(entity != null && !_innerSession.Contains(entity))
			{
				_innerSession.Lock(entity, LockMode.None);
			}
		}

		/// <summary>
		/// Re-read the state of the given instance from the underlying database.
		/// </summary>
		/// <param name="entity">The persistent entity to refresh.</param>
		public void Refresh(object entity)
		{
			_innerSession.Refresh(entity);
		}

		/// <summary>
		/// Evicts the given entity from this session.
		/// </summary>
		/// <param name="entity">The entity to evict.</param>
		public void Evict( object entity )
		{
			_innerSession.Evict( entity );
		}

		/// <summary>
		/// Intialises a lazy loaded entity.
		/// </summary>
		/// <param name="entity">The entity to initialise.</param>
		public void Initialise( object entity )
		{
			NHibernateUtil.Initialize( entity );
		}

		/// <summary>
		/// Persists an entity in the database.
		/// </summary>
		/// <typeparam name="T">The type of entity to persist.</typeparam>
		/// <param name="entity">The entity to persisted.</param>
		/// <param name="behaviour">The required behaviour of the persist operation.</param>
		public void Persist(object entity, PersistBehaviour behaviour)
		{
			// Save the entity
			switch(behaviour)
			{
				case PersistBehaviour.QueryIfNotInferable:
					_innerSession.SaveOrUpdate(entity);
					return;

				case PersistBehaviour.SaveIfNotInferable:
					_innerSession.Persist(entity);
					return;
			}
		}

		/// <summary>
		/// Saves a transient entity to the database.
		/// </summary>
		/// <param name="entity">The entity to save.</param>
		public void Save(object entity)
		{
			_innerSession.Save(entity);
		}

		/// <summary>
		/// Updates a persistent entity in the database.
		/// </summary>
		/// <param name="entity">The entity to update.</param>
		public void Update(object entity)
		{
			_innerSession.Update(entity);
		}

		/// <summary>
		/// Merges an entity into the database.
		/// </summary>
		/// <param name="entity">The transient or persistent entity to be merged into the database.</param>
		/// <returns>The newly merged entity.</returns>
		/// <remarks>
		/// If the given entity is not resident in the session it is loaded from the database before being merged
		/// with the given <paramref name="entity"/>.
		/// </remarks>
		public T Merge<T>(T entity) where T : class
		{
			return _innerSession.Merge(entity);
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id) where T : class
		{
			return FindById<T>(id, false);
		}

		/// <summary>
		/// Finds the entity with the given identifier.
		/// </summary>
		/// <param name="id">The database identifier of the entity to be retrieved.</param>
		/// <param name="acquireLock">Indicates whether an update lock should be acquired.</param>
		/// <returns>The retrieved entity if found; otherwise <c>null</c>.</returns>
		public T FindById<T>(object id, bool acquireLock) where T : class
		{
			if (acquireLock)
			{
				return (T)_innerSession.Get(typeof(T), id, LockMode.Upgrade);
			}
			else
			{
				return (T)_innerSession.Get(typeof(T), id);
			}
		}
		
		/// <summary>
		/// Retrieves all entities of the type designated for the repository from the database.
		/// </summary>
		/// <returns>A list of all entities.</returns>
		public IList<T> ListAll<T>() where T : class
		{
			return ListAll<T>(false);
		}

		/// <summary>
		/// Retrieves all entities of the type designated for the repository from the database.
		/// </summary>
		/// <param name="cacheable">Indicates whether the results should be cached in the second level cache.</param>
		/// <returns>A list of all entities.</returns>
		public IList<T> ListAll<T>(bool cacheable) where T : class
		{
			return _innerSession.CreateCriteria(typeof(T)).SetCacheable(cacheable).List<T>();
		}

		/// <summary>
		/// Deletes a persistent entity from the repository.
		/// </summary>
		/// <param name="entity">The persistent entity to be deleted from the repository.</param>
		public void Delete(object entity)
		{
			_innerSession.Delete(entity);
		}

		/// <summary>
		/// Provides a queryable interface for objects of the given type.
		/// </summary>
		/// <typeparam name="T">The type of entity to find.</typeparam>
		/// <returns>A queryable interface objects of the given type.</returns>
		public IQueryable<T> Query<T>() where T : class
		{
			return _innerSession.Query<T>();
		}

		/// <summary>
		/// Disposes of this session.
		/// </summary>
		public void Dispose()
		{
			_innerSession.Dispose();
		}
	}
}
