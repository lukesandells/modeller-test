using System.Data.Common;
using MI.Framework.Validation;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Adapter that mediates between the <see cref="Persistence.ITransaction"/> and <see cref="global::NHibernate.ITransaction"/> interfaces.
	/// </summary>
	internal class TransactionAdapter : ITransaction
	{
		/// <summary>
		/// The inner NHibernate transaction.
		/// </summary>
		private global::NHibernate.ITransaction _innerTransaction;

		/// <summary>
		/// Initialises a new instance of the <see cref="TransactionAdapter"/> class.
		/// </summary>
		/// <param name="innerTransaction">The inner NHibernate transaction.</param>
		internal TransactionAdapter( global::NHibernate.ITransaction innerTransaction )
		{
			_innerTransaction = ArgumentValidation.AssertNotNull( innerTransaction, "innerTransaction" );
		}

		/// <summary>
		/// Commits the transaction.
		/// </summary>
		public void Commit()
		{
			_innerTransaction.Commit();
		}

		/// <summary>
		/// Enlists a database command in the transaction.
		/// </summary>
		/// <param name="command"></param>
		public void Enlist( DbCommand command )
		{
			_innerTransaction.Enlist( command );
		}

		/// <summary>
		/// Rolls back the transaction.
		/// </summary>
		public void Rollback()
		{
			_innerTransaction.Rollback();
		}
	}
}
