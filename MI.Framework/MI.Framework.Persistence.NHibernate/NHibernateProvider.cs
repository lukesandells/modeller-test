using System.Collections.Specialized;

using MI.Framework.Validation;

namespace MI.Framework.Persistence.NHibernate
{
	/// <summary>
	/// Provides persistence related functionality based on the NHibernate library.
	/// </summary>
	public sealed class NHibernateProvider : IPersistenceProvider
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="PersistenceProvider"/> class.
		/// </summary>
		public NHibernateProvider()
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="PersistenceProvider"/> class.
		/// </summary>
		/// <param name="parameters">The provider parameters.</param>
		public NHibernateProvider( NameValueCollection parameters )
		{
		}

		/// <summary>
		/// Configure the persistence characteristics for the AppDomain.
		/// </summary>
		/// <param name="config">An NHibernate <see cref="global::NHibernate.Cfg.Configuration" /> object.</param>
		public static void Configure(NHibernateConfiguration config)
		{
			// Add the declared indices
			ArgumentValidation.AssertNotNull(config, nameof(config)).AddDeclaredIndices();
			SessionFactorySingleton.BuildSessionFactory(config);
		}

		/// <summary>
		/// Opens a new session.
		/// </summary>
		/// <param name="stateless">Stipulates whether to open a stateless session.</param>
		/// <returns>A new session.</returns>
		public IPersistenceSession OpenSession( bool stateless )
		{
			// Do we have a session factory?
			if( SessionFactorySingleton.Self.Factory == null )
			{
				// No, then throw an exception
				throw new FrameworkException(ExceptionMessage.ProviderNotConfigured);
			}

			// Create a new session
			if (stateless)
			{
				global::NHibernate.IStatelessSession session = SessionFactorySingleton.Self.Factory.OpenStatelessSession();
				return new StatelessSessionAdapter(session);
			}
			else
			{
				global::NHibernate.ISession session = SessionFactorySingleton.Self.Factory.OpenSession();
				return new StatefulSessionAdapter(session);
			}
		}
	}
}
