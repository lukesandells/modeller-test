﻿namespace MI.Framework.ObjectBrowsing.Persistence
{
	public enum FolderMetaType
	{
		NodeFolder = 1,
		PropertyLinksFolder = 2,
		RecycleBin = 3
	}
}
