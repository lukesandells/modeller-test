﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using System;
using NHibernate.Linq;
using MI.Framework.Persistence;
using MI.Framework.Validation;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	/// <summary>
	/// Persistence extension methods for enumerables of <see cref="BrowserNode"/> objects.
	/// </summary>
	public static class BrowserNodeEnumerableExtensions
	{
		/// <summary>
		/// Saves a collection of transient nodes and all their descendant transient nodes to the database.
		/// </summary>
		/// <param name="nodes">The nodes to save.</param>
		public static void Save(this IEnumerable<BrowserNode> nodes)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Save the nodes
				nodes.ForEach(node => scope.Persist(node, PersistBehaviour.SaveIfNotInferable));

				// Need to save the definition nodes before the link nodes because the link nodes 
				// may reference the definition nodes
				var descendants = nodes.SelectMany(node => node.DescendantsOrderedByDepth);
				descendants.OfType<DefinitionNode>().ForEach(n => scope.Persist(n, PersistBehaviour.SaveIfNotInferable));
				descendants.OfType<LinkNode>().ForEach(n => scope.Persist(n, PersistBehaviour.SaveIfNotInferable));
				scope.Done();
			}
		}

		/// <summary>
		/// Updates the collection of nodes in the database.
		/// </summary>
		/// <param name="nodesToUpdate">The nodes to update.</param>
		public static void Update(this IEnumerable<BrowserNode> nodesToUpdate)
		{
			nodesToUpdate = ArgumentValidation.AssertNotNull(nodesToUpdate, nameof(nodesToUpdate));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				nodesToUpdate.ForEach(nodeToUpdate => scope.Update(nodeToUpdate));
				scope.Done();
			}
		}

		/// <summary>
		/// Attaches the collection of nodes and their ascendants to the current persistence session.
		/// </summary>
		/// <param name="nodes">The nodes to attach with their ascendants.</param>
		/// <returns>The attached nodes.</returns>
		internal static IEnumerable<BrowserNode> AttachWithAscendants(this IEnumerable<BrowserNode> nodes)
		{
			using (var scope = new PersistenceScope(TransactionOption.Supported))
			{
				var attachedNodes = nodes.Union(nodes.SelectMany(node => node.Ascendants).Distinct());
				attachedNodes.ForEach(node => scope.Attach(node));
				scope.Done();
				return attachedNodes;
			}
		}

		/// <summary>
		/// Pre-fetches the descendant nodes (including any related target objects with registered pre-fetch strategies) of the given node.
		/// </summary>
		/// <param name="node">The node for which to pre-fetch descendants.</param>
		/// <param name="operationType">The type of node operation for which to pre-fetch.</param>
		public static void PreFetchNodeDescendants(this ParentNode node, OperationType operationType)
		{
			var stopwatch = new Stopwatch();
			stopwatch.Start();

			using (var scope = new PersistenceScope(TransactionOption.Supported))
			{
				// Get and execute the distinct set of registered pre-fetch strategies
				NodeType.RegisteredTypes.Select(nodeType => nodeType.Configuration.PreFetchStrategy)
					.Where(strategy => strategy != null)
					.Distinct()
					.ForEach(strategy => strategy.PreFetchTargetObjects(node, operationType));

				if (operationType != OperationType.CopyTo)
				{
					// Pre-fetch inbound links of descendants.  Not needed for CopyTo because inbound links aren't read during a copy operation.
					scope.Query<DefinitionNode>().Where(element => element.Ascendants.Contains(node))
						.FetchMany(element => element.InboundLinks)
						.ThenFetchMany(element => element.Ascendants)
						.ToFuture();
				}

				if (operationType == OperationType.MoveTo)
				{
					// Pre-fetch validation results.  Not needed for CopyTo because validation results of copied nodes don't change.
					// Not needed for Delete because nodes aren't validated when deleted.
					scope.Query<BrowserNode>().Where(element => element.Ascendants.Contains(node))
						.FetchMany(element => element.ValidationResults)
						.ThenFetch(element => element.SourceNode)
						.ToFuture();
				}
				
				// Descendants pre-fetch - parent folder and parent
				scope.Query<BrowserNode>().Where(element => element.Ascendants.Contains(node))
					.Fetch(element => element.ParentFolder)
					.ThenFetch(folder => folder.Parent)
					.ToFuture();

				// Descendants pre-fetch - child folders/nodes
				scope.Query<ParentNode>().Where(element => element.Ascendants.Contains(node))
					.FetchMany(element => element.NonEmptyNonInverseFolders)
					.ThenFetchMany(folder => folder.Nodes)
					.ToFuture();

				// Descendants pre-fetch - ascendants of descendants
				scope.Query<BrowserNode>().Where(element => element.Ascendants.Contains(node))
					.FetchMany(element => element.Ascendants)
					.ToFuture();

				// Descendants pre-fetch - decendants of descendants
				scope.Query<ParentNode>().Where(element => element.Ascendants.Contains(node))
					.FetchMany(element => element.Descendants)
					.ToFuture().ToList();

				scope.Done();
			}

			stopwatch.Stop();
			Console.WriteLine("Node descendant pre-fetch took {0} seconds.", stopwatch.Elapsed.TotalSeconds);
		}

		/// <summary>
		/// Copies the collection of nodes to a folder and saves the copies to the database.
		/// </summary>
		/// <param name="nodesToCopy">The nodes to copy and save.</param>
		/// <param name="destinationFolder">The destination folder in which to place the copies.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		/// <returns>A read only dictionary of the copied nodes, indexed by the original nodes.</returns>
		public static IReadOnlyDictionary<BrowserNode, BrowserNode> CopyAndPersistTo(this IEnumerable<BrowserNode> nodesToCopy, 
			BrowserFolder destinationFolder, bool prefetch)
		{
			nodesToCopy = ArgumentValidation.AssertNotNull(nodesToCopy, nameof(nodesToCopy));
			destinationFolder = ArgumentValidation.AssertNotNull(destinationFolder, nameof(destinationFolder));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Pre-fetch the descendants of the nodes being copied
				if (prefetch)
				{
					nodesToCopy.OfType<DefinitionNode>().ForEach(node => node.PreFetchNodeDescendants(OperationType.CopyTo));
				}
				
				// Attach the destination and its ascendants because their collections will change
				destinationFolder.Parent.AttachWithAscendants();

				// Execute the copy
				var copies = nodesToCopy.CopyTo(destinationFolder);

				// Save all copied nodes
				copies.Values.Save();
				scope.Done();

				return copies;
			}
		}

		/// <summary>
		/// Copies the collection of nodes to a folder and saves the copies to the database.
		/// </summary>
		/// <param name="nodesToCopy">The nodes to copy and save.</param>
		/// <param name="destinationNode">The destination node to which to copy the nodes.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		/// <returns>A read only dictionary of the copied nodes, indexed by the original nodes.</returns>
		public static IReadOnlyDictionary<BrowserNode, BrowserNode> CopyAndPersistTo(this IEnumerable<BrowserNode> nodesToCopy,
			ParentNode destinationNode, bool prefetch)
		{
			nodesToCopy = ArgumentValidation.AssertNotNull(nodesToCopy, nameof(nodesToCopy));
			destinationNode = ArgumentValidation.AssertNotNull(destinationNode, nameof(destinationNode));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Pre-fetch the descendants of the nodes being copied
				if (prefetch)
				{
					nodesToCopy.OfType<DefinitionNode>().ForEach(node => node.PreFetchNodeDescendants(OperationType.CopyTo));
				}
				
				// Attach the destination and its ascendants because their collections will change
				destinationNode.AttachWithAscendants();

				// Execute the copy
				var copies = nodesToCopy.CopyTo(destinationNode);

				// Save all copied nodes
				copies.Values.Save();
				scope.Done();
				return copies;
			}
		}

		/// <summary>
		/// Deletes the collection of nodes and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToDelete">The nodes to delete.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static void DeleteAndPersist(this IEnumerable<BrowserNode> nodesToDelete, bool prefetch)
		{
			DeleteAndPersist(nodesToDelete, false, prefetch);
		}

		/// <summary>
		/// Deletes the collection of nodes and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToDelete">The nodes to delete.</param>
		/// <param name="deletePermanently"><c>true</c> to delete the nodes permanently or <c>false</c> to move the nodes to the recycle bin.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static void DeleteAndPersist(this IEnumerable<BrowserNode> nodesToDelete, bool deletePermanently, bool prefetch)
		{
			nodesToDelete = ArgumentValidation.AssertNotNull(nodesToDelete, nameof(nodesToDelete));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Pre-fetch the descendants
				if (prefetch)
				{
					nodesToDelete.OfType<DefinitionNode>().ForEach(node => node.PreFetchNodeDescendants(OperationType.Delete));
				}
				
				// Attach the ascendants because their collections will change
				nodesToDelete.AttachWithAscendants();

				using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
				{
					nodesToDelete.Delete(deletePermanently);

					// Delete the permanently deleted nodes
					listener.PermanentlyDeletedNodes.ForEach(node => scope.Delete(node));
					scope.Done();
				}
			}
		}

		/// <summary>
		/// Adds the collection of definition nodes to a folder as a link.
		/// </summary>
		/// <param name="nodesToAdd">The definition nodes to add to the destination folder.</param>
		/// <param name="destinationFolder">The folder into which to add the definition nodes (as links).</param>
		/// <returns>The new link nodes, indexed by the added definition nodes.</returns>
		public static IReadOnlyDictionary<DefinitionNode, LinkNode> AddAndPersistTo(this IEnumerable<DefinitionNode> nodesToAdd, 
			BrowserFolder destinationFolder)
		{
			nodesToAdd = ArgumentValidation.AssertNotNull(nodesToAdd, nameof(nodesToAdd));
			destinationFolder = ArgumentValidation.AssertNotNull(destinationFolder, nameof(destinationFolder));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Attach the destination node and its ascendants because their collections will change
				destinationFolder.Parent.AttachWithAscendants();

				var linkNodes = nodesToAdd.AddTo(destinationFolder);
				linkNodes.Values.Save();
				scope.Done();
				return linkNodes;
			}
		}

		/// <summary>
		/// Adds the collection of definition nodes to a parent node as a link.
		/// </summary>
		/// <param name="nodesToAdd">The definition nodes to add to the destination node.</param>
		/// <param name="destinationNode">The parent node into which to add the definition nodes (as links).</param>
		/// <returns>The new link nodes, indexed by the added definition nodes.</returns>
		public static IReadOnlyDictionary<DefinitionNode, LinkNode> AddAndPersistTo(this IEnumerable<DefinitionNode> nodesToAdd, 
			DefinitionNode destinationNode)
		{
			nodesToAdd = ArgumentValidation.AssertNotNull(nodesToAdd, nameof(nodesToAdd));
			destinationNode = ArgumentValidation.AssertNotNull(destinationNode, nameof(destinationNode));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Attach the destination node and its ascendants because their collections will change
				destinationNode.AttachWithAscendants();

				var linkNodes = nodesToAdd.AddTo(destinationNode);
				linkNodes.Values.Save();
				scope.Done();
				return linkNodes;
			}
		}

		/// <summary>
		/// Moves the collection of nodes to the given destination folder and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToMove">The nodes to move.</param>
		/// <param name="destinationFolder">The folder to which to move the nodes.</param>
		public static void MoveAndPersistTo(this IEnumerable<BrowserNode> nodesToMove, BrowserFolder destinationFolder)
		{
			nodesToMove = ArgumentValidation.AssertNotNull(nodesToMove, nameof(nodesToMove));
			destinationFolder = ArgumentValidation.AssertNotNull(destinationFolder, nameof(destinationFolder));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Attach the ascendants of the nodes to be moved and the destination because their collections will change
				nodesToMove.Union(new[] { destinationFolder.Parent }).AttachWithAscendants();

				// Execute the move
				nodesToMove.OfType<DefinitionNode>().ForEach(node => node.PreFetchNodeDescendants(OperationType.MoveTo));
				using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
				{
					nodesToMove.MoveTo(destinationFolder);
					listener.PersistChanges();
					scope.Done();
				}
			}
		}

		/// <summary>
		/// Moves the collection of nodes to the given destination node and reflects the changes to the database.
		/// </summary>
		/// <param name="nodesToMove">The nodes to move.</param>
		/// <param name="destinationNode">The node to which to move the nodes.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static void MoveAndPersistTo(this IEnumerable<BrowserNode> nodesToMove, ParentNode destinationNode, bool prefetch)
		{
			nodesToMove = ArgumentValidation.AssertNotNull(nodesToMove, nameof(nodesToMove));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Attach the ascendants of the nodes to be moved and the destination because their collections will change
				nodesToMove.Union(new[] { destinationNode }).AttachWithAscendants();

				// Execute the move
				if (prefetch)
				{
					nodesToMove.OfType<DefinitionNode>().ForEach(node => node.PreFetchNodeDescendants(OperationType.MoveTo));
				}

				using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
				{
					nodesToMove.MoveTo(destinationNode);
					listener.PersistChanges();
					scope.Done();
				}
			}
		}
	}
}