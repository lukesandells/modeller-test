﻿using System.Linq;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;
using MI.Framework.Validation;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	/// <summary>
	/// Persistence extension methods for the <see cref="NodeTransaction"/> class.
	/// </summary>
	public static class NodeTransactionExtensions
	{
		/// <summary>
		/// Rolls back the transaction and persists the changes to the database.
		/// </summary>
		/// <param name="transaction">The transaction to roll back.</param>
		/// <returns>The rollback transaction.</returns>
		public static NodeTransaction RollBackAndPersist(this NodeTransaction transaction)
		{
			transaction = ArgumentValidation.AssertNotNull(transaction, nameof(transaction));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				transaction.Attach();
				using (var listener = NodeActivityLog.AddListener<NodeChangesListener>())
				{
					var rollbackTransaction = transaction.RollBack();

					// Delete the permanently deleted nodes
					listener.PermanentlyDeletedNodes.ForEach(node => scope.Delete(node));

					// Save newly created nodes
					listener.AddedNodes.ForEach(node => scope.Save(node));

					scope.Done();
					return rollbackTransaction;
				}
			}
		}

		/// <summary>
		/// Attaches the transaction to the current persistence scope.
		/// </summary>
		/// <param name="transaction">The transaction to attach.</param>
		private static void Attach(this NodeTransaction transaction)
		{
			var nodesToAttach = new HashSet<BrowserNode>();
			foreach (var logEntry in transaction.LogEntries)
			{
				if (logEntry is NewNodeTransactionLogEntry newNodeEntry)
				{
					nodesToAttach.Add(newNodeEntry.NewNode);
				}
				else if (logEntry is DeleteNodeTransactionLogEntry deletedNodeEntry)
				{
					nodesToAttach.Add(deletedNodeEntry.DeletedNode);
					nodesToAttach.Add(deletedNodeEntry.DeletedFromParentNode);
				}
				else if (logEntry is MoveNodeTransactionLogEntry movedNodeEntry)
				{
					nodesToAttach.Add(movedNodeEntry.MovedNode);
					nodesToAttach.Add(movedNodeEntry.SourceNode);
				}
				else if (logEntry is SetNodePropertyTransactionLogEntry nodePropertySetEntry)
				{
					nodesToAttach.Add(nodePropertySetEntry.Node);
				}
			}

			using (var scope = new PersistenceScope(TransactionOption.Supported))
			{
				nodesToAttach.AttachWithAscendants();
				nodesToAttach.Union(nodesToAttach.SelectMany(node => node.DescendantsOrderedByDepth)).ForEach(node => scope.Attach(node));
				scope.Done();
			}
		}
	}
}
