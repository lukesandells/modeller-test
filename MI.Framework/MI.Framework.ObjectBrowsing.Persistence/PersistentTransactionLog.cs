﻿using System.Linq;
using System.Collections.Generic;
using MI.Framework.ObjectBrowsing.Transactions;
using MI.Framework.Persistence;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class PersistentTransactionLog : ITransactionLog
	{
		public virtual IQueryable<NodeTransaction> Transactions => _transactions.AsQueryable();
		private IList<NodeTransaction> _transactions = new List<NodeTransaction>();

		private PersistentTransactionLog()
		{
		}

		public static PersistentTransactionLog Load()
		{
			using (var scope = new PersistenceScope(TransactionOption.Supported))
			{
				var log = new PersistentTransactionLog()
				{
					_transactions = scope.Query<NodeTransaction>().OrderBy(transaction => transaction.TransactionNumber).ToList()
				};

				scope.Done();
				return log;
			}
		}

		public void Add(NodeTransaction transaction)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				scope.Save(transaction);
				_transactions.Add(transaction);
				scope.Done();
			}
		}

		public void ClearFinalised()
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				foreach (var transaction in Transactions.Where(transaction => transaction.IsFinalised).ToList())
				{
					scope.Delete(transaction);
					_transactions.Remove(transaction);
				}

				scope.Done();
			}
		}
	}
}
