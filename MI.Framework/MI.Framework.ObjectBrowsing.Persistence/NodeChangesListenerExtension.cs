﻿using MI.Framework.Persistence;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public static class NodeChangesListenerExtension
	{
		/// <summary>
		/// Persists the changes this listener has picked up 
		/// </summary>
		/// <param name="listener"></param>
		public static void PersistChanges(this NodeChangesListener listener)
		{
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				// Delete the permanently deleted nodes
				listener.PermanentlyDeletedNodes.ForEach(node => scope.Delete(node));

				// Save newly created property link nodes
				listener.AddedNodes.ForEach(node => scope.Persist(node, PersistBehaviour.SaveIfNotInferable));
				scope.Done();
			}
		}
	}
}
