﻿using NHibernate.Mapping.ByCode;
using NHibernate.Mapping.ByCode.Conformist;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public class DefinitionNodeMapping : SubclassMapping<DefinitionNode>
	{
		public DefinitionNodeMapping()
		{
			Extends(typeof(ParentNode));
			DiscriminatorValue(null);

			Set(o => o.InboundLinks, set =>
			{
				set.Key(k => k.Column("LinkedDefinitionNodeId"));
				set.Access(Accessor.NoSetter);
				set.Fetch(CollectionFetchMode.Select);
				set.Cascade(Cascade.None);
				set.Inverse(true);
			}, rel => rel.OneToMany());
		}
	}
}
