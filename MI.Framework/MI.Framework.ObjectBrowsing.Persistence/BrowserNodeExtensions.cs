﻿using System.Collections.Generic;
using MI.Framework.Persistence;
using MI.Framework.Validation;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	/// <summary>
	/// Persistence extension methods for the <see cref="BrowserNode"/> class.
	/// </summary>
	public static class BrowserNodeExtensions
	{
		/// <summary>
		/// Saves a transient node and all its descendant transient nodes to the database.
		/// </summary>
		/// <param name="node">The node to save.</param>
		public static void Save(this BrowserNode node)
		{
			new[] { node }.Save();
		}

		/// <summary>
		/// Updates the node in the database.
		/// </summary>
		/// <param name="node">The node to update.</param>
		public static void Update(this BrowserNode node)
		{
			new[] { node }.Update();
		}

		/// <summary>
		/// Attaches the node and its ascendants ascendants to the current persistence session.
		/// </summary>
		/// <param name="node">The node to attach with its ascendants.</param>
		/// <returns>The attached nodes.</returns>
		internal static IEnumerable<BrowserNode> AttachWithAscendants(this BrowserNode node)
		{
			return new[] { node }.AttachWithAscendants();
		}

		/// <summary>
		/// Adds a new node to the node and saves the new node to the database.
		/// </summary>
		/// <param name="node">The node to which to add the new node.</param>
		/// <param name="nodeType">The type of node to add.</param>
		/// <param name="initialisationParameters">The initialisation parameters for the new node.</param>
		public static DefinitionNode AddNewAndPersist(this ParentNode node, NodeType nodeType, object[] initialisationParameters = null)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				node.AttachWithAscendants();
				var addedNode = node.AddNew(nodeType, initialisationParameters);
				addedNode.Save();

				scope.Done();
				return addedNode;
			}
		}

		/// <summary>
		/// Adds a new node to the node and saves the new node to the database.
		/// </summary>
		/// <param name="node">The node to which to add the new node.</param>
		/// <param name="nodeType">The type of node to add.</param>
		/// <param name="displayId">The display ID for the new node.</param>
		/// <param name="initialisationParameters">The initialisation parameters for the new node.</param>
		public static DefinitionNode AddNewAndPersist(this ParentNode node, NodeType nodeType, string displayId, 
			object[] initialisationParameters = null)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				node.AttachWithAscendants();
				var addedNode = node.AddNew(nodeType, displayId, initialisationParameters);
				addedNode.Save();

				scope.Done();
				return addedNode;
			}
		}

		/// <summary>
		/// Adds a new node to the node and saves the new node to the database.
		/// </summary>
		/// <param name="node">The node to which to add the new node.</param>
		/// <param name="nodeType">The type of node to add.</param>
		/// <param name="folder">The folder into which to place the new node.</param>
		/// <param name="initialisationParameters">The initialisation parameters for the new node.</param>
		public static DefinitionNode AddNewAndPersist(this ParentNode node, NodeType nodeType, BrowserFolder folder, 
			object[] initialisationParameters = null)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				node.AttachWithAscendants();
				var addedNode = node.AddNew(nodeType, folder, initialisationParameters);
				addedNode.Save();

				scope.Done();
				return addedNode;
			}
		}

		/// <summary>
		/// Adds a new node to the node and saves the new node to the database.
		/// </summary>
		/// <param name="node">The node to which to add the new node.</param>
		/// <param name="nodeType">The type of node to add.</param>
		/// <param name="folder">The folder into which to place the new node.</param>
		/// <param name="displayId">The display ID for the new node.</param>
		/// <param name="initialisationParameters">The initialisation parameters for the new node.</param>
		public static DefinitionNode AddNewAndPersist(this ParentNode node, NodeType nodeType, BrowserFolder folder, string displayId, 
			object[] initialisationParameters = null)
		{
			node = ArgumentValidation.AssertNotNull(node, nameof(node));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				node.AttachWithAscendants();
				var addedNode = node.AddNew(nodeType, folder, displayId, initialisationParameters);
				addedNode.Save();

				scope.Done();
				return addedNode;
			}
		}

		/// <summary>
		/// Copies the node to a folder and saves the copy to the database.
		/// </summary>
		/// <param name="nodeToCopy">The node to copy and save.</param>
		/// <param name="destinationFolder">The destination folder in which to place the copy.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		/// <returns>The copy of the given source node.</returns>
		public static BrowserNode CopyAndPersistTo(this BrowserNode nodeToCopy, BrowserFolder destinationFolder, bool prefetch)
		{
			return new[] { nodeToCopy }.CopyAndPersistTo(destinationFolder, prefetch)[nodeToCopy];
		}

		/// <summary>
		/// Copies the node to a destination node and saves the copy to the database.
		/// </summary>
		/// <param name="nodeToCopy">The node to copy and save.</param>
		/// <param name="destinationNode">The destination node in which to place the copy.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		/// <returns>The copy of the given source node.</returns>
		public static BrowserNode CopyAndPersistTo(this BrowserNode nodeToCopy, ParentNode destinationNode, bool prefetch)
		{
			return new[] { nodeToCopy }.CopyAndPersistTo(destinationNode, prefetch)[nodeToCopy];
		}

		/// <summary>
		/// Deletes a node and reflects the changes to the database.
		/// </summary>
		/// <param name="nodeToDelete">The node to delete.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static void DeleteAndPersist(this BrowserNode nodeToDelete, bool prefetch)
		{
			new[] { nodeToDelete }.DeleteAndPersist(prefetch);
		}

		/// <summary>
		/// Adds a definition node to a folder as a link.
		/// </summary>
		/// <param name="nodeToAdd">The node to add to the destination folder.</param>
		/// <param name="destinationFolder">The folder into which to add the link to the given source node.</param>
		/// <returns>The new link node, referencing the given source node from the destination folder.</returns>
		public static LinkNode AddAndPersistTo(this DefinitionNode nodeToAdd, BrowserFolder destinationFolder)
		{
			return new[] { nodeToAdd }.AddAndPersistTo(destinationFolder)[nodeToAdd];
		}

		/// <summary>
		/// Adds a definition node to another node as a link.
		/// </summary>
		/// <param name="nodeToAdd">The node to add to the destination node.</param>
		/// <param name="destinationNode">The node into which to add the link to the given source node.</param>
		/// <returns>The new link node, referencing the given source node from the destination node.</returns>
		public static LinkNode AddAndPersistTo(this DefinitionNode nodeToAdd, DefinitionNode destinationNode)
		{
			return new[] { nodeToAdd }.AddAndPersistTo(destinationNode)[nodeToAdd];
		}

		/// <summary>
		/// Moves the node to the given destination folder and reflects the changes to the database.
		/// </summary>
		/// <param name="nodeToMove">The node to move.</param>
		/// <param name="destinationFolder">The folder to which to move the node.</param>
		public static void MoveAndPersistTo(this BrowserNode nodeToMove, BrowserFolder destinationFolder)
		{
			new[] { nodeToMove }.MoveAndPersistTo(destinationFolder);
		}

		/// <summary>
		/// Moves the node to the given destination node and reflects the changes to the database.
		/// </summary>
		/// <param name="nodeToMove">The node to move.</param>
		/// <param name="destinationNode">The node to which to move the node.</param>
		/// <param name="prefetch">Whether or not to prefetch related nodes before executing</param>
		public static void MoveAndPersistTo(this BrowserNode nodeToMove, ParentNode destinationNode, bool prefetch)
		{
			new[] { nodeToMove }.MoveAndPersistTo(destinationNode, prefetch);
		}
	}
}
