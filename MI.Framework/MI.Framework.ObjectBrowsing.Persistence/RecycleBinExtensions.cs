﻿using MI.Framework.Persistence;
using MI.Framework.Validation;

namespace MI.Framework.ObjectBrowsing.Persistence
{
	public static class RecycleBinExtensions
	{
		public static void EmptyAndPersist(this RecycleBin recycleBin)
		{
			recycleBin = ArgumentValidation.AssertNotNull(recycleBin, nameof(recycleBin));
			using (var scope = new PersistenceScope(TransactionOption.Required))
			{
				scope.Attach(recycleBin.Parent);
				var contents = recycleBin.Nodes.ToHashSet();
				recycleBin.Empty();
				foreach (var node in contents)
				{
					scope.Delete(node);
				}

				scope.Done();
			}
		}
	}
}
