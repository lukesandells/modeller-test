﻿using System;
using System.Collections.Generic;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Resolves node type IDs and target object types to node types.
	/// </summary>
	public interface INodeTypeResolver
	{
		/// <summary>
		/// Gets the node type with the given node type ID.
		/// </summary>
		/// <param name="nodeTypeId">The node type ID.</param>
		/// <returns>The node type with the given node type ID or <c>null</c> if not found.</returns>
		NodeType NodeTypeWithId(Guid nodeTypeId);

		/// <summary>
		/// Gets the node type for the given the given target object type.
		/// </summary>
		/// <param name="targetObjectType">The type of target object.</param>
		/// <returns>The node type with the given target object type or <c>null</c> if not found.</returns>
		NodeType NodeTypeFor(Type targetObjectType);
	}
}
