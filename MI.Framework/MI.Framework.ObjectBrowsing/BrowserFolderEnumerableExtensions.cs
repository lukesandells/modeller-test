﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Framework.ObjectBrowsing
{
	/// <summary>
	/// Extension methods for collections of browser folders.
	/// </summary>
	public static class BrowserFolderEnumerableExtensions
	{
		/// <summary>
		/// Filters the collection of browser folders to return only those folders that are of the given type.
		/// </summary>
		/// <typeparam name="TBrowserFolder">The type of folder to return.</typeparam>
		/// <param name="folders">The collection of folders to filter.</param>
		/// <returns>The collection of browser folders that are of the given type.</returns>
		/// <remarks>
		/// This method exists because the <see cref="BrowserFolder"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, a member of a folder
		/// collection may in fact be a proxy to a browser folder object, where the proxy derives directly from the <see cref="BrowserFolder"/> 
		/// class, irrespective of whether the proxied object is a subclass of the browser folder class.  Therefore, the standard
		/// LINQ <see cref="Enumerable.OfType{TResult}(System.Collections.IEnumerable)"/> extension method is not guaranteed to 
		/// always find the all folders of the given type because some of the folders in the collection may be proxies.  This extension
		/// method provides a replacement implementation of the <see cref="Enumerable.OfType{TResult}(System.Collections.IEnumerable)"/>
		/// method, which correctly evaluates the type of a proxied browser folder object, which may be found in a collection of 
		/// browser folders.
		/// </remarks>
		public static IEnumerable<TBrowserFolder> OfType<TBrowserFolder>(this IEnumerable<BrowserFolder> folders) where TBrowserFolder : BrowserFolder
		{
			return folders.Where(folder => folder.Is<TBrowserFolder>()).Cast<TBrowserFolder>();
		}

		/// <summary>
		/// Casts the elements of a collection of browser folders to the given type of browser folder.
		/// </summary>
		/// <typeparam name="TBrowserFolder">The type of browser folder to which to cast.</typeparam>
		/// <param name="folders">The collection of folders to cast to the given type.</param>
		/// <returns>A collection of folders with elements from the given collection cast to the given type of folder.</returns>
		/// This method exists because the <see cref="BrowserFolder"/> class has derived classes and may participate in many-to-one
		/// polymorphic associations, in which it may be proxied by the persistence framework.  If this occurs, a member of a folder
		/// collection may in fact be a proxy to a browser folder object, where the proxy derives directly from the <see cref="BrowserFolder"/> 
		/// class, irrespective of whether the proxied object is a subclass of the browser folder class.  Therefore, the standard
		/// LINQ <see cref="Enumerable.Cast{TResult}(System.Collections.IEnumerable)"/> extension method is not guaranteed to 
		/// always successfully cast the elements of the collection to the given type because some of the folders in the collection may 
		/// be proxies.  This extension method provides a replacement implementation of the 
		/// <see cref="Enumerable.Cast{TResult}(System.Collections.IEnumerable)"/> method, which is able to bypass the proxy of 
		/// a proxied browser folder object, which may be found in a collection of browser folders, in order to be able to return the 
		/// collection of browser folders cast to the given type.
		/// </remarks>
		public static IEnumerable<TBrowserFolder> Cast<TBrowserFolder>(this IEnumerable<BrowserFolder> folders) where TBrowserFolder : BrowserFolder
		{
			return folders.Select(folder => folder.Cast<TBrowserFolder>());
		}
	}
}
