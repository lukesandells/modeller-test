﻿using System;
using System.Collections.Generic;

namespace MI.Framework.ObjectBrowsing.Transactions
{
	/// <summary>
	/// Listens to node transaction events as they occur.
	/// </summary>
	/// <remarks>
	/// Node transaction listeners derive from the <see cref="NodeTransactionListener"/> class and are registered with 
	/// the <see cref="NodeTransaction.AddListener{TListener}"/> method.
	/// </remarks>
	public abstract class NodeTransactionListener : IDisposable
	{
		/// <summary>
		/// Invoked when a new node transaction has begun.
		/// </summary>
		/// <param name="transaction">The new transaction.</param>
		public virtual void TransactionBegun(NodeTransaction transaction)
		{
		}

		/// <summary>
		/// Invoked when a node transaction is committed.
		/// </summary>
		/// <param name="transaction">The committed transaction.</param>
		public virtual void TransactionCommitted(NodeTransaction transaction)
		{
		}

		/// <summary>
		/// Invoked when a node transaction is completed.
		/// </summary>
		/// <param name="transaction">The completed transaction.</param>
		public virtual void TransactionCompleted(NodeTransaction transaction)
		{
		}
		
		/// <summary>
		/// Invoked when a node transaction is rolled back.
		/// </summary>
		/// <param name="transaction">The rolled back transaction.</param>
		public virtual void TransactionRolledBack(NodeTransaction transaction)
		{
		}

		/// <summary>
		/// Invoked when all outstanding uncommitted transactions are committed.
		/// </summary>
		/// <param name="transactions">The committed transactions.</param>
		public virtual void UncommittedTransactionsCommitted(IEnumerable<NodeTransaction> transactions)
		{
		}
		
		/// <summary>
		/// Invoked when all outstanding uncommitted transactions are rolled back.
		/// </summary>
		/// <param name="transactions">The rolled back transactions.</param>
		public virtual void UncommittedTransactionsRolledBack(IEnumerable<NodeTransaction> transactions)
		{
		}
		
		/// <summary>
		/// Disposes of the listener, unregistering it such that it no longer receives any further notifications.
		/// </summary>
		/// <param name="disposing">A value of <c>true</c> if invoked via the <see cref="Dispose"/> method; otherwise <c>false</c>, meaning 
		/// invoked by the garbage collector.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				NodeTransaction.RemoveListener(this);
			}
		}

		/// <summary>
		/// Disposes of the listener, unregistering it such that it no longer receives any further notifications.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
		}
	}
}
