﻿namespace MI.Framework.ObjectBrowsing.Mapping
{
	/// <summary>
	/// A node mapping.
	/// </summary>
	public interface INodeMapping
	{
		/// <summary>
		/// Compiles the node mapping into a node type.
		/// </summary>
		/// <param name="passNumber">The compile pass number.</param>
		/// <param name="furtherPassNeeded">Stipulates whether a further compiler pass is needed.</param>
		/// <returns>The compiled node type.</returns>
		NodeType Compile(int passNumber, out bool furtherPassNeeded);
	}
}
