﻿using System;
using MI.Framework.ObjectBrowsing.Configuration;
using System.Collections.Generic;

namespace MI.Framework.ObjectBrowsing
{
	public class RecycleBin : BrowserFolder
	{
		protected RecycleBin()
		{
		}

		internal RecycleBin(ParentNode parent)
			: base(FolderConfiguration.RecycleBin.MemberId, parent)
		{
		}

		public virtual void PermanentlyDelete(BrowserNode node)
		{
			node.Delete(true);
		}

		public override bool CanAddNew(out IEnumerable<NodeType> permittedTypes)
		{
			// Cannot add new nodes to a link node because link nodes do not have folders or child nodes
			permittedTypes = new NodeType[] { };
			return false;
		}

		public override bool CanAddNew(NodeType nodeType, object[] initialisationParameters)
		{
			// Cannot add new nodes to a link node because link nodes do not have folders or child nodes
			return false;
		}

		public virtual void Empty()
		{
			Clear();
		}
	}
}
