﻿using System;

namespace MI.Framework.ObjectBrowsing
{
	public abstract class NodeActivityListener : IDisposable
	{
		/// <summary>
		/// Invoked when a new node is created.
		/// </summary>
		/// <param name="newNode">The newly created node.</param>
		public virtual void NodeCreated(BrowserNode newNode)
		{
		}

		/// <summary>
		/// Invoked when a node is deleted.
		/// </summary>
		/// <param name="deletedNode">The deleted node.</param>
		/// <param name="deletedFrom">The folder from which the node was deleted.</param>
		public virtual void NodeDeleted(BrowserNode deletedNode, BrowserFolder deletedFrom, bool permanentlyDeleted)
		{
		}

		/// <summary>
		/// Invoked when a node is moved between folders.
		/// </summary>
		/// <param name="movedNode">The moved node.</param>
		/// <param name="movedFrom">The folder from which the node was moved.</param>
		/// <param name="movedTo">The foler to which the node was moved.</param>
		public virtual void NodeMoved(BrowserNode movedNode, BrowserFolder movedFrom, BrowserFolder movedTo)
		{
		}

		/// <summary>
		/// Invoked when a property value is set on a definition node.
		/// </summary>
		/// <param name="node">The node that had its property value set.</param>
		/// <param name="memberId">The node type member ID of the set property.</param>
		/// <param name="formerValue">The former value of the property.</param>
		public virtual void NodePropertySet(DefinitionNode node, Guid memberId, object formerValue)
		{
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				NodeActivityLog.RemoveListener(this);
			}
		}

		public void Dispose()
		{
			Dispose(true);
		}

		/// <summary>
		/// Invoked when a new node is added to it's new parent.
		/// </summary>
		/// <param name="newNode">The newly added node.</param>
		public virtual void NodeAddedNew(BrowserNode newNode)
		{ }
	}
}
