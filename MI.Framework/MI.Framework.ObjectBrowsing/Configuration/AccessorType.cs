﻿namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// Describes the type of an object member accessor.
	/// </summary>
	/// <remarks>
	/// See the <see cref="Accessor"/> class.
	/// </remarks>
	public enum AccessorType
	{
		/// <summary>
		/// Member values are read and written directly against a field member.
		/// </summary>
		Field,

		/// <summary>
		/// Member values are read and written using a property member.
		/// </summary>
		Property,

		/// <summary>
		/// Member values are read using a property member and written directly to a field member.
		/// </summary>
		NoSetter,

		/// <summary>
		/// Member values are read-only using a read-only property member.
		/// </summary>
		ReadOnlyProperty
	}
}
