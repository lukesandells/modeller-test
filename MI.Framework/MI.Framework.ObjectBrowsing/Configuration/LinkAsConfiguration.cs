﻿using System;

namespace MI.Framework.ObjectBrowsing.Configuration
{
	/// <summary>
	/// Configuration for linking nodes of a particular type as a different type of node.
	/// </summary>
	internal class LinkAsConfiguration
	{
		/// <summary>
		/// Identifies the type of node that newly created link nodes of the type of node to which the configuration pertains shall be.
		/// </summary>
		internal Guid LinkNodeTypeId { get; set; }

		/// <summary>
		/// The type of node that newly created link nodes of the type of node to which the configuration pertains shall be.
		/// </summary>
		internal NodeType LinkNodeType => NodeType.WithId(LinkNodeTypeId);

		/// <summary>
		/// A function that returns the required target object for link nodes of the type of node to which the configuration pertains.
		/// </summary>
		internal Func<object, object> LinkTargetObjectFunc { get; set; }
	}
}
