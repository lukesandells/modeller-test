﻿using System;
using System.Collections.Generic;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validator for a node.
	/// </summary>
	public interface INodeValidator
	{
		/// <summary>
		/// The types of validation results produced by the validator.  Returned types must derive from the <see cref="NodeValidationResultType"/> class.
		/// </summary>
		IEnumerable<Type> ResultTypes { get; }

		/// <summary>
		/// The trigger condition for validating a node with the validator.
		/// </summary>
		INodeValidationTriggerCondition TriggerCondition { get; }

		/// <summary>
		/// Validates the given node, adding <see cref="NodeValidationResult"/> objects to whatever nodes are required to reflect the validation result.
		/// </summary>
		/// <param name="node">The node to be validated.</param>
		void ValidateNode(BrowserNode node);
	}
}
