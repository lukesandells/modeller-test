﻿using System.Collections.Generic;
using System.Linq;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// A trigger condition for node validation.
	/// </summary>
	public interface INodeValidationTriggerCondition
	{
		/// <summary>
		/// Returns the set of nodes that must be validated because they meet the validation trigger condition.
		/// </summary>
		/// <param name="triggerEvents">The set of events that may trigger node validation, indexed by event type then depth.</param>
		/// <returns>The set of nodes meeting the validation trigger condition.</returns>
		/// <remarks>
		/// The trigger event type is indexed using an <see cref="IReadOnlyDictionary{TKey, TValue}"/> while the depth is indexed using an
		/// <see cref="ILookup{TKey, TElement}"/> because there can only be a single depth lookup for each trigger event type, while there
		/// may be any number of trigger events for each depth.
		/// </remarks>
		IEnumerable<BrowserNode> Evaluate(IReadOnlyDictionary<NodeValidationTriggerEventType, ILookup<int, NodeValidationTriggerEvent>> triggerEvents);
	}
}
