﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Defines a set of conditions that may be validated.
	/// </summary>
	internal static class ValidatableCondition
	{
		/// <summary>
		/// Indicates that the display ID has been validated as available for a node.
		/// </summary>
		public const string DisplayIdAvailable = nameof(DisplayIdAvailable);
	}
}
