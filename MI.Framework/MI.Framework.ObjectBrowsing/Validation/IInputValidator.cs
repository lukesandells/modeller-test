﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validator for input of a property value.
	/// </summary>
	public interface IInputValidator
	{
		/// <summary>
		/// Validates a given input value.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input value to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input as an instance of the appropriate type; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		bool ValidateInput(NodeProperty property, object input, out object validated, out string displayMessage);
	}
}
