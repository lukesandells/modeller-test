﻿using System.Linq;

namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Validates an input display ID.
	/// </summary>
	/// <remarks>
	/// Display IDs do not permit spaces anywhere within the input text and must be available in the folder where the node resides.
	/// </remarks>
	public class DisplayIdInputValidator : IdentifierInputValidator
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="DisplayIdInputValidator"/> class.
		/// </summary>
		/// <param name="autoTrim">Stipulates whether leading and trailing spaces shall be automatically trimmed rather than deemed invalid.</param>
		public DisplayIdInputValidator(bool autoTrim)
			: base(autoTrim)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="DisplayIdInputValidator"/> class.
		/// </summary>
		/// <param name="autoTrim">Stipulates whether leading and trailing spaces shall be automatically trimmed rather than deemed invalid.</param>
		/// <param name="predicate">A custom input validation predicate.</param>
		public DisplayIdInputValidator(bool autoTrim, InputValidationPredicate<string> predicate)
			: base(autoTrim, predicate)
		{
		}

		/// <summary>
		/// Validates the given input string.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input string to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, string input, out string validated, out string displayMessage)
		{
			if (!base.ValidateInput(property, input, out validated, out displayMessage))
			{
				return false;
			}

			// Can expect the property only corresponds to a single node because display ID properties don't permit batch updates
			var node = property.ParentNodes.Single();
			var path = FolderPath.To(node.ParentFolder);
			if (!path.IsIdAvailable(node.Type, validated))
			{
				displayMessage = string.Format(ValidationMessage.DisplayIdNotAvailable, validated, node.Type.DisplayName, path.ToString());
				return false;
			}
			
			return true;
		}
	}
}
