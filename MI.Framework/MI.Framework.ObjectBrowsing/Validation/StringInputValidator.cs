﻿namespace MI.Framework.ObjectBrowsing.Validation
{
	/// <summary>
	/// Inputs an input string with a given predicate.
	/// </summary>
	public class StringInputValidator : InputValidator<string, string>
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="StringInputValidator"/> class.
		/// </summary>
		/// <param name="predicate">A custom input validation predicate.</param>
		public StringInputValidator(InputValidationPredicate<string> predicate)
			: base(predicate)
		{
		}

		/// <summary>
		/// Validates the given input string.
		/// </summary>
		/// <param name="property">The property to receive the input.</param>
		/// <param name="input">The input string to be validated.</param>
		/// <param name="validated">If the input is valid, the validated input; otherwise <c>null</c>.</param>
		/// <param name="displayMessage">If the input is invalid, a display message describing the reason.</param>
		/// <returns>The validated input cast to the appropriate type.</returns>
		protected override bool ValidateInput(NodeProperty property, string input, out string validated, out string displayMessage)
		{
			validated = input;
			displayMessage = null;
			return true;
		}
	}
}
