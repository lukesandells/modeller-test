﻿namespace MI.Framework.Validation
{
	/// <summary>
	/// A validated value.
	/// </summary>
	/// <typeparam name="T">The type of validated value.</typeparam>
	public interface IValidated<out T> : IValidatable<T>
	{
	}
}
