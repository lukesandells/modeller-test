﻿namespace MI.Framework.Validation
{
	/// <summary>
	/// A validatable value.
	/// </summary>
	/// <typeparam name="T">The type of validatable value.</typeparam>
	public interface IValidatable<out T>
	{
		/// <summary>
		/// Indicates whether the value is fully validated.
		/// </summary>
		/// <returns><c>true</c> if the value is fully validated; otherwise <c>false</c>.</returns>
		bool IsValidated();

		/// <summary>
		/// Indicates whether the given condition is validated for the value.
		/// </summary>
		/// <param name="condition">The condition to evaluate.</param>
		/// <returns><c>true</c> if the given condition is true for the value; otherwise <c>false</c>.</returns>
		bool IsValidated(string condition);

		/// <summary>
		/// Returns the value as validated.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <returns>The value as validated.</returns>
		IValidated<T> Validated();

		/// <summary>
		/// Returns the value as validated for a given condition.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <param name="condition">The validated condition.</param>
		/// <returns>The value as validated for the given condition.</returns>
		IValidatable<T> Validated(string condition);

		/// <summary>
		/// Returns the value as unvalidated.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <returns>The value as unvalidated.</returns>
		IValidatable<T> Unvalidated();

		/// <summary>
		/// Returns the value as unvalidated for a given condition.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <param name="condition">The previously validated condition that is no longer validated.</param>
		/// <returns>The value as unvalidated for the given condition.</returns>
		IValidatable<T> Unvalidated(string condition);

		/// <summary>
		/// The validatable value.
		/// </summary>
		T Value { get; }
	}
}
