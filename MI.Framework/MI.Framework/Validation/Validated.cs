﻿using System;
using System.Collections.Generic;

namespace MI.Framework.Validation
{
	/// <summary>
	/// A fully validated value.
	/// </summary>
	/// <typeparam name="T">The type of validated value.</typeparam>
	internal struct Validated<T> : IValidated<T>
	{
		/// <summary>
		/// The set of validated conditions.
		/// </summary>
		private readonly ISet<string> _validatedConditions;

		/// <summary>
		/// The value.
		/// </summary>
		public T Value => _value;
		private readonly T _value;

		/// <summary>
		/// Initialises a new instance of the <see cref="Validatable{T}"/> value type.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="validatedConditions">The set of pre-validated conditions.</param>
		internal Validated(T value, ISet<string> validatedConditions)
		{
			_value = value;
			_validatedConditions = validatedConditions;
		}

		/// <summary>
		/// Determines whether the value is fully validated.
		/// </summary>
		/// <returns><c>true</c> if the value is fully validated; otherwise <c>false</c>.</returns>
		bool IValidatable<T>.IsValidated()
		{
			return true;
		}

		/// <summary>
		/// Determines whether the value is validated for a specific condition.
		/// </summary>
		/// <param name="condition">The condition to evaluate.</param>
		/// <returns><c>true</c> if the value is validated for the given condition; otherwise <c>false</c>.</returns>
		public bool IsValidated(string condition)
		{
			return _validatedConditions.Contains(condition);
		}

		/// <summary>
		/// Returns the value as validated.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <returns>The value as validated.</returns>
		IValidated<T> IValidatable<T>.Validated()
		{
			throw new InvalidOperationException(ExceptionMessage.ValueAlreadyValidated);
		}

		/// <summary>
		/// Returns the value as validated for a given condition.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <param name="condition">The validated condition.</param>
		/// <returns>The value as validated for the given condition.</returns>
		IValidatable<T> IValidatable<T>.Validated(string condition)
		{
			throw new InvalidOperationException(ExceptionMessage.ValueAlreadyValidated);
		}

		/// <summary>
		/// Returns the value as unvalidated.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <returns>The value as unvalidated.</returns>
		public IValidatable<T> Unvalidated()
		{
			_validatedConditions.Clear();
			return new Validatable<T>(_value, _validatedConditions);
		}

		/// <summary>
		/// Returns the value as unvalidated for a given condition.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <param name="condition">The previously validated condition that is no longer validated.</param>
		/// <returns>The value as unvalidated for the given condition.</returns>
		public IValidatable<T> Unvalidated(string condition)
		{
			_validatedConditions.Remove(condition);
			return new Validatable<T>(_value, _validatedConditions);
		}
	}
}

