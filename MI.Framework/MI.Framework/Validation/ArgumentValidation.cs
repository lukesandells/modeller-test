using System;
using System.Collections.Generic;
using System.Linq;

namespace MI.Framework.Validation
{
    /// <summary>
    /// Argument validation helper class.
    /// </summary>
    public static class ArgumentValidation
    {
        /// <summary>
        /// Tests an <see cref="IComparable" /> argument to ensure it is greater than or equal to zero.
        /// </summary>
        /// <typeparam name="T">The type to be tested.</typeparam>
        /// <param name="value">The value to be tested.</param>
        /// <param name="name">The argument name.</param>
        /// <returns>The argument value.</returns>
        /// <exception cref="ArgumentOutOfRangeException">The argument is less than zero.</exception>
        public static T AssertGreaterThanOrEqualToZero<T>(T value, string name) where T : IComparable
        {
			// Ensure value is greater than or equal to zero
			if (value?.CompareTo(default(T)) < 0)
            {
                throw new ArgumentOutOfRangeException(name, value, ExceptionMessage.ArgumentMustBeGreaterThanEqualToZero);
            }

            return value;
        }

		/// <summary>
		/// Tests an <see cref="IComparable"/> argument to ensure it is greater than zero.
		/// </summary>
		/// <typeparam name="T">The type to be tested.</typeparam>
		/// <param name="value">The value to be tested.</param>
		/// <param name="name">The argument name.</param>
		/// <returns>The argument value.</returns>
		/// <exception cref="ArgumentOutOfRangeException">The argument is less than or equal to zero.</exception>
		public static T AssertGreaterThanZero<T>(T value, string name) where T : IComparable
        {
			// Ensure value is greater than or equal to zero
			if (value?.CompareTo(default(T)) <= 0)
            {
                throw new ArgumentOutOfRangeException(name, value, ExceptionMessage.ArgumentMustBeGreaterThanZero);
            }

            return value;
        }

		/// <summary>
		/// Asserts that the given <see cref="DateTime" /> parameter has no time component.
		/// </summary>
		/// <param name="value">The value to test.</param>
		/// <param name="name">The parameter name.</param>
		/// <returns>The given value to test.</returns>
		/// <exception cref="ArgumentException">The value has a time component.</exception>
		public static DateTime AssertNoTimeOfDay(DateTime value, string name)
        {
			if (value != value.Date)
            {
                throw new ArgumentException(ExceptionMessage.ArgumentHasTimeComponent, name);
            }

            return value;
        }

		/// <summary>
		/// Asserts that the given argument is not <c>null</c>.
		/// </summary>
		/// <param name="value">The value to test for <c>null</c>.</param>
		/// <param name="name">The name of the argument.</param>
		/// <returns>The original object <paramref name="value" /> is returned.</returns>
		/// <exception cref="ArgumentNullException">The value is <c>null</c>.</exception>
		public static T AssertNotNull<T>(T value, string name)
        {
			// Ensure value not null
			if (value == null)
            {
                throw new ArgumentNullException(name);
            }

            return value;
        }

		/// <summary>
        /// Asserts that the given argument is not <c>null</c> or an empty string.
        /// </summary>
        /// <param name="value">The <see cref="String"/> value to test.</param>
        /// <param name="name">The name of the argument.</param>
        /// <returns>The <paramref name="value"/> argument.</returns>
        /// <exception cref="ArgumentException">The value is <c>null</c> or empty.</exception>
        public static string AssertNotNullOrEmpty(string value, string name)
        {
			if (string.IsNullOrEmpty(value))
			{
				throw new ArgumentException(ExceptionMessage.ArgumentCannotBeNullOrEmpty, name);
			}

			return value;
		}

		/// <summary>
		/// Asserts that the given argument is not <c>null</c> or white space.
		/// </summary>
		/// <param name="value">The <see cref="String"/> value to test.</param>
		/// <param name="name">The name of the argument.</param>
		/// <returns>The <paramref name="value"/> argument.</returns>
		/// <exception cref="ArgumentException">The value is <c>null</c> or white space.</exception>
		public static string AssertNotNullOrWhiteSpace(string value, string name)
		{
			if (string.IsNullOrWhiteSpace(value))
			{
				throw new ArgumentException(ExceptionMessage.ArgumentCannotBeNullOrWhiteSpace, name);
			}

			return value;
		}

		/// <summary>
		/// Asserts that the given argument is a valid identifier (i.e. not null, empty or containing any white space).
		/// </summary>
		/// <param name="value">The value to evaluate.</param>
		/// <param name="name">The name of the argument.</param>
		/// <returns>The <paramref name="value"/> argument.</returns>
		/// <exception cref="ArgumentException">The value is not a valid identifier.</exception>
		public static string AssertValidIdentifier(string value, string name)
		{
			if (value == string.Empty || value.Any(Char.IsWhiteSpace))
			{
				throw new ArgumentException(ExceptionMessage.ArgumentNotValidIdentifier, name);
			}

			return value;
		}

		/// <summary>
		/// Asserts that the given validatable argument has been validated.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The value to evaluate.</param>
		/// <param name="name">The name of the argument.</param>
		/// <returns>The <paramref name="value"/> argument as validated.</returns>
		/// <exception cref="ArgumentException">The value has not been validated.</exception>
		public static IValidated<T> AssertValidated<T>(IValidatable<T> value, string name)
		{
			if (value is IValidated<T> validated)
			{
				return validated;
			}

			throw new ArgumentException(ExceptionMessage.ArgumentNotValidated, name);
		}

		/// <summary>
		/// Asserts that the given validatable argument has been validated.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The value to evaluate.</param>
		/// <param name="condition">The validation condition to assert.</param>
		/// <param name="name">The name of the argument.</param>
		/// <returns>The <paramref name="value"/> argument.</returns>
		/// <exception cref="ArgumentException">The value has not been validated.</exception>
		public static IValidatable<T> AssertValidated<T>(IValidatable<T> value, string condition, string name)
		{
			if (value.IsValidated(condition))
			{
				return value;
			}

			throw new ArgumentException(ExceptionMessage.ArgumentNotValidated, name);
		}
	}
}