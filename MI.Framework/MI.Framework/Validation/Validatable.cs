﻿using System.Collections.Generic;

namespace MI.Framework.Validation
{
	/// <summary>
	/// Provides methods for creating validated and unvalidated validatable values.
	/// </summary>
	public static class Validatable
	{
		/// <summary>
		/// Returns the value as validated.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validated value.</param>
		/// <returns>The value as validated.</returns>
		public static IValidated<T> Validated<T>(T value)
		{
			return new Validated<T>(value, new HashSet<string>());
		}

		/// <summary>
		/// Returns the value as validated.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validated value.</param>
		/// <param name="condition">The validated condition.</param>
		/// <returns>The value as validated for the given condition.</returns>
		public static IValidated<T> Validated<T>(T value, string condition)
		{
			return new Validated<T>(value, new[] { condition }.ToHashSet());
		}

		/// <summary>
		/// Returns the value as unvalidated.
		/// </summary>
		/// <typeparam name="T">The type of unvalidated value.</typeparam>
		/// <param name="value">The unvalidated value.</param>
		/// <returns>The value as unvalidated.</returns>
		public static IValidatable<T> Unvalidated<T>(T value)
		{
			return new Validatable<T>(value);
		}
	}
}
