﻿using System;
using System.Collections.Generic;

namespace MI.Framework.Validation
{
	/// <summary>
	/// A validatable value.
	/// </summary>
	/// <typeparam name="T">The type of validatable value.</typeparam>
	internal struct Validatable<T> : IValidatable<T>
	{
		/// <summary>
		/// The set of validated conditions.
		/// </summary>
		private readonly ISet<string> _validatedConditions;

		/// <summary>
		/// The value.
		/// </summary>
		public T Value => _value;
		private readonly T _value;

		/// <summary>
		/// Initialises a new instance of the <see cref="Validatable{T}"/> value type.
		/// </summary>
		/// <param name="value">The validatable value.</param>
		internal Validatable(T value)
			: this(value, new HashSet<string>())
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="Validatable{T}"/> value type.
		/// </summary>
		/// <param name="value">The validatable value.</param>
		/// <param name="validatedConditions">The set of pre-validated conditions.</param>
		internal Validatable(T value, ISet<string> validatedConditions)
		{
			_value = value;
			_validatedConditions = validatedConditions;
		}

		/// <summary>
		/// Determines whether the value is fully validated.
		/// </summary>
		/// <returns><c>true</c> if the value is fully validated; otherwise <c>false</c>.</returns>
		public bool IsValidated()
		{
			return false;
		}

		/// <summary>
		/// Determines whether the value is validated for a specific condition.
		/// </summary>
		/// <param name="condition">The condition to evaluate.</param>
		/// <returns><c>true</c> if the value is validated for the given condition; otherwise <c>false</c>.</returns>
		public bool IsValidated(string condition)
		{
			return _validatedConditions.Contains(condition);
		}

		/// <summary>
		/// Returns the value as validated.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <returns>The value as validated.</returns>
		public IValidated<T> Validated()
		{
			return new Validated<T>(_value, _validatedConditions);
		}

		/// <summary>
		/// Returns the value as validated for a given condition.
		/// </summary>
		/// <typeparam name="T">The type of validated value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <param name="condition">The validated condition.</param>
		/// <returns>The value as validated for the given condition.</returns>
		public IValidatable<T> Validated(string condition)
		{
			condition = ArgumentValidation.AssertNotNullOrWhiteSpace(condition, nameof(condition));
			if (!_validatedConditions.Add(condition))
			{
				throw new ArgumentException(string.Format(ExceptionMessage.ValueAlreadyValidatedForGivenCondition, condition), nameof(condition));
			}

			return this;
		}

		/// <summary>
		/// Returns the value as unvalidated.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <returns>The value as unvalidated.</returns>
		public IValidatable<T> Unvalidated()
		{
			_validatedConditions.Clear();
			return this;
		}

		/// <summary>
		/// Returns the value as unvalidated for a given condition.
		/// </summary>
		/// <typeparam name="T">The type of validatable value.</typeparam>
		/// <param name="value">The validatable value.</param>
		/// <param name="condition">The previously validated condition that is no longer validated.</param>
		/// <returns>The value as unvalidated for the given condition.</returns>
		public IValidatable<T> Unvalidated(string condition)
		{
			_validatedConditions.Remove(condition);
			return this;
		}
	}
}
