﻿using System;

namespace MI.Framework
{
    /// <summary>
    ///     Class Extensions.
    /// </summary>
    public static class ArrayExtensions
    {
        #region Members

        /// <summary>
        ///     Determines whether [is null or empty] [the specified array].
        /// </summary>
        /// <param name="array">The array.</param>
        /// <returns><c>true</c> if [is null or empty] [the specified array]; otherwise, <c>false</c>.</returns>
        public static bool IsNullOrEmpty(this Array array)
        {
            return (array == null) || (array.Length == 0);
        }

        #endregion
    }
}