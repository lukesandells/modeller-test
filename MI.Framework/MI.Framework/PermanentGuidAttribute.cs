﻿using System;

namespace MI.Framework
{
	/// <summary>
	/// Apply to anything to assign a permanent iD.
	/// </summary>
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	public class PermanentGuidAttribute : Attribute
	{
		/// <summary>
		/// Returns the permanent ID.
		/// </summary>
		public Guid Value => new Guid(_value);
		private string _value;

		/// <summary>
		/// Initialises a new instance of the <see cref="PermanentGuidAttribute"/> class.
		/// </summary>
		/// <param name="value">The permanent ID.</param>
		public PermanentGuidAttribute(string value)
		{
			_value = value;
		}
	}
}
