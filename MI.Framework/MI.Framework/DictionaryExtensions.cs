﻿using System.Collections.Generic;

using MI.Framework.Validation;

namespace MI.Framework
{
	/// <summary>
	/// Extension methods for <see cref="IDictionary{TKey, TValue}"/>.
	/// </summary>
	public static class DictionaryExtensions
	{
		/// <summary>
		/// Adds a collection of key/value pairs to the dictionary.
		/// </summary>
		/// <typeparam name="TKey">The type of key.</typeparam>
		/// <typeparam name="TValue">The type of value.</typeparam>
		/// <param name="dictionary">The dictionary to which to add the key/value pairs.</param>
		/// <param name="keyValuePairs">The key/value pairs to add to the dictionary.</param>
		public static void Add<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, IEnumerable<KeyValuePair<TKey, TValue>> keyValuePairs)
		{
			dictionary = ArgumentValidation.AssertNotNull(dictionary, nameof(dictionary));
			keyValuePairs = ArgumentValidation.AssertNotNull(keyValuePairs, nameof(keyValuePairs));

			keyValuePairs.ForEach(kvp => dictionary.Add(kvp));
		}
	}
}
