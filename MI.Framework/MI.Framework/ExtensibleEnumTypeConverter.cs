﻿using System;
using System.Reflection;
using MI.Framework.Validation;

namespace MI.Framework
{
	/// <summary>
	/// Converts objects to and from the <see cref="ExtensibleEnum{TEnum}"/> type.
	/// </summary>
	public class ExtensibleEnumTypeConverter : ITypeConverter
	{
		/// <summary>
		/// Attempts to convert the given object to the given type.
		/// </summary>
		/// <param name="obj">The object to convert.</param>
		/// <param name="conversionType">The type to which to convert the object.</param>
		/// <param name="result">The given object converted to the given type.</param>
		/// <returns><c>true</c> if the conversion was successful; otherwise <c>false</c>.</returns>
		public bool TryConvert(object obj, Type conversionType, out object result)
		{
			conversionType = ArgumentValidation.AssertNotNull(conversionType, nameof(conversionType));
			obj = ArgumentValidation.AssertNotNull(obj, nameof(obj));
			result = null;

			// Converting to an extensible enum?
			if (conversionType.IsGenericType && conversionType.GetGenericTypeDefinition() == typeof(ExtensibleEnum<>))
			{
				var stringValue = obj.GetType() == conversionType.GetGenericArguments()[0] ? obj.ToString() : obj as string;
				if (stringValue != null)
				{
					result = Activator.CreateInstance(conversionType, BindingFlags.NonPublic | BindingFlags.Instance,
						null, new object[] { stringValue }, null);
				}
				else
				{
					result = null;
				}

				return result != null;
			}

			// Converting from an extensible enum?
			if (obj.GetType().IsGenericType && obj.GetType().GetGenericTypeDefinition() == typeof(ExtensibleEnum<>))
			{
				if (conversionType == typeof(string))
				{
					result = obj.ToString();
				}

				if (conversionType == obj.GetType().GetGenericArguments()[0])
				{
					try
					{
						result = Enum.Parse(conversionType, obj.ToString());
					}
					catch (ArgumentException)
					{
					}
				}
			}

			return result != null;
		}
	}
}
