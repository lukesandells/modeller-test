﻿using System;

using MI.Framework.Validation;
using System.Linq;

namespace MI.Framework
{
	/// <summary>
	/// Extension methods for the <see cref="Type"/> class.
	/// </summary>
	public static class TypeExtensions
	{
		/// <summary>
		/// Gets the inheritance depth of the type.
		/// </summary>
		/// <param name="type">This type.</param>
		/// <returns>The inheritence depth of the given type.</returns>
		public static int GetInheritanceDepth(this Type type)
		{
			type = ArgumentValidation.AssertNotNull(type, nameof(type));

			int depth = 0;
			while (type.BaseType != null)
			{
				type = type.BaseType;
				depth++;
			}

			return depth;
		}
		
		/// <summary>
		/// Gets the first base type of a type that has the given generic type definition.
		/// </summary>
		/// <param name="type">The type for which to find the base type.</param>
		/// <param name="genericTypeDefinition">The generic type definition to find.</param>
		/// <returns>The first base type of a type that has the given generic type definition.</returns>
		public static Type GetBaseTypeOfGenericType(this Type type, Type genericTypeDefinition)
		{
			type = ArgumentValidation.AssertNotNull(type, nameof(type));
			genericTypeDefinition = ArgumentValidation.AssertNotNull(genericTypeDefinition, nameof(genericTypeDefinition));
			if (!genericTypeDefinition.IsGenericTypeDefinition)
			{
				throw new ArgumentException(ExceptionMessage.GenericTypeDefinitionParameterMustBeGenericTypeDefinition, nameof(genericTypeDefinition));
			}

			if (!type.IsGenericType)
			{
				throw new ArgumentException(ExceptionMessage.TypeParameterMustBeGeneric, nameof(type));
			}

			while (type.BaseType != null)
			{
				type = type.BaseType;
				if (type.GetGenericTypeDefinition() == genericTypeDefinition)
				{
					return type;
				}
			}

			throw new InvalidOperationException(string.Format(ExceptionMessage.TypeDoesNotDeriveFromTypeWithGivenGenericTypeDefinition,
				type.Name, genericTypeDefinition.Name));
		}

		/// <summary>
		/// Returns a <see cref="Boolean"/> stipulating whether the type derives from the given type.
		/// </summary>
		/// <param name="type">The derived type.</param>
		/// <param name="baseType">The base type.</param>
		/// <returns><c>true</c> if the type derives from the given base type; otherwise <c>false</c>.</returns>
		public static bool DerivesFrom(this Type type, Type baseType)
		{
			type = ArgumentValidation.AssertNotNull(type, nameof(type));
			baseType = ArgumentValidation.AssertNotNull(baseType, nameof(baseType));

			while (type.BaseType != null)
			{
				type = type.BaseType;
				if (type.IsGenericType && baseType.IsGenericTypeDefinition && type.GetGenericTypeDefinition() == baseType)
				{
					return true;
				}
					
				if (!baseType.IsGenericTypeDefinition && type == baseType)
				{
					return true;
				}
			}

			return false;
		}
		
		/// <summary>
		/// Returns the non-generic type name of the type.
		/// </summary>
		/// <param name="type">The type for which to get the non-generic type name.</param>
		/// <returns>The non-generic type name of the given type.</returns>
		public static string GetNonGenericTypeName(this Type type)
		{
			type = ArgumentValidation.AssertNotNull(type, nameof(type));
			if (type.IsGenericType)
			{
				return type.Name.Split('`')[0];
			}

			return type.Name;
		}

		/// <summary>
		/// Returns <c>true</c> if the type implements the given interface; otherwise <c>false</c>.
		/// </summary>
		/// <param name="type">The type to evaluate.</param>
		/// <param name="interfaceType">The type of interface to evaluate.</param>
		/// <returns><c>true</c> if the type implements the given interface; otherwise <c>false</c>.</returns>
		public static bool HasInterface(this Type type, Type interfaceType)
		{
			type = ArgumentValidation.AssertNotNull(type, nameof(type));
			interfaceType = ArgumentValidation.AssertNotNull(interfaceType, nameof(interfaceType));
			return type.GetInterfaces().Any(i => i.IsGenericType && interfaceType == i.GetGenericTypeDefinition() || i == interfaceType);
		}

		/// <summary>
		/// Gets all the interfaces implemented or inherited by the type that are instances of the given generic interface type definition.
		/// </summary>
		/// <param name="type">The type to evaluate.</param>
		/// <param name="genericInterfaceTypeDefinition">The generic interface type definition upon which the interfaces to be found are defined.</param>
		/// <returns>All the interfaces implemented or inherited by the type that are instances of the given generic interface type definition.</returns>
		public static Type[] GetInterfaces(this Type type, Type genericInterfaceTypeDefinition)
		{
			type = ArgumentValidation.AssertNotNull(type, nameof(type));
			genericInterfaceTypeDefinition = ArgumentValidation.AssertNotNull(genericInterfaceTypeDefinition, nameof(genericInterfaceTypeDefinition));
			if (!genericInterfaceTypeDefinition.IsGenericTypeDefinition)
			{
				throw new ArgumentException(ExceptionMessage.GenericTypeDefinitionParameterMustBeGenericTypeDefinition, nameof(genericInterfaceTypeDefinition));
			}

			return type.GetInterfaces().Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == genericInterfaceTypeDefinition).ToArray();
		}
	}
}
