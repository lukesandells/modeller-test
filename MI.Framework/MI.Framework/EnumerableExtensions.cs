﻿using System;
using System.Collections.Generic;
using System.Linq;

using MI.Framework.Validation;

namespace MI.Framework
{
	/// <summary>
	/// Extension methods for the <see cref="IEnumerable{T}"/> interface.
	/// </summary>
	public static class EnumerableExtensions
	{
		/// <summary>
		/// Returns a new <see cref="HashSet{T}"/> from the collection.
		/// </summary>
		/// <typeparam name="T">The type of elements in the set.</typeparam>
		/// <param name="source">The source elements.</param>
		/// <returns>A new hash set comprising the source elements.</returns>
		public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
		{
			return new HashSet<T>(source);
		}

		/// <summary>
		/// Returns a deep copy of the collection.
		/// </summary>
		/// <typeparam name="T">The type of element in the collection.</typeparam>
		/// <param name="source">The collection to copy.</param>
		/// <returns>A deep copy of the collection.</returns>
		public static IEnumerable<T> Copy<T>(this IEnumerable<ICopyable<T>> source) where T : class
		{
			source = ArgumentValidation.AssertNotNull(source, nameof(source));
			return source.Select(element => element.Copy());
		}

		/// <summary>
		/// Performs the given action for each element in the collection.
		/// </summary>
		/// <typeparam name="T">The type of element in the collection.</typeparam>
		/// <param name="enumerable">The collection containing the elements for which to perform the given action.</param>
		/// <param name="action">The action to perform for each element in the collection.</param>
		public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
		{
			enumerable = ArgumentValidation.AssertNotNull(enumerable, nameof(enumerable));
			action = ArgumentValidation.AssertNotNull(action, nameof(action));

			foreach (var element in enumerable)
			{
				action(element);
			}
		}

		/// <summary>
		/// Recursively selects a hierarchy of objects.
		/// </summary>
		/// <typeparam name="T">The type of object in the object hierarchy.</typeparam>
		/// <param name="enumerable">The collection from which to select.</param>
		/// <param name="selector">A transform function to apply to each element.</param>
		/// <returns>The selected hierarchy of objects.</returns>
		public static IEnumerable<T> SelectHierarchy<T>(this IEnumerable<T> enumerable, Func<T, IEnumerable<T>> selector)
		{
			enumerable = ArgumentValidation.AssertNotNull(enumerable, nameof(enumerable));
			foreach (var item in enumerable)
			{
				yield return item;
				foreach (var subItem in selector(item).SelectHierarchy(selector))
				{
					yield return subItem;
				}
			}
		}

		/// <summary>
		/// Returns the given enumerable, except the given excluded element.
		/// </summary>
		/// <typeparam name="T">The type of element in the enumerable.</typeparam>
		/// <param name="enumerable">The enumerable from which to exclude the given excluded element.</param>
		/// <param name="excludedElement">The excluded element.</param>
		/// <returns>The given enumerable, except the given excluded element.</returns>
		public static IEnumerable<T> Except<T>(this IEnumerable<T> enumerable, T excludedElement)
		{
			enumerable = ArgumentValidation.AssertNotNull(enumerable, nameof(enumerable));
			return enumerable.Except(new[] { excludedElement });
		}

		/// <summary>
		/// Returns the intersection between the set of sets.
		/// </summary>
		/// <typeparam name="T">The type of element in each set.</typeparam>
		/// <param name="sets">The enumerable of sets.</param>
		/// <returns>The intersection between the set of sets.</returns>
		public static IEnumerable<T> Intersection<T>(this IEnumerable<IEnumerable<T>> sets)
		{
			var set = sets.FirstOrDefault();
			sets.Except(set).ForEach(s => set = set.Intersect(s));
			return set;
		}
	}
}
