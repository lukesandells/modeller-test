using System;
using System.Runtime.Serialization;

namespace MI.Framework
{
    /// <summary>
    /// An exception that has occurred during the execution of the MI framework.
    /// </summary>
    [Serializable]
    public class FrameworkException : Exception
    {
        /// <summary>
        /// Initialises a new instance of the <see cref="FrameworkException" /> class with a relevant system supplied exception message.
        /// </summary>
        public FrameworkException() : base(ExceptionMessage.DefaultFrameworkExceptionMessage)
		{
		}

        /// <summary>
        ///     Initialises a new instance of the <see cref="FrameworkException" /> class with a specified error message.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        public FrameworkException(string message) : base(message)
		{
		}

        /// <summary>
        /// Initialises a new instance of the <see cref="FrameworkException" /> class with a specified error message
        /// and a reference to the inner exception that is the cause of this exception.
        /// </summary>
        /// <param name="message">The error message that explains the reason for the exception.</param>
        /// <param name="innerException">The exception that is the cause of the current exception.</param>
        public FrameworkException(string message, Exception innerException) : base(message, innerException)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="FrameworkException" /> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext" /> that contains contextual information about the source or destination.</param>
		protected FrameworkException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
    }
}