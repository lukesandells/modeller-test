﻿using System;

namespace MI.Framework
{
	public static class ExtensibleEnumExtensions
	{
		public static ExtensibleEnum<T> ToExtensibleEnum<T>(this Enum source) where T : struct
		{
			return ExtensibleEnum<T>.Parse(source.ToString());
		}
	}
}
