namespace MI.Framework.Contexts
{
	/// <summary>
	/// Used by the <see cref="Scope"/> class to create contexts when context scopes are constructed.
	/// Based on the Abstract Factory design pattern.
	/// </summary>
	public interface IContextFactory
	{
		/// <summary>
		/// Creates a new context.
		/// </summary>
		/// <param name="currentContext">The current context if one exists, otherwise Null.</param>
		/// <returns>A new context, enrolled in the current context if present.</returns>
		Context CreateContext( Context currentContext );

		/// <summary>
		/// Gets the <see cref="System.Type"/> indicating the type of <see cref="Context" /> object
		/// produced by the context factory.
		/// </summary>
		System.Type ContextType{ get; }
	}
}
