using System;
using System.Runtime.Remoting.Messaging;

namespace MI.Framework.Contexts
{
	/// <summary>
	/// A thread affinative context stack.  Instances of this class will be passed across AppDomain and network boundaries
	/// during remoting calls when placed in the call context.
	/// </summary>
	[ Serializable() ]
	internal class ThreadAffinativeContextStack : ContextStack, ILogicalThreadAffinative
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="ThreadAffinativeContextStack" /> class.
		/// </summary>
		/// <param name="contextType">The <see cref="System.Type" /> of <see cref="Context"/> derived object 
		/// that will be housed in the <see cref="ContextStack"/>.</param>
		internal ThreadAffinativeContextStack( System.Type contextType ) : base( contextType )
		{
		}
	}
}
