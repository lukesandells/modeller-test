using System;
using System.Runtime.Serialization;

namespace MI.Framework.Contexts
{
	/// <summary>
	/// The exception that is thrown when a <see cref="Scope" /> is enrolled in an existing 
	/// but incompatible <see cref="Scope" />.
	/// </summary>
	[ Serializable() ]
	public class ContextStackOutOfOrderException : FrameworkException
	{
		/// <summary>
		/// Initialises a new instance of the <see cref="ContextStackOutOfOrderException" /> class with a 
		/// relevant system supplied exception message.
		/// </summary>
		public ContextStackOutOfOrderException() : base(ExceptionMessage.DefaultContextStackOrder)
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ContextStackOutOfOrderException" /> class with a 
		/// specified error message.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		public ContextStackOutOfOrderException( string message ) : base( message )
		{
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ContextStackOutOfOrderException" /> class with a specified error message
		/// and a reference to the inner exception that is the cause of this exception.
		/// </summary>
		/// <param name="message">The error message that explains the reason for the exception.</param>
		/// <param name="innerException">The exception that is the cause of the current exception.</param>
		public ContextStackOutOfOrderException( string message, System.Exception innerException ) : base( message, innerException )
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ContextStackOutOfOrderException" /> class with serialized data.
		/// </summary>
		/// <param name="info">The <see cref="SerializationInfo"/> that holds the serialized object data about the 
		/// exception being thrown.</param>
		/// <param name="context">The <see cref="StreamingContext"/> that contains contextual information about 
		/// the source or destination.</param>
		protected ContextStackOutOfOrderException( SerializationInfo info, StreamingContext context ) : base( info, context )
		{
		}
	}
}
