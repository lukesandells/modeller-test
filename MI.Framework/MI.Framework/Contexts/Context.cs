using System;

namespace MI.Framework.Contexts
{
	/// <summary>
	/// Base class for all context objects.
	/// </summary>
	/// <remarks>
	/// <see cref="Context" /> objects represent thread local contextual information that travels 
	/// between method calls in an out-of-band fashion.  The lifetime of a <see cref="Context" /> 
	/// object is controlled by the lifetime of a corresponding <see cref="Scope" /> object.
	/// 
	/// Each time a <see cref="Scope" /> object creates a new <see cref="Context" />, it is placed 
	/// on the current thread's <see cref="ContextStack" />.  There is a <see cref="ContextStack "/>
	/// on each thread for each type of <see cref="Context" /> used.
	/// 
	/// When a <see cref="Context" /> object's corresponding <see cref="Scope"/> object is disposed,
	/// the <see cref="Context" /> is removed from the context stack and the next <see cref="Context" />
	/// object on the stack becomes the current context.
	/// </remarks>
	[ Serializable() ]
	public abstract class Context
	{
		/// <summary>
		/// A GUID uniquely identifying the context instance.  This is necessary to maintain context 
		/// identity across AppDomain boundaries.  Serialization and deserialization messes up object
		/// identity.
		/// </summary>
		private Guid _contextId = Guid.NewGuid();

		/// <summary>
		/// Gets the unique identifier for the <see cref="Context" /> instance.
		/// </summary>
		public Guid ContextId
		{
			get { return _contextId; }
		}

		/// <summary>
		/// Gets the current context of a given type.
		/// </summary>
		/// <typeparam name="T">The context type.</typeparam>
		/// <returns>The current context if present; otherwise <c>null</c>.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate" )]
		[System.Diagnostics.CodeAnalysis.SuppressMessage( "Microsoft.Design", "CA1004:GenericMethodsShouldProvideTypeParameter" )]
		public static T GetCurrent<T>() where T : Context
		{
			ContextStack stack = ContextStack.GetStack( typeof( T ) );
			if( stack != null )
			{
				if( stack.Count > 0 )
				{
					return stack.Peek() as T;
				}
			}

			return null;
		}

		/// <summary>
		/// Determines whether the specified <see cref="System.Object" /> is equal to the current <see cref="System.Object" />.
		/// </summary>
		/// <param name="obj">The <see cref="System.Object" /> to compare with the current <see cref="System.Object" /></param>
		/// <returns>A <see cref="System.Boolean" /> indicating wether the specified <see cref="System.Object" /> 
		/// is equal to the current <see cref="System.Object" /></returns>
		public override bool Equals( object obj )
		{
			Context candidate = obj as Context;

			// Is the object being compared a Context?
			if( ( object )candidate == null )
			{
				// No, then we can't be the same
				return false;
			}

			// Does the Context being compared have the same code?
			return this._contextId == candidate._contextId;
		}

		/// <summary>
		/// Tests the two <see cref="Context" /> instances for equality.
		/// </summary>
		/// <param name="left">The <see cref="Context" /> on the left hand side of the equality operator.</param>
		/// <param name="right">The <see cref="Context"/> on the right hand side of the equality operator.</param>
		/// <returns>A <see cref="System.Boolean" /> indicating whether the two specified <see cref="Context" /> 
		/// instances are equal.</returns>
		public static bool operator ==( Context left, Context right )
		{
			// Is the object on the left null?
			if( ( object )left == null )
			{
				// Yes, then we are only equal if the object on the right is Null too.
				return ( object )right == null;
			}
			else
			{
				// Object on the left is not null, so test for equality using Equals method.
				return left.Equals( right );
			}
		}

		/// <summary>
		/// Tests the two <see cref="Context" /> instances for inequality
		/// </summary>
		/// <param name="left">The <see cref="Context" /> on the left hand side of the equality operator.</param>
		/// <param name="right">The <see cref="Context"/> on the right hand side of the equality operator.</param>
		/// <returns>A <see cref="System.Boolean" /> indicating whether the two specified <see cref="Context" /> 
		/// instances are not equal.</returns>
		public static bool operator!=( Context left, Context right )
		{
			// Use the == operator overload to test for inequality
			return !( left == right);
		}

		/// <summary>
		/// Serves as a hash function for the <see cref="Context" /> instance, suitable for use in hashing algorithms
		/// and data structures like a hash table.
		/// </summary>
		/// <returns>A hash code based on the Context text.</returns>
		public override int GetHashCode()
		{
			return _contextId.GetHashCode();
		}
	}
}
