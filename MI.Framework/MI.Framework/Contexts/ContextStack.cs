using System;
using System.Collections;
using System.Security.Permissions;
using System.Runtime.Remoting.Messaging;
using System.Globalization;

namespace MI.Framework.Contexts
{
	/// <summary>
	/// Encapsulates a thread local Stack for the purpose of housing a stack of context executing on any given thread.
	/// </summary>
	[ Serializable() ]
	internal class ContextStack : IDisposable
	{
		/// <summary>
		/// Holds the stack of context objects
		/// </summary>
		private ArrayList _stack;

		/// <summary>
		/// The <see cref="System.Type" /> of <see cref="Context"/> object that will be housed in 
		/// the <see cref="ContextStack" />.
		/// </summary>
		private System.Type _contextType;

		/// <summary>
		/// Initialises a new instance of the <see cref="ContextStack" /> class and then places that new 
		/// instance in the <see cref="CallContext" /> for later use during the call.
		/// </summary>
		/// <param name="contextType">The <see cref="System.Type" /> of <see cref="Context"/> derived object 
		/// that will be housed in the <see cref="ContextStack"/>.</param>
		[ SecurityPermission( SecurityAction.Demand, Infrastructure = true ) ]
		internal ContextStack( System.Type contextType )
		{
			// Create the contained stack instance
			_stack = new ArrayList();

			// Save the call context key for use later
			_contextType = contextType;

			// Check if the specified context key is already in use
			if( CallContext.GetData( contextType.GUID.ToString() ) != null )
			{
				throw new FrameworkException( string.Format( 
                    CultureInfo.CurrentUICulture,
					ExceptionMessage.StackAlreadyExists, 
                    _contextType.GUID.ToString() 
                    ) );
			}

			// Save this instance into the call context
			CallContext.SetData( contextType.GUID.ToString(), this );
		}

		/// <summary>
		/// Loads the context stack with the given key from the call context
		/// </summary>
		/// <param name="contextType">The <see cref="System.Type" /> of <see cref="Context"/> derived object 
		/// that will be housed in the <see cref="ContextStack"/>.</param>
		/// <returns>The <see cref="ContextStack" /> for the provided type of <see cref="Context" /></returns>
		[ SecurityPermission( SecurityAction.Demand, Infrastructure = true ) ]
		public static ContextStack GetStack( System.Type contextType )
		{
			// Get the named context stack from the call context
			return ( ContextStack )CallContext.GetData( contextType.GUID.ToString() );
		}

		/// <summary>
		/// Pushes a new context object onto the context stack.
		/// </summary>
		/// <param name="context">The context object to be pushed onto the stack.  Anything pushed onto the stack
		/// must be marked Serializable if the contexts it holds are to be distributable across AppDomains or network
		/// boundaries.</param>
		public int Push( Context context )
		{
			_stack.Add( context );
			return _stack.Count - 1;
		}

		/// <summary>
		/// Pops the top context object off the context stack.
		/// </summary>
		/// <returns>The top context on the context stack.</returns>
		public Context Pop()
		{
			// Get the current context, remove it, then return it
			Context topContext = this.Peek();
			_stack.RemoveAt( _stack.Count - 1 );

			return topContext;
		}

		/// <summary>
		/// Retrieves a <see cref="Context" /> from the context stack, without removing it.
		/// </summary>
		/// <param name="index">The index of the <see cref="Context" /> in the stack to retrieve.</param>
		/// <returns>The <see cref="Context" /> at the specified index in the context stack if the stack is not empty.  
		/// Otherwise return <c>Null</c> (<c>Nothing</c> in Visual Basic).</returns>
		public Context Peek( int index )
		{
			// Anything on the stack?
			if( _stack.Count > 0 )
			{
				// Yep, peek the top context off the stack
				return ( Context )_stack[ index ];
			}
			else
			{
				// Nothing on the stack so return Null.
				return null;
			}
		}

		/// <summary>
		/// Retrieves the top <see cref="Context" /> from the context stack, without removing it.
		/// </summary>
		/// <returns>The top <see cref="Context" /> on the context stack.</returns>
		public Context Peek()
		{
			return Peek( _stack.Count - 1 );
		}

		/// <summary>
		/// Returns the number of items on the context stack.
		/// </summary>
		public int Count
		{
			get{ return _stack.Count; }
		}

		#region IDisposable Members

		/// <summary>
		/// Removes the context stack instance from the call context.
		/// </summary>
		[ SecurityPermission( SecurityAction.Demand, Flags = SecurityPermissionFlag.Infrastructure ) ]
		public void Dispose()
		{
			// Clear the call context slot for the stack
			CallContext.FreeNamedDataSlot( _contextType.GUID.ToString() );

			// Suppress finalize call
			GC.SuppressFinalize( this );
		}

		#endregion
	}
}
