using System;
using MI.Framework.Logging;

namespace MI.Framework.Contexts
{
	/// <summary>
	/// Demarks the boundaries of a <see cref="Context" />.
	/// </summary>
	/// <remarks>
	/// The point at which a <see cref="Scope" /> instance is created on a given thread is the point at which 
	/// the provided context attributes come into effect.  These attributes stay in effect until the 
	/// <see cref="Dispose()" /> method is called on the <see cref="Scope" /> instance.
	/// </remarks>
	public abstract class Scope : IDisposable
	{
		/// <summary>
		/// Log for code instrumentation
		/// </summary>
		private static readonly ILog _log = LogManager.GetLogger(typeof(Scope));

		/// <summary>
		/// The <see cref="System.Type" /> of <see cref="Context" /> object whose lifetime is demarked by the 
		/// current <see cref="Scope" /> instance.
		/// </summary>
		private Type _contextType;

		/// <summary>
		/// A GUID uniquely identifying the <see cref="Context" /> controlled by the current 
		/// <see cref="Scope" /> instance.
		/// </summary>
		private Guid _contextId;

		/// <summary>
		/// The index of the <see cref="Context" /> managed by the current <see cref="Scope" /> in the context stack.
		/// </summary>
		private int _stackIndex;

		/// <summary>
		/// Initialises an instance of the <see cref="Scope" /> class.
		/// </summary>
		/// <param name="factory">A context factory that will create appropriate contexts at the request of 
		/// the <see cref="Scope" /> instance.</param>
		protected Scope( IContextFactory factory )
		{
			// Ensure factory not null
			if( factory == null )
			{
				throw new ArgumentNullException( "factory" );
			}

			Context currentContext = null;
			Context newContext = null;
			
			// Get the type of context objects we're dealing with
			_contextType = factory.ContextType;
			if( _contextType == null )
			{
				throw new FrameworkException(ExceptionMessage.NullContextType);
			}

			// Get the relevant context stack
			ContextStack stack = ContextStack.GetStack( _contextType );

			// Was the context stack found in the call context?
			if( stack == null )
			{
				// No, then create a new context stack for persistence contexts and store it in the call context.
				if( _contextType.GetCustomAttributes( typeof( ThreadAffinativeAttribute ), true ).Length > 0 )
				{
					stack = new ThreadAffinativeContextStack( _contextType );
				}
				else
				{
					stack = new ContextStack( _contextType );
				}
			}
			else
			{
				// Yes - any items on it?
				if( stack.Count > 0 )
				{
					// Yep, so get the current context
					currentContext = stack.Peek();
				}
			}

			try
			{
				// Create the new context and enter it
				newContext = factory.CreateContext( currentContext );

				// Ensure the new context is of the right type
				if( newContext.GetType() != _contextType )
				{
					throw new FrameworkException(ExceptionMessage.ContextTypeMismatch);
				}

				// Remember the context ID
				_contextId = newContext.ContextId;

				// Push the new context onto the context stack
				_stackIndex = stack.Push( newContext );
			}
			catch( Exception e )
			{
				// Make a note of the exception
				_log.Error( "Error creating new context scope.", e);

				// Clean up the new context if we managed to create one and it is disposable
				if (newContext is IDisposable disposable)
				{
					// Yes, then dispose of it
					disposable.Dispose();
				}

				// Re-throw our exception
				throw;
			}
		}

		/// <summary>
		/// Retrieves the current context from the top of the context stack.  
		/// </summary>
		/// <remarks>
		/// This property actually retrievea the context stack directly from the call context each time the 
		/// property is accessed.  This is done because the slot may at any time, point to a new context stack 
		/// instance due to a remote call causing a new context stack to be deserialised from the remote AppDomain.
		/// </remarks>
		protected Context Context
		{
			get{ return ContextStack.GetStack( _contextType ).Peek( _stackIndex ); }
		}

		/// <summary>
		/// Cleans up resources held by the <see cref="Scope" /> instance.
		/// </summary>
		/// <param name="disposing"><c>True</c> if called while disposing.  Otherwise <c>False</c>.</param>
		protected virtual void Dispose( bool disposing )
		{
			// If we are not disposing, then don't bother doing anything - we don't have any unmanaged resources
			if( !disposing )
			{
				return;
			}

			// Get the context stack for the type of context we are managing
			ContextStack stack = ContextStack.GetStack( _contextType );

			// If there is no stack, or if the stack is empty, then there is nothing left to dispose.
			if( stack == null || stack.Count == 0 )
			{
				return;
			}

			// Pop the context off the stack
			Context context = stack.Pop();
			Guid contextId = context.ContextId;

			// Clean up the context
			if (context is IDisposable disposable)
			{
				disposable.Dispose();
			}

			// Check we got the right context
			if ( contextId != _contextId )
			{
				stack.Dispose();
				throw new ContextStackOutOfOrderException();
			}

			// If we are the root Scope, then clean up the context stack.
			if( stack.Count == 0 )
			{
				stack.Dispose();
			}
		}

		#region IDisposable Members

		/// <summary>
		/// Cleans up resources held by the <see cref="Scope" /> instance.
		/// </summary>
		public void Dispose()
		{
			Dispose( true );
			GC.SuppressFinalize( this );
		}

		#endregion
	}
}
