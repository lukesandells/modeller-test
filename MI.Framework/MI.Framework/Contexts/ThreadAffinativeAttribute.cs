using System;

namespace MI.Framework.Contexts
{
	/// <summary>
	/// Applied to a <see cref="Context" /> derived class, this attribute specifies that the 
	/// context be marshalled across AppDomain boundaries.
	/// </summary>
	[ AttributeUsage( AttributeTargets.Class, AllowMultiple = false, Inherited = true ) ]
	public sealed class ThreadAffinativeAttribute : Attribute
	{
	}
}
