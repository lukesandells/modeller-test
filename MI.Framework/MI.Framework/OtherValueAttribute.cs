﻿using System;

namespace MI.Framework
{
	/// <summary>
	/// Apply to a member of an <see cref="Enum"/> to mark it as the other value for that <see cref="Enum"/>.
	/// </summary>
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = false)]
	public class OtherValueAttribute : Attribute
	{
	}
}
