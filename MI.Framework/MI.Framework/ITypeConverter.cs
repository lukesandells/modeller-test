﻿using System;

namespace MI.Framework
{
	/// <summary>
	/// Converts an object to another type.
	/// </summary>
	public interface ITypeConverter
	{
		/// <summary>
		/// Attempts to convert the given object to the given type.
		/// </summary>
		/// <param name="obj">The object to convert.</param>
		/// <param name="conversionType">The type to which to convert the object.</param>
		/// <param name="result">The given object converted to the given type.</param>
		/// <returns><c>true</c> if the conversion was successful; otherwise <c>false</c>.</returns>
		bool TryConvert(object obj, Type conversionType, out object result);
	}
}
