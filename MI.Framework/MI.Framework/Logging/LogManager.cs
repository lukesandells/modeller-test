﻿using System;

namespace MI.Framework.Logging
{
	/// <summary>
	/// This class is used by client applications to request logger instances.
	/// </summary>
	public static class LogManager
	{
		/// <summary>
		/// The log provider.
		/// </summary>
		public static ILogProvider Provider { get; set; } = new NullLogProvider();

		/// <summary>
		/// Gets a logger for a given type.
		/// </summary>
		/// <param name="type">The type for which log entries shall be created.</param>
		/// <returns>A logger for the given type.</returns>
		public static ILog GetLogger(Type type)
		{
			return Provider.GetLogger(type);
		}
	}
}
