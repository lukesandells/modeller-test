﻿using System;

namespace MI.Framework.Logging
{
	/// <summary>
	/// A log provider that logs nothing.  This is the default log provider.
	/// </summary>
	public class NullLogProvider : ILogProvider
	{
		/// <summary>
		/// A logger that logs nothing.
		/// </summary>
		private class Log : ILog
		{
			/// <summary>
			/// Indicates whether debug is enabled on the logger.
			/// </summary>
			public bool IsDebugEnabled => false;

			/// <summary>
			/// Logs a formatted message string with the Debug level.
			/// </summary>
			/// <param name="format">The format string.</param>
			/// <param name="args">The arguments.</param>
			public void DebugFormat(string format, params object[] args)
			{
			}

			/// <summary>
			/// Logs a message with the Error level.
			/// </summary>
			/// <param name="error">The error text.</param>
			/// <param name="exception">The corresponding exception.</param>
			public void Error(string error, Exception exception)
			{
			}
		}

		/// <summary>
		/// Gets a new logger from the provider for a given type.
		/// </summary>
		/// <param name="type">The type for which log entries shall be created.</param>
		/// <returns>A new logger for the given type.</returns>
		public ILog GetLogger(Type type)
		{
			return new Log();
		}
	}
}
