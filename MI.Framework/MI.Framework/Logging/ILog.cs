﻿using System;

namespace MI.Framework.Logging
{
	/// <summary>
	/// A logger.
	/// </summary>
	public interface ILog
	{
		/// <summary>
		/// Logs a message with the Error level.
		/// </summary>
		/// <param name="error">The error text.</param>
		/// <param name="exception">The corresponding exception.</param>
		void Error(string error, Exception exception);

		/// <summary>
		/// Indicates whether debug is enabled on the logger.
		/// </summary>
		bool IsDebugEnabled { get; }

		/// <summary>
		/// Logs a formatted message string with the Debug level.
		/// </summary>
		/// <param name="format">The format string.</param>
		/// <param name="args">The arguments.</param>
		void DebugFormat(string format, params object[] args);
	}
}
