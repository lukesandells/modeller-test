﻿using System;

namespace MI.Framework.Logging
{
	/// <summary>
	/// A provider of logging functions.
	/// </summary>
	public interface ILogProvider
	{
		/// <summary>
		/// Gets a new logger from the provider for a given type.
		/// </summary>
		/// <param name="type">The type for which log entries shall be created.</param>
		/// <returns>A new logger for the given type.</returns>
		ILog GetLogger(Type type);
	}
}
