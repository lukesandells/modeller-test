﻿namespace MI.Framework
{
	/// <summary>
	/// A copyable object.
	/// </summary>
	/// <typeparam name="TCopy">The type of the copy.</typeparam>
	public interface ICopyable<out TCopy>
		where TCopy : class
	{
		/// <summary>
		/// Returns a copy of the given object.
		/// </summary>
		/// <returns>A copy of the given object.</returns>
		TCopy Copy();
	}
}
