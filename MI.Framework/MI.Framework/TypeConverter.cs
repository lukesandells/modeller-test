﻿using MI.Framework.Validation;
using System;
using System.Collections.Generic;

namespace MI.Framework
{
	/// <summary>
	/// Converts between different types of objects.
	/// </summary>
	public static class TypeConverter
	{
		/// <summary>
		/// The set of registered converters.
		/// </summary>
		private static ICollection<ITypeConverter> _converters = new HashSet<ITypeConverter>() { new ExtensibleEnumTypeConverter() };

		/// <summary>
		/// Adds the given converter to be applied during conversions.
		/// </summary>
		/// <param name="converter">The converter to add.</param>
		public static void AddConverter(ITypeConverter converter)
		{
			_converters.Add(converter);
		}

		/// <summary>
		/// Returns the given object converted to the given type.
		/// </summary>
		/// <param name="obj">The object to convert.</param>
		/// <param name="conversionType">The type to which to convert the object.</param>
		/// <returns>The given object converted to the given type.</returns>
		public static object Convert(object obj, Type conversionType)
		{
			// If it's a nullable we need to use the underlying type to convert
			conversionType = Nullable.GetUnderlyingType(conversionType) ?? conversionType;

			if (obj == null)
			{
				return null;
			}

			if (obj.GetType() == conversionType)
			{
				return obj;
			}

			foreach (var converter in _converters)
			{
				if (converter.TryConvert(obj, conversionType, out var result))
				{
					return result;
				}
			}

			if (obj is IConvertible)
			{
				return System.Convert.ChangeType(obj, conversionType);
			}

			throw new InvalidCastException();
		}

		/// <summary>
		/// Returns the given object converted to the given type parameter.
		/// </summary>
		/// <typeparam name="T">The type to which to convert the given object.</typeparam>
		/// <param name="obj">The object to convert.</param>
		/// <returns>The given object converted to the given type.</returns>
		public static T Convert<T>(object obj)
		{
			return (T)Convert(obj, typeof(T));
		}
	}
}
