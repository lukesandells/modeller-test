﻿using System;
using System.Reflection;

using MI.Framework.Validation;

namespace MI.Framework
{
	/// <summary>
	/// Extension methods for the <see cref="MemberInfo"/> class.
	/// </summary>
	public static class MemberInfoExtensions
	{
		/// <summary>
		/// Gets the permanent ID for this enumerator.
		/// </summary>
		/// <param name="e">The enumerator for which to get the permanent ID.</param>
		/// <returns>The permanent ID.</returns>
		public static Guid GetPermanentId(this Enum e)
		{
			e = ArgumentValidation.AssertNotNull(e, nameof(e));
			return e.GetType().GetField(e.ToString()).GetPermanentId();
		}

		/// <summary>
		/// Gets the permanent ID for any member/type.
		/// </summary>
		/// <param name="element">The member/type for which to get the permanent ID.</param>
		/// <returns>The permanent ID.</returns>
		public static Guid GetPermanentId(this MemberInfo element)
		{
			element = ArgumentValidation.AssertNotNull(element, nameof(element));
			
			var attribute = element.GetCustomAttribute<PermanentGuidAttribute>(false);
			if (attribute == null)
			{
				throw new ArgumentException(string.Format(ExceptionMessage.PermanentIdAttributeNotDefined, MemberInfoDescription(element)));
			}

			return attribute.Value;
		}

		/// <summary>
		/// Gets a <see cref="String"/> description of a <see cref="MemberInfo"/> object.
		/// </summary>
		/// <param name="memberInfo">The member info object.</param>
		/// <returns>A string description of the given member info object.</returns>
		private static string MemberInfoDescription(MemberInfo memberInfo)
		{
			var description = memberInfo.Name + " " + memberInfo.MemberType.ToString();
			if (memberInfo.DeclaringType != null)
			{
				description = description + " of " + memberInfo.DeclaringType.ToString();
			}

			return description;
		}

		/// <summary>
		/// Returns whether the element defines a <see cref="PermanentGuidAttribute"/>.
		/// </summary>
		/// <param name="element">The member/type for test.</param>
		/// <returns><c>true</c> if the attribute is defined, otherwise <c>false</c>.</returns>
		public static bool HasPermanentId(this MemberInfo element)
		{
			return element.IsDefined(typeof(PermanentGuidAttribute), true);
		}
	}
}
