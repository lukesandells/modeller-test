﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Resources;
using MI.Framework.Validation;

namespace MI.Framework
{
	/// <summary>
	/// Extends an enumerator to allow values other than those that are pre-defined in the enumerator.
	/// </summary>
	/// <typeparam name="TEnum">The base enumerator.</typeparam>
	[Serializable]
	public struct ExtensibleEnum<TEnum> where TEnum : struct
	{
		/// <summary>
		/// The value of the <see cref="ExtensibleEnum{TEnum}"/>.
		/// </summary>
		private string _value;
		private string Value => _value ?? default(TEnum).ToString();

		/// <summary>
		/// Indicates whether the <see cref="ExtensibleEnum{TEnum}"/> has a value that is not defined in the base enumerator.
		/// </summary>
		public bool HasOtherValue => Attribute.IsDefined(typeof(TEnum).GetField(((TEnum)this).ToString()), typeof(OtherValueAttribute));

		/// <summary>
		/// Initialises a new instance of the <see cref="ExtensibleEnum{TEnum}"/> class.
		/// </summary>
		/// <param name="value">The initial enumerator value.</param>
		private ExtensibleEnum(string value)
		{
			if (!typeof(TEnum).IsEnum)
			{
				throw new FrameworkException(ExceptionMessage.TypeParameterForExtensibleEnumMustBeAnEnumerator);
			}

			_value = value;
		}

		/// <summary>
		/// Initialises a new instance of the <see cref="ExtensibleEnum{TEnum}"/> class.
		/// </summary>
		/// <param name="e">The initial enumerator value.</param>
		private ExtensibleEnum(TEnum e) : this(e.ToString())
		{
		}

		/// <summary>
		/// Gets the members of the <see cref="ExtensibleEnum{TEnum}"/> class.
		/// </summary>
		public static IList<ExtensibleEnum<TEnum>> Members => Enum.GetNames(typeof(TEnum)).Select(name => ExtensibleEnum<TEnum>.Parse(name)).ToList();
		
		/// <summary>
		/// Implicit conversion of an enumerator to an <see cref="ExtensibleEnum{TEnum}"/>.
		/// </summary>
		/// <param name="e">The enumerator to convert.</param>
		public static implicit operator ExtensibleEnum<TEnum>(TEnum e)
		{
			return new ExtensibleEnum<TEnum>(e.ToString());
		}

		/// <summary>
		/// Explicit conversion of an <see cref="ExtensibleEnum{TEnum}"/> to an enumerator.
		/// </summary>
		/// <param name="e">The <see cref="ExtensibleEnum{TEnum}"/> to convert.</param>
		public static explicit operator TEnum(ExtensibleEnum<TEnum> e)
		{
			// Try to parse the value into a TEnum
			if (Enum.TryParse<TEnum>(e.Value, out var result))
			{
				return result;
			}
			else
			{
				// Failed to parse, so try to find the enumerator member decorated with the OtherValue attribute.
				foreach (FieldInfo f in typeof(TEnum).GetFields())
				{
					if (Attribute.IsDefined(f, typeof(OtherValueAttribute)))
					{
						return (TEnum)Enum.Parse(typeof(TEnum), f.Name);
					}
				}

				throw new InvalidCastException(string.Format(ExceptionMessage.NoOtherValueDefined, typeof(TEnum)));
			}
		}

		/// <summary>
		/// Parses the given string into an <see cref="ExtensibleEnum{TEnum}"/>.
		/// </summary>
		/// <param name="s">The string to parse.</param>
		/// <returns>An <see cref="ExtensibleEnum{TEnum}"/>.</returns>
		public static ExtensibleEnum<TEnum> Parse(string s)
		{
			if (string.IsNullOrEmpty(s) || s.StartsWith(" ") || s.EndsWith(" "))
			{
				throw new ArgumentException(ExceptionMessage.InvalidStringValue);
			}
			
			return new ExtensibleEnum<TEnum>(s);
		}

		/// <summary>
		/// Returns a string representing this <see cref="ExtensibleEnum{TEnum}"/>.
		/// </summary>
		/// <returns>A string representing this <see cref="ExtensibleEnum{TEnum}"/></returns>
		public override string ToString()
		{
			return Value;
		}

		/// <summary>
		/// Gets a hash code for this <see cref="ExtensibleEnum{TEnum}"/>.
		/// </summary>
		/// <returns>A hash code.</returns>
		public override int GetHashCode()
		{
			return Value.GetHashCode();
		}

		/// <summary>
		/// Compares with another object for equality.
		/// </summary>
		/// <param name="obj">The object with which to compare.</param>
		/// <returns>True if the objects are equal, otherwise false.</returns>
		public override bool Equals(object obj)
		{
			// If parameter is null return false.
			if (obj == null)
			{
				return false;
			}

			// If parameter cannot be cast, return false.
			if (!(obj is ExtensibleEnum<TEnum>))
			{
				if (!(obj is TEnum))
				{
					return false;
				}
				else // Is a TEnum
				{
					return _value == obj.ToString();
				}
			}

			// Return true if the fields match
			return (((ExtensibleEnum<TEnum>)obj)._value == _value);
		}

		/// <summary>
		/// Compares two <see cref="ExtensibleEnum{TEnum}"/> objects to test for equality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are equal, otherwise <c>false</c>.</returns>
		public static bool operator ==(ExtensibleEnum<TEnum> left, ExtensibleEnum<TEnum> right)
		{
			// If both are null, or both are same instance, return true.
			if (ReferenceEquals(left, right))
			{
				return true;
			}

			// If one is null, but not both, return false.
			if (((object)left == null) || ((object)right == null))
			{
				return false;
			}

			// Return true if the fields match
			return left._value == right._value;
		}

		/// <summary>
		/// Compares two <see cref="ExtensibleEnum{TEnum}"/> objects to test for inequality.
		/// </summary>
		/// <param name="left">The left-hand side of the operator.</param>
		/// <param name="right">The right-hand side of the operator.</param>
		/// <returns><c>true</c> if the objects are not equal, otherwise <c>false</c>.</returns>
		public static bool operator !=(ExtensibleEnum<TEnum> left, ExtensibleEnum<TEnum> right)
		{
			return !(left == right);
		}
	}

	/// <summary>
	/// Non-generic methods to assist with the use of ExtensibleEnum
	/// </summary>
	public static class ExtensibleEnum
	{

		/// <summary>
		/// Gets the names of an extensible enum
		/// </summary>
		/// <param name="t">The type of enum. Throws an exception if this is not an extensible enum</param>
		/// <returns>The list of strings representing the names of the underlying type</returns>
		public static IEnumerable<string> GetNames(this Type t)
		{
			if (!t.IsExtensibleEnum())
			{
				throw new ArgumentException(ExceptionMessage.TypeMustBeExtensibleEnum);
			}
			return Enum.GetNames(t.GenericTypeArguments[0]);
		}

		/// <summary>
		/// Determines if the given type is an ExtensibleEnum
		/// </summary>
		/// <param name="t">The type to check</param>
		public static bool IsExtensibleEnum(this Type t)
		{
			t = Nullable.GetUnderlyingType(t) ?? t;
			return t.IsGenericType && (Nullable.GetUnderlyingType(t)?.GetGenericTypeDefinition() ?? t.GetGenericTypeDefinition()) == typeof(ExtensibleEnum<>);
		}

		/// <summary>
		/// Gets a lookup of the displayname for an Enum or ExtensibleEnum type
		/// </summary>
		/// <param name="t">The Enum type to resolve for. If this is not an Enum or ExtensibleEnum then an expetion is thrown.</param>
		/// <returns></returns>
		public static IDictionary<object, string> GetEnumDisplayNames(this Type t)
		{
			if (!t.IsEnum && !t.IsExtensibleEnum())
			{
				throw new ArgumentException(ExceptionMessage.TypeMustBeEnum);
			}
			return UnderlyingEnumType(t).GetEnumValues().OfType<object>().ToDictionary(k => k, GetEnumValueDisplayName);
		}

		/// <summary>
		/// Gets the display name of a specific enum value. 
		/// </summary>
		/// <param name="o">The Enum value to resolve for. If this is not an Enum or ExtensibleEnum then an expetion is thrown.</param>
		/// <returns></returns>
		public static string GetEnumValueDisplayName(object o)
		{
			if (!o.GetType().IsEnum && !o.GetType().IsExtensibleEnum())
			{
				throw new ArgumentException(ExceptionMessage.TypeMustBeEnum);
			}
			var enumType = UnderlyingEnumType(o.GetType());
			var propertyInfo = enumType.Assembly.GetType(enumType.FullName + "Name").GetTypeInfo().GetDeclaredProperty("ResourceManager");
			var resourceManager = (ResourceManager)propertyInfo.GetValue(null);
			return resourceManager.GetString(o.ToString());	
		}

		/// <summary>
		/// For a given string gets a corresponding enum value based on the display names
		/// </summary>
		/// <param name="s">The string to resolve</param>
		/// <param name="enumType">The type of enum being targeted. If this is not an Enum or ExtensibleEnum then an expetion is thrown.</param>
		/// <returns>The underlying enum type or null if it cannot be resolved (e.g. an Other value)</returns>
		public static object GetEnumValueFromDisplayName(string s, Type enumType)
		{
			if (!enumType.IsEnum && !enumType.IsExtensibleEnum())
			{
				throw new ArgumentException(ExceptionMessage.TypeMustBeEnum);
			}
			var lookup = GetEnumDisplayNames(enumType);
			return lookup.Keys.FirstOrDefault(k => lookup[k] == s);
		}

		/// <summary>
		/// The underyling enum type
		/// </summary>
		/// <param name="t">The type to examin</param>
		/// <returns></returns>
		public static Type UnderlyingEnumType(Type t)
		{
			t = Nullable.GetUnderlyingType(t) ?? t;
			return t.IsEnum
				? t
				: t.GenericTypeArguments[0];
		}
	}
}
